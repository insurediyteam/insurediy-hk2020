<?php

defined('_JEXEC') or die;

class plgSystemMysystem extends JPlugin {

	public function __construct(&$subject, $config) {
		parent::__construct($subject, $config);
	}

	public function onAfterInitialise() {
		$app = JFactory::getApplication();

		defined('DS') or define('DS', DIRECTORY_SEPARATOR);
		defined('JPATH_INCS') or define('JPATH_INCS', JPATH_ROOT . DS . 'incs');
		//include lang file
		JFactory::getLanguage()->load('common', JPATH_ROOT);
		JFactory::getLanguage()->load('common_idy', JPATH_ROOT);

		if (!$app->isAdmin()) {
			JLoader::import('incs.form.field', JPATH_ROOT);
		}
		//include php files
		require_once JPATH_INCS . '/lib/fpdf/fpdf.php';
		require_once JPATH_INCS . '/lib/fpdf/fpdi.php';

		JLoader::import('incs.myhelper', JPATH_ROOT);
		JLoader::import('incs.insurediyhelper', JPATH_ROOT);
		JLoader::import('incs.pdfhelper', JPATH_ROOT);
		JLoader::import('incs.xmlhelper', JPATH_ROOT);

		JLoader::import('incs.myuri', JPATH_ROOT);
		JLoader::import('incs.constants', JPATH_ROOT);

		JHtml::_('behavior.framework', TRUE); //load mootool first

		JHtml::_('Jquery.framework', TRUE);

		$jsUrls = MyUri::getUrls('js_scripts');
		JHtml::_('script', $jsUrls['myready'], FALSE, FALSE);

//		JLoader::import('incs.globals', JPATH_ROOT);
		//add paths
		JHtml::addIncludePath(JPATH_INCS . DS . 'html');
		JForm::addFieldPath(JPATH_INCS . DS . 'form' . DS . 'fields');
		JForm::addRulePath(JPATH_INCS . DS . 'form' . DS . 'rules');
		JTable::addIncludePath(JPATH_SITE . DS . 'incs' . DS . 'tables');
	}

	public function onAfterRoute() {
		$app = JFactory::getApplication();
		if ($app->isAdmin()) {
			return;
		}
	}

	public function onBeforeRender() {
		
	}

}
