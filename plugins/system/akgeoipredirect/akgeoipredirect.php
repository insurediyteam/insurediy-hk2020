<?php
/**
 * @package		akgeoip
 * @copyright	Copyright (c)2013-2017 Nicholas K. Dionysopoulos
 * @license		GNU General Public License version 3, or later
 *
 * This plugin contains code from the following projects:
 * -- Composer, (c) Nils Adermann <naderman@naderman.de>, Jordi Boggiano <j.boggiano@seld.be>
 * -- GeoIPv2, (c) MaxMind www.maxmind.com
 * -- Guzzle, (c) 2011 Michael Dowling, https://github.com/mtdowling <mtdowling@gmail.com>
 * -- MaxMind DB Reader PHP API, (c) MaxMind www.maxmind.com
 * -- Symfiny, (c) 2004-2013 Fabien Potencier
 *
 * Third party software is distributed as-is, each one having its own copyright and license.
 * For more information please see the respective license and readme files, found under
 * the lib directory of this plugin.
 */

defined('_JEXEC') or die();

// PHP version check
if(defined('PHP_VERSION')) {
	$version = PHP_VERSION;
} elseif(function_exists('phpversion')) {
	$version = phpversion();
} else {
	$version = '5.0.0'; // all bets are off!
}
if(!version_compare($version, '5.3.0', '>=')) return;

JLoader::import('joomla.application.plugin');

class plgSystemAkgeoipredirect extends JPlugin
{
	public function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);
	}

	public function onAfterRoute()
	{
		// Get the application object.
		$app = JFactory::getApplication();

		// Do not enable the plugin at Joomla! backend
		if ($app->isAdmin())
		{
			return;
		}
        
        // Get Params from plugin
		$plugin 			= JPluginHelper::getPlugin( 'system', 'akgeoipredirect' );
		$params 			= new JRegistry($plugin->params); 
        
        $redirect = array();
        for($i=1; $i<=3; $i++){
            if($params->get('country_code'.$i) && $params->get('redirect'.$i)){
                $obj = new JObject();
                $obj->country_code = $params->get('country_code'.$i);
                $obj->redirect = $params->get('redirect'.$i);
                $redirect[] = $obj;
            }
        }
        
        // Check first time redirect
        $session = JFactory::getSession();
        $first_redirect = $session->get('akgeoipredirect_firsttime');       
        
        // Get the visitor's IP address
        $ip = FOFUtilsIp::getIp();
        $geoip     = new AkeebaGeoipProvider();
        $country   = $geoip->getCountryCode($ip);
        
        //check home page
        $home = false;                
        $app = JFactory::getApplication();
        $menu = $app->getMenu();
        $lang = JFactory::getLanguage();
        if ($menu->getActive() == $menu->getDefault($lang->getTag())) {
        	$home = true;
        }
        
        if($home && $redirect && !$first_redirect && $country){          
            // get URL
            $http_s = isset($_SERVER['HTTPS']) ? "https" : "http";	
            $current_url = $http_s . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            
            // Build the destination URL
            $flag = false;
            foreach($redirect as $row){
                $country_codes = $row->country_code;
                
                foreach($country_codes as $value){
                    if($value == $country){
                        $flag = true;
                        break;
                    }
                }
                
                if($flag){
                    $destination_url = $row->redirect;
                    break;
                }
            }
            if($flag == false){
                $destination_url = $current_url;
            }
            
            // Build the final destination URL
    		if (!empty($destination_url)){
    			$url = $destination_url;
    		}else{
    			$url = $current_url;
    		}
    
    		// If the final destination URL is not the same with the current, then redirect to the final destination with www or non-www.
    		if (strpos($current_url, $destination_url) === false){
          		$this->redirectTo($url);
        	}
        } 
	}
    
    // Redirect URL
	public static function redirectTo($url)
	{
		if (!empty($url))
		{
			$session = JFactory::getSession();
            $session->set('akgeoipredirect_firsttime', '1');

            $app = JFactory::getApplication();
            $app->redirect(JRoute::_($url, false));            
		}
	}
}