<?php
/**
 * @version		$Id$
 * @author		JoomlaUX
 * @package		Joomla!
 * @subpackage	JUX_MegaMenu_Framework
 * @copyright	Copyright (C) 2008 - 2013 by JoomlaUX Solutions. All rights reserved.
 * @license		http://www.gnu.org/licenses/gpl.html GNU/GPL version 3, SEE LICENSE.php
 */

// no direct access
defined('_JEXEC') or die('Restricted access');
/**
 * Radio List Element
 *
 * @since      Class available since Release 1.2.0
 */
class JFormFieldCountries extends JFormField
{
	/**
	 * Element name
	 *
	 * @access	protected
	 * @var		string
	 */
	protected $type = 'Countries';

	function getInput( ) {
 	    $groups = array(
            "SG" => "Singapore", "HK" => "Hong Kong", "MY" => "Malaysia", "VN" => "Vietnam"
        );

		$groupHTML = array();
		if ($groups && count ($groups)) {
			foreach ($groups as $code=>$name){
				$groupHTML[] = JHTML::_('select.option', $code, $name);
			}
		}
        
        $attr = '';
        if($this->required) $attr .= 'required="true"';
		$lists = JHTML::_('select.genericlist', $groupHTML, $this->name.'[]', ' multiple="multiple" '.$attr, 'value', 'text', $this->value);

		return $lists.'<div style="height:0px;"></div>';
	}
} 