<?php

/**
 * Main Plugin File
 * Does all the magic!
 *
 * @package         Modules Anywhere
 * @version         3.3.5
 *
 * @author          Peter van Westen <peter@nonumber.nl>
 * @link            http://www.nonumber.nl
 * @copyright       Copyright © 2013 NoNumber All Rights Reserved
 * @license         http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 */
defined('_JEXEC') or die;

/**
 * Button Plugin that places Editor Buttons
 */
class plgButtonEmailVariables extends JPlugin {

	protected $autoloadLanguage = true;

	/**
	 * Display the button
	 *
	 * @return array A two element array of ( imageName, textToInsert )
	 */
	function onDisplay($name, $asset, $author) {
		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		$extension = $app->input->get('option');

		if ($asset == '') {
			$asset = $extension;
		}

		if ($user->authorise('core.edit', $asset) || $user->authorise('core.create', $asset) || (count($user->getAuthorisedCategories($asset, 'core.create')) > 0) || ($user->authorise('core.edit.own', $asset) && $author == $user->id) || (count($user->getAuthorisedCategories($extension, 'core.edit')) > 0) || (count($user->getAuthorisedCategories($extension, 'core.edit.own')) > 0 && $author == $user->id)) {
//			$link = 'index.php?option=com_media&amp;view=images&amp;tmpl=component&amp;e_name=' . $name . '&amp;asset=' . $asset . '&amp;author=' . $author;
			$link = '../index.php?option=com_popup&amp;view=plugins&amp;layout=variables&amp;tmpl=component&amp;e_name=' . $name;

			JHtml::_('behavior.modal');
			$button = new JObject;
			$button->modal = true;
			$button->class = 'btn';
			$button->link = $link;
			$button->text = JText::_('PLG_EMAIL_VARIABLES');
			$button->name = 'equalizer';
			$button->options = "{handler: 'iframe', size: {x: 350, y: 500}}";

			return $button;
		} else {
			return false;
		}
	}

}
