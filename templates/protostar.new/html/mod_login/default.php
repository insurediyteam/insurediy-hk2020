<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_login
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
//JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.formvalidation');
//$return = base64_encode("index.php?option=com_users&view=dashboard&Itemid=134");
?>
<table>
	<tbody>
		<tr>
			<td><a onclick="javascript:$('insurediy-login-box').toggle();" href="javascript:void(0)" class="login-button"><?php echo JText::_("MOD_LOGIN_LOGIN_BTN"); ?></a>&nbsp;</td>
			<td><a href="index.php?option=com_users&amp;view=registration&amp;Itemid=133" class="login-button"><?php echo JText::_("MOD_LOGIN_SIGN_UP_BTN"); ?></a>&nbsp;</td>
			<td><img class="hasTooltip" src="images/help-quote.png" alt="help-quote" data-original-title="<?php echo JText::_("MOD_LOGIN_SIGN_UP_DESC"); ?>" /></td>
		</tr>
	</tbody>
</table>
<div id="insurediy-login-box" style="display: none;">
	<a class="login-close" href="#" onclick="javascript:$('insurediy-login-box').toggle();">&nbsp;</a>
	<div class="clear"></div>
	<form style="padding: 20px;padding-top: 8px" action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" id="login-form" class="form-inline form-validate">
		<?php if ($params->get('pretext')) : ?>
			<div class="pretext">
				<p><?php echo $params->get('pretext'); ?></p>
			</div>
		<?php endif; ?>
		<div class="userdata">
			<div id="form-login-username" class="control-group">
				<div class="controls">
					<?php if (!$params->get('usetext')) : ?>
						<div class="input-prepend">
							<input id="modlgn-username" type="text" name="username" class="input-small required validate-username" tabindex="0" size="18" placeholder="<?php echo JText::_('JGLOBAL_EMAIL') ?>" />
						</div>
					<?php else: ?>
						<label for="modlgn-username"><?php echo JText::_('MOD_LOGIN_VALUE_USERNAME') ?></label>
						<input id="modlgn-username" type="text" name="username" class="input-small required validate-username" tabindex="0" size="18" placeholder="<?php echo JText::_('JGLOBAL_EMAIL') ?>" />
					<?php endif; ?>
				</div>
			</div>
			<div id="form-login-password" class="control-group">
				<div class="controls">
					<?php if (!$params->get('usetext')) : ?>
						<div class="input-prepend">
							<input id="modlgn-passwd" type="password" name="password" class="input-small required" tabindex="0" size="18" placeholder="<?php echo JText::_('JGLOBAL_PASSWORD') ?>" />
						</div>
					<?php else: ?>
						<label for="modlgn-passwd"><?php echo JText::_('JGLOBAL_PASSWORD') ?></label>
						<input id="modlgn-passwd" type="password" name="password" class="input-small required" tabindex="0" size="18" placeholder="<?php echo JText::_('JGLOBAL_PASSWORD') ?>" />
					<?php endif; ?>
				</div>
			</div>
			<ul class="unstyled">
				<li style="text-align:right;font-size:11px;">
					<a href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>" style="color:#fff;"><?php echo JText::_('MOD_LOGIN_FORGOT_YOUR_PASSWORD'); ?></a>
				</li>
			</ul>
			<?php if (count($twofactormethods) > 1): ?>
				<div id="form-login-secretkey" class="control-group">
					<div class="controls">
						<?php if (!$params->get('usetext')) : ?>
							<div class="input-prepend input-append">
								<span class="add-on">
									<span class="icon-star hasTooltip" title="<?php echo JText::_('JGLOBAL_SECRETKEY'); ?>">
									</span>
									<label for="modlgn-secretkey" class="element-invisible"><?php echo JText::_('JGLOBAL_SECRETKEY'); ?>
									</label>
								</span>
								<input id="modlgn-secretkey" type="text" name="secretkey" class="input-small" tabindex="0" size="18" placeholder="<?php echo JText::_('JGLOBAL_SECRETKEY') ?>" />
								<span class="btn width-auto hasTooltip" title="<?php echo JText::_('JGLOBAL_SECRETKEY_HELP'); ?>">
									<span class="icon-help"></span>
								</span>
							</div>
						<?php else: ?>
							<label for="modlgn-secretkey"><?php echo JText::_('JGLOBAL_SECRETKEY') ?></label>
							<input id="modlgn-secretkey" type="text" name="secretkey" class="input-small" tabindex="0" size="18" placeholder="<?php echo JText::_('JGLOBAL_SECRETKEY') ?>" />
							<span class="btn width-auto hasTooltip" title="<?php echo JText::_('JGLOBAL_SECRETKEY_HELP'); ?>">
								<span class="icon-help"></span>
							</span>
						<?php endif; ?>

					</div>
				</div>
			<?php endif; ?>
			<div style="margin-top:29px;">
				<?php if (JPluginHelper::isEnabled('system', 'remember')) : ?>
					<div id="form-login-remember" class="control-group" style="float:left;color:#fff;margin-top:5px;">
						<input id="modlgn-remember" type="checkbox" name="remember" value="yes"/>&nbsp;<label for="modlgn-remember" class="control-label"><?php echo JText::_('MOD_LOGIN_REMEMBER_ME') ?></label>
					</div>
				<?php endif; ?>

				<div id="form-login-submit" class="control-group" style="float:right">
					<div class="controls">
						<button type="submit" tabindex="0" name="Submit" class="btn btn-primary" style="padding:5px 20px;font-size:14px !important;height:30px;"><?php echo JText::_('JLOGIN') ?></button>
					</div>
				</div>
				<div class="clear"></div>
			</div>
			<?php $usersConfig = JComponentHelper::getParams('com_users'); ?>

			<input type="hidden" name="option" value="com_users" />
			<input type="hidden" name="task" value="user.login" />
			<input type="hidden" name="return" value="<?php echo $return; ?>" />
			<?php echo JHtml::_('form.token'); ?>
		</div>
		<?php if ($params->get('posttext')) : ?>
			<div class="posttext">
				<p><?php echo $params->get('posttext'); ?></p>
			</div>
		<?php endif; ?>
	</form>
</div>
