<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

$user = JFactory::getUser();
$homeUrl = MyUri::getUrls("site");

$CurrUser = MyHelper::getCurrentUser();
?>
<?php if (FALSE): ?>
	<div id="fb-root"></div>
	<script>
		window.fbAsyncInit = function() {
			FB.init({
				appId: '316454825196499',
				xfbml: true,
				version: 'v2.0'
			});
		};
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id))
				return;
			js = d.createElement(s);
			js.id = id;
			js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=316454825196499&version=v2.0";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));
	</script>
	<script src="http://tjs.sjs.sinajs.cn/open/api/js/wb.js" type="text/javascript" charset="utf-8"></script>
<?php endif; ?>
<fieldset id="users-profile-core">
	<div class="dl-horizontal">
		<div class="left">
			<div class="label1"><?php echo JText::_('COM_USERS_PROFILE_GENDER_LABEL'); ?></div>
			<div class="field"><?php echo (strtolower($this->data->gender) == "f") ? "Female" : "Male"; ?></div>
			<div class="clear"></div>
			<div class="label1"><?php echo JText::_('COM_USERS_PROFILE_DOB_LABEL'); ?></div>
			<div class="field"><?php echo $this->data->dob; ?></div>
			<div class="clear"></div>
			<div class="label1"><?php echo JText::_('COM_USERS_PROFILE_MARITAL_STATUS_LABEL'); ?></div>
			<div class="field"><?php echo (strtolower($this->data->marital_status) == "m") ? "Married" : "Single"; ?></div>
			<div class="clear"></div>
			<div class="label1"><?php echo JText::_('COM_USERS_PROFILE_DEPENDENTS_LABEL'); ?></div>
			<div class="field"><?php echo ((int) $this->data->dep_young_children + (int) $this->data->dep_kids + (int) $this->data->dep_teenagers + (int) $this->data->dep_young_adults + (int) $this->data->dep_elderly); ?></div>
			<div class="clear"></div>
			<div class="label1"><?php echo JText::_('COM_USERS_PROFILE_MONTHLY_SALARY_LABEL'); ?></div>
			<div class="field"><?php echo JText::_($this->data->monthly_salary); ?></div>
			<div class="clear"></div>
			<div class="label1"><?php echo JText::_('COM_USERS_PROFILE_OCCUPATION_LABEL'); ?></div>
			<div class="field"><?php echo $this->data->occupation; ?></div>
			<div class="clear"></div>
			<div style="border-bottom: 1px solid #ccc; width: 95%; height: 0;margin-bottom: 20px;"></div>
			<div class="label1"><?php echo JText::_('COM_USERS_PROFILE_NAME_LABEL'); ?></div>
			<div class="field"><?php echo $this->data->name; ?></div>
			<div class="clear"></div>
			<div class="label1"><?php echo JText::_('COM_USERS_PROFILE_LASTNAME_LABEL'); ?></div>
			<div class="field"><?php echo $this->data->lastname; ?></div>
			<div class="clear"></div>
			<div class="label1"><?php echo JText::_('COM_USERS_PROFILE_EMAIL_LABEL'); ?></div>
			<div class="field"><?php echo $this->data->email; ?></div>
			<div class="clear"></div>
			<div class="label1"><?php echo JText::_('COM_USERS_PROFILE_PASSWORD_LABEL'); ?></div>
			<div class="field">***</div>
			<div class="clear"></div>
		</div>

		<div class="right">
			<div class="label2"><?php echo JText::_('COM_USERS_PROFILE_ADDRESS_LABEL'); ?></div>
			<div class="field">
				<?php echo JText::sprintf("JGLOBAL_INSUREDIY_ADDRESS_FORMAT_1", $this->data->room_no, $this->data->floor_no, $this->data->block_no, $this->data->building_name, $this->data->street_no, $this->data->district, $this->data->country); ?>
			</div>
			<div class="label2"><?php echo JText::_('COM_USERS_REGISTER_NATIONALITY_LABEL'); ?></div>
			<div class="field"><?php echo InsurediyHelper::getNationality($this->data->nationality); ?></div>
			<div class="clear"></div>
			<div class="label2"><?php echo JText::_('COM_USERS_PROFILE_POSTALCODE_LABEL'); ?></div>
			<div class="field"><?php echo $this->data->postal_code; ?></div>
			<div class="clear"></div>
			<div class="label2"><?php echo JText::_('COM_USERS_PROFILE_CONTACT_NO_LABEL'); ?></div>
			<div class="field"><?php echo $this->data->country_code . ' ' . $this->data->contact_no; ?></div>
			<div class="clear"></div>
			<div class="spacer-1"></div>
			<div class="container5 clearfix" style="padding: 20px 20px; margin-top:10px;">
				<div class="clearfix">
					<div>
						<i class="icon-referral"></i><span class="blue-title-text"><?php echo JText::_("PROFILE_REFERRAL_CODE"); ?></span>
					</div>
					<div style="padding:10px 0 ;">
						<input type="text" readonly="readonly" class="inputbox border-box" style="width: 100%; height: 35px;" value="<?php echo $user->referral_id; ?>" />
					</div>
					<div class="grey-hrule"></div>
					<div class="spacer-2"></div>
					<div>
						<i class="icon-social-share"></i><span class="blue-title-text"><?php echo JText::_("SOCIAL_SHARE_TITLE"); ?></span>
					</div>
					<div class="spacer-2"></div>
					<div class="clearfix">
						<div style="float:left;margin-right: 20px;padding-top:6px;">
							<div class="social-btn-wrapper">
								<script type="text/javascript">var sharer = "<?php echo InsureDIYHelper::getFbSharer() ?>";</script>
								<a href="javascript: void(0)" onclick="window.open(sharer, 'sharer', 'width=626,height=436');"><img src="images/layout-icons/facebook.png"></a>
							</div>
						</div>
						<div style="float:left;">
							<div class="social-btn-wrapper">
								<script type="text/javascript">var sharerwb = "<?php echo InsureDIYHelper::getWeiboSharer() ?>";</script>
								<a href="javascript: void(0)" onclick="window.open(sharerwb, 'sharer', 'width=626,height=436');"><img src="images/layout-icons/weibo.png"></a>
							</div>
						</div>

						<?php if (FALSE): ?>
							<div class="cpan2">
								<div class="fb-share-button" data-href="<?php echo $homeUrl; ?>"></div>
							</div>
							<div class="cpan2">
								<wb:share-button addition="simple" type="button" default_text="Body" ralateUid="006597970317" pic=""></wb:share-button>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</fieldset>
<?php if ($CurrUser->s_popup): ?>
	<div id="s_popup" class="modal hide fade custom-modal" style="background-color:#e8f8ff">
		<div class="modal-body">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<div class="clearfix" style="padding: 20px 20px; margin-top:10px;">
				<div class="clearfix">
					<div>
						<i class="icon-referral"></i><span class="blue-title-text"><?php echo JText::_("PROFILE_REFERRAL_CODE"); ?></span>
					</div>
					<div style="padding:10px 0 ;">
						<input type="text" readonly="readonly" class="inputbox border-box" style="width: 100%; height: 35px;" value="<?php echo $user->referral_id; ?>" />
					</div>
					<div class="grey-hrule"></div>
					<div class="spacer-2"></div>
					<div>
						<i class="icon-social-share"></i><span class="blue-title-text"><?php echo JText::_("SOCIAL_SHARE_TITLE"); ?></span>
					</div>
					<div class="spacer-2"></div>
					<div class="clearfix">
						<div style="float:left;margin-right: 20px;padding-top:6px;">
							<div class="social-btn-wrapper">
								<script type="text/javascript">var sharer = "<?php echo InsureDIYHelper::getFbSharer() ?>";</script>
								<a href="javascript: void(0)" target="_blank" onclick="window.open(sharer, 'sharer', 'width=626,height=436');"><img src="images/layout-icons/facebook.png"></a>
							</div>
						</div>
						<div style="float:left;">
							<div class="social-btn-wrapper">
								<script type="text/javascript">var sharerwb = "<?php echo InsureDIYHelper::getWeiboSharer() ?>";</script>
								<a href="javascript: void(0)" target="_blank" onclick="window.open(sharerwb, 'sharer', 'width=626,height=436');"><img src="images/layout-icons/weibo.png"></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		jQuery("#s_popup").modal("show");
	</script>
<?php endif; ?>

