<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::_('jquery.framework');
?>

<div class="profile <?php echo $this->pageclass_sfx ?>">
	<?php if ($this->params->get('show_page_heading')) : ?>
		<div class="header-top">
			<div class="h1-heading"><?php echo $this->escape($this->params->get('page_heading')); ?></div>
		</div>
	<?php else: ?>
		<div class="header-top">
			<div class="h1-heading">Profile</div>
			<div class="customer-dashboard-menu">{modulepos insurediy-my-dashboard-menu}</div>
			<div class="clear"></div>
		</div>
	<?php endif; ?>

	<div class="padding20">
		<?php if (FALSE): ?>
			<div class="top-module">&nbsp;</div>
		<?php endif; ?>

		<?php echo $this->loadTemplate('core'); ?>

		<?php //echo $this->loadTemplate('params');  ?>

		<?php //echo $this->loadTemplate('custom');  ?>

		<div class="form-actions" style="padding:20px 0;">
			<div style="float:left;margin:15px 0;">
				{modulepos insurediy-secured-ssl}
			</div>
			<div class="btn-toolbar" style="float:right">
				<div class="btn-group">
					<a class="btn btn-primary validate" style="margin-right:10px;" href="<?php echo JRoute::_('index.php?Itemid=404') ?>">
						<?php echo JText::_('COM_USERS_SHOW_INSURANCE_CALCULATOR_ANALYSIS') ?>
					</a>
					<?php if (JFactory::getUser()->id == $this->data->id) : ?>
						<a class="btn btn-primary validate" href="<?php echo JRoute::_('index.php?option=com_users&task=profile.edit&user_id=' . (int) $this->data->id); ?>">
							<?php echo JText::_('COM_USERS_EDIT_PROFILE') ?>
						</a>
					<?php endif; ?>
				</div>
			</div>
			<div class="clear"></div>
		</div>
	</div>

</div>
