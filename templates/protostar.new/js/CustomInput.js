/*-------------------------------------------------------------------- 
 * MooTools Class: CustomInput
 * by Philip Hutchison, http://pipwerks.com
 * Article: http://pipwerks.com/2010/03/11/custominput/
 *
 * Based on jQuery plugin: customInput()
 * by Maggie Wachs and Scott Jehl, http://www.filamentgroup.com
 * Copyright (c) 2009 Filament Group
 * Dual licensed under the MIT (filamentgroup.com/examples/mit-license.txt) and GPL (filamentgroup.com/examples/gpl-license.txt) licenses.
 * Article: http://www.filamentgroup.com/lab/accessible_custom_designed_checkbox_radio_button_inputs_styled_css_jquery/  
--------------------------------------------------------------------*/

var CustomInput = new Class({

    initialize: function (selector){
    
        if(!selector){ selector = "input"; }
        
        var checked = "checked", checkedHover = "checkedHover", checkedFocus = "checkedFocus";
        
        /* Helper functions */
        function getLabel(prop){ return document.id(document.body).getElement("label[for=" +prop +"]"); }
        
        function isCheckbox(el){ return el.type === "checkbox"; }
        
        function isRadio(el){ return el.type === "radio"; }
        
        function wrapElements(input, label){ 
            return new Element("div", { "class":"custom-" +input.type }).inject(input, "before").adopt(input).adopt(label);
        }
        
        /* The dirty work */
		var inputs = $$(selector);
        inputs.each(function(input){
        
            if(isCheckbox(input) || isRadio(input)){
            
                var label = getLabel(input.id);
                wrapElements(input, label);
                
                label.addEvents({ 
                    mouseenter: function (){
                        var input = document.id(this.get("for"));
                        this.addClass("hover");
                        if(isCheckbox(input) && this.hasClass(checked)){ this.addClass(checkedHover); }
                    }, 
                    mouseleave: function (){ this.removeClass("hover").removeClass(checkedHover); }
                });
                
                input.addEvents({
                    click: function (){
                        var label = getLabel(this.id);
                        if(isRadio(this)){
                            $$("input[name=" +this.name +"]").each(function (el){ getLabel(el.id).removeClass(checked); });
                        }
                        if(label.hasClass(checked)){
                            label.removeClass(checked).removeClass(checkedHover).removeClass(checkedFocus);
                        } else { 
                            label.addClass(checked);
                        }                    
                    }, 
                    focus: function(){
                        var label = getLabel(this.id);
                        label.addClass("focus");
                        if(isCheckbox(this) && label.hasClass(checked)){ label.addClass(checkedFocus); }
                    }, 
                    blur: function (){ getLabel(this.id).removeClass("focus").removeClass(checkedFocus); }
                });    
            }
        });
		return inputs;
    }
});