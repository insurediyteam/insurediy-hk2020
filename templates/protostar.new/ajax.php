<?php defined('_JEXEC') or die('Restricted access'); ?>
<?php
	$app = JFactory::getApplication();

	//clear meta tags
	$jDoc = JFactory::getDocument();
	$jDoc->setDescription('');
	$jDoc->setGenerator('');
	$jDoc->_metaTags = array();
	$jDoc->setTitle('');
?>
<?php if ('1' !== $app->input->getString('xhead')): ?>
	<jdoc:include type="head" />
<?php endif; ?>
<?php if ($app->input->getString('type') == 'raw'): ?>
	<jdoc:include type="component" />
<?php else: ?>
	<jdoc:include type="message" />
	<jdoc:include type="component" />
<?php endif; ?>