<?php
	/**
		* @package     Joomla.Site
		* @subpackage  Templates.protostar
		*
		* @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
		* @license     GNU General Public License version 2 or later; see LICENSE.txt
	*/
	defined('_JEXEC') or die;
	
	// Getting params from template
	$params = JFactory::getApplication()->getTemplate(true)->params;
	
	$app			 = JFactory::getApplication();
	$doc			 = JFactory::getDocument();
	$this->language	 = $doc->language;
	$this->direction = $doc->direction;
	$session		 = JFactory::getSession();
	$sef = "en";
	switch ($this->language) {
    case "zh-tw":
		$sef = "zh";
    break;
	}
	
	/*function is_bot($user_agent) {
 
		$botRegexPattern = "(googlebot\/|Googlebot\-Mobile|Googlebot\-Image|Google favicon|Mediapartners\-Google|bingbot|slurp|java|wget|curl|Commons\-HttpClient|Python\-urllib|libwww|httpunit|nutch|phpcrawl|msnbot|jyxobot|FAST\-WebCrawler|FAST Enterprise Crawler|biglotron|teoma|convera|seekbot|gigablast|exabot|ngbot|ia_archiver|GingerCrawler|webmon |httrack|webcrawler|grub\.org|UsineNouvelleCrawler|antibot|netresearchserver|speedy|fluffy|bibnum\.bnf|findlink|msrbot|panscient|yacybot|AISearchBot|IOI|ips\-agent|tagoobot|MJ12bot|dotbot|woriobot|yanga|buzzbot|mlbot|yandexbot|purebot|Linguee Bot|Voyager|CyberPatrol|voilabot|baiduspider|citeseerxbot|spbot|twengabot|postrank|turnitinbot|scribdbot|page2rss|sitebot|linkdex|Adidxbot|blekkobot|ezooms|dotbot|Mail\.RU_Bot|discobot|heritrix|findthatfile|europarchive\.org|NerdByNature\.Bot|sistrix crawler|ahrefsbot|Aboundex|domaincrawler|wbsearchbot|summify|ccbot|edisterbot|seznambot|ec2linkfinder|gslfbot|aihitbot|intelium_bot|facebookexternalhit|yeti|RetrevoPageAnalyzer|lb\-spider|sogou|lssbot|careerbot|wotbox|wocbot|ichiro|DuckDuckBot|lssrocketcrawler|drupact|webcompanycrawler|acoonbot|openindexspider|gnam gnam spider|web\-archive\-net\.com\.bot|backlinkcrawler|coccoc|integromedb|content crawler spider|toplistbot|seokicks\-robot|it2media\-domain\-crawler|ip\-web\-crawler\.com|siteexplorer\.info|elisabot|proximic|changedetection|blexbot|arabot|WeSEE:Search|niki\-bot|CrystalSemanticsBot|rogerbot|360Spider|psbot|InterfaxScanBot|Lipperhey SEO Service|CC Metadata Scaper|g00g1e\.net|GrapeshotCrawler|urlappendbot|brainobot|fr\-crawler|binlar|SimpleCrawler|Livelapbot|Twitterbot|cXensebot|smtbot|bnf\.fr_bot|A6\-Indexer|ADmantX|Facebot|Twitterbot|OrangeBot|memorybot|AdvBot|MegaIndex|SemanticScholarBot|ltx71|nerdybot|xovibot|BUbiNG|Qwantify|archive\.org_bot|Applebot|TweetmemeBot|crawler4j|findxbot|SemrushBot|yoozBot|lipperhey|y!j\-asr|Domain Re\-Animator Bot|AddThis|YisouSpider|BLEXBot|YandexBot|SurdotlyBot|AwarioRssBot|FeedlyBot|Barkrowler|Gluten Free Crawler|Cliqzbot)";
		return preg_match("/{$botRegexPattern}/", $user_agent);
 
	}
 
	if ( !is_bot($_SERVER['HTTP_USER_AGENT']) ) {
		
		// ....code for the call to geoplugin.net
		$menu = $app->getMenu();
		$flag1 = $app->input->get('flag', 1);
		if ($menu->getActive()->home == $menu->getDefault()->home && $flag1) {

			$xmldata = @file_get_contents("http://www.geoplugin.net/xml.gp?ip=".$_SERVER['REMOTE_ADDR']);
			
			$xml = simplexml_load_string($xmldata); 
	
			$var_country_code = trim($xml->geoplugin_countryCode); 
			
			if ($var_country_code == "SG") {
				header('Location: http://www.insurediy.com.sg/');exit;
			}
			else if ($var_country_code == "MY") {
				header('Location: http://www.insurediy.com.my/');exit;
			}
			else {
				//header('Location: http://www.insurediy.com');
			}
		}
	}*/
	
	
	
	
	// Detecting Active Variables
	$option		 = $app->input->getCmd('option', '');
	$view		 = $app->input->getCmd('view', '');
	$layout		 = $app->input->getCmd('layout', '');
	$task		 = $app->input->getCmd('task', '');
	$itemid		 = $app->input->getCmd('Itemid', '');
	$sitename	 = $app->getCfg('sitename');
	$showMessage = TRUE;
	$urls		 = MyUri::getUrls();
	
	if ($task == "edit" || $layout == "form") {
		$fullWidth = 1;
		} else {
		$fullWidth = 0;
	}
	$noMessageOptions = array(
	"com_insurediylife",
	"com_insurediytravel",
	"com_insurediyci",
	"com_insurediyhospitalself",
	"com_insurediyhospitalchild",
	"com_insurediydomestic"
	);
	if (in_array($option, $noMessageOptions)) {
		$showMessage = FALSE;
	}
	//exec
	//exec("pdftk images/test.pdf dump_data_fields > images/fields.txt");
	//MyHelper::debug(MyHelper::generateReferralId());
	//MyHelper::debug(MyHelper::generateRandomId(33244));
	//exit;
	// Add JavaScript Frameworks
	JHtml::_('bootstrap.framework');
	$doc->addScript('templates/' . $this->template . '/js/template_mootools.js');
	$doc->addScript('templates/' . $this->template . '/js/template.js');
	$doc->addScript($urls['js'] . 'placeholders/placeholders.min.js');
	
	// Add Stylesheets
	$doc->addStyleSheet('templates/' . $this->template . '/css/template.css');
	$doc->addStyleSheet('templates/' . $this->template . '/css/template_a.css?v=1.0');
	$doc->addStyleSheet('templates/' . $this->template . '/css/templatez.css');
	$doc->addStyleSheet('templates/' . $this->template . '/css/templateci.css');
	
	$broswer	 = JBrowser::getInstance();
	$isMobile	 = preg_match('/mobile/', strtolower($broswer->getAgentString()));
	
	if ($isMobile) {
		$doc->addStyleSheet('templates/' . $this->template . '/css/mobile.css');
	}
	// Load optional RTL Bootstrap CSS
	JHtml::_('bootstrap.loadCss', false, $this->direction);
	//$doc->addScript('templates/' . $this->template . '/js/modernizr.custom.js');
	// Add current user information
	$user = JFactory::getUser();
	
	// Logo file or site title param
	if ($this->params->get('logoFile')) {
		$logo = '<img src="' . JUri::root() . $this->params->get('logoFile') . '" alt="' . $sitename . '" />';
		} elseif ($this->params->get('sitetitle')) {
		$logo = '<span class="site-title" title="' . $sitename . '">' . htmlspecialchars($this->params->get('sitetitle')) . '</span>';
		} else {
		$logo = '<span class="site-title" title="' . $sitename . '">' . $sitename . '</span>';
	}
	JHtml::_('bootstrap.tooltip');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language;
	?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
	<head>
		
		<!-- Google Tag Manager -->
		<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
		new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
		j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
		'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
		})(window,document,'script','dataLayer','GTM-P63XRS5');</script>
		<!-- End Google Tag Manager -->
		
		<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
		<meta http-equiv="Pragma" content="no-cache" />
		<meta http-equiv="Expires" content="0" />
		
		<meta name="viewport" content="initial-scale=0.7" />
		<meta property="og:title" content="InsureDIY" />
		<meta property="og:description" content="Compare & get the best insurance deals on InsureDIY.com. Please click on the InsureDIY logo on the left side of this post and both of us will get points when you buy your first Travel or Term Insurance policy. Thanks!" />
		<meta property="og:image" content="http://www.insurediy.com/images/fb_share_img.png" />
		<meta property="og:url" content="http://www.insurediy.com/" />
		
		<jdoc:include type="head" />
		<?php
			// Use of Google Font
			if ($this->params->get('googleFont')) {
			?>
			<link href='//fonts.googleapis.com/css?family=<?php echo $this->params->get('googleFontName'); ?>' rel='stylesheet' type='text/css' />
			<style type="text/css">
				h1,h2,h3,h4,h5,h6,.site-title{
				font-family: '<?php echo str_replace('+', ' ', $this->params->get('googleFontName')); ?>', sans-serif;
				}
			</style>
			<?php
			}
		?>
		<?php
			// Template color
			if ($this->params->get('templateColor')) {
			?>
			<style type="text/css">
				body.site
				{
				border-top: 3px solid <?php echo $this->params->get('templateColor'); ?>;
				background-color: <?php echo $this->params->get('templateBackgroundColor'); ?>
				}
				a
				{
				color: <?php echo $this->params->get('templateColor'); ?>;
				}
				.navbar-inner, .nav-list > .active > a, .nav-list > .active > a:hover, .dropdown-menu li > a:hover, .dropdown-menu .active > a, .dropdown-menu .active > a:hover, .nav-pills > .active > a, .nav-pills > .active > a:hover,
				.btn-primary
				{
				background: <?php echo $this->params->get('templateColor'); ?>;
				}
				.navbar-inner
				{
				-moz-box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
				-webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
				box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
				}
			</style>
			<?php
			}
		?>
		<!--[if lt IE 9]>
			<link href="<?php echo $this->baseurl ?>/templates/protostar/css/ie8.css" type="text/css" rel="stylesheet" />
		<![endif]-->
		<!--[if lt IE 9]>
			<script src="<?php echo $this->baseurl ?>/media/jui/js/html5.js"></script>
		<![endif]-->
		<script src="<?php echo $this->baseurl ?>/templates/protostar/js/checkboxes.js"></script>
		<?php if (FALSE): ?>
		<script src="<?php echo $this->baseurl ?>/templates/protostar/js/CustomInput.js"></script>
		<?php endif; ?>
		<link href='//fonts.googleapis.com/css?family=Montserrat' rel='stylesheet' type='text/css' />
		<script type="text/javascript">
			window.smartlook||(function(d) {
			var o=smartlook=function(){ o.api.push(arguments)},h=d.getElementsByTagName('head')[0];
			var c=d.createElement('script');o.api=new Array();c.async=true;c.type='text/javascript';
			c.charset='utf-8';c.src='https://rec.smartlook.com/recorder.js';h.appendChild(c);
			})(document);
			smartlook('init', '1b1ce3e48f1447ccb8f8a009292d485154b4e8cc');
		</script>
		
		<?php if ($user->id != 0): ?>
			<script>
				smartlook('identify', "<?php echo $user->id; ?>", {
					"name": "<?php echo $user->name; ?>",
					"email": "<?php echo $user->email; ?>"
				});
			</script>
		<?php endif; ?>
		
		<!--Start of Zendesk Chat Script-->
		<script type="text/javascript">
			window.$zopim||(function(d,s){var z=$zopim=function(c){
			z._.push(c)},$=z.s=
			d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
			_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute('charset','utf-8');
			$.src='https://v2.zopim.com/?4hNwM4OAxVKFzbgJz9JLr7rNZTXrLdN3';z.t=+new Date;$.
			type='text/javascript';e.parentNode.insertBefore($,e)})(document,'script');
		</script>
		<!--End of Zendesk Chat Script-->
		
		<!-- Hotjar Tracking Code for https://www.insurediy.com/ -->
		<script>
    (function(h,o,t,j,a,r){
        h.hj=h.hj||function(){(h.hj.q=h.hj.q||[]).push(arguments)};
        h._hjSettings={hjid:856468,hjsv:6};
        a=o.getElementsByTagName('head')[0];
        r=o.createElement('script');r.async=1;
        r.src=t+h._hjSettings.hjid+j+h._hjSettings.hjsv;
        a.appendChild(r);
    })(window,document,'https://static.hotjar.com/c/hotjar-','.js?sv=');
		</script>

		<script type="text/javascript">
    window._mfq = window._mfq || [];
    (function() {
        var mf = document.createElement("script");
        mf.type = "text/javascript"; mf.async = true;
        mf.src = "//cdn.mouseflow.com/projects/e86f6df2-a7d5-4713-8bfe-25b94e2d4058.js";
        document.getElementsByTagName("head")[0].appendChild(mf);
    })();
		</script>

	</head>
	
	<body class="site <?php
		echo $option
		. ' view-' . $view
		. ($layout ? ' layout-' . $layout : ' no-layout')
		. ($task ? ' task-' . $task : ' no-task')
		. ($itemid ? ' itemid-' . $itemid : '')
		. ($params->get('fluidContainer') ? ' fluid' : '');
		?>">
		
		<!-- Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-P63XRS5"
		height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
		
		<div class="insurediy-header-fixed">
			<div id="insurediy-header">
				<div class="container">
					<div class="left">
						<a href="<?php echo $this->baseurl.'/'. $sef ?>"><img src="templates/protostar/images/logo.png" alt="InsureDIY logo" width="300" height="68"/></a>
					</div>
					<div class="right right-wrapper">
						<div class="right size14 relative">
							<?php if ($this->countModules('insurediy-login-signup') && $user->get('guest')) : ?>
							<div><jdoc:include type="modules" name="insurediy-login-signup" style="none" /></div><div></div>
							<div id="insurediy-login-box" style="display:none;"><jdoc:include type="modules" name="insurediy-logged-in" style="none" /></div>
							<?php else : //     ?>
							<div style="font-family:sourcesanspro-regular;">
								<div style="float:left;"><jdoc:include type="modules" name="insurediy-logged-in" style="none" /></div>
								<div style="float:left;"><jdoc:include type="modules" name="insurediy-customer-menu" style="none" /></div>
								<div class="clear"></div>
							</div>
							<?php endif; ?>
						</div>
						<?php if ($this->countModules('insurediy-main-search')): ?>
						<div class="right" style="margin-right:10px;"><jdoc:include type="modules" name="insurediy-main-search" style="none" /></div>
						<?php endif; ?>
						<?php if ($this->countModules('insurediy-language-switcher')): ?>
						<div class="right" style="margin-right:10px;"><jdoc:include type="modules" name="insurediy-language-switcher" style="none" /></div>
						<?php endif; ?>
						<?php if ($this->countModules('insurediy-country-flag')): ?>
						<div class="right" style="margin-right:10px;"><jdoc:include type="modules" name="insurediy-country-flag" style="none" /></div>
						<?php endif; ?>
						<div class="clear"></div>
					</div>
					<div class="clear"></div>
				</div>
			</div>
			
			<div id="insurediy-main-nav">
				<div class="container">
					<jdoc:include type="modules" name="insurediy-main-menu" style="none" />
				</div>
			</div>
			<div id="insurediy-main-nav-bottom">
			</div>
			<div class="clear"></div>
		</div>
		<!-- Body -->
		<div class="body">
			
			<div class="container<?php echo ($params->get('fluidContainer') ? '-fluid' : ''); ?>">
				
				<?php if ($this->countModules('before-content')) : ?>
				<div><jdoc:include type="modules" name="before-content" style="none" /></div>
				<?php endif; ?>
				
				<?php if ($this->countModules('position-1')) : ?>
				<nav class="navigation" role="navigation">
					<jdoc:include type="modules" name="position-1" style="none" />
				</nav>
				<?php endif; ?>
				
				<div class="row-fluid">
					<?php if ($this->countModules('position-8')) : ?>
					<!-- Begin Sidebar -->
					<div id="sidebar" class="span3">
						<div class="sidebar-nav">
							<jdoc:include type="modules" name="position-8" style="xhtml" />
						</div>
					</div>
					<!-- End Sidebar -->
					<?php endif; ?>
					<div id="content" role="main" class="<?php //echo $span;                            ?>">
						<!-- Begin Content -->
						<?php if ($showMessage): ?>
						<jdoc:include type="message" />
						<?php endif; ?>
						<jdoc:include type="component" />
						<!-- End Content -->
						<div class="clear"></div>
					</div>
					<?php /* </main> */ ?>
					
					<?php if ($this->countModules('insurediy-homepage-banner') || $this->countModules('insurediy-homepage-beside-banner')) : ?>
					<div style="margin-bottom:20px;">
						<div style="float:left;width:634px;"><jdoc:include type="modules" name="insurediy-homepage-banner" style="none" /></div>
						<div style="float:right;margin-right:10px;"><jdoc:include type="modules" name="insurediy-homepage-beside-banner" style="none" /></div>
						<div class="clear"></div>
					</div>
					<?php endif; ?>
					
					<?php if ($this->countModules('insurediy-homepage-straight-to') || $this->countModules('insurediy-homepage-policyholder-query')) : ?>
					<div style="margin-bottom:20px;">
						<div style="float:left;width:50%;height:262px;"><jdoc:include type="modules" name="insurediy-homepage-straight-to" style="homepagemod" /></div>
						<div style="float:right;width:49%;height:262px;"><jdoc:include type="modules" name="insurediy-homepage-policyholder-query" style="homepagemod" /></div>
						<div class="clear"></div>
					</div>
					<?php endif; ?>
					
					<?php if ($this->countModules('insurediy-get-a-quote-choices')) : ?>
					<div style="margin-bottom:20px;">
						<div><jdoc:include type="modules" name="insurediy-get-a-quote-choices" style="none" /></div>
						<div class="clear"></div>
					</div>
					<?php endif; ?>
					
					<?php if ($this->countModules('insurediy-homepage-1')) : ?>
					<div style="margin-bottom:20px;">
						<div><jdoc:include type="modules" name="insurediy-homepage-1" style="none" /></div>
						<div class="clear"></div>
					</div>
					<?php endif; ?>
					
					<?php if ($this->countModules('insurediy-partner-logo-slider')) : ?>
					<div><jdoc:include type="modules" name="insurediy-partner-logo-slider" style="partner" /></div>
					<?php endif; ?>
					
				</div>
			</div>
		</div>
		<!-- Footer -->
		<div class="footer" role="contentinfo">
			<div class="container<?php echo ($params->get('fluidContainer') ? '-fluid' : ''); ?>">
				<div style="float:left;"><jdoc:include type="modules" name="insurediy-footer-menu" style="none" /></div>
				<div style="float:right;"><jdoc:include type="modules" name="insurediy-copyright" style="none" /></div>
				<?php /* <p class="pull-right"><a href="#top" id="back-top"><?php echo JText::_('TPL_PROTOSTAR_BACKTOTOP'); ?></a></p> */ ?>
				<div class="clear"></div>
			</div>
		</div>
		<!-- Facebook Pixel Code -->
		<script>
			!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
			n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
			document,'script','//connect.facebook.net/en_US/fbevents.js');
			fbq('init', '1557375841252270');
		fbq('track', "PageView");</script>
		<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1557375841252270&ev=PageView&noscript=1"/></noscript>
		<!-- End Facebook Pixel Code -->
		
		<script>(function(w,d,t,r,u){var f,n,i;w[u]=w[u]||[],f=function(){var o={ti:"5117152"};o.q=w[u],w[u]=new UET(o),w[u].push("pageLoad")},n=d.createElement(t),n.src=r,n.async=1,n.onload=n.onreadystatechange=function(){var s=this.readyState;s&&s!=="loaded"&&s!=="complete"||(f(),n.onload=n.onreadystatechange=null)},i=d.getElementsByTagName(t)[0],i.parentNode.insertBefore(n,i)})(window,document,"script","//bat.bing.com/bat.js","uetq");</script><noscript><img src="//bat.bing.com/action/0?ti=5117152&Ver=2" height="0" width="0" style="display:none; visibility: hidden;" /></noscript>
		
		<script type="text/javascript">

    adroll_adv_id = "ZS6IA4GHIBFFHFRK4UXQTI";
    adroll_pix_id = "ZLNKDJP4TJDNJM2IZXITRJ";

    /* OPTIONAL: provide email to improve user identification */
    /* adroll_email = "username@example.com"; */
    (function () {
        var _onload = function(){
            if (document.readyState && !/loaded|complete/.test(document.readyState)){setTimeout(_onload, 10);return}
            if (!window.__adroll_loaded){__adroll_loaded=true;setTimeout(_onload, 50);return}
            var scr = document.createElement("script");
            var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
            scr.setAttribute('async', 'true');
            scr.type = "text/javascript";
            scr.src = host + "/j/roundtrip.js";
            ((document.getElementsByTagName('head') || [null])[0] ||
                document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
        };
        if (window.addEventListener) {window.addEventListener('load', _onload, false);}
        else {window.attachEvent('onload', _onload)}
    }());

		</script>
		
		<jdoc:include type="modules" name="debug" style="none" />
	</body>
</html>
