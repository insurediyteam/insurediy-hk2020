<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
//JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', '.registration select');
JHtml::_('script', 'system/jquery.validate.js', false, true);
?>
<script type="text/javascript">
			jQuery(document).ready(function () {
				jQuery("#member-registration").validate({
					ignore: [], // <-- option so that hidden elements are validated
				});
				var jForm = jQuery("form#member-registration");
				console.log(jForm);
				var jSelects = jForm.find("select");
				var jInputs = jForm.find("input");
				jQuery('html, body').scrollTop(0);
				jSelects.each(function () {
					var jThis = jQuery(this);
					jThis.on("change", selectOnChange);
				});
				jInputs.each(function () {
					var jThis = jQuery(this);
					jThis.on("change", selectOnChange);
				});
				var jForm = jQuery("form#member-registration");
				jForm.submit(function () {
					var errors = jForm.find("label.error");
					if (errors.length > 0) {
						var label = jForm.find("label.error:first");
						jQuery('html, body').animate({
							'scrollTop': label.offset().top - 350
						}, 300);
					}
				});
			});
			function selectOnChange() {
				var jThis = jQuery(this);
				if (jThis.val()) {
					jThis.siblings("label.error").remove();
				}
			}
			function tnaOnClickNo() {
				$jq("#jform_protection_needs_analysis").val("0");
				cleanInsureDIYPopup('insurediy-popup-box-2');
				return false;
			}
		</script>
<div class="registration<?php echo $this->pageclass_sfx ?>">
	<?php if ($this->params->get('show_page_heading')) : ?>
		<div class="page-header">
			<div class="h1-heading">
				<?php echo $this->escape($this->params->get('page_heading')); ?>
			</div>
		</div>
	<?php else : ?>
		<div class="header-top">
			<div class="h1-heading">
				<i class="icon-signup-c"></i>
				Sign Up
			</div>
		</div>
	<?php endif; ?>
	<div class="padding20">
		<?php if (FALSE): ?>
			<div class="top-module">
			</div>
		<?php endif; ?>

		<form id="member-registration" action="<?php echo JRoute::_('index.php?option=com_users&task=registration.register'); ?>" method="post" class=" form-vertical" enctype="multipart/form-data">
			<fieldset>

				<div class="control-group1">
					<div class="control-label"><?php echo $this->form->getLabel('gender'); ?></div>
					<div class="controls" style="margin-bottom:30px;"><?php echo $this->form->getInput('gender'); ?></div>
					<div class="control-label"><?php echo $this->form->getLabel('dob'); ?></div>
					<div class="controls" style="margin-bottom:30px;"><?php echo $this->form->getInput('dob'); ?></div>
					<div class="control-label"><?php echo $this->form->getLabel('marital_status'); ?></div>
					<div class="controls" style="margin-bottom:30px;"><?php echo $this->form->getInput('marital_status'); ?></div>
				</div>

				<div class="control-group3">
					<div class="control-label">Dependents</div>
					<div class="dependents">
						<div class="control-sub-label"><?php echo $this->form->getLabel('dep_young_children'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('dep_young_children'); ?></div>
						<div class="clear"></div>
						<div class="control-sub-label"><?php echo $this->form->getLabel('dep_kids'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('dep_kids'); ?></div>
						<div class="clear"></div>
						<div class="control-sub-label"><?php echo $this->form->getLabel('dep_teenagers'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('dep_teenagers'); ?></div>
						<div class="clear"></div>
						<div class="control-sub-label"><?php echo $this->form->getLabel('dep_young_adults'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('dep_young_adults'); ?></div>
						<div class="clear"></div>
						<div class="control-sub-label"><?php echo $this->form->getLabel('dep_elderly'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('dep_elderly'); ?></div>
						<div class="clear"></div>
					</div>

					<div class="control-group3a">
						<div class="control-label"><?php echo $this->form->getLabel('monthly_salary'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('monthly_salary'); ?></div>
					</div>

					<div class="control-group3a">
						<div class="control-label"><?php echo $this->form->getLabel('occupation'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('occupation'); ?></div>
					</div>

				</div>

				<div class="clear"></div>
				<div class="control-groups-left">
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('name'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('name'); ?></div>
					</div>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('lastname'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('lastname'); ?></div>
					</div>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('nationality'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('nationality'); ?></div>
					</div>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('email1'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('email1'); ?></div>
					</div>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('password1'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('password1'); ?></div>
					</div>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('password2'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('password2'); ?></div>
					</div>
				</div>
				<div class="control-groups-right">
					<div class="control-group2">
						<div class="control-label"><?php echo $this->form->getLabel('room_no'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('room_no'); ?></div>
					</div>
					<div class="control-group2">
						<div class="control-label"><?php echo $this->form->getLabel('floor_no'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('floor_no'); ?></div>
					</div>
					<div class="control-group2">
						<div class="control-label"><?php echo $this->form->getLabel('block_no'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('block_no'); ?></div>
					</div>
					<div class="clear"></div>
					<div class="control-group1">
						<div class="control-label"><?php echo $this->form->getLabel('building_name'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('building_name'); ?></div>
					</div>
					<div class="control-group1">
						<div class="control-label"><?php echo $this->form->getLabel('street_no'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('street_no'); ?></div>
					</div>
					<div class="clear"></div>
					<div class="control-group1">
						<div class="control-label"><?php echo $this->form->getLabel('district'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('district'); ?></div>
					</div>
					<div class="control-group1">
						<div class="control-label"><?php echo $this->form->getLabel('postal_code'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('postal_code'); ?></div>
					</div>
					<div class="clear"></div>
					<div class="control-group1">
						<div class="control-label"><?php echo $this->form->getLabel('country'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('country'); ?></div>
					</div>
					<div class="clear"></div>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('contact_no'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('country_code'); ?><?php echo $this->form->getInput('contact_no'); ?></div>
					</div>
					<div class="clear"></div>
				</div>
				<?php if (TRUE): ?>
					<div class="clear"></div>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('captcha'); ?></div>
						<div class="controls" style="margin-bottom:30px;"><?php echo $this->form->getInput('captcha'); ?></div>
					</div>
				<?php endif; ?>
			</fieldset>

			<div class="form-actions" style="padding:20px 0; background-color: transparent;">
				<?php echo $this->form->getInput('s_popup'); ?>
				<input type="hidden" name="option" value="com_users" />
				<input type="hidden" name="task" value="registration.register" />
				<?php echo JHtml::_('form.token'); ?>

				<div style="float:left;margin:15px 0;">
					{modulepos insurediy-secured-ssl}
				</div>
				<div class="btn-toolbar" style="float:right">
					<div class="btn-group">
						<button type="submit" class="btn btn-primary validate"><?php echo JText::_('JREGISTER') ?></button>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</form>

	</div>
</div>