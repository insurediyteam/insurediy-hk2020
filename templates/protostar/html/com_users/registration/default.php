<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
//JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', '.registration select');
JHtml::_('script', 'system/jquery.validate.js', false, true);

$session = JFactory::getSession();
?>
<style>
.registration #content {margin-top:26px;}
.registration input {
	width:93%;
	border-radius: 0;
}
#member-registration .control-groups-left {
	float:left;
	width:45%;
	padding-top: 25px;
	padding-right:0;
    margin-right:23px;
	border:0;
}
#member-registration .control-groups-right {
	float:left;
	width: 45%;
    height: auto;
	border-left: 1px solid #ccc;
	padding-top: 25px;
    padding-left: 23px;
    margin-left:0;
}
#member-registration .control-groups-left .control-group,#member-registration .control-groups-right .control-group {
	margin-bottom:10px;
}
</style>
<script type="text/javascript">
			jQuery(document).ready(function () {
				jQuery("#member-registration").validate({
					ignore: [], // <-- option so that hidden elements are validated
				});
				var jForm = jQuery("form#member-registration");
				console.log(jForm);
				var jSelects = jForm.find("select");
				var jInputs = jForm.find("input");
				jQuery('html, body').scrollTop(0);
				jSelects.each(function () {
					var jThis = jQuery(this);
					jThis.on("change", selectOnChange);
				});
				jInputs.each(function () {
					var jThis = jQuery(this);
					jThis.on("change", selectOnChange);
				});
				var jForm = jQuery("form#member-registration");
				jForm.submit(function () {
					var errors = jForm.find("label.error");
					if (errors.length > 0) {
						var label = jForm.find("label.error:first");
						jQuery('html, body').animate({
							'scrollTop': label.offset().top - 350
						}, 300);
					}
				});
			});
			function selectOnChange() {
				var jThis = jQuery(this);
				if (jThis.val()) {
					jThis.siblings("label.error").remove();
				}
			}
			function tnaOnClickNo() {
				$jq("#jform_protection_needs_analysis").val("0");
				cleanInsureDIYPopup('insurediy-popup-box-2');
				return false;
			}
		</script>
<div class="registration montserat-font <?php echo $this->pageclass_sfx ?>">
	<?php /* if ($this->params->get('show_page_heading')) : ?>
		<div class="page-header">
			<div class="h1-heading">
				<?php echo $this->escape($this->params->get('page_heading')); ?>
			</div>
		</div>
	<?php else : ?>
		<div class="header-top">
			<div class="h1-heading">
				<i class="icon-signup-c"></i>
				Sign Up
			</div>
		</div>
	<?php endif; ?>
	<div class="padding20">
		<?php if (FALSE): ?>
			<div class="top-module">
			</div>
		<?php endif; */?>
		<div style="
		font-weight: bold;
   		font-size: 25px;
    	text-align: center;
		">
		<img style="width: 150px;" src="images/registration-bulb.png" alt="bulb with lights" width="150" height="213">
		<br><br>
		<?php echo JText::_('COM_USERS_REGISTRATION_HEADER'); ?><br></div>
		<div style="width:50%; margin:auto;margin-top:30px">
		<form id="member-registration" action="<?php echo JRoute::_('index.php?option=com_users&task=registration.register'); ?>" method="post" class=" form-vertical" enctype="multipart/form-data">
			<fieldset>
<?php /*
				<div class="control-group1">
					<div class="control-label"><?php echo test ?></div>
					<div class="controls" style="margin-bottom:30px;"><?php echo $this->form->getInput('gender'); ?></div>
					<div class="control-label"><?php echo $this->form->getLabel('dob'); ?></div>
					<div class="controls" style="margin-bottom:30px;"><?php echo $this->form->getInput('dob'); ?></div>
					<div class="control-label"><?php echo $this->form->getLabel('marital_status'); ?></div>
					<div class="controls" style="margin-bottom:30px;"><?php echo $this->form->getInput('marital_status'); ?></div>
				</div>

				<div class="control-group3">
					<div class="control-label">Dependents</div>
					<div class="dependents">
						<div class="control-sub-label"><?php echo $this->form->getLabel('dep_young_children'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('dep_young_children'); ?></div>
						<div class="clear"></div>
						<div class="control-sub-label"><?php echo $this->form->getLabel('dep_kids'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('dep_kids'); ?></div>
						<div class="clear"></div>
						<div class="control-sub-label"><?php echo $this->form->getLabel('dep_teenagers'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('dep_teenagers'); ?></div>
						<div class="clear"></div>
						<div class="control-sub-label"><?php echo $this->form->getLabel('dep_young_adults'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('dep_young_adults'); ?></div>
						<div class="clear"></div>
						<div class="control-sub-label"><?php echo $this->form->getLabel('dep_elderly'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('dep_elderly'); ?></div>
						<div class="clear"></div>
					</div>

					<div class="control-group3a">
						<div class="control-label"><?php echo $this->form->getLabel('monthly_salary'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('monthly_salary'); ?></div>
					</div>

					<div class="control-group3a">
						<div class="control-label"><?php echo $this->form->getLabel('occupation'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('occupation'); ?></div>
					</div>

				</div>
*/?>
				<div class="clear"></div>
				<div>
					<div class="control-groups-left">
						<div class="control-group">
							<div class="control-label"><?php echo $this->form->getLabel('name'); ?></div>
							<div class="controls"><?php echo $this->form->getInput('name'); ?></div>
						</div>
						<div class="control-group">
							<div class="control-label"><?php echo $this->form->getLabel('lastname'); ?></div>
							<div class="controls"><?php echo $this->form->getInput('lastname'); ?></div>
						</div>
	<?php /*			<div class="control-group">
							<div class="control-label"><?php echo $this->form->getLabel('nationality'); ?></div>
							<div class="controls"><?php echo $this->form->getInput('nationality'); ?></div>
						</div> */?>
						<div class="control-group">
							<div class="control-label"><?php echo $this->form->getLabel('email1'); ?></div>
							<div class="controls"><?php echo $this->form->getInput('email1'); ?></div>
						</div>
					</div>
					<div class="control-groups-right">
						<div class="control-group">
							<div class="control-label"><?php echo $this->form->getLabel('password1'); ?></div>
							<div class="controls"><?php echo $this->form->getInput('password1'); ?></div>
						</div>
						<div class="control-group">
							<div class="control-label"><?php echo $this->form->getLabel('password2'); ?></div>
							<div class="controls"><?php echo $this->form->getInput('password2'); ?></div>
						</div>
						<?php 
							$referred_by = $session->get(SESSION_KEY_REFID, FALSE); 
							
							if($referred_by){
								$this->form->setValue('referred_by', "", $referred_by);
							}
						?>
						<div class="control-group">
							<div class="control-label"><?php echo $this->form->getLabel('referred_by'); ?></div>
							<div class="controls"><?php echo $this->form->getInput('referred_by'); ?></div>
						</div>
				<?php /*		<div class="control-group">
							<div class="control-label"><?php echo $this->form->getLabel('referred_by'); ?></div>
							<div class="controls"><?php echo $this->form->getInput('referred_by'); ?></div>
						</div> 
						<div class="control-group1">
							<div class="control-label"><?php echo $this->form->getLabel('block_no'); ?></div>
							<div class="controls"><?php echo $this->form->getInput('block_no'); ?></div>
						</div>
						<div class="control-group1">
							<div class="control-label"><?php echo $this->form->getLabel('street_no'); ?></div>
							<div class="controls"><?php echo $this->form->getInput('street_no'); ?></div>
						</div>
						<div class="clear"></div>
						<div class="control-group1">
							<div class="control-label"><?php echo $this->form->getLabel('unit'); ?></div>
							<div class="controls"><?php echo $this->form->getInput('unit'); ?></div>
						</div>
						<div class="control-group1">
							<div class="control-label"><?php echo $this->form->getLabel('building_name'); ?></div>
							<div class="controls"><?php echo $this->form->getInput('building_name'); ?></div>
						</div>					
						<div class="clear"></div>
						<div class="control-group1">
							<div class="control-label"><?php echo $this->form->getLabel('postal_code'); ?></div>
							<div class="controls"><?php echo $this->form->getInput('postal_code'); ?></div>
						</div>
						<div class="clear"></div>
						<div class="control-group1">
							<div class="control-label"><?php echo $this->form->getLabel('country'); ?></div>
							<div class="controls"><?php echo $this->form->getInput('country'); ?></div>
						</div>
						<div class="clear"></div>
						<div class="control-group">
							<div class="control-label"><?php echo $this->form->getLabel('contact_no'); ?></div>
							<div class="controls"><?php echo $this->form->getInput('country_code'); ?><?php echo $this->form->getInput('contact_no'); ?></div>
						</div>
						<div class="clear"></div>*/?> 
					</div>
				</div>
				<div class="clear"></div>
				<div class="control-group" style="">
					<div class="controls" style=""><?php echo $this->form->getInput('captcha'); ?></div>
				</div>
				<div style="text-align:center;    margin-top: 20px;">
					<span style="
					font-size: 10px;
    				white-space: nowrap;
    				color:#acacac;"
    				>Click here to see our 
    				<a style="font-weight: 600;"href="https://www.insurediy.com.sg/en/privacy-policy.html" target="_blank">Privacy Policy</a>
    				 and 
    				 <a style="font-weight: 600;" href="https://www.insurediy.com.sg/en/website-terms-of-use-disclaimers.html" target="_blank">Website Terms</a>
    				 </span>
					<div class="form-actions" style="background-color: unset;padding:0;margin:15px 0 0;border:none;">
						<?php echo $this->form->getInput('s_popup'); ?>
						<input type="hidden" name="option" value="com_users" />
						<input type="hidden" name="task" value="registration.register" />
						<?php echo JHtml::_('form.token'); ?>
						<div class="btn-toolbar" style="">
							<div class="btn-group">
								<button type="submit" class="motor-btn validate" style="padding: 10px 20px 10px 20px;text-transform: uppercase;"><?php echo JText::_('JREGISTER') ?></button>
							</div>
						</div>
						<div class="clear"></div>
					</div>
				</div>				
			</fieldset>

			
			
		</form></div>
		
		<div style="float:left;margin:15px 15px 0">
					{modulepos insurediy-secured-ssl}
		</div>
		<div class="clear"><br></div>
		<div style="text-align:center;width:100%;background-color:#f8f8f8;padding-top:15px;">
			<span style="font-size:15px;font-weight:bold;"><?php echo JText::_('INSUREDIY_AWARDS') ?></span>
		</div>
		<div id="awards">
			<img src="<?php echo JText::_('CORPHUB_LOGO') ?>" alt="Corphub logo "width="190" height="100">
			<img src="<?php echo JText::_('WPH_LOGO') ?>" alt="Hong Kong Work Happiness
 logo"width="190" height="100">
		</div>
	</div>
</div>