<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', '.registration select');

//load user_profile plugin language
$lang = JFactory::getLanguage();
$lang->load('plg_user_profile', JPATH_ADMINISTRATOR);
?>
<script type="text/javascript">
	Joomla.submitbutton = function (task)
	{
		if (document.formvalidator.isValid(document.getElementById('member-registration')))
		{
			Joomla.submitform(task);
		}
	}

	window.addEvent("domready", function () {
//		var styled_radios = new CustomInput(".insurediy-custom-radio input[type='radio']")
	});
</script>	

<?php /*
  <div class="profile-edit<?php echo $this->pageclass_sfx?>">
  <?php if ($this->params->get('show_page_heading')) : ?>
  <div class="page-header">
  <h1><?php echo $this->escape($this->params->get('page_heading')); ?></h1>
  </div>
  <?php endif; ?>

  <script type="text/javascript">
  Joomla.twoFactorMethodChange = function(e)
  {
  var selectedPane = 'com_users_twofactor_' + jQuery('#jform_twofactor_method').val();

  jQuery.each(jQuery('#com_users_twofactor_forms_container>div'), function(i, el) {
  if (el.id != selectedPane)
  {
  jQuery('#' + el.id).hide(0);
  }
  else
  {
  jQuery('#' + el.id).show(0);
  }
  });
  }
  </script>

  <form id="member-profile" action="<?php echo JRoute::_('index.php?option=com_users&task=profile.save'); ?>" method="post" class="form-validate form-horizontal" enctype="multipart/form-data">
  <?php foreach ($this->form->getFieldsets() as $group => $fieldset):// Iterate through the form fieldsets and display each one.?>
  <?php $fields = $this->form->getFieldset($group);?>
  <?php if (count($fields)):?>
  <fieldset>
  <?php if (isset($fieldset->label)):// If the fieldset has a label set, display it as the legend.?>
  <legend><?php echo JText::_($fieldset->label); ?></legend>
  <?php endif;?>
  <?php foreach ($fields as $field):// Iterate through the fields in the set and display them.?>
  <?php if ($field->hidden):// If the field is hidden, just display the input.?>
  <div class="control-group">
  <div class="controls">
  <?php echo $field->input;?>
  </div>
  </div>
  <?php else:?>
  <div class="control-group">
  <div class="control-label">
  <?php echo $field->label; ?>
  <?php if (!$field->required && $field->type != 'Spacer') : ?>
  <span class="optional"><?php echo JText::_('COM_USERS_OPTIONAL'); ?></span>
  <?php endif; ?>
  </div>
  <div class="controls">
  <?php echo $field->input; ?>
  </div>
  </div>
  <?php endif;?>
  <?php endforeach;?>
  </fieldset>
  <?php endif;?>
  <?php endforeach;?>

  <?php if (count($this->twofactormethods) > 1): ?>
  <fieldset>
  <legend><?php echo JText::_('COM_USERS_PROFILE_TWO_FACTOR_AUTH') ?></legend>

  <div class="control-group">
  <div class="control-label">
  <label id="jform_twofactor_method-lbl" for="jform_twofactor_method" class="hasTooltip"
  title="<strong><?php echo JText::_('COM_USERS_PROFILE_TWOFACTOR_LABEL') ?></strong><br/><?php echo JText::_('COM_USERS_PROFILE_TWOFACTOR_DESC') ?>">
  <?php echo JText::_('COM_USERS_PROFILE_TWOFACTOR_LABEL'); ?>
  </label>
  </div>
  <div class="controls">
  <?php echo JHtml::_('select.genericlist', $this->twofactormethods, 'jform[twofactor][method]', array('onchange' => 'Joomla.twoFactorMethodChange()'), 'value', 'text', $this->otpConfig->method, 'jform_twofactor_method', false) ?>
  </div>
  </div>
  <div id="com_users_twofactor_forms_container">
  <?php foreach($this->twofactorform as $form): ?>
  <?php $style = $form['method'] == $this->otpConfig->method ? 'display: block' : 'display: none'; ?>
  <div id="com_users_twofactor_<?php echo $form['method'] ?>" style="<?php echo $style; ?>">
  <?php echo $form['form'] ?>
  </div>
  <?php endforeach; ?>
  </div>
  </fieldset>

  <fieldset>
  <legend>
  <?php echo JText::_('COM_USERS_PROFILE_OTEPS') ?>
  </legend>
  <div class="alert alert-info">
  <?php echo JText::_('COM_USERS_PROFILE_OTEPS_DESC') ?>
  </div>
  <?php if (empty($this->otpConfig->otep)): ?>
  <div class="alert alert-warning">
  <?php echo JText::_('COM_USERS_PROFILE_OTEPS_WAIT_DESC') ?>
  </div>
  <?php else: ?>
  <?php foreach ($this->otpConfig->otep as $otep): ?>
  <span class="span3">
  <?php echo substr($otep, 0, 4) ?>-<?php echo substr($otep, 4, 4) ?>-<?php echo substr($otep, 8, 4) ?>-<?php echo substr($otep, 12, 4) ?>
  </span>
  <?php endforeach; ?>
  <div class="clearfix"></div>
  <?php endif; ?>
  </fieldset>
  <?php endif; ?>

  <div class="form-actions">
  <button type="submit" class="btn btn-primary validate"><span><?php echo JText::_('JSUBMIT'); ?></span></button>
  <a class="btn" href="<?php echo JRoute::_(''); ?>" title="<?php echo JText::_('JCANCEL'); ?>"><?php echo JText::_('JCANCEL'); ?></a>

  <input type="hidden" name="option" value="com_users" />
  <input type="hidden" name="task" value="profile.save" />
  <?php echo JHtml::_('form.token'); ?>
  </div>
  </form>
  </div>
 */ ?>

<div class="registration<?php echo $this->pageclass_sfx ?>">
	<?php if ($this->params->get('show_page_heading')) : ?>
		<div class="page-header">
			<div class="h1-heading"><?php echo $this->escape($this->params->get('page_heading')); ?></div>
		</div>
	<?php else : ?>
		<div class="header-top">
			<div class="h1-heading">Edit Profile</div>
		</div>
	<?php endif; ?>
	<div class="padding20">
		<?php if (FALSE): ?>
			<div class="top-module">
			</div>
		<?php endif; ?>

		<form id="member-registration" name="adminForm" action="<?php echo JRoute::_('index.php?option=com_users&task=profile.save'); ?>" method="post" class="form-validate form-vertical" enctype="multipart/form-data">
			<div class="clear"></div>
			<fieldset>
				<div class="control-group1">
					<div class="control-label"><?php echo $this->form->getLabel('gender'); ?></div>
					<div class="controls" style="margin-bottom:30px;"><?php echo $this->form->getInput('gender'); ?></div>
					<div class="control-label"><?php echo $this->form->getLabel('dob'); ?></div>
					<div class="controls" style="margin-bottom:30px;"><?php echo $this->form->getInput('dob'); ?></div>
					<div class="control-label"><?php echo $this->form->getLabel('marital_status'); ?></div>
					<div class="controls" style="margin-bottom:30px;"><?php echo $this->form->getInput('marital_status'); ?></div>
				</div>
				<div class="control-group3">
					<div class="control-label">Dependents</div>
					<div class="dependents">
						<div class="control-sub-label"><?php echo $this->form->getLabel('dep_young_children'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('dep_young_children'); ?></div>
						<div class="clear"></div>
						<div class="control-sub-label"><?php echo $this->form->getLabel('dep_kids'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('dep_kids'); ?></div>
						<div class="clear"></div>
						<div class="control-sub-label"><?php echo $this->form->getLabel('dep_teenagers'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('dep_teenagers'); ?></div>
						<div class="clear"></div>
						<div class="control-sub-label"><?php echo $this->form->getLabel('dep_young_adults'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('dep_young_adults'); ?></div>
						<div class="clear"></div>
						<div class="control-sub-label"><?php echo $this->form->getLabel('dep_elderly'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('dep_elderly'); ?></div>
						<div class="clear"></div>
					</div>
					<div class="control-group3a">
						<div class="control-label"><?php echo $this->form->getLabel('monthly_salary'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('monthly_salary'); ?></div>
					</div>
					<div class="control-group3a">
						<div class="control-label"><?php echo $this->form->getLabel('occupation'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('occupation'); ?></div>
					</div>
				</div>
				<div class="clear"></div>
				<div class="control-groups-left">
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('name'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('name'); ?></div>
					</div>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('lastname'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('lastname'); ?></div>
					</div>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('nationality'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('nationality'); ?></div>
					</div>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('email1'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('email1'); ?></div>
					</div>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('password1'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('password1'); ?></div>
					</div>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('password2'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('password2'); ?></div>
					</div>
				</div>
				<div class="control-groups-right">
					<div class="control-group2">
						<div class="control-label"><?php echo $this->form->getLabel('room_no'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('room_no'); ?></div>
					</div>
					<div class="control-group2">
						<div class="control-label"><?php echo $this->form->getLabel('floor_no'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('floor_no'); ?></div>
					</div>
					<div class="control-group2">
						<div class="control-label"><?php echo $this->form->getLabel('block_no'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('block_no'); ?></div>
					</div>
					<div class="clear"></div>
					<div class="control-group1">
						<div class="control-label"><?php echo $this->form->getLabel('building_name'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('building_name'); ?></div>
					</div>
					<div class="control-group1">
						<div class="control-label"><?php echo $this->form->getLabel('street_no'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('street_no'); ?></div>
					</div>
					<div class="clear"></div>
					<div class="control-group1">
						<div class="control-label"><?php echo $this->form->getLabel('district'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('district'); ?></div>
					</div>

					<div class="control-group1">
						<div class="control-label"><?php echo $this->form->getLabel('postal_code'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('postal_code'); ?></div>
					</div>
					<div class="clear"></div>
					<div class="control-group1">
						<div class="control-label"><?php echo $this->form->getLabel('country'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('country'); ?></div>
					</div>
					<div class="clear"></div>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('contact_no'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('country_code'); ?><?php echo $this->form->getInput('contact_no'); ?></div>
					</div>
					<div class="clear"></div>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('s_popup'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('s_popup'); ?></div>
					</div>
					<div class="clear"></div>
				</div>
				<div class="clear"></div>
			</fieldset>

			<div class="form-actions" style="padding:20px 0;">
				<?php echo $this->form->getInput('id'); ?>
				<input type="hidden" name="option" value="com_users" />
				<input type="hidden" name="task" value="profile.save" />
				<?php echo JHtml::_('form.token'); ?>

				<div style="float:left;margin:15px 0;">
					{modulepos insurediy-secured-ssl}
				</div>
				<div class="btn-toolbar" style="float:right">
					<div class="btn-group">
						<button type="submit" class="btn btn-primary validate"><?php echo JText::_('JSAVE') ?></button>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</form>

	</div>
</div>
