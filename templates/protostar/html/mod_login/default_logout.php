<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_login
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
$app = JFactory::getApplication();
$menu = $app->getMenu("site");
$items = $menu->getItems("menutype", "my-dashboard");
?>
<script>
	function toggleMenu(menu_id) {
		var jmenu = jQuery(menu_id);
		if (jmenu.css("display") === "none") {
			jmenu.show("fast");
		}
	}

	window.addEvent('load', function () {

		var jmenu = jQuery("#usermenu");
		jQuery('#usermenu').on('click', function (e) {
			e.stopPropagation();
		});

		jQuery(document).on("click", function () {
			if (jmenu.css("display") !== "none") {
				jmenu.hide("fast");
			}
		});

	})
</script>
<form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure')); ?>" method="post" id="login-form" class="form-vertical">
	<?php if ($params->get('greeting')) : ?>
		<div class="login-greeting" style="float:left;">
			<span class="login-name">
				<?php
				if (strlen($user->get('name')) > 0) : {
						echo JText::sprintf('MOD_LOGIN_HINAME', htmlspecialchars($user->get('name')));
					} else : {
						echo JText::sprintf('MOD_LOGIN_HINAME', htmlspecialchars($user->get('username')));
					} endif;
				?>
			</span>
			<a id="usermenu-arrow" href="javascript:toggleMenu('#usermenu')" class="usermenu-arrow">&nbsp;</a>
		</div>
	<?php endif; ?>
	<div id="usermenu" class="logout-button" style="display: none;">
		<ul class='unstyled'>
			<?php
			foreach ($items as $k => $item):
				$class = ($k + 1) % 2 == 0 ? "even" : "odd";
				?>
				<?php if ($item->type == "alias"): ?>
					<li class="<?php echo $class ?>"><a href='<?php echo JRoute::_("index.php?Itemid=" . $item->params->get("aliasoptions")); ?>'><?php echo $item->title; ?></a></li>
				<?php else: ?>
					<li class="<?php echo $class ?>"><a href='<?php echo JRoute::_("index.php?Itemid=" . $item->id); ?>'><?php echo $item->title; ?></a></li>
				<?php endif; ?>

			<?php endforeach; ?>

			<?php if (FALSE): ?>
				<li><a href='<?php echo JRoute::_("index.php?Itemid=134"); ?>'>Dashboard</a></li>
				<li class='even'><a href='<?php echo JRoute::_("index.php?Itemid=135"); ?>'>Profile</a></li>
				<li><a href='<?php echo JRoute::_("index.php?Itemid=136"); ?>'>Portfolio</a></li>
				<li class='even'><a href='<?php echo JRoute::_("index.php?Itemid=137"); ?>'>Pending Quotes</a></li>
				<li class='even'><a href='<?php echo JRoute::_("index.php?Itemid=137"); ?>'>Pending Quotes</a></li>
			<?php endif; ?>

			<li class='even'><a href="javascript:jQuery('#login-form').submit();">Logout</a></li>

			<?php if (FALSE): ?>
				<input type="submit" name="Submit" style="padding:2px 20px;font-size:12px !important;" value="<?php echo JText::_('JLOGOUT'); ?>" />
			<?php endif; ?>

		</ul>
		<input type="hidden" name="option" value="com_users" />
		<input type="hidden" name="task" value="user.logout" />
		<input type="hidden" name="return" value="<?php echo $return; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
	<div class="clear"></div>
</form>
