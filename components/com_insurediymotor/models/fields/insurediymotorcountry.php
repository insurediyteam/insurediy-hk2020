<?php
	
	defined('JPATH_PLATFORM') or die;
	
	class JFormFieldInsureDIYMotorCountry extends JFormField {
		
		protected $type = 'InsureDIYMotorCountry';
		
		protected function getGroups() {
			$groups = array();
			$label = 0;
			
			$data = array();
			$db = JFactory::getDbo();
			
			$query = $db->getQuery(TRUE);
			
			$query->select(" * ");
			$query->from("#__insure_motor_countries");
			$db->setQuery($query);
			$results = $db->loadAssocList();
			
			$group_name = '';
			foreach ($results as $k => $result) {
				if($result['variable'] == 'GROUP') { $group_name = $result['value'];continue;}
				$data[$group_name][$k]['value'] = $result['variable'];
				$data[$group_name][$k]['text'] = $result['value'];
			}
			
			if (count($data) > 0) {
				$groups = array_merge($groups, $data);
			}
			
			reset($groups);
			return $groups;
		}
		
		protected function getInput() {
			$html = array();
			$attr = '';
			
			// Initialize some field attributes.
			$attr .= $this->element['class'] ? ' class="' . (string) $this->element['class'] . '"' : '';
			$attr .= ((string) $this->element['disabled'] == 'true') ? ' disabled="disabled"' : '';
			$attr .= $this->element['size'] ? ' size="' . (int) $this->element['size'] . '"' : '';
			$attr .= $this->multiple ? ' multiple="multiple"' : '';
			$attr .= $this->required ? ' required="required" aria-required="true"' : '';
			
			// Initialize JavaScript field attributes.
			$attr .= $this->element['onchange'] ? ' onchange="' . (string) $this->element['onchange'] . '"' : '';
			
			// Get the field groups.
			$groups = (array) $this->getGroups();
			
			// Create a read-only list (no name) with a hidden input to store the value.
			if ((string) $this->element['readonly'] == 'true') {
				$html[] = JHtml::_(
				'select.groupedlist', $groups, null, array(
				'list.attr' => $attr, 'id' => $this->id, 'list.select' => $this->value, 'group.items' => null, 'option.key.toHtml' => false,
				'option.text.toHtml' => false
				)
				);
				$html[] = '<input type="hidden" name="' . $this->name . '" value="' . $this->value . '"/>';
			}
			// Create a regular list.
			else {
				$html[] = JHtml::_(
				'select.groupedlist', $groups, $this->name, array(
			'list.attr' => $attr, 'id' => $this->id, 'list.select' => $this->value, 'group.items' => null, 'option.key.toHtml' => false,
			'option.text.toHtml' => false
			)
			);
			}
			
			return implode($html);
			}
			
			}
						