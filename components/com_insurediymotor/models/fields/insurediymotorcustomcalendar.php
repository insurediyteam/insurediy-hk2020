<?php

defined('JPATH_PLATFORM') or die;
JFormHelper::loadFieldClass('calendar');

class JFormFieldInsureDIYMotorCustomCalendar extends JFormFieldCalendar {

	protected $type = 'CustomCalendar';

	protected function getInput() {
		// Translate placeholder text
		$hint = $this->translateHint ? JText::_($this->hint) : $this->hint;
		// Initialize some field attributes.
		$format = $this->format;

		// Build the attributes array.
		$attributes = array();

		empty($this->size) ? null : $attributes['size'] = $this->size;
		empty($this->maxlength) ? null : $attributes['maxlength'] = $this->maxlength;
		empty($this->class) ? null : $attributes['class'] = $this->class;
		!$this->readonly ? null : $attributes['readonly'] = '';
		!$this->disabled ? null : $attributes['disabled'] = '';
		empty($this->onchange) ? null : $attributes['onchange'] = $this->onchange;
		empty($hint) ? null : $attributes['placeholder'] = $hint;
		$this->autocomplete ? null : $attributes['autocomplete'] = 'off';
		!$this->autofocus ? null : $attributes['autofocus'] = '';

		if ($this->required) {
			$attributes['required'] = '';
			$attributes['aria-required'] = 'true';
		}

		if (is_array($attributes)) {
			$attribs = JArrayHelper::toString($attributes);
		}

		// Handle the special case for "now".
		if (!is_array($this->value)) {
			if(strtoupper($this->value) == 'NOW')
			$this->value = strftime($format);
		}

		// Get some system objects.
		$config = JFactory::getConfig();
		$user = JFactory::getUser();

		// If a known filter is given use it.
		switch (strtoupper($this->filter)) {
			case 'SERVER_UTC':
				// Convert a date to UTC based on the server timezone.
				if (!is_array($this->value)) {
					if((int)$this->value) {
						// Get a date object based on the correct timezone.
						$date = JFactory::getDate($this->value, 'UTC');
						$date->setTimezone(new DateTimeZone($config->get('offset')));
	
						// Transform the date string.
						$this->value = $date->format('d-M-Y', true, false);
					}
				}

				break;

			case 'USER_UTC':
				// Convert a date to UTC based on the user timezone.
				if (!is_array($this->value)) {
					if((int)$this->value) {
						// Get a date object based on the correct timezone.
						$date = JFactory::getDate($this->value, 'UTC');
	
						$date->setTimezone(new DateTimeZone($user->getParam('timezone', $config->get('offset'))));
	
						// Transform the date string.
						$this->value = $date->format('d-M-Y', true, false);
					}
				}

				break;
		}
		
		date_default_timezone_set('UTC');
		
		// Including fallback code for HTML5 non supported browsers.
		JHtml::_('jquery.framework');
		JHtml::_('script', 'system/html5fallback.js', false, true);
		JHtml::_('script', 'system/jquery.ui.core.js', false, true);
		JHtml::_('script', 'system/jquery.ui.datepicker.js', false, true);
		JHtml::_('stylesheet', 'system/jquery-ui.css', false, true);

		$year = date("Y", time());
		$maxYr = (isset($this->element['maxYear'])) ? $year + (int) $this->element['maxYear'] : '';
		$minYr = (isset($this->element['minYear'])) ? $year + (int) $this->element['minYear'] : '';
		$defaultDate = "01-01-1970";

		$today_d = date("d");
		$today_m = date("m");

		if ($this->element['after4pm'] == "1")  {
			$sg_time = date("G");
			if($sg_time >= 8 && $sg_time < 24) { // GMT time hours
				$today_d += 1;
			}
		}

		if (isset($this->element['defaultDate'])) {
			$defaultDate = (string) $this->element['defaultDate'];
		}
		
		$document = JFactory::getDocument();
		if ($defaultDate == "+ 365 day") {
			$defaultDate = JHtml::date(strtotime(date("Y-M-d", mktime()) . " + 365 day"), "d-M-Y");
		} elseif ($defaultDate == "today") {
			$defaultDate = JHtml::date(JFactory::getDate('now'), "d-M-Y");
		} elseif ($defaultDate == "mindate") {
			$defaultDate = $minDate;
		} else {
			$minDate = $today_d . "-" . $today_m . "-" . $minYr;
			if(!is_array($this->value)) {
				if((int)$this->value)
				$defaultDate = (strlen($this->value) > 0) && (strtotime($this->value) > strtotime($minDate)) ? $this->value : $defaultDate;
			}
		}
		
		// min Date
		if (isset($this->element['minDate'])) {
			$minDate = (string) $this->element['minDate'];
			if($minDate == 'mindate1') { 				$minDate = ', minDate: "-80Y +1D" ';
			} else if($minDate == 'mindate2'){ 	$minDate = ', minDate: 0 '; // today
			} else { $minDate = ', minDate: new Date('.$minDate.')';}
		} else {
			$minDate = $minYr . "," . ($today_m - 1) . "," . $today_d;
			$minDate = ', minDate: new Date('.$minDate.')';
		}
		
		// max Date
		$maxDate = '';
		if (isset($this->element['maxDate'])) {
			$maxDate = (string) $this->element['maxDate'];
			if($maxDate == 'maxdate1') {
				$maxDate = ', maxDate: "-6M" ';
			} else if($maxDate == 'maxdate2'){ $maxDate = ', maxDate: "-18Y" '; // 18 years - 1 day 
			} else if($maxDate == 'maxdate3'){ $maxDate = ', maxDate: "+6M" '; // 6 Month
			} else if($maxDate == 'maxdate4'){ $maxDate = ', maxDate: "+182D" '; // + 182 days

			} else {  $maxDate = ', maxDate: new Date('.$maxDate.')';}	
		}
		
		$yearRange = '';
		if(!empty($minYr) && !empty($maxYr)) {
			$yearRange = ', yearRange: "' . $minYr . ':' . $maxYr . '"';
		}
		
		$btnUrl = MyUri::getUrls("site") . "media/system/images/ico-calendar.png";
		
		if($this->name == "jform[policyStartDate]") {
			$script = 'jQuery(function() {
				jQuery( "#' . $this->id . '" ).datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "focus",
					minDate: 0,
					dateFormat : "dd-M-yy",
					yearRange: "1920:2050",
					onChangeMonthYear:function(y, m, i){                     
						var d = i.selectedDay;
						jQuery(this).datepicker("setDate", new Date(y, m-1, d));
					}
				}).datepicker("setDate", new Date());
			});';
		}
		elseif($this->name == "jform[coeExpireDate]") {
			$script = 'jQuery(function() {
				jQuery( "#' . $this->id . '" ).datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "focus",
					minDate: 0,
					dateFormat : "dd-M-yy",
					yearRange: "2018:2050",
					onChangeMonthYear:function(y, m, i){                     
						var d = i.selectedDay;
						jQuery(this).datepicker("setDate", new Date(y, m-1, d));
					}
				}).datepicker("setDate", new Date());
			});';
		}
		elseif($this->name == "jform[drivingTest]") {
			$script = 'jQuery(function() {
				jQuery( "#' . $this->id . '" ).datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "focus",
					minDate: 0,
					dateFormat : "dd-M-yy",
					yearRange: "2018:2050",
					onChangeMonthYear:function(y, m, i){                     
						var d = i.selectedDay;
						jQuery(this).datepicker("setDate", new Date(y, m-1, d));
					}
				}).datepicker("setDate", new Date());
			});';
		}
		elseif($this->name == "jform[prevPolicyEnd]") {
			$script = 'jQuery(function() {
				jQuery( "#' . $this->id . '" ).datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "focus",
					minDate: 0,
					dateFormat : "dd-M-yy",
					yearRange: "2019:2050",
					onChangeMonthYear:function(y, m, i){                     
						var d = i.selectedDay;
						jQuery(this).datepicker("setDate", new Date(y, m-1, d));
					}
				}).datepicker("setDate", null);
			});';
		}
		else {
			$script = 'jQuery(function() {
				jQuery( "#' . $this->id . '" ).datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "focus",
					dateFormat : "dd-M-yy",
					yearRange: "1920:2050",
					defaultDate: "01-JAN-1980",
					onChangeMonthYear:function(y, m, i){                     
						var d = i.selectedDay;
						jQuery(this).datepicker("setDate", new Date(y, m-1, d));
					}
				});
			});';
		}
		
		if($this->name == "jform[dateOfBirth]") {
			$this->name = "jform[dateOfBirth][0]";
		} elseif ($this->name == "jform[drivingTest]") {
			$this->name = "jform[drivingTest][0]";
		}

		$document->addScriptDeclaration($script);

		return '<input type="text" title="" class="input-text calendar-input" readonly name="' . $this->name . '" id="' . $this->id . '" value="' . htmlspecialchars($defaultDate, ENT_COMPAT, 'UTF-8') . '" ' . $attribs . ' />';
	}

}