<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_banners
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('JPATH_BASE') or die;

/**
 * Impressions Field class for the Joomla Framework.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_banners
 * @since       1.6
 */
JLoader::import('incs.form.fields.customlabel', JPATH_ROOT);

class JFormFieldInsureDIYGender extends JformFieldCustomLabel {

	protected $type = 'InsureDIYGender';

	protected function getInput() {

		$value = empty($this->value) ? $this->default : $this->value;
		$checked = ' checked="checked" ';

		return '<div class="insurediy-custom-radio">'
				. '<div class="insurediy-custom-radio-gender-male"><input type="radio" id="' . $this->id . '-insurediy-gender-male" ' . (($value == 'M') ? $checked : '') . ' value="M" name="' . $this->name . '" /><label for="' . $this->id . '-insurediy-gender-male">&nbsp;</label></div>'
				. '<div class="insurediy-custom-radio-gender-female"><input type="radio" id="' . $this->id . '-insurediy-gender-female" ' . (($value == 'F') ? $checked : '') . ' value="F" name="' . $this->name . '" /><label for="' . $this->id . '-insurediy-gender-female">&nbsp;</label></div>'
				. '<div style="clear:both"></div>'
				. '</div>';
	}

}
