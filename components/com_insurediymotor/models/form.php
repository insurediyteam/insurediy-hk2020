<?php

defined('_JEXEC') or die;

JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . '/tables');

class InsureDIYMotorModelForm extends JModelForm {

	protected $_context = 'com_insurediymotor.quotation';
	protected $_motor_make = "#__insure_motor_make_master";
	protected $_motor_model = "#__insure_motor_model_master";
	protected $_motor_make_model_sompo = "#__insure_motor_make_model_sompo";
	protected $_motor_make_model_msig = "#__insure_motor_make_model_msig";
	protected $_motor_make_model_hla = "#__insure_motor_make_model_hla";
	protected $_motor_make_model_aig = "#__insure_motor_make_model_aig";
	protected $_motor_make_model_pingan = "#__insure_motor_make_model_pingan";
	protected $_motor_make_model_aw = "#__insure_motor_make_model_aw";
	protected $_motor_driver_info = "#__insure_motor_driver_info";
	protected $_motor_quote = "#__insure_motor_quote_master";
	protected $_motor_quote_sompo = "#__insure_motor_quote_sompo";
	protected $_motor_quote_msig = "#__insure_motor_quote_msig";
	protected $_motor_quote_hla = "#__insure_motor_quote_hla";
	protected $_motor_quote_aig = "#__insure_motor_quote_aig";
	protected $_motor_quote_pingan = "#__insure_motor_quote_pingan";
	protected $_motor_quote_aw = "#__insure_motor_quote_aw";
	protected $_motor_quote_renewal = "#__insure_motor_quote_renewal";

	public function getReturnPage() {
		return base64_encode($this->getState('return_page'));
	}

	protected function populateState() {
		$app = JFactory::getApplication();
		$params = $app->getParams();
		$this->setState('params', $params);
	}

	public function getItem($id = null) {
		// nothing to do
	}

	public function getTable($type = 'Quotation', $prefix = 'InsureDIYMotorTable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}

	public function getForm($data = array(), $loadData = TRUE) {
		// Get the form.
		$form = $this->loadForm('com_insurediymotor.form', 'form', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}

		return $form;
	}

	protected function loadFormData() {
		$data = $this->getData();
		$this->preprocessData('com_insurediymotor.form', $data);

		return $data;
	}

	protected function getData() {
		$user = JFactory::getUser();
		$session = JFactory::getSession();
		$data = new stdClass();
		$inputQid = InsureDIYMotorHelper::getInputQid();
		$quotation = array();

		if ($user->get('guest')) {
			if ($inputQid) {
				$this->setError(JText::_("ERR_NO_ACCESS_TO_VIEW"));
				return FALSE;
			}
		} else {
			if ($inputQid) {
				$quotation = InsureDIYMotorHelper::getQuotation($inputQid);
				if (!InsureDIYMotorHelper::checkQuotation($quotation)) {
					InsureDIYMotorHelper::resetQuotation($quotation);
					$quotation = InsureDIYMotorHelper::getQuotation($quotation['id']);
				}
				if (empty($quotation) || $quotation['user_id'] != $user->id) {
					$this->setError(JText::_("ERR_NO_ACCESS_TO_VIEW"));
					return FALSE;
				}
				$session->set("motor.quotation_id", $inputQid);
				$session->set("motor.data", $quotation);
			} else {
				$quotation = $session->get("motor.data", array());
			}
		}
		foreach ($quotation as $k => $v) {
			$data->$k = $v;
		}
		return $data;
	}

	public function getCurrentUser() {
		$db = JFactory::getDBO();
		$user = JFactory::getUser();

		if ($user->guest) {
			return;
		}

		$query = " SELECT * FROM #__users WHERE id = " . $db->Quote($user->id, false) . " LIMIT 1 ";
		$db->setQuery($query);
		$result = $db->loadObject();

		return $result;
	}

	public function getPolicyPlans() {
		$db = JFactory::getDBO();
		$session = JFactory::getSession();
		$data = $session->get("motor.data");
		$query = $db->getQuery(TRUE);

		$query->select("p.*, c.logo, c.color_code ");
		$query->from($this->_tb_plan . " AS p");
		$query->innerJoin($this->_tb_company . " AS c ON c.insurer_code = p.insurer_code");
		$query->innerJoin($this->_tb_zones . " AS z ON z.insurer_code = p.insurer_code AND z.zone = p.zone_type");
		
		// filters
		$country = $data['country'];
		$trip_type = $data['trip_type'];
		$group_type = $data['group_type'];
		$no_of_days = $data['no_of_days'];
		$no_of_travellers = $data['no_of_travellers'];

		$no_of_adults = $data['no_of_adults'];
		$no_of_children = $data['no_of_children'];
		$no_of_s_children = $data['no_of_s_children'];

		$query->where("p.trip_type = " . $db->Quote($trip_type));
		$query->where("p.group_type = " . $db->Quote($group_type));

		if(isset($country)) {
			$query->where("z.country_iso = ". $db->Quote($country));
		}
		
		if ($trip_type == "ST") {
			$query->where("p.no_of_days = " . $db->Quote($no_of_days));
		}

		if ($group_type == "JM" || $group_type == "WG") {
			$query->where("p.no_of_travellers = " . $db->Quote($no_of_travellers));
		} else {
			$query->where("p.no_of_adults = " . $db->Quote($no_of_adults));
			$query->where("p.no_of_children = " . $db->Quote($no_of_children));
			$query->where("p.no_of_s_children = " . $db->Quote($no_of_s_children));
		}
		$query->where("p.state = 1");
		$db->setQuery($query, 0, 7);
		$rows = $db->loadObjectList();
		return $rows;
	}

	public function getMyPlans() {
		$db = JFactory::getDBO();
		$quotation_id = InsureDIYMotorHelper::getQid();
		$query = $db->getQuery(TRUE);
		$query->select("qtp.*, c.company_name");
		$query->from($this->_tb_quotation_plan . " AS qtp");
		$query->from($this->_tb_company . " AS c");
		$query->where("qtp.quotation_id = " . $db->quote($quotation_id));
		$query->where("c.insurer_code = qtp.insurer_code");
		$db->setQuery($query);
		$rows = $db->loadObject();
		return $rows;
	}

	public function getMyInfo($id = FALSE) {
		$db = JFactory::getDBO();
		
		$query = $db->getQuery(TRUE)
		->select("*")
		->from($this->_motor_quote)
		->where("quote_payment_request_id = " . $db->quote($id) . " AND " . "quote_deleted_time IS NULL");
		$db->setQuery($query);
		$quoteData = $db->loadObject();

		

	
		if(!$quoteData) {
			return false;
		}

		
		$query = $db->getQuery(TRUE)
		->select("*")
		->from($this->_motor_driver_info)
		->where("quote_master_id = " . $db->quote($quoteData->id));
		$db->setQuery($query);
		$driverData = $db->loadObject();

		$resultArr = (object) array(
				"id" => $quoteData->id,
				"contact_firstname" => explode(",", $driverData->quote_driver_first_name)[0],
				"contact_lastname" => explode(",", $driverData->quote_driver_last_name)[0],
				"unique_order_no" => $quoteData->id,
				"plan_name" => $quoteData->quote_policy_selected_plan_code,
				"insurer_code" => $quoteData->quote_policy_selected_partner_code,
				"start_date" => $quoteData->quote_policy_start_date,
				"address" => $driverData->quote_driver_postcode,
				"contact_number" => $driverData->quote_driver_contact,
				"email" => explode(",", $driverData->quote_driver_email)[0]
		);

		
		return $resultArr;
		
	}

	public function getPDPayment() {
		$app = JFactory::getApplication();
		$params = $app->getParams();
		$session = JFactory::getSession();
		$quotation_id = $session->get('motor.quotation_id', JRequest::getVar('quotation_id'));
		$data = new stdClass();

		if ($this->hasPayment($quotation_id)) {
			$data->hasPayment = TRUE;
			$quotation = $this->getQuotation($quotation_id);
			if (!$quotation) {
				return FALSE;
			}
			$data->action = $params->get('pdUrl', "https://test.paydollar.com/b2cDemo/eng/payment/payForm.jsp");
			$data->mpsMode = $params->get('mpsMode', 'NIL');
			$data->currCode = $params->get('currCode', '344');
			$data->lang = $params->get('lang', 'E');
			$data->merchantId = $params->get('merchantId', FALSE);
			$data->orderRef = $quotation->unique_order_no;
			$data->amount = $quotation->premium;
			$data->cancelUrl = JURI::base() . 'index.php?option=com_insurediymotor&view=form&layout=canceled&Itemid=159';
			$data->failUrl = JURI::base() . 'index.php?option=com_insurediymotor&view=form&layout=error&Itemid=159';
			$data->successUrl = JURI::base() . 'index.php?option=com_insurediymotor&view=form&layout=thankyou&Itemid=159';
			$data->payType = "N";
			$data->payMethod = "ALL";
			$data->remark = "motor"; // Using remark to check for different type of insurances. Do not use this for other purpose.
			if (!$data->merchantId) {
				return FALSE;
			}
		} else {
			$data->hasPayment = FALSE;
		}
		return $data;
	}

	public function getQuotation($quotation_id, $user_id = FALSE) {
		if (!$quotation_id) {
			return FALSE;
		}
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE);

		$query->select("q.*, qp.premium, qp.insurer_code");
		$query->from($this->_tb_quotation . " AS q");
		$query->leftJoin($this->_tb_quotation_plan . " AS qp ON q.id = qp.quotation_id");
		$query->where("q.id = " . $db->quote($quotation_id));
		if ($user_id) {
			$query->where("q.user_id = " . (int) $user_id);
		}
		$db->setQuery($query);
		$quotation = $db->loadObject();
		return $quotation;
	}

	private function hasPayment($quotation_id) { // quotation_id(int), return boolean
		$has = TRUE;
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from($this->_tb_quotation)
				->where("id = " . (int) $quotation_id);
		$db->setQuery($query);
		$quotation = $db->loadObject();
		if ($quotation->payment_status != "N" && $quotation->payment_status != "P") {
			$has = FALSE;
		}
		if ($quotation->quote_stage < 5) {
			$has = FALSE;
		}
		if ($quotation->unique_order_no == "") {
			$has = FALSE;
		}
		return $has;
	}

	public function getMySumInsuredTotal() {
		return $this->checkSumInsuredQuotationTotal();
	}

	public function getContactDetails() {
		$user = JFactory::getUser();
		$data = array();

		if ($user->id > 0) {
			$db = JFactory::getDbo();
			$query = $db->getQuery(TRUE);
			$query->select("u.name AS contact_firstname, "
					. "u.lastname AS contact_lastname, "
					. "u.block_no AS contact_block_no,"
					. "u.unit AS contact_unit,"
					. "u.building_name AS contact_building_name,"
					. "u.street_no AS contact_street_name,"
					. "u.country_code AS contact_country_code,"
					. "u.contact_no AS contact_contact_no,"
					. "u.postal_code AS contact_postalcode,"
					. "u.country AS contact_country");
			$query->from("#__users u");
			$query->where("id=" . $db->quote($user->id));
			$db->setQuery($query);
			$results = $db->loadAssoc();

			if (!is_null($results)) {
				$session = JFactory::getSession();
				$this->my_detail = $session->get('details');
				$this->my_detail['contact_country_code'] = isset($results["contact_country_code"]) ? $results["contact_country_code"] : "";
				$session->set('details', $this->my_detail);
			}

			return ($results) ? $results : $data;
		}
		return $data;
	}
	
	public function getDataByRequestId($requestId) {
		$db = JFactory::getDbo();
		$session = JFactory::getSession();
		$user = JFactory::getUser();
		
// 		if ($user->guest) {
// 			return $session->get("motor.data", FALSE);
// 		}
// 		else {
			$query = $db->getQuery(TRUE)
			->select("*")
			->from($this->_motor_quote)
			->where("quote_payment_request_id = " . $db->quote($requestId) . " AND " . "quote_deleted_time IS NULL");
			$db->setQuery($query);
			$quoteData = $db->loadObject();
			
			if(!$quoteData) {
				return false;
			}
			
			$query = $db->getQuery(TRUE)
			->select("*")
			->from($this->_motor_driver_info)
			->where("quote_master_id = " . $db->quote($quoteData->id));
			$db->setQuery($query);
			$driverData = $db->loadObject();

			$savedInputData = Array (
					"offPeakCar" => $driverData->quote_off_peak,
					"carMake" => $driverData->quote_car_make,
					"carModel" => $driverData->quote_car_model,
					"companyRegistered" => $driverData->quote_car_com_registered,
					"vehicleRegNo" =>  $driverData->quote_car_reg_no,
					"vehicleValue" =>  $driverData->quote_car_value,
					"highestOffencePoint" =>  $driverData->quote_highest_offence_point,
					"offencePoint" =>  $driverData->quote_offence_point,
					"nightParking" =>  $driverData->quote_night_parking,
					"chinaExtension" =>  $driverData->quote_china_extension,
					"coverType" =>  $driverData->quote_cover_type,
					"vehicleEngineCc" =>  $driverData->quote_car_engine_cc,
					"carBodyType" =>  $driverData->quote_car_body_type,
					"carMakeYear" =>  $driverData->quote_car_make_year,
					"carRegisterYear" =>  $driverData->quote_car_reg_year,
					"Gender" => explode(",", $driverData->quote_driver_gender),
					"dateOfBirth" => explode(",", $driverData->quote_driver_dob),
					"yearsOfDrivingExp" => explode(",", $driverData->quote_driver_exp),
					"occupationType" => explode(",", $driverData->quote_driver_occ_type),
					"Occupation" => explode(",", $driverData->quote_driver_occ),
					"Industry" => explode(",", $driverData->quote_driver_industry),
					"maritalStatus" => explode(",", $driverData->quote_driver_marital_status),
					"relationshipWithMainDriver" =>  explode(",", $driverData->quote_driver_main_driver_relation),
					"meritCert" => $driverData->quote_driver_merit_cert,
					"NCDPoints" => $driverData->quote_car_ncd,
					"ncdReason" => $driverData->quote_car_zero_ncd_reason,
					"otherNCD" => $driverData->quote_car_other_ncd,
					"demeritPoint" => explode(",", $driverData->quote_driver_demerit_point),
					"claimsPast3Years" => explode(",", $driverData->quote_driver_claim_no)[0],
					"claimAmount" => explode(",", $driverData->quote_driver_claim_amount)[0],
					"validLicense" => $driverData->quote_driver_valid_license,
					"coeExpireDate" => $driverData->quote_car_coe_expire_date,
					"policyStartDate" => $quoteData->quote_policy_start_date,
					"currentInsurer" => $driverData->quote_driver_current_insurer,
					"workshop" => $quoteData->quote_plan_workshop,
					"los" => $quoteData->quote_plan_los,
					"ncdPro" => $quoteData->quote_plan_ncdpro,
					"quotationId" => $quoteData->id,
					"transactionId" => $quoteData->quote_payment_txn_id,
					"selectedPartner" => $quoteData->quote_policy_selected_partner_code,
					"processStage" =>  $quoteData->quote_process_stage,
					"firstName" =>  explode(",", $driverData->quote_driver_last_name),
					"lastName" =>  explode(",", $driverData->quote_driver_first_name),
					"icType" => array(0=>$driverData->quote_driver_nric_type),
					"Nric" => explode(",", $driverData->quote_driver_nric),
					"Email" => array(0=>$driverData->quote_driver_email),
					"mobileNo" => array(0=>$driverData->quote_driver_contact),
					"agreeDeclaration" => $quoteData->quote_user_declaration_agreed,
					"agreePDC" => $quoteData->quote_user_privacy_agreed,
					"partnerId" => $quoteData->quote_policy_selected_partner_code,
					"plan_id" => $quoteData->quote_policy_selected_plan_code,
					"previousPolicyNo" => $quoteData->quote_policy_previous_code,
					"vehicleEngineNo" => $driverData->quote_car_engine_no,
					"vehicleChassisNo" => $driverData->quote_car_chassis_no,
					"previousRegNo" => $driverData->quote_car_prev_reg_no,
					"driveToWestMY" => $driverData->quote_driver_to_west_my,
					"firstName" => explode(",", $driverData->quote_driver_first_name),
					"lastName" => explode(",", $driverData->quote_driver_last_name),
					"icType" => explode(",", $driverData->quote_driver_nric_type),
					"Nric" => explode(",", $driverData->quote_driver_nric),
					"driverRace" => explode(",", $driverData->quote_driver_race),
					"Email" => explode(",", $driverData->quote_driver_email),
					"demeritPoint" => explode(",", $driverData->quote_driver_demerit_point),
					"driverNationality" => explode(",", $driverData->quote_driver_nationality),
					"driverApplicant" => $driverData->quote_driver_is_appliant,
					"blockNo" => $driverData->quote_driver_address_block_no,
					"streetName" => $driverData->quote_driver_address_street_name,
					"unitNo" => $driverData->quote_driver_address_unit_no,
					"buildingName" => $driverData->quote_driver_address_building_name,
					"zipCode" => $driverData->quote_driver_postcode,
					"country" => $driverData->quote_driver_country,
					"loan" => $driverData->quote_driver_loan,
					"loanCompany" => $driverData->quote_driver_loan_company
					);
			
			if($quoteData->quote_policy_selected_partner_code == "sompo") {
				$query = $db->getQuery(TRUE)
				->select("*")
				->from($this->_motor_quote_sompo)
				->where("quote_master_id = " . $db->quote($quoteData->id));
				$db->setQuery($query);
				$sompoData = $db->loadObject();
				
				$planKey = array_search($quoteData->quote_policy_selected_plan_code, explode(",", $sompoData->sompo_plan_id));
				
				$planName = explode(",", $sompoData->sompo_plan_name)[$planKey];
				
				$planOrder = explode(",", $sompoData->sompo_plan_order)[$planKey];
				$planCategory = isset(explode(",", $sompoData->sompo_plan_category)[$planKey]) ? explode(",", $sompoData->sompo_plan_category)[$planKey] : "";
				$planRisk = explode("#", $sompoData->sompo_plan_risk)[$planKey];
				
				$planPremiumBeforeTax = explode(",", $sompoData->sompo_plan_premium_before_tax)[$planKey];
				$planPremium = explode(",", $sompoData->sompo_plan_premium_total)[$planKey];
				$planTaxAmt = explode(",", $sompoData->sompo_plan_premium_tax_amt)[$planKey];
				
				$savedInputData["vehVariantSompo"] = $sompoData->quote_car_variant;
				$savedInputData["ref_id"] = $sompoData->sompo_app_quote_ref_id;
				$savedInputData["plan_order"] = $planOrder;
				$savedInputData["plan_category"] = $planCategory;
				$savedInputData["sompoQuoteId"] = $sompoData->id;
				$savedInputData["sompo_agent_ref_id"] = $sompoData->sompo_pol_agent_ref_id;
			}
			else if($quoteData->quote_policy_selected_partner_code == "msig") {
				$query = $db->getQuery(TRUE)
				->select("*")
				->from($this->_motor_quote_msig)
				->where("quote_master_id = " . $db->quote($quoteData->id));
				$db->setQuery($query);
				$msigData = $db->loadObject();
				
				$planKey = array_search($quoteData->quote_policy_selected_plan_code, explode(",", $msigData->msig_plan_id));
				
				$planName = explode(",", $msigData->msig_plan_name)[$planKey];
				
				$planRisk = "";
				
				$planPremium = explode(",", $msigData->msig_plan_premium_total)[$planKey];
				$planTaxAmt = explode(",", $msigData->msig_plan_premium_tax_amt)[$planKey];
				$planPremiumBeforeTax = $planPremium - $planTaxAmt;
				
				$savedInputData["vehVariantMsig"] = $msigData->quote_car_variant;
				$savedInputData["ref_id"] = $msigData->msig_app_ref_id;
				$savedInputData["msigQuoteId"] = $msigData->id;
			}
			else if($quoteData->quote_policy_selected_partner_code == "hla") {
				$query = $db->getQuery(TRUE)
				->select("*")
				->from($this->_motor_quote_hla)
				->where("quote_master_id = " . $db->quote($quoteData->id));
				$db->setQuery($query);
				$hlaData = $db->loadObject();
				
				$planName = $hlaData->hla_plan_name;
				
				$planRisk = "";
				
				$planPremium = $hlaData->hla_plan_premium_total;
				$planTaxAmt = "";
				$planPremiumBeforeTax = "";
				
				$savedInputData["vehVariantHla"] = $hlaData->quote_car_variant;
				$savedInputData["ref_id"] = "";
				$savedInputData["hlaQuoteId"] = $hlaData->id;
			}
			else if($quoteData->quote_policy_selected_partner_code == "aig") {
				$query = $db->getQuery(TRUE)
				->select("*")
				->from($this->_motor_quote_aig)
				->where("quote_master_id = " . $db->quote($quoteData->id));
				$db->setQuery($query);
				$aigData = $db->loadObject();
				
				$planName = $aigData->aig_plan_name;
				
				$planRisk = "";
				
				$planPremium = $aigData->aig_plan_premium_total;
				$planTaxAmt = "";
				$planPremiumBeforeTax = "";
				
				$savedInputData["vehVariantAig"] = $aigData->quote_car_variant;
				$savedInputData["ref_id"] = "";
				$savedInputData["aigQuoteId"] = $aigData->id;
			}
			else if($quoteData->quote_policy_selected_partner_code == "aw") {
				$query = $db->getQuery(TRUE)
				->select("*")
				->from($this->_motor_quote_aw)
				->where("quote_master_id = " . $db->quote($quoteData->id));
				$db->setQuery($query);
				$awData = $db->loadObject();
				
				$planName = $awData->aw_plan_name;
				
				$planRisk = "";
				
				$planPremium = $awData->aw_plan_premium_total;
				$planTaxAmt = "";
				$planPremiumBeforeTax = "";
				
				$savedInputData["vehVariantAw"] = $awData->quote_car_variant;
				$savedInputData["ref_id"] = "";
				$savedInputData["awQuoteId"] = $awData->id;
			}
			else if($quoteData->quote_policy_selected_partner_code == "pingan") {
				$query = $db->getQuery(TRUE)
				->select("*")
				->from($this->_motor_quote_pingan)
				->where("quote_master_id = " . $db->quote($quoteData->id));
				$db->setQuery($query);
				$pinganData = $db->loadObject();
				
				$planName = $pinganData->pingan_plan_name;
				
				$planRisk = "";
				
				$planPremium = $pinganData->pingan_plan_premium_total;
				$planTaxAmt = "";
				$planPremiumBeforeTax = "";
				
				$savedInputData["vehVariantPingan"] = $pinganData->quote_car_variant;
				$savedInputData["ref_id"] = "";
				$savedInputData["pinganQuoteId"] = $pinganData->id;
			}
			
			$savedInputData["plan_name"] = $planName;
			$savedInputData["plan_risk"] = $planRisk;
			$savedInputData["plan_premium"] = $planPremium;
			$savedInputData["plan_premium_before_tax"] = $planPremiumBeforeTax;
			$savedInputData["plan_premium_tax_amt"] = $planTaxAmt;
			
			$session->set("motor.data", $savedInputData);
			
			return $savedInputData;
// 		}
	}
	
	public function getOccupationList() {
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
		->select("*")
		->from("#__insure_motor_occupation")
		->order('occ_name ASC');
		$db->setQuery($query);
		$check = $db->loadObjectList();
		
		return $check;
	}

	public function getOccupationParentList() {
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
		->select("*")
		->from("#__insure_motor_occupation_parent")
		->order('occ_name ASC');
		$db->setQuery($query);
		$check = $db->loadObjectList();
		
		return $check;
	}

	public function getDriverDocument($id) {
		$session = JFactory::getSession();
		$customerData = $session->get("motor.data", FALSE);
		$qid = $customerData["quotationId"];

		if ($id == 0) {
			$db = JFactory::getDBO();
			$query = $db->getQuery(TRUE)
			->select("*")
			->from("#__insure_motor_quote_master")
			->where("id =" . $db->quote($qid));
			$db->setQuery($query);
			$docs = $db->loadObjectList();
		} else {
			$db = JFactory::getDBO();
			$query = $db->getQuery(TRUE)
			->select("*")
			->from("#__insure_motor_quote_master")
			->where("userId =" . $db->quote($id));
			$db->setQuery($query);
			$docs = $db->loadObjectList();
		}

		

		$filedatas = [];
		$result = [];

		for ($i=0; $i < count($docs); $i++) { 

		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
		->select("*")
		->from("#__insure_motor_driver_info")
		->where("quote_master_id =" . $db->quote($docs[$i]->id));
		$db->setQuery($query);
		$driver = $db->loadObjectList();

		$driverCount = count(explode(",", $driver[0]->quote_driver_gender));

		$vReg = explode("/", $docs[$i]->quote_file_vehicle_reg);
		$vReg = end($vReg);
		$drivingL = explode("/", $docs[$i]->quote_file_driving_license);
		$drivingL = end($drivingL);
		$drivingL_2 = explode("/", $docs[$i]->quote_file_driving_license_2);
		$drivingL_2 = end($drivingL_2);
		$drivingL_3 = explode("/", $docs[$i]->quote_file_driving_license_3);
		$drivingL_3 = end($drivingL_3);
		$hkid = explode("/", $docs[$i]->quote_file_hkid);
		$hkid = end($hkid);
		$hkid_2 = explode("/", $docs[$i]->quote_file_hkid_2);
		$hkid_2 = end($hkid_2);
		$hkid_3 = explode("/", $docs[$i]->quote_file_hkid_3);
		$hkid_3 = end($hkid_3);
		$renew = explode("/", $docs[$i]->quote_file_renewal_notice);
		$renew = end($renew);
		$ncd = explode("/", $docs[$i]->quote_file_ncd);
		$ncd = end($ncd);
		$isFinish = $docs[$i]->quote_file_finish;

		$fileData = Array (
			"propsalId" => $docs[$i]->quote_payment_request_id,
			"registrationDoc" => $vReg,
			"registrationDocType" => "file_vehicle_reg",
			"drivingLicense" => $drivingL,
			"drivingLicenseType" => "file_driving_license",
			"drivingLicense_2" => $drivingL_2,
			"drivingLicenseType_2" => "file_driving_license_2",
			"drivingLicense_3" => $drivingL_3,
			"drivingLicenseType_3" => "file_driving_license_3",
			"hkid" => $hkid,
			"hkidType" => "file_hkid",
			"hkid_2" => $hkid_2,
			"hkidType_2" => "file_hkid_2",
			"hkid_3" => $hkid_3,
			"hkidType_3" => "file_hkid_3",
			"renewalNotice" => $renew,
			"renewalNoticeType" => "file_renewal_notice",
			"ncd" => $ncd,
			"ncdType" => "file_ncd",
			"isFinish" => $isFinish,
			"driverCount" => $driverCount
		);

		$result[$docs[$i]->quote_payment_request_id] = $fileData;
		$result[$docs[$i]->quote_payment_request_id]["login"] = true;

		$singlePid = $docs[$i]->quote_payment_request_id;
	}
		
		if($id < 1) {
			if ($qid) {
				if(count($docs) == 1){
					$result[$singlePid]["single"] = true;
					$result[$singlePid]["login"] = true;
					return $result[$singlePid];
				} else {
					$result["login"] = true;
					return $result;
				}
			} else {
				return array("login" => false);
			}
			
		} else {
			if(count($docs) == 1){
				$result[$singlePid]["single"] = true;
				$result[$singlePid]["login"] = true;
				return $result[$singlePid];
			} else {
				$result["login"] = true;
				return $result;
			}
		}
		
		
	}
	
	public function getOccupationName($code) {
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
		->select("occ_name")
		->from("#__insure_motor_occupation")
		->where("occ_code =" . $db->quote($code));
		$db->setQuery($query);
		$check = $db->loadResult();
		
		return $check;
	}
	
	public function getInsurerList() {
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
		->select("*")
		->from("#__insure_motor_current_insurer")
		->order('insurer_name ASC');
		$db->setQuery($query);
		$check = $db->loadObjectList();
		
		return $check;
	}
	
	public function getSavedInput() {
		$db = JFactory::getDbo();
		$session = JFactory::getSession();
		$user = JFactory::getUser();
		$customerData = $session->get("motor.data", FALSE);
		
		if ($user->guest && !$customerData["quotationId"]) {
			return $session->get("motor.data", FALSE);
		}
		else {
				if ($user->id == 0) {
					$query = $db->getQuery(TRUE)
					->select("*")
					->from($this->_motor_quote)
					->where("id = " . $db->quote($customerData["quotationId"]) . " AND " . "quote_deleted_time IS NULL AND quote_payment_txn_id= ''");
					$db->setQuery($query);
					$quoteData = $db->loadObject();
				} else {
					$query = $db->getQuery(TRUE)
					->select("*")
					->from($this->_motor_quote)
					->where("userId = " . $db->quote($user->id) . " AND " . "quote_deleted_time IS NULL AND quote_payment_txn_id= ''")
					->order('id DESC');
					$db->setQuery($query);
					$quoteData = $db->loadObject();
				}
				
				
				if(!$quoteData) {
					return $session->get("motor.data", FALSE);
				}
				
				$query = $db->getQuery(TRUE)
				->select("*")
				->from($this->_motor_driver_info)
				->where("quote_master_id = " . $db->quote($quoteData->id));
				$db->setQuery($query);
				$driverData = $db->loadObject();
				
				$savedInputData = Array (
						"offPeakCar" => $driverData->quote_off_peak,
						"carMake" => $driverData->quote_car_make,
						"carModel" => $driverData->quote_car_model,
						"companyRegistered" => $driverData->quote_car_com_registered,
						"vehicleRegNo" =>  $driverData->quote_car_reg_no,
						"vehicleValue" =>  $driverData->quote_car_value,
						"highestOffencePoint" =>  $driverData->quote_highest_offence_point,
						"offencePoint" =>  $driverData->quote_offence_point,
						"nightParking" =>  $driverData->quote_night_parking,
						"chinaExtension" =>  $driverData->quote_china_extension,
						"coverType" =>  $driverData->quote_cover_type,
						"prevPolicyEnd" =>  $driverData->quote_car_policy_end,
						"drivingTest" =>  explode(",",$driverData->quote_driver_driving_test),
						"vehicleEngineCc" =>  $driverData->quote_car_engine_cc,
						"carBodyType" =>  $driverData->quote_car_body_type,
						"carMakeYear" =>  $driverData->quote_car_make_year,
						"carRegisterYear" =>  $driverData->quote_car_reg_year,
						"Gender" => explode(",", $driverData->quote_driver_gender),
						"dateOfBirth" => explode(",", $driverData->quote_driver_dob),
						"yearsOfDrivingExp" => explode(",", $driverData->quote_driver_exp),
						"occupationType" => explode(",", $driverData->quote_driver_occ_type),
						"Occupation" => explode(",", $driverData->quote_driver_occ),
						"Industry" => explode(",", $driverData->quote_driver_industry),
						"maritalStatus" => explode(",", $driverData->quote_driver_marital_status),
						"relationshipWithMainDriver" =>  explode(",", $driverData->quote_driver_main_driver_relation),
						"meritCert" => $driverData->quote_driver_merit_cert,
						"NCDPoints" => $driverData->quote_car_ncd,
						"ncdReason" => $driverData->quote_car_zero_ncd_reason,
						"otherNCD" => $driverData->quote_car_other_ncd,
						"demeritPoint" => explode(",", $driverData->quote_driver_demerit_point),
						"claimsPast3Years" => explode(",", $driverData->quote_driver_claim_no),
						"claimAmount" => explode(",", $driverData->quote_driver_claim_amount),
						"offencePoint" => explode(",", $driverData->quote_offence_point),
						"highestOffencePoint" => explode(",", $driverData->quote_highest_offence_point),
						"validLicense" => explode(",", $driverData->quote_driver_valid_license),
						"coeExpireDate" => $driverData->quote_car_coe_expire_date,
						"policyStartDate" => $quoteData->quote_policy_start_date,
						"currentInsurer" => $driverData->quote_driver_current_insurer,
						"workshop" => $quoteData->quote_plan_workshop,
						"los" => $quoteData->quote_plan_los,
						"ncdPro" => $quoteData->quote_plan_ncdpro,
						"quotationId" => $quoteData->id,
						"quoteStep" => $quoteData->quote_process_stage,
						"promoCode" => $quoteData->quote_promo_code,
						"selectedPartner" => $quoteData->quote_policy_selected_partner_code,
						"firstName" => explode(",", $driverData->quote_driver_last_name),
						"lastName" => explode(",", $driverData->quote_driver_first_name),
						"icType" => array(0=>$driverData->quote_driver_nric_type),
						"Nric" => array(0=>$driverData->quote_driver_nric),
						"Email" => array(0=>$driverData->quote_driver_email),
						"mobileNo" => $driverData->quote_driver_contact,
						"addressOne" => $driverData->quote_driver_address_line_1,
						"addressTwo" => $driverData->quote_driver_address_line_2,
						"drivingLicense" => explode(",", $driverData->quote_driver_valid_license),
						"agreeDeclaration" => $quoteData->quote_user_declaration_agreed,
						"agreePDC" => $quoteData->quote_user_privacy_agreed,
						"partnerId" => $quoteData->quote_policy_selected_partner_code,
						"plan_id" => $quoteData->quote_policy_selected_plan_code,
						"previousPolicyNo" => $quoteData->quote_policy_previous_code,
						"vehicleEngineNo" => $driverData->quote_car_engine_no,
						"vehicleChassisNo" => $driverData->quote_car_chassis_no,
						"previousRegNo" => $driverData->quote_car_prev_reg_no,
						"driveToWestMY" => $driverData->quote_driver_to_west_my,
						"firstName" => explode(",", $driverData->quote_driver_first_name),
						"lastName" => explode(",", $driverData->quote_driver_last_name),
						"icType" => explode(",", $driverData->quote_driver_nric_type),
						"Nric" => explode(",", $driverData->quote_driver_nric),
						"driverRace" => explode(",", $driverData->quote_driver_race),
						"Email" => explode(",", $driverData->quote_driver_email),
						"demeritPoint" => explode(",", $driverData->quote_driver_demerit_point),
						"driverNationality" => explode(",", $driverData->quote_driver_nationality),
						"driverApplicant" => $driverData->quote_driver_is_appliant,
						"blockNo" => $driverData->quote_driver_address_block_no,
						"streetName" => $driverData->quote_driver_address_street_name,
						"unitNo" => $driverData->quote_driver_address_unit_no,
						"buildingName" => $driverData->quote_driver_address_building_name,
						"zipCode" => $driverData->quote_driver_postcode,
						"country" => $driverData->quote_driver_country,
						"loan" => $driverData->quote_driver_loan,
						"loanCompany" => $driverData->quote_driver_loan_company,
						"vehVariant" => explode(",", $quoteData->quote_selected_model_variant),
						"preChange" => $quoteData->quote_pre_quote_change_flag
				);
				
				$selectedPartnerFlag = false;
				
				if($quoteData->quote_policy_selected_partner_code == "sompo") {
					$query = $db->getQuery(TRUE)
					->select("*")
					->from($this->_motor_quote_sompo)
					->where("quote_master_id = " . $db->quote($quoteData->id));
					$db->setQuery($query);
					$sompoData = $db->loadObject();
					
					if($sompoData) {
						$selectedPartnerFlag = true;
						
						$planKey = array_search($quoteData->quote_policy_selected_plan_code, explode(",", $sompoData->sompo_plan_id));
						
						$planName = explode(",", $sompoData->sompo_plan_name)[$planKey];
						
						$planRisk = explode("#", $sompoData->sompo_plan_risk)[$planKey];
						
						$planPremium = explode(",", $sompoData->sompo_plan_premium_total)[$planKey];
						
						$savedInputData["vehVariantSompo"] = $sompoData->quote_car_variant;
						$savedInputData["ref_id"] = $sompoData->sompo_app_quote_ref_id;
					}
				}
				else if($quoteData->quote_policy_selected_partner_code == "msig") {
					$query = $db->getQuery(TRUE)
					->select("*")
					->from($this->_motor_quote_msig)
					->where("quote_master_id = " . $db->quote($quoteData->id));
					$db->setQuery($query);
					$msigData = $db->loadObject();
					
					if($msigData) {
						$selectedPartnerFlag = true;
						
						$planKey = array_search($quoteData->quote_policy_selected_plan_code, explode(",", $msigData->msig_plan_id));
						
						$planName = explode(",", $msigData->msig_plan_name)[$planKey];
						
						$planRisk =  explode(",", $msigData->msig_plan_excess_standard)[$planKey];
						
						$planPremium = explode(",", $msigData->msig_plan_premium_total)[$planKey];
						
						$savedInputData["msigId"] = $msigData->id;
						$savedInputData["vehVariantMsig"] = $msigData->quote_car_variant;
						$savedInputData["ref_id"] = $msigData->msig_ref_id;
						$savedInputData["msigValidThrough"] = $msigData->msig_quote_valid_through;
					}
				}
				else if($quoteData->quote_policy_selected_partner_code == "hla") {
					$query = $db->getQuery(TRUE)
					->select("*")
					->from($this->_motor_quote_hla)
					->where("quote_master_id = " . $db->quote($quoteData->id));
					$db->setQuery($query);
					$hlaData = $db->loadObject();
					
					if($hlaData) {
						$selectedPartnerFlag = true;
						
						$planKey = array_search($quoteData->quote_policy_selected_plan_code, explode(",", $hlaData->hla_plan_id));
						
						$planName = explode(",", $hlaData->hla_plan_name)[$planKey];
						
						$planRisk = "<b>";
						
						$planRisk .= $savedInputData["workshop"] == "Yes" ? explode("#", $hlaData->hla_plan_excess_standard)[0] : explode("#", $hlaData->hla_plan_excess_standard)[1];
						
						$planRisk .= "</b></li><li class=\"desc-list\">Young and/or inexperienced driver excess : <b>" . $hlaData->hla_plan_excess_young . "</b>";
						
						$planPremium = explode(",", $hlaData->hla_plan_premium_total)[$planKey];
						
						$savedInputData["vehVariantHla"] = $hlaData->quote_car_variant;
						$savedInputData["ref_id"] = "";
					}
				}
				else if($quoteData->quote_policy_selected_partner_code == "aig") {
					$query = $db->getQuery(TRUE)
					->select("*")
					->from($this->_motor_quote_aig)
					->where("quote_master_id = " . $db->quote($quoteData->id));
					$db->setQuery($query);
					$aigData = $db->loadObject();
					
					if($aigData) {
						$selectedPartnerFlag = true;
						
						$planKey = array_search($quoteData->quote_policy_selected_plan_code, explode(",", $aigData->aig_plan_id));
						
						$planName = explode(",", $aigData->aig_plan_name)[$planKey];
						
						$planRisk = "";
						
						$planPremium = explode(",", $aigData->aig_plan_premium_total)[$planKey];
						
						$savedInputData["vehVariantAig"] = $aigData->quote_car_variant;
						$savedInputData["ref_id"] = "";
					}
				}
				else if($quoteData->quote_policy_selected_partner_code == "aw") {
					$query = $db->getQuery(TRUE)
					->select("*")
					->from($this->_motor_quote_aw)
					->where("quote_master_id = " . $db->quote($quoteData->id));
					$db->setQuery($query);
					$awData = $db->loadObject();
					
					if($awData) {
						$selectedPartnerFlag = true;
						
						$planKey = array_search($quoteData->quote_policy_selected_plan_code, explode(",", $awData->aw_plan_id));
						
						$planName = explode(",", $awData->aw_plan_name)[$planKey];
						
						$planRisk = "";
						
						$planPremium = explode(",", $awData->aw_plan_premium_total)[$planKey];
						
						$savedInputData["vehVariantAw"] = $awData->quote_car_variant;
						$savedInputData["ref_id"] = "";
					}
				}
				else if($quoteData->quote_policy_selected_partner_code == "pingan") {
					$query = $db->getQuery(TRUE)
					->select("*")
					->from($this->_motor_quote_pingan)
					->where("quote_master_id = " . $db->quote($quoteData->id));
					$db->setQuery($query);
					$pinganData = $db->loadObject();
					
					if($pinganData) {
						$selectedPartnerFlag = true;
						
						$planKey = array_search($quoteData->quote_policy_selected_plan_code, explode(",", $pinganData->pingan_plan_id));
						
						$planName = explode(",", $pinganData->pingan_plan_name)[$planKey];
						
						$planRisk = "";
						
						$planPremium = explode(",", $pinganData->pingan_plan_premium_total)[$planKey];
						
						$savedInputData["vehVariantPingan"] = $pinganData->quote_car_variant;
						$savedInputData["ref_id"] = "";
					}
				}
				
				if($quoteData->quote_policy_selected_partner_code != "" && $selectedPartnerFlag) {
					$savedInputData["plan_name"] = $planName;
					$savedInputData["plan_risk"] = $planRisk;
					$savedInputData["plan_premium"] = $planPremium;
				}
				
				$session->set("motor.data", $savedInputData);
				
				return $savedInputData;
			//}
		}
	}
	
	public function getSavedPlans() {
		$db = JFactory::getDBO();
		
		$session = JFactory::getSession();
		$user = JFactory::getUser();
		$customerData = $session->get("motor.data", FALSE);
		
		// if ($user->guest) {
		// 	return false;
		// }
		// else {
			$result = array();
			
			$query = $db->getQuery(TRUE)
			->select("*")
			->from($this->_motor_quote)
			->where("id = " . $db->quote($customerData["quotationId"]) . " AND " . "quote_deleted_time IS NULL AND quote_payment_txn_id= ''");
			$db->setQuery($query);
			$quoteData = $db->loadObject();
			
			if(!$quoteData) {
				return false;
			}
			
			if($quoteData->quote_pre_quote_change_flag == "true") {
				return false;
			}
			
			$now = time();
			$prev = strtotime($quoteData->quote_updated_time);
			$dateDiff = round(($now - $prev)/ (60 * 60 * 24));
			
			if($dateDiff > 1) {
				return false;
			}
			
			
			$query = $db->getQuery(TRUE)
			->select("*")
			->from($this->_motor_quote_aig)
			->where("quote_master_id = " . $db->quote($quoteData->id));
			$db->setQuery($query);
			$aigData = $db->loadObject();

			$query = $db->getQuery(TRUE)
			->select("*")
			->from($this->_motor_quote_aw)
			->where("quote_master_id = " . $db->quote($quoteData->id));
			$db->setQuery($query);
			$awData = $db->loadObject();

			$query = $db->getQuery(TRUE)
			->select("*")
			->from($this->_motor_quote_pingan)
			->where("quote_master_id = " . $db->quote($quoteData->id));
			$db->setQuery($query);
			$pinganData = $db->loadObject();
			
			$query = $db->getQuery(TRUE)
			->select("*")
			->from($this->_motor_quote_renewal)
			->where("quote_master_id = " . $db->quote($quoteData->id));
			$db->setQuery($query);
			$renewalData = $db->loadObject();
			
			
			if($aigData) {
				$aigArr = (object)array(
						"id" => $aigData->id,
						//"hla_ref_id" =>"",
						// "quote_policy_selected_plan_code" =>$quoteData->quote_policy_selected_plan_code,
						"aig_plan_id" => $aigData->aig_plan_id,
						"aig_plan_name" => $aigData->aig_plan_name,
						"aig_plan_premium_total" => $aigData->aig_plan_premium_total,
						"aig_plan_general_excess" => $aigData->aig_plan_general_excess,
						"aig_plan_theft_loss_excess" => $aigData->aig_plan_theft_loss_excess,
						"aig_plan_third_party_excess" => $aigData->aig_plan_third_party_excess,
						"aig_plan_young_excess" => $aigData->aig_plan_young_excess,
						"aig_plan_inexperience_excess" => $aigData->aig_plan_inexperience_excess,
						"aig_plan_parking_excess" => $aigData->aig_plan_parking_excess,
						"aig_plan_unammed_excess" => $aigData->aig_plan_unammed_excess
				);
				
				$result["aig"] = $aigArr;
			}

			if($awData) {
				$awArr = (object)array(
						"id" => $awData->id,
						"aw_plan_id" => $awData->aw_plan_id,
						"aw_plan_name" => $awData->aw_plan_name,
						"aw_plan_premium_total" => $awData->aw_plan_premium_total,
						"aw_plan_general_excess" => $awData->aw_plan_general_excess,
						"aw_plan_theft_loss_excess" => $awData->aw_plan_theft_loss_excess,
						"aw_plan_third_party_excess" => $awData->aw_plan_third_party_excess,
						"aw_plan_young_excess" => $awData->aw_plan_young_excess,
						"aw_plan_inexperience_excess" => $awData->aw_plan_inexperience_excess,
						"aw_plan_parking_excess" => $awData->aw_plan_parking_excess,
						"aw_plan_unammed_excess" => $awData->aw_plan_unammed_excess
				);
				
				$result["aw"] = $awArr;
			}

			if($pinganData) {
				$pinganArr = (object)array(
						"id" => $pinganData->id,
						"pingan_plan_id" => $pinganData->pingan_plan_id,
						"pingan_plan_name" => $pinganData->pingan_plan_name,
						"pingan_plan_premium_total" => $pinganData->pingan_plan_premium_total,
						"pingan_plan_general_excess" => $pinganData->pingan_plan_general_excess,
						"pingan_plan_theft_loss_excess" => $pinganData->pingan_plan_theft_loss_excess,
						"pingan_plan_third_party_excess" => $pinganData->pingan_plan_third_party_excess,
						"pingan_plan_young_excess" => $pinganData->pingan_plan_young_excess,
						"pingan_plan_inexperience_excess" => $pinganData->pingan_plan_inexperience_excess,
						"pingan_plan_parking_excess" => $pinganData->pingan_plan_parking_excess,
						"pingan_plan_unammed_excess" => $pinganData->pingan_plan_unammed_excess

				);
				
				$result["pingan"] = $pinganArr;
			}
			
			return $result;
		// }
	}
	
	public function getPartnerBenefits() {
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
		->select("*")
		->from("#__insure_motor_partner_setting");
		$db->setQuery($query);
		$result = $db->loadObjectList();
		$benefit = array();
		
		foreach($result as $item) {
			if($item->partner_id == "sompo") {
				$benefit["sompo"] = $item;
			}
			if($item->partner_id == "msig") {
				$benefit["msig"] = $item;
			}
			if($item->partner_id == "hla") {
				$benefit["hla"] = $item;
			}
		}
		
		return $benefit;
	}
	
	public function getMakeModel($carMake) {
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
		->select("*")
		->from($this->_motor_make)
		->where("make_code > ''")
		->order("make_name ASC");
		$db->setQuery($query);
		$carMakeData = $db->loadObjectList();
		
		$query = $db->getQuery(TRUE)
		->select("*")
		->from($this->_motor_model)
		->where("make_code=".$db->quote($carMake))
		->order("model_name ASC");
		$db->setQuery($query);
		$carModelData = $db->loadObjectList();
		
		return array("make" => $carMakeData, "model" => $carModelData);
	}
	
	public function getModelName($make, $model) {
		$db = JFactory::getDBO();
		
		$query = $db->getQuery(TRUE)
		->select("make_name")
		->from($this->_motor_make)
		->where("make_code = " .$db->quote($make));
		$db->setQuery($query);
		$makeData = $db->loadResult();
		
		if(!$makeData) {
			return false;
		}
		
		$query = $db->getQuery(TRUE)
		->select("model_name")
		->from($this->_motor_model)
		->where("model_code = " .$db->quote($model));
		$db->setQuery($query);
		$modelData = $db->loadResult();
		
		if(!$modelData) {
			return false;
		}
		
		return array("make" => $makeData, "model" => $modelData);
	}

	public function getModelVariant($model, $make, $year) {
		
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
		->select("*")
		->from($this->_motor_make_model_aig)
		->where("model_master_code = " .$db->quote($model));
		$db->setQuery($query);
		$aigModelData = $db->loadObjectList();

		$query = $db->getQuery(TRUE)
		->select("*")
		->from($this->_motor_make_model_aw)
		->where("model_master_code = " .$db->quote($model))
		->where("year = " .$db->quote($year));
		$db->setQuery($query);
		$awModelData = $db->loadObjectList();

		$query = $db->getQuery(TRUE)
		->select("*")
		->from($this->_motor_make_model_pingan)
		->where("model_master_code = " .$db->quote($model));
		$db->setQuery($query);
		$pinganModelData = $db->loadObjectList();
		
		// if variant not found, show all under same make
		if (count($aigModelData) == 0) {
			$carMake = str_replace("_", " ", $make);
			$carMake = str_replace("SSANGYONG", "SSANG YONG", $carMake);
			$query = $db->getQuery(TRUE)
			->select("*")
			->from($this->_motor_make_model_aig)
			->where("make_name = " .$db->quote($carMake));
			$db->setQuery($query);
			$aigModelData = $db->loadObjectList();
		}

		if (count($awModelData) == 0) {
			$carMake = str_replace("_", " ", $make);
			$carMake = str_replace("MERCEDES BENZ", "MERCEDES-BENZ", $carMake);
			$query = $db->getQuery(TRUE)
			->select("*")
			->from($this->_motor_make_model_aw)
			->where("make_name = " .$db->quote($carMake));
			$db->setQuery($query);
			$awModelData = $db->loadObjectList();
		}

		if (count($pinganModelData) == 0) {
			$carMake = str_replace("_", " ", $make);
			$query = $db->getQuery(TRUE)
			->select("*")
			->from($this->_motor_make_model_pingan)
			->where("make_name = " .$db->quote($carMake));
			$db->setQuery($query);
			$pinganModelData = $db->loadObjectList();
		}

		
		return array("aig" => $aigModelData, "aw" => $awModelData, "pingan" => $pinganModelData);
	}
	public function getBodyType() {
		$bodyType = array(
			"1" => "CONVERTIBLE",
			"2" => "COUPE",
			"3" => "HATCHBACK",
			"5" => "SALOON",
			"6" => "SPORTS",
			"7" => "STATION WAGGON",
			"9" => "STATION WAGGON (WA)",
			"8" => "STATION WAGGON (HVY)"
		);

		return $bodyType;
	}
	
	public function getSavedCarData($quotationId, $partnerCode) {
		$db = JFactory::getDBO();
		$carModelData = "";
		
		if($partnerCode == "sompo") {
			$query = $db->getQuery(TRUE)
			->select("quote_car_variant")
			->from($this->_motor_quote_sompo)
			->where("quote_master_id = " .$db->quote($quotationId));
			$db->setQuery($query);
			$carVariantId = $db->loadResult();
			
			if(!$carVariantId) {
				return false;
			}
			
			$query = $db->getQuery(TRUE)
			->select("*")
			->from($this->_motor_make_model_sompo)
			->where("model_code = " .$db->quote($carVariantId));
			$db->setQuery($query);
			$carModelData= $db->loadObject();
			
			$query = $db->getQuery(TRUE)
			->select("make_code")
			->from($this->_motor_model)
			->where("model_code = " .$db->quote($carModelData->model_master_code));
			$db->setQuery($query);
			$carMakeCode = $db->loadResult();
			
			if(!$carMakeCode) {
				return false;
			}
			
			$query = $db->getQuery(TRUE)
			->select("make_name")
			->from($this->_motor_make)
			->where("make_code = " .$db->quote($carMakeCode));
			$db->setQuery($query);
			$carMake= $db->loadResult();
			
			if(!$carMake) {
				return false;
			}
			
			$carModelData->make_name = $carMake;
		}
		
		if($partnerCode == "msig") {
			$query = $db->getQuery(TRUE)
			->select("quote_car_variant")
			->from($this->_motor_quote_msig)
			->where("quote_master_id = " .$db->quote($quotationId));
			$db->setQuery($query);
			$carVariantId = $db->loadResult();
			
			if(!$carVariantId) {
				return false;
			}
			
			$query = $db->getQuery(TRUE)
			->select("*")
			->from($this->_motor_make_model_msig)
			->where("model_code = " .$db->quote($carVariantId));
			$db->setQuery($query);
			$carModelData= $db->loadObject();
			
			if(!$carModelData) {
				return false;
			}
			
			$query = $db->getQuery(TRUE)
			->select("make_code")
			->from($this->_motor_model)
			->where("model_code = " .$db->quote($carModelData->model_master_code));
			$db->setQuery($query);
			$carMakeCode = $db->loadResult();
			
			if(!$carMakeCode) {
				return false;
			}
			
			$query = $db->getQuery(TRUE)
			->select("make_name")
			->from($this->_motor_make)
			->where("make_code = " .$db->quote($carMakeCode));
			$db->setQuery($query);
			$carMake= $db->loadResult();
			
			if(!$carMake) {
				return false;
			}
			
			$carModelData->make_name = $carMake;
		}
		
		if($partnerCode == "hla") {
			$query = $db->getQuery(TRUE)
			->select("quote_car_variant")
			->from($this->_motor_quote_hla)
			->where("quote_master_id = " .$db->quote($quotationId));
			$db->setQuery($query);
			$carVariantId = $db->loadResult();
			
			if(!$carVariantId) {
				return false;
			}
			
			$query = $db->getQuery(TRUE)
			->select("*")
			->from($this->_motor_make_model_hla)
			->where("model_code = " .$db->quote($carVariantId));
			$db->setQuery($query);
			$carModelData= $db->loadObject();
			
			if(!$carModelData) {
				return false;
			}
			
			$query = $db->getQuery(TRUE)
			->select("make_code")
			->from($this->_motor_model)
			->where("model_code = " .$db->quote($carModelData->model_master_code));
			$db->setQuery($query);
			$carMakeCode = $db->loadResult();
			
			if(!$carMakeCode) {
				return false;
			}
			
			$query = $db->getQuery(TRUE)
			->select("make_name")
			->from($this->_motor_make)
			->where("make_code = " .$db->quote($carMakeCode));
			$db->setQuery($query);
			$carMake= $db->loadResult();
			
			if(!$carMake) {
				return false;
			}
			
			$carModelData->make_name = $carMake;
		}

		if($partnerCode == "aig") {
			$query = $db->getQuery(TRUE)
			->select("quote_car_variant")
			->from($this->_motor_quote_aig)
			->where("quote_master_id = " .$db->quote($quotationId));
			$db->setQuery($query);
			$carVariantId = $db->loadResult();
			
			if(!$carVariantId) {
				return false;
			}
			
			$query = $db->getQuery(TRUE)
			->select("*")
			->from($this->_motor_make_model_aig)
			->where("id = " .$db->quote($carVariantId));
			$db->setQuery($query);
			$carModelData= $db->loadObject();
			
			if(!$carModelData) {
				return false;
			}
			
			$query = $db->getQuery(TRUE)
			->select("make_code")
			->from($this->_motor_model)
			->where("model_code = " .$db->quote($carModelData->model_master_code));
			$db->setQuery($query);
			$carMakeCode = $db->loadResult();
			
			if(!$carMakeCode) {
				return false;
			}
			
			$query = $db->getQuery(TRUE)
			->select("make_name")
			->from($this->_motor_make)
			->where("make_code = " .$db->quote($carMakeCode));
			$db->setQuery($query);
			$carMake= $db->loadResult();
			
			if(!$carMake) {
				return false;
			}
			
			$carModelData->make_name = $carMake;
		}

		if($partnerCode == "aw") {
			$query = $db->getQuery(TRUE)
			->select("quote_car_variant")
			->from($this->_motor_quote_aw)
			->where("quote_master_id = " .$db->quote($quotationId));
			$db->setQuery($query);
			$carVariantId = $db->loadResult();
			
			if(!$carVariantId) {
				return false;
			}
			
			$query = $db->getQuery(TRUE)
			->select("*")
			->from($this->_motor_make_model_aw)
			->where("id = " .$db->quote($carVariantId));
			$db->setQuery($query);
			$carModelData= $db->loadObject();
			
			if(!$carModelData) {
				return false;
			}
			
			$query = $db->getQuery(TRUE)
			->select("make_code")
			->from($this->_motor_model)
			->where("model_code = " .$db->quote($carModelData->model_master_code));
			$db->setQuery($query);
			$carMakeCode = $db->loadResult();

			if(!$carMakeCode) {
				return false;
			}
			
			$query = $db->getQuery(TRUE)
			->select("make_name")
			->from($this->_motor_make)
			->where("make_code = " .$db->quote($carMakeCode));
			$db->setQuery($query);
			$carMake= $db->loadResult();
			
			if(!$carMake) {
				return false;
			}
			
			$carModelData->make_name = $carMake;
		}

		if($partnerCode == "pingan") {
			$query = $db->getQuery(TRUE)
			->select("quote_car_variant")
			->from($this->_motor_quote_pingan)
			->where("quote_master_id = " .$db->quote($quotationId));
			$db->setQuery($query);
			$carVariantId = $db->loadResult();
			
			if(!$carVariantId) {
				return false;
			}
			

			$query = $db->getQuery(TRUE)
			->select("*")
			->from($this->_motor_make_model_pingan)
			->where("id = " .$db->quote($carVariantId));
			$db->setQuery($query);
			$carModelData= $db->loadObject();

			if(!$carModelData) {
				return false;
			}
			
			$make = explode("-", $carModelData->model_master_code);
			$make = $make[0];

			// $query = $db->getQuery(TRUE)
			// ->select("make_code")
			// ->from($this->_motor_model)
			// ->where("model_code = " .$db->quote($carModelData->model_master_code));
			// $db->setQuery($query);
			// $carMakeCode = $db->loadResult();

			// if(!$carMakeCode) {
			// 	return false;
			// }
			
			// $query = $db->getQuery(TRUE)
			// ->select("make_name")
			// ->from($this->_motor_make)
			// ->where("make_code = " .$db->quote($carMakeCode));
			// $db->setQuery($query);
			// $carMake= $db->loadResult();

			// if(!$carMake) {
			// 	return false;
			// }
			
			$carModelData->make_name = $make;
		}
		
		return $carModelData;
	}
	
	public function getloanCompanyList() {
		$db = JFactory::getDBO();
		
		$query = $db->getQuery(TRUE)
			->select("*")
			->from("#__insure_motor_loan_company")
			->order("mortgagee_name ASC");
			
		$db->setQuery($query);
			
		$data = $db->loadObjectList();

		return $data;
	}
	
	public function getLoanCompanyName($code) {
		$db = JFactory::getDBO();
		
		$query = $db->getQuery(TRUE)
		->select("mortgagee_name")
		->from("#__insure_motor_loan_company")
		->where("mortgagee_code = " .$db->quote($code));
		
		$db->setQuery($query);
		
		$data = $db->loadResult();
		
		return $data;
	}
	
	public function isEmailRegistered($email) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query = 'SELECT id FROM #__users WHERE email = ' . $db->Quote($email);
		$db->setQuery($query, 0, 1);
		$check = $db->loadResult();
		
		if($check){
			return true;
		}
		else {
			return false;
		}
	}
	
	public function setDataForRegister($data) {
		$requestData = array(
				"gender" => $data["Gender"][0] == "MALE" ? "M" : "F",
				"dob" => DateTime::createFromFormat('d-M-Y', $data["dateOfBirth"][0])->format('d-m-Y'),
				"marital_status" => $data["maritalStatus"][0] == "Married" ? "M" : "S",
				"name" => $data["firstName"][0],
				"lastname" => $data["lastName"][0],
				"nationality" => "SG",
				"email1" => $data["Email"][0],
				"occupation" => $this->getOccupationName($data["Occupation"][0]),
				"country" => "SG",
				"country_code" => "65",
				"contact_no" => $data["mobileNo"][0]
		);
		
		JFactory::getApplication()->setUserState('com_users.registration.data', $requestData);
	}

	public function generateRandomString($length = 100) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	
	public function step1save($quotation) {
		$db = JFactory::getDBO();
		$user = JFactory::getUser();
		$session = JFactory::getSession();
		$session->set("motor.data", $quotation);
		$dataobj = MyHelper::array2jObject($quotation);
		$quoteId = "";
		
		if($user->id && $user->id != 0) {
			$query = $db->getQuery(TRUE)
			->select("*")
			->from($this->_motor_quote)
			->where("userId = " . $db->quote($user->id) . " AND " . "quote_deleted_time IS NULL AND quote_payment_txn_id=\"\"")
			->order('id DESC');
			$db->setQuery($query);
			$quoteDbData = $db->loadObject();

			$date = new DateTime('now');
			$date->setTimezone(new DateTimeZone('Asia/Singapore'));
			$now = $date->format("Y-m-d\TH:i:s.u+08:00");
			
			$quoteData = (object)array(
					"userId" => $user->id,
					"quote_user_declaration_agreed" => $quotation["agreeDeclaration"],
					"quote_plan_workshop" => $quotation["workshop"],
					"quote_plan_los" => $quotation["los"],
					"quote_plan_ncdpro" => $quotation["ncdPro"],
					"quote_policy_start_date" => $quotation['policyStartDate'],
					"quote_updated_time" => $now,
					"quote_process_stage" => 2
			);
			
			$driverData = (object)array(
					"quote_car_make" => $quotation["carMake"],
					"quote_car_model" => $quotation["carModel"],
					"quote_car_com_registered" => $quotation["companyRegistered"],
					"quote_off_peak" => $quotation["offPeakCar"],
					"quote_car_reg_no" => $quotation["vehicleRegNo"],
					"quote_car_value" => $quotation["vehicleValue"],
					"quote_highest_offence_point" => $quotation["highestOffencePoint"],
					"quote_offence_point" => $quotation["offencePoint"],
					"quote_night_parking" => $quotation["nightParking"],
					"quote_china_extension" => $quotation["chinaExtension"],
					"quote_cover_type" => $quotation["coverType"],
					"quote_car_engine_cc" => $quotation["vehicleEngineCc"],
					"quote_car_body_type" => $quotation["carBodyType"],
					"quote_car_make_year" => $quotation["carMakeYear"],
					"quote_car_reg_year" => $quotation["carRegisterYear"],
					"quote_car_ncd" => $quotation["NCDPoints"],
					"quote_car_zero_ncd_reason" => $quotation["ncdReason"],
					"quote_car_other_ncd" => $quotation["otherNCD"],
					"quote_car_claim_amount" => $quotation["claimAmount"],
					"quote_car_claim_no" => $quotation["claimsPast3Years"],
					"quote_car_coe_expire_date" => $quotation["coeExpireDate"],
					"quote_driver_gender" => implode(",", $quotation["Gender"]),
					"quote_driver_dob" => implode(",", $quotation["dateOfBirth"]),
					"quote_driver_exp" => implode(",", $quotation["yearsOfDrivingExp"]),
					"quote_driver_occ_type" => implode(",", $quotation["occupationType"]),
					"quote_driver_occ" => implode(",", $quotation["Occupation"]),
					"quote_driver_industry" => implode(",", $quotation["Industry"]),
					"quote_driver_marital_status" => implode(",", $quotation["maritalStatus"]),
					"quote_driver_main_driver_relation" => implode(",", $quotation["relationshipWithMainDriver"]),
					"quote_driver_demerit_point" => implode(",", $quotation["demeritPoint"]),
					"quote_driver_claim_no" => implode(",", array($quotation["claimsPast3Years"])),
					"quote_driver_claim_amount" => implode(",", array($quotation["claimAmount"])),
					"quote_driver_valid_license" => $quotation["validLicense"],
					"quote_driver_merit_cert" => $quotation["meritCert"],
					"quote_driver_current_insurer" => $quotation["currentInsurer"]
			);
			
			if (isset($quoteDbData->id)) {
				$quoteId = $quoteDbData->id;
				$quoteData->id = $quoteId;
				$driverData->quote_master_id = $quoteId;
				
				$sompoData = (object)array("quote_master_id" => $quoteId);
				
				$query = $db->getQuery(TRUE)
				->select("*")
				->from($this->_motor_driver_info)
				->where("quote_master_id = " . $db->quote($quoteId));
				$db->setQuery($query);
				
				$driverDbData = $db->loadObject();
				$driverId = $driverDbData->id;
			
				$driverData->id = $driverId;
				$driverDbData->quote_plan_workshop = $quoteDbData->quote_plan_workshop;
				$driverDbData->quote_plan_los = $quoteDbData->quote_plan_los;
				$driverDbData->quote_plan_ncdpro = $quoteDbData->quote_plan_ncdpro;
				$driverDbData->quote_policy_start_date = $quoteDbData->quote_policy_start_date;
				
				if(InsureDIYMotorHelper::validateInputChanged($driverDbData, $quotation, false) && $quoteDbData->quote_pre_quote_change_flag != "true") {
					$quoteData->quote_pre_quote_change_flag = "false";
				}
				else {
					$quoteData->quote_pre_quote_change_flag = "true";
				}
				
				$check = $db->updateObject($this->_motor_quote, $quoteData, "id");
				
				if($check) {
					$check = $db->updateObject($this->_motor_driver_info, $driverData, "id");
					
					//$check = $db->updateObject($this->_motor_quote_sompo, $sompoData, "id");
				}
				else {
					return false;
				}
				
			} else {
				$quoteData->quote_created_time = $now;
				
				$check = $db->insertObject($this->_motor_quote, $quoteData);
				
				if($check) {
					$quoteId = $db->insertid();
					
					$driverData->quote_master_id = $quoteId;
					$sompoData->quote_master_id = $quoteId;
					
					$check = $db->insertObject($this->_motor_driver_info, $driverData);
					
					$check = $db->insertObject($this->_motor_quote_sompo, $sompoData);
				}
				else {
					return false;
				}
			}
			
			if ($quoteId != "") {
				$quotation["quotationId"] = $quoteId;
				
				$session->set("motor.data", $quotation);
			}
			
			return true;
		} else {
			$session = JFactory::getSession();
			$customerData = $session->get("motor.data", FALSE);

			$query = $db->getQuery(TRUE)
			->select("*")
			->from($this->_motor_quote)
			->where("id = " . $db->quote($customerData["quotationId"]) . " AND " . "quote_deleted_time IS NULL AND quote_payment_txn_id=\"\"");
			$db->setQuery($query);
			$quoteDbData = $db->loadObject();

			$date = new DateTime('now');
			$date->setTimezone(new DateTimeZone('Asia/Singapore'));
			$now = $date->format("Y-m-d\TH:i:s.u+08:00");
			
			$quoteData = (object)array(
					"userId" => $user->id,
					"quote_plan_workshop" => $quotation["workshop"],
					"quote_plan_los" => $quotation["los"],
					"quote_plan_ncdpro" => $quotation["ncdPro"],
					"quote_policy_start_date" => $quotation['policyStartDate'],
					"quote_updated_time" => $now,
					"quote_process_stage" => 2
			);
			
			$driverData = (object)array(
					"quote_car_make" => $quotation["carMake"],
					"quote_car_model" => $quotation["carModel"],
					"quote_car_com_registered" => $quotation["companyRegistered"],
					"quote_off_peak" => $quotation["offPeakCar"],
					"quote_car_reg_no" => $quotation["vehicleRegNo"],
					"quote_car_value" => $quotation["vehicleValue"],
					"quote_highest_offence_point" => $quotation["highestOffencePoint"],
					"quote_offence_point" => $quotation["offencePoint"],
					"quote_night_parking" => $quotation["nightParking"],
					"quote_china_extension" => $quotation["chinaExtension"],
					"quote_cover_type" => $quotation["coverType"],
					"quote_car_engine_cc" => $quotation["vehicleEngineCc"],
					"quote_car_body_type" => $quotation["carBodyType"],
					"quote_car_make_year" => $quotation["carMakeYear"],
					"quote_car_reg_year" => $quotation["carRegisterYear"],
					"quote_car_ncd" => $quotation["NCDPoints"],
					"quote_car_zero_ncd_reason" => $quotation["ncdReason"],
					"quote_car_other_ncd" => $quotation["otherNCD"],
					"quote_car_claim_amount" => $quotation["claimAmount"],
					"quote_car_claim_no" => $quotation["claimsPast3Years"],
					"quote_car_coe_expire_date" => $quotation["coeExpireDate"],
					"quote_driver_gender" => implode(",", $quotation["Gender"]),
					"quote_driver_dob" => implode(",", $quotation["dateOfBirth"]),
					"quote_driver_exp" => implode(",", $quotation["yearsOfDrivingExp"]),
					"quote_driver_occ_type" => implode(",", $quotation["occupationType"]),
					"quote_driver_occ" => implode(",", $quotation["Occupation"]),
					"quote_driver_industry" => implode(",", $quotation["Industry"]),
					"quote_driver_marital_status" => implode(",", $quotation["maritalStatus"]),
					"quote_driver_main_driver_relation" => implode(",", $quotation["relationshipWithMainDriver"]),
					"quote_driver_demerit_point" => implode(",", $quotation["demeritPoint"]),
					"quote_driver_claim_no" => implode(",", array($quotation["claimsPast3Years"])),
					"quote_driver_claim_amount" => implode(",", array($quotation["claimAmount"])),
					"quote_driver_valid_license" => $quotation["validLicense"],
					"quote_driver_merit_cert" => $quotation["meritCert"],
					"quote_driver_current_insurer" => $quotation["currentInsurer"]
			);
			
			if (isset($quoteDbData->id)) {
				$quoteId = $quoteDbData->id;
				$quoteData->id = $quoteId;
				$driverData->quote_master_id = $quoteId;
				
				$sompoData = (object)array("quote_master_id" => $quoteId);
				
				$query = $db->getQuery(TRUE)
				->select("*")
				->from($this->_motor_driver_info)
				->where("quote_master_id = " . $db->quote($quoteId));
				$db->setQuery($query);
				
				$driverDbData = $db->loadObject();
				$driverId = $driverDbData->id;
			
				$driverData->id = $driverId;
				$driverDbData->quote_plan_workshop = $quoteDbData->quote_plan_workshop;
				$driverDbData->quote_plan_los = $quoteDbData->quote_plan_los;
				$driverDbData->quote_plan_ncdpro = $quoteDbData->quote_plan_ncdpro;
				$driverDbData->quote_policy_start_date = $quoteDbData->quote_policy_start_date;
				
				if(InsureDIYMotorHelper::validateInputChanged($driverDbData, $quotation, false) && $quoteDbData->quote_pre_quote_change_flag != "true") {
					$quoteData->quote_pre_quote_change_flag = "false";
				}
				else {
					$quoteData->quote_pre_quote_change_flag = "true";
				}
				
				$check = $db->updateObject($this->_motor_quote, $quoteData, "id");
				
				if($check) {
					$check = $db->updateObject($this->_motor_driver_info, $driverData, "id");
					
					//$check = $db->updateObject($this->_motor_quote_sompo, $sompoData, "id");
				}
				else {
					return false;
				}
				
			} else {
				$quoteData->quote_created_time = $now;
				$quoteData->quote_pre_quote_change_flag = "false";
				$check = $db->insertObject($this->_motor_quote, $quoteData);
				
				if($check) {
					$quoteId = $db->insertid();
					
					$driverData->quote_master_id = $quoteId;
					$sompoData->quote_master_id = $quoteId;
					
					$check = $db->insertObject($this->_motor_driver_info, $driverData);
					
					$check = $db->insertObject($this->_motor_quote_sompo, $sompoData);
				}
				else {
					return false;
				}
			}
			
			if ($quoteId != "") {
				
				$quotation["quotationId"] = $quoteId;
				
				$session->set("motor.data", $quotation);
			}
		}
		
		return $dataobj->id;
	}

	public function step2save($quotation) {

		$db = JFactory::getDbo();
		$session = JFactory::getSession();
		$user = JFactory::getUser();
		$customerData = $session->get("motor.data", FALSE);
		
		// if($user->id == 0) {
		// 	$quotation["login_redirect"] = true;
		// 	$session->set("motor.data", $quotation);
			
		// 	return false;
 		// }
		 
		if($user->id == 0) {
			$query = $db->getQuery(TRUE)
 				->select("*")
 				->from($this->_motor_quote)
 				->where("id = " . $db->quote($customerData["quotationId"]) . " AND " . "quote_deleted_time IS NULL AND quote_payment_txn_id=\"\"");
			$db->setQuery($query);
			$quoteDbData = $db->loadObject();
			$quoteId = $quoteDbData->id;
		} else {
			$query = $db->getQuery(TRUE)
 				->select("*")
 				->from($this->_motor_quote)
				 ->where("userId = " . $db->quote($user->id) . " AND " . "quote_deleted_time IS NULL AND quote_payment_txn_id=\"\"")
				 ->order('id DESC');
			$db->setQuery($query);
			$quoteDbData = $db->loadObject();
			$quoteId = $quoteDbData->id;
		}
 		
 		
 		$date = new DateTime('now');
 		$date->setTimezone(new DateTimeZone('Asia/Singapore'));
 		$now = $date->format("Y-m-d\TH:i:s.u+08:00");
 		
 		$quoteData = (object)array(
 				"userId" => $user->id,
 				"quote_plan_workshop" => $quotation["workshop"],
 				"quote_plan_los" => $quotation["los"],
 				"quote_plan_ncdpro" => $quotation["ncdPro"],
 				"quote_policy_start_date" => $quotation['policyStartDate'],
 				"quote_updated_time" => $now,
 				"quote_user_privacy_agreed" => $quotation["agreePDC"],
 				"quote_user_marketing_agreed" => $quotation["agreeMarketing"],
 				"quote_user_declaration_agreed" => $quotation["agreeDeclaration"],
 				"quote_process_stage" => 3
 		);
 		
 		$driverData = (object)array(
 				"quote_car_make" => $quotation["carMake"],
 				"quote_car_model" => $quotation["carModel"],
 				"quote_car_com_registered" => $quotation["companyRegistered"],
 				"quote_off_peak" => $quotation["offPeakCar"],
				 "quote_car_reg_no" => $quotation["vehicleRegNo"],
				 "quote_car_value" => $quotation["vehicleValue"],
				 "quote_highest_offence_point" => $quotation["highestOffencePoint"],
				 "quote_offence_point" => $quotation["offencePoint"],
				 "quote_night_parking" => $quotation["nightParking"],
				 "quote_china_extension" => $quotation["chinaExtension"],
				 "quote_cover_type" => $quotation["coverType"],
				 "quote_car_engine_cc" => $quotation["vehicleEngineCc"],
				 "quote_car_body_type" => $quotation["carBodyType"],
 				"quote_car_make_year" => $quotation["carMakeYear"],
 				"quote_car_reg_year" => $quotation["carRegisterYear"],
 				"quote_car_ncd" => $quotation["NCDPoints"],
				 "quote_car_zero_ncd_reason" => $quotation["ncdReason"],
				 "quote_car_other_ncd" => $quotation["otherNCD"],
 				"quote_car_claim_amount" => $quotation["claimAmount"],
 				"quote_car_claim_no" => $quotation["claimsPast3Years"],
 				"quote_car_coe_expire_date" => $quotation["coeExpireDate"],
 				"quote_driver_gender" => implode(",", $quotation["Gender"]),
 				"quote_driver_first_name" => implode(",", $quotation["firstName"]),
 				"quote_driver_last_name" => implode(",", $quotation["lastName"]),
 				"quote_driver_email" => implode(",", $quotation["Email"]),
 				"quote_driver_nric" => implode(",", $quotation["Nric"]),
 				"quote_driver_nric_type" => implode(",", $quotation["icType"]),
 				"quote_driver_contact" => implode(",", $quotation["mobileNo"]),
 				"quote_driver_dob" => implode(",", $quotation["dateOfBirth"]),
 				"quote_driver_exp" => implode(",", $quotation["yearsOfDrivingExp"]),
 				"quote_driver_occ_type" => implode(",", $quotation["occupationType"]),
				 "quote_driver_occ" => implode(",", $quotation["Occupation"]),
				 "quote_driver_industry" => implode(",", $quotation["Industry"]),
 				"quote_driver_marital_status" => implode(",", $quotation["maritalStatus"]),
 				"quote_driver_main_driver_relation" => implode(",", $quotation["relationshipWithMainDriver"]),
 				"quote_driver_demerit_point" => implode(",", $quotation["demeritPoint"]),
 				"quote_driver_claim_no" => implode(",", array($quotation["claimsPast3Years"])),
 				"quote_driver_claim_amount" => implode(",", array($quotation["claimAmount"])),
 				"quote_driver_valid_license" => $quotation["validLicense"],
 				"quote_driver_merit_cert" => $quotation["meritCert"],
 				"quote_driver_current_insurer" => $quotation["currentInsurer"]
		 );
		 
 		
 		if ($quoteId) {
 			$quoteData->id = $quoteId;
 			$driverData->quote_master_id = $quoteId;
 			
 			$sompoData = (object)array("quote_master_id" => $quoteId);
 			
 			$query = $db->getQuery(TRUE)
 			->select("*")
 			->from($this->_motor_driver_info)
 			->where("quote_master_id = " . $db->quote($quoteId));
 			$db->setQuery($query);
 			
 			$driverDbData = $db->loadObject();
 			$driverId = $driverDbData->id;
 			
 			$driverData->id = $driverId;
 			$driverDbData->quote_plan_workshop = $quoteDbData->quote_plan_workshop;
 			$driverDbData->quote_plan_los = $quoteDbData->quote_plan_los;
 			$driverDbData->quote_plan_ncdpro = $quoteDbData->quote_plan_ncdpro;
 			$driverDbData->quote_policy_start_date = $quoteDbData->quote_policy_start_date;
 			
 			if(InsureDIYMotorHelper::validateInputChanged($driverDbData, $quotation, true) && $quoteDbData->quote_pre_quote_change_flag != "true") {
 				$quoteData->quote_pre_quote_change_flag = "false";
 			}
 			else {
 				$quoteData->quote_pre_quote_change_flag = "true";
 			}
 			
			$check = $db->updateObject($this->_motor_quote, $quoteData, "id");
			
			if($check) {
				$check = $db->updateObject($this->_motor_driver_info, $driverData, "id");
				
				//$check = $db->updateObject($this->_motor_quote_sompo, $sompoData, "id");
			}
			else {
				return false;
			}
			
		} else {
			
			$quoteData->quote_created_time = $now;
			
			$check = $db->insertObject($this->_motor_quote, $quoteData);
			
			$quoteId = $db->insertid();
			
			$driverData->quote_master_id = $quoteId;
			
			$check = $db->insertObject($this->_motor_driver_info, $driverData);
		}
		
		if ($quoteId) {
			$quotation["quotationId"] = $quoteId;
			
			$session->set("motor.data", $quotation);
		}

		return true;
	}

	public function step3save($data) {
 		$db = JFactory::getDbo();
		$session = JFactory::getSession();
		
		$customerData = $session->get("motor.data", FALSE);
		
		if(!$customerData) {
			return false;
		}
		
		$customerData["quotationId"] = $data["quotationId"];
		// $customerData["workshop"] = $data["workshop"];
		// $customerData["los"] = $data["los"];
		// $customerData["ncdPro"] = $data["ncdPro"];
		// $customerData["ref_id"] = $data["ref_id"];
		$customerData["plan_id"] = $data["plan_id"];
		$customerData["plan_name"] = $data["plan_name"];
		$customerData["partner_id"] = $data["partner_id"];
		
		$quoteData = (object)array();
		
		$quoteData->id = $customerData["quotationId"];
		// $quoteData->quote_plan_workshop = $customerData["workshop"];
		// $quoteData->quote_plan_los = $customerData["los"];
		// $quoteData->quote_plan_ncdpro = $customerData["ncdPro"];
		$quoteData->quote_policy_selected_plan_code = $customerData["plan_id"];
		$quoteData->quote_policy_selected_partner_code = $customerData["partner_id"];
		$quoteData->quote_process_stage = 4;
		
		$check = $db->updateObject($this->_motor_quote, $quoteData, "id");
		
		$session->set("motor.data", $customerData);
		
		return true;
	}

	public function step4save($data) {

		$session = JFactory::getSession();
		$db = JFactory::getDbo();
		$app = JFactory::getApplication();
		$params = $app->getParams();
		
		$query = $db->getQuery(TRUE)
		->select("id")
		->from($this->_motor_driver_info)
		->where("quote_master_id = " . $db->quote($data["quotationId"]));
		$db->setQuery($query);
		
		$driverId = $db->loadResult();
		
		$quoteData = (object) array(
				"id" => $data["quotationId"],
				"quote_policy_previous_code" => $data["previousPolicyNo"],
				"quote_process_stage" => 5
		);
		
		$driverData = (object)array(
				"id" => $driverId,
				"quote_driver_marital_status" => implode(",",$data["maritalStatus"]),
				"quote_driver_occ_type" => implode(",",$data["occupationType"]),
				"quote_driver_occ" => implode(",",$data["Occupation"]),
				"quote_driver_industry" => implode(",",$data["Industry"]),
				"quote_car_engine_no" => $data["vehicleEngineNo"],
				"quote_driver_main_driver_relation" =>  implode(",",$data["relationshipWithMainDriver"]),
				"quote_car_reg_no" => $data["vehicleRegNo"],
				"quote_car_policy_end" => $data["prevPolicyEnd"],
				"quote_driver_valid_license" => implode(",",$data["drivingLicense"]),
				"quote_driver_driving_test" => implode(",",$data["drivingTest"]),
				"quote_driver_contact" => $data["mobileNo"],
				"quote_driver_address_line_1" => $data["addressOne"],
				"quote_driver_address_line_2" => $data["addressTwo"],
				"quote_driver_claim_no" => implode(",",$data["claimsPast3Years"]),
				"quote_driver_claim_amount" => implode(",",$data["claimAmount"]),
				"quote_offence_point" => implode(",",$data["offencePoint"]),
				"quote_driver_gender" => implode(",",$data["Gender"]),
				"quote_driver_dob" => implode(",",$data["dateOfBirth"]),
				"quote_highest_offence_point" => implode(",",$data["highestOffencePoint"]),
				"quote_car_chassis_no" => $data["vehicleChassisNo"],
				"quote_car_prev_reg_no" => $data["previousRegNo"],
				"quote_driver_to_west_my" => $data["driveToWestMY"],
				"quote_driver_first_name" => implode(",", $data["firstName"]),
				"quote_driver_last_name" => implode(",", $data["lastName"]),
				"quote_driver_nric_type" => implode(",", $data["icType"]),
				"quote_driver_nric" => implode(",", $data["Nric"]),
				"quote_driver_email" => implode(",", $data["Email"]),
				"quote_driver_demerit_point" => implode(",", $data["demeritPoint"]),
				"quote_driver_nationality" => implode(",", $data["driverNationality"]),
				"quote_driver_driving_test" => implode(",", $data["drivingTest"]),
				"quote_driver_is_appliant" => 0,
				"quote_driver_race" => implode(",", $data["driverRace"]),
				"quote_driver_address_block_no" => $data["blockNo"],
				"quote_driver_address_street_name" => $data["streetName"],
				"quote_driver_address_unit_no" => $data["unitNo"],
				"quote_driver_address_building_name" => $data["buildingName"],
				//"quote_driver_state" => $data["state"],
				//"quote_driver_city" => $data["city"],
				"quote_driver_postcode" => $data["zipCode"],
				"quote_driver_country" => $data["country"],
				"quote_night_parking" => $data["nightParking"],
				"quote_driver_loan" => $data["loan"],
				"quote_driver_loan_company" => $data["loanCompany"]
		);
		
		$check = $db->updateObject($this->_motor_driver_info, $driverData, "id");
		$check = $db->updateObject($this->_motor_quote, $quoteData, "id");
		
		$session->set("motor.data", $data);
		$session->set("motor.result", "");

		if($data["partnerId"] == "sompo") {
			if(!InsureDIYMotorHelper::sompoEncryptData("check")) {
				$result["success"] = false;
				
				$result["error_message"] = array(
						(object)array("errorDesc" => "Failed to communicate with Sompo")
				);
				
				$session->set("motor.result", $result);
				
				$errorData = "Sompo Certicate file not found at step4save!";
				
				$db->insertObject("#__insure_motor_policy_error", $errorData);
				
				return false;
			}
			
			$data["loanCompany"] = $this->getLoanCompanyName($data["loanCompany"]);
			
			$ssoToken = InsureDIYMotorHelper::getSsoToken();
			$requestObj = InsureDIYMotorHelper::sompoProposalRequestData($data);

			$ch = curl_init();

			curl_setopt($ch, CURLOPT_URL, $params->get('sompoApiLink', "")."/getProposal");
			//curl_setopt($ch, CURLOPT_VERBOSE, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'Accept: application/json',
					'Data-type: application/json',
					'Authorization: bearer '.$ssoToken->access_token
			)
					);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestObj));
			
			$server_output = curl_exec ($ch);
			$server_output = json_decode($server_output);
			$errorMsg = curl_error($ch);
			
			curl_close($ch);
			
			$result= array();
			
			if($server_output) {
				$responseData = $server_output->serviceReplyObject;
				
				if(isset($responseData->CURRENTPOLICY->PLAN_LIST)) {
					$policy = $responseData->CURRENTPOLICY;
					$quoteRefId = $responseData->CURRENTPOLICY->APPQUOTEPOLICYREFID;
					$proposalRefId = $responseData->CURRENTPOLICY->POL_AGENT_REF_NO;
					
					$data["quote_ref_id"] = $quoteRefId;
					$data["proposal_ref_id"] = $proposalRefId;
					
					$session->set("motor.data", $data);
					
					$query = $db->getQuery(TRUE)
					->select("*")
					->from($this->_motor_quote_sompo)
					->where("quote_master_id = " . $db->quote($data["quotationId"]));
					$db->setQuery($query);
					
					$sompoQuote = $db->loadObject();
					
					$key = 0;
					
					foreach($policy->PLAN_LIST as $index => $item) {
						if($item->PLANCODE== $data["plan_id"]) {
							$key = $index;
						}
					}
					
					$planRisk = explode("#", $sompoQuote->sompo_plan_risk);
					
					$planRisk[$key] = json_encode($policy->PLAN_LIST[0]->PLANXRISKLIST);
					
					$sompoQuoteData = (object)array(
							"id" => $sompoQuote->id,
							"sompo_pol_agent_ref_id" => $proposalRefId,
							"sompo_app_quote_ref_id" => $quoteRefId,
							"sompo_plan_risk" => $planRisk
					);
					
					$check = $db->updateObject($this->_motor_quote_sompo, $sompoQuoteData, "id");
				}
				
				if(isset($responseData->BUSINESSVALIDATIONMESSAGE)) {
					$error = $responseData->BUSINESSVALIDATIONMESSAGE;
					
					$result["success"] = false;
					
					$result["error_message"] = $error;
					
					$session->set("motor.result", $result);
					
					$errorData = (object) array(
							"quote_master_id" => $data["quotationId"],
							"policy_request_data" => json_encode($requestObj),
							"policy_error_msg" => json_encode($error)
					);
					
					$db->insertObject("#__insure_motor_policy_error", $errorData);
					
					return false;
				}
			}
			else {
				$result["success"] = false;
				
				$result["error_message"] = $errorMsg;
				
				$session->set("motor.result", $result);
				
				$errorData = (object) array(
						"quote_master_id" => $data["quotationId"],
						"policy_request_data" => json_encode($requestObj),
						"policy_error_msg" => json_encode($errorMsg)
				);
				
				$db->insertObject("#__insure_motor_policy_error", $errorData);
				
				return false;
			}
		}
		
		if($data["partnerId"] == "msig") {
			$query = $db->getQuery(TRUE)
			->select("*")
			->from($this->_motor_quote_msig)
			->where("quote_master_id = " . $db->quote($data["quotationId"]));
			$db->setQuery($query);
			
			$msigQuote = $db->loadObject();
			$msigQuoteId = $msigQuote->id;
			
			
			//$data["ref_id"] = $msigQuote->msig_app_ref_id;
			
				$accessToken = InsureDIYMotorHelper::getMSIGAccessToken();
				
				if(!$accessToken) {
					return false;
				}
				
				$requestObj = InsureDIYMotorHelper::msigApplicationRequestData($data);
				
				$ch = curl_init();
				
				curl_setopt($ch, CURLOPT_URL, $params->get('msigApiLink', "")."/products/QMX/applications");
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
						'Content-Type: application/json',
						'Accept: application/json',
						'Data-type: application/json',
						'Authorization: bearer '.$accessToken->access_token
				)
						);
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestObj));
				
				$server_output = curl_exec ($ch);
				$server_output = json_decode($server_output);
				$errorMsg = curl_error($ch);
				
				curl_close($ch);
				
				$result= array();
				
				if($server_output) {
					if(isset($server_output->applicationReference)) {
						$ref_id = $server_output->applicationReference;
						
						$data["quote_ref_id"] = $ref_id;
						$data["proposal_ref_id"] = $ref_id;
						
						$session->set("motor.data", $data);
						
						$msigQuoteData = (object)array(
								"id" => $msigQuoteId,
								"msig_app_ref_id" => $ref_id
						);
						
						$check = $db->updateObject($this->_motor_quote_msig, $msigQuoteData, "id");
						
						if(!$check) {
							$result["success"] = false;
							
							$result["error_message"] = $msigQuoteData;
							$result["request_object"] = $requestObj;
							
							$session->set("motor.result", $result);
							
							$errorData = (object) array(
									"quote_master_id" => $data["quotationId"],
									"policy_request_data" => json_encode($requestObj),
									"policy_error_msg" => json_encode($msigQuoteData)
							);
							
							$db->insertObject("#__insure_motor_policy_error", $errorData);
							
							return false;
						}
					}
					
					if(isset($server_output->errorCode)) {
						$error = $server_output->fieldErrors;
						
						$result["success"] = false;
						
						$error["fieldErrors"] = $server_output->errorDesc;
						
						$result["error_message"] = $error;
						$result["request_object"] = $requestObj;
						//$result["error_message"] = $requestObj;
						
						$session->set("motor.result", $result);
						
						$errorData = (object) array(
								"quote_master_id" => $data["quotationId"],
								"policy_request_data" => json_encode($requestObj),
								"policy_error_msg" => json_encode($server_output)
						);
						
						$db->insertObject("#__insure_motor_policy_error", $errorData);
						
						return false;
					}
				}
				else {
					$result["success"] = false;
					
					$result["error_message"] = $errorMsg;
					
					$session->set("motor.result", $result);
					
					$errorData = (object) array(
							"quote_master_id" => $data["quotationId"],
							"policy_request_data" => json_encode($requestObj),
							"policy_error_msg" => json_encode($errorMsg)
					);
					
					$db->insertObject("#__insure_motor_policy_error", $errorData);
					
					return false;
				}
		}
		
		return true;
	}
	
	public function renewalcase($data) {
		$session = JFactory::getSession();
		$db = JFactory::getDbo();
		
		$query = $db->getQuery(TRUE)
		->select("id")
		->from($this->_motor_quote_renewal)
		->where("quote_master_id = " . $db->quote($data["quotationId"]));
		$db->setQuery($query);
		
		$renewalId = $db->loadResult();
		
		if(!$renewalId) {
			$quoteData = (object) array(
				"quote_master_id" => $data["quotationId"],
				"quote_car_variant" => "",
				"renewal_plan_id" => "",
				"renewal_plan_name" => "",
				"renewal_plan_premium_total" => "",
				"renewal_plan_excess_standard" => "",
				"renewal_plan_excess_young" => "",
				"renewal_plan_status" => "pending",
				"renewal_plan_partner" => $data["partner_id"]
			);
			
			$check = $db->insertObject($this->_motor_quote_renewal, $quoteData);
			
			if($check) {
				return true;
			}
			else {
				return false;
			}
		}
		
		return true;
	}
	
	public function createPolicy($request_id) {	
		$session = JFactory::getSession();
		$data = $this->getDataByRequestId($request_id);
		$db = JFactory::getDbo();
		$params = JComponentHelper::getParams('com_insurediymotor');
		
		if($data == "completed") {
			return false;
		}
		
		if($data["partnerId"] == "sompo") {
			if(!InsureDIYMotorHelper::sompoEncryptData("check")) {
				$result["success"] = false;
				
				$result["error_message"] = array(
						(object)array("errorDesc" => "Failed to communicate with Sompo")
				);
				
				$session->set("motor.result", $result);
				
				$errorData = "Sompo Certicate file not found at createPolicy!";
				
				$db->insertObject("#__insure_motor_policy_error", $errorData);
				
				return false;
			}
			
			$ssoToken = InsureDIYMotorHelper::getSsoToken();
			
			$data["loanCompany"] = $this->getLoanCompanyName($data["loanCompany"]);
			
			$requestObj = InsureDIYMotorHelper::sompoPolicyRequestData($data);
			
			$ch = curl_init();
			
			curl_setopt($ch, CURLOPT_URL, $params->get('sompoApiLink', "")."/createPolicy");
			//curl_setopt($ch, CURLOPT_VERBOSE, true);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array(
					'Content-Type: application/json',
					'Accept: application/json',
					'Data-type: application/json',
					'Authorization: bearer '.$ssoToken->access_token
			)
					);
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestObj));
			
			$server_output = curl_exec ($ch);
			$server_output = json_decode($server_output);
			$errorMsg = curl_error($ch);
			
			curl_close($ch);
			
			$result= array();
			
			if($server_output) {
				$responseData = $server_output->serviceReplyObject;

				if(isset($responseData->ACKNOWLEGMENT_ID)) {
					if($data["sompo_agent_ref_id"] == $responseData->PROPOSAL_NO) {
						$sompoDataArr = (object) array(
								"id" => $data["sompoQuoteId"],
								"sompo_pol_ack_id" => $responseData->ACKNOWLEGMENT_ID
						);
						
						$check = $db->updateObject($this->_motor_quote_sompo, $sompoDataArr, "id");
						
						$date = new DateTime('now');
						$date->setTimezone(new DateTimeZone('Asia/Singapore'));
						$now = $date->format("Y-m-d\TH:i:s.u+08:00");
						
						$quoteData = (object) array(
								"id" => $data["quotationId"],
								"quote_updated_time" => $now,
								"quote_process_stage" => "completed"
						);
						
						$check = $db->updateObject("#__insure_motor_quote_master", $quoteData, "id");
						
						$result["success"] = true;
						$result["data"] =  (object) array(
								"transactionId" => $data["transactionId"],
								"acknowledgementId" => $responseData->ACKNOWLEGMENT_ID,
								"proposalId" => $responseData->PROPOSAL_NO
						);
						
						$session->set("motor.result", $result);
						$session->set("motor.data", "");
						
						return true;
					}
				}
				
				if(isset($responseData->BUSINESSVALIDATIONMESSAGE)) {
					$error = $responseData->BUSINESSVALIDATIONMESSAGE;
					
					$result["success"] = false;
					
					$result["error_message"] = $error;
					
					$session->set("motor.result", $result);
					
					$errorData = (object) array(
							"quote_master_id" => $data["quotationId"],
							"policy_request_data" => json_encode($requestObj),
							"policy_error_msg" => json_encode($error)
					);
					
					$db->insertObject("#__insure_motor_policy_error", $errorData);
					
					return false;
				}
				
				$error = $server_output;
				
				$result["success"] = false;
				
				$result["error_message"] = $error;
				
				$session->set("motor.result", $result);
				
				$errorData = (object) array(
						"quote_master_id" => $data["quotationId"],
						"policy_request_data" => json_encode($requestObj),
						"policy_error_msg" => json_encode($error)
				);
				
				$db->insertObject("#__insure_motor_policy_error", $errorData);
				
				return false;
			}
			else {
				$result["success"] = false;
				
				$result["error_message"] = $errorMsg;
				
				$session->set("motor.result", $result);
				
				$errorData = (object) array(
						"quote_master_id" => $data["quotationId"],
						"policy_request_data" => json_encode($requestObj),
						"policy_error_msg" => json_encode($errorMsg)
				);
				
				$db->insertObject("#__insure_motor_policy_error", $errorData);
				
				return false;
			}
		}
		
		if($data["partnerId"] == "msig") {
			if($data["transactionId"] != "") {
				$accessToken = InsureDIYMotorHelper::getMSIGAccessToken();
				
				if(!$accessToken) {
					return false;
				}
				
				$requestObj = InsureDIYMotorHelper::msigPolicyRequestData($data);
				
				$ch = curl_init();
				
				curl_setopt($ch, CURLOPT_URL, $params->get('msigApiLink', "")."/products/QMX/policies");
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_POST, 1);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
						'Content-Type: application/json',
						'Accept: application/json',
						'Data-type: application/json',
						'Authorization: bearer '.$accessToken->access_token
				)
						);
				curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestObj));
				
				$server_output = curl_exec ($ch);
				$server_output = json_decode($server_output);
				$errorMsg = curl_error($ch);
				
				curl_close($ch);
				
				$result= array();

// 				$date = new DateTime('now');
// 				$date->setTimezone(new DateTimeZone('Asia/Singapore'));
// 				$now = $date->format("Y-m-d\TH:i:s.u+08:00");
// 				//Something to write to txt log
// 				$log  = $now. ": SUCCESS".PHP_EOL.
// 				"-------------------------".PHP_EOL.
// 				"RESULT = ".json_encode($requestObj).PHP_EOL.
// 				"POST = ".json_encode($server_output).PHP_EOL;
// 				//Save string to log, use FILE_APPEND to append.
// 				file_put_contents('./checkkk.log', $log, FILE_APPEND);
				
				if($server_output) {
					if(isset($server_output->policyNumber)) {
						$pol_id = $server_output->policyNumber;
						
						$date = new DateTime('now');
						$date->setTimezone(new DateTimeZone('Asia/Singapore'));
						$now = $date->format("Y-m-d\TH:i:s.u+08:00");
						
						$quoteData = (object) array(
								"id" => $data["quotationId"],
								"quote_policy_code" => $pol_id,
								"quote_updated_time" => $now,
								"quote_process_stage" => "completed"
						);
						
						$check = $db->updateObject("#__insure_motor_quote_master", $quoteData, "id");
						
						$result["success"] = true;
						$result["data"] =  (object) array(
								"transactionId" => $data["transactionId"],
								"proposalId" => $pol_id
						);
						
						$session->set("motor.result", $result);
						$session->set("motor.data", "");
						
						return true;
					}
					
					if(isset($server_output->errorCode)) {
						$error = $server_output->fieldErrors;
						
						$result["success"] = false;
						
						$error["fieldErrors"] = $server_output->errorDesc;
						
						$result["error_message"] = $error;
						//$result["error_message"] = $requestObj;
						
						$errorData = (object) array(
								"quote_master_id" => $data["quotationId"],
								"policy_request_data" => json_encode($requestObj),
								"policy_error_msg" => json_encode($server_output)
						);
						
						$db->insertObject("#__insure_motor_policy_error", $errorData);
						
						return false;
					}
				}
				else {
					$result["success"] = false;
					
					$result["error_message"] = $errorMsg;
					
					$session->set("motor.result", $result);
					
					$errorData = (object) array(
							"quote_master_id" => $data["quotationId"],
							"policy_request_data" => json_encode($requestObj),
							"policy_error_msg" => $params->get('msigApiLink', "")."/products/QMX/policies"
					);
					
					$db->insertObject("#__insure_motor_policy_error", $errorData);
					
					return false;
				}
			}
		}
		
		if($data["partnerId"] == "hla") {
			if($data["transactionId"] != "") {
				if(InsureDIYMotorHelper::requestHlaQuotations($data, true)) {
					$date = new DateTime('now');
					$date->setTimezone(new DateTimeZone('Asia/Singapore'));
					$now = $date->format("Y-m-d\TH:i:s.u+08:00");
					
					$quoteData = (object) array(
							"id" => $data["quotationId"],
							"quote_updated_time" => $now,
							"quote_process_stage" => "completed"
					);
					
					$check = $db->updateObject("#__insure_motor_quote_master", $quoteData, "id");
					
					$result["success"] = true;
					$result["data"] =  (object) array(
							"transactionId" => $data["transactionId"]
					);
					
					$session->set("motor.result", $result);
					$session->set("motor.data", "");
					
					return true;
				}
				else {
					$result["success"] = false;
					
					$result["error_message"] = "Failed to save quote on HLA";
					
					$session->set("motor.result", $result);
					
					$errorData = (object) array(
							"quote_master_id" => $data["quotationId"],
							"policy_request_data" => json_encode($data),
							"policy_error_msg" => json_encode("Failed to save quote on HLA")
					);
					
					$db->insertObject("#__insure_motor_policy_error", $errorData);
					
					return false;
				}
			}
		}
		
		$session->set("motor.data", "");
		
		return true;
	}

	public function saveContactDetails($qid) {
		$contact = $this->getContactDetails();
		$db = JFactory::getDbo();
		$contact['id'] = $qid;
		return $db->updateObject($this->_tb_quotation, MyHelper::array2jObject($contact), "id");
	}

	public function deleteAllDrivers($quotation_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->delete($this->_tb_quotation_traveller)
				->where("quotation_id = " . $db->quote($quotation_id));
		$db->setQuery($query);
		return $db->execute();
	}

	public function getPlan($plan_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from($this->_tb_plan)
				->where("plan_id = " . $db->quote($plan_id));
		$db->setQuery($query);
		return $db->loadObject();
	}

	public function updateStage($quotation_id, $stage) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->update($this->_tb_quotation)
				->set("quote_stage = " . $stage)
				->where("id = " . $db->quote($quotation_id));
		$db->setQuery($query);
		$check = $db->execute();
		return $check;
	}

	public function getStep($quotation_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("quote_stage")
				->from($this->_tb_quotation)
				->where("id = " . $db->quote($quotation_id));
		$db->setQuery($query);
		$check = $db->loadResult();
		return $check;
	}

	public function changeToAnuual($quotation_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->update($this->_tb_quotation)
				->set("trip_type = " . $db->quote("AT"))
				->where("id = " . $db->quote($quotation_id));
		$db->setQuery($query);
		$check = $db->execute();
		return $check;
	}

	public function getTripType($quotation_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("trip_type")
				->from($this->_tb_quotation)
				->where("id = " . $db->quote($quotation_id));
		$db->setQuery($query);
		$check = $db->loadResult();
		return $check;
	}

	public function savePDFPaths($quotation_id, $array = array(), $client = TRUE) {
		if (empty($array)) {
			return;
		}
		$toJson = new stdClass();
		foreach ($array as $k => $v) {
			if (is_array($v)) {
				$toJson->$k = implode(" | ", $v);
			} else {
				$toJson->$k = $v;
			}
		}
		$db = JFactory::getDBO();
		if ($client) {
			$query = " UPDATE " . $this->_tb_quotation . " SET `file_json_pdfpath` = " . $db->Quote(json_encode($toJson), false) . " WHERE id = '$db->quote($quotation_id)' ";
		} else {
			$query = " UPDATE " . $this->_tb_quotation . " SET `file_json_pdfpath2` = " . $db->Quote(json_encode($toJson), false) . " WHERE id = '$db->quote($quotation_id)' ";
		}
		$db->setQuery($query);
		$db->query();
	}

	// Mail Stuffs - start
	public function sendEmails($pdfarrs, $client = TRUE, $id = FALSE) {
		$mailer = JFactory::getMailer();
		$params = JComponentHelper::getParams('com_insurediymotor');
		$myInfo = $this->getMyInfo($id);
		$legit = $this->checkBlacklist($myInfo);
		$maildata = $this->getMailData($myInfo);
		$emails = $this->getMails($params, $legit, $myInfo->email);
		$emailbodies = $this->getEmailBodies($params, $maildata);

		$mode = TRUE;
		
		if($pdfarrs){
			foreach ($pdfarrs as $pdfarr) {
				$attachments = $this->prepareAttachments($pdfarr, $myInfo->id);
				if ($client && $legit) {
					$mailer->ClearAttachments();
					$mailer->ClearAllRecipients();
					$mailer->sendMail($emails['from'], $emails['fromName'], $emails['recipient'], $emails['subject'], $emailbodies['ce_customer'], $mode, NULL, $emails['bcc'], $attachments);
				} else {
					$mailer->ClearAttachments();
					$mailer->ClearAllRecipients();
					$mailer->sendMail($emails['from'], $emails['fromName'], $emails['service_recipient'], $emails['subject'], $emailbodies['ce_service'], $mode, NULL, $emails['bcc'], $attachments);
				}
			}
		}
		else {
			$mailer->ClearAttachments();
			$mailer->ClearAllRecipients();
			$mailer->sendMail($emails['from'], $emails['fromName'], $emails['service_recipient'], $emails['subject'], $emailbodies['cer_service'], $mode);
		}
	}

	public function prepareAttachments($pdfarr, $id) {
		$temp = array();
		foreach ($pdfarr as $pdf) {
			$temp[] = MyHelper::getDeepPath(QUOTATION_PDF_SAVE_PATH, $id) . $pdf;
		}
		return $temp;
	}

	public function getEmailBodies($params, $data) {
		$array = array();
		$array['cover_letter'] = InsureDIYHelper::replaceVariables($params->get("clformat"), $data);
		$array['ce_customer'] = InsureDIYHelper::replaceVariables($params->get("cecformat"), $data);
		$array['ce_service'] = InsureDIYHelper::replaceVariables($params->get("cesformat"), $data);
		$array['cer_service'] = InsureDIYHelper::replaceVariables($params->get("cerformat"), $data);
		$array['sup_service'] = InsureDIYHelper::replaceVariables($params->get("supformat"), $data);
		return $array;
	}

	public function getMails($params, $legit, $user_email) {
		$confg = JFactory::getConfig();
		$array = array();
		if ($legit) {
			$array['recipient'] = $user_email;
			$sales_email = $params->get('sales_email');
			$array['bcc'] = array("0" => $sales_email);
			$array['subject'] = 'InsureDIY - Application Form';
		} else {
			$array['recipient'] = $params->get('admin_email');
			$sales_email = $params->get('sales_email');
			$array['bcc'] = array("0" => $sales_email);
			$array['subject'] = 'InsureDIY - Application Form[Blacklist]';
		}
		$array['service_recipient'] = $params->get('service_email', "");
		$array['from'] = $confg->get("mailfrom");
		$array['fromName'] = $confg->get("fromname");
		return $array;
	}

	public function getMailData($myInfo) {
		$session = JFactory::getSession();
		$data = array();

		$data['first_name'] = $myInfo->contact_firstname;
		$data['last_name'] = $myInfo->contact_lastname;
		$data['order_no'] = $myInfo->unique_order_no;
		$data['product'] = $myInfo->plan_name;
		$data['insurer'] = strtoupper($myInfo->insurer_code);
		$data['start_date'] = date("d-M-Y", strtotime($myInfo->start_date));
		$data['address'] = $myInfo->address;
		$data['contact_number'] = $myInfo->contact_number;

		return $data;
	}

	public function checkBlacklist($data) {
		$name = $data->contact_firstname . " " . $data->contact_lastname;
//		$nationality = $data->contact_country;
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$query->select("count(*)");
		$query->from("#__insure_blacklist");
		$query->where("UPPER(name) = UPPER(" . $db->quote($name) . ")");
//		$query->where("UPPER(name) = UPPER(" . $db->quote($name) . ") AND nationality = " . $db->quote($nationality));
		$db->setQuery($query);
		$result = $db->loadResult();
		if ($result > 0) {
			return FALSE;
		}
		return TRUE;
	}

	// Mail Stuffs - end

	public function createPoints($quotation_id) {
		$quotation = InsureDIYMotorHelper::getQuotation($quotation_id, FALSE);
		$plan = $quotation['selected_plan'];
		$total_pur_points = $plan->pur_points;
		$total_ref_points = $plan->ref_points;
		if (InsureDIYHelper::checkReferralWithQuotation($quotation)) {
			InsureDIYHelper::updateReferredByWithQuotation($quotation);
			InsureDIYHelper::createReferralWithQuotation($quotation, "motor." . $quotation_id, $quotation['unique_order_no'], $total_ref_points, sprintf(JText::_("REFERRAL_POINT_RECORD_DESCRIPTION"), $quotation['email']));
		}
		$desc = sprintf(JText::_("PURCHASE_POINT_RECORD_DESCRIPTION"), $quotation['unique_order_no']);
		$percent = InsureDIYHelper::getPromoCode($quotation['promo_code']);
		if ($percent) {
			$total_pur_points = $total_pur_points * (100 + $percent) / 100;
			InsureDIYHelper::usePromoCode($quotation['promo_code']);
			$desc .= ". With promo code " . $quotation['promo_code'];
		}
		InsureDIYHelper::createPoint($quotation['user_id'], $total_pur_points, "motor." . $quotation_id, $quotation['unique_order_no'], "", $desc, 1);
		return TRUE;
	}

	public function createPolicyRecord($quotation_id) {
		$db = JFactory::getDbo();
		$quotation = InsureDIYMotorHelper::getQuotation($quotation_id, FALSE);
		$obj = new stdClass();
		$obj->user_id = $quotation['user_id'];
		$obj->type = "5"; // motor is 4
		$obj->insurer = $quotation['selected_plan']->insurer_code;
		$obj->currency = 344; // HK$
		$obj->status = 2; // Set as pending. 1 => active, 0 => inactive, 2 => pending
		$obj->quotation_id = $quotation_id;
		$db->insertObject("#__insure_policies", $obj);
	}

	public function back($quotation_id) {
		$db = JFactory::getDBO();
		$user = JFactory::getUser();
		$current_stage = InsureDIYMotorHelper::getCurrentStage($quotation_id);
		$backables = array(2 => 1, 3 => 2, 4 => 3, 5 => 4);
		$backstage = (in_array($current_stage, array_keys($backables))) ? $backables[$current_stage] : FALSE;
		if ($backstage && !$user->get('guest')) {
			$query = $db->getQuery(TRUE)
					->update($this->_tb_quotation)
					->set("quote_stage = " . $backstage)
					->where("id = " . $db->quote($quotation_id))
					->where("quote_stage > 1")
					->where("user_id = " . $db->quote($user->id));
			$result = $db->setQuery($query)->execute();
		}

		return $backstage;
	}
	
	public function getPaymentData($partner, $premium, $quotationId) {
		$db = JFactory::getDBO();
		$app = JFactory::getApplication();
		$params = $app->getParams();
		
		if($partner == "sompo") {
			//$request_id = InsureDIYMotorHelper::fullGuid();
			$request_time_stamp = gmdate("YmdHis");
			//$request_id = $partner["quote_ref_id"];
			$merchant_account_id = $params->get('wcMerchantId', "");
			$transaction_type = 'purchase';
			$requested_amount = number_format($premium, 2, '.', '');
			$request_amount_currency = 'SGD';
			$redirect_url = JURI::root(). 'component/insurediymotor/form/sompoPaymentReturn';
			$cancel_url = JURI::root(). 'component/insurediymotor/form/sompoPaymentIPN';
			$ip_address = $_SERVER['REMOTE_ADDR'];
			$secret_key = $params->get('wcSecretKey', "");
			
			$query = $db->getQuery(TRUE)
			->select("*")
			->from($this->_motor_quote)
			->where("id = " . $db->quote($quotationId) . " AND " . "quote_deleted_time IS NULL");
			$db->setQuery($query);
			$quoteData = $db->loadObject();
			$requestId = $quoteData->quote_payment_request_id;
			
			$query = $db->getQuery(TRUE)
			->select("*")
			->from($this->_motor_quote_sompo)
			->where("quote_master_id = " . $db->quote($quoteData->id));
			$db->setQuery($query);
			$sompoData= $db->loadObject();
			$request_id = $sompoData->sompo_pol_agent_ref_id;
			 
			if($requestId != "" && $quoteData->quote_payment_txn_id == "") {
				$result = $this->validatePaymentStatus($requestId);
				
				if($result) {
					$check = $this->createPolicy($requestId);
					
					if($check) {
						return true;
					}
					else {
						return false;
					}
				}
			}

			$request_signature = hash('sha256', trim($request_time_stamp . $request_id . $merchant_account_id . $transaction_type . $requested_amount . $request_amount_currency . $redirect_url . $ip_address . $secret_key));
			
			$paymentData = array(
					"target_url" => $params->get('wcHostedUrl', ""),
					"request_signature" => $request_signature,
					"request_time_stamp" => $request_time_stamp,
					"request_id" => $request_id,
					"merchant_account_id" => $merchant_account_id,
					"transaction_type" => $transaction_type,
					"requested_amount" => $requested_amount,
					"request_amount_currency" => $request_amount_currency,
					"redirect_url" => $redirect_url,
					"notification_url_1" => $cancel_url,
					"ip_address" => $ip_address
					//"string" => $request_time_stamp . $request_id . $merchant_account_id . $transaction_type . $requested_amount . $request_amount_currency . $redirect_url . $ip_address . $secret_key,
					//"key" => $secret_key
			);

			$quoteData = (object) array(
					"id" => $quotationId,
					"quote_payment_stage" => "pending",
					"quote_payment_request_id" => $request_id,
					"quote_payment_amount" => $premium
			);
			
			$check = $db->updateObject($this->_motor_quote, $quoteData, "id");
			
			return $paymentData;
		}
		else if($partner == "msig") {
			$request_id = InsureDIYMotorHelper::guid();
			$merchant_account_id = $params->get('merchantId', "12105413");
			$transaction_type = 'N';
			$requested_amount = (number_format($premium, 2, '.', ''));
			$redirect_url = JURI::root(). 'component/insurediymotor/form/msigPaymentSuccess';
			$cancel_url = JURI::root(). 'component/insurediymotor/form/msigPaymentCancel';
			$fail_url = JURI::root(). 'component/insurediymotor/form/msigPaymentFail';
			
			$query = $db->getQuery(TRUE)
			->select("*")
			->from($this->_motor_quote)
			->where("id = " . $db->quote($quotationId) . " AND " . "quote_deleted_time IS NULL");
			$db->setQuery($query);
			$quoteData = $db->loadObject();
			$requestId= $quoteData->quote_payment_request_id;
			
			if($requestId != "" && $quoteData->quote_payment_txn_id == "") {
				//$result = $this->validateAsiaPayPaymentStatus($merchant_account_id, $requestId);
				
				// 				if($result) {
				// 					$check = $this->createPolicy($requestId);
									
				// 					if($check) {
				// 						return true;
				// 					}
				// 					else {
				// 						return false;
				// 					}
									
				// 				}
			}
			
			$paymentData = array(
					"target_url" => $params->get('pdUrl', "https://test.paydollar.com/b2cDemo/eng/payment/payForm.jsp"),
					"request_id" => $request_id,
					"merchant_account_id" => $merchant_account_id,
					"transaction_type" => $transaction_type,
					"requested_amount" => $requested_amount,
					"request_amount_currency" => $params->get('currCode', "702"),
					"mps_mode" => "NIL",
					"redirect_url" => $redirect_url,
					"cancel_url" => $cancel_url,
					"failed_url" => $fail_url,
					"payment_method" => "ALL",
					"remark" => "motor",
					"redirect" => 3,
					"language" => "E",
					
			);			
			
			$quoteData = (object) array(
					"id" => $quotationId,
					"quote_payment_stage" => "pending",
					"quote_payment_request_id" => $request_id,
					"quote_payment_amount" => $requested_amount
			);
			
			$check = $db->updateObject($this->_motor_quote, $quoteData, "id");
			
			return $paymentData;
		}
		else if($partner == "hla") {
			$request_id = InsureDIYMotorHelper::guid();
			$merchant_account_id = $params->get('merchantId', "12105413");
			$transaction_type = 'N';
			$requested_amount = (number_format($premium, 2, '.', ''));
			$redirect_url = JURI::root(). 'component/insurediymotor/form/hlaPaymentSuccess';
			$cancel_url = JURI::root(). 'component/insurediymotor/form/hlaPaymentCancel';
			$fail_url = JURI::root(). 'component/insurediymotor/form/hlaPaymentFail';
			
			$query = $db->getQuery(TRUE)
			->select("*")
			->from($this->_motor_quote)
			->where("id = " . $db->quote($quotationId) . " AND " . "quote_deleted_time IS NULL");
			$db->setQuery($query);
			$quoteData = $db->loadObject();
			$requestId= $quoteData->quote_payment_request_id;
			
			if($requestId != "" && $quoteData->quote_payment_txn_id == "") {
				//$result = $this->validateAsiaPayPaymentStatus($merchant_account_id, $requestId);
				
				// 				if($result) {
				// 					$check = $this->createPolicy($requestId);
				
				// 					if($check) {
				// 						return true;
				// 					}
				// 					else {
				// 						return false;
				// 					}
					
				// 				}
			}
				
			$paymentData = array(
					"target_url" => $params->get('pdUrl', "https://test.paydollar.com/b2cDemo/eng/payment/payForm.jsp"),
					"request_id" => $request_id,
					"merchant_account_id" => $merchant_account_id,
					"transaction_type" => $transaction_type,
					"requested_amount" => $requested_amount,
					"request_amount_currency" => $params->get('currCode', "702"),
					"mps_mode" => "NIL",
					"redirect_url" => $redirect_url,
					"cancel_url" => $cancel_url,
					"failed_url" => $fail_url,
					"payment_method" => "ALL",
					"remark" => "motor",
					"redirect" => 3,
					"language" => "E",
						
			);
				
			$quoteData = (object) array(
					"id" => $quotationId,
					"quote_payment_stage" => "pending",
					"quote_payment_request_id" => $request_id,
					"quote_payment_amount" => $requested_amount
			);
				
			$check = $db->updateObject($this->_motor_quote, $quoteData, "id");
				
			return $paymentData;
		}
		else if($partner == "aig") {
			$request_id = InsureDIYMotorHelper::guid();
			$merchant_account_id = $params->get('merchantId', "88117268");
			$transaction_type = 'N';
			$requested_amount = (number_format($premium, 2, '.', ''));
			$redirect_url = JURI::root(). 'component/insurediymotor/form/aigPaymentSuccess';
			$cancel_url = JURI::root(). 'component/insurediymotor/form/aigPaymentCancel';
			$fail_url = JURI::root(). 'component/insurediymotor/form/aigPaymentFail';
			
			$query = $db->getQuery(TRUE)
			->select("*")
			->from($this->_motor_quote)
			->where("id = " . $db->quote($quotationId) . " AND " . "quote_deleted_time IS NULL");
			$db->setQuery($query);
			$quoteData = $db->loadObject();
			$requestId= $quoteData->quote_payment_request_id;
			
			if($requestId != "" && $quoteData->quote_payment_txn_id == "") {
				//$result = $this->validateAsiaPayPaymentStatus($merchant_account_id, $requestId);
				
				// 				if($result) {
				// 					$check = $this->createPolicy($requestId);
				
				// 					if($check) {
				// 						return true;
				// 					}
				// 					else {
				// 						return false;
				// 					}
					
				// 				}
			}

			$hash = InsureDIYMotorHelper::generatePaymentSecureHash($params->get('merchantId'), $request_id, $params->get('currCode', '344'), $requested_amount, $transaction_type, $params->get('hashCode'));
			$paymentData = array(
					"target_url" => $params->get('pdUrl', ""),
					"request_id" => $request_id,
					"merchant_account_id" => $params->get('merchantId', FALSE),
					"transaction_type" => $transaction_type,
					"requested_amount" => $requested_amount,
					"request_amount_currency" => $params->get('currCode', '344'),
					"mps_mode" => $params->get('mpsMode', 'NIL'),
					"redirect_url" => $redirect_url,
					"cancel_url" => $cancel_url,
					"failed_url" => $fail_url,
					"payment_method" => "ALL",
					"remark" => "motor",
					"redirect" => 3,
					"language" => "E",
					"secureHash" => $hash,
						
			);
				
			$quoteData = (object) array(
					"id" => $quotationId,
					"quote_payment_stage" => "pending",
					"quote_payment_request_id" => $request_id,
					"quote_payment_amount" => $requested_amount
			);
				
			$check = $db->updateObject($this->_motor_quote, $quoteData, "id");
				
			return $paymentData;
		}
		else if($partner == "aw") {
			$request_id = InsureDIYMotorHelper::guid();
			$merchant_account_id = $params->get('merchantId', "88117268");
			$transaction_type = 'N';
			$requested_amount = (number_format($premium, 2, '.', ''));
			$redirect_url = JURI::root(). 'component/insurediymotor/form/awPaymentSuccess';
			$cancel_url = JURI::root(). 'component/insurediymotor/form/awPaymentCancel';
			$fail_url = JURI::root(). 'component/insurediymotor/form/awPaymentFail';
			
			$query = $db->getQuery(TRUE)
			->select("*")
			->from($this->_motor_quote)
			->where("id = " . $db->quote($quotationId) . " AND " . "quote_deleted_time IS NULL");
			$db->setQuery($query);
			$quoteData = $db->loadObject();
			$requestId= $quoteData->quote_payment_request_id;
			
			if($requestId != "" && $quoteData->quote_payment_txn_id == "") {
				//$result = $this->validateAsiaPayPaymentStatus($merchant_account_id, $requestId);
				
				// 				if($result) {
				// 					$check = $this->createPolicy($requestId);
				
				// 					if($check) {
				// 						return true;
				// 					}
				// 					else {
				// 						return false;
				// 					}
					
				// 				}
			}

			$hash = InsureDIYMotorHelper::generatePaymentSecureHash($params->get('merchantId'), $request_id, $params->get('currCode', '344'), $requested_amount, $transaction_type, $params->get('hashCode'));
			$paymentData = array(
					"target_url" => $params->get('pdUrl', ""),
					"request_id" => $request_id,
					"merchant_account_id" => $params->get('merchantId', FALSE),
					"transaction_type" => $transaction_type,
					"requested_amount" => $requested_amount,
					"request_amount_currency" => $params->get('currCode', '344'),
					"mps_mode" => $params->get('mpsMode', 'NIL'),
					"redirect_url" => $redirect_url,
					"cancel_url" => $cancel_url,
					"failed_url" => $fail_url,
					"payment_method" => "ALL",
					"remark" => "motor",
					"redirect" => 3,
					"language" => "E",
					"secureHash" => $hash,
						
			);
				
			$quoteData = (object) array(
					"id" => $quotationId,
					"quote_payment_stage" => "pending",
					"quote_payment_request_id" => $request_id,
					"quote_payment_amount" => $requested_amount
			);
				
			$check = $db->updateObject($this->_motor_quote, $quoteData, "id");
				
			return $paymentData;
		}
		else if($partner == "pingan") {
			$request_id = InsureDIYMotorHelper::guid();
			$merchant_account_id = $params->get('merchantId', "88117268");
			$transaction_type = 'N';
			$requested_amount = (number_format($premium, 2, '.', ''));
			$redirect_url = JURI::root(). 'component/insurediymotor/form/pinganPaymentSuccess';
			$cancel_url = JURI::root(). 'component/insurediymotor/form/pinganPaymentCancel';
			$fail_url = JURI::root(). 'component/insurediymotor/form/pinganPaymentFail';
			
			$query = $db->getQuery(TRUE)
			->select("*")
			->from($this->_motor_quote)
			->where("id = " . $db->quote($quotationId) . " AND " . "quote_deleted_time IS NULL");
			$db->setQuery($query);
			$quoteData = $db->loadObject();
			$requestId= $quoteData->quote_payment_request_id;
			
			if($requestId != "" && $quoteData->quote_payment_txn_id == "") {
				//$result = $this->validateAsiaPayPaymentStatus($merchant_account_id, $requestId);
				
				// 				if($result) {
				// 					$check = $this->createPolicy($requestId);
				
				// 					if($check) {
				// 						return true;
				// 					}
				// 					else {
				// 						return false;
				// 					}
					
				// 				}
			}

			$hash = InsureDIYMotorHelper::generatePaymentSecureHash($params->get('merchantId'), $request_id, $params->get('currCode', '344'), $requested_amount, $transaction_type, $params->get('hashCode'));
			$paymentData = array(
					"target_url" => $params->get('pdUrl', ""),
					"request_id" => $request_id,
					"merchant_account_id" => $params->get('merchantId', FALSE),
					"transaction_type" => $transaction_type,
					"requested_amount" => $requested_amount,
					"request_amount_currency" => $params->get('currCode', '344'),
					"mps_mode" => $params->get('mpsMode', 'NIL'),
					"redirect_url" => $redirect_url,
					"cancel_url" => $cancel_url,
					"failed_url" => $fail_url,
					"payment_method" => "ALL",
					"remark" => "motor",
					"redirect" => 3,
					"language" => "E",
					"secureHash" => $hash,
						
			);
				
			$quoteData = (object) array(
					"id" => $quotationId,
					"quote_payment_stage" => "pending",
					"quote_payment_request_id" => $request_id,
					"quote_payment_amount" => $requested_amount
			);
				
			$check = $db->updateObject($this->_motor_quote, $quoteData, "id");
				
			return $paymentData;
		}
	}
	
	public function checkValidity($item) {
		$flag = false;
		$db = JFactory::getDbo();
		
		if(isset($item["partnerId"])) {
			if($item["partnerId"] == "msig") {
				if(isset($item["msigValidThrough"])) {
					$today = date("y-m-d", strtotime("today"));
					$validThrough = date("y-m-d", strtotime($item["msigValidThrough"]));
					
					if ($today <= $validThrough) {
						$flag = true;
					}
					else {
						$quoteData = (object)array(
								"id" => $item["quotationId"],
								"quote_pre_quote_change_flag" => "true"
						);
						$check = $db->updateObject($this->_motor_quote, $quoteData, "id");
						
						$quoteData = (object)array(
								"id" => $item["msigId"],
								"msig_quote_valid_through" => ""
						);
						
						$check = $db->updateObject($this->_motor_quote_msig, $quoteData, "id");
						
						JFactory::getApplication()->enqueueMessage(JText::_("Previous quotation is expired. Please try again."), 'Warning');
						
						return false;
					}
				}
			}
		}
		
		if(isset($item["policyStartDate"])) {
			$today = date("y-m-d", strtotime("today"));
			$policyStartDate = date("y-m-d", strtotime($item["policyStartDate"]));
			
			if ($policyStartDate >= $today) {
				$flag = true;
			}
			else {
				$quoteData = (object)array(
						"id" => $item["quotationId"],
						"quote_pre_quote_change_flag" => "true"
				);
				$check = $db->updateObject($this->_motor_quote, $quoteData, "id");
				
				JFactory::getApplication()->enqueueMessage(JText::_("Policy start date is expired. Please update policy start date."), 'Warning');
				
				return false;
			}
		}
		
		return $flag;
	}
	
	public function validatePaymentStatus($request_id) {
		$app = JFactory::getApplication();
		$params = $app->getParams();
		$ch = curl_init();
		
		$url = $params->get('wcRestUrl', "").$params->get('wcMerchantId', "")."/payments/?request_id=".$request_id;
		
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Accept: application/json',
		));
		curl_setopt($ch, CURLOPT_USERPWD, $params->get('wcUsername', "wirecard3d").":".$params->get('wcPassword', "Tomcat123"));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		
		$server_output = curl_exec ($ch);
		$result = json_decode($server_output);
		$errorMsg = curl_error($ch);

		curl_close($ch);
		
		$db = JFactory::getDBO();
		
		if($server_output) {
			if(!empty((array)$result->payments)) {
				$data = $result->payments->payment[0];
				$reqId = $data->{'request-id'};
				$transactionState = $data->{'transaction-state'};
				$transactionId = $data->{'transaction-id'};
				
				if($transactionState== "success") {
					$query = $db->getQuery(TRUE)
					->select("*")
					->from($this->_motor_quote)
					->where("quote_payment_request_id = " . $db->quote($reqId) . " AND " . "quote_deleted_time IS NULL");
					$db->setQuery($query);
					$quote = $db->loadObject();
					
					$quoteData = (object) array(
							"id" => $quote->id,
							"quote_payment_stage" => $transactionState,
							"quote_payment_txn_id" => $transactionId,
							"quote_process_stage" => "issuing policy",
							"quote_user_terms_agreed" => "true"
					);
					
					$check = $db->updateObject($this->_motor_quote, $quoteData, "id");
					
					if($quote->quote_policy_code != "") {
						
						return false;
					}
					
					return true;
				}
				else {
					$session = JFactory::getSession();
					
					$errorResult["success"] = false;
					
					$errorResult["error_message"] = $data;
					
					$session->set("motor.result", $errorResult);
					
					$errorData = (object) array(
							"quote_master_id" => "sompo-payment-error",
							"policy_request_data" => $request_id,
							"policy_error_msg" => json_encode($data)
					);
					
					$db->insertObject("#__insure_motor_policy_error", $errorData);
					
					$this->sendEmails(FALSE, FALSE, $request_id);
					
					return false;
				}
			}
			else {
				return false;
			}
		}
		else {
			$session = JFactory::getSession();
			
			$errorResult["success"] = false;
			
			$errorResult["error_message"] = $data;
			
			$session->set("motor.result", $errorResult);
			
			$errorData = (object) array(
					"quote_master_id" => "sompo-payment-validation-error",
					"policy_request_data" => $request_id,
					"policy_error_msg" => $server_output
			);
			
			$db->insertObject("#__insure_motor_policy_error", $errorData);
			
			$this->sendEmails(FALSE, FALSE, $request_id);
			
			return false;
		}
	}
	
	public function validateAsiaPayPaymentStatus($txn_id, $request_id) {

		$db = JFactory::getDBO();
		
		$query = $db->getQuery(TRUE)
		->select("id")
		->from($this->_motor_quote)
		->where("quote_payment_request_id = " . $db->quote($request_id) . " AND " . "quote_deleted_time IS NULL");
		$db->setQuery($query);
		$quoteId = $db->loadResult();
		
		if(!$quoteId) {
			return false;
		}
		
		$date = new DateTime('now');
		$date->setTimezone(new DateTimeZone('Asia/Singapore'));
		$now = $date->format("Y-m-d\TH:i:s.u+08:00");
		
		$quoteData = (object) array(
				"id" => $quoteId,
				"quote_payment_stage" => "success",
				"quote_payment_txn_id" => $txn_id,
				"quote_process_stage" => "completed",
				"quote_user_terms_agreed" => "true",
				"quote_updated_time" => $now
		);
		
		$check = $db->updateObject($this->_motor_quote, $quoteData, "id");
		
		if($check) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public function savePromoCode($code, $quotationId) {
		$db = JFactory::getDBO();
		
		$quoteData = (object) array(
				"id" => $quotationId,
				"quote_promo_code" => $code
		);
		
		$check = $db->updateObject($this->_motor_quote, $quoteData, "id");
		
		return $check;
	}

	public function saveErrorMessage($errorObj) {
		$db = JFactory::getDBO();
		
		$errorData = (object) array(
				"quote_master_id" => $errorObj["quotationId"],
				"policy_request_data" => $errorObj["requestData"],
				"policy_error_msg" => $errorObj["errorMsg"]
				
		);
		
		$db->insertObject("#__insure_motor_policy_error", $errorData);
	}
}







