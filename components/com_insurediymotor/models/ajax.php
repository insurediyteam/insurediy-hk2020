<?php 
defined('_JEXEC') or die;

class InsureDIYMotorModelAjax extends JModelItem {
	
	protected $_motor_quote = "#__insure_motor_quote_master";
	protected $_motor_driver_info = "#__insure_motor_driver_info";



	public function specialContact($id) {
		
		$db = JFactory::getDBO();
		
		$query = $db->getQuery(TRUE)
		->select("*")
		->from("#__insure_motor_quote_master")
		->where("id = " . $db->quote($id). " AND " . "quote_deleted_time IS NULL");
		$db->setQuery($query);
		$quoteData = $db->loadObject();

		if(!$quoteData) {
			return false;
		}
		
		$csData = (object) array(
				"id" => $quoteData->id,
				"quote_cs_required" => "true"
		);
		
		$check = $db->updateObject("#__insure_motor_quote_master", $csData, "id");
		
		$query = $db->getQuery(TRUE)
		->select("*")
		->from("#__insure_motor_driver_info")
		->where("quote_master_id = " . $db->quote($quoteData->id));
		$db->setQuery($query);
		$driverData = $db->loadObject();
		
		
		
		$resultArr = (object) array(
				"order_no" => $quoteData->id,
				"contact_firstname" => explode(",", $driverData->quote_driver_first_name)[0],
				"contact_lastname" => explode(",", $driverData->quote_driver_last_name)[0],
				"contact_number" => $driverData->quote_driver_contact,
				"email" => explode(",", $driverData->quote_driver_email)[0]
		);

	
		return $this->sendEmailsCustomContact($quoteData->quote_payment_request_id);

	}
		
	public function getQuotations() {
		$session = JFactory::getSession();
		$result = array();
		
		if($session) {
			$userData = $session->get("motor.data", FALSE);
			
			if($userData) {
				$result["success"] = true;
				if($userData["currentInsurer"] != "NCD000003") {
					if(isset($userData["vehVariantAig"]) && $userData["vehVariantAig"] != null) {
						$aigResult = $this->getAigQuotations($userData);
						
						if($aigResult) {
							$result["aig"] = $aigResult;
						}
						else {
							$result["aig"]["success"] = false;
							$result["aig"]["error_message"] = "Failed to grab quotation from AIG.";
						}
					}
					else {
						$result["aig"]["success"] = false;
						$result["aig"]["error_message"] = "No model is selected for AIG.";
					}
				}
				else {
					$result["aig"]["success"] = false;
					$result["aig"]["currentInsurer"] = true;
				}

				if($userData["currentInsurer"] != "NCD000001") {
					if(isset($userData["vehVariantAw"]) && $userData["vehVariantAw"] != null) {
						$awResult = $this->getAwQuotations($userData);
						
						if($awResult) {
							$result["aw"] = $awResult;
						}
						else {
							$result["aw"]["success"] = false;
							$result["aw"]["error_message"] = "Failed to grab quotation from AW.";
						}
					}
					else {
						$result["aw"]["success"] = false;
						$result["aw"]["error_message"] = "No model is selected for AW.";
					}
				}
				else {
					$result["aw"]["success"] = false;
					$result["aw"]["currentInsurer"] = true;
				}

				if($userData["currentInsurer"] != "NCD000002") {
					if(isset($userData["vehVariantPingan"]) && $userData["vehVariantPingan"] != null) {
						$pinganResult = $this->getPinganQuotations($userData);
						
						if($pinganResult) {
							$result["pingan"] = $pinganResult;
						}
						else {
							$result["pingan"]["success"] = false;
							$result["pingan"]["error_message"] = "Failed to grab quotation from PINGAN.";
						}
					}
					else {
						$result["pingan"]["success"] = false;
						$result["pingan"]["error_message"] = "No model is selected for PINGAN.";
					}
				}
				else {
					$result["pingan"]["success"] = false;
					$result["pingan"]["currentInsurer"] = true;
				}
			}
			else {
				$result["success"] = false;
				$result["error_message"] = "Motor data not found.";
			}
		}
		else {
			$result["success"] = false;
			$result["error_message"] = "Session data not found.";
		}
		
		
		$ids = $this->saveQuotationToDb($result);

		
		// $result["sompo"]["id"] = $ids["sompo"];
		// $result["msig"]["id"] = $ids["msig"];
		$result["aw"]["id"] = $ids["aw"];
		$result["aig"]["id"] = $ids["aig"];
		$result["pingan"]["id"] = $ids["pingan"];
		
		if($userData["currentInsurer"] != "null") {
			$check = InsureDIYMotorHelper::getRenewalInfo($userData["currentInsurer"]);
			
			if($check) {
				$result["currentInsurer"] = $check;
			}
		}
		
		return $result;
	}
	
	public function getModelList($make) {
		$db = JFactory::getDBO();

		$query = $db->getQuery(TRUE)
		->select("*")
		->from("#__insure_motor_model_master")
		->where("make_code = ".$db->quote($make))
		->order("model_name ASC");
		$db->setQuery($query);
		$carModelData = $db->loadObjectList();
		
		return $carModelData;
	}

	public function getJobList($job) {
		$db = JFactory::getDBO();

		$query = $db->getQuery(TRUE)
		->select("*")
		->from("#__insure_motor_occupation")
		->where("parent = ".$db->quote($job))
		->order("occ_name ASC");
		$db->setQuery($query);
		$jobList = $db->loadObjectList();

		return $jobList;
	}
	
	private function getAxaQuotations($data) {
		$postData = array(
				"carMake" => "HONDA",
				"carModel" => "ACCORD 2.4",
				"carMakeYear" => $data["carMakeYear"],
				"offPeakCar" => $data["offPeakCar"],
				"Seats" => "5",
				"cubicCapacity" => "2453",
				"Gender" => $data["Gender"],
				"maritalStatus" => $data["maritalStatus"],
				"relationshipWithMainDriver" => $data["relationshipWithMainDriver"],
				"dateOfBirth" => $data["dateOfBirth"],
				"yearsOfDrivingExp" => $data["yearsOfDrivingExp"],
				"NCDPoints" => $data["NCDPoints"],
				"claimsPast3Years" => $data["claimsPast3Years"],
				"policyStartDate" => $data["policyStartDate"],
				"certificateOfMerit" => $data["meritCert"],
				"emailAddress" => $data["Email"],
				"includeNoClaimDiscPro" => "no"
		);
		
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, JURI::base()."AXA.php");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
		
		$server_output = curl_exec ($ch);
		
		curl_close($ch);       
		
		return json_decode($server_output);
	}
	
	private function getHlaQuotations($data) {
		$result= array();
		
		if($data["vehVariantHla"] == "" || $data["vehVariantHla"] == "null" || $data["vehVariantHla"] == null) {
			$result["success"] = false;
			
			$result["error_message"] = "No Model Variant is found";
			
			return $result;
		}
		
		return InsureDIYMotorHelper::requestHlaQuotations($data, false);
	}

	private function getAigQuotations($data) {
		$result= array();
		
		if($data["vehVariantAig"] == "" || $data["vehVariantAig"] == "null" || $data["vehVariantAig"] == null) {
			$result["success"] = false;
			
			$result["error_message"] = "No Model Variant is found";
			
			return $result;
		}
		
		return InsureDIYMotorHelper::requestAigQuotations($data, false);
	}

	private function getAwQuotations($data) {
		$result= array();
		
		if($data["vehVariantAw"] == "" || $data["vehVariantAw"] == "null" || $data["vehVariantAw"] == null) {
			$result["success"] = false;
			
			$result["error_message"] = "No Model Variant is found";
			
			return $result;
		}
		
		return InsureDIYMotorHelper::requestAwQuotations($data, false);
	}

	private function getPinganQuotations($data) {
		$result= array();
		
		if($data["vehVariantPingan"] == "" || $data["vehVariantPingan"] == "null" || $data["vehVariantPingan"] == null) {
			$result["success"] = false;
			
			$result["error_message"] = "No Model Variant is found";
			
			return $result;
		}
		
		return InsureDIYMotorHelper::requestPinganQuotations($data, false);
	}
	
	private function getSompoQuotations($data) {		
		$result= array();
		$params = JComponentHelper::getParams('com_insurediymotor');
		
		if($data["vehVariantSompo"] == "" || $data["vehVariantSompo"] == "null" || $data["vehVariantSompo"] == null) {
			$result["success"] = false;
			
			$result["error_message"] = "No Model Variant is found";
			
			return $result;
		}
		
		$ssoToken = InsureDIYMotorHelper::getSsoToken();
		
		if(!$ssoToken || !isset($ssoToken->access_token)) {
			$result["success"] = false;
			
			$result["error_message"] = json_encode($ssoToken);
			
			return $result;
		}
		
		$requestObj = InsureDIYMotorHelper::sompoQuoteRequestData($data);
		
		$ch = curl_init();
		$ua = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13';
		
		curl_setopt($ch, CURLOPT_URL, $params->get('sompoApiLink', "")."/getQuote");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Accept: application/json',
				'Data-type: application/json',
				'Authorization: bearer '.$ssoToken->access_token
				)
		);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestObj));
		
		$curl_result = curl_exec ($ch);
		$server_output = json_decode($curl_result);
		$error = curl_error($ch);
		
		curl_close($ch);
		
		if($server_output) {
			if(isset($server_output->serviceReplyObject)) {
				$responseData = $server_output->serviceReplyObject;
				
				if(isset($responseData->CURRENTPOLICY->PLAN_LIST)) {
					$planList = $responseData->CURRENTPOLICY->PLAN_LIST;
					$quotePolicyRefId = $responseData->CURRENTPOLICY->APPQUOTEPOLICYREFID;
					
					$result["success"] = true;
					$result["quote_ref_id"] = $quotePolicyRefId;
					$result["plan_list"] = $planList;
					$result["request_id"] = $requestObj->requestUniqueId;
					//$result["request"] = json_encode($requestObj);
				}
				if(isset($responseData->BUSINESSVALIDATIONMESSAGE)) {
					$error = "";
					foreach($responseData->BUSINESSVALIDATIONMESSAGE as $errorItem) {
						$error.= " ".$errorItem->ERRORMESSAGE;
					}
					
					$result["success"] = false;
					
					$result["error_message"] = $error;
				}
			}
			else {
				$result["success"] = false;
				
				$result["error_message"] = json_encode($server_output);
				
				$result["request"] = json_encode($requestObj);
			}
		}
		else {
			return false;
		}
		
		return $result;
	} 

	private function getMsigQuotations($data) {
		$validated = InsureDIYMotorHelper::validateMsigQuotation($data);
		$params = JComponentHelper::getParams('com_insurediymotor');
		
		if($validated) {
			$planList = array();

			$discountAmountArr = explode(",", $validated->msig_plan_premium_disc_amt);
			$discountTypeArr = explode(",", $validated->msig_plan_premium_disc_type);
			$discountValueArr = explode(",", $validated->msig_plan_premium_disc_amt);
			$excessStandardArr = explode(",", $validated->msig_plan_excess_standard);
			$excessWSArr = explode(",", $validated->msig_plan_excess_windscreen);
			$finalPremiumArr = explode(",", $validated->msig_plan_premium_total);
			$originalPremiumArr = explode(",", $validated->msig_plan_premium_before_disc);
			$planCodeArr = explode(",", $validated->msig_plan_id);
			$planDescriptionArr = explode(",", $validated->msig_plan_description);
			$planNameArr = explode(",", $validated->msig_plan_name);
			$taxAmountArr = explode(",", $validated->msig_plan_premium_tax_amt);
			$taxPrcntArr = explode(",", $validated->msig_plan_premium_tax_percent);
			
			foreach($planCodeArr as $key=>$val) {
				$object = (object) array(
						"discountAmount" => $discountAmountArr[$key],
						"discountType" => $discountTypeArr[$key],
						"discountValue" => $discountValueArr[$key],
						"excessStandard" => $excessStandardArr[$key],
						"excessWS" => $excessWSArr[$key],
						"finalPremium" => $finalPremiumArr[$key],
						"originalPremium" => $originalPremiumArr[$key],
						"planCode" => $planCodeArr[$key],
						"planDescription" => $planDescriptionArr[$key],
						"planName" => $planNameArr[$key],
						"productCode" => "QMX",
						"taxAmount" => $taxAmountArr[$key],
						"taxPrcnt" => $taxPrcntArr[$key]
				);
				
				$planList[] = $object;
			}
			
			$result["success"] = true;
			$result["quote_ref_id"] = $validated->msig_ref_id;
			$result["plan_list"] = $planList;
			$result["request_id"] = "skip";
			$result["valid_through"] = $validated->msig_quote_valid_through;
			
			return $result;
		}
		
		$result= array();
		
		if($data["vehVariantMsig"] == "" || $data["vehVariantMsig"] == "null" || $data["vehVariantMsig"] == null) {
			$result["success"] = false;
			
			$result["error_message"] = "No Model Variant is found";
			
			return $result;
		}
		
		$accessToken = InsureDIYMotorHelper::getMSIGAccessToken();
		
		if(!$accessToken || !isset($accessToken->access_token)) {
			$result["success"] = false;
			
			$result["error_message"] = "Access Denied!";
			
			return $result;
		}
		
		
		$requestObj = InsureDIYMotorHelper::msigQuoteRequestData($data);
		
		$ch = curl_init();
		$ua = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13';
		
		curl_setopt($ch, CURLOPT_URL, $params->get('msigApiLink', "")."/products/QMX/quotes");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
						'Content-Type: application/json',
						'Accept: application/json',
						'Data-type: application/json',
						'Authorization: bearer '.$accessToken->access_token
				)
		);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestObj));
		
		$curl_result = curl_exec ($ch);
		$server_output = json_decode($curl_result);
		$error = curl_error($ch);
		
		curl_close($ch);
		
		if($server_output) {
			if(isset($server_output->quoteReference)) {
				$planList = $server_output->premium;
				$quoteRefId = $server_output->quoteReference;
				
				$result["success"] = true;
				$result["quote_ref_id"] = $quoteRefId;
				$result["plan_list"] = $planList;
				$result["request_id"] = "";
				$result["valid_through"] = $server_output->validThrough;
			}
			
			if(isset($server_output->errorCode)) {
				$error = "";
				foreach($server_output->fieldErrors as $errorItem) {
					$error.= " [".$errorItem->fieldCode ."]-". $errorItem->errorDesc ."<br>";
				}
				
				$result["success"] = false;
				
				$result["error_message"] = $error;
			}
		}
		else {
			return false;
		}
		
		return $result;
	}
	
	public function sompoPolicyAckCallback($data) {
		$result["applicationId"] = $data->applicationId;
		$result["serviceId"] = $data->serviceId;
		$result["requestUniqueId"] = $data->requestUniqueId;
		
		if($data->serviceReplyObject->updatePolicyCreationReply->acknowledgementId) {
			$db = JFactory::getDBO();
			
			$query = $db->getQuery(TRUE)
			->select("*")
			->from("#__insure_motor_quote_sompo")
			->where("sompo_pol_ack_id = ".$db->quote($data->serviceReplyObject->updatePolicyCreationReply->acknowledgementId));
			
			$db->setQuery($query);
			
			$sompoData = $db->loadObject();
			
			if(!$sompoData) {
				$obj = (object)array(
						"updatePolicyCreationReply" => (object) array(
								"statusCd" => "99",
								"statusDescription" => "acknowledgementId not found in database"
						)
				);
				
				$this->sendEmails(FALSE, FALSE,$result["requestUniqueId"]);
				
				return $obj;
			}
			
			$date = new DateTime('now');
			$date->setTimezone(new DateTimeZone('Asia/Singapore'));
			$now = $date->format("Y-m-d\TH:i:s.u+08:00");
			
			$quoteData = (object) array(
					"id" => $sompoData->quote_master_id,
					"quote_policy_code" => $data->serviceReplyObject->updatePolicyCreationReply->policyNo,
					"quote_updated_time" => $now
			);
			
			$check = $db->updateObject("#__insure_motor_quote_master", $quoteData, "id");
			
			if($check) {
				$obj = (object)array(
						"updatePolicyCreationReply" => (object) array(
								"statusCd" => "01",
								"statusDescription" => "success"
						)
				);
			}
			else {
				$obj = (object)array(
						"updatePolicyCreationReply" => (object) array(
								"statusCd" => "99",
								"statusDescription" => "failed to update policy"
						)
				);
				
				$this->sendEmails(FALSE, FALSE,$result["requestUniqueId"]);
			}
		}
		else {
			$obj = (object)array(
					"updatePolicyCreationReply" => (object) array(
							"statusCd" => "99",
							"statusDescription" => "acknowledgementId not found in database"
					)
			);
			
			$this->sendEmails(FALSE, FALSE,$result["requestUniqueId"]);
		}
		
		$result["serviceReplyObject"] = $obj;
		
		return $result;
	}
	
	public function saveQuotationToDb($data) {
		$session = JFactory::getSession();
		$userData = $session->get("motor.data", FALSE);
		$db = JFactory::getDBO();
		$aigQuoteId = "0";
		$pinganQuoteId = "0";
		$awQuoteId = "0";
		
		$date = new DateTime('now');
		$date->setTimezone(new DateTimeZone('Asia/Singapore'));
		$now = $date->format("Y-m-d\TH:i:s.u+08:00");
		
		if($userData) {
			foreach($data as $key=>$val) {
				// if($key == "sompo") {
				// 	if($val["success"]) {
				// 		$query = $db->getQuery(TRUE)
				// 		->select("id")
				// 		->from("#__insure_motor_quote_sompo")
				// 		->where("quote_master_id = ".$db->quote($userData["quotationId"]));
				// 		$db->setQuery($query);
				// 		$sompoQuoteId = $db->loadResult();
						
				// 		$planId = array();
				// 		$planName = array();
				// 		$premiumBeforeTax = array();
				// 		$premiumTaxAmt = array();
				// 		$premiumTaxPercent = array();
				// 		$premiumTotal = array();
				// 		$planOrder = array();
				// 		$planRisk = array();
						
				// 		foreach($val["plan_list"] as $item) {
				// 			$planId[] = $item->PLANCODE;
				// 			$planName[] = $item->PLANNAME;
				// 			$premiumBeforeTax[] = $item->PREMIUMBEFORETAX;
				// 			$premiumTaxAmt[] = $item->PLANTAXAMOUNT;
				// 			$premiumTaxPercent[] = $item->PLANTAXPERCENT;
				// 			$premiumTotal[] = $item->PLANTOTALPREMIUM;
				// 			$planOrder[] = $item->PLAN_ORDER;
				// 			$planRisk[] = json_encode($item->PLANXRISKLIST);
				// 		}
						
				// 		$sompoDataArr = (object) array(
				// 				"quote_master_id" => $userData["quotationId"],
				// 				"quote_car_variant" => $userData["vehVariantSompo"],
				// 				"sompo_req_unique_id" => $val["request_id"],
				// 				"sompo_app_quote_ref_id" => $val["quote_ref_id"],
				// 				"sompo_plan_id" => implode(",", $planId),
				// 				"sompo_plan_name" => implode(",", $planName),
				// 				"sompo_plan_premium_before_tax" => implode(",", $premiumBeforeTax),
				// 				"sompo_plan_premium_tax_amt" => implode(",", $premiumTaxAmt),
				// 				"sompo_plan_premium_tax_percent" => implode(",", $premiumTaxPercent),
				// 				"sompo_plan_premium_total" => implode(",", $premiumTotal),
				// 				"sompo_plan_order" => implode(",", $planOrder),
				// 				"sompo_plan_risk" => implode("#", $planRisk)
				// 		);
						
				// 		if($sompoQuoteId) {
				// 			$sompoDataArr->id = $sompoQuoteId;
							
				// 			$check = $db->updateObject("#__insure_motor_quote_sompo", $sompoDataArr, "id");
							
				// 			$sompoQuoteId = $sompoQuoteId;
				// 		}
				// 		else {
				// 			$check = $db->insertObject("#__insure_motor_quote_sompo", $sompoDataArr);
							
				// 			$sompoQuoteId = $db->insertid();
				// 		}
				// 	}
				// 	else {
				// 		$query = $db->getQuery(TRUE)
				// 		->delete("#__insure_motor_quote_sompo")
				// 		->where("quote_master_id = ".$db->quote($userData["quotationId"]));
				// 		$db->setQuery($query);
						
				// 		$result = $db->execute();
				// 	}
				// }
				
				// if($key == "msig") {
				// 	if(isset($val["success"])) {
				// 		if($val["success"]){
				// 			$query = $db->getQuery(TRUE)
				// 			->select("id")
				// 			->from("#__insure_motor_quote_msig")
				// 			->where("quote_master_id = ".$db->quote($userData["quotationId"]));
				// 			$db->setQuery($query);
				// 			$msigQuoteId= $db->loadResult();
							
				// 			if($val["request_id"] == "skip") {
				// 				continue;
				// 			}
							
				// 			$planId = array();
				// 			$planName = array();
				// 			$planDesc = array();
				// 			$premiumBeforeTax = array();
				// 			$premiumTaxAmt = array();
				// 			$premiumTaxPercent = array();
				// 			$premiumTotal = array();
				// 			$excess = array();
				// 			$windscreen = array();
							
				// 			foreach($val["plan_list"] as $item) {
				// 				$planId[] = $item->planCode;
				// 				$planName[] = $item->planName;
				// 				$planDesc[] = $item->planDescription;
				// 				$premiumTaxAmt[] = $item->taxAmount;
				// 				$premiumTaxPercent[] = $item->taxPrcnt;
				// 				$premiumTotal[] = $item->finalPremium;
				// 				$premiumBeforeDisc[] = $item->originalPremium;
				// 				$premiumDiscType[] = $item->discountType;
				// 				$premiumDiscAmt[] = $item->discountType == "PRCNT" ? $item->discountAmount :  $item->discountValue;
				// 				$excess[] = $item->excessStandard;
				// 				$windscreen[] = $item->excessWS;
				// 			}
							
				// 			$msigDataArr= (object) array(
				// 					"quote_master_id" => $userData["quotationId"],
				// 					"quote_car_variant" => $userData["vehVariantMsig"],
				// 					"msig_ref_id" => $val["quote_ref_id"],
				// 					"msig_quote_valid_through" => $val["valid_through"],
				// 					"msig_plan_id" => implode(",", $planId),
				// 					"msig_plan_name" => implode(",", $planName),
				// 					"msig_plan_description" => implode(",", $planDesc),
				// 					"msig_plan_premium_tax_amt" => implode(",", $premiumTaxAmt),
				// 					"msig_plan_premium_tax_percent" => implode(",", $premiumTaxPercent),
				// 					"msig_plan_premium_total" => implode(",", $premiumTotal),
				// 					"msig_plan_premium_before_disc" => implode(",", $premiumBeforeDisc),
				// 					"msig_plan_premium_disc_type" => implode(",", $premiumDiscType),
				// 					"msig_plan_premium_disc_amt" => implode(",", $premiumDiscAmt),
				// 					"msig_plan_excess_standard" => implode(",", $excess),
				// 					"msig_plan_excess_windscreen" => implode(",", $windscreen)
				// 			);
							
				// 			if($msigQuoteId) {
				// 				$quoteDataArr = (object) array(
				// 						"id" => $userData["quotationId"],
				// 						"quote_pre_quote_change_flag" => "false"
				// 				);
								
				// 				$msigDataArr->id = $msigQuoteId;
								
				// 				$check = $db->updateObject("#__insure_motor_quote_msig", $msigDataArr, "id");
								
				// 				$check = $db->updateObject("#__insure_motor_quote_master", $quoteDataArr, "id");
				// 			}
				// 			else {
				// 				$check = $db->insertObject("#__insure_motor_quote_msig", $msigDataArr);
								
				// 				$msigQuoteId = $db->insertid();
				// 			}
				// 		}
				// 		else {
				// 			$query = $db->getQuery(TRUE)
				// 			->delete("#__insure_motor_quote_msig")
				// 			->where("quote_master_id = ".$db->quote($userData["quotationId"]));
				// 			$db->setQuery($query);
							
				// 			$result = $db->execute();
				// 		}
				// 	}
				// 	else {
				// 		$query = $db->getQuery(TRUE)
				// 		->delete("#__insure_motor_quote_msig")
				// 		->where("quote_master_id = ".$db->quote($userData["quotationId"]));
				// 		$db->setQuery($query);
						
				// 		$result = $db->execute();
				// 	}
				// }
				
				// if($key == "hla") {
				// 	if(isset($val["success"])) {
				// 		if($val["success"]){
				// 			$query = $db->getQuery(TRUE)
				// 			->select("id")
				// 			->from("#__insure_motor_quote_hla")
				// 			->where("quote_master_id = ".$db->quote($userData["quotationId"]));
				// 			$db->setQuery($query);
				// 			$hlaQuoteId= $db->loadResult();
							
				// 			$planId = $val["plan_list"]->productCode;
				// 			$planName = $val["plan_list"]->planName;
				// 			$premiumTotal = $val["plan_list"]->finalPremium;
				// 			$excess = implode("#", $val["plan_list"]->excessStandard);
				// 			$excessYoung = $val["plan_list"]->excessYoung;
				// 			$windscreen = $val["plan_list"]->excessWS;
							
				// 			$hlaDataArr= (object) array(
				// 					"quote_master_id" => $userData["quotationId"],
				// 					"quote_car_variant" => $userData["vehVariantHla"],
				// 					"hla_plan_id" => $planId,
				// 					"hla_plan_name" => $planName,
				// 					"hla_plan_premium_total" => $premiumTotal,
				// 					"hla_plan_excess_standard" => $excess,
				// 					"hla_plan_excess_young" => $excessYoung,
				// 					"hla_plan_excess_windscreen" => $windscreen
				// 			);
							
				// 			if($hlaQuoteId) {
				// 				$hlaDataArr->id = $hlaQuoteId;
								
				// 				$check = $db->updateObject("#__insure_motor_quote_hla", $hlaDataArr, "id");
				// 			}
				// 			else {
				// 				$check = $db->insertObject("#__insure_motor_quote_hla", $hlaDataArr);
								
				// 				$hlaQuoteId = $db->insertid();
				// 			}
				// 		}
				// 		else {
				// 			$query = $db->getQuery(TRUE)
				// 			->delete("#__insure_motor_quote_hla")
				// 			->where("quote_master_id = ".$db->quote($userData["quotationId"]));
				// 			$db->setQuery($query);
							
				// 			$result = $db->execute();
				// 		}
				// 	}
				// 	else {
				// 		$query = $db->getQuery(TRUE)
				// 		->delete("#__insure_motor_quote_hla")
				// 		->where("quote_master_id = ".$db->quote($userData["quotationId"]));
				// 		$db->setQuery($query);
						
				// 		$result = $db->execute();
				// 	}
				// }

				if($key == "aig") {
					if(isset($val["success"])) {
						if($val["success"]){
							$query = $db->getQuery(TRUE)
							->select("id")
							->from("#__insure_motor_quote_aig")
							->where("quote_master_id = ".$db->quote($userData["quotationId"]));
							$db->setQuery($query);
							$aigQuoteId= $db->loadResult();
							
							$planId = $val["plan_list"]->productCode;
							$planName = $val["plan_list"]->planName;
							$premiumTotal = $val["plan_list"]->finalPremium;
							$aigPlanAnnualizedPremium = $val["plan_list"]->annualizedPremium;
							$mib = $val["plan_list"]->mib;
							$ProducerCommAmount = $val["plan_list"]->producerCommAmount;
							
							$aigDataArr= (object) array(
									"quote_master_id" => $userData["quotationId"],
									"quote_car_variant" => $userData["vehVariantAig"],
									"aig_plan_id" => $planId,
									"aig_plan_name" => $planName,
									"aig_plan_premium_total" => $premiumTotal,
									"aig_plan_annualized_premium" => $aigPlanAnnualizedPremium,
									"aig_plan_mib" => $mib,
									"aig_plan_prdr_comm_amt" => $ProducerCommAmount,
									"aig_plan_general_excess" => $val["plan_list"]->generalExcess,
									"aig_plan_theft_loss_excess" => $val["plan_list"]->theftLoss,
									"aig_plan_parking_excess" => $val["plan_list"]->parkingDamage,
									"aig_plan_third_party_excess" => $val["plan_list"]->thirdParty,
									"aig_plan_young_excess" => $val["plan_list"]->youngDriver,
									"aig_plan_inexperience_excess" => $val["plan_list"]->inexperienceDriver,
									"aig_plan_unammed_excess" => $val["plan_list"]->unamedDriver
							);
							
							if($aigQuoteId) {
								$aigDataArr->id = $aigQuoteId;
								
								$check = $db->updateObject("#__insure_motor_quote_aig", $aigDataArr, "id");
							}
							else {
								$check = $db->insertObject("#__insure_motor_quote_aig", $aigDataArr);
								
								$aigQuoteId = $db->insertid();
							}
						}
						else {
							$query = $db->getQuery(TRUE)
							->delete("#__insure_motor_quote_aig")
							->where("quote_master_id = ".$db->quote($userData["quotationId"]));
							$db->setQuery($query);
							
							$result = $db->execute();
						}
					}
					else {
						$query = $db->getQuery(TRUE)
						->delete("#__insure_motor_quote_aig")
						->where("quote_master_id = ".$db->quote($userData["quotationId"]));
						$db->setQuery($query);
						
						$result = $db->execute();
					}
				}
				if($key == "aw") {
					if(isset($val["success"])) {
						if($val["success"]){
							$query = $db->getQuery(TRUE)
							->select("id")
							->from("#__insure_motor_quote_aw")
							->where("quote_master_id = ".$db->quote($userData["quotationId"]));
							$db->setQuery($query);
							$awQuoteId= $db->loadResult();
							
							$planId = $val["plan_list"]->productCode;
							$planName = $val["plan_list"]->planName;
							$premiumTotal = $val["plan_list"]->finalPremium;
							$awPlanAnnualizedPremium = $val["plan_list"]->annualizedPremium;
							$mib = $val["plan_list"]->mib;
							$ProducerCommAmount = $val["plan_list"]->producerCommAmount;
							
							$awDataArr= (object) array(
									"quote_master_id" => $userData["quotationId"],
									"quote_car_variant" => $userData["vehVariantAw"],
									"aw_plan_id" => $planId,
									"aw_plan_name" => $planName,
									"aw_plan_premium_total" => $premiumTotal,
									"aw_plan_annualized_premium" => $awPlanAnnualizedPremium,
									"aw_plan_mib" => $mib,
									"aw_plan_prdr_comm_amt" => $ProducerCommAmount,
									"aw_plan_general_excess" => $val["plan_list"]->generalExcess,
									"aw_plan_theft_loss_excess" => $val["plan_list"]->theftLoss,
									"aw_plan_parking_excess" => $val["plan_list"]->parkingDamage,
									"aw_plan_third_party_excess" => $val["plan_list"]->thirdParty,
									"aw_plan_young_excess" => $val["plan_list"]->youngDriver,
									"aw_plan_inexperience_excess" => $val["plan_list"]->inexperienceDriver,
									"aw_plan_unammed_excess" => $val["plan_list"]->unamedDriver
							);
							
							if($awQuoteId) {
								$awDataArr->id = $awQuoteId;
								
								$check = $db->updateObject("#__insure_motor_quote_aw", $awDataArr, "id");
							}
							else {
								$check = $db->insertObject("#__insure_motor_quote_aw", $awDataArr);
								
								$awQuoteId = $db->insertid();
							}
						}
						else {
							$query = $db->getQuery(TRUE)
							->delete("#__insure_motor_quote_aw")
							->where("quote_master_id = ".$db->quote($userData["quotationId"]));
							$db->setQuery($query);
							
							$result = $db->execute();
						}
					}
					else {
						$query = $db->getQuery(TRUE)
						->delete("#__insure_motor_quote_aw")
						->where("quote_master_id = ".$db->quote($userData["quotationId"]));
						$db->setQuery($query);
						
						$result = $db->execute();
					}
				}
				if($key == "pingan") {
					if(isset($val["success"])) {
						if($val["success"]){
							$query = $db->getQuery(TRUE)
							->select("id")
							->from("#__insure_motor_quote_pingan")
							->where("quote_master_id = ".$db->quote($userData["quotationId"]));
							$db->setQuery($query);
							$pinganQuoteId= $db->loadResult();
							
							$planId = $val["plan_list"]->productCode;
							$planName = $val["plan_list"]->planName;
							$premiumTotal = $val["plan_list"]->finalPremium;
							$pinganPlanAnnualizedPremium = $val["plan_list"]->annualizedPremium;
							$mib = $val["plan_list"]->mib;
							$ProducerCommAmount = $val["plan_list"]->producerCommAmount;
							
							$pinganDataArr= (object) array(
									"quote_master_id" => $userData["quotationId"],
									"quote_car_variant" => $userData["vehVariantPingan"],
									"pingan_plan_id" => $planId,
									"pingan_plan_name" => $planName,
									"pingan_plan_premium_total" => $premiumTotal,
									"pingan_plan_annualized_premium" => $pinganPlanAnnualizedPremium,
									"pingan_plan_mib" => $mib,
									"pingan_plan_prdr_comm_amt" => $ProducerCommAmount,
									"pingan_plan_general_excess" => $val["plan_list"]->generalExcess,
									"pingan_plan_theft_loss_excess" => $val["plan_list"]->theftLossExcess,
									"pingan_plan_parking_excess" => $val["plan_list"]->parkingExcess,
									"pingan_plan_third_party_excess" => $val["plan_list"]->thirdPartyExcess,
									"pingan_plan_young_excess" => $val["plan_list"]->youngExcess,
									"pingan_plan_inexperience_excess" => $val["plan_list"]->inexperienceExcess,
									"pingan_plan_unammed_excess" => $val["plan_list"]->unammedExcess

							);
							
							if($pinganQuoteId) {
								$pinganDataArr->id = $pinganQuoteId;
								
								$check = $db->updateObject("#__insure_motor_quote_pingan", $pinganDataArr, "id");
							}
							else {
								$check = $db->insertObject("#__insure_motor_quote_pingan", $pinganDataArr);
								
								$pinganQuoteId = $db->insertid();
							}
						}
						else {
							$query = $db->getQuery(TRUE)
							->delete("#__insure_motor_quote_pingan")
							->where("quote_master_id = ".$db->quote($userData["quotationId"]));
							$db->setQuery($query);
							
							$result = $db->execute();
						}
					}
					else {
						$query = $db->getQuery(TRUE)
						->delete("#__insure_motor_quote_pingan")
						->where("quote_master_id = ".$db->quote($userData["quotationId"]));
						$db->setQuery($query);
						
						$result = $db->execute();
					}
				}
			}
			
			$quoteDataArr = (object) array(
					"id" => $userData["quotationId"],
					"quote_pre_quote_change_flag" => "false",
					"quote_updated_time" => $now,
					"quote_policy_selected_plan_code" => "",
					"quote_policy_selected_partner_code" => "",
					"quote_plan_workshop" => $userData["workshop"],
					"quote_plan_los" => $userData["los"],
					"quote_plan_ncdpro" => $userData["ncdPro"],
					"quote_identifier" => $userData["identifier"],
					// "quote_selected_model_variant" => $userData["vehVariantHla"].",".$userData["vehVariantMsig"].",".$userData["vehVariantSompo"]
					"quote_selected_model_variant" => $userData["vehVariantAig"].",".$userData["vehVariantAw"].",".$userData["vehVariantPingan"]
			);
			

			$check = $db->updateObject("#__insure_motor_quote_master", $quoteDataArr, "id");

			
			// return array("sompo"=>$sompoQuoteId, "msig"=>$msigQuoteId, "hla" =>$hlaQuoteId);
			return array("aig"=>$aigQuoteId, "aw"=>$awQuoteId, "pingan"=>$pinganQuoteId);
		}
	}
	
	
// 	// Mail Stuffs - start
// 	public function sendEmails($myInfo) {
// 		$mailer = JFactory::getMailer();
// 		$params = JComponentHelper::getParams('com_insurediymotor');
// 		$maildata = $this->getMailData($myInfo);
// 		$emails = $this->getMails($params, $myInfo->email);
// 		$emailbodies = $this->getEmailBodies($params, $maildata);
		
// 		$mode = TRUE;
		
// 		$mailer->ClearAttachments();
// 		$mailer->ClearAllRecipients();
// 		$mailer->sendMail($emails['from'], $emails['fromName'], $emails['service_recipient'], $emails['subject'], $emailbodies['sup_service'], $mode, NULL, $emails['bcc']);
		
// 		return true;
// 	}
	
// 	public function getEmailBodies($params, $data) {
// 		$array = array();
// 		$array['cover_letter'] = InsureDIYHelper::replaceVariables($params->get("clformat"), $data);
// 		$array['ce_customer'] = InsureDIYHelper::replaceVariables($params->get("cecformat"), $data);
// 		$array['ce_service'] = InsureDIYHelper::replaceVariables($params->get("cesformat"), $data);
// 		$array['cer_service'] = InsureDIYHelper::replaceVariables($params->get("cerformat"), $data);
// 		$array['sup_service'] = InsureDIYHelper::replaceVariables($params->get("supformat"), $data);
// 		return $array;
// 	}
	
// 	public function getMails($params, $user_email) {
// 		$confg = JFactory::getConfig();
// 		$array = array();
		
// 		$array['recipient'] = $params->get('admin_email');
// 		$sales_email = $params->get('sales_email');
// 		$array['bcc'] = array("0" => $sales_email);
// 		$array['subject'] = 'InsureDIY - Customer Special Request';
		
// 		$array['service_recipient'] = $params->get('service_email', "");
// 		$array['from'] = $confg->get("mailfrom");
// 		$array['fromName'] = $confg->get("fromname");
// 		return $array;
// 	}
	
// 	public function getMailData($myInfo) {
// 		$session = JFactory::getSession();
// 		$data = array();
		
// 		$data['email'] = $myInfo->email;
// 		$data['order_no'] = $myInfo->order_no;
// 		$data['first_name'] = $myInfo->contact_firstname;
// 		$data['last_name'] = $myInfo->contact_lastname;
// 		$data['contact_number'] = $myInfo->contact_number;
		
// 		return $data;
// 	}	
	
	
	public function testGetToken() {
		$ssoToken = InsureDIYMotorHelper::getSsoToken();
		
		$adminParams = JComponentHelper::getParams("com_insurediymotor");
		$credential = array(
				"URL" => $adminParams->get("sompoSSO",""),
				"grant_type" => "password",
				"client_id" => $adminParams->get("sompoClientId",""),
				"username" => $adminParams->get("sompoUsername",""),
				"password" => $adminParams->get("sompoPassword","")
		);

		return array("request"=>$credential, "response"=>$ssoToken);
	}
	
	public function testgetQuotations() {
		$postData = array(
						"offPeakCar" => false,
						"carMake" => "AUDI",
						"carModel" => "a",
						"vehVariantSompo" => "a",
						"companyRegistered" => "No",
						"vehicleRegNo" => "SGH2341C",
						"carMakeYear" => "2016",
						"carRegisterYear" => "2016",
						"Gender" => Array (
								"0" => "MALE",
								//"1" => "FEMALE"
						),
						"dateOfBirth" => Array (
										"0" => "08-Sep-1990",
										//"1" => "05-Apr-1989"
						),
						"yearsOfDrivingExp" => Array (
												"0" => 5,
												//"1" => 10
						),
						"Occupation" => Array (
											"0" => "Job1",
											//"1" => "Job2"
						),
						"maritalStatus" => Array(
											"0" => "Single",
											//"1" => "Single"
						),
						"relationshipWithMainDriver" => Array (
															"0" => "I",
															//"1" => "S"
						),
						"meritCert" => true,
						"NCDPoints" => 40,
						"ncdReason" => null,
						"demeritPoint" => Array (
											"0" => 2
						),
						"claimsPast3Years" => 0,
						"claimAmount" => 0,
						"validLicense" => "No",
						"coeExpireDate" => "31-Dec-2028",
						"policyStartDate" => "31-Dec-2017",
						"currentInsurer" => "NCD000005",
						"workshop" => "Yes",
						"los" => "Yes",
						"ncdPro" => "Yes",
						"id" => 0,
						"firstName" => Array (
										"0" => "wai hong"
						),
						"lastName" => Array (
										"0" => "phun"
						),
						"icType" => Array (
										"0" => "N"
						),
						"Nric" => Array (
										"0" => "S3619066C"
						),
						"Email" => Array (
										"0" => "waihong.phun@insurediy.com"
						),
						"mobileNo" => Array (
										"0" => "94497248"
						),
						"agreePDC" => "on",
						"agreeMarketing" => true,
						"agreeDeclaration" => "on"
		);
		
		$ssoToken = InsureDIYMotorHelper::getSsoToken();
		$requestObj = InsureDIYMotorHelper::sompoQuoteRequestData($postData);
		
		$ch = curl_init();
		$ua = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13';
		
		curl_setopt($ch, CURLOPT_URL, "http://uat.sompo.com.sg:2052/getQuote");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Accept: application/json',
				'Data-type: application/json',
				'Authorization: bearer '.$ssoToken->access_token
		)
				);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestObj));
		
		$server_output = json_decode(curl_exec ($ch));
		
		curl_close($ch);

		if($server_output) {
			return array("request"=>$requestObj, "response"=>$server_output);
		}
		else {
			$error = curl_error($ch);
			
			return false;
		}
	}
	
	public function testGetProposals($ref_id) {
		$postData = array(
						"ref_id" => $ref_id,
						"offPeakCar" => "false",
						"carMake" => "AUDI",
						"carModel" => "a",
						"companyRegistered" => "No",
						"vehicleRegNo" => "SGH2341C",
						"carMakeYear" => "2016",
						"carRegisterYear" => "2016",
						"Gender" => Array (
									"0" => "MALE"
						),
						"dateOfBirth" => Array (
										"0" => "31-Jan-1990"
						),
						"yearsOfDrivingExp" => Array (
												"0" => 10
						),
						"Occupation" => Array (
											"0" => "Job1"
						),
						"maritalStatus" => Array (
											"0" => "Single"
						),
						"relationshipWithMainDriver" => Array (
															"0" => "I"
						),
						"meritCert" => true,
						"NCDPoints" => 40,
						"ncdReason" => null,
						"demeritPoint" => Array (
											"0" => 0
						),
						"claimsPast3Years" => 0,
						"claimAmount" => 0,
						"validLicense" => "Yes",
						"coeExpireDate" => "31-Dec-2020",
						"policyStartDate" => "31-Dec-2017",
						"currentInsurer" => "NCD000001",
						"workshop" => "Yes",
						"los" => "Yes",
						"ncdPro" => "Yes",
						"id" => 0,
						"firstName" => Array (
										"0" => "wai hong"
						),
						"lastName" => Array (
										"0" => "phun"
						),
						"icType" => Array (
										"0" => "N"
						),
						"Nric" => Array (
									"0" => "S3619066C"
						),
						"Email" => Array (
									"0" => "waihong.phun@insurediy.com"
						),
						"mobileNo" => Array (
										"0" => "94497248"
						),
						"agreePDC" => "on",
						"agreeMarketing" => true,
						"agreeDeclaration" => "on",
						"vehVariantAxa" => "a",
						"vehVariantMsig" => "a",
						"vehVariantSompo" => "a",
						"plan_id" => "EDP",
						"plan_name" => "ExcelDrive PRESTIGE",
						"partner_id" => "sompo",
						"vehicleBodyType" => "Saloon",
						"vehicleCc" => "1496",
						"vehicleEngineNo" => "engine1234",
						"vehicleChassisNo" => "chassis2234",
						"vehicleSeatingCapacity" => 5,
						"driveToWestMY" => "0",
						"previousPolicyNo" => "123123",
						"previousRegNo" => "SGH2341C",
						"driverNationality" => Array (
													"0" => "SG"
						),
						"driverApplicant" => 0,
						"address" => Array (
										"0" => "11-1234",
										"1" => "321, toa payoh ave 2",
										"2" => "blk 222"
						),
						"state" => "singapore",
						"city" => "singapore",
						"zipCode" => "123456",
						"country" => "SG",
						"loan" => "Yes",
						"loanCompany" => "DBS"																																
		);
		
		$ssoToken = InsureDIYMotorHelper::getSsoToken();
		$requestObj = InsureDIYMotorHelper::sompoProposalRequestData($postData);
		
		$ch = curl_init();
		$ua = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13';
		
		curl_setopt($ch, CURLOPT_URL, "http://uat.sompo.com.sg:2052/getProposal");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Accept: application/json',
				'Data-type: application/json',
				'Authorization: bearer '.$ssoToken->access_token
		)
				);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestObj));
		
		$server_output = json_decode(curl_exec ($ch));
		$error = curl_error($ch);
		
		curl_close($ch);
		
		if($server_output) {
			return array("request"=>$requestObj, "response"=>$server_output);
		}
		else {
			return array("request"=>$requestObj, "response"=>$server_output);
		}
	}
	
	public function testCreatePolicy($ref_id, $policy) {
		$postData = array(
				"ref_id" => $ref_id,
				"proposal_ref_id" => $policy->POL_AGENT_REF_NO,
				"offPeakCar" => "false",
				"carMake" => "AUDI",
				"carModel" => "a",
				"companyRegistered" => "No",
				"vehicleRegNo" => "SGH2341C",
				"carMakeYear" => "2016",
				"carRegisterYear" => "2016",
				"Gender" => Array (
						"0" => "MALE"
						),
				"dateOfBirth" => Array (
						"0" => "31-Jan-1990"
						),
				"yearsOfDrivingExp" => Array (
						"0" => 10
						),
				"Occupation" => Array (
						"0" => "Job1"
						),
				"maritalStatus" => Array (
						"0" => "Single"
						),
				"relationshipWithMainDriver" => Array (
						"0" => "I"
						),
				"meritCert" => true,
				"NCDPoints" => 40,
				"ncdReason" => null,
				"demeritPoint" => Array (
						"0" => 0
						),
				"claimsPast3Years" => 0,
				"claimAmount" => 0,
				"validLicense" => "Yes",
				"coeExpireDate" => "31-Dec-2020",
				"policyStartDate" => "31-Dec-2017",
				"currentInsurer" => "NCD000001",
				"workshop" => "Yes",
				"los" => "Yes",
				"ncdPro" => "Yes",
				"id" => 0,
				"firstName" => Array (
						"0" => "wai hong"
						),
				"lastName" => Array (
						"0" => "phun"
						),
				"icType" => Array (
						"0" => "N"
						),
				"Nric" => Array (
						"0" => "S3619066C"
						),
				"Email" => Array (
						"0" => "waihong.phun@insurediy.com"
						),
				"mobileNo" => Array (
						"0" => "94497248"
						),
				"agreePDC" => "on",
				"agreeMarketing" => true,
				"agreeDeclaration" => "on",
				"vehVariantAxa" => "a",
				"vehVariantMsig" => "a",
				"vehVariantSompo" => "a",
				"ref_id" => "2c56d03d-613f-47a2-a44b-cae708d456b8",
				"plan_id" => "EDP",
				"plan_name" => "ExcelDrive PRESTIGE",
				"partner_id" => "sompo",
				"vehicleBodyType" => "Saloon",
				"vehicleCc" => "1496",
				"vehicleEngineNo" => "engine1234",
				"vehicleChassisNo" => "chassis2234",
				"vehicleSeatingCapacity" => 5,
				"driveToWestMY" => "0",
				"previousPolicyNo" => "123123",
				"previousRegNo" => "SGH2341C",
				"driverNationality" => Array (
						"0" => "SG"
						),
				"driverApplicant" => 0,
				"address" => Array (
						"0" => "11-1234",
						"1" => "321, toa payoh ave 2",
						"2" => "blk 222"
						),
				"state" => "singapore",
				"city" => "singapore",
				"zipCode" => "123456",
				"country" => "SG",
				"loan" => "Yes",
				"loanCompany" => "DBS",
				"currentPolicy" => $policy
		);
		
		$ssoToken = InsureDIYMotorHelper::getSsoToken();
		$requestObj = InsureDIYMotorHelper::sompoPolicyRequestData($postData);
		
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, "http://uat.sompo.com.sg:2052/createPolicy");
		//curl_setopt($ch, CURLOPT_VERBOSE, true);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
				'Content-Type: application/json',
				'Accept: application/json',
				'Data-type: application/json',
				'Authorization: bearer '.$ssoToken->access_token
		)
				);
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestObj));
		
		$server_output = curl_exec ($ch);
		$server_output = json_decode($server_output);
		$errorMsg = curl_error($ch);
		
		curl_close($ch);
		
		$result= array();
		
		if($server_output) {
			return array("request"=>$requestObj, "response"=>$server_output);
		}
	}

	// Mail Stuffs - start
	public function sendEmailsCustomContact($id) {
		$mailer = JFactory::getMailer();
		$params = JComponentHelper::getParams('com_insurediymotor');
		$myInfo = $this->getMyInfo($id);
		$legit = $this->checkBlacklist($myInfo);
		$maildata = $this->getMailData($myInfo);
		$emails = $this->getMails($params, $legit, $myInfo->email);
		$emailbodies = $this->getEmailBodies($params, $maildata);
		$mode = TRUE;

		$mailer->ClearAttachments();
		$mailer->ClearAllRecipients();
		$send = $mailer->sendMail($emails['from'], $emails['fromName'], $emails['service_recipient'], $emails['subject'], $emailbodies['sup_service'], $mode);

	}

	public function sendEmails($pdfarrs, $client = TRUE, $id = FALSE) {
		$mailer = JFactory::getMailer();
		$params = JComponentHelper::getParams('com_insurediymotor');
		$myInfo = $this->getMyInfo($id);
		$legit = $this->checkBlacklist($myInfo);
		$maildata = $this->getMailData($myInfo);
		$emails = $this->getMails($params, $legit, $myInfo->email);
		
		$emailbodies = $this->getEmailBodies($params, $maildata);
		
		$mode = TRUE;
	
		
		if($pdfarrs){
			foreach ($pdfarrs as $pdfarr) {
				$attachments = $this->prepareAttachments($pdfarr, $myInfo->id);
				if ($client && $legit) {
					$mailer->ClearAttachments();
					$mailer->ClearAllRecipients();
					$mailer->sendMail($emails['from'], $emails['fromName'], $emails['recipient'], $emails['subject'], $emailbodies['ce_customer'], $mode, NULL, $emails['bcc'], $attachments);
				} else {
					$mailer->ClearAttachments();
					$mailer->ClearAllRecipients();
					$mailer->sendMail($emails['from'], $emails['fromName'], $emails['service_recipient'], $emails['subject'], $emailbodies['ce_service'], $mode, NULL, $emails['bcc'], $attachments);
				}
			}
		}
		else {
			$mailer->ClearAttachments();
			$mailer->ClearAllRecipients();
			$mailer->sendMail($emails['from'], $emails['fromName'], $emails['service_recipient'], $emails['subject'], $emailbodies['cer_service'], $mode, NULL, $emails['bcc']);
		}
	}
	
	public function prepareAttachments($pdfarr, $id) {
		$temp = array();
		foreach ($pdfarr as $pdf) {
			$temp[] = MyHelper::getDeepPath(QUOTATION_PDF_SAVE_PATH, $id) . $pdf;
		}
		return $temp;
	}
	
	public function getEmailBodies($params, $data) {
		$array = array();
		$array['cover_letter'] = InsureDIYHelper::replaceVariables($params->get("clformat"), $data);
		$array['ce_customer'] = InsureDIYHelper::replaceVariables($params->get("cecformat"), $data);
		$array['ce_service'] = InsureDIYHelper::replaceVariables($params->get("cesformat"), $data);
		$array['cer_service'] = InsureDIYHelper::replaceVariables($params->get("cerformat"), $data);
		$array['sup_service'] = InsureDIYHelper::replaceVariables($params->get("supformat"), $data);
		return $array;
	}
	
	public function getMails($params, $legit, $user_email) {
		$confg = JFactory::getConfig();
		$array = array();
		if ($legit) {
			$array['recipient'] = $user_email;
			$sales_email = $params->get('sales_email');
			$array['bcc'] = array("0" => $sales_email);
			$array['subject'] = 'InsureDIY - Application Form';
		} else {
			$array['recipient'] = $params->get('admin_email');
			$sales_email = $params->get('sales_email');
			$array['bcc'] = array("0" => $sales_email);
			$array['subject'] = 'InsureDIY - Application Form[Blacklist]';
		}
		$array['service_recipient'] = $params->get('service_email', "");
		$array['from'] = $confg->get("mailfrom");
		$array['fromName'] = $confg->get("fromname");
		return $array;
	}
	
	public function getMailData($myInfo) {
		$session = JFactory::getSession();
		$data = array();
		
		$data['first_name'] = $myInfo->contact_firstname;
		$data['last_name'] = $myInfo->contact_lastname;
		$data['order_no'] = $myInfo->unique_order_no;
		$data['product'] = $myInfo->plan_name;
		$data['insurer'] = $myInfo->insurer_code;
		$data['start_date'] = date("d-M-Y", strtotime($myInfo->start_date));
		$data['address'] = $myInfo->address;
		$data['contact_number'] = $myInfo->contact_number;
		
		return $data;
	}
	
	public function checkBlacklist($data) {
		$name = $data->contact_firstname . " " . $data->contact_lastname;
		//		$nationality = $data->contact_country;
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$query->select("count(*)");
		$query->from("#__insure_blacklist");
		$query->where("UPPER(name) = UPPER(" . $db->quote($name) . ")");
		//		$query->where("UPPER(name) = UPPER(" . $db->quote($name) . ") AND nationality = " . $db->quote($nationality));
		$db->setQuery($query);
		$result = $db->loadResult();
		if ($result > 0) {
			return FALSE;
		}
		return TRUE;
	}
	
	public function getMyInfo($id = FALSE) {
		
		$db = JFactory::getDBO();
		
		$query = $db->getQuery(TRUE)
		->select("*")
		->from($this->_motor_quote)
		->where("quote_payment_request_id = " . $db->quote($id) . " AND " . "quote_deleted_time IS NULL");
		$db->setQuery($query);

		$quoteData = $db->loadObject();

	
		if(!$quoteData) {
			return false;
		}
		
		$query = $db->getQuery(TRUE)
		->select("*")
		->from($this->_motor_driver_info)
		->where("quote_master_id = " . $db->quote($quoteData->id));
		$db->setQuery($query);
		$driverData = $db->loadObject();
		
		$resultArr = (object) array(
				"id" => $quoteData->id,
				"contact_firstname" => explode(",", $driverData->quote_driver_first_name)[0],
				"contact_lastname" => explode(",", $driverData->quote_driver_last_name)[0],
				"unique_order_no" => $quoteData->id,
				"plan_name" => $quoteData->quote_policy_selected_plan_code,
				"insurer_code" => $quoteData->quote_policy_selected_partner_code,
				"start_date" => $quoteData->quote_policy_start_date,
				"address" => $driverData->quote_driver_postcode,
				"contact_number" => $driverData->quote_driver_contact,
				"email" => explode(",", $driverData->quote_driver_email)[0]
		);
		
		return $resultArr;
	}
	// Mail Stuffs - end
}
	
?>