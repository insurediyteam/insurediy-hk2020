<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_insurediymotor
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * HTML Article View class for the component
 *
 * @package     Joomla.Site
 * @subpackage  com_insurediymotor
 * @since       1.5
 */
class InsureDIYMotorViewForm extends JViewLegacy {

	protected $form;
	protected $item;
	protected $state;
	protected $result;
	protected $makeModel;
	protected $plans;
	protected $carData;
	protected $loanCompany;

	public function display($tpl = null) {
		$session = JFactory::getSession();
		$model = $this->getModel();
		$params = JComponentHelper::getParams('com_insurediymotor');
		//$mailchimp = JComponentHelper::getParams('com_joomailermailchimpintegration');
		$user = JFactory::getUser();
		$form = $this->get("Form");
		//$quotation = InsureDIYMotorHelper::getQuotation();
		//$quotation_id = InsureDIYMotorHelper::getQid();
		$currentUser = MyHelper::getCurrentUser();

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseWarning(500, implode("\n", $errors));
			return false;
		}
		
		$step = JFactory::getApplication()->input->get("step", FALSE);

		$layout = InsureDIYMotorHelper::getLayout();
		
		if($layout == "step3" || $layout == "step4" || $layout == "step5") {
			$data = $session->get("motor.data", FALSE);
			
			if($layout == "step3" && array_key_exists("login_redirect", $data)) {
				$result = $model->step2save($data);
				
				if($result) {
					$data = $session->get("motor.data", FALSE);
					
					unset($data["login_redirect"]);
					
					$session->set("motor.data", $data);
					
					$this->item = $data;
					
					if(!$model->checkValidity($this->item)) {
						JFactory::getApplication()->redirect(JRoute::_('index.php?option=com_insurediymotor&view=motor&Itemid=159&step=1'));
					}
				}
			}
			else {
				$this->item = $model->getSavedInput();
				
				if(!$model->checkValidity($this->item)) {
					JFactory::getApplication()->redirect(JRoute::_('index.php?option=com_insurediymotor&view=motor&Itemid=159&step=1'));
				}
			}
		}
		
		switch ($layout) {
			case 'step1':
				$this->item = $model->getSavedInput();
				$this->makeModel = $model->getMakeModel(isset($this->item["carMake"]) ? $this->item["carMake"] : "");
				$this->occupation = $model->getOccupationList();
				$this->occupationParent = $model->getOccupationParentList();
				$this->insurer = $model->getInsurerList();
				if($this->item["quoteStep"] == "completed"){
					JFactory::getApplication()->redirect(JRoute::_('index.php?option=com_insurediymotor&view=form&layout=thankyou'));
				}
				break;
			case 'step2':
				$this->item = $session->get("motor.data", FALSE);
				if(!$this->item) {
					JFactory::getApplication()->redirect(JRoute::_('index.php?option=com_insurediymotor&view=motor&Itemid=159&step=1'));
				}
				
				break;
			case 'step3':
				$quote = $model->getSavedInput();
				if($quote["quoteStep"] == "completed"){
					JFactory::getApplication()->redirect(JRoute::_('index.php?option=com_insurediymotor&view=form&layout=thankyou'));
				}
			    //Joomailerintegration start
			    $getdata = $this->item;
			    // $this->test = JoomailerMailchimpHelper::addbeforerenewalMotorCart($user,$getdata);// call function from joomailer helper file
                //Joomailerintegration End
// 				$data = $session->get("motor.data", FALSE);
// 				$this->test = InsureDIYMotorHelper::msigQuoteRequestData($data);
                 $this->user = $user;


// 				 //joomailer get cart Start
//                 $definecart = JoomailerMailchimpHelper::defineCart($email,$name,$lastname,$quotationid,$userid,$url,$product_id,$renewalstore);
//                 if (empty($definecart)){
//                     $delcart = JoomailerMailchimpHelper::delCart($quotationid,$renewalstore);
//                     $addcart = JoomailerMailchimpHelper::addCart($email,$name,$lastname,$quotationid,$userid,$url,$product_id,$renewalstore);
//                     $this->test = 'Empty Scenario';

//                 } else {
//                     $delcart = JoomailerMailchimpHelper::delCart($quotationid,$renewalstore);
//                     $addcart = JoomailerMailchimpHelper::addCart($email,$name,$lastname,$quotationid,$userid,$url,$product_id,$renewalstore);
//                     $this->test = 'not empty scenario';
//                 }


//                $getcart = JoomailerMailchimpHelper::getCart($storeid);
//				$decodegetcart = json_decode($getcart,true);
//				$cartid = $decodegetcart["carts"]["0"]["id"];
//				if ($quotationid == $cartid){
//					$deldata =JoomailerMailchimpHelper::delCart($quotationid);
//					if ($deldata == '204'){
//					$this->test = JoomailerMailchimpHelper::addCart($email,$name,$lastname,$quotationid,$userid,$url,$product_id);
//					}
//				} else {
//					$this->test = JoomailerMailchimpHelper::addCart($email,$name,$lastname,$quotationid,$userid,$url,$product_id);
//				}
				 //joomailer get cart end
// 				if($data) {
// 					if(array_key_exists("login_redirect", $data)) {
// 						$result = $model->step2save($data);
						
// 						if($result) {
// 							unset($data["login_redirect"]);
							
// 							$session->set("motor.data", $data);
// 						}
// 					}
// 				}
				$this->plans = $model->getSavedPlans();
				$this->benefits = $model->getPartnerBenefits();
				$this->makeModel = $model->getModelVariant($this->item["carModel"], $this->item["carMake"], $this->item["carMakeYear"]);
				$this->carData = $model->getModelName($this->item["carMake"], $this->item["carModel"]);
				
				if(!$this->item || !$this->carData) {
					JFactory::getApplication()->redirect(JRoute::_('index.php?option=com_insurediymotor&view=motor&Itemid=159&step=1'));
				}
				
				if($this->item["currentInsurer"] != "null") {
					$check = InsureDIYMotorHelper::getRenewalInfo($this->item["currentInsurer"]);
					
					if($check) {
						$this->renewal = $check;
					}
				}
				
				break;
			case 'step4':
				$quote = $model->getSavedInput();
				if($quote["quoteStep"] == "completed"){
					JFactory::getApplication()->redirect(JRoute::_('index.php?option=com_insurediymotor&view=form&layout=thankyou'));
				}
				$this->makeModel = $model->getSavedCarData($this->item["quotationId"], $this->item["selectedPartner"]);
				$this->bodyType = $model->getBodyType();
				$this->occupation = $model->getOccupationList();
				$this->occupationParent = $model->getOccupationParentList();
				$this->loanCompany = $model->getloanCompanyList();
				$this->result = $session->get("motor.result", FALSE);
				
				if(!$this->item || !$this->makeModel || $this->item["preChange"] == "true") {
					JFactory::getApplication()->redirect(JRoute::_('index.php?option=com_insurediymotor&view=motor&Itemid=159&step=3'));
				}
				break;
			case 'step5':
				$quote = $model->getSavedInput();
				if($quote["quoteStep"] == "completed"){
					JFactory::getApplication()->redirect(JRoute::_('index.php?option=com_insurediymotor&view=form&layout=thankyou'));
				}
				$this->occupationParent = $model->getOccupationParentList();
				$this->bodyType = $model->getBodyType();
			    $storeid = 'insurediysingapore';
				if(!$session->get("motor.redirected", false)) {
					$result = $model->step4save($this->item);
					
					if(!$result) {
						JFactory::getApplication()->redirect(JRoute::_('index.php?option=com_insurediymotor&view=motor&Itemid=159&step=4'));
					}
				}
				
				$this->makeModel = $model->getSavedCarData($this->item["quotationId"], $this->item["selectedPartner"]);
				$this->paymentData = $model->getPaymentData($this->item["selectedPartner"], $this->item["plan_premium"], $this->item["quotationId"]);
				$getdata = $this->item;
				$quotationid = $getdata['quotationId'];
//                $deldata =JoomailerMailchimpHelper::delCart($quotationid,$storeid);
//                $this->test = $deldata;
                if(!$this->item || !$this->makeModel || $this->item["preChange"] == "true") {
					JFactory::getApplication()->redirect(JRoute::_('index.php?option=com_insurediymotor&view=motor&Itemid=159&step=1'));
				}
				
				if(!is_array($this->paymentData)) {
					if($this->paymentData) {
                        $deldata =JoomailerMailchimpHelper::delCart($quotationid,$storeid);

                        JFactory::getApplication()->redirect("index.php?option=com_insurediymotor&view=form&layout=thankyou");
					}
					else {
						JFactory::getApplication()->redirect(JRoute::_('index.php?option=com_insurediymotor&view=motor&Itemid=159&step=4'));
					}
				}
				
				$session->set("motor.redirected", false);
				
				break;
				
			case 'login':
				$this->return = $step;
				$data = $session->get("motor.data", FALSE);
				if(isset($data["Email"])) {
					$this->emailExist = $model->isEmailRegistered($data["Email"][0]);
				}
				else {
					$this->emailExist = "";
				}
				break;
			default:
				break;
		}
		
		//$this->quotation = $quotation;
		$this->setLayout($layout);
		$this->form = $form;
		$this->state = $this->get('State');
		$this->currency = $params->get("currency", "HK$");
		$this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));
		$this->params = $params;
		$this->user = $user;
		$this->current_user = $currentUser;
		$this->flag_banner_path = 'images/flag_banners';
		$this->_prepareDocument();
		
		switch($layout) {
			case "thankyou":
				$this->item = $session->get("motor.data", FALSE);
				$this->result = $session->get("motor.result", FALSE);
				$this->docs = $model->getDriverDocument($user->id);

				//InsureDIYMotorHelper::clearSessionData();
			case "cancelled":
				$this->result = $session->get("motor.result", FALSE);
				//InsureDIYMotorHelper::clearSessionData();
			break;	
		}
		
		parent::display($tpl);
	}

	/**
	 * Prepares the document
	 */
	protected function _prepareDocument() {
		$app = JFactory::getApplication();
		$menus = $app->getMenu();
		$title = null;

		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();

		$head = JText::_('COM_INSUREDIYMOTOR_FORM_PAGE_HEADING');
		
// 		if (empty($this->item->id)) {
// 			$head = JText::_('COM_INSUREDIYMOTOR_FORM_PAGE_HEADING');
// 		} else {
// 			$head = JText::_('COM_INSUREDIYMOTOR_FORM_EDIT_PAGE_HEADING');
// 		}

		if ($menu) {
// 			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
			
			
			$description = $menu->params->get('menu-meta_description');
			
			if ($description) {
				$this->document->setDescription($menu->params->get('menu-meta_description'));
			}
			
			if ($menu->params->get('menu-meta_keywords')) {
				$this->document->setMetadata('keywords', $menu->params->get('menu-meta_keywords'));
			}
			
			if ($menu->params->get('robots')) {
				$this->document->setMetadata('robots', $menu->params->get('robots'));
			}
		}
		
		$this->params->def('page_heading', $head);

		$title = $this->params->def('page_title', $head);
		if ($app->getCfg('sitename_pagetitles', 0) == 1) {
			$title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
		} elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
			$title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
		}
		$this->document->setTitle($title);

		//facebook stuffs
		$this->document->setMetaData("og:title", InsureDIYMotorHelper::getFbTitle());
		$this->document->setMetaData("og:description", InsureDIYMotorHelper::getFbDesc());
		$this->document->setMetaData("og:image", InsureDIYMotorHelper::getFbImage());
		$this->document->setMetaData("og:app_id", InsureDIYMotorHelper::getFbAppId());
		$this->document->setMetaData("og:site_name", InsureDIYMotorHelper::getFbSiteName());
	}

	protected function getComparisonTbRows() {
		$rows = array(
		);
		return $rows;
	}

}
