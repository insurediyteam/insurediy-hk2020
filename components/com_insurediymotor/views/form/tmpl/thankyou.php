<?php
	/**
		* @package     Joomla.Site
		* @subpackage  com_insurediymotor
		*
		* @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
		* @license     GNU General Public License version 2 or later; see LICENSE.txt
	*/
	defined('_JEXEC') or die;
	
	JHtml::_('behavior.keepalive');
	JHtml::_('behavior.formvalidation');
	JHtml::_('formbehavior.chosen', '.insurediy-form-content select');
	JHtml::_('script', 'system/loadingoverlay.min.js', false, true);
	JHtml::_('script', 'system/select2.min.js', false, true);
	JHtml::_('stylesheet', 'system/select2.css', false, true);
	
	//JHtml::_('behavior.modal', 'a.modal_jform_contenthistory');
	// Create shortcut to parameters.
	$params = $this->state->get('params');
	
	$result = $this->result;

	$pid = $_GET["pid"];
	$docs = $this->docs;
	$selectPid = $docs;

	if (isset($pid) && $pid != ""){
		$docs = $docs[$pid];
		$driverCount = $docs["driverCount"];
	} else {
		$pid = $docs["propsalId"];
		$driverCount = $docs["driverCount"];
	}

?>

<script type="text/javascript">
	window.addEvent('domready', function() {
		var myAccordion = new Fx.Accordion($$('.togglers'), $$('.step2-checkbox'), {display: -1, alwaysHide: true});
	});
	
	function clickFileType(div_id) {
		$(div_id).click();
	}
	
	function copyPasteText(div_id, text) {
		$(div_id).setProperty('value', text);
	}
</script>

<div class="insurediy-form montserat-font" style="background-color:#fff;">
	<div class="motor-header-top-wrapper">
			<?php echo InsureDIYMotorHelper::renderHeader('icon-travel', JText::_('COM_INSUREDIYMOTOR_PAGE_HEADING'), 5); ?>
	</div>
	<form class="upload-login" style="padding: 30px;width: 50%;margin: 0 auto;border: 2px solid #2196F3;border-radius: 10px;<?php echo ($docs["login"] == false) ? "display:block;" : "display:none;" ;  ?>">
		<span style="font-size: 18px;text-align: center;display: block;margin-bottom: 20px;color: #F44336;font-weight: 500;">Please login to continue</span>
		<div class="control-group">
			<div class="control-label">
				<label id="jform_email-lbl" for="jform_email" class="required">Email</label>
			</div>
			<div class="controls">
				<input style="width: 100%;padding: 4px 12px;height: 38px;" class="required" type="email" id="jform_email" value="" name="username">
			</div>
		</div>
		<div class="control-group">
			<div class="control-label">
				<label id="jform_password-lbl" for="jform_password" class="required">Password</label>
			</div>
			<div class="controls">
				<input style="width: 100%;padding: 4px 12px;height: 38px;" class="required" type="password" id="jform_password" value="" name="password">
			</div>
		</div>
		<input type="hidden" name="option" value="com_insurediymotor" />
		<input type="hidden" name="task" value="user.ajaxLogin" />
		<input type="hidden" name="return" value="" />
		<?php echo JHtml::_('form.token'); ?>
		<div class="btn-toolbar" style="">
			<button type="submit" class="motor-btn upload-login-btn" style="padding: 10px 20px 10px 20px;text-transform: uppercase;">Login</button>
		</div>
	</form>
	<div class="edit<?php echo $this->pageclass_sfx; ?> insurediy-motor-form-content center">
		<div class="input-wrapper" style="margin-bottom: 40px;<?php echo ($selectPid["single"] || $docs["login"] == false) ? "display:none;" : ""; ?>">
			<span class="input-title" style="margin-right:0;">Select your proposal ID</span>
			<div class="input-container-block">
				<div class="select_mate" data-mate-select="active" style="width:100%;margin-top:0;">
					<select name="pid" class="hide-select custom-select">
						<option value="" selected>Select Proposal ID</option>
							<?php foreach($selectPid as $do):?>
									<option value="<?php echo $do["propsalId"]; ?>" <?php if(isset($pid)) if($pid == $do["propsalId"]) echo 'selected="selected"'; ?>><?php echo ucwords($do["propsalId"]); ?></option>
							<?php endforeach;?>
					</select>
				</div>
			</div>
		</div>
		<div class="delay" <?php echo (empty($result["data"]) ? "style=display:none;" : ""); ?>>
		<div class="insurediy-thankyou-top-container">
			<img src="images/websiteicon/thankyou-icon.png" class="insurediy-thankyou-tick-icon"/>
		</div>
		<div class="insurediy-thankyou-middle-container">
			<span class="insurediy-thankyou-h1">
				Thank You!
			</span>
			<span class="insurediy-thankyou-h2">
				for choosing to purchase your <br>policy through InsureDIY Limited.
			</span>
		</div>
		</div>
		<?php if(isset($result["data"])): ?>
		<div class="insurediy-thankyou-middle-container" style="margin-bottom:25px;">
			<?php if(isset($result["data"]->transactionId)): ?>
				<span class="insurediy-thankyou-h5">
					<b>Payment Transaction ID: </b> <span style="color: #5197d5;"><?php echo $result["data"]->transactionId; ?></span>
				</span>
				<br>
			<?php endif; ?>
			<?php if(isset($result["data"]->proposalId)): ?>
				<span class="insurediy-thankyou-h5">
					<b>Insurance Proposal ID: </b> <span style="color: #5197d5;"><?php echo $result["data"]->proposalId; ?></span>
				</span>
			<?php endif; ?>
				<br>
				<!-- <span class="insurediy-thankyou-h2">	
				</span> -->
		</div>
		<?php endif; ?>
		<div class="idd_section" style="<?php echo (!$selectPid["single"] && !isset($pid)) ? "display:none;" : ""; ?>">  
				<div class="idd_blue_bar">
					<span>Upload Documents Proposal ID:  <?php echo $pid; ?> </span>
				</div>
				<div class="confirm-upload" style="display:none;">Please make sure you have completed document upload before leaving.</div>
				<div class="idd_blue_content motor-upload">
					<fieldset class="insurediy-form-fields insurediy-form-fields" style="margin-bottom: 0;<?php echo ($docs["isFinish"] == "1") ? "display:none;" : "" ;  ?>">
						<div class="idd_step3_helper_info">
							<span>
								<strong>In order to process your policy, you are required to submit the following supporting documents.</strong><br>
								We accept scans or photos. If you do not upload the information here now, you can email the attachments to us at <a href="mailto:customer_service@insurediy.com"><strong>customer_service@insurediy.com</strong></a>.
							</span>
							<br><br>
							<span style="font-size:12px;">
								Files accepted: ".pdf", ".doc", ".docx", ".jpg", ".png", ".gif"
							</span>
						</div>
						<div class="idd_step3_helper_info driver1">
							<div class="idd_column control-group" >
								<div class="control-label" style="font-weight:bold;">Copy of Vehicle Registration Document</div>
								<form class="vehicleForm" enctype="multipart/form-data" autocomplete="off" data-name="file_vehicleReg">
								<div class="file-container">
									<input type="file" id="file_vehicleReg" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" />
									<label for="file_vehicleReg" class="vehicleRegForm">
										<span></span>
										<strong class="upButton">
											<svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
												<path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
											</svg>
											Choose a file&hellip;
										</strong>
									</label>
									<div class="removeFile"><img src="images/close-mark.png">Remove File</div>
								</div>
								</form>
							</div>
							<div class="idd_column control-group" >
								<div class="control-label" style="font-weight:bold;">Copy of Insured Driver’s Driving License</div>
								<form class="vehicleForm" enctype="multipart/form-data" autocomplete="off" data-name="file_insured">
								<div class="file-container">
									<input type="file" id="file_insured" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" />
									<label for="file_insured" class="vehicleRegForm">
										<span></span>
										<strong class="upButton">
											<svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
												<path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
											</svg>
											Choose a file&hellip;
										</strong>
									</label>
									<div class="removeFile"><img src="images/close-mark.png">Remove File</div>
								</div>
								</form>
							</div>
							<div class="idd_column control-group" >
								<div class="control-label" style="font-weight:bold;">Copy of Insured Driver’s HKID Card</div>
								<form class="vehicleForm" enctype="multipart/form-data" autocomplete="off" data-name="file_hkid">
								<div class="file-container">
									<input type="file" id="file_hkid" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" />
									<label for="file_hkid" class="vehicleRegForm">
										<span></span>
										<strong class="upButton">
											<svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
												<path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
											</svg>
											Choose a file&hellip;
										</strong>
									</label>
									<div class="removeFile"><img src="images/close-mark.png">Remove File</div>
								</div>
								</form>
							</div>
							<div class="idd_column control-group" >
								<div class="control-label" style="font-weight:bold;">Updated Renewal Notice</div>
								<form class="vehicleForm" enctype="multipart/form-data" autocomplete="off" data-name="file_renewal">
								<div class="file-container">
									<input type="file" id="file_renewal" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" />
									<label for="file_renewal" class="vehicleRegForm">
										<span></span>
										<strong class="upButton">
											<svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
												<path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
											</svg>
											Choose a file&hellip;
										</strong>
									</label>
									<div class="removeFile"><img src="images/close-mark.png">Remove File</div>
								</div>
								</form>
							</div>
							<div class="idd_column control-group" >
								<div class="control-label" style="font-weight:bold;">NCD Confirmation Proof from Previous Insurer (if NCD is not 0)</div>
								<form class="vehicleForm" enctype="multipart/form-data" autocomplete="off" data-name="file_ncd">
								<div class="file-container">
									<input type="file" id="file_ncd" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" />
									<label for="file_ncd" class="vehicleRegForm">
										<span></span>
										<strong class="upButton">
											<svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
												<path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
											</svg>
											Choose a file&hellip;
										</strong>
									</label>
									<div class="removeFile"><img src="images/close-mark.png">Remove File</div>
								</div>
								</form>
							</div>
						</div>
						<?php if($driverCount > 1): ?>
						<div style="font-size: 20px;font-weight: 600;margin-top: 40px;color: #03A9F4;">Additional Driver Documents</div>
						
						<div class="idd_step3_helper_info driver2">
							<div class="idd_column control-group" >
								<div class="control-label" style="font-weight:bold;">Copy of Named Driver 2’s Driving License</div>
								<form class="vehicleForm" enctype="multipart/form-data" autocomplete="off" data-name="file_insured_2">
								<div class="file-container">
									<input type="file" id="file_insured_2" class="inputfile inputfile-6" data-multiple-caption="{count} files selected"  />
									<label for="file_insured_2" class="vehicleRegForm">
										<span></span>
										<strong class="upButton">
											<svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
												<path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
											</svg>
											Choose a file&hellip;
										</strong>
									</label>
									<div class="removeFile"><img src="images/close-mark.png">Remove File</div>
								</div>
								</form>
							</div>
							<div class="idd_column control-group" >
								<div class="control-label" style="font-weight:bold;">Copy of Named Driver 2’s HKID Card</div>
								<form class="vehicleForm" enctype="multipart/form-data" autocomplete="off" data-name="file_hkid_2">
								<div class="file-container">
									<input type="file" id="file_hkid_2" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" />
									<label for="file_hkid_2" class="vehicleRegForm">
										<span></span>
										<strong class="upButton">
											<svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
												<path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
											</svg>
											Choose a file&hellip;
										</strong>
									</label>
									<div class="removeFile"><img src="images/close-mark.png">Remove File</div>
								</div>
								</form>
							</div>
						</div>
						<?php endif; ?>
						<?php if($driverCount > 2): ?>
						<div class="idd_step3_helper_info driver3">
							<div class="idd_column control-group" >
								<div class="control-label" style="font-weight:bold;">Copy of Named Driver 3’s Driving License</div>
								<form class="vehicleForm" enctype="multipart/form-data" autocomplete="off" data-name="file_insured_3">
								<div class="file-container">
									<input type="file" id="file_insured_3" class="inputfile inputfile-6" data-multiple-caption="{count} files selected"  />
									<label for="file_insured_3" class="vehicleRegForm">
										<span></span>
										<strong class="upButton">
											<svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
												<path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
											</svg>
											Choose a file&hellip;
										</strong>
									</label>
									<div class="removeFile"><img src="images/close-mark.png">Remove File</div>
								</div>
								</form>
							</div>
							<div class="idd_column control-group" >
								<div class="control-label" style="font-weight:bold;">Copy of Named Driver 3’s HKID Card</div>
								<form class="vehicleForm" enctype="multipart/form-data" autocomplete="off" data-name="file_hkid_3">
								<div class="file-container">
									<input type="file" id="file_hkid_3" class="inputfile inputfile-6" data-multiple-caption="{count} files selected" />
									<label for="file_hkid_3" class="vehicleRegForm">
										<span></span>
										<strong class="upButton">
											<svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17">
												<path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/>
											</svg>
											Choose a file&hellip;
										</strong>
									</label>
									<div class="removeFile"><img src="images/close-mark.png">Remove File</div>
								</div>
								</form>
							</div>
						</div>
						<?php endif; ?>
						
						<button class="finish-btn" type="button" style="font-family: 'Montserrat', sans-serif;background: #FF9800;color: #fff;border: none;font-size: 16px;padding: 15px 30px;font-weight: 600;margin-top: 30px;">SUBMIT</button>
					</fieldset>
					<div class="thankyou-upload" style="text-align: center;font-size: 20px;font-weight: 600;line-height: 29px;<?php echo ($docs["isFinish"] == "1") ? "" : "display:none;" ;  ?>"><img src="images/websiteicon/thankyou-icon.png" style="width: 100px;display: block;margin: 20px auto;"/>Thank you for submitting your documents. Your application is being processed</div>
				</div>
		</div>
		<div class="insurediy-thankyou-bottom-container" style="<?php echo ($docs["login"] == false) ? "display:none;" : "" ;  ?>">
			<span class="insurediy-thankyou-h3">
				Your policy will be emailed to you shortly.
			</span>
			<br>
			<span class="insurediy-thankyou-h4">
				<?php echo JText::_("COM_INSUREDIYMOTOR_EXPLANATION_OUR_STAFFS_WILL_CONTACT"); ?>
			</span>
			
		</div>
		
		<!-- 
		<div class="edit<?php echo $this->pageclass_sfx; ?>">
			<div class="form-quote-box-2" style="width: 790px">
				<span style="color:#2f2f2f;font-size:32px;line-height: 40px;">
					<?php echo JText::_("COM_INSUREDIYMOTOR_PAGE_SUB_HEADING_THANK_YOU_FOR_QUOTATION"); ?>
				</span>
				<div class="center" style="color:#2f2f2f;font-size:14px;margin-top: 15px; margin-left:20%; text-align:left;">
					<?php if(isset($result["data"])): ?>
					<?php if(isset($result["data"]->transactionId)): ?>
					<span style="width: 200px;display: inline-block;">Payment Transaction ID </span>: <span style="font-style: italic;color: #5197d5;"><?php echo $result["data"]->transactionId; ?></span>
					<?php endif; ?>
					<?php endif; ?>
					<?php if(isset($result["data"])): ?>
					<?php if(isset($result["data"]->proposalId)): ?>
					<br>
					<span style="width: 200px;display: inline-block;">Insurance Proposal ID </span>: <span style="font-style: italic;color: #5197d5;"><?php echo $result["data"]->proposalId; ?></span>
					<?php endif; ?>
					<?php endif; ?>
				</div>
				<div class="center" style="color:#2f2f2f;font-size:14px;margin-top: 15px;">
					<?php echo JText::_("COM_INSUREDIYMOTOR_EXPLANATION_OUR_STAFFS_WILL_CONTACT"); ?>
				</div>
			</div>
		</div>
		 -->
	</div>
</div>

<div class="logo-slider"><?php echo MyHelper::load_module(102); ?></div>
<?php 
	$jinput = JFactory::getApplication()->input;
	$params = JComponentHelper::getParams('com_insurediymotor');
	$ch = curl_init();
	
	if($jinput->get("authorized") == true) {
		if($jinput->get("Ref")) {
			$txnId = $jinput->get("Ref");
			
			$postData = array(
					"merchantId" => $params->get('merchantId', ""),
					"loginId" => $params->get('apiloginId', ""),
					"password" => $params->get('apiPassword', ""),
					"actionType" => "Query",
					"orderRef" => $txnId
			);
			
			curl_setopt($ch, CURLOPT_URL, $params->get('apiUrl', ""));
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
			
			$server_output = curl_exec ($ch);
			$server_output = json_encode(simplexml_load_string($server_output, "SimpleXMLElement", LIBXML_NOCDATA));
			$server_output = json_decode($server_output,TRUE);
			$server_output = $server_output["record"];
			$errorMsg = curl_error($ch);
			
			curl_close($ch);
			
			$quoteId = "";
			$paidAmt = "";
			$planName = "";
			$planId = "";
			$selectedPartner = "";
			
			if($server_output) {
				$quoteId = "offline-". $txnId;			
				$paidAmt = $server_output["amt"];
				$planName = $jinput->get("planName");
				$planId = $jinput->get("planId");
				$selectedPartner = $jinput->get("selectedPartner");
			}
		}
	}
	else {
		$quoteId = $this->item['quotationId'];
		$paidAmt = $this->item['plan_premium'];
		$planName = $this->item['plan_name'];
		$planId = $this->item['plan_id'];
		$selectedPartner = $this->item['selectedPartner'];
		
		if(isset($this->item['partnerId'])) {
			if($this->item['partnerId'] == "msig" || $this->item['partnerId'] == "hla") {
				$paidAmt = $this->item['plan_premium']-30;
			}
		}
	}
?>
<script>
	<?php if($docs["login"] == false && isset($pid)): ?>
	window.location.href = window.location.pathname+"?option=com_insurediymotor&view=form&layout=thankyou";
	<?php endif; ?>
	jQuery("select[name='pid']").on('change', function(obj){
		var newPid = jQuery("select[name='pid']").val();
		window.location.href = window.location.pathname+"?option=com_insurediymotor&view=form&layout=thankyou&pid="+newPid;
	});
	jQuery(document).ready(function(){
		jQuery('.custom-select').select2({width: '50%'});
	});
	var docs = <?php echo json_encode($docs); ?>;
	var myForm = jQuery("#file_vehicleReg").parents('form:first');
	var dataName = jQuery(myForm).attr("data-name");
	var removeBox = jQuery(myForm).find(".removeFile");
	var inputBox = jQuery(myForm).find(".vehicleRegForm");
	var uploadButton = jQuery(myForm).find(".upButton");
	var filenameHolder = jQuery(myForm).find("span");
	if(docs["registrationDoc"] != ""){
		var upButton = '<img src="images/correct-symbol.png" style="width:14px;margin-right:5px;"> File Uploaded';
		jQuery(uploadButton).html(upButton).css("background-color", "#4caf50");
		jQuery(inputBox).css("border", "2px solid #4caf50");
		jQuery(removeBox).fadeIn();
		jQuery(removeBox).attr("data-filename", docs["registrationDoc"]);
		jQuery(removeBox).attr("data-type", docs["registrationDocType"]);
		jQuery(inputBox).children().bind('click', function(){ return false; });
		jQuery(inputBox).children().css("cursor", "no-drop");
		jQuery(filenameHolder).html(docs["registrationDoc"]);
	}
	var myForm = jQuery("#file_insured").parents('form:first');
	var dataName = jQuery(myForm).attr("data-name");
	var removeBox = jQuery(myForm).find(".removeFile");
	var inputBox = jQuery(myForm).find(".vehicleRegForm");
	var uploadButton = jQuery(myForm).find(".upButton");
	var filenameHolder = jQuery(myForm).find("span");
	if(docs["drivingLicense"] != ""){
		var upButton = '<img src="images/correct-symbol.png" style="width:14px;margin-right:5px;"> File Uploaded';
		jQuery(uploadButton).html(upButton).css("background-color", "#4caf50");
		jQuery(inputBox).css("border", "2px solid #4caf50");
		jQuery(removeBox).fadeIn();
		jQuery(removeBox).attr("data-filename", docs["drivingLicense"]);
		jQuery(removeBox).attr("data-type", docs["drivingLicenseType"]);
		jQuery(inputBox).children().bind('click', function(){ return false; });
		jQuery(inputBox).children().css("cursor", "no-drop");
		jQuery(filenameHolder).html(docs["drivingLicense"]);
	}
	var myForm = jQuery("#file_insured_2").parents('form:first');
	var dataName = jQuery(myForm).attr("data-name");
	var removeBox = jQuery(myForm).find(".removeFile");
	var inputBox = jQuery(myForm).find(".vehicleRegForm");
	var uploadButton = jQuery(myForm).find(".upButton");
	var filenameHolder = jQuery(myForm).find("span");
	if(docs["drivingLicense_2"] != ""){
		var upButton = '<img src="images/correct-symbol.png" style="width:14px;margin-right:5px;"> File Uploaded';
		jQuery(uploadButton).html(upButton).css("background-color", "#4caf50");
		jQuery(inputBox).css("border", "2px solid #4caf50");
		jQuery(removeBox).fadeIn();
		jQuery(removeBox).attr("data-filename", docs["drivingLicense_2"]);
		jQuery(removeBox).attr("data-type", docs["drivingLicenseType_2"]);
		jQuery(inputBox).children().bind('click', function(){ return false; });
		jQuery(inputBox).children().css("cursor", "no-drop");
		jQuery(filenameHolder).html(docs["drivingLicense_2"]);
	}
	var myForm = jQuery("#file_insured_3").parents('form:first');
	var dataName = jQuery(myForm).attr("data-name");
	var removeBox = jQuery(myForm).find(".removeFile");
	var inputBox = jQuery(myForm).find(".vehicleRegForm");
	var uploadButton = jQuery(myForm).find(".upButton");
	var filenameHolder = jQuery(myForm).find("span");
	if(docs["drivingLicense_3"] != ""){
		var upButton = '<img src="images/correct-symbol.png" style="width:14px;margin-right:5px;"> File Uploaded';
		jQuery(uploadButton).html(upButton).css("background-color", "#4caf50");
		jQuery(inputBox).css("border", "2px solid #4caf50");
		jQuery(removeBox).fadeIn();
		jQuery(removeBox).attr("data-filename", docs["drivingLicense_3"]);
		jQuery(removeBox).attr("data-type", docs["drivingLicenseType_3"]);
		jQuery(inputBox).children().bind('click', function(){ return false; });
		jQuery(inputBox).children().css("cursor", "no-drop");
		jQuery(filenameHolder).html(docs["drivingLicense_3"]);
	}
	var myForm = jQuery("#file_hkid").parents('form:first');
	var dataName = jQuery(myForm).attr("data-name");
	var removeBox = jQuery(myForm).find(".removeFile");
	var inputBox = jQuery(myForm).find(".vehicleRegForm");
	var uploadButton = jQuery(myForm).find(".upButton");
	var filenameHolder = jQuery(myForm).find("span");
	if(docs["hkid"] != ""){
		var upButton = '<img src="images/correct-symbol.png" style="width:14px;margin-right:5px;"> File Uploaded';
		jQuery(uploadButton).html(upButton).css("background-color", "#4caf50");
		jQuery(inputBox).css("border", "2px solid #4caf50");
		jQuery(removeBox).fadeIn();
		jQuery(removeBox).attr("data-filename", docs["hkid"]);
		jQuery(removeBox).attr("data-type", docs["hkidType"]);
		jQuery(inputBox).children().bind('click', function(){ return false; });
		jQuery(inputBox).children().css("cursor", "no-drop");
		jQuery(filenameHolder).html(docs["hkid"]);
	}
	var myForm = jQuery("#file_hkid_2").parents('form:first');
	var dataName = jQuery(myForm).attr("data-name");
	var removeBox = jQuery(myForm).find(".removeFile");
	var inputBox = jQuery(myForm).find(".vehicleRegForm");
	var uploadButton = jQuery(myForm).find(".upButton");
	var filenameHolder = jQuery(myForm).find("span");
	if(docs["hkid_2"] != ""){
		var upButton = '<img src="images/correct-symbol.png" style="width:14px;margin-right:5px;"> File Uploaded';
		jQuery(uploadButton).html(upButton).css("background-color", "#4caf50");
		jQuery(inputBox).css("border", "2px solid #4caf50");
		jQuery(removeBox).fadeIn();
		jQuery(removeBox).attr("data-filename", docs["hkid_2"]);
		jQuery(removeBox).attr("data-type", docs["hkidType_2"]);
		jQuery(inputBox).children().bind('click', function(){ return false; });
		jQuery(inputBox).children().css("cursor", "no-drop");
		jQuery(filenameHolder).html(docs["hkid_2"]);
	}
	var myForm = jQuery("#file_hkid_3").parents('form:first');
	var dataName = jQuery(myForm).attr("data-name");
	var removeBox = jQuery(myForm).find(".removeFile");
	var inputBox = jQuery(myForm).find(".vehicleRegForm");
	var uploadButton = jQuery(myForm).find(".upButton");
	var filenameHolder = jQuery(myForm).find("span");
	if(docs["hkid_3"] != ""){
		var upButton = '<img src="images/correct-symbol.png" style="width:14px;margin-right:5px;"> File Uploaded';
		jQuery(uploadButton).html(upButton).css("background-color", "#4caf50");
		jQuery(inputBox).css("border", "2px solid #4caf50");
		jQuery(removeBox).fadeIn();
		jQuery(removeBox).attr("data-filename", docs["hkid_3"]);
		jQuery(removeBox).attr("data-type", docs["hkidType_3"]);
		jQuery(inputBox).children().bind('click', function(){ return false; });
		jQuery(inputBox).children().css("cursor", "no-drop");
		jQuery(filenameHolder).html(docs["hkid_3"]);
	}
	var myForm = jQuery("#file_renewal").parents('form:first');
	var dataName = jQuery(myForm).attr("data-name");
	var removeBox = jQuery(myForm).find(".removeFile");
	var inputBox = jQuery(myForm).find(".vehicleRegForm");
	var uploadButton = jQuery(myForm).find(".upButton");
	var filenameHolder = jQuery(myForm).find("span");
	if(docs["renewalNotice"] != ""){
		var upButton = '<img src="images/correct-symbol.png" style="width:14px;margin-right:5px;"> File Uploaded';
		jQuery(uploadButton).html(upButton).css("background-color", "#4caf50");
		jQuery(inputBox).css("border", "2px solid #4caf50");
		jQuery(removeBox).fadeIn();
		jQuery(removeBox).attr("data-filename", docs["renewalNotice"]);
		jQuery(removeBox).attr("data-type", docs["renewalNoticeType"]);
		jQuery(inputBox).children().bind('click', function(){ return false; });
		jQuery(inputBox).children().css("cursor", "no-drop");
		jQuery(filenameHolder).html(docs["renewalNotice"]);
	}
	var myForm = jQuery("#file_ncd").parents('form:first');
	var dataName = jQuery(myForm).attr("data-name");
	var removeBox = jQuery(myForm).find(".removeFile");
	var inputBox = jQuery(myForm).find(".vehicleRegForm");
	var uploadButton = jQuery(myForm).find(".upButton");
	var filenameHolder = jQuery(myForm).find("span");
	if(docs["ncd"] != ""){
		var upButton = '<img src="images/correct-symbol.png" style="width:14px;margin-right:5px;"> File Uploaded';
		jQuery(uploadButton).html(upButton).css("background-color", "#4caf50");
		jQuery(inputBox).css("border", "2px solid #4caf50");
		jQuery(removeBox).fadeIn();
		jQuery(removeBox).attr("data-filename", docs["ncd"]);
		jQuery(removeBox).attr("data-type", docs["ncdType"]);
		jQuery(inputBox).children().bind('click', function(){ return false; });
		jQuery(inputBox).children().css("cursor", "no-drop");
		jQuery(filenameHolder).html(docs["ncd"]);
	}
	jQuery('.inputfile').on('change', function( e ){
			var $label	 = jQuery(this).next( 'label' );
			var labelVal = $label.html();
			var fileName = '';

			if( this.files && this.files.length > 1 )
				fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
			else if( e.target.value )
				fileName = e.target.value.split( '\\' ).pop();

			if( fileName )
				$label.find( 'span' ).html( fileName );
			else
				$label.html( labelVal );

			jQuery(this).parents('form:first').submit();
	});
	jQuery( '.inputfile' )
		.on( 'focus', function(){ this.addClass( 'has-focus' ); })
		.on( 'blur', function(){ this.removeClass( 'has-focus' ); });

	jQuery(function() {
		setTimeout(function() {
			jQuery(".delay").slideUp()
		}, 4000);
	});

		jQuery(".finish-btn").click(function() {
			if (confirm('Please make sure you have completed uploading all the required documents')) {
				var formData = new FormData();
				formData.append("pid", "<?php echo $pid ?>");
				jQuery.ajax({
				type: "POST",
				url: "<?php echo JURI::root().'index.php?option=com_insurediymotor&task=ajax.uploadFinish&format=json'; ?>",
				data: formData,
				dataType: "json",
				contentType: false,
			    processData: false,
				beforeSend: function() {
					jQuery(".finish-btn").LoadingOverlay("show");
				},
				success: function(result){
					if(result["success"] == true) {
						jQuery(".finish-btn").LoadingOverlay("hide");
						window.scrollTo(0, 0);
						jQuery("fieldset").delay(500).fadeOut();
						jQuery(".thankyou-upload").delay(500).fadeIn();
					}
				},
				error: function(xhr, textStatus, errorThrown){
					jQuery(".finish-btn").LoadingOverlay("hide");
				}
				});
			}
		});
		jQuery(".vehicleForm").submit(function(e) {
		    e.preventDefault();    

		    var file = jQuery(this).find("input")[0].files[0];
			var dataName = jQuery(this).attr("data-name");
		    var formData = new FormData();
			var removeBox = jQuery(this).find(".removeFile");
			var inputBox = jQuery(this).find(".vehicleRegForm");
			var uploadButton = jQuery(this).find(".upButton");
			var filenameHolder = jQuery(this).find("span");
		    
		    formData.append(dataName, file);
			formData.append("pid", "<?php echo $pid ?>");
		    
			jQuery.ajax({
				type: "POST",
				url: "<?php echo JURI::root().'index.php?option=com_insurediymotor&task=ajax.uploadFile&format=json'; ?>",
				data: formData,
				dataType: "json",
				contentType: false,
			    processData: false,
				beforeSend: function() {
					jQuery(inputBox).LoadingOverlay("show");
				},
				success: function(result){
					if(result["success"] == true) {
						jQuery(inputBox).LoadingOverlay("hide");
						var upButton = '<img src="images/correct-symbol.png" style="width:14px;margin-right:5px;"> File Uploaded';
						jQuery(uploadButton).html(upButton).css("background-color", "#4caf50");
						jQuery(inputBox).css("border", "2px solid #4caf50");
						jQuery(removeBox).fadeIn();
						jQuery(removeBox).attr("data-filename", result["file"]);
						jQuery(removeBox).attr("data-type", result["type"]);
						jQuery(inputBox).children().bind('click', function(){ return false; });
						jQuery(inputBox).children().css("cursor", "no-drop");
					}
					else {
						jQuery(inputBox).LoadingOverlay("hide");
						if(result["login"] == false){
							window.location.href = window.location.pathname+"?option=com_insurediymotor&view=form&layout=thankyou";
						} else {
							alert("Upload failed");
							jQuery(filenameHolder).html("");
						}
						
					}
				},
				error: function(xhr, textStatus, errorThrown){
					jQuery(inputBox).LoadingOverlay("hide");
				}
			});
		});

		jQuery(".upload-login").submit(function(e) {
		    e.preventDefault();    
			var formData = new FormData(jQuery(this)[0]);
			jQuery(".upload-login").LoadingOverlay("show");
			jQuery.ajax({
				type: "POST",
				url: "<?php echo JRoute::_('index.php', true); ?>",
				data: formData,
				dataType: "json",
				contentType: false,
			    processData: false,
				beforeSend: function() {
					jQuery(inputBox).LoadingOverlay("show");
				},
				success: function(result){
					if (result["success"] == true) {
						window.location.href = window.location.pathname+"?option=com_insurediymotor&view=form&layout=thankyou";
					} else {
						alert(result["message"]);
						jQuery(".upload-login").LoadingOverlay("hide");
					}
				},
				error: function(xhr, textStatus, errorThrown){
					console.log(errorThrown);
					jQuery(".upload-login").LoadingOverlay("hide");
				}
			});
		});

		jQuery(".removeFile").click(function(e) {
		    e.preventDefault();
			var theForm = jQuery(this).parents("form:first");
			var removeBox = jQuery(theForm).find(".removeFile");
			var inputBox = jQuery(theForm).find(".vehicleRegForm");
			var filenameHolder = jQuery(theForm).find("span");
			var uploadButton = jQuery(theForm).find(".upButton");
			var fileName = jQuery(this).attr("data-filename");
			var fileType = jQuery(this).attr("data-type");
			var thisButton = jQuery(this);
	    
			jQuery.ajax({
				type: "POST",
				url: "<?php echo JURI::root().'index.php?option=com_insurediymotor&task=ajax.deleteFile&format=json'; ?>",
				data: "filename="+fileName+"&filetype="+fileType+"&pid=<?php echo $pid ?>",
				dataType: "json",
			    processData: false,
				beforeSend: function() {
					jQuery(thisButton).LoadingOverlay("show");
				},
				success: function(result){
					if(result["success"] == true) {
						jQuery(thisButton).LoadingOverlay("hide");
						var upButton = '<svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg>Choose a file&hellip;'
						jQuery(thisButton).fadeOut();
						jQuery(inputBox).css("border", "2px solid #5197d5");
						jQuery(filenameHolder).html("");
						jQuery(inputBox).children().css("cursor", "pointer");
						jQuery(uploadButton).html(upButton).css("background-color", "#5197d5");
						jQuery(inputBox).children().unbind('click');
					}
					else {
						jQuery(thisButton).LoadingOverlay("hide");
						alert("Delete failed");
					}
				},
				error: function(xhr, textStatus, errorThrown){
					jQuery(thisButton).LoadingOverlay("hide");
				}
			});
		});


	// Send transaction data with a pageview if available
	// when the page loads. Otherwise, use an event when the transaction
	// data becomes available.
		
	dataLayer.push({
		'ecommerce': {
			'purchase': {
				'actionField': {
					'id': '<?php echo $quoteId; ?>',
					// Transaction ID. Required for purchases and refunds.
					'revenue': '<?php echo $paidAmt; ?>'
				},
				'products': [{ // List of productFieldObjects.
					'name': '<?php echo $planName; ?>',
					// Name or ID is required.
					'id': '<?php echo $planId; ?>',
					'price': '<?php echo $paidAmt; ?>',
					'brand': '<?php echo $selectedPartner; ?>',
					'category': 'Motor',
					'variant': '',
					'quantity': 1 // Optional fields may be omitted or set to empty string.
				}]
			}
		}
	});
</script>
<?php if($paidAmt!=""): ?>

<img src="https://marktamerica.go2cloud.org/aff_l?offer_id=6279&adv_sub=<?php echo $quoteId; ?>&amount=<?php echo $paidAmt; ?>" scrolling="no" frameborder="0" width="1" height="1" />
<?php endif; ?>

