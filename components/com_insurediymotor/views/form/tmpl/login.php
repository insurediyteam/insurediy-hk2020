<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_insurediymotor
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', '.insurediy-form-content select');
JHtml::_('script', 'system/jquery.validate.js', false, true);

// Create shortcut to parameters.
$params = $this->state->get('params');

$session = JFactory::getSession();
$customerData = $session->get("motor.data", FALSE);

?>

<script type="text/javascript">
	Joomla.submitbutton = function (task)
	{
		if (document.formvalidator.isValid(document.getElementById('adminForm')))
		{
			Joomla.submitform(task);
		}
	}
	window.addEvent("domready", function () {

	});

</script>

<div class="insurediy-form montserat-font" style="padding:20px;">
	<div class="motor-header-top-wrapper">
		<?php echo InsureDIYMotorHelper::renderHeader('icon-travel', JText::_('COM_INSUREDIYMOTOR_PAGE_HEADING'), 3); ?>
	</div>
	<div class="edit<?php echo $this->pageclass_sfx; ?> insurediy-motor-form-content" style="border: 1px solid rgba(0, 0, 0, 0.15);background-color: #f2f2f2;">
		<h3 style="font-size: 16px; margin:0 0 20px;"><?php echo JText::_("COM_INSUREDIYMOTOR_LOG_IN_TO_PROCEED"); ?></h3>
		<?php //echo MyHelper::renderDefaultMessage(); ?>
		<fieldset class="insurediy-life-log-in">
			<form action="<?php echo JRoute::_('index.php', true); ?>" method="post" id="login-form" class="form-inline form-validate loginForm">
				<h2><?php echo JText::_("COM_INSUREDIYMOTOR_LOG_IN_LOGIN"); ?></h2>
				<div class="userdata">
					<div style="height: 130px;">
						<div id="form-login-username" class="control-group">
							<div class="controls">
								<div class="input-prepend">
									<input id="modlgn-username" type="text" name="username" class="input-small required validate-username" tabindex="0" size="18" placeholder="<?php echo JText::_('JGLOBAL_EMAIL') ?>" value="<?php if($this->emailExist && isset($customerData["Email"][0])){ echo $customerData["Email"][0]; }?>" />
								</div>
							</div>
						</div>
						<div id="form-login-password" class="control-group">
							<div class="controls">
								<div class="input-prepend">
									<input id="modlgn-passwd" type="password" name="password" class="input-small required" tabindex="0" size="18" placeholder="<?php echo JText::_('JGLOBAL_PASSWORD') ?>" />
								</div>
							</div>
						</div>
						<a class="insurediy-motor-forgot-password" href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>" target="_blank"><?php echo JText::_('FORGOT_PASSWORD') ?></a>
					</div>
					<div style="margin-top:73px;">
						<div id="form-login-submit" class="control-group" style="float:right">
							<div class="controls">
								<button type="submit" tabindex="0" name="Submit" class="btn btn-primary idd_bluebutton" style="padding:5px 20px;font-size:14px !important;height:30px;"><?php echo JText::_('JSUBMIT') ?></button>
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<input type="hidden" name="option" value="com_insurediymotor" />
					<input type="hidden" name="task" value="user.login" />
					<input type="hidden" name="return" value="<?php echo $this->return; ?>" />
					<?php echo JHtml::_('form.token'); ?>
				</div>
			</form>
		</fieldset>
		<div style="width:5%;float: left">
			<div class="mid-line"></div><div style="text-align: center;"><a href="#" onclick="return false;" class="btn btn-primary or-btn idd_redroundbutton">OR</a></div><div class="mid-line"></div>
		</div>
		<fieldset class="insurediy-life-register">
			<form action="<?php echo JRoute::_('index.php', true); ?>" method="post" id="register-form" class="form-inline form-validate registerForm">
				<h2><?php echo JText::_("COM_INSUREDIYMOTOR_LOG_IN_REGISTER"); ?></h2>
				<div class="userdata">
					<div>
						<div id="form-login-username" class="control-group">
							<div class="controls">
								<div class="input-prepend">
									<input id="email" type="text" name="email" class="input-small required validate-email" tabindex="0" size="18" placeholder="<?php echo JText::_('JGLOBAL_EMAIL') ?>" value="<?php if(!$this->emailExist && isset($customerData["Email"][0])){ echo $customerData["Email"][0]; }?>" />
								</div>
							</div>
						</div>
						<div id="form-login-password" class="control-group">
							<div class="controls">
								<div class="input-prepend">
									<input id="password" type="password" name="password" class="input-small required validate-password" tabindex="0" size="18" placeholder="<?php echo JText::_('JGLOBAL_PASSWORD') ?>" />
								</div>
							</div>
						</div>
						<div id="form-login-password" class="control-group">
							<div class="controls">
								<div class="input-prepend">
									<input id="password2" type="password" name="password2" class="input-small required validate-password" tabindex="0" size="18" placeholder="Confirm <?php echo JText::_('JGLOBAL_PASSWORD') ?>" />
								</div>
							</div>
						</div>
					</div>
					<div style="margin-top:29px;">
						<div id="form-login-submit" class="control-group" style="float:right">
							<div class="controls">
								<button type="submit" tabindex="0" name="Submit" class="btn btn-primary idd_bluebutton" style="padding:5px 20px;font-size:14px !important;height:30px;"><?php echo JText::_('JSUBMIT') ?></button>
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<?php $usersConfig = JComponentHelper::getParams('com_users'); ?>

					<input type="hidden" name="option" value="com_insurediymotor" />
					<input type="hidden" name="task" value="user.register" />
					<input type="hidden" name="return" value="<?php echo $this->return; ?>" />
					<?php echo JHtml::_('form.token'); ?>
				</div>
			</form>
		</fieldset>
		<div class="clear"></div>
		<div style="float:left;margin-top:15px">
					{modulepos insurediy-secured-ssl}
				</div>
		</form>	
		<div class="clear"></div>
	</div>
	<div style="text-align:center;width:100%;background-color:#ffffff;padding-top:20px;">
			<span style="font-size:15px;font-weight:bold;"><?php echo JText::_('INSUREDIY_AWARDS') ?></span>
		</div>
		<div id="car-awards">
			<img src="<?php echo JText::_('CORPHUB_LOGO') ?>">
			<img src="<?php echo JText::_('WPH_LOGO') ?>">
		</div>
</div>
<div class="logo-slider"><?php echo MyHelper::load_module(102); ?></div>

<?php /* Popup Box */ ?>
<div class="insurediy-popup" id="insurediy-popup-box" style="display:none;">
	<div class="header">
		<div class="text-header"><?php echo JText::_("POP_IMPORTANT_NOTICE"); ?></div>
		<a class="close" href="javascript:cleanInsureDIYPopup('insurediy-popup-box');">&nbsp;</a>
		<div class="clear"></div>
	</div>
	<div class="padding">
		<div><?php echo JText::_("COM_INSUREDIYMOTOR_MEDICAL_ISSUE_MSG"); ?></div>
		<div style="text-align:right;"><input type="button" class="btn btn-primary" style="height:30px;padding:5px 20px;font-size:14px !important;" onclick="javascript:cleanInsureDIYPopup('insurediy-popup-box');
				return false;" value="Ok" /></div>
	</div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function () {
		// validate signup form on keyup and submit
		var validator = jQuery("form.registerForm").validate({
			rules: {
				password: "required",
			    password2: {
			      equalTo: "#password"
			    }
			},
			messages: {
				password2 : {
					equalTo : "Password does not match."
				}
			}
		});
	});
</script>
