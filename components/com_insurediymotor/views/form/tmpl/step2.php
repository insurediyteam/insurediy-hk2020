<?php
	defined('_JEXEC') or die;
	
	JHtml::_('behavior.keepalive');
	//JHtml::_('behavior.formvalidation');
	JHtml::_('bootstrap.tooltip');
	JHtml::_('script', 'system/jquery.validate.js', false, true);
	$data = $this->item;
	$params = $this->state->get('params');
	//$quotation = $this->quotation;
// 	print_r($data);
	//$this->policy_plans[] = $this->policy_plans[0];
	//$total_plans = count($this->policy_plans);
	$btn_font_size = "";
// 	if ($total_plans == 5) {
// 		$btn_font_size = "font-size: 12px!important;";
// 		} elseif ($total_plans > 5) {
// 		$btn_font_size = "font-size: 12px!important; padding: 0 12px;";
// 	}
	//$width = ($total_plans) ? 670 / $total_plans : 670;
?>

<script type="text/javascript">
	
	var checkboxHeight = "33";
	var radioHeight = "18";
	var selectWidth = "190";
	
	function chooseYourPlans() {
		var has_chosen = 0;
		
		$$('input[type="checkbox"]:checked').each(function (elem) {
			has_chosen = 1;
		});
		
		if (has_chosen == 0) {
			$('insurediy-popup-box').setStyle('display', 'block');
			} else {
			document.choosePlanForm.submit();
		}
	}
</script>

	<div class="insurediy-form montserat-font" style="background-color:#fff;">
		<div class="motor-header-top-wrapper">
			<?php echo InsureDIYMotorHelper::renderHeader('icon-travel', JText::_('COM_INSUREDIYMOTOR_PAGE_HEADING'), 2); ?>
		</div>
		<div class="insurediy-motor-form-content">
			<?php echo MyHelper::renderDefaultMessage(); ?>
			<div class="edit<?php echo $this->pageclass_sfx; ?>">
				<form action="<?php echo JRoute::_('index.php?option=com_insurediymotor'); ?>" method="post" name="custDetailForm" id="custDetailForm" class="form-vertical">
					<div class="motor-form-input-wrapper">
						<div class="motor-accordion">MAIN DRIVER CONTACT DETAILS</div>
						<div class="panel">
							<div class="sub-panel-top">
								<div class="input-wrapper-block">
									<span class="input-title">First Name</span>
									<div class="input-container-block">
										<input type="text" class="input-text-border no-commas" name="jform[firstName][0]" value="<?php if(isset($data["firstName"])) echo $data["firstName"][0]; ?>" />
										<label for="jform[firstName][0]" class="error" style="display: none;">Comma is not allowed</label>
									</div>
								</div>
								<div class="input-wrapper-block">
									<span class="input-title">Last Name</span>
									<div class="input-container-block">
										<input type="text" class="input-text-border no-commas" name="jform[lastName][0]" value="<?php if(isset($data["lastName"])) echo $data["lastName"][0]; ?>" />
										<label for="jform[lastName][0]" class="error" style="display: none;">Comma is not allowed</label>
									</div>
								</div>
								<br>
								<div class="input-wrapper-block">
									<span class="input-title">HKID Card Number</span>
									<div class="input-container-block">
										<select class="input-select-small-box" name="jform[icType][0]" style="display:none;">
											<option value="N" <?php if(isset($data["icType"])){ if($data["icType"][0]== "N") echo 'selected="selected"'; } ?>>NRIC (Singaporean)</option>
											<option value="B" <?php if(isset($data["icType"])){ if($data["icType"][0]== "B") echo 'selected="selected"'; } ?>>NRIC (Singapore PR)</option>
											<option value="E" <?php if(isset($data["icType"])){ if($data["icType"][0]== "E") echo 'selected="selected"'; } ?>>FIN (Employment Pass)</option>
											<option value="W" <?php if(isset($data["icType"])){ if($data["icType"][0]== "W") echo 'selected="selected"'; } ?>>FIN (Work Permit / S Pass)</option>
										</select>
										<input type="text" class="input-text-border" name="jform[Nric][0]" value="<?php if(isset($data["Nric"])) echo $data["Nric"][0]; ?>" />
									</div>
								</div>
								<div class="input-wrapper-block">
									<span class="input-title">Email Address</span>
									<div class="input-container-block">
										<input type="text" class="input-text-border" name="jform[Email][0]" value="<?php if(isset($data["Email"])){ echo $data["Email"][0];}elseif ($this->user) { echo $this->user->email; } ?>" />
									</div>
								</div>
								<div class="input-wrapper-block">
									<span class="input-title">Mobile No.</span>
									<div class="input-container-block">
										<span class="mobile-prefix">
											+852
										</span>
										<input type="text"  maxlength="11" class="input-text-border" name="jform[mobileNo][0]" value="<?php if(isset($data["mobileNo"])) echo $data["mobileNo"][0]; ?>" style="width:143px;"/>
									</div>
								</div>
							</div>
							<div class="sub-panel-bottom">
								<div class="input-wrapper-block-top">
									<div class="input-panel-left">
										<div class="squaredFour">
									    	<input type="checkbox" id="squaredFour" name="jform[agreePDC]" <?php if(isset($data["agreePDC"])){ if($data["agreePDC"] == "true") echo "checked=\"checked\""; }else { echo "checked=\"checked\"";} ?> value="true"/>
									    	<label for="squaredFour"></label>
									    </div>
									</div>
									<div class="input-panel-right">
										<div class="input-h1">
											I Agree
										</div>
										<div class="input-h2">
											to the use and transfer of my personal data to InsureDIY Limited in accordance with the Personal Data Collection Statement, although I understand that InsureDIY Limited acknowledges that I may opt out of the use of my personal data for direct marketing purpose by indicating as such below.*
										</div>
									</div>
								</div>
								<div class="input-wrapper-block-top">
									<div class="input-panel-left">
										<div class="squaredFour">
									    	<input type="checkbox" id="squaredFour2" name="jform[agreeMarketing]" <?php if(isset($data["agreeMarketing"])){ if($data["agreeMarketing"] == "true") echo "checked=\"checked\""; }else { echo "checked=\"checked\"";} ?> value="true"/>
									    	<label for="squaredFour2"></label>
									    </div>
									</div>
									<div class="input-panel-right">
										<div class="input-h1">
											I Agree
										</div>
										<div class="input-h2">
											InsureDIY Limited and its affiliated companies and partners (insurance companies included) would like to send me information on relevant products and services that may be of interest to me. Often, InsureDIY Limited or its affiliated companies or partners (insurance companies included) are able to notify me on promotional prices that will give me good value. I agree to InsureDIY Limited and their affiliated companies using my personal data (email, telephone number and postal address) for direct marketing of products and services as set out in their Personal Data Collection Statement.*
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="motor-form-input-wrapper">
						<div class="motor-accordion">DECLARATION</div>
						<div class="panel">
							<div class="panel-top">
								<div class="input-h1" style="margin-bottom: 10px;">
										Declarations 
								</div>
								<ul class="dash-list">
									<li>
										I am the car owner and first named driver;
									</li>
									<li>
										I and all named driver(s) are aged 25 to 65, and the driving experience is 2 years or above, holding with valid driving license and incurred not more than 8 points in Driving-offence Points System;
									</li>
									<li>
										I and all named driver(s) have never been disqualified and have a clean claims record over the immediately preceding 3 year period.
									</li>
									<li>
										I and all named driver(s) did not involve any dangerous driving, drunk driving, drug driving or penalized with driving license suspension in the last 3 years;
									</li>
									<li>
										I and all named driver(s) have never been refused renewal by any insurers, cancel or additional terms in the last 3 years;
									</li>
									<li>
										I am not in one of the following occupations: an actor/actress and Entertainment Industry; journalist/reporter/ employee in the media industry
									</li>
									<li>
										My car is not used for commercial purpose or used for hire/reward.
									</li>
									<li>
										My car is registered and used only in Hong Kong.
									</li>
									<li>
										I understand that InsureDIY will review my information and if my application falls under a referral risk for the selected insurer, InsureDIY will contact me for more information. 
									</li>
									<li>
										I understand that my application may or may not be accepted by the insurer and if not accepted by the insurer, InsureDIY will fully refund the premium that I paid on the InsureDIY website.
									</li>
								</ul>
							</div>
							<div class="panel-below">
								<div class="input-wrapper-block-top">
									<div class="input-panel-left">
										<div class="squaredFour">
									    	<input type="checkbox" id="squaredFour3" name="jform[agreeDeclaration]" value="true"/>
									    	<label for="squaredFour3"></label>
									    </div>
									</div>
									<div class="input-panel-right" style="width:auto;">
										<div class="input-h2">
											I agree with <span>ALL</span> of the above statements.
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					
					
					
					<input type="hidden" name="task" value="form.step2save" />
					<input type="hidden" name="quotation_id" value="<?php //echo $quotation['id']; ?>" />
					<?php echo JHtml::_('form.token'); ?>
					
					<div class="motor-btn-wrapper">
						<div class="motor-btn-group">
							<a href="/component/insurediymotor/?view=motor&Itemid=159&step=1" style="text-decoration:none;margin-right:20px;padding: 7px 16px 11px 9px;" type="button" class="motor-btn validate"><span class="white-arrow-left"></span>BACK</a>
							<button type="submit" id="submit_button" class="motor-btn validate">CONTINUE<span class="white-arrow"></span></button>
						</div>
					</div>
					
					<div class="clear"></div>
					</div>
				</form>
			</div>
			
		</div>
	</div>
	
	<?php /* Popup Box */ ?>
	<div class="insurediy-popup" id="insurediy-popup-box" style="display:none;">
		<div class="header">
			<div class="text-header"><?php echo JText::_("POP_IMPORTANT_NOTICE"); ?></div>
			<a class="close" href="javascript:cleanInsureDIYPopup('insurediy-popup-box');">&nbsp;</a>
			<div class="clear"></div>
		</div>
		<div class="padding">
			<div><?php echo JText::_("COM_INSUREDIYMOTOR_AT_LEAST_1_PLAN_MSG"); ?></div>
			<div style="text-align:right;"><input type="button" class="btn btn-primary" style="height:30px;padding:5px 20px;font-size:14px !important;" onclick="javascript:cleanInsureDIYPopup('insurediy-popup-box');
			return false;" value="Ok" /></div>
		</div>
	</div>
	
<script type="text/javascript">
	var acc = document.getElementsByClassName("motor-accordion");
	var i;
	
	for (i = 0; i < acc.length; i++) {
		acc[i].nextElementSibling.style.maxHeight = (acc[i].nextElementSibling.scrollHeight + 100) + "px";
	  acc[i].onclick = function() {
	    this.classList.toggle("active");
	    var panel = this.nextElementSibling;
	    if (panel.style.maxHeight){
	      panel.style.maxHeight = null;
	    } else {
	      panel.style.maxHeight = (panel.scrollHeight + 100) + "px";
	    } 
	  }
	}



	jQuery.validator.addMethod("validateMobile", function validateMobile(str, element) {
		var regEx = /^1[0-9]{10}$|^[569][0-9]{7}$/;
		var phone = str.match(regEx);
		
	    if (phone) 
	       return true;
	    else 
		   return false;
	},
        "Must be Mobile Number"
    );

	jQuery.validator.addMethod("validateHkid", function validateHkid(str, element) {
		var strValidChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

		// basic check length
		if (str.length < 8)
			return false;
	
		// handling bracket
		if (str.charAt(str.length-3) == '(' && str.charAt(str.length-1) == ')')
			str = str.substring(0, str.length - 3) + str.charAt(str.length -2);

		// convert to upper case
		str = str.toUpperCase();

		// regular expression to check pattern and split
		var hkidPat = /^([A-Z]{1,2})([0-9]{6})([A0-9])$/;
		var matchArray = str.match(hkidPat);

		// not match, return false
		if (matchArray == null)
			return false;

		// the character part, numeric part and check digit part
		var charPart = matchArray[1];
		var numPart = matchArray[2];
		var checkDigit = matchArray[3];

		// calculate the checksum for character part
		var checkSum = 0;
		if (charPart.length == 2) {
			checkSum += 9 * (10 + strValidChars.indexOf(charPart.charAt(0)));
			checkSum += 8 * (10 + strValidChars.indexOf(charPart.charAt(1)));
		} else {
			checkSum += 9 * 36;
			checkSum += 8 * (10 + strValidChars.indexOf(charPart));
		}

		// calculate the checksum for numeric part
		for (var i = 0, j = 7; i < numPart.length; i++, j--)
			checkSum += j * numPart.charAt(i);

		// verify the check digit
		var remaining = checkSum % 11;
		var verify = remaining == 0 ? 0 : 11 - remaining;

		return verify == checkDigit || (verify == 10 && checkDigit == 'A');
	},
		"Invalid HKID format"
    );


	// validate signup form on keyup and submit
	jQuery("form#custDetailForm").validate({
		rules: {
			"jform[firstName][0]": {
                required: true
            },
			"jform[lastName][0]": {
                required: true
            },
			"jform[Nric][0]": {
                required: true,
                validateHkid: true
            },
			"jform[Email][0]": {
                required: true,
                email: true
            },
			"jform[mobileNo][0]": {
                required: false,
                validateMobile: true
            },
			"jform[agreePDC]": {
                required: true
            },
			"jform[agreeDeclaration]": {
                required: true
            }
		},
		messages: {
			"jform[firstName]": {
				required : "Please fill in your first name"
			},
			"jform[lastName]": {
				required : "Please fill in your last name"
			},
			"jform[Nric]": {
				required : "Please fill in your NRIC",
				validateNRIC: "Invalid NRIC!"
			},
			"jform[Email]": {
				required : "Please fill in your email",
				email : "Invalid email address"
			},
			"jform[mobileNo]": {
				required : "Please fill in your contact number"
			},
			"jform[agreePDC]": {
				required : "Please check on this box"
			},
			"jform[agreeDeclaration]": {
                required : "Please check on this box"
            }
		}
	});

	jQuery(".no-commas").on('keydown', function(e){
	    if (!e) var e = window.event;
	     if (e.keyCode  ==  '188' )
	     {
	    	 jQuery(this).next().html("Comma is not allowed");
	         jQuery(this).next().css("display", "block");
	         return false;
	     }
	});

	jQuery("select[name=jform\\[icType\\]\\[0\\]]").on("change", function(){jQuery("input[name=jform\\[Nric\\]\\[0\\]]").focus();});
</script>
