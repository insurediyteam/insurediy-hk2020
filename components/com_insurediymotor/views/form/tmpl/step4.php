<?php
	defined('_JEXEC') or die;
	
	JHtml::_('behavior.keepalive');
	JHtml::_('MyBehavior.jsInsurediy');
	JHtml::_('script', 'system/jquery.validate.js', false, true);
	JHtml::_('script', 'system/select2.min.js', false, true);
	JHtml::_('stylesheet', 'system/select2.css', false, true);
	JHtml::_('script', 'system/loadingoverlay.min.js', false, true);
	
	$currency = $this->currency;
	
	$form = & $this->form;
	$fldGroup = "contact-details";
	$data = $this->item;

	print_r($data["driverNationality"]);
	$user = $this->user;

	$carData = $this->makeModel;
	$bodyType = $this->bodyType;

	$occupationListing = "";
	$occupationParentListing = "";
	
	foreach($this->occupation as $occupation) {
		$occupationListing .= "<option value=\"" . $occupation->occ_code . "\">" . ucwords(strtolower($occupation->occ_name)) . "</option>";
	}

	foreach($this->occupationParent as $occupationParent) {
		$occupationParentListing .= "<option value=\"" . $occupationParent->id . "\">" . ucwords(strtolower($occupationParent->occ_name)) . "</option>";
	}
//	print_r($data);
// 	echo "<br><hr><br>";
// 	print_r($this->result);
	
	if($this->result) {
		if(!$this->result->success) {
			$errorMsg = "";
			
			foreach($this->result["error_message"] as $item) {
				$errorMsg .= $item->errorDesc . "<br>";
			}
			
			JFactory::getApplication()->enqueueMessage(JText::_($errorMsg), 'Warning');
		}
	}
?>
<script>
<?php
if($this->result) {
	if(!$this->result->success) {
		echo "console.log('". json_encode($this->result)."');";
	}
}
?>
</script>
<div class="insurediy-form bb-form montserat-font step4">
	<div class="motor-header-top-wrapper">
		<?php echo InsureDIYMotorHelper::renderHeader('icon-travel', JText::_('COM_INSUREDIYMOTOR_PAGE_HEADING'), 4); ?>
	</div>
	<div class="edit<?php echo $this->pageclass_sfx; ?> insurediy-motor-form-content">
		<form action="<?php echo JRoute::_('index.php?option=com_insurediymotor&view=form'); ?>" method="post" name="adminForm" id="adminForm">
			<div class="motor-form-input-wrapper">
				<div class="motor-accordion">CAR DETAILS</div>
				<div class="panel">
					<div class="panel-left" style="margin-top:10px">
						<div class="input-half-wrapper">
							<span class="input-title">Type of Body</span>
							<div class="input-container-block">
								<input type="text" class="input-text" name="jform[vehicleBodyType]" value="<?php if(isset($data["carBodyType"])) echo strtoupper($bodyType[$data["carBodyType"]]); ?>" disabled/>
							</div>
						</div>
						<div class="input-half-wrapper">
							<span class="input-title">Cubic Capacity</span>
							<div class="input-container-block">
								<input type="text" class="input-text" name="jform[vehicleCc]" value="<?php if(isset($data["vehicleEngineCc"])) echo $data["vehicleEngineCc"]; ?>" disabled/>
							</div>
						</div>
						<div class="input-half-wrapper">
							<span class="input-title">Engine No.</span>
							<div class="input-container-block">
								<input type="text" class="input-text" name="jform[vehicleEngineNo]" value="<?php if(isset($data["vehicleEngineNo"])) echo $data["vehicleEngineNo"]; ?>" required/>
							</div>
						</div>
						<div class="input-half-wrapper">
							<span class="input-title">Chassis No.</span>
							<div class="input-container-block">
								<input type="text" class="input-text" name="jform[vehicleChassisNo]" value="<?php if(isset($data["vehicleChassisNo"])) echo $data["vehicleChassisNo"]; ?>" required/>
							</div>
						</div>
						<div class="input-half-wrapper nightParking">
							<span class="input-title">Night Parking District</span>
							<div class="input-container-block" style="margin-top:-8px;position:relative;">
								<select id="nightParking" name="jform[nightParking]" onchange="" onclick="return false;" class="hide-select custom-select night-parking" required>
									<option value="" selected>Select</option>
									<option value="HK" <?php if(isset($data["nightParking"]) && $data["nightParking"] == "HK") echo "selected"; ?>>Hong Kong</option>
									<option value="KLN" <?php if(isset($data["nightParking"]) && $data["nightParking"] == "KLN") echo "selected"; ?>>Kowloon</option>
									<option value="NT" <?php if(isset($data["nightParking"]) && $data["nightParking"] == "NT") echo "selected"; ?>>New Territories</option>
								</select>
							</div>
						</div>
						<div class="input-half-wrapper">
							<span class="input-title">Vehicle registration No.</span>
							<div class="input-container-block">
								<input type="text" class="input-text" name="jform[vehicleRegNo]" value="<?php if(isset($data["vehicleRegNo"])) echo $data["vehicleRegNo"]; ?>" required/>
							</div>
						</div>
						<div class="input-half-wrapper" style="height:115px;">
							<span class="input-title">Previous Policy No.</span>
							<span class="small-note">Input NA if this is a new car with no previous insurance policy</span>
							<div class="input-container-block">
								<input type="text" class="input-text" name="jform[previousPolicyNo]" value="<?php if(isset($data["previousPolicyNo"])) echo $data["previousPolicyNo"]; ?>" required/>
							</div>
						</div>
						<div class="input-half-wrapper" style="height:115px;">
							<span class="input-title">Previous Car Registration No.</span>
							<span class="small-note">Input NA if this is a new car with no previous car registration number</span>
							<div class="input-container-block">
								<input type="text" class="input-text" name="jform[previousRegNo]" value="<?php if(isset($data["previousRegNo"])) echo $data["previousRegNo"]; ?>" required />
							</div>
						</div>
						<?php if($data["NCDPoints"] > 0): ?>
							<!-- <div class="input-wrapper">
								<span class="input-title">Previous Policy No.</span>
								<span class="small-note">Input NA if this is a new car with no previous insurance policy</span>
								<div class="input-container-block">
									<input type="text" class="input-text" name="jform[previousPolicyNo]" value="<?php if(isset($data["previousPolicyNo"])) echo $data["previousPolicyNo"]; ?>" required/>
								</div>
							</div> -->
						<?php else: ?>
							<!-- <input type="hidden" name="jform[previousPolicyNo]" value="NA" /> -->
						<?php endif; ?>
					</div>
					<div class="panel-right">
						<div class="input-wrapper" style="margin-top: -6px;">
							<span class="input-title">Expiry Date of Previous Policy</span>
							<div class="input-container-block driver-birth">
											<?php 
												if(isset($data["prevPolicyEnd"])) { 
													echo $this->form->getInput('prevPolicyEnd');
													
													echo '<script>jQuery(function() {
																jQuery( "input[name=\'jform[prevPolicyEnd]\']" ).datepicker({
																	changeMonth: true,
																	changeYear: true,
																	showOn: "focus",
																	dateFormat : "dd-M-yy",
																	yearRange: "2018:2050",
																	onChangeMonthYear:function(y, m, i){                     
																		var d = i.selectedDay;
																		jQuery(this).datepicker("setDate", new Date(y, m-1, d));
																	}
																}).datepicker("setDate", "' . $data["prevPolicyEnd"]. '");
															});</script>';
												}
												else {
													echo $this->form->getInput('prevPolicyEnd');
												}
											?>
							</div>
						</div>
						<div class="input-wrapper" style="margin-top:15px;">
							<span class="input-title" style="margin:0;">Seating Capacity</span><span style="font-size:10px; font-weight:bold;"> (including Driver)</span>
							<div class="input-container-block">
								<input type="text" class="input-text" style="width:66%;" name="jform[vehicleSeatingCapacity]" value="<?php if(isset($carData->passenger)) {echo $carData->passenger;} else { echo "Not Applicable"; }?>" disabled/>
							</div>
						</div>
						<div class="input-wrapper loan-radio">
							<span class="input-title">Are you under financial loan?</span>
							<br>
							<div class="input-container-inline">
								<input type="radio" id="radio-workshop-0" class="input-radio" name="jform[loan]" <?php if(isset($data["loan"])){ if($data["loan"] == "Yes") echo "checked"; } ?> value="Yes" onclick="radioClicked(this);" required/>
								<label class="radio-label yes-loan loan-label" for="radio-workshop-0">
									<span class="radio-label-title <?php if(isset($data["loan"])){ if($data["loan"] == "Yes") echo "active";} ?>">Yes</span>
								</label>
							</div>
							<div class="input-container-inline">
								<input type="radio" id="radio-workshop-1" class="input-radio" name="jform[loan]" <?php if(isset($data["loan"])){ if($data["loan"] == "No") echo "checked"; } ?>  value="No" onclick="radioClicked(this);" required/>
								<label class="radio-label loan-label" for="radio-workshop-1">
									<span class="radio-label-title <?php if(isset($data["loan"])){ if($data["loan"] == "No") echo "active";} ?>">No</span>
								</label>
							</div>
						</div>
						<div class="input-wrapper" id="financial-loan" style="padding:0;margin-top:24px;<?php echo (isset($data["loan"]) && $data["loan"] == "No") ? "display:none;" : ""; ?><?php echo (isset($data["loan"]) && $data["loan"] == "") ? "display:none;" : ""; ?>">
							<span class="input-title">Financial Loan Company</span>
							<div class="input-container-block" style="margin-top: 31px;">
								<select name="jform[loanCompany]" onchange="" onclick="return false;" id="" class="hide-select custom-select">
								<option value="" selected>Select Loan Company</option>
								<?php foreach($this->loanCompany as $key => $val):?>
									<option value="<?php echo $val->mortgagee_code;?>" <?php if(isset($data["loanCompany"])){ if($val->mortgagee_code == $data["loanCompany"]) echo 'selected="selected"'; } ?>><?php echo $val->mortgagee_name;?></option>
								<?php endforeach; ?>
								</select>
							</div>
						</div>
						
						<?php if($data["NCDPoints"] > 0): ?>
							<!-- <div class="input-wrapper">
								<span class="input-title">Previous Car Registration No.</span>
								<span class="small-note">Input NA if this is a new car with no previous car registration number</span>
								<div class="input-container-block">
									<input type="text" class="input-text" name="jform[previousRegNo]" style="height: 35px!important;" value="<?php if(isset($data["previousRegNo"])) echo $data["previousRegNo"]; ?>" required />
								</div>
							</div> -->
						<?php else: ?>
							<!-- <input type="hidden" name="jform[previousRegNo]" value="NA" /> -->
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="motor-form-input-wrapper">
				<div class="motor-accordion">DRIVER DETAILS</div>
				<div class="panel">
					<div class="tab-header-container" style="width:100%; margin-bottom:10px;">
						<?php for($i=1; $i<= count($data["Gender"]); $i++):?>
							<?php if($i == 1):?>
								<div class="tab-pane active driver-pane"  onClick="changeTab(this);" id="driver-0" data-id="driver-panel-0">
									<img class="tab-pane-image" src="images/websiteicon/driver-logo.png" />
									<span class="tab-pane-title">MAIN DRIVER</span>
								</div>
							<?php else: ?>
								<div class="tab-pane driver-pane"  onClick="changeTab(this);" id="driver-<?php echo $i-1; ?>" data-id="driver-panel-<?php echo $i-1; ?>">
									<img class="tab-pane-image" src="images/websiteicon/driver-logo.png" />
									<span class="tab-pane-title">DRIVER <?php echo $i; ?></span>
								</div>
							<?php  endif; ?>
						<?php endfor; ?>
					</div>
					<div class="tab-content-container driver-container" style="margin-bottom:20px;">
						<?php for($i=0; $i< count($data["Gender"]); $i++):?>
								<div class="tab-wrapper driver-wrapper <?php echo ($i>=1) ? "display-none" : ""; ?>" id="driver-panel-<?php echo $i; ?>">
									<div class="input-sub-wrapper-2" style="width:40%;margin-right:80px;height:108px;">
										<span class="input-title">Gender</span>
										<br>
										<div class="input-container-inline">
											<input type="radio" id="radio-gender-<?php echo $i; ?>-M" class="input-radio" name="jform[Gender][<?php echo $i; ?>]" <?php if(isset($data["Gender"])){ if($data["Gender"][$i] == "MALE") echo "checked"; } ?> value="MALE" onclick="radioClicked(this);" />
											<label class="radio-label-logo <?php if(isset($data["Gender"])){ if($data["Gender"][$i] == "MALE") echo "active";} ?>" for="radio-gender-0-M">
												<img class="radio-label-gender-icon" alt="male" src="images/websiteicon/male-logo.png"/>
												<span class="radio-label-title-gender">Male</span>
											</label>
										</div>
										<div class="input-container-inline">
											<input type="radio" id="radio-gender-<?php echo $i; ?>-F" class="input-radio" name="jform[Gender][<?php echo $i; ?>]" <?php if(isset($data["Gender"])){ if($data["Gender"][$i] == "FEMALE") echo "checked"; } ?> value="FEMALE" onclick="radioClicked(this);" />
											<label class="radio-label-logo <?php if(isset($data["Gender"])){ if($data["Gender"][$i] == "FEMALE") echo "active";} ?>" for="radio-gender-<?php echo $i; ?>-F">
											<img class="radio-label-gender-icon" alt="female" src="images/websiteicon/female-logo.png"/>
												<span class="radio-label-title-gender">Female</span>
											</label>
										</div>
									</div>
									<div class="input-sub-wrapper-2" style="width:40%;margin-right:80px;height:108px;">
										<span class="input-title">Date Of Birth</span>
										<div class="input-container-block driver-birth">
														<?php 
															if(isset($data["dateOfBirth"])) { 
																echo str_replace("jform_dateOfBirth", "jform[dateOfBirth][$i]", str_replace("jform[dateOfBirth][0]", "jform[dateOfBirth][$i]", $this->form->getInput('dateOfBirth')));
																
																echo '<script>jQuery(function() {
																			jQuery( "input[name=\'jform[dateOfBirth]['.$i.']\']" ).datepicker({
																				changeMonth: true,
																				changeYear: true,
																				showOn: "focus",
																				dateFormat : "dd-M-yy",
																				yearRange: "1947:1998",
																				defaultDate: "01-JAN-1980",
																				onChangeMonthYear:function(y, m, i){                     
																					var d = i.selectedDay;
																					jQuery(this).datepicker("setDate", new Date(y, m-1, d));
																				}
																			}).datepicker("setDate", "' . $data["dateOfBirth"][$i]. '");
																		});</script>';
															}
															else {
																echo $this->form->getInput('dateOfBirth');
															}
														?>
										</div>
									</div>
									<div class="input-sub-wrapper-2" style="width:40%;margin-right:80px;height:70px;">
										<span class="input-title">First Name</span>
										<div class="input-container-block">
											<input type="text" class="input-text no-commas" name="jform[firstName][<?php echo $i; ?>]" style="" value="<?php if(isset($data["firstName"][$i])) echo $data["firstName"][$i]; ?>" required/>
											<label for="jform[firstName][<?php echo $i; ?>]" class="error" style="display: none;">Comma is not allowed</label>
										</div>
									</div>
									<div class="input-sub-wrapper-2" style="width:40%;margin-right:80px;height:70px;">
										<span class="input-title">HKID number</span>
										<div class="input-container-block">
											<input type="text" class="input-text" name="jform[Nric][<?php echo $i; ?>]" value="<?php if(isset($data["Nric"][$i])) echo $data["Nric"][$i]; ?>" required/>
										</div>
									</div>
									<div class="input-sub-wrapper-2" style="width:40%;margin-right:80px;height:70px;">
										<span class="input-title">Last Name</span>
										<div class="input-container-block">
											<input type="text" class="input-text no-commas" name="jform[lastName][<?php echo $i; ?>]" style="" value="<?php if(isset($data["lastName"][$i])) echo $data["lastName"][$i]; ?>" required/>
											<label for="jform[lastName][<?php echo $i; ?>]" class="error" style="display: none;">Comma is not allowed</label>
										</div>
									</div>
									<div class="input-sub-wrapper-2" style="width:40%;margin-right:80px;height:70px;">
										<span class="input-title">Job Industry</span>
										<div class="input-container-block">
											<div class="select_mate" data-mate-select="active" >
												<select name="jform[Industry][<?php echo $i; ?>]" onchange="" onclick="return false;" id="" class="hide-select custom-select">
													<option value="" disabled <?php if(!isset($data["Industry"])){ echo 'selected="selected"'; } else{ if($data["Industry"][$i] == "") echo 'selected="selected"'; }?>>Select Job Industry</option>
													<?php foreach($this->occupationParent as $occupation): ?>
														<option value="<?php echo $occupation->id; ?>" <?php if(isset($data["Industry"])){ if($data["Industry"][$i]== $occupation->id) echo 'selected="selected"'; } ?>><?php echo ucwords(strtolower($occupation->occ_name)); ?></option>
													<?php endforeach; ?>
												</select>
											</div>
											<!-- Custom select structure --> 
										</div>
									</div>
									<div class="input-sub-wrapper-2" style="width:40%;margin-right:80px;height:70px;">
										<span class="input-title">Nationality</span>
										<div class="insurediy-motor-custom-dropdown">
											<select name="jform[driverNationality][<?php echo $i; ?>]" class="custom-select" required>
												<?php 
													$countryList = $this->form->getInput('country_list');
													
													foreach($countryList as $country) {
														foreach($country as $key => $val) {
															if(isset($data["driverNationality"][$i])){ 
																if($data["driverNationality"][$i]== $key) {
 																	echo '<option value="' . $key . '" selected="selected">' . $val . '</option>';
 																}
 																else {
																	if($key == "HK"){
																		echo '<option value="' . $key . '" selected="selected">' . $val . '</option>';
																	} else {
																		echo '<option value="' . $key . '">' . $val . '</option>';
																	}
 																}
 															}
 															else {
																if($key == "HK"){
																	echo '<option value="' . $key . '" selected="selected">' . $val . '</option>';
																} else {
																	echo '<option value="' . $key . '">' . $val . '</option>';
																}
 																
 															}
														}
														
													}
												?>
											</select>
										</div>
									</div>
									<div class="input-sub-wrapper-2" style="width:40%;margin-right:80px;height:70px;">
										<span class="input-title">Job Position</span>
										<div class="select_mate" data-mate-select="active" >
											<select name="jform[Occupation][<?php echo $i; ?>]" onchange="" onclick="return false;" id="" class="hide-select custom-select">
												<option value="" disabled <?php if(!isset($data["Occupation"])){ echo 'selected="selected"'; } else{ if($data["Occupation"][$i] == "") echo 'selected="selected"'; }?>>Select Job Position</option>
												<?php $x = 0; ?>
												<?php foreach($this->occupation as $occupation): ?>
													<?php if($data["Occupation"][$i]== $occupation->occ_code && $x < 1): ?>
													<option value="<?php echo $occupation->occ_code; ?>" <?php if(isset($data["Occupation"])){ if($data["Occupation"][$i]== $occupation->occ_code) echo 'selected="selected"'; } ?>><?php echo ucwords(strtolower($occupation->occ_name)); ?></option>
													<?php $x++; ?>
													<?php endif; ?>
												<?php endforeach; ?>
											</select>
										</div>
									</div>
									<div class="input-sub-wrapper-2" style="width:40%;margin-right:80px;height:70px;">
										<span class="input-title">Marital Status</span>
										<div class="input-container-block">
											<div class="select_mate" data-mate-select="active" >
												<select name="jform[maritalStatus][<?php echo $i; ?>]" onchange="" onclick="return false;" id="" class="hide-select custom-select" required>
													<option value="" disabled <?php if(!isset($data["maritalStatus"])){ echo 'selected="selected"'; } else{ if($data["maritalStatus"][$i] == "") echo 'selected="selected"'; }?>>Marital Status</option>
													<option value="Married" <?php if(isset($data["maritalStatus"])){ if($data["maritalStatus"][$i]== "Married") echo 'selected="selected"'; } ?>>Married</option>
													<option value="Single" <?php if(isset($data["maritalStatus"])){ if($data["maritalStatus"][$i]== "Single") echo 'selected="selected"'; } ?>>Single</option>
													<option value="Divorced" <?php if(isset($data["maritalStatus"])){ if($data["maritalStatus"][$i]== "Divorced") echo 'selected="selected"'; } ?>>Divorced</option>
												</select>
											</div>
											<!-- Custom select structure --> 
										</div>
									</div>
									<div class="input-sub-wrapper-2" style="width:40%;margin-right:80px;height:70px;">
										<span class="input-title">Driving License No.</span>
										<div class="input-container-block">
											<input type="text" class="input-text no-commas" name="jform[drivingLicense][<?php echo $i; ?>]" style="" value="<?php if(isset($data["drivingLicense"][$i])) echo $data["drivingLicense"][$i]; ?>" required/>
											<label for="jform[drivingLicense][<?php echo $i; ?>]" class="error" style="display: none;">Comma is not allowed</label>
										</div>
									</div>
									<?php if($i == 0): ?>
									<div class="input-sub-wrapper-2" style="width:40%;margin-right:80px;height:70px;">
										<span class="input-title">Email</span>
										<div class="input-container-block">
											<input type="text" style="width: 83%;" class="input-text" name="jform[Email][0]" value="<?php if(isset($data["Email"][0])) echo $data["Email"][0]; ?>" required/>
										</div>
									</div>
									<?php endif; ?>
									<div class="input-sub-wrapper-2" style="width:40%;margin-right:80px;height:70px;margin-top:-6px;">
										<span class="input-title" style="padding-top: 6px;">Date Of Passing Driving Test</span>
										<div class="input-container-block driver-birth">
													<?php 
														if(isset($data["drivingTest"][0])) { 
															echo str_replace("jform_drivingTest", "jform[drivingTest][$i]", str_replace("jform[drivingTest][0]", "jform[drivingTest][$i]", $this->form->getInput('drivingTest')));
															
															echo '<script>jQuery(function() {
																		jQuery( "input[name=\'jform[drivingTest]['.$i.']\']" ).datepicker({
																			changeMonth: true,
																			changeYear: true,
																			showOn: "focus",
																			dateFormat : "dd-M-yy",
																			yearRange: "2000:2050",
																			onChangeMonthYear:function(y, m, i){                     
																				var d = i.selectedDay;
																				jQuery(this).datepicker("setDate", new Date(y, m-1, d));
																			}
																		}).datepicker("setDate", "' . $data["drivingTest"][$i]. '");
																	});</script>';
														}
														else {
															echo $this->form->getInput('drivingTest');
														}
													?>
										</div>
									</div>
									<?php if($i == 0): ?>
									<div class="input-sub-wrapper-2" style="width:40%;margin-right:80px;height:70px;">
										<span class="input-title">Mobile No.</span>
										<div class="input-container-block">
											<input type="text" style="width: 83%;" class="input-text" name="jform[mobileNo]" value="<?php if(isset($data["mobileNo"])) echo $data["mobileNo"]; ?>" required/>
										</div>
									</div>
									<div class="input-sub-wrapper-2" style="width:40%;margin-right:80px;height:70px;">
										<span class="input-title">Total driving-offence points in the last 3 years?</span>
										<div class="input-container-block">
											<div class="select_mate" data-mate-select="active" >
												<select name="jform[offencePoint][0]" onchange="" onclick="return false;" id="" class="hide-select custom-select">
													<option value="0" <?php if(isset($data["offencePoint"][0]) && $data["offencePoint"][0] == "0") echo "selected"; ?>>0</option>
													<option value="3" <?php if(isset($data["offencePoint"][0]) && $data["offencePoint"][0] == "3") echo "selected"; ?>>3</option>
													<option value="5" <?php if(isset($data["offencePoint"][0]) && $data["offencePoint"][0] == "5") echo "selected"; ?>>5</option>
													<option value="6" <?php if(isset($data["offencePoint"][0]) && $data["offencePoint"][0] == "6") echo "selected"; ?>>6</option>
													<option value="8" <?php if(isset($data["offencePoint"][0]) && $data["offencePoint"][0] == "8") echo "selected"; ?>>8</option>
													<option value="99" <?php if(isset($data["offencePoint"][0]) && $data["offencePoint"][0] == "99") echo "selected"; ?>>Above 8</option>
												</select>
											</div>
										</div>
									</div>
									<div class="input-sub-wrapper-2" style="width:40%;margin-right:80px;height:70px;">
										<span class="input-title">Address Line 1</span>
										<div class="input-container-block">
											<input type="text" style="width: 83%;" class="input-text" name="jform[addressOne]" value="<?php if(isset($data["addressOne"])) echo $data["addressOne"]; ?>" required/>
										</div>
									</div>
									<div class="input-sub-wrapper-2" style="width:40%;margin-right:80px;height:70px;">
										<span class="input-title">Highest driving-offence point in the last 3 years?</span>
										<div class="input-container-block">
											<div class="select_mate" data-mate-select="active" >
												<select name="jform[highestOffencePoint][0]" onchange="" onclick="return false;" id="" class="hide-select custom-select">
													<option value="0" <?php if(isset($data["highestOffencePoint"][0]) && $data["highestOffencePoint"][0] == "0") echo "selected"; ?>>0</option>
													<option value="3" <?php if(isset($data["highestOffencePoint"][0]) && $data["highestOffencePoint"][0] == "3") echo "selected"; ?>>3</option>
													<option value="5" <?php if(isset($data["highestOffencePoint"][0]) && $data["highestOffencePoint"][0] == "5") echo "selected"; ?>>5</option>
													<option value="8" <?php if(isset($data["highestOffencePoint"][0]) && $data["highestOffencePoint"][0] == "8") echo "selected"; ?>>8</option>
													<option value="10" <?php if(isset($data["highestOffencePoint"][0]) && $data["highestOffencePoint"][0] == "10") echo "selected"; ?>>10</option>
													<option value="99" <?php if(isset($data["highestOffencePoint"][0]) && $data["highestOffencePoint"][0] == "99") echo "selected"; ?>>Above 10</option>
												
												</select>
											</div>
										</div>
									</div>
									<div class="input-sub-wrapper-2" style="width:40%;margin-right:80px;height:70px;">
										<span class="input-title">Address Line 2</span>
										<div class="input-container-block">
											<input type="text" style="width: 83%;" class="input-text" name="jform[addressTwo]" value="<?php if(isset($data["addressTwo"])) echo $data["addressTwo"]; ?>"/>
										</div>
									</div>
									<div class="input-sub-wrapper-2" style="width:40%;margin-right:80px;height:70px;">
										<span class="input-title">Any claims in the last 3 years?</span>
										<div class="input-container-block">
											<div class="select_mate" data-mate-select="active" >
												<select name="jform[claimsPast3Years][0]" class="hide-select custom-select past3yrs">
													<option value="0" <?php if(isset($data["claimsPast3Years"][0])){ if($data["claimsPast3Years"][0] == 0) echo "selected"; } ?>>0</option>
													<option value="1" <?php if(isset($data["claimsPast3Years"][0])){ if($data["claimsPast3Years"][0] == 1) echo "selected"; } ?>>1</option>
													<option value="2" <?php if(isset($data["claimsPast3Years"][0])){ if($data["claimsPast3Years"][0] == 2) echo "selected"; } ?>>2</option>
													<option value="3" <?php if(isset($data["claimsPast3Years"][0])){ if($data["claimsPast3Years"][0] == 3) echo "selected"; } ?>>3</option>
													<option value="4" <?php if(isset($data["claimsPast3Years"][0])){ if($data["claimsPast3Years"][0] == 4) echo "selected"; } ?>>4</option>
													<option value="5" <?php if(isset($data["claimsPast3Years"][0])){ if($data["claimsPast3Years"][0] == 5) echo "selected"; } ?>>5</option>													
												</select>
											</div>
										</div>
									</div>
									
									<div class="input-sub-wrapper-2" style="width:40%;margin-right:80px;height:70px;">
										<span class="input-title">Country</span>
										<div class="insurediy-motor-custom-dropdown">
											<select name="jform[country]" class="custom-select" required>
												<?php 
													$countryList = $this->form->getInput('country_list');
													print_r($countryList);
													
													foreach($countryList as $country) {
														foreach($country as $key => $val) {
															if(isset($data["country"])){
																if($data["country"]== $key) {
																	echo '<option value="' . $key . '" selected="selected">' . $val . '</option>';
																}
																else {
																	if($key == "HK"){
																		echo '<option value="' . $key . '" selected="selected">' . $val . '</option>';
																	} else {
																		echo '<option value="' . $key . '">' . $val . '</option>';
																	}
																}
															}
															else {
																if($key == "HK"){
																	echo '<option value="' . $key . '" selected="selected">' . $val . '</option>';
																} else {
																	echo '<option value="' . $key . '">' . $val . '</option>';
																}
															}
														}
														
													}
												?>
											</select>
											<div class="error-container"></div>
										</div>
									</div>
									<div class="input-sub-wrapper-2 claimAmtWrapper" style="width:40%;margin-right:80px;height:70px;<?php if(isset($data["claimAmount"][0])){ if($data["claimAmount"][0] == 0) echo 'display:none'; }else { echo 'display:none';}?>" >
										<span class="input-title">Total Amount of Claim</span>
										<div class="input-container-block">
											<div class="select_mate" data-mate-select="active" >
												<select name="jform[claimAmount][0]" onchange="" onclick="return false;" id="" class="hide-select custom-select">
													<option value="0" <?php if(!isset($data["claimAmount"][0])){ echo 'selected="selected"'; }else{ if($data["claimAmount"][0]== 0) echo 'selected="selected"'; }?>>0</option>
													<option value="5000" <?php if(isset($data["claimAmount"][0])){ if($data["claimAmount"][0]== 5000) echo 'selected="selected"'; } ?>>Less than HK$ 5,000</option>
													<option value="10000" <?php if(isset($data["claimAmount"][0])){ if($data["claimAmount"][0]== 10000) echo 'selected="selected"'; } ?>>Less than HK$ 10,000</option>
													<option value="30000" <?php if(isset($data["claimAmount"][0])){ if($data["claimAmount"][0]== 30000) echo 'selected="selected"'; } ?>>Less than HK$ 30,000</option>
													<option value="60000" <?php if(isset($data["claimAmount"][0])){ if($data["claimAmount"][0]== 60000) echo 'selected="selected"'; } ?>>Less than HK$ 60,000</option>
													<option value="60001" <?php if(isset($data["claimAmount"][0])){ if($data["claimAmount"][0]== 60001) echo 'selected="selected"'; } ?>>More than HK$ 60,000</option>
												</select>
											</div>
										</div>
									</div>
									<?php endif; ?>
									<?php if($i == 0): ?>
										<input type="hidden" name="jform[relationshipWithMainDriver][<?php echo $i; ?>]" value="I"/>
										<div class="input-sub-wrapper-2 add-driver">
											<div onClick="addDriver(this);" style="float:right">
												<img class="add-driver-img" alt="add driver" src="images/websiteicon/add-driver-logo.png"/>
												<span class="add-driver-title">ADD A DRIVER</span>
											</div>
										</div>
									<?php else: ?>
										<div class="input-sub-wrapper-2" style="width:40%;margin-right:80px;height:70px;">
											<span class="input-title">Relationship with main driver?</span>
											<div class="input-container-block">
												<div class="select_mate" data-mate-select="active" >
													<select name="jform[relationshipWithMainDriver][<?php echo $i; ?>]" onchange="" onclick="return false;" id="" class="hide-select custom-select">
														<option value="" disabled <?php if(!isset($data["relationshipWithMainDriver"])){ echo 'selected="selected"'; } else{ if($data["relationshipWithMainDriver"][$i] == "") echo 'selected="selected"'; }?>>Relationship</option>
														<option value="F" <?php if(isset($data["relationshipWithMainDriver"])){ if($data["relationshipWithMainDriver"][$i]== "F") echo 'selected="selected"'; } ?>>Friend</option>
														<option value="R" <?php if(isset($data["relationshipWithMainDriver"])){ if($data["relationshipWithMainDriver"][$i]== "R") echo 'selected="selected"'; } ?>>Relative</option>
														<option value="L" <?php if(isset($data["relationshipWithMainDriver"])){ if($data["relationshipWithMainDriver"][$i]== "L") echo 'selected="selected"'; } ?>>Siblings</option>
														<option value="P" <?php if(isset($data["relationshipWithMainDriver"])){ if($data["relationshipWithMainDriver"][$i]== "P") echo 'selected="selected"'; } ?>>Parent</option>
														<option value="S" <?php if(isset($data["relationshipWithMainDriver"])){ if($data["relationshipWithMainDriver"][$i]== "S") echo 'selected="selected"'; } ?>>Spouse</option>
														<option value="C" <?php if(isset($data["relationshipWithMainDriver"])){ if($data["relationshipWithMainDriver"][$i]== "C") echo 'selected="selected"'; } ?>>Child</option>
														<option value="I" <?php if(isset($data["relationshipWithMainDriver"])){ if($data["relationshipWithMainDriver"][$i]== "I") echo 'selected="selected"'; } ?>>Self</option>
														<option value="O" <?php if(isset($data["relationshipWithMainDriver"])){ if($data["relationshipWithMainDriver"][$i]== "O") echo 'selected="selected"'; } ?>>Others</option>
														</select>
												</div>
											</div>
										</div>
										<div class="input-wrapper add-driver" style="margin-top: 85px;display: block;">
												<div onClick="removeDriver(this);" style="float:left" data-id="<?php echo $i; ?>">
													<img class="add-driver-img" alt="remove driver" src="images/websiteicon/remove-driver-logo.png"/>
													<span class="remove-driver-title">REMOVE THIS DRIVER</span>
												</div>
												<div onClick="addDriver(this);" style="float:right">
													<img class="add-driver-img" alt="add driver" src="images/websiteicon/add-driver-logo.png"/>
													<span class="add-driver-title">ADD A DRIVER</span>
												</div>
										</div>
									<?php endif; ?>
									
								</div>
						<?php endfor; ?>
					</div>
				</div>
			</div>
			<input type="hidden" name="task" value="form.step4save" />
			<input type="hidden" name="quotation_id" value="<?php //echo $quotation['id']; ?>" />
			
			<div class="token">
			<?php echo JHtml::_('form.token'); ?>
			</div>
			
			<div class="origin-next" style="<?php echo ($user->id > 0) ? "" : "display:none;"; ?>">
			<?php echo InsureDIYMotorHelper::renderProceedButton(true);?>
			</div>
			
			<div class="clear"></div>
		</form>
			<div class="motor-form-input-wrapper login-section" style="<?php echo ($user->id > 0) ? "display:none;" : ""; ?>">
				<div class="motor-accordion">LOGIN/REGISTER</div>
				<div class="panel" style="margin-bottom:20px;">
					<div class="tab-header-container" style="width:auto;height:auto;margin:0;">
						<div class="tab-pane active login-pane" onclick="changeLoginTab(this);" id="login-0" data-id="login-panel" data-title="Login and enjoy up to HK$200 off">
							<span class="tab-pane-title" style="font-size:12px;">LOGIN</span>
						</div>
						<div class="tab-pane login-pane" onclick="changeLoginTab(this);" id="register-1" data-id="register-panel" data-title="Register and enjoy up to HK$200 off"> 
							<span class="tab-pane-title" style="font-size:12px;">REGISTER</span>
						</div>
					</div>
					<div class="tab-content-container" style="padding:0;margin-top: 20px;">
						<div class="tab-wrapper login-wrapper" id="login-panel">
						    <div class="red-text" style="color:#FB5A6E;font-weight: 600;margin-bottom: 20px;">Login and enjoy up to HK$200 off!</div>
							<div class="insurediy-form" style="background-color: transparent;border: none;margin: 0;">
								<form id="login-form-popup">
								<div class="input-wrapper" style="padding:0;display: inline-block;width: 45%;">
									<span class="input-title" style="font-size:12px;">Email</span>
									<div class="input-container-block">
										<input type="text" class="input-text" style="width:90%;" name="username" />
									</div>
								</div>
								<div class="input-wrapper" style="padding:0;display: inline-block;width: 45%;">
									<span class="input-title" style="font-size:12px;">Password</span>
									<div class="input-container-block">
										<input type="password" class="input-text" style="width:90%;" name="password" value="" />
										<input type="hidden" name="option" value="com_insurediymotor" />
										<input type="hidden" name="task" value="user.ajaxLogin" />
										<input type="hidden" name="return" value="" />
										<?php echo JHtml::_('form.token'); ?>
									</div>
								</div>
								<div class="motor-btn-wrapper">
									<div class="motor-btn-group">
										<button style="margin-right:20px; padding: 10px 18px 10px 10px;" type="button" onclick="window.history.back();" class="motor-btn validate"><span class="white-arrow-left"></span>BACK</button>
										<button type="submit" id="submit_button login-btn" class="motor-btn validate">CONTINUE<span class="white-arrow"></span></button>
									</div>
									<div class="no-continue" style="color:#FB5A6E;text-decoration:underline;cursor:pointer;">No thanks, I will pay full amount</div>
								</div>
								</form>
							</div>
						</div>
						<div class="tab-wrapper display-none login-wrapper" id="register-panel">
							<div class="red-text" style="color:#FB5A6E;font-weight: 600;margin-bottom: 20px;">Register and enjoy up to HK$200 off!</div>
							<div class="insurediy-form" style="background-color: transparent;border: none;margin: 0;">
							    <form id="register-form-popup">
								<div class="input-wrapper" style="padding:0;display: inline-block;width: 30%;">
									<span class="input-title" style="font-size:12px;">Email</span>
									<div class="input-container-block">
										<input type="text" class="input-text" style="width:90%;" name="email" />
									</div>
								</div>
								<div class="input-wrapper" style="padding:0;display: inline-block;width: 30%;">
									<span class="input-title" style="font-size:12px;">Password</span>
									<div class="input-container-block">
										<input type="password" class="input-text" style="width:90%;" name="password" value="" />
									</div>
								</div>
								<div class="input-wrapper" style="padding:0;display: inline-block;width: 30%;">
									<span class="input-title" style="font-size:12px;">Confirm Password</span>
									<div class="input-container-block">
										<input type="password" class="input-text" style="width:90%;" name="password2" value="" />
										<input type="hidden" name="option" value="com_insurediymotor" />
										<input type="hidden" name="task" value="user.ajaxRegister" />
										<input type="hidden" name="return" value="" />
										<?php echo JHtml::_('form.token'); ?>
									</div>
								</div>
								<div class="motor-btn-wrapper">
									<div class="motor-btn-group">
										<button style="margin-right:20px; padding: 10px 18px 10px 10px;" type="button" onclick="window.history.back();" class="motor-btn validate"><span class="white-arrow-left"></span>BACK</button>
										<button type="submit" id="submit_button reg-btn" class="motor-btn validate">CONTINUE<span class="white-arrow"></span></button>
									</div>
									<div class="no-continue" style="color:#FB5A6E;text-decoration:underline;cursor:pointer;">No thanks, I will pay full amount</div>
								</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
	</div>
</div>
<div class="logo-slider"><?php echo MyHelper::load_module(102); ?></div>

<?php /* Popup Box */ ?>
<div class="insurediy-popup" id="insurediy-popup-box" style="display:none;">
	<div class="header">
		<div class="text-header"><?php echo JText::_("POP_IMPORTANT_NOTICE"); ?></div>
		<a class="close" href="javascript:cleanInsureDIYPopup('insurediy-popup-box');">&nbsp;</a>
		<div class="clear"></div>
	</div>
	<div class="padding">
		<div><?php echo JText::_("COM_INSUREDIYMOTOR_NAME_CHANGE_NOTICE_MSG"); ?></div>
		<div style="text-align:right;"><input type="button" class="btn btn-primary" style="height:30px;padding:5px 20px;font-size:14px !important;" onclick="javascript:cleanInsureDIYPopup('insurediy-popup-box');
			return f
		alse;" value="<?php echo JText::_("JOK"); ?>" /></div>
	</div>
</div>

<script>
jQuery("input[name='jform\[drivingTest\]\[0\]']").datepicker({
	changeMonth: true,
	changeYear: true,
	showOn: "focus",
	dateFormat : "dd-M-yy",
	yearRange: "1990:2050",
	defaultDate: "01-JAN-2014",
	onChangeMonthYear:function(y, m, i){                     
		var d = i.selectedDay;
		jQuery(this).datepicker("setDate", new Date(y, m-1, d));
	}
});
jQuery('.loan-label').click(function() {
	jQuery('#financial-loan .custom-select').select2({width: '269px'});
	if ( jQuery('.yes-loan span').hasClass("active") ){
		jQuery("#financial-loan").slideDown();
	} else {
		jQuery("#financial-loan").slideUp();
	}
});
jQuery("select[name='jform\[Industry\]\[0\]']").on('change', function(obj){
	jQuery.ajax({
		type: "POST",
		url: "<?php echo JURI::root().'index.php?option=com_insurediymotor&task=ajax.getJobList&format=json'; ?>",
		data:'jobIndustry='+jQuery("select[name='jform\[Industry\]\[0\]']").val(),
		dataType: "json",
		beforeSend: function() {
			jQuery(".job-position").LoadingOverlay("show");
		},
		success: function(result){
			jQuery("select[name='jform\[Occupation\]\[0\]']").html('<option value="" disabled selected>Select Job Position</option>');
			result.forEach(function(item, index){
				jQuery("select[name='jform\[Occupation\]\[0\]']").append(
						'<option value="'+item.occ_code+'">'+item.occ_name+'</option>'
				);
			});

			jQuery(".job-position").LoadingOverlay("hide");
		},
		error: function(xhr, textStatus, errorThrown){
			jQuery(".job-position").LoadingOverlay("hide");
			alert('request failed : '+errorThrown);
		}
	});
});
jQuery(document).on('change',"select[name='jform\[Industry\]\[1\]']", function(obj){
	jQuery.ajax({
		type: "POST",
		url: "<?php echo JURI::root().'index.php?option=com_insurediymotor&task=ajax.getJobList&format=json'; ?>",
		data:'jobIndustry='+jQuery("select[name='jform\[Industry\]\[1\]']").val(),
		dataType: "json",
		beforeSend: function() {
			jQuery(".job-position").LoadingOverlay("show");
		},
		success: function(result){
			jQuery("select[name='jform\[Occupation\]\[1\]']").html('<option value="" disabled selected>Select Job Position</option>');
			result.forEach(function(item, index){
				jQuery("select[name='jform\[Occupation\]\[1\]']").append(
						'<option value="'+item.occ_code+'">'+item.occ_name+'</option>'
				);
			});

			jQuery(".job-position").LoadingOverlay("hide");
		},
		error: function(xhr, textStatus, errorThrown){
			jQuery(".job-position").LoadingOverlay("hide");
			alert('request failed : '+errorThrown);
		}
	});
});
jQuery(document).on('change',"select[name='jform\[Industry\]\[2\]']", function(obj){
	jQuery.ajax({
		type: "POST",
		url: "<?php echo JURI::root().'index.php?option=com_insurediymotor&task=ajax.getJobList&format=json'; ?>",
		data:'jobIndustry='+jQuery("select[name='jform\[Industry\]\[2\]']").val(),
		dataType: "json",
		beforeSend: function() {
			jQuery(".job-position").LoadingOverlay("show");
		},
		success: function(result){
			jQuery("select[name='jform\[Occupation\]\[2\]']").html('<option value="" disabled selected>Select Job Position</option>');
			result.forEach(function(item, index){
				jQuery("select[name='jform\[Occupation\]\[2\]']").append(
						'<option value="'+item.occ_code+'">'+item.occ_name+'</option>'
				);
			});

			jQuery(".job-position").LoadingOverlay("hide");
		},
		error: function(xhr, textStatus, errorThrown){
			jQuery(".job-position").LoadingOverlay("hide");
			alert('request failed : '+errorThrown);
		}
	});
});
jQuery(document).on('change',"select[name='jform\[Industry\]\[3\]']", function(obj){
	jQuery.ajax({
		type: "POST",
		url: "<?php echo JURI::root().'index.php?option=com_insurediymotor&task=ajax.getJobList&format=json'; ?>",
		data:'jobIndustry='+jQuery("select[name='jform\[Industry\]\[3\]']").val(),
		dataType: "json",
		beforeSend: function() {
			jQuery(".job-position").LoadingOverlay("show");
		},
		success: function(result){
			jQuery("select[name='jform\[Occupation\]\[3\]']").html('<option value="" disabled selected>Select Job Position</option>');
			result.forEach(function(item, index){
				jQuery("select[name='jform\[Occupation\]\[3\]']").append(
						'<option value="'+item.occ_code+'">'+item.occ_name+'</option>'
				);
			});

			jQuery(".job-position").LoadingOverlay("hide");
		},
		error: function(xhr, textStatus, errorThrown){
			jQuery(".job-position").LoadingOverlay("hide");
			alert('request failed : '+errorThrown);
		}
	});
});
jQuery(document).on('change',"select[name='jform\[Industry\]\[4\]']", function(obj){
	jQuery.ajax({
		type: "POST",
		url: "<?php echo JURI::root().'index.php?option=com_insurediymotor&task=ajax.getJobList&format=json'; ?>",
		data:'jobIndustry='+jQuery("select[name='jform\[Industry\]\[4\]']").val(),
		dataType: "json",
		beforeSend: function() {
			jQuery(".job-position").LoadingOverlay("show");
		},
		success: function(result){
			jQuery("select[name='jform\[Occupation\]\[4\]']").html('<option value="" disabled selected>Select Job Position</option>');
			result.forEach(function(item, index){
				jQuery("select[name='jform\[Occupation\]\[4\]']").append(
						'<option value="'+item.occ_code+'">'+item.occ_name+'</option>'
				);
			});

			jQuery(".job-position").LoadingOverlay("hide");
		},
		error: function(xhr, textStatus, errorThrown){
			jQuery(".job-position").LoadingOverlay("hide");
			alert('request failed : '+errorThrown);
		}
	});
});
jQuery(document).on('change',"select[name='jform\[Industry\]\[5\]']", function(obj){
	jQuery.ajax({
		type: "POST",
		url: "<?php echo JURI::root().'index.php?option=com_insurediymotor&task=ajax.getJobList&format=json'; ?>",
		data:'jobIndustry='+jQuery("select[name='jform\[Industry\]\[5\]']").val(),
		dataType: "json",
		beforeSend: function() {
			jQuery(".job-position").LoadingOverlay("show");
		},
		success: function(result){
			jQuery("select[name='jform\[Occupation\]\[5\]']").html('<option value="" disabled selected>Select Job Position</option>');
			result.forEach(function(item, index){
				jQuery("select[name='jform\[Occupation\]\[5\]']").append(
						'<option value="'+item.occ_code+'">'+item.occ_name+'</option>'
				);
			});

			jQuery(".job-position").LoadingOverlay("hide");
		},
		error: function(xhr, textStatus, errorThrown){
			jQuery(".job-position").LoadingOverlay("hide");
			alert('request failed : '+errorThrown);
		}
	});
});
function generateToken(){
	jQuery.ajax({
		type: "POST",
		url: "<?php echo JURI::root().'index.php?option=com_insurediymotor&task=ajax.generateToken&format=json'; ?>",
		data: "",
		dataType: "json",
		contentType: false,
		processData: false,
		success: function(result){
			jQuery(".token").html(result["token"]);
		},
		error: function(xhr, textStatus, errorThrown){
			console.log(errorThrown);
		}
	});
}


jQuery(document).ready(function(){
	jQuery("#login-form-popup").submit(function(e) {
		e.preventDefault();    
		var formData = new FormData(jQuery(this)[0]);
		jQuery("button").LoadingOverlay("show");
		jQuery.ajax({
			type: "POST",
			url: "<?php echo JRoute::_('index.php', true); ?>",
			data: formData,
			dataType: "json",
			contentType: false,
			processData: false,
			success: function(result){
				if (result["success"] == true) {
					generateToken();
					jQuery(document).ajaxStop(function () {
						jQuery("form#adminForm").submit();
					});
					jQuery(".origin-next").show();
					jQuery(".login-section").slideUp();
					jQuery("button").LoadingOverlay("hide");
				} else {
					alert(result["message"]);
					jQuery("button").LoadingOverlay("hide");
				}
			},
			error: function(xhr, textStatus, errorThrown){
				console.log(errorThrown);
				jQuery("button").LoadingOverlay("hide");
			}
		});
	});

	jQuery("#register-form-popup").submit(function(e) {
		e.preventDefault();    
		var formData = new FormData(jQuery(this)[0]);
		jQuery("button").LoadingOverlay("show");
		jQuery.ajax({
			type: "POST",
			url: "<?php echo JRoute::_('index.php', true); ?>",
			data: formData,
			dataType: "json",
			contentType: false,
			processData: false,
			success: function(result){
				if (result["success"] == true) {
					generateToken();
					jQuery(document).ajaxStop(function () {
						jQuery("form#adminForm").submit();
					});
					jQuery(".origin-next").show();
					jQuery(".login-section").slideUp();
					jQuery("button").LoadingOverlay("hide");
				} else {
					alert(result["message"]);
					jQuery("button").LoadingOverlay("hide");
				}
			},
			error: function(xhr, textStatus, errorThrown){
				console.log(errorThrown);
				jQuery("button").LoadingOverlay("hide");
			}
		});
	});

	jQuery('.custom-select').select2({width: '180px'});
	jQuery('.custom-select.night-parking').select2({width: '70%'});
	jQuery('.driver-container .custom-select').select2({width: '343px'});
	jQuery('.driver-container input').css("width", "343px");
	
	jQuery(".past3yrs").change(function(){
	if(jQuery(this).val() == 0) {
		var firstOption = jQuery("select[name='jform[claimAmount]']").find("option:first-child").html();
		jQuery("select[name='jform[claimAmount]']").prop('selectedIndex',0);
		jQuery("select[name='jform[claimAmount]']").next().find(".select2-selection__rendered").html(firstOption);
		
		jQuery(".claimAmtWrapper").css("display", "none");
	}

	if(jQuery(this).val() > 0) {
		jQuery(".claimAmtWrapper").css("display", "inline-block");
	}
});
});

// var formValidator = jQuery("form#adminForm").validate();
var formValidator = jQuery("form#adminForm").validate({
		rules: {
			"jform[Gender][0]": {
                required: true
            },
			"jform[nightParking]": {
                required: true
            },
			"jform[prevPolicyEnd]": {
                required: false
            }
		},
		messages: {
			"jform[Gender][0]": {
				required : "Please select gender"
			}
		}
	});


var acc = document.getElementsByClassName("motor-accordion");
var i;

for (i = 0; i < acc.length; i++) {
	acc[i].nextElementSibling.style.maxHeight = (acc[i].nextElementSibling.scrollHeight + 500) + "px";
  acc[i].onclick = function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = (panel.scrollHeight + 500) + "px";
    } 
  }
}

jQuery.LoadingOverlaySetup({
    color           : "rgba(0, 0, 0, 0.4)"
});

var customElement = jQuery('\
		<div class="overlay-container">\
		<div class="overlay-content-top">\
			<img class="overlay-content-img" src="<?php echo JURI::base(); ?>/images/websiteicon/company-logo.png"/>\
		</div>\
		<div class="overlay-content-middle">\
			<div class="overlay-content-header">\
				<span class="overlay-content-title">Thank you for your patience, we are consolidating your details. <br><br></span>\
				<span class="overlay-content-brand">Please wait a moment . . .</span>\
			</div>\
		</div>\
		<div class="overlay-content-bottom">\
			<div class="spinner">\
			  <div></div>\
			  <div></div>\
			  <div></div>\
			  <div></div>\
			  <div></div>\
			</div>\
		</div>\
	</div>\
');

function validateNRIC(str, type) {
    if (str.length != 9) 
        return false;

    str = str.toUpperCase();

    var i, 
        icArray = [];
    for(i = 0; i < 9; i++) {
        icArray[i] = str.charAt(i);
    }

    icArray[1] = parseInt(icArray[1], 10) * 2;
    icArray[2] = parseInt(icArray[2], 10) * 7;
    icArray[3] = parseInt(icArray[3], 10) * 6;
    icArray[4] = parseInt(icArray[4], 10) * 5;
    icArray[5] = parseInt(icArray[5], 10) * 4;
    icArray[6] = parseInt(icArray[6], 10) * 3;
    icArray[7] = parseInt(icArray[7], 10) * 2;

    var weight = 0;
    for(i = 1; i < 8; i++) {
        weight += icArray[i];
    }

    var offset = (icArray[0] == "T" || icArray[0] == "G") ? 4:0;
    var temp = (offset + weight) % 11;

    var st = ["J","Z","I","H","G","F","E","D","C","B","A"];
    var fg = ["X","W","U","T","R","Q","P","N","M","L","K"];

    var theAlpha;
//	    if (icArray[0] == "S" || icArray[0] == "T") { theAlpha = st[temp]; }
//	    else if (icArray[0] == "F" || icArray[0] == "G") { theAlpha = fg[temp]; }

	var icType = type;

	if (icType == "N" || icType == "B") { theAlpha = st[temp]; }
    else if (icType == "E" || icType == "W") { theAlpha = fg[temp]; }

    return (icArray[8] === theAlpha);
}

function validateHkid(str, element) {
		var strValidChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

		// basic check length
		if (str.length < 8)
			return false;
	
		// handling bracket
		if (str.charAt(str.length-3) == '(' && str.charAt(str.length-1) == ')')
			str = str.substring(0, str.length - 3) + str.charAt(str.length -2);

		// convert to upper case
		str = str.toUpperCase();

		// regular expression to check pattern and split
		var hkidPat = /^([A-Z]{1,2})([0-9]{6})([A0-9])$/;
		var matchArray = str.match(hkidPat);

		// not match, return false
		if (matchArray == null)
			return false;

		// the character part, numeric part and check digit part
		var charPart = matchArray[1];
		var numPart = matchArray[2];
		var checkDigit = matchArray[3];

		// calculate the checksum for character part
		var checkSum = 0;
		if (charPart.length == 2) {
			checkSum += 9 * (10 + strValidChars.indexOf(charPart.charAt(0)));
			checkSum += 8 * (10 + strValidChars.indexOf(charPart.charAt(1)));
		} else {
			checkSum += 9 * 36;
			checkSum += 8 * (10 + strValidChars.indexOf(charPart));
		}

		// calculate the checksum for numeric part
		for (var i = 0, j = 7; i < numPart.length; i++, j--)
			checkSum += j * numPart.charAt(i);

		// verify the check digit
		var remaining = checkSum % 11;
		var verify = remaining == 0 ? 0 : 11 - remaining;

		return verify == checkDigit || (verify == 10 && checkDigit == 'A');
	}
jQuery(".no-continue").on("click", function() {
	generateToken();
	jQuery(document).ajaxStop(function () {
		jQuery("form#adminForm").submit();
  	});
});
jQuery("#nightParking").on("change", function(obj) {
	jQuery("form#adminForm").validate().element("#nightParking");
});
jQuery("#submit_button").on("click", function(e) {
	e.preventDefault();
	generateToken();
	jQuery(document).ajaxStop(function () {
		jQuery("form#adminForm").submit();
  	});
});
jQuery("form#adminForm").on("submit", function(obj) {
	var firstName = jQuery("[name^=jform\\[firstName\\]]");
	var lastName = jQuery("[id^=jform\\[lastName\\]]");
	var Nric = jQuery("[name^=jform\\[Nric\\]]");
	var Occupation = jQuery("[name^=jform\\[Occupation\\]]");
	var Email = jQuery("[name^=jform\\[Email\\]]");
	var race = jQuery("[name^=jform\\[driverRace\\]]");
	
	flag = validateField(firstName);
	flag = validateField(lastName);
	flag = validateField(Nric);
	flag = validateField(Occupation);
	flag = validateField(Email);
	flag = validateField(race);
	flag = validateRules();

	if(flag && formValidator.numberOfInvalids() == 0) {
		
		jQuery.LoadingOverlay("show", {
	        image   : "",
	        custom  : customElement
	    });
	}
	
	return flag;
});

jQuery("[name^=jform\\[loan\\]]").on("click", function(obj) {
	$value = jQuery(this).val();

	if($value == "Yes") {
		jQuery("#financial-loan").css('display', "inline-block");
	}
	else {
		var firstOption = jQuery("[name^=jform\\[loanCompany\\]]").find("option:first-child").html();
		
		jQuery("#financial-loan").css('display', "none");
		jQuery("[name^=jform\\[loanCompany\\]]").prop('selectedIndex',0);
		jQuery("[name^=jform\\[loanCompany\\]]").next().find(".select2-selection__rendered").html(firstOption);
	}
});

function validateRules() {
	var Nric = jQuery("[name^=jform\\[Nric\\]]");
	var firstName = jQuery("[name^=jform\\[firstName\\]]");
	var nightParking = jQuery("[name^=jform\\[nightParking\\]]");
	var lastName = jQuery("[name^=jform\\[lastName\\]]");
	var Email = jQuery("[name^=jform\\[Email\\]]");
	var nricType = jQuery("[name^=jform\\[icType\\]]");
	var maritalStatus = jQuery("[name^=jform\\[maritalStatus\\]]");
	var nationality = jQuery("[name^=jform\\[driverNationality\\]]");
	var nricList = new Array();

		
	for (var i = 0, len = Nric.length; i < len; i++) {
		var result = validateHkid(jQuery(Nric[i]).val(), false);
		nricList[i] = jQuery(Nric[i]).val();
		
		if(!result) {
			var errors = {};
			var name = jQuery(Nric[i]).attr("name");
			
			errors[name] = "Invalid HKID Format";
			
			formValidator.showErrors(errors);

			jQuery(".tab-pane.driver-pane").removeClass("active");
			jQuery("#driver-"+i).addClass("active");
			jQuery('.tab-wrapper.driver-wrapper').addClass('display-none');
			jQuery("#driver-panel-"+i).removeClass("display-none");

			jQuery(window).scrollTop((jQuery(Nric[i]).offset().top)-285);
			
			return false;
		}
	}

	for (var i = 0, len = firstName.length; i < len; i++) {
		if(jQuery(firstName[i]).val() == "") {
			var errors = {};
			var name = jQuery(firstName[i]).attr("name");
			
			errors[name] = "Please enter first name";
			
			formValidator.showErrors(errors);

			jQuery(".tab-pane.driver-pane").removeClass("active");
			jQuery("#driver-"+i).addClass("active");
			jQuery('.tab-wrapper.driver-wrapper').addClass('display-none');
			jQuery("#driver-panel-"+i).removeClass("display-none");

			jQuery(window).scrollTop((jQuery(Nric[i]).offset().top)-285);
			
			return false;
		}
	}

	for (var i = 0, len = lastName.length; i < len; i++) {
		if(jQuery(lastName[i]).val() == "") {
			var errors = {};
			var name = jQuery(lastName[i]).attr("name");
			
			errors[name] = "Please enter last name";
			
			formValidator.showErrors(errors);

			jQuery(".tab-pane.driver-pane").removeClass("active");
			jQuery("#driver-"+i).addClass("active");
			jQuery('.tab-wrapper.driver-wrapper').addClass('display-none');
			jQuery("#driver-panel-"+i).removeClass("display-none");

			jQuery(window).scrollTop((jQuery(Nric[i]).offset().top)-285);
			
			return false;
		}
	}

	for (var i = 0, len = maritalStatus.length; i < len; i++) {
		if(jQuery(maritalStatus[i]).val() == "" || jQuery(maritalStatus[i]).val() == null) {
			var errors = {};
			var name = jQuery(maritalStatus[i]).attr("name");
			
			errors[name] = "Please select one";
			
			formValidator.showErrors(errors);

			jQuery(".tab-pane.driver-pane").removeClass("active");
			jQuery("#driver-"+i).addClass("active");
			jQuery('.tab-wrapper.driver-wrapper').addClass('display-none');
			jQuery("#driver-panel-"+i).removeClass("display-none");

			jQuery(window).scrollTop((jQuery(Nric[i]).offset().top)-285);
			
			return false;
		}
	}

	for (var i = 0, len = Email.length; i < len; i++) {
		if(jQuery(Email[i]).val() == "") {
			var errors = {};
			var name = jQuery(Email[i]).attr("name");
			
			errors[name] = "Please enter email";
			
			formValidator.showErrors(errors);

			jQuery(".tab-pane.driver-pane").removeClass("active");
			jQuery("#driver-"+i).addClass("active");
			jQuery('.tab-wrapper.driver-wrapper').addClass('display-none');
			jQuery("#driver-panel-"+i).removeClass("display-none");

			jQuery(window).scrollTop((jQuery(Nric[i]).offset().top)-285);
			
			return false;
		}
	}
	
	if(jQuery("[name^=jform\\[loan\\]]:checked").val() == "Yes") {
		if(jQuery("[name^=jform\\[loanCompany\\]]").val() == "" || jQuery("[name^=jform\\[loanCompany\\]]").val() == null) {					
			var errors = {};
			var name = jQuery("[name^=jform\\[loanCompany\\]]").attr("name");
			
			errors[name] = "Please fill in company name";
			
			formValidator.showErrors(errors);
			
			return false;
		}
	}

	return true;
}

function checkDuplicate(list) {
	for (var i = 0, len = list.length; i < len; i++) {
		var temp = list.slice();

		delete temp[i];
		
		var result = temp.indexOf(list[i]);

		if(result != -1) {
			return true;
		}
	}

	return false;
}

function validateField(Obj) {
	var flag = "";
	var counter = 0;
	for (var i = 0, len = Obj.length; i < len; i++) {
		
		if(jQuery(Obj[i]).attr('type') == "radio") {
			if(jQuery(Obj[i]).prop("checked")) {
				flag = jQuery(Obj[i]).attr("name");
			}
			else {
				if(jQuery(Obj[i]).attr("name") == flag) {
					flag = "";
					counter = 0;
				}
				else {
					counter = jQuery(Obj[i]).attr("name").replace("jform[Gender][", "").replace("]", "");
				}
			}
		}
		else {
			if(jQuery(Obj[i]).val() == "" || jQuery(Obj[i]).val() == null) {					
				jQuery(".tab-pane.driver-pane").removeClass("active");
				jQuery("#driver-"+i).addClass("active");
				jQuery('.tab-wrapper.driver-wrapper').addClass('display-none');
				jQuery("#driver-panel-"+i).removeClass("display-none");
				
				return false;
			}
		}
	}

	if(jQuery(Obj[0]).attr('type') == "radio") {
		jQuery(".tab-pane.driver-pane").removeClass("active");
		jQuery("#driver-"+counter).addClass("active");
		jQuery('.tab-wrapper.driver-wrapper').addClass('display-none');
		jQuery("#driver-panel-"+counter).removeClass("display-none");

		return false;
	}
	return true;
}

// var next = document.getElementById("radio-next-button");

// next.onclick = function() {
// 	document.getElementById("year-block-1").classList.remove("display-block");
// 	document.getElementById("year-block-1").classList.add("display-none");
// 	document.getElementById("year-block-2").classList.remove("display-none");
// 	document.getElementById("year-block-2").classList.add("display-block");
// }

// var back = document.getElementById("radio-back-button");

// back.onclick = function() {
// 	document.getElementById("year-block-2").classList.remove("display-block");
// 	document.getElementById("year-block-2").classList.add("display-none");
// 	document.getElementById("year-block-1").classList.remove("display-none");
// 	document.getElementById("year-block-1").classList.add("display-block");
// }

function radioClicked(obj) {
	var otherRadios = document.getElementsByName(obj.getAttribute('name'));
	var j;
	
	if(jQuery(obj).parent().find('label[class^="radio-label"]').hasClass("radio-label-logo")) {
		for(j=0;j < otherRadios.length; j++) {
			jQuery(otherRadios[j]).parent().find('label[class^="radio-label"]').removeClass("active");
		}

		jQuery(obj).parent().find('label[class^="radio-label"]').toggleClass("active");
	}
	else {
		for(j=0;j < otherRadios.length; j++) {
			jQuery(otherRadios[j]).parent().find('span.radio-label-title').removeClass("active");
		}

		jQuery(obj).parent().find('span.radio-label-title').toggleClass("active");
	}
	
	
//		if(obj.nextElementSibling.children[0].classList[0].indexOf("icon") !== -1) {
//			for(j=0;j < otherRadios.length; j++) {
//				otherRadios[j].nextElementSibling.classList.remove("active");
//			}
		
		
//		}
//		else {
//			for(j=0;j < otherRadios.length; j++) {
//				otherRadios[j].nextElementSibling.children[0].classList.remove("active");
//			}
		
//			obj.nextElementSibling.children[0].classList.toggle("active");
//		}
}

var driverLimit = <?php echo isset($data["Gender"]) ? (count($data["Gender"])) : 0; ?>;
var driver = <?php echo isset($data["Gender"]) ? (count($data["Gender"])-1) : 0; ?>;
var removedDriver = new Array();

var dateOfBirth = '<?php echo $this->form->getInput('dateOfBirth'); ?>';
var drivingTest = '<?php echo $this->form->getInput('drivingTest'); ?>';

function addDriver(obj) {
	
	if(driverLimit < 4) {

		jQuery(".tab-pane.driver-pane").removeClass("active");
	
		driverLimit++;

		if(removedDriver.length > 0) {
			driver = Number(removedDriver[0]);

			removedDriver.shift();
		}
		else {
			driver++;
		}

		var dateOfBirth2 = dateOfBirth.replace(/jform_dateOfBirth/g, "jform[dateOfBirth][" + driver + "]");
		var drivingTest2 = drivingTest.replace(/jform_drivingTest/g, "jform[drivingTest][" + driver + "]");
		
		var divExist = jQuery("#driver-panel-"+(Number(driver)-1)).length;

		if(divExist == 0) {
			jQuery(".tab-header-container").append('<div class="tab-pane driver-pane active" onClick="changeTab(this);" id="driver-' + driver + '" data-id="driver-panel-' + driver + '"> \
		        	<img class="tab-pane-image" alt="remove driver" src="images/websiteicon/driver-logo.png" /> \
			        	<span class="tab-pane-title">DRIVER ' + (driver+1) + '</span> \
					</div>');

			jQuery('.tab-wrapper.driver-wrapper').addClass('display-none');
			jQuery('.tab-content-container').append('\
					<div class="tab-wrapper driver-wrapper" id="driver-panel-' + driver + '">\
					<div class="input-sub-wrapper-2" style="width: 409px;margin-right:0;height: 129px;margin-top: 27px;">\
						<span class="input-title">What is driver ' + (driver+1) + ' gender?</span>\
						<br>\
						<div class="input-container-inline">\
							<input type="radio" id="radio-gender-' + driver + '-M" class="input-radio" name="jform[Gender][' + driver + ']" value="MALE" onclick="radioClicked(this);" required/>\
							<label class="radio-label-logo" for="radio-gender-' + driver + '-M">\
								<img class="radio-label-gender-icon" alt="male" src="images/websiteicon/male-logo.png"/>\
								<span class="radio-label-title-gender">Male</span>\
							</label>\
						</div>\
						<div class="input-container-inline">\
							<input type="radio" id="radio-gender-' + driver + '-F" class="input-radio" name="jform[Gender][' + driver + ']" value="FEMALE" onclick="radioClicked(this);" required/>\
							<label class="radio-label-logo" for="radio-gender-' + driver + '-F">\
							<img class="radio-label-gender-icon" alt="female" src="images/websiteicon/female-logo.png"/>\
								<span class="radio-label-title-gender">Female</span>\
							</label>\
						</div>\
					</div>\
					<div class="input-sub-wrapper-2" style="margin-top: 80px;">\
						<span class="input-title">Date Of Birth</span>\
						<div class="input-container-block">\
							' + dateOfBirth2.replace(/jform\[dateOfBirth\]\[0\]/g, "jform[dateOfBirth]["+driver+"]") + '\
						</div>\
					</div>\
					<div class="input-sub-wrapper-2" style="display: inline-block;width: 40%;margin-right: 65px;vertical-align: top;">\
						<span class="input-title">First Name</span>\
						<div class="input-container-block">\
							<input type="text" class="input-text no-commas" name="jform[firstName]['+driver+']" required/>\
							<label for="jform[firstName]['+driver+']" class="error" style="display: none;">Comma is not allowed</label>\
						</div>\
					</div>\
					<div class="input-sub-wrapper-2" style="width: 409px;margin-right:0;">\
						<span class="input-title">What is driver ' + (driver+1) + ' job industry?</span>\
						<div class="input-container-block">\
							<div class="select_mate" data-mate-select="active" >\
								<select name="jform[Industry][' + driver + ']" onchange="" onclick="return false;" id="" class="hide-select custom-select" required>\
									<option value="" disabled selected>Select Occupation</option>\
									<?php echo $occupationParentListing; ?>\
								</select>\
							</div>\
						  	<!-- Custom select structure --> \
						</div>\
					</div>\
					<div class="input-sub-wrapper-2" style="width: 409px;margin-right:0;">\
						<span class="input-title">What is driver ' + (driver+1) + ' marital status?</span>\
						<div class="input-container-block">\
							<div class="select_mate" data-mate-select="active" >\
								<select name="jform[maritalStatus][' + driver + ']" onchange="" onclick="return false;" id="" class="hide-select custom-select" required>\
									<option value="" disabled selected>Marital Status</option>\
									<option value="Married">Married</option>\
								    <option value="Single">Single</option>\
									<option value="Divorced">Divorced</option>\
								  </select>\
							</div>\
						</div>\
					</div>\
					<div class="input-sub-wrapper-2" style="width: 409px;margin-right:0;">\
						<span class="input-title">What is driver ' + (driver+1) + ' job position?</span>\
						<div class="input-container-block job-position">\
							<div class="select_mate" data-mate-select="active" >\
								<select name="jform[Occupation][' + driver + ']" onchange="" onclick="return false;" id="" class="hide-select custom-select" required>\
									<option value="" disabled selected>Select Job Position</option>\
								</select>\
							</div>\
						  	<!-- Custom select structure --> \
						</div>\
					</div>\
					<div class="input-sub-wrapper-2" style="width: 409px;margin-right:0;">\
					<span class="input-title">Relationship with main driver?</span>\
					<div class="input-container-block">\
						<div class="select_mate" data-mate-select="active" >\
							<select name="jform[relationshipWithMainDriver][' + driver + ']" onchange="" onclick="return false;" id="" class="hide-select custom-select" required>\
								<option value="" disabled selected>Relationship</option>\
								<option value="F">Friend</option>\
								<option value="R">Relative</option>\
								<option value="L">Siblings</option>\
								<option value="P">Parent</option>\
								<option value="S">Spouse</option>\
								<option value="C">Child</option>\
								<option value="I">Self</option>\
								<option value="O">Others</option>\
							  </select>\
						</div>\
					</div>\
				</div>\
					<div class="input-wrapper add-driver">\
						<div onClick="removeDriver(this);" style="float:left" data-id="' + driver + '">\
							<img class="add-driver-img" alt="remove driver" src="images/websiteicon/remove-driver-logo.png"/>\
							<span class="remove-driver-title">REMOVE THIS DRIVER</span>\
						</div>\
						<div onClick="addDriver(this);" style="float:right">\
							<img class="add-driver-img" alt="add driver" src="images/websiteicon/add-driver-logo.png"/>\
							<span class="add-driver-title">ADD A DRIVER</span>\
						</div>\
					</div>\
				</div>\
					');
			jQuery('.custom-select').select2({width: '343px'});
			jQuery('.driver-container input').css("width", "343px");
			jQuery('.custom-select.night-parking').select2({width: '70%'});
			

			jQuery("input[name='jform\[dateOfBirth\]\["+driver+"\]']").datepicker({
				changeMonth: true,
				changeYear: true,
				showOn: "focus",
				dateFormat : "dd-M-yy",
				yearRange: "1947:1992",
				defaultDate: "01-JAN-1980",
				onChangeMonthYear:function(y, m, i){                     
					var d = i.selectedDay;
					jQuery(this).datepicker("setDate", new Date(y, m-1, d));
				}
			});
	 	}
	 	else {
	 		//driver--;
	 		jQuery("#driver-" + (driver-1)).after('<div class="tab-pane driver-pane active" onClick="changeTab(this);" id="driver-' + driver + '" data-id="driver-panel-' + driver + '"> \
		        	<img class="tab-pane-image" alt="add driver" src="images/websiteicon/driver-logo.png" /> \
			        	<span class="tab-pane-title">DRIVER ' + (driver+1) + '</span> \
					</div>');

			jQuery('.tab-wrapper.driver-wrapper').addClass('display-none');
			jQuery("#driver-panel-" + (driver-1)).after('\
					<div class="tab-wrapper driver-wrapper" id="driver-panel-' + driver + '">\
					<div class="input-sub-wrapper-2" style="width: 409px;margin-right:0;height: 129px;margin-top: 27px;">\
						<span class="input-title">Gender</span>\
						<br>\
						<div class="input-container-inline">\
							<input type="radio" id="radio-gender-' + driver + '-M" class="input-radio" name="jform[Gender][' + driver + ']" value="MALE" onclick="radioClicked(this);" required/>\
							<label class="radio-label-logo" for="radio-gender-' + driver + '-M">\
								<img class="radio-label-gender-icon" src="images/websiteicon/male-logo.png"/>\
								<span class="radio-label-title-gender">Male</span>\
							</label>\
						</div>\
						<div class="input-container-inline">\
							<input type="radio" id="radio-gender-' + driver + '-F" class="input-radio" name="jform[Gender][' + driver + ']" value="FEMALE" onclick="radioClicked(this);" required/>\
							<label class="radio-label-logo" for="radio-gender-' + driver + '-F">\
							<img class="radio-label-gender-icon" src="images/websiteicon/female-logo.png"/>\
								<span class="radio-label-title-gender">Female</span>\
							</label>\
						</div>\
					</div>\
					<div class="input-sub-wrapper-2" style="margin-top: 80px;">\
						<span class="input-title">Date Of Birth</span>\
						<div class="input-container-block">\
							' + dateOfBirth2.replace(/jform\[dateOfBirth\]\[0\]/g, "jform[dateOfBirth]["+driver+"]") + '\
						</div>\
					</div>\
					<div class="input-sub-wrapper-2" style="display: inline-block;width: 40%;margin-right: 65px;vertical-align: top;">\
						<span class="input-title">First Name</span>\
						<div class="input-container-block">\
							<input type="text" class="input-text no-commas" style="width: 343px;" name="jform[firstName]['+driver+']" required/>\
							<label for="jform[firstName]['+driver+']" class="error" style="display: none;">Comma is not allowed</label>\
						</div>\
					</div>\
					<div class="input-sub-wrapper-2" style="width: 409px;margin-right:0;margin-top: 2px;">\
						<span class="input-title">Job Industry</span>\
						<div class="input-container-block">\
							<div class="select_mate" data-mate-select="active" >\
								<select name="jform[Industry][' + driver + ']" onchange="" onclick="return false;" id="" class="hide-select custom-select" required>\
									<option value="" disabled selected>Select Job Industry</option>\
									<?php echo $occupationParentListing; ?>\
								</select>\
							</div>\
						</div>\
					</div>\
					<div class="input-sub-wrapper-2" style="display: inline-block;width: 40%;margin-right: 65px;vertical-align: top;">\
						<span class="input-title">Last Name</span>\
						<div class="input-container-block">\
							<input type="text" class="input-text no-commas" name="jform[lastName]['+driver+']"  required/>\
							<label for="jform[lastName]['+driver+']" class="error" style="display: none;">Comma is not allowed</label>\
						</div>\
					</div>\
					<div class="input-sub-wrapper-2" style="width: 409px;margin-right:0;margin-top:2px;">\
						<span class="input-title">Job Position</span>\
						<div class="input-container-block job-position">\
							<div class="select_mate" data-mate-select="active" >\
								<select name="jform[Occupation][' + driver + ']" onchange="" onclick="return false;" id="" class="hide-select custom-select" required>\
									<option value="" disabled selected>Select Job Position</option>\
								</select>\
							</div>\
						  	<!-- Custom select structure --> \
						</div>\
					</div>\
					<div class="input-sub-wrapper-2" style="display: inline-block;width: 40%;margin-right: 65px;vertical-align: top;">\
						<span class="input-title">Nationality</span>\
						<div class="insurediy-motor-custom-dropdown">'+
							`<select name="jform[driverNationality][` + driver + `]" class="custom-select" required>\
								<?php 
									$countryList = $this->form->getInput('country_list');
									
									foreach($countryList as $country) {
										foreach($country as $key => $val) {
											if($key == "HK"){
												echo '<option value="' . $key . '" selected="selected">' . $val . '</option>';
											} else {
												echo '<option value="' . $key . '">' . $val . '</option>';
											}
										}
									}
								?>\
							</select>` + 
						'</div>\
					</div>\
					<div class="input-sub-wrapper-2" style="display: inline-block;width: 40%;margin-right: 80px;vertical-align: top;margin-top: -2px;">\
						<span class="input-title">HKID number</span>\
						<div class="input-container-block">\
							<input type="text" class="input-text" name="jform[Nric][' + driver + ']" required/>\
						</div>\
					</div>\
					<div class="input-sub-wrapper-2" style="display: inline-block;width: 40%;margin-right: 65px;vertical-align: top;">\
						<span class="input-title">Driving License No.</span>\
						<div class="input-container-block">\
							<input type="text" class="input-text no-commas" name="jform[drivingLicense][' + driver + ']" required/>\
							<label for="jform[drivingLicense][' + driver + ']" class="error" style="display: none;">Comma is not allowed</label>\
						</div>\
					</div>\
					<div class="input-sub-wrapper-2" style="width: 409px;margin-right:0;margin-top:2px;">\
						<span class="input-title">Marital Status</span>\
						<div class="input-container-block">\
							<div class="select_mate" data-mate-select="active" >\
								<select name="jform[maritalStatus][' + driver + ']" onchange="" onclick="return false;" id="" class="hide-select custom-select" required>\
									<option value="" disabled selected>Marital Status</option>\
									<option value="Married">Married</option>\
								    <option value="Single">Single</option>\
									<option value="Divorced">Divorced</option>\
								  </select>\
							</div>\
						</div>\
					</div>\
					<div class="input-sub-wrapper-2" style="display: inline-block;width: 40%;margin-right: 65px;vertical-align: top;">\
						<span class="input-title">Date Of Passing Driving Test</span>\
						<div class="input-container-block">\
							' + drivingTest2.replace(/jform\[drivingTest\]\[0\]/g, "jform[drivingTest]["+driver+"]") + '\
						</div>\
					</div>\
					<div class="input-sub-wrapper-2" style="width: 409px;margin-right:0;margin-top:8px;">\
					<span class="input-title">Relationship with main driver</span>\
					<div class="input-container-block">\
						<div class="select_mate" data-mate-select="active" >\
							<select name="jform[relationshipWithMainDriver][' + driver + ']" onchange="" onclick="return false;" id="" class="hide-select custom-select" required>\
								<option value="" disabled selected>Relationship</option>\
								<option value="F">Friend</option>\
								<option value="R">Relative</option>\
								<option value="L">Siblings</option>\
								<option value="P">Parent</option>\
								<option value="S">Spouse</option>\
								<option value="C">Child</option>\
								<option value="I">Self</option>\
								<option value="O">Others</option>\
							  </select>\
						</div>\
					</div>\
				</div>\
					<div class="input-wrapper add-driver">\
						<div onClick="removeDriver(this);" style="float:left" data-id="' + driver + '">\
							<img class="add-driver-img" src="images/websiteicon/remove-driver-logo.png"/>\
							<span class="remove-driver-title">REMOVE THIS DRIVER</span>\
						</div>\
						<div onClick="addDriver(this);" style="float:right">\
							<img class="add-driver-img" src="images/websiteicon/add-driver-logo.png"/>\
							<span class="add-driver-title">ADD A DRIVER</span>\
						</div>\
					</div>\
				</div>\
					');
			jQuery('.custom-select').select2({width: '343px'});
			jQuery('.driver-container input').css("width", "343px");
			jQuery('.custom-select.night-parking').select2({width: '70%'});
			

			jQuery("input[name='jform\[dateOfBirth\]\["+driver+"\]']").datepicker({
				changeMonth: true,
				changeYear: true,
				showOn: "focus",
				dateFormat : "dd-M-yy",
				yearRange: "1947:1992",
				defaultDate: "01-JAN-1980",
				onChangeMonthYear:function(y, m, i){                     
					var d = i.selectedDay;
					jQuery(this).datepicker("setDate", new Date(y, m-1, d));
				}
			});

			jQuery("input[name='jform\[drivingTest\]\["+driver+"\]']").datepicker({
				changeMonth: true,
				changeYear: true,
				showOn: "focus",
				dateFormat : "dd-M-yy",
				yearRange: "1990:2050",
				defaultDate: "01-JAN-2014",
				onChangeMonthYear:function(y, m, i){                     
					var d = i.selectedDay;
					jQuery(this).datepicker("setDate", new Date(y, m-1, d));
				}
			});

			driver = (jQuery("[id^=driver-panel-]").length) - 1;
	 	}
	}
}

function removeDriver(obj) {
	var dataId = jQuery(obj).attr('data-id');
	var newId = dataId-1; 
	removedDriver.indexOf(dataId) === -1 ? removedDriver.push(dataId) : "";
	removedDriver.sort();

	driverLimit--;
	
	if(driver == 1) {
		//jQuery(obj).parents().eq(4).css("height", "580px");
	}

	if(!jQuery('#driver-'+newId).length) {
		newId = 0;
	}
	
	jQuery("#driver-" + dataId).remove();
	jQuery("#driver-panel-" + dataId).remove();

	jQuery(".tab-pane.driver-pane").removeClass("active");
	jQuery("#driver-"+ newId).addClass("active");
	jQuery('.tab-wrapper.driver-wrapper').addClass('display-none');
	jQuery("#driver-panel-"+ newId).removeClass("display-none");

	driver = Number(dataId)-1;
}

function changeLoginTab(obj) {
	jQuery(".tab-pane.login-pane").removeClass("active");
	jQuery(obj).addClass("active");
	jQuery('.tab-wrapper.login-wrapper').addClass('display-none');
	jQuery("#"+jQuery(obj).attr("data-id")).removeClass("display-none");
}

function changeTab(obj) {
	jQuery(".tab-pane.driver-pane").removeClass("active");
	jQuery(obj).addClass("active");
	jQuery('.tab-wrapper.driver-wrapper').addClass('display-none');
	jQuery("#"+jQuery(obj).attr("data-id")).removeClass("display-none");
}

function pushHeight(obj) {
	var panelHeight = jQuery(obj).parents().eq(4).height();
	var height = jQuery(obj).parent().find(".chzn-drop").height();
	var toggleFlag = jQuery(obj).parent().find(".chzn-single").attr("data-extend");
	
	if(typeof toggleFlag == "undefined") {
		jQuery(obj).attr("data-extend", "false");
		toggleFlag = jQuery(obj).parent().find(".chzn-single").attr("data-extend");
	}

	if(toggleFlag == "false") {
		jQuery(obj).parents().eq(4).animate({height:(panelHeight+height)},200);
		jQuery(obj).parent().find(".chzn-single").attr("data-extend", "true");
	}
	else {
		jQuery(obj).parents().eq(4).animate({height:(panelHeight-height)},200);
		jQuery(obj).parent().find(".chzn-single").attr("data-extend", "false");
	}	
}

jQuery(document).on("click", function(obj){
	var dropdown = jQuery(event.target).parent().attr('class');
	var dropdownObj = jQuery(event.target).closest('.chzn-single');
	
	if(jQuery(dropdownObj).attr('class') == "chzn-single") {
		var panelHeight = jQuery(dropdownObj).closest('.panel').height();
		var height = jQuery(dropdownObj).siblings(".chzn-drop").height();
		var toggleFlag = jQuery(dropdownObj).attr("data-extend");
		
		if(typeof toggleFlag == "undefined") {
			jQuery(dropdownObj).attr("data-extend", "false");
			toggleFlag = jQuery(dropdownObj).attr("data-extend");
		}

		if(toggleFlag == "false") {
			jQuery(dropdownObj).closest('.panel').animate({height:(panelHeight+height)},200);
			jQuery(dropdownObj).attr("data-extend", "true");
		}
		else {
			jQuery(dropdownObj).closest('.panel').animate({height:(panelHeight-height)},200);
			jQuery(dropdownObj).attr("data-extend", "false");
		}	
		//jQuery(dropdownObj).parents().eq(5).animate({height:(height+200)},200);
	}

	if(dropdown == "chzn-results") {
		dropdownObj = jQuery(event.target).parents().eq(2);

		var panelHeight = jQuery(dropdownObj).parents().eq(4).height();
		var height = jQuery(dropdownObj).find(".chzn-drop").height();
		var toggleFlag = jQuery(dropdownObj).find(".chzn-single").attr("data-extend");
		
		if(typeof toggleFlag == "undefined") {
			jQuery(dropdownObj).find(".chzn-single").attr("data-extend", "false");
			toggleFlag = jQuery(dropdownObj).find(".chzn-single").attr("data-extend");
		}

		if(toggleFlag == "false") {
			jQuery(dropdownObj).parents().eq(4).animate({height:(panelHeight+height)},200);
			jQuery(".chzn-single").attr("data-extend", "false");
			jQuery(dropdownObj).find(".chzn-single").attr("data-extend", "true");
		}
		else {
			jQuery(dropdownObj).parents().eq(4).animate({height:(panelHeight-height)},200);
			jQuery(".chzn-single").attr("data-extend", "false");
		}	
	}
});

jQuery(".no-commas").on('keydown', function(e){
    if (!e) var e = window.event;
     if (e.keyCode  ==  '188' )
     {
    	 jQuery(this).next().html("Comma is not allowed");
         jQuery(this).next().css("display", "block");
         return false;
     }
});

</script>

<script>
	dataLayer.push({
		'event': 'checkoutOption',
		'ecommerce': {
			'checkout': {
				'actionField': {'step': 1}
			}
		}
	});
	dataLayer.push({
		'event': 'checkoutOption',
		'ecommerce': {
			'checkout': {
				'actionField': {'step': 2}
			}
		}
	});
</script>
