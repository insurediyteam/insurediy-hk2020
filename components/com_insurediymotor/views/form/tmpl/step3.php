<?php
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');
JHtml::_('bootstrap.tooltip');
JHtml::_('formbehavior.chosen', '.insurediy-form-content select');
JHtml::_('script', 'system/jquery.validate.js', false, true);
JHtml::_('script', 'system/loadingoverlay.min.js', false, true);
JHtml::_('script', 'system/select2.min.js', false, true);
JHtml::_('stylesheet', 'system/select2.css', false, true);

$params = $this->state->get('params');
$form = $this->form;
$data = $this->item;
$plans = $this->plans;

$user = $this->user;

$from = strtotime($data["policyStartDate"]);
$today = time();
$difference =  floor(($from - $today)/ 86400);

$regYear = date($data["carRegisterYear"]);
$now = date("Y");


$yearDiff = $now - $regYear;

if ($plans) {

	$products = [];
	$aig = [];
	$pingan = [];
	$aw = [];
	$renewAig = [];
	$renewAxa = [];
	$renewAw = [];
	//print_r($plans);
	if(isset($plans["aig"])){
		$aig["insurer"] = "AIG";
		$aig["name"] = "AIG Car Insurance";
		$aig["premium"] = $plans["aig"]->aig_plan_premium_total;
		$aig["image"] = "https://www.insurediy.com/images/logo_companies/f7bg/f7bg-aig.jpg";
	} 

	if (isset($plans["pingan"])) {
		$pingan["insurer"] = "PINGAN";
		$pingan["name"] = "PINGAN Car Insurance";
		$pingan["premium"] = $plans["pingan"]->pingan_plan_premium_total;
		$pingan["image"] = "https://www.insurediy.com/images/logo_companies/f7bg/f7bg-pingan.jpg";
	} 

	if (isset($plans["aw"])) {
		$aw["insurer"] = "Allied World";
		$aw["name"] = "Allied World Car Insurance";
		$aw["premium"] = $plans["aw"]->aw_plan_premium_total;
		$aw["image"] = "https://www.insurediy.com/images/logo_companies/f7bg/f7bg-aw.jpg";
	}

	if($data["currentInsurer"] == "NCD000003") {
		$renewAig["insurer"] = "AIG Assurance";
		$renewAig["name"] = "AIG Car Insurance";
		$renewAig["premium"] = 0;
		$renewAig["image"] = "https://www.insurediy.com/images/logo_companies/f7bg/f7bg-aig.jpg";
	}

	if($data["currentInsurer"] == "NCD000001") {
		$renewAw["insurer"] = "Allied World";
		$renewAw["name"] = "Allied World Car Insurance";
		$renewAw["premium"] = 0;
		$renewAw["image"] = "https://www.insurediy.com/images/logo_companies/f7bg/f7bg-aw.jpg";
	}

	if($data["currentInsurer"] == "NCD000009") {
		$renewAxa["insurer"] = "AXA";
		$renewAxa["name"] = "AXA Car Insurance";
		$renewAxa["premium"] = 0;
		$renewAxa["image"] = "https://www.insurediy.com/images/logo_companies/f7bg/f7bg-axa.jpg";
	}

	$products = array($aig, $pingan, $aw, $renewAig, $renewAw, $renewAxa);

	if (isset($user->email)) {
		$mailQueue = (object)array(
			"email" => $user->email,
			"products" => array_filter($products),
			"type" => "car-after",
			"parent" => "car"
		);
		
		RemarketingHelpersRemarketing::newRemarketing($mailQueue);
	}
} else {
	if (isset($user->email)) {
		$mailQueue = (object)array(
			"email" => $user->email,
			"products" => 0,
			"type" => "car-before",
			"parent" => "car"
		);
		
		RemarketingHelpersRemarketing::newRemarketing($mailQueue);
	}
}
?>

<script>
	<?php 
		$jsData = json_encode($data);
		
		echo 'var currentInsurer = "'. $data["currentInsurer"]. '";';
	?>
	
</script>
<script>
	//Measure a view of product details. This example assumes the detail view occurs on pageload,
	//and also tracks a standard pageview of the details page.
	<?php if($plans && !empty($plans)): ?>
	
	dataLayer.push({
		'ecommerce': {
			'impressions': [
				<?php if(isset($plans["aig"]) && $plans): ?>
				{
					'name': '<?php echo $plans["aig"]->aig_plan_name; ?>', // Name or ID is required.
					'id': '<?php echo $plans["aig"]->aig_plan_id; ?>',
					'price': '<?php echo $plans["aig"]->aig_plan_premium_total; ?>',
					'brand': 'AIG',
					'category': 'Motor',
					'list': 'Motor Quotation',
					'position': '1'
				},
				<?php endif; ?>
				<?php if(isset($plans["aw"]) && $plans): ?>
				{
					'name': '<?php echo $plans["aw"]->aw_plan_name; ?>', // Name or ID is required.
					'id': '<?php echo $plans["aw"]->aw_plan_id; ?>',
					'price': '<?php echo $plans["aw"]->aw_plan_premium_total; ?>',
					'brand': 'AW',
					'category': 'Motor',
					'list': 'Motor Quotation',
					'position': '2'
				},
				<?php endif; ?>
				<?php if(isset($plans["pingan"]) && $plans): ?>
				{
					'name': '<?php echo $plans["pingan"]->pingan_plan_name; ?>', // Name or ID is required.
					'id': '<?php echo $plans["pingan"]->pingan_plan_id; ?>',
					'price': '<?php echo $plans["pingan"]->pingan_plan_premium_total; ?>',
					'brand': 'PINGAN',
					'category': 'Motor',
					'list': 'Motor Quotation',
					'position': '3'
				},
				<?php endif; ?>
			]
		}
	});

	<?php endif; ?>
	
	//Measure adding a product to a shopping cart by using an 'add' actionFieldObject
	//and a list of productFieldObjects.
	function addToCart(productObj) {
		dataLayer.push({
			'event': 'productClick',
			'ecommerce': {
				'click': {
					'actionField': {'list': 'Motor Quotation'}, // Optional list property.
					'products': [{
						'name': productObj.plan_name,
						'id': productObj.plan_index_code,
						'price': productObj.premium,
						'brand': productObj.insurer_code,
						'category': 'Motor',
						'variant': productObj.trip_type
					}]
				}
			}
		});
		dataLayer.push({
			'event': 'addToCart',
			'ecommerce': {
				'currencyCode': 'SGD',
				'add': { // 'add' actionFieldObject measures.
					'products': [{ // adding a product to a shopping cart.
						'name': productObj.plan_name,
						'id': productObj.plan_index_code,
						'price': productObj.premium,
						'brand': productObj.insurer_code,
						'category': 'Motor',
						'variant': productObj.trip_type,
						'quantity': 1
					}]
				}
			}
		});
	}	

</script>	
<div class="insurediy-form montserat-font">
	<div class="motor-header-top-wrapper">
			<?php echo InsureDIYMotorHelper::renderHeader('icon-travel', JText::_('COM_INSUREDIYMOTOR_PAGE_HEADING'), 3); ?>
	</div>
	<div class="edit<?php echo $this->pageclass_sfx; ?> insurediy-motor-form-content">
		<form action=""method="post"  name="getQuoteForm" id="getQuoteForm" class="">
		<div class="motor-quote-container" <?php if($plans && !empty($plans)){ echo "style=\"display:none;\""; } ?>>
			<div class="motor-quote-top-wrapper">
				<div class="motor-quote-content-text">
					<span class="font-yellow">
					<?php if($yearDiff > 9): ?>
						Comprehensive / Third Party, Fire and Theft
					<?php else: ?>
						Comprehensive
					<?php endif; ?>
					</span> private motor car insurance for 
					<span class="font-yellow"><?php echo $this->carData["make"]." ".$this->carData["model"]; ?></span>
					 manufactured in <span class="font-yellow"><?php echo $data["carMakeYear"]; ?></span> with NCD <span class="font-yellow"><?php echo $data["NCDPoints"]; ?>%</span>
					 to start on <span class="font-yellow"><?php echo $data["policyStartDate"]; ?></span>.
				</div>
			</div>
			<div class="motor-quote-bottom-wrapper">
				<div class="motor-quote-bottom-header">
					1. Select your car's model. Each insurer has a different list, so simply pick the one that is closest. <br>
					2. Click on "<span style="color: #fc5b6c;">Click Here To Get Quote</span>" to see offers by our insurers.
				</div>
				<div class="motor-quote-bottom-content-wrapper">
					<?php if($data["chinaExtension"] == "N"): ?>
					<?php if(count($this->makeModel["aig"])>0): ?>
					<div class="motor-quote-block-wrapper step3-model">
						<div class="motor-quote-block-header">
							My car model is
						</div>
						<img src="images/websiteicon/aig-logo.png" class="motor-quote-brand-logo">
						<select class="motor-quote-model-variant" id="insurer-model-select-1" name="variant-aig">
							<?php if(isset($data["vehVariant"][0]) && $data["vehVariant"][0] != ""):?>
								<option value="" selected>Select Car Variant</option>
								<?php if(is_array($this->makeModel["aig"])):?>
									<?php foreach($this->makeModel["aig"] as $item):?>
										<?php if($item->id == $data["vehVariant"][0]):?>
											<option value="<?php echo $item->id; ?>" selected="selected"><?php echo $item->model_name; ?></option>
										<?php else: ?>
											<option value="<?php echo $item->id; ?>"><?php echo $item->model_name; ?></option>
										<?php endif; ?>
									<?php endforeach;?>
								<?php endif; ?>
							<?php else: ?>
								<option value="" selected>Select Car Variant</option>
								<?php foreach($this->makeModel["aig"]as $item):?>
									<option value="<?php echo $item->id; ?>"><?php echo $item->model_name; ?></option>	
								<?php endforeach;?>
							<?php endif;?>
						</select>
					</div>
					<?php else: ?>
						<div class="motor-quote-block-wrapper step3-model">
							<div class="motor-quote-block-header">
								My car model is
							</div>
							<img src="images/websiteicon/aig-logo.png" class="motor-quote-brand-logo">
							<select class="motor-quote-model-variant" id="insurer-model-select-1" name="variant-aig">
											<option value="" selected="">Variant Not Found</option>
							</select>
							<!-- <small>Variant not found on Insurer</small> -->
							
						</div>
					<?php endif; ?>
					<?php else: ?>
					<input type="hidden" value="" name="variant-aig">
					<?php endif; ?>
					<?php if(count($this->makeModel["aw"])>0): ?>
					<div class="motor-quote-block-wrapper step3-model">
						<div class="motor-quote-block-header">
							My car model is
						</div>
						<img src="images/websiteicon/allied-world-logo.png" class="motor-quote-brand-logo">
						<select class="motor-quote-model-variant" id="insurer-model-select-2" name="variant-aw">
							<?php if(isset($data["vehVariant"][1]) && $data["vehVariant"][1] != ""):?>
								<option value="" selected>Select Car Variant</option>
								<?php if(is_array($this->makeModel["aw"])):?>
									<?php foreach($this->makeModel["aw"] as $item):?>
										<?php if($item->id == $data["vehVariant"][1]):?>
											<option value="<?php echo $item->id; ?>" selected="selected"><?php echo $item->model_name ." - ". $item->model_cc . "CC - " . $item->year." - ". $item->description; ?></option>
										<?php else: ?>
											<option value="<?php echo $item->id; ?>"><?php echo $item->model_name ." - ". $item->model_cc . "CC - " . $item->year." - ". $item->description; ?></option>
										<?php endif; ?>
									<?php endforeach;?>
								<?php endif; ?>
							<?php else: ?>
								<option value="" selected>Select Car Variant</option>
								<?php foreach($this->makeModel["aw"]as $item):?>
									<option value="<?php echo $item->id; ?>"><?php echo $item->model_name ." - ". $item->model_cc . "CC - " . $item->year." - ". $item->description; ?></option>	
								<?php endforeach;?>
							<?php endif;?>
						</select>
						
					</div>
					<?php else: ?>
						<div class="motor-quote-block-wrapper step3-model">
							<div class="motor-quote-block-header">
								My car model is
							</div>
							<img src="images/websiteicon/allied-world-logo.png" class="motor-quote-brand-logo">
							<select class="motor-quote-model-variant" id="insurer-model-select-2" name="variant-aw">
											<option value="" selected="">Variant Not Found</option>
											<?php foreach($this->makeModel["aw"]as $item):?>
												<option value="<?php echo $item->id; ?>"><?php echo $item->model_name ." - ". $item->model_cc . "CC - " . $item->year; ?></option>	
											<?php endforeach;?>
							</select>
							
						</div>
					<?php endif; ?>
					<?php if($data["chinaExtension"] == "N" && $data["currentInsurer"] != "NCD000002noRenew"): ?>
					<?php if(count($this->makeModel["pingan"])>0): ?>
					<div class="motor-quote-block-wrapper step3-model">
						<div class="motor-quote-block-header">
							My car model is
						</div>
						<img src="images/websiteicon/pingan-logo.png" class="motor-quote-brand-logo">
						<select class="motor-quote-model-variant" id="insurer-model-select-3" name="variant-pingan">
							<?php if(isset($data["vehVariant"][2]) && $data["vehVariant"][2] != ""):?>
								<option value="" selected>Select Car Variant</option>
								<?php if(is_array($this->makeModel["pingan"])):?>
									<?php foreach($this->makeModel["pingan"] as $item):?>
										<?php if($item->id == $data["vehVariant"][2]):?>
											<option value="<?php echo $item->id; ?>" selected="selected"><?php echo $item->model_name; ?></option>
										<?php else: ?>
											<option value="<?php echo $item->id; ?>"><?php echo $item->model_name; ?></option>
										<?php endif; ?>
									<?php endforeach;?>
								<?php endif; ?>
							<?php else: ?>
								<option value="" selected>Select Car Variant</option>
								<?php foreach($this->makeModel["pingan"]as $item):?>
									<option value="<?php echo $item->id; ?>"><?php echo $item->model_name; ?></option>	
								<?php endforeach;?>
							<?php endif;?>
						</select>
						
					</div>
					<?php else: ?>
						<div class="motor-quote-block-wrapper step3-model">
							<div class="motor-quote-block-header">
							My car model is
							</div>
							<img src="images/websiteicon/pingan-logo.png" class="motor-quote-brand-logo">
							<select class="motor-quote-model-variant" id="insurer-model-select-3" name="variant-pingan">
											<option value="" selected="">Variant Not Found</option>
							</select>
							<!-- <small>Variant not found on Insurer</small> -->
							
						</div>
					<?php endif; ?>
					<?php else: ?>
					<input type="hidden" value="" name="variant-pingan">
					<?php endif; ?>
				</div>
				
				<div class="motor-quote-bottom-button-wrapper">
					<div>
						<img style="display:none" class="captcha-img" src="" alt="">
						<input style="display:none" type="text" name="captcha">
						<span class="wrong-captcha" style="display:block;font-size:11px;">Please enter captcha (Case Sensitive)</span>
					</div>
					<input id="validateCaptcha" type="button" class="motor-get-quote-button" value="Click Here To Get Quote" />
					<input style="display:none" type="submit" class="motor-get-quote-button post-captcha" value="Click Here To Get Quote" />
					<span class="warning-text"> *Please note that quotes from some insurers may not be available.</span>
				</div>
				<div class="motor-btn-wrapper" style="padding-top:20px;">
					<div class="motor-btn-group">
						<a href="/component/insurediymotor/?view=motor&Itemid=159&step=1" style="text-decoration:none;margin-right:20px; padding: 10px 18px 10px 10px;" class="motor-btn validate"><span class="white-arrow-left"></span>BACK</a>
					</div>
				</div>
			</div>
		</div>
		</form>
		
		<div class="motor-quote-result-container" <?php if($plans && !empty($plans)){ echo "style=\"display:block;\""; } ?>>
			<div class="motor-quote-result-top-wrapper">
				<div class="motor-quote-edit-button">
					EDIT MOTOR DETAILS 
					<span class="white-pen"></span>
				</div>
			</div>
			<div class="motor-quote-result-middle-wrapper">
			<table class="motor-result-table">
					<tr class="motor-result-row">
						<?php if($plans && !empty($plans)): ?>
						<td class="motor-result-column-title">
									
						</td>

						<?php if(isset($plans["aig"]) && $plans && $data["currentInsurer"] != "NCD000003"): ?>
						<td class="motor-result-column aig-column">
							<div class="motor-quote-result-brand-logo-container">
								<img src="images/websiteicon/aig-logo.png" class="motor-quote-result-brand-logo">
							</div>
							<div class="motor-quote-result-block-header">
								<div class="motor-quote-premium-header"><?php echo $plans["aig"]->aig_plan_name; ?></div>
								<div class="motor-quote-premium">HK$<?php echo number_format(($plans["aig"]->aig_plan_premium_total), 2, '.', ','); ?><span class="motor-quote-premium-promo"></span></div>
								<div class="motor-quote-discount"><!-- $<?php echo number_format($plans["aig"]->aig_plan_premium_total, 2, '.', ','); ?> --></div>
								<div class="motor-quote-diy-points">
									<?php if ($data["vehicleValue"] < 150000): ?>
									*based on $HK 150,000 car value
									<?php endif; ?>
								</div>  
							</div>
							<div class="motor-quote-result-block-body renewal-text-red" style="margin:0;<?php echo ($user->id > 0) ? "display:none;" : ""; ?>">
								<a class="loginpopup" href="#">Click here to login</a> and apply code "CAR200" for HK$200 off at checkout
							</div>
							<div class="motor-quote-result-block-footer">
								<form id="aig-form" action="<?php echo JRoute::_('index.php?option=com_insurediymotor'); ?>" method="POST">
									<input type="hidden" name="task" value="form.step3save" />
									<input type="hidden" name="jform[id]" value="<?php echo $plans["aig"]->id; ?>"/>
									<input type="hidden" name="jform[plan_name]" value="<?php echo $plans["aig"]->aig_plan_name; ?>"/>
									<input type="hidden" name="jform[plan_id]" value="<?php echo $plans["aig"]->aig_plan_id; ?>"/>
									<input type="hidden" name="jform[partner_id]" value="aig"/>
										<input type="hidden" name="jform[quotationId]" value="<?php echo $data["quotationId"]; ?>"/>
									<?php echo JHtml::_('form.token'); ?>
									<input type="submit" class="motor-quote-buy-button buy-aig" value="BUY NOW" onclick="confirmAlert(); addToCart({plan_name:'<?php echo $plans["aig"]->aig_plan_name; ?>', plan_index_code:'<?php echo $plans["aig"]->aig_plan_id; ?>', premium:'<?php echo $plans["aig"]->aig_plan_premium_total; ?>', insurer_code:'aig', trip_type:''})" <?php if($plans["aig"]->aig_plan_premium_total == 0) echo 'disabled'; ?>/>
								</form>
							</div>
						</td>
						<td class="motor-result-divider msig-column aig-column">&nbsp;</td>
						<?php elseif($data["currentInsurer"] == "NCD000003") : ?>
						<td class="motor-result-divider renewal-column">&nbsp;</td>
							<td class="motor-result-column renewal-column renewal-container">
							<div class="motor-quote-result-brand-logo-container">
								<img src="<?php echo JURI::base(); ?>/images/websiteicon/aig-logo.png" class="motor-quote-result-brand-logo">
							</div>
							<div class="motor-quote-result-block-header">
								<div class="motor-quote-premium-header">Your Current Insurer is</div>
								<div class="motor-quote-premium renewal-company-name">AIG Assurance</div>
							</div>
							<div class="motor-quote-result-block-body renewal-text-red">
								<?php echo ($data["coverType"] == "01" ? "Renew with us to get HK$200 off" : "Renew with us to get HK$100 off" ); ?>
							</div>
							<div class="motor-quote-result-block-footer">
								<form target="_blank" action="<?php echo JRoute::_('index.php?option=com_insurediymotor'); ?>" method="POST">
								<input type="hidden" name="task" value="form.renewalcase" />
								<input type="hidden" name="jform[partner_id]" value="aig"/>
								<input type="hidden" name="jform[quotationId]" value="<?php echo $data["quotationId"]; ?>"/>
								<?php echo JHtml::_('form.token'); ?>
								<input type="submit" class="motor-quote-buy-button renewal-text-red" value="Renew now" />
								</form>
							</div>
							<div class="renewal-overlay">
								<div class="renewal-hover-text-container">
									<div class="renewal-hover-text-h1" style="font-size:160% !important;line-height:24px;"><?php echo ($data["coverType"] == "01" ? "HK$200 OFF" : "HK$100 OFF" ); ?></div>
									<div class="renewal-hover-text">your renewal <br> price now!</div>
								</div>
								<div class="motor-quote-result-block-footer">
									<form target="_blank" action="<?php echo JRoute::_('index.php?option=com_insurediymotor'); ?>" method="POST">
									<input type="hidden" name="task" value="form.renewalcase" />
									<input type="hidden" name="jform[partner_id]" value="aig"/>
									<input type="hidden" name="jform[quotationId]" value="<?php echo $data["quotationId"]; ?>"/>
									<?php echo JHtml::_('form.token'); ?>
									<input type="submit" class="motor-quote-buy-button renewal-text-red" value="Renew now" />
									</form>
								</div>
							</div>
						</td>
						<td class="motor-result-divider msig-column hla-column">&nbsp;</td>
						<?php endif; ?>
						<?php if(isset($plans["aw"]) && $plans && $data["currentInsurer"] != "NCD000001"): ?>
						<td class="motor-result-column aw-column">
							<div class="motor-quote-result-brand-logo-container">
								<img src="images/websiteicon/allied-world-logo.png" class="motor-quote-result-brand-logo">
							</div>
							<div class="motor-quote-result-block-header">
								<div class="motor-quote-premium-header"><?php echo $plans["aw"]->aw_plan_name; ?></div>
								<div class="motor-quote-premium">HK$<?php echo number_format(($plans["aw"]->aw_plan_premium_total), 2, '.', ','); ?><span class="motor-quote-premium-promo"></span></div>
							</div>
							<div class="motor-quote-result-block-body renewal-text-red" style="margin:0;<?php echo ($user->id > 0) ? "display:none;" : ""; ?>">
								<a class="loginpopup" href="#">Click here to login</a> and apply code "CAR200" for HK$200 off at checkout
							</div>
							<div class="motor-quote-result-block-footer">
								<form id="aw-form" action="<?php echo JRoute::_('index.php?option=com_insurediymotor'); ?>" method="POST">
									<input type="hidden" name="task" value="form.step3save" />
									<input type="hidden" name="jform[id]" value="<?php echo $plans["aw"]->id; ?>"/>
									<input type="hidden" name="jform[plan_name]" value="<?php echo $plans["aw"]->aw_plan_name; ?>"/>
									<input type="hidden" name="jform[plan_id]" value="<?php echo $plans["aw"]->aw_plan_id; ?>"/>
									<input type="hidden" name="jform[partner_id]" value="aw"/>
										<input type="hidden" name="jform[quotationId]" value="<?php echo $data["quotationId"]; ?>"/>
									<?php echo JHtml::_('form.token'); ?>
									<input type="submit" class="motor-quote-buy-button buy-aw" value="BUY NOW" onclick="confirmAlert(); addToCart({plan_name:'<?php echo $plans["aw"]->aw_plan_name; ?>', plan_index_code:'<?php echo $plans["aw"]->aw_plan_id; ?>', premium:'<?php echo $plans["aw"]->aw_plan_premium_total; ?>', insurer_code:'aw', trip_type:''})" <?php if($plans["aw"]->aw_plan_premium_total == 0) echo 'disabled'; ?>/>
								</form>
							</div>
						</td>
						<td class="motor-result-divider msig-column aw-column">&nbsp;</td>
						<?php elseif($data["currentInsurer"] == "NCD000001") : ?>
						<td class="motor-result-divider renewal-column">&nbsp;</td>
							<td class="motor-result-column renewal-column renewal-container">
							<div class="motor-quote-result-brand-logo-container">
								<img src="<?php echo JURI::base(); ?>/images/websiteicon/allied-world-logo.png" class="motor-quote-result-brand-logo">
							</div>
							<div class="motor-quote-result-block-header">
								<div class="motor-quote-premium-header">Your Current Insurer is</div>
								<div class="motor-quote-premium renewal-company-name">Allied World</div>
							</div>
							<div class="motor-quote-result-block-body renewal-text-red">
								<?php echo ($data["coverType"] == "01" ? "Renew with us to get HK$200 off" : "Renew with us to get HK$100 off" ); ?>
							</div>
							<div class="motor-quote-result-block-footer">
								<form target="_blank" action="<?php echo JRoute::_('index.php?option=com_insurediymotor'); ?>" method="POST">
								<input type="hidden" name="task" value="form.renewalcase" />
								<input type="hidden" name="jform[partner_id]" value="aw"/>
								<input type="hidden" name="jform[quotationId]" value="<?php echo $data["quotationId"]; ?>"/>
								<?php echo JHtml::_('form.token'); ?>
								<input type="submit" class="motor-quote-buy-button renewal-text-red" value="Renew now" />
								</form>
							</div>
							<div class="renewal-overlay">
								<div class="renewal-hover-text-container">
									<div class="renewal-hover-text-h1" style="font-size:160% !important;line-height:24px;"><?php echo ($data["coverType"] == "01" ? "HK$200 OFF" : "HK$100 OFF" ); ?></div>
									<div class="renewal-hover-text">your renewal <br> price now!</div>
								</div>
								<div class="motor-quote-result-block-footer">
									<form target="_blank" action="<?php echo JRoute::_('index.php?option=com_insurediymotor'); ?>" method="POST">
									<input type="hidden" name="task" value="form.renewalcase" />
									<input type="hidden" name="jform[partner_id]" value="aw"/>
									<input type="hidden" name="jform[quotationId]" value="<?php echo $data["quotationId"]; ?>"/>
									<?php echo JHtml::_('form.token'); ?>
									<input type="submit" class="motor-quote-buy-button renewal-text-red" value="Renew now" />
									</form>
								</div>
							</div>
						</td>
						<td class="motor-result-divider msig-column hla-column">&nbsp;</td>
						<?php endif; ?>
						<?php if(isset($plans["pingan"]) && $plans && $data["currentInsurer"] != "NCD000002noRenew"): ?>
						<td class="motor-result-column pingan-column">
							<div class="motor-quote-result-brand-logo-container">
								<img src="images/websiteicon/pingan-logo.png" class="motor-quote-result-brand-logo">
							</div>
							<div class="motor-quote-result-block-header">
								<div class="motor-quote-premium-header"><?php echo $plans["pingan"]->pingan_plan_name; ?></div>
								<div class="motor-quote-premium">HK$<?php echo number_format(($plans["pingan"]->pingan_plan_premium_total), 2, '.', ','); ?><span class="motor-quote-premium-promo"></span></div>
							</div>
							<div class="motor-quote-result-block-body renewal-text-red" style="margin:0;<?php echo ($user->id > 0) ? "display:none;" : ""; ?>">
								<a class="loginpopup" href="#">Click here to login</a> and apply code "CAR200" for HK$200 off at checkout
							</div>
							<div class="motor-quote-result-block-footer">
								<form id="pingan-form" action="<?php echo JRoute::_('index.php?option=com_insurediymotor'); ?>" method="POST">
									<input type="hidden" name="task" value="form.step3save" />
									<input type="hidden" name="jform[id]" value="<?php echo $plans["pingan"]->id; ?>"/>
									<input type="hidden" name="jform[plan_name]" value="<?php echo $plans["pingan"]->pingan_plan_name; ?>"/>
									<input type="hidden" name="jform[plan_id]" value="<?php echo $plans["pingan"]->pingan_plan_id; ?>"/>
									<input type="hidden" name="jform[partner_id]" value="pingan"/>
										<input type="hidden" name="jform[quotationId]" value="<?php echo $data["quotationId"]; ?>"/>
									<?php echo JHtml::_('form.token'); ?>
									<input type="submit" class="motor-quote-buy-button buy-pingan" value="BUY NOW" onclick="confirmAlert(); addToCart({plan_name:'<?php echo $plans["pingan"]->pingan_plan_name; ?>', plan_index_code:'<?php echo $plans["pingan"]->pingan_plan_id; ?>', premium:'<?php echo $plans["pingan"]->pingan_plan_premium_total; ?>', insurer_code:'pingan', trip_type:''})" <?php if($plans["pingan"]->pingan_plan_premium_total == 0) echo 'disabled'; ?>/>
								</form>
							</div>
						</td>
						<td class="motor-result-divider msig-column pingan-column">&nbsp;</td>
						<?php elseif($data["currentInsurer"] == "NCD000002") : ?>
						<td class="motor-result-divider renewal-column">&nbsp;</td>
							<td class="motor-result-column renewal-column renewal-container">
							<div class="motor-quote-result-brand-logo-container">
								<img src="<?php echo JURI::base(); ?>/images/websiteicon/pingan-logo.png" class="motor-quote-result-brand-logo">
							</div>
							<div class="motor-quote-result-block-header">
								<div class="motor-quote-premium-header">Your Current Insurer is</div>
								<div class="motor-quote-premium renewal-company-name">Pingan</div>
							</div>
							<div class="motor-quote-result-block-body renewal-text-red">
								<?php echo ($data["coverType"] == "01" ? "Renew with us to get HK$200 off" : "Renew with us to get HK$100 off" ); ?>
							</div>
							<div class="motor-quote-result-block-footer">
								<form target="_blank" action="<?php echo JRoute::_('index.php?option=com_insurediymotor'); ?>" method="POST">
								<input type="hidden" name="task" value="form.renewalcase" />
								<input type="hidden" name="jform[partner_id]" value="pingan"/>
								<input type="hidden" name="jform[quotationId]" value="<?php echo $data["quotationId"]; ?>"/>
								<?php echo JHtml::_('form.token'); ?>
								<input type="submit" class="motor-quote-buy-button renewal-text-red" value="Renew now" />
								</form>
							</div>
							<div class="renewal-overlay">
								<div class="renewal-hover-text-container">
									<div class="renewal-hover-text-h1" style="font-size:160% !important;line-height:24px;"><?php echo ($data["coverType"] == "01" ? "HK$200 OFF" : "HK$100 OFF" ); ?></div>
									<div class="renewal-hover-text">your renewal <br> price now!</div>
								</div>
								<div class="motor-quote-result-block-footer">
									<form target="_blank" action="<?php echo JRoute::_('index.php?option=com_insurediymotor'); ?>" method="POST">
									<input type="hidden" name="task" value="form.renewalcase" />
									<input type="hidden" name="jform[partner_id]" value="pingan"/>
									<input type="hidden" name="jform[quotationId]" value="<?php echo $data["quotationId"]; ?>"/>
									<?php echo JHtml::_('form.token'); ?>
									<input type="submit" class="motor-quote-buy-button renewal-text-red" value="Renew now" />
									</form>
								</div>
							</div>
						</td>
						<td class="motor-result-divider msig-column hla-column">&nbsp;</td>
						<?php endif; ?>
						<?php if($data["currentInsurer"] == "NCD000009") : ?>
						<td class="motor-result-divider renewal-column">&nbsp;</td>
							<td class="motor-result-column renewal-column renewal-container">
							<div class="motor-quote-result-brand-logo-container">
								<img src="<?php echo JURI::base(); ?>/images/websiteicon/axa-logo.png" class="motor-quote-result-brand-logo">
							</div>
							<div class="motor-quote-result-block-header">
								<div class="motor-quote-premium-header">Your Current Insurer is</div>
								<div class="motor-quote-premium renewal-company-name">AXA</div>
							</div>
							<div class="motor-quote-result-block-body renewal-text-red">
								<?php echo ($data["coverType"] == "01" ? "Renew with us to get HK$200 off" : "Renew with us to get HK$100 off" ); ?>
							</div>
							<div class="motor-quote-result-block-footer">
								<form target="_blank" action="<?php echo JRoute::_('index.php?option=com_insurediymotor'); ?>" method="POST">
								<input type="hidden" name="task" value="form.renewalcase" />
								<input type="hidden" name="jform[partner_id]" value="axa"/>
								<input type="hidden" name="jform[quotationId]" value="<?php echo $data["quotationId"]; ?>"/>
								<?php echo JHtml::_('form.token'); ?>
								<input type="submit" class="motor-quote-buy-button renewal-text-red" value="Renew now" />
								</form>
							</div>
							<div class="renewal-overlay">
								<div class="renewal-hover-text-container">
									<div class="renewal-hover-text-h1" style="font-size:160% !important;line-height:24px;"><?php echo ($data["coverType"] == "01" ? "HK$200 OFF" : "HK$100 OFF" ); ?></div>
									<div class="renewal-hover-text">your renewal <br> price now!</div>
								</div>
								<div class="motor-quote-result-block-footer">
									<form target="_blank" action="<?php echo JRoute::_('index.php?option=com_insurediymotor'); ?>" method="POST">
									<input type="hidden" name="task" value="form.renewalcase" />
									<input type="hidden" name="jform[partner_id]" value="axa"/>
									<input type="hidden" name="jform[quotationId]" value="<?php echo $data["quotationId"]; ?>"/>
									<?php echo JHtml::_('form.token'); ?>
									<input type="submit" class="motor-quote-buy-button renewal-text-red" value="Renew now" />
									</form>
								</div>
							</div>
						</td>
						<td class="motor-result-divider msig-column hla-column">&nbsp;</td>
						<?php endif; ?>
						<?php if($data["currentInsurer"] == "NCD000031") : ?>
						<td class="motor-result-divider renewal-column">&nbsp;</td>
							<td class="motor-result-column renewal-column renewal-container">
							<div class="motor-quote-result-brand-logo-container">
								<img src="<?php echo JURI::base(); ?>/images/websiteicon/msig-logo.png" class="motor-quote-result-brand-logo">
							</div>
							<div class="motor-quote-result-block-header">
								<div class="motor-quote-premium-header">Your Current Insurer is</div>
								<div class="motor-quote-premium renewal-company-name">MSIG</div>
							</div>
							<div class="motor-quote-result-block-body renewal-text-red">
								<?php echo ($data["coverType"] == "01" ? "Renew with us to get HK$200 off" : "Renew with us to get HK$100 off" ); ?>
							</div>
							<div class="motor-quote-result-block-footer">
								<form target="_blank" action="<?php echo JRoute::_('index.php?option=com_insurediymotor'); ?>" method="POST">
								<input type="hidden" name="task" value="form.renewalcase" />
								<input type="hidden" name="jform[partner_id]" value="msig"/>
								<input type="hidden" name="jform[quotationId]" value="<?php echo $data["quotationId"]; ?>"/>
								<?php echo JHtml::_('form.token'); ?>
								<input type="submit" class="motor-quote-buy-button renewal-text-red" value="Renew now" />
								</form>
							</div>
							<div class="renewal-overlay">
								<div class="renewal-hover-text-container">
									<div class="renewal-hover-text-h1" style="font-size:160% !important;line-height:24px;"><?php echo ($data["coverType"] == "01" ? "HK$200 OFF" : "HK$100 OFF" ); ?></div>
									<div class="renewal-hover-text">your renewal <br> price now!</div>
								</div>
								<div class="motor-quote-result-block-footer">
									<form target="_blank" action="<?php echo JRoute::_('index.php?option=com_insurediymotor'); ?>" method="POST">
									<input type="hidden" name="task" value="form.renewalcase" />
									<input type="hidden" name="jform[partner_id]" value="msig"/>
									<input type="hidden" name="jform[quotationId]" value="<?php echo $data["quotationId"]; ?>"/>
									<?php echo JHtml::_('form.token'); ?>
									<input type="submit" class="motor-quote-buy-button renewal-text-red" value="Renew now" />
									</form>
								</div>
							</div>
						</td>
						<td class="motor-result-divider msig-column hla-column">&nbsp;</td>
						<?php endif; ?>
						<?php if($data["currentInsurer"] == "NCD000030") : ?>
						<td class="motor-result-divider renewal-column">&nbsp;</td>
							<td class="motor-result-column renewal-column renewal-container">
							<div class="motor-quote-result-brand-logo-container">
								<img src="<?php echo JURI::base(); ?>/images/websiteicon/china-taiping-logo.png" class="motor-quote-result-brand-logo">
							</div>
							<div class="motor-quote-result-block-header">
								<div class="motor-quote-premium-header">Your Current Insurer is</div>
								<div class="motor-quote-premium renewal-company-name">China Taiping</div>
							</div>
							<div class="motor-quote-result-block-body renewal-text-red">
								<?php echo ($data["coverType"] == "01" ? "Renew with us to get HK$200 off" : "Renew with us to get HK$100 off" ); ?>
							</div>
							<div class="motor-quote-result-block-footer">
								<form target="_blank" action="<?php echo JRoute::_('index.php?option=com_insurediymotor'); ?>" method="POST">
								<input type="hidden" name="task" value="form.renewalcase" />
								<input type="hidden" name="jform[partner_id]" value="taiping"/>
								<input type="hidden" name="jform[quotationId]" value="<?php echo $data["quotationId"]; ?>"/>
								<?php echo JHtml::_('form.token'); ?>
								<input type="submit" class="motor-quote-buy-button renewal-text-red" value="Renew now" />
								</form>
							</div>
							<div class="renewal-overlay">
								<div class="renewal-hover-text-container">
									<div class="renewal-hover-text-h1" style="font-size:160% !important;line-height:24px;"><?php echo ($data["coverType"] == "01" ? "HK$200 OFF" : "HK$100 OFF" ); ?></div>
									<div class="renewal-hover-text">your renewal <br> price now!</div>
								</div>
								<div class="motor-quote-result-block-footer">
									<form target="_blank" action="<?php echo JRoute::_('index.php?option=com_insurediymotor'); ?>" method="POST">
									<input type="hidden" name="task" value="form.renewalcase" />
									<input type="hidden" name="jform[partner_id]" value="taiping"/>
									<input type="hidden" name="jform[quotationId]" value="<?php echo $data["quotationId"]; ?>"/>
									<?php echo JHtml::_('form.token'); ?>
									<input type="submit" class="motor-quote-buy-button renewal-text-red" value="Renew now" />
									</form>
								</div>
							</div>
						</td>
						<td class="motor-result-divider msig-column hla-column">&nbsp;</td>
						<?php endif; ?>

						<?php if(isset($this->renewal)): ?>
						<!-- <td class="motor-result-divider renewal-column">&nbsp;</td>
						<td class="motor-result-column renewal-column renewal-container">
							<div class="motor-quote-result-brand-logo-container">
								<img src="<?php echo JURI::base(); ?>/images/websiteicon/<?php echo  $this->renewal["partnerCode"]; ?>-logo.png" class="motor-quote-result-brand-logo">
							</div>
							<div class="motor-quote-result-block-header">
								<div class="motor-quote-premium-header">Your Current Insurer is</div>
								<div class="motor-quote-premium renewal-company-name"><?php echo $this->renewal["partnerName"]; ?></div>
							</div>
							<div class="motor-quote-result-block-body renewal-text-red">
								<?php echo ($data["coverType"] == "01" ? "Renew with us to get HK$200 off" : "Renew with us to get HK$100 off" ); ?>
							</div>
							<div class="motor-quote-result-block-footer">
								<form target="_blank" action="<?php echo JRoute::_('index.php?option=com_insurediymotor'); ?>" method="POST">
								<input type="hidden" name="task" value="form.renewalcase" />
								<input type="hidden" name="jform[partner_id]" value="<?php echo  $this->renewal["partnerCode"]; ?>"/>
								<input type="hidden" name="jform[quotationId]" value="<?php echo $data["quotationId"]; ?>"/>
								<?php echo JHtml::_('form.token'); ?>
								<input type="submit" class="motor-quote-buy-button renewal-text-red" value="Renew now" />
								</form>
							</div>
							<div class="renewal-overlay">
								<div class="renewal-hover-text-container">
									<div class="renewal-hover-text-h1" style="font-size:160% !important;line-height:24px;"><?php echo ($data["coverType"] == "01" ? "HK$200 OFF" : "HK$100 OFF" ); ?></div>
									<div class="renewal-hover-text">your renewal <br> price now!</div>
								</div>
								<div class="motor-quote-result-block-footer">
									<form target="_blank" action="<?php echo JRoute::_('index.php?option=com_insurediymotor'); ?>" method="POST">
									<input type="hidden" name="task" value="form.renewalcase" />
									<input type="hidden" name="jform[partner_id]" value="<?php echo  $this->renewal["partnerCode"]; ?>"/>
									<input type="hidden" name="jform[quotationId]" value="<?php echo $data["quotationId"]; ?>"/>
									<?php echo JHtml::_('form.token'); ?>
									<input type="submit" class="motor-quote-buy-button renewal-text-red" value="Renew now" />
									</form>
								</div>
							</div>
						</td> -->
						<?php endif; ?>
					<?php endif; ?>
					</tr>
				</table>
			
			</div>
			<?php if($data["coverType"] == "01"): ?>
			<div class="motor-quote-result-bottom-wrapper">
				<div class="motor-form-input-wrapper">
					<div class="motor-accordion">Cover Types & Excess</div>
					<div class="panel" style="padding: 0 2px; margin-top: -15px;margin-bottom: 15px;">
						<table class="motor-benefit-table">
							<tr class="motor-benefit-row cover-type-row">
								<td class="motor-benefit-column-title">
									Cover Type
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									<?php if($data["coverType"] == "02"): ?>
										Third Party
										<?php else: ?>
										Comprehensive
									<?php endif; ?>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									<?php if($data["coverType"] == "02"): ?>
										Third Party
										<?php else: ?>
										Comprehensive
									<?php endif; ?>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									<?php if($data["coverType"] == "02"): ?>
										Third Party
										<?php else: ?>
										Comprehensive
									<?php endif; ?>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
										-
									</td>
								<?php endif; ?>
							</tr>
							<tr class="motor-benefit-row motor-benefit-cover-type-row">
								<td class="motor-benefit-column-title">
									General Excess
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ <?php echo number_format(($plans["aig"]->aig_plan_general_excess), 0, '.', ','); ?>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ <?php echo number_format(($plans["aw"]->aw_plan_general_excess), 0, '.', ','); ?>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ <?php echo number_format(($plans["pingan"]->pingan_plan_general_excess), 0, '.', ','); ?>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column" id="motor-benefit-key-excess-renewal">
										-
									</td>
								<?php endif; ?>
							</tr>
							<tr class="motor-benefit-row">
								<td class="motor-benefit-column-title">
									Unnamed Driver Excess
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ <?php echo number_format(($plans["aig"]->aig_plan_unammed_excess), 0, '.', ','); ?>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ <?php echo number_format(($plans["aw"]->aw_plan_unammed_excess), 0, '.', ','); ?>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ <?php echo number_format(($plans["pingan"]->pingan_plan_unammed_excess), 0, '.', ','); ?>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column">
										-
									</td>
								<?php endif; ?>
							</tr>
							<tr class="motor-benefit-row allowance-row">
								<td class="motor-benefit-column-title">
									Young Driver Excess
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ <?php echo number_format(($plans["aig"]->aig_plan_young_excess), 0, '.', ','); ?>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ <?php echo number_format(($plans["aw"]->aw_plan_young_excess), 0, '.', ','); ?>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ <?php echo number_format(($plans["pingan"]->pingan_plan_young_excess), 0, '.', ','); ?>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column">
										-
									</td>
								<?php endif; ?>
								
							</tr>
							<tr class="motor-benefit-row allowance-row">
								<td class="motor-benefit-column-title">
									Inexperienced Driver Excess
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ <?php echo number_format(($plans["aig"]->aig_plan_inexperience_excess), 0, '.', ','); ?>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ <?php echo number_format(($plans["aw"]->aw_plan_inexperience_excess), 0, '.', ','); ?>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ <?php echo number_format(($plans["pingan"]->pingan_plan_inexperience_excess), 0, '.', ','); ?>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column">
										-
									</td>
								<?php endif; ?>
								
							</tr>
							<tr class="motor-benefit-row allowance-row">
								<td class="motor-benefit-column-title">
									Parking Damage Excess
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ <?php echo number_format(($plans["aig"]->aig_plan_parking_excess), 0, '.', ','); ?>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ <?php echo number_format(($plans["aw"]->aw_plan_parking_excess), 0, '.', ','); ?>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ <?php echo number_format(($plans["pingan"]->pingan_plan_parking_excess), 0, '.', ','); ?>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column">
										-
									</td>
								<?php endif; ?>
								
							</tr>
							<tr class="motor-benefit-row allowance-row">
								<td class="motor-benefit-column-title">
									Theft Loss Excess
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ <?php echo number_format(($plans["aig"]->aig_plan_theft_loss_excess), 0, '.', ','); ?>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ <?php echo number_format(($plans["aw"]->aw_plan_theft_loss_excess), 0, '.', ','); ?>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ <?php echo number_format(($plans["pingan"]->pingan_plan_theft_loss_excess), 0, '.', ','); ?>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column">
										-
									</td>
								<?php endif; ?>
								
							</tr>
							<tr class="motor-benefit-row allowance-row">
								<td class="motor-benefit-column-title">
									Third Party Property Damage Excess
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ <?php echo number_format(($plans["aig"]->aig_plan_third_party_excess), 0, '.', ','); ?>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ <?php echo number_format(($plans["aw"]->aw_plan_third_party_excess), 0, '.', ','); ?>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ <?php echo number_format(($plans["pingan"]->pingan_plan_third_party_excess), 0, '.', ','); ?>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column">
										-
									</td>
								<?php endif; ?>
								
							</tr>
						</table>
					</div>
				</div>
				<div class="motor-form-input-wrapper">
					<div class="motor-accordion">Third Party Cover</div>
					<div class="panel" style="padding: 0 2px;margin-top: -15px;margin-bottom: 15px;">
						<table class="motor-benefit-table">
							<tr class="motor-benefit-row allowance-row">
								<td class="motor-benefit-column-title">
									Damage to third party property
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ 2,000,000
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ 2,000,000
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ 2,000,000
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column">
										-
									</td>
								<?php endif; ?>
								
							</tr>
							<tr class="motor-benefit-row allowance-row">
								<td class="motor-benefit-column-title">
									Death and/or bodily injury to third party
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ $100,000,000
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ $100,000,000
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ $100,000,000
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column">
										-
									</td>
								<?php endif; ?>
								
							</tr>
						</table>
					</div>
				</div>
				<div class="motor-form-input-wrapper">
					<div class="motor-accordion">Damage to Own Car</div>
					<div class="panel" style="padding: 0 2px;margin-top: -15px;margin-bottom: 15px;">
						<table class="motor-benefit-table">
							<tr class="motor-benefit-row allowance-row">
								<td class="motor-benefit-column-title">
								Own Damage, Fire and Theft Cover
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									Lower of Market Value and Insurer's estimate
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									Lower of Market Value and Insurer's estimate
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									Lower of Market Value and Insurer's estimate
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column">
										-
									</td>
								<?php endif; ?>
								
							</tr>
							<tr class="motor-benefit-row allowance-row">
								<td class="motor-benefit-column-title">
								Windscreen, Window Glass and Roof Glass
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									Up to HK$4,000 per policy year
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									No limit on frequency. HK$5,000 per accident
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									Limited to 1 claim per policy year up to HK$5,000 using designated repairer
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column">
										-
									</td>
								<?php endif; ?>
								
							</tr>
							<tr class="motor-benefit-row allowance-row">
								<td class="motor-benefit-column-title">
								New for Old
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									Yes
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									Yes
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									Yes
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column">
										-
									</td>
								<?php endif; ?>
								
							</tr>
							<tr class="motor-benefit-row allowance-row">
								<td class="motor-benefit-column-title">
								Free Choice of Repairers
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									Yes
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									Yes
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									Yes
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column">
										-
									</td>
								<?php endif; ?>
								
							</tr>
						</table>
					</div>
				</div>
				<div class="motor-form-input-wrapper">
					<div class="motor-accordion">NCD Protection</div>
					<div class="panel" style="padding: 0 2px;margin-top: -15px;margin-bottom: 15px;">
						<table class="motor-benefit-table">
							<tr class="motor-benefit-row allowance-row">
								<td class="motor-benefit-column-title">
								No Claim Discount (NCD) Protection
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									Yes, if total amount claimed within one policy year does not exceed HK$50,000 or 15% of the estimated car value (whichever is less)
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									Yes, if total amount claimed within one policy year does not exceed HK$80,000 or 20% of the estimated car value(whichever is less)
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									Yes, if the claim amount does not exceed HK$60,000 or 15% of the sum insured (whichever is the less); or no third party bodily injury is involved in the claims
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column">
										-
									</td>
								<?php endif; ?>
								
							</tr>
						</table>
					</div>
				</div>
				<div class="motor-form-input-wrapper">
					<div class="motor-accordion">Personal Cover</div>
					<div class="panel" style="padding: 0 2px;margin-top: -15px;margin-bottom: 15px;">
						<table class="motor-benefit-table">
							<tr class="motor-benefit-row allowance-row">
								<td class="motor-benefit-column-title">
								Personal Accident
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									Yes
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ 100,000 cover for Named Drivers
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ 2,000 cover for Named Drivers
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column">
										-
									</td>
								<?php endif; ?>
								
							</tr>
							<tr class="motor-benefit-row allowance-row">
								<td class="motor-benefit-column-title">
									Medical Expenses
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									None
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									Insured Drivers and passengers HK$ 2,000 per accident
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ 2,000 cover for Named Drivers
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column">
										-
									</td>
								<?php endif; ?>
								
							</tr>
						</table>
					</div>
				</div>
				<div class="motor-form-input-wrapper">
					<div class="motor-accordion">Services</div>
					<div class="panel" style="padding: 0 2px;margin-top: -15px;margin-bottom: 15px;">
						<table class="motor-benefit-table">
							<tr class="motor-benefit-row allowance-row">
								<td class="motor-benefit-column-title">
									Replacement Car Service
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									None
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ 5,000 per accident in rental charges (Up to HK$ 1,000 per day)
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									None
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column">
										-
									</td>
								<?php endif; ?>
								
							</tr>
							<tr class="motor-benefit-row allowance-row">
								<td class="motor-benefit-column-title">
									Emergency Towing
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									Yes
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ 2,000 per accident
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									Towing charges covered for 1 accident per policy year
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column">
										-
									</td>
								<?php endif; ?>
								
							</tr>
							<tr class="motor-benefit-row allowance-row">
								<td class="motor-benefit-column-title">
								Emergency Roadside Repairs
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									Yes
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$2,000 per accident
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									None
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column">
										-
									</td>
								<?php endif; ?>
								
							</tr>
						</table>
					</div>
				</div>
				<div class="motor-form-input-wrapper">
					<div class="motor-accordion">Other</div>
					<div class="panel" style="padding: 0 2px;margin-top: -15px;margin-bottom: 15px;">
						<table class="motor-benefit-table">
							<tr class="motor-benefit-row allowance-row">
								<td class="motor-benefit-column-title">
									Other
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									IMPT: Final benefits are confirmed on issuance of cover note
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column">
										-
									</td>
								<?php endif; ?>
								
							</tr>
							<tr class="motor-benefit-row allowance-row">
								<td class="motor-benefit-column-title">
									Brochure & Policy Terms
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									<div>
										<a class="download2" target="_blank" href="media/com_insurediymotor/doc/AIG_Motor.pdf">
											<span>
												<img src="images/download-d1-box.svg">
											</span>  
											Download
										</a>
									</div>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									<div>
										<a class="download2" target="_blank" href="media/com_insurediymotor/doc/Allied_World_Motor.pdf">
											<span>
												<img src="images/download-d1-box.svg">
											</span>  
											Download
										</a>
									</div>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									<div>
										<a class="download2" target="_blank" href="media/com_insurediymotor/doc/PingAn_Motor.pdf">
											<span>
												<img src="images/download-d1-box.svg">
											</span>  
											Download
										</a>
									</div>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column">
										-
									</td>
								<?php endif; ?>
								
							</tr>
						</table>
					</div>
				</div>
			</div>
			<?php endif; ?>
			<?php if($data["coverType"] == "02"): ?>
			<div class="motor-quote-result-bottom-wrapper">
				<div class="motor-form-input-wrapper">
					<div class="motor-accordion">Cover Types & Excess</div>
					<div class="panel" style="padding: 0 2px; margin-top: -15px;margin-bottom: 15px;">
						<table class="motor-benefit-table">
							<tr class="motor-benefit-row cover-type-row">
								<td class="motor-benefit-column-title">
									Cover Type
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									<?php if($data["coverType"] == "02"): ?>
										Third Party
										<?php else: ?>
										Comprehensive
									<?php endif; ?>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									<?php if($data["coverType"] == "02"): ?>
										Third Party
										<?php else: ?>
										Comprehensive
									<?php endif; ?>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									<?php if($data["coverType"] == "02"): ?>
										Third Party
										<?php else: ?>
										Comprehensive
									<?php endif; ?>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
										-
									</td>
								<?php endif; ?>
							</tr>
							<tr class="motor-benefit-row motor-benefit-cover-type-row">
								<td class="motor-benefit-column-title">
									General Excess
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									N/A
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									N/A
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									N/A
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column" id="motor-benefit-key-excess-renewal">
										-
									</td>
								<?php endif; ?>
							</tr>
							<tr class="motor-benefit-row">
								<td class="motor-benefit-column-title">
									Unnamed Driver Excess
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									N/A
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									N/A
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ 7,500
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column">
										-
									</td>
								<?php endif; ?>
							</tr>
							<tr class="motor-benefit-row allowance-row">
								<td class="motor-benefit-column-title">
									Young Driver Excess
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ 10,000
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ 10,000
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ 20,000
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column">
										-
									</td>
								<?php endif; ?>
								
							</tr>
							<tr class="motor-benefit-row allowance-row">
								<td class="motor-benefit-column-title">
									Inexperienced Driver Excess
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ 10,000
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ 10,000
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ 20,000
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column">
										-
									</td>
								<?php endif; ?>
								
							</tr>
							<tr class="motor-benefit-row allowance-row">
								<td class="motor-benefit-column-title">
									Parking Damage Excess
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									N/A
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									N/A
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									N/A
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column">
										-
									</td>
								<?php endif; ?>
								
							</tr>
							<tr class="motor-benefit-row allowance-row">
								<td class="motor-benefit-column-title">
									Theft Loss Excess
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									N/A
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									N/A
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									N/A
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column">
										-
									</td>
								<?php endif; ?>
								
							</tr>
							<tr class="motor-benefit-row allowance-row">
								<td class="motor-benefit-column-title">
									Third Party Property Damage Excess
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ 5,000
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ 5,000
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ 7,000
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column">
										-
									</td>
								<?php endif; ?>
								
							</tr>
						</table>
					</div>
				</div>
				<div class="motor-form-input-wrapper">
					<div class="motor-accordion">Third Party Cover</div>
					<div class="panel" style="padding: 0 2px;margin-top: -15px;margin-bottom: 15px;">
						<table class="motor-benefit-table">
							<tr class="motor-benefit-row allowance-row">
								<td class="motor-benefit-column-title">
									Damage to third party property
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ 2,000,000
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ 2,000,000
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ 2,000,000
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column">
										-
									</td>
								<?php endif; ?>
								
							</tr>
							<tr class="motor-benefit-row allowance-row">
								<td class="motor-benefit-column-title">
									Death and/or bodily injury to third party
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ 100,000,000
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ 100,000,000
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									HK$ 100,000,000
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column">
										-
									</td>
								<?php endif; ?>
								
							</tr>
							<tr class="motor-benefit-row allowance-row">
								<td class="motor-benefit-column-title">
									Brochure & Policy Terms
								</td>
								<?php if(!isset($plans["aig"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000003") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									<div>
										<a class="download2" target="_blank" href="media/com_insurediymotor/doc/AIG_Motor.pdf">
											<span>
												<img src="images/download-d1-box.svg">
											</span>  
											Download
										</a>
									</div>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["aw"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "NCD000001") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									<div>
										<a class="download2" target="_blank" href="media/com_insurediymotor/doc/Allied_World_Motor.pdf">
											<span>
												<img src="images/download-d1-box.svg">
											</span>  
											Download
										</a>
									</div>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($plans["pingan"]) && $plans): ?>
								<?php if($data["currentInsurer"] == "norenew") : ?>
								<td class="motor-benefit-column renewal-column" id="motor-benefit-cover-type-renewal">
									-
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php else: ?>
								<td class="motor-benefit-column">
									<div>
										<a class="download2" target="_blank" href="media/com_insurediymotor/doc/PingAn_Motor.pdf">
											<span>
												<img src="images/download-d1-box.svg">
											</span>  
											Download
										</a>
									</div>
								</td>
								<td class="motor-benefit-divider">&nbsp;</td>
								<?php endif; ?>
								<?php if(!isset($this->renewal)): ?>
								<?php else: ?>
									<td class="motor-benefit-divider renewal-column">&nbsp;</td>
									<td class="motor-benefit-column renewal-column">
										-
									</td>
								<?php endif; ?>
								
							</tr>
						</table>
					</div>
				</div>
			</div>
			<?php endif; ?>
		</div>
		
		<div class="motor-quote-no-result-container" style="display:none;">
			<div class="motor-quote-no-result-header">
				We would like to serve you better!
			</div>
			<div class="motor-quote-no-result-content">
				<span class="motor-quote-no-result-span-1">
					Based on your input, it seems that a personalised quote is required.
				</span>
				<form action="<?php echo JRoute::_('index.php?option=com_insurediymotor'); ?>" method="post" name="contactForm" id="contactForm" novalidate class="form-validate form-vertical">
					<input type="hidden" name="quotationId" value="<?php echo $data["quotationId"]; ?>" />
					<button type="submit" class="contact-special-button" ><?php echo JText::_('Email Me My Quote!') ?></button>
				</form>
			</div>
		</div>
		
		<div class="motor-quote-no-result-status" style="display:none;">
			<div class="motor-quote-no-result-header">
				Thanks for staying with InsureDIY
			</div>
			<div class="motor-quote-no-result-content">
				<span class="motor-quote-no-result-span-1">
					Our representative will be in contact with you within 48 hours.
				</span>
				<a href="<?php echo JRoute::_('index.php?option=com_insurediymotor&view=motor&Itemid=159&step=1'); ?>" class="contact-special-button" >Go Back</a>
			</div>
		</div>
		
		<!-- <form action="<?php echo JRoute::_('index.php?option=com_insurediymotor'); ?>" method="post" name="adminForm" id="adminForm" novalidate class="form-validate form-vertical">
			<div id="fields-holder">
				
			</div>
			<div class="clear"></div>
			<input type="hidden" name="task" value="form.step3save" />

			<?php //echo JHtml::_('form.token'); ?>
			<div style="float:left;margin:15px 0;">
				<div>{modulepos insurediy-secured-ssl}</div>
			</div>
			<!--<div class="btn-toolbar" style="float:right">
				<div>
					<button style="margin-right:10px;" type="button" onclick="javascript:document.adminForm.task.value = 'form.back';
							document.adminForm.submit();" class="btn btn-primary validate"><?php echo JText::_('BTN_BACK') ?></button>
					<button type="submit" class="btn btn-primary validate" ><?php echo JText::_('JCONTINUE') ?></button>
				</div>
			</div>
			<div class="clear"></div>
		</form>-->
		 
	</div>
</div>
<div class="logo-slider"><?php echo MyHelper::load_module(102); ?></div>

<div id="dialog-alert" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridSystemModalLabel">
  <div class="modal-dialog" role="document">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">Message</h4>
      </div>
      <div class="modal-body" style="color:#222;margin-top: 0;margin-bottom: 20px;">
        
      </div>
  <!--    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div> -->
  </div>
</div>

<div id="dialog-alert-login" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="loginpopup" style="width: 350px;margin-left: -174px;top: 15%;padding: 20px;z-index:-1;">
  <div class="modal-dialog" role="document">
      <div class="modal-header" style="padding-bottom:0;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="loginpopup">Login and enjoy up to HK$200 off</h4>
      </div>
      <div class="modal-body" style="color:#222;margin-top: 0;overflow: hidden;">
	  	<div class="tab-header-container" style="width:auto;height:auto;margin:0;">
			<div class="tab-pane active" onclick="changeTab(this);" id="login-0" data-id="login-panel" data-title="Login and enjoy up to HK$200 off">
				<span class="tab-pane-title" style="font-size:12px;">LOGIN</span>
			</div>
			<div class="tab-pane" onclick="changeTab(this);" id="register-1" data-id="register-panel" data-title="Register and enjoy up to HK$200 off"> 
				<span class="tab-pane-title" style="font-size:12px;">REGISTER</span>
			</div>
		</div>
		<div class="tab-content-container" style="padding:0;margin-top: 20px;">
			<div class="tab-wrapper" id="login-panel">
				<form id="login-form-popup">
				<div class="insurediy-form" style="background-color: transparent;border: none;margin: 0;">
					<div class="input-wrapper" style="padding:0;">
						<span class="input-title" style="font-size:12px;">Email</span>
						<div class="input-container-block">
							<input type="text" class="input-text" style="width:100%;" name="username" />
						</div>
					</div>
					<div class="input-wrapper" style="padding:0;">
						<span class="input-title" style="font-size:12px;">Password</span>
						<div class="input-container-block">
							<input type="password" class="input-text" style="width:100%;" name="password" value="" />
							<input type="hidden" name="option" value="com_insurediymotor" />
							<input type="hidden" name="task" value="user.ajaxLogin" />
							<input type="hidden" name="return" value="" />
							<?php echo JHtml::_('form.token'); ?>
						</div>
					</div>
				</div>
				<div class="motor-btn-wrapper" style="width:auto;padding-bottom: 0;">
					<div class="motor-btn-group">
						<button type="submit" id="submit_button" class="motor-btn validate" style="padding: 10px 20px;">LOGIN</button>
					</div>
				</div>
				</form>
			</div>
			<div class="tab-wrapper display-none" id="register-panel">
				<div class="insurediy-form" style="background-color: transparent;border: none;margin: 0;">
					<form id="register-form-popup">
					<div class="input-wrapper" style="padding:0;">
						<span class="input-title" style="font-size:12px;">Email</span>
						<div class="input-container-block">
							<input type="text" class="input-text" style="width:100%;" name="email" />
						</div>
					</div>
					<div class="input-wrapper" style="padding:0;">
						<span class="input-title" style="font-size:12px;">Password</span>
						<div class="input-container-block">
							<input type="password" class="input-text" style="width:100%;" name="password" value="" />
						</div>
					</div>
					<div class="input-wrapper" style="padding:0;">
						<span class="input-title" style="font-size:12px;">Confirm Password</span>
						<div class="input-container-block">
							<input type="password" class="input-text" style="width:100%;" name="password2" value="" />
							<input type="hidden" name="option" value="com_insurediymotor" />
							<input type="hidden" name="task" value="user.ajaxRegister" />
							<input type="hidden" name="return" value="" />
							<?php echo JHtml::_('form.token'); ?>
						</div>
					</div>
					<div class="motor-btn-wrapper" style="width:auto;padding-bottom: 0;">
						<div class="motor-btn-group">
							<button type="submit" id="submit_button" class="motor-btn validate" style="padding: 10px 20px;">REGISTER</button>
						</div>
					</div>
					<div class="red-text no-continue" style="cursor:pointer;text-align: center;text-decoration: underline;color: #FB5A6E;">No thanks, I will pay full amount.</div>
					</form>
				</div>
			</div>
		</div>	
      </div>
  </div>
</div>

<!-- OLD CAR MODAL -->
<div id="oldCarConfirm" class="old-car-modal">
	<div class="modal-content">
		<div class="modal-header">
			Cover Type
		</div>
		<div class="modal-body">
			<div class="modal-content-h1">
				You have selected a plan with Third Party, Fire and Theft Coverage only.
			</div>
			<div class="modal-content-h2">
				This means that you are covered for any third party damages in the event of an accident that you are deemed liable for, and also includes insurance for your own car if it's stolen or damaged by fire or if it is damages in an attempted theft. However, your own car damages in the event of an accident you are deemed liable for will not be covered. 
				<br><br>
				Click <a target="_blank" href="<?php echo JURI::base()."images/motor/hla-benefit-illustration-tpft.pdf"; ?>">here</a> for more details.
				<br><br>
				Please confirm that you wish to proceed. 
			</div>
		</div>
		<div class="modal-new-footer">
			<div class="modal-button" data-dismiss="modal" onclick="proceedForm(false);">
				CANCEL
			</div>
			<div class="modal-button" data-dismiss="modal" onclick="proceedForm(true);">
				PROCEED
			</div>
		</div>
	</div>
</div>
<!-- END OLD CAR MODAL -->
<script>
	function changeTab(obj) {
		jQuery(".tab-pane").removeClass("active");
		jQuery(obj).addClass("active");
		jQuery('.tab-wrapper').addClass('display-none');
		jQuery("#"+jQuery(obj).attr("data-id")).removeClass("display-none");
		jQuery("#loginpopup").html(jQuery(obj).attr("data-title"));
	}
	jQuery('.loginpopup').on('click', function(e){
		e.preventDefault();
		jQuery('#login-0').click();
		jQuery('#loginpopup').html("Login and enjoy up to HK$200 off");
		jQuery('#dialog-alert-login').css("z-index", "1050");
		jQuery('#dialog-alert-login').modal('show');
	});
	jQuery('.regpopup').on('click', function(e){
		e.preventDefault();
		jQuery('#register-1').click();
		jQuery('#loginpopup').html("Register and enjoy up to HK$200 off");
		jQuery('#dialog-alert-login').css("z-index", "1050");
		jQuery('#dialog-alert-login').modal('show');
	});
	jQuery('.panel').css("max-height", "1000px");
	var numItems = jQuery('.motor-result-column').length;
	if (numItems == 4) {
		jQuery('.motor-result-column .motor-quote-premium').css("font-size", "15px");
	} else if (numItems == 3) {
		jQuery('.motor-result-column .motor-quote-premium').css("font-size", "18px");
	} else {
		jQuery('.motor-result-column .motor-quote-premium').css("font-size", "22px");
	}

	jQuery(".no-continue").on("click", function() {
		jQuery('#dialog-alert-login').modal('hide');
	});

	jQuery("#login-form-popup").submit(function(e) {
		e.preventDefault();    
		var formData = new FormData(jQuery(this)[0]);
		jQuery(".input-text").LoadingOverlay("show");
		jQuery.ajax({
			type: "POST",
			url: "<?php echo JRoute::_('index.php', true); ?>",
			data: formData,
			dataType: "json",
			contentType: false,
			processData: false,
			success: function(result){
				if (result["success"] == true) {
					location.reload();
				} else {
					alert(result["message"]);
					jQuery(".input-text").LoadingOverlay("hide");
				}
			},
			error: function(xhr, textStatus, errorThrown){
				console.log(errorThrown);
				jQuery(".input-text").LoadingOverlay("hide");
			}
		});
	});

	jQuery("#register-form-popup").submit(function(e) {
		e.preventDefault();    
		var formData = new FormData(jQuery(this)[0]);
		jQuery(".input-text").LoadingOverlay("show");
		jQuery.ajax({
			type: "POST",
			url: "<?php echo JRoute::_('index.php', true); ?>",
			data: formData,
			dataType: "json",
			contentType: false,
			processData: false,
			success: function(result){
				if (result["success"] == true) {
					location.reload();
				} else {
					alert(result["message"]);
					jQuery(".input-text").LoadingOverlay("hide");
				}
			},
			error: function(xhr, textStatus, errorThrown){
				console.log(errorThrown);
				jQuery(".input-text").LoadingOverlay("hide");
			}
		});
	});
</script>

<script>
	var dataFetched = false;
	var allPlanList = new Array();
	var variantSelected = "";
	var acc = jQuery(".motor-accordion");
	var i;
	
	jQuery('.motor-quote-result-container').css("display", "block");


	//jQuery(window).on("load", function() {
		for (i = 0; i < acc.length; i++) {
			acc[i].nextElementSibling.style.maxHeight = (acc[i].nextElementSibling.scrollHeight + 30) + "px";
		  	acc[i].onclick = function() {
		    this.classList.toggle("active");
		    var panel = this.nextElementSibling;
		    if (panel.style.maxHeight){
		      panel.style.maxHeight = null;
		    } else {
		      panel.style.maxHeight = (panel.scrollHeight + 30) + "px";
		    } 
		  }
		}	
	//});

	<?php if(!$plans || empty($plans)){ echo "jQuery('.motor-quote-result-container').css(\"display\", \"none\");"; } ?>
	
	jQuery.LoadingOverlaySetup({
	    color           : "rgba(0, 0, 0, 0.4)"
	});

	function modalSlide() {
		count = 0;
		var trigger = setInterval(function () {
		
			count++;
			if(count == 1) {
				jQuery("img.overlay-content-img").attr("src", "<?php echo JURI::base(); ?>/images/websiteicon/aig-logo.png");
					jQuery("span.overlay-content-title").html("Retrieving Quotes from ");
					jQuery("span.overlay-content-brand").html("AIG . . .");
			}

			if(count == 2) {
				jQuery("img.overlay-content-img").attr("src", "<?php echo JURI::base(); ?>/images/websiteicon/allied-world-logo.png");
					jQuery("span.overlay-content-title").html("Retrieving Quotes from ");
					jQuery("span.overlay-content-brand").html("Allied World . . .");
			}

			if(count == 3) {
				jQuery("img.overlay-content-img").attr("src", "<?php echo JURI::base(); ?>/images/websiteicon/pingan-logo.png");
					jQuery("span.overlay-content-title").html("Retrieving Quotes from ");
					jQuery("span.overlay-content-brand").html("Pingan . . .");
			}

			if(count == 4) {
				jQuery("img.overlay-content-img").attr("src", "<?php echo JURI::base(); ?>/images/websiteicon/company-logo.png");
				jQuery("span.overlay-content-title").html("Did You know?<br>");
				jQuery("span.overlay-content-brand").html("You can renew with your current <br> insurer with us & get discounts?");
			}
			
			if(count == 5) {
				jQuery("img.overlay-content-img").attr("src", "<?php echo JURI::base(); ?>/images/websiteicon/company-logo.png");
				jQuery("span.overlay-content-title").html("Please be patient . . .<br>");
				jQuery("span.overlay-content-brand").html("We are almost there!");
			}
			
			if(count > 5) {
				clearInterval(trigger);
			}
		}, 5000);
	}

	var customElement = jQuery('\
			<div class="overlay-container">\
			<div class="overlay-content-top">\
				<img class="overlay-content-img" src="<?php echo JURI::base(); ?>/images/websiteicon/company-logo.png"/>\
			</div>\
			<div class="overlay-content-middle">\
				<div class="overlay-content-header">\
					<span class="overlay-content-title">We are gathering quotations for you <br></span>\
					<span class="overlay-content-brand">Please wait a moment . . .</span>\
				</div>\
			</div>\
			<div class="overlay-content-bottom">\
				<div class="spinner">\
				  <div></div>\
				  <div></div>\
				  <div></div>\
				  <div></div>\
				  <div></div>\
				</div>\
			</div>\
		</div>\
	');
	
	jQuery(".motor-quote-edit-button").on('click', function(){
		jQuery(".motor-quote-container, .motor-quote-result-container").fadeToggle( "fast", "linear" );
		jQuery("#validateCaptcha").val("Click Here to Get Quote");
	});

	var workshop = "";
	var ncdPro = "";
	var los = "";

	function getVariantString() {
		var aw = jQuery("select[name='variant-aw']").val();
		var aig = jQuery("select[name='variant-aig']").val();
		var pingan = jQuery("select[name='variant-pingan']").val();
		var result = "";
		

		if(aw == undefined || aw == null || aw == "") {
			result += "-";
		}
		else {
			result += aw;
		}

		if(aig == undefined || aig == null || aig == "") {
			result += "-";
		}
		else {
			result += aig;
		}

		if(pingan == undefined || pingan == null || pingan == "") {
			result += "-";
		}
		else {
			result += pingan;
		}
		
		return result;
	}

	var preChanged = <?php if($plans) {echo '"'.$data["preChange"].'"';} else {echo "true";} ?>;
	var variantSelected = getVariantString();

	jQuery("form#contactForm").on('submit', function(obj){
		var userInput = jQuery('form#contactForm').serializeArray();
		
		jQuery.ajax({
		    type: "POST",
		    url: "<?php echo JURI::root().'index.php?option=com_insurediymotor&task=ajax.specialContact&format=json'; ?>",
		    data: userInput,
		    dataType: "json",
			success: function(result){
				//console.log(result);
		        //var json = jQuery.parseJSON(result);
				jQuery(".motor-quote-no-result-container, .motor-quote-no-result-status").fadeToggle( "fast", "linear" );
			},
			error: function(xhr, textStatus, errorThrown){
		    	//alert('request failed : '+errorThrown);
				//console.log(errorThrown);
			}
		});

		obj.preventDefault();
	    return false;
	});

	jQuery("#validateCaptcha").on('click', function(obj){
		var currVariant = getVariantString();
		
		if(currVariant == "---") {
			jQuery("#dialog-alert .modal-body").html("Please select car variant to see quote from respective insurer.");
			jQuery('#dialog-alert').modal('show');

			return false;
		}

		var captcha = jQuery("input[name='captcha']").val();
		jQuery("#validateCaptcha").val("Validating...");
		jQuery.ajax({
		    type: "POST",
		    url: "<?php echo JURI::root().'index.php?option=com_insurediymotor&task=ajax.validateCaptcha&format=json'; ?>",
		    data: "captcha="+captcha,
		    dataType: "json",
			success: function(result){
				if (result["result"] == "true") {
					jQuery(".wrong-captcha").css("visibility", "hidden");
					jQuery("#validateCaptcha").val("Retreiving Quotes...");
					jQuery(".post-captcha").click();
				} else {
					jQuery("#validateCaptcha").val("Click Here to Get Quote");
					jQuery(".wrong-captcha").addClass("warning-text");
					jQuery(".wrong-captcha").html("Please enter the correct captcha (Case Sensitive)");
					jQuery(".wrong-captcha").css("visibility", "visible");
				}
			},
			error: function(xhr, textStatus, errorThrown){
		    	//alert('request failed : '+errorThrown);
				//console.log(errorThrown);
			}
		});

		obj.preventDefault();
	    return false;
	});


	jQuery( document ).ready(function() {
		jQuery.ajax({
		    type: "POST",
		    url: "<?php echo JURI::root().'index.php?option=com_insurediymotor&task=ajax.getCaptcha&format=json'; ?>",
		    data: "",
		    dataType: "",
			success: function(result){
				jQuery("img.captcha-img").attr("src", 'data:image/png;base64,' + result["captcha"]);
				jQuery("img.captcha-img, input[name='captcha']").fadeIn("slow");
			},
			error: function(xhr, textStatus, errorThrown){
				jQuery(".wrong-captcha").html("Unable to load captcha, please refresh the page!");
				jQuery(".wrong-captcha").css("visibility", "visible");
		    	//alert('request failed : '+errorThrown);
				//console.log(errorThrown);
			}
		});
	});
	
	jQuery("form#getQuoteForm").on('submit', function(obj){
		var currVariant = getVariantString();
		var userInput = jQuery('form#getQuoteForm').serializeArray();
		//var workshop2 = jQuery('select[name=workshop]').val();
		//var los2 = jQuery('select[name=los]').val();
		//var ncdPro2 = jQuery('select[name=ncdPro]').val();
		
		if(currVariant == "---") {
			jQuery("#dialog-alert .modal-body").html("Please select car model from at least one insurer.");
			jQuery('#dialog-alert').modal('show');

			return false;
		}

		if(currVariant == "---") {
			jQuery("#dialog-alert .modal-body").html("Please select car model from at least one insurer.");
			jQuery('#dialog-alert').modal('show');

			return false;
		}
		
		if(variantSelected != currVariant || preChanged == true) {
			jQuery.ajax({
			    type: "POST",
			    url: "<?php echo JURI::root().'index.php?option=com_insurediymotor&task=ajax.getQuote&format=json'; ?>",
			    data: userInput,
			    dataType: "json",
			    beforeSend: function() {
			       	jQuery.LoadingOverlay("show", {
			            image   : "",
			            custom  : customElement
			        });

			       	modalSlide();
				},
				success: function(result){
			       	variantSelected = currVariant;
					var json = result;
						
					if(selectPlan(json)){

						preChanged = false;
					}
					if(result["aig"]["success"] == false && result["aw"]["success"] == false && result["pingan"]["success"] == false){
						jQuery.LoadingOverlay("hide");
						jQuery(".motor-quote-container, .motor-quote-no-result-container").fadeToggle( "fast", "linear" );
					}

					//jQuery.LoadingOverlay("hide");
					//jQuery("#validateCaptcha").val("Click Here to Get Quote");
				},
				error: function(xhr, textStatus, errorThrown){
			    	//alert('request failed : '+errorThrown);
					//console.log(errorThrown);
			    	jQuery.LoadingOverlay("hide");
					jQuery("#validateCaptcha").val("Click Here To Get Quote");
				}
			});
		}
		else {
			jQuery(".motor-quote-container, .motor-quote-result-container").fadeToggle( "fast", "linear" );
		}
		
		obj.preventDefault();
	    return false;
	});

	jQuery("select[name='workshop'], select[name='los'], select[name='ncdPro']").on("change", function(obj){
		if(typeof allPlanList.success !== 'undefined') {
			
		}
	});

	function selectPlan(insurerList) {
		if(!insurerList.success) {
			//console.log(insurerList);
			//alert('Request failed : '+ insurerList.error);
			console.log('1');
			return false;
		}
		
		var aig = getAigPlan(insurerList.aig);
		var aw = getAwPlan(insurerList.aw);
		var pingan = getPinganPlan(insurerList.pingan);

		var errorMsg = "";
		

		if(!insurerList.aig.success) {
			if(insurerList.aig.currentInsurer === undefined) {
				errorMsg += "<br>";
				errorMsg += "AIG - " + insurerList.aig["error_message"];
			}
		}

		if(!insurerList.aw.success) {
			if(insurerList.aw.currentInsurer === undefined) {
				errorMsg += "<br>";
				errorMsg += "AW - " + insurerList.aw["error_message"];
			}
		}

		if(!insurerList.pingan.success) {
			if(insurerList.pingan.currentInsurer === undefined) {
				errorMsg += "<br>";
				errorMsg += "PINGAN - " + insurerList.pingan["error_message"];
			}
		}



		if(!aig && !pingan && !aw) {
			console.log('2');
			return false;
		}
		
		var renewal = false;

		if(insurerList.currentInsurer !== undefined) {
			renewal = insurerList.currentInsurer;
		}

		if(updateLayout(aig, aw, pingan, renewal)) {
			return true;
			showLayout();

			
		}
		else {
			console.log('3');
			return false;
		}
	}


	function getAigPlan(data) {
		if(!data.success) {
			if(data.currentInsurer === undefined) {
				//console.log('Request failed : '+ data["error_message"]);
				return false;
			}
			else {
				return data;
			}
		}

		var userInput = jQuery('form#getQuoteForm').serializeArray();
	
		var result = new Array();
		
		result["partner_id"] = "aig";
		result["id"] = data.id;
		result["plan_code"] = "AIG";
		result["plan_name"] = data.plan_list.planName;
		result["premium_total"] = parseFloat(data.plan_list.finalPremium).toFixed(2).toLocaleString().toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		result["premium_before_discount"] = (data.plan_list.finalPremium).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		result["brand_logo"] = "<?php echo JURI::base(); ?>/images/websiteicon/aig-logo.png";
		result["brand_positioning"] = "";
		
		return result;
	}

	function getAwPlan(data) {
		if(!data.success) {
			if(data.currentInsurer === undefined) {
				return false;
			}
			else {
				return data;
			}
		}

		var userInput = jQuery('form#getQuoteForm').serializeArray();
	
		var result = new Array();
		
		result["partner_id"] = "aw";
		result["id"] = data.id;
		result["plan_code"] = "AW";
		result["plan_name"] = data.plan_list.planName;
		result["premium_total"] = parseFloat(data.plan_list.finalPremium).toFixed(2).toLocaleString().toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		result["premium_before_discount"] = (data.plan_list.finalPremium).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		result["brand_logo"] = "<?php echo JURI::base(); ?>/images/websiteicon/allied-world-logo.png";
		result["brand_positioning"] = "";
		
		return result;
	}

	function getPinganPlan(data) {
		if(!data.success) {
			if(data.currentInsurer === undefined) {
				return false;
			}
			else {
				return data;
			}
		}

		var userInput = jQuery('form#getQuoteForm').serializeArray();
	
		var result = new Array();
		
		result["partner_id"] = "pingan";
		result["id"] = data.id;
		result["plan_code"] = "PINGAN";
		result["plan_name"] = data.plan_list.planName;
		result["premium_total"] = parseFloat(data.plan_list.finalPremium).toFixed(2).toLocaleString().toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		result["premium_before_discount"] = (data.plan_list.finalPremium).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		result["brand_logo"] = "<?php echo JURI::base(); ?>/images/websiteicon/pingan-logo.png";
		result["brand_positioning"] = "";
		
		return result;
	}


	function updateLayout(aig, aw, pingan, renewal) {
		location.reload();
	}

	function showLayout() {
		//jQuery(".motor-accordion").eq(0).next().css("max-height","201px");
		
		jQuery(".motor-quote-container, .motor-quote-result-container").fadeToggle( "fast", "linear" );
		
		var acc = document.getElementsByClassName("motor-accordion");
		var i;
		
		for (i = 0; i < acc.length; i++) {			
			acc[i].nextElementSibling.style.maxHeight = (acc[i].nextElementSibling.scrollHeight + 200) + "px";
		  	acc[i].onclick = function() {
		    this.classList.toggle("active");
		    var panel = this.nextElementSibling;
		    if (panel.style.maxHeight){
		      panel.style.maxHeight = null;
		    } else {
		      panel.style.maxHeight = (panel.scrollHeight + 200) + "px";
		    } 
		  }
		}
	}


	function format2(n, currency) {
		return currency + parseFloat(n).toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
	}

	function confirmAlert() {		
	}

	function proceedForm(flag) {
		if(flag) {
			jQuery("#hla-form").submit();
		}
		else {
			// jQuery(".old-car-modal").css("display", "none");
		}
	}


	jQuery(document).ready(function() {
		jQuery('.motor-quote-model-variant').select2({width: '201px'});
		jQuery(".motor-quote-model-variant").on('change', function () {
			var selectId = "#" + jQuery(this).attr('id');
			var nextExpand = selectId.substr(selectId.length - 1);
			var nextExpandId = parseInt(nextExpand) + 1;
			var nextExpandFull = "#insurer-model-select-" + nextExpandId;
			if (jQuery(this).val() != "") {
				jQuery(nextExpandFull).select2("open");
			}
			
		});
		
	});
</script>




