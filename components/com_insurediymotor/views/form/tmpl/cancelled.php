<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_insurediymotor
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', '.insurediy-form-content select');

//JHtml::_('behavior.modal', 'a.modal_jform_contenthistory');
// Create shortcut to parameters.
$params = $this->state->get('params');


?>

<script type="text/javascript">
	window.addEvent('domready', function() {
		var myAccordion = new Fx.Accordion($$('.togglers'), $$('.step2-checkbox'), {display: -1, alwaysHide: true});
	});

	function clickFileType(div_id) {
		$(div_id).click();
	}

	function copyPasteText(div_id, text) {
		$(div_id).setProperty('value', text);
	}
</script>

<div class="insurediy-form">
	<div class="header-top-wrapper">
		<?php echo InsureDIYMotorHelper::renderHeader('icon-travel', JText::_('PAYMENT_CANCEL_HEADING'), 4); ?>
	</div>
	<div style="padding:30px;margin-top: 100px;">
		<?php echo MyHelper::renderDefaultMessage(); ?>
		<div class="edit<?php echo $this->pageclass_sfx; ?>">
			<div class="form-quote-box" style="width: 670px">
				<span style="color:#2f2f2f;font-size:37px;line-height: 40px;">
					<?php echo JText::_("COM_INSUREDIYMOTOR_PAGE_SUB_HEADING_PAYMENT_CANCEL"); ?>
				</span>
				<div class="center" style="color:#2f2f2f;font-size:14px;margin-top: 15px;">
					To continue the quotation, please click <a href="<?php echo JRoute::_('index.php?option=com_insurediymotor&view=motor&Itemid=159&step=5'); ?>">here</a> to proceed to payment. 
					<br>
					Thank you.	
				</div>
			</div>
		</div>
	</div>
</div>
<div class="logo-slider"><?php echo MyHelper::load_module(102); ?></div>
