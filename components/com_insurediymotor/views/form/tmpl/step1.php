<?php
	defined('_JEXEC') or die;
	
	JHtml::_('behavior.keepalive');
	//JHtml::_('behavior.formvalidation');
	JHtml::_('formbehavior.chosen', '.insurediy-form-content select');
	JHtml::_('script', 'system/jquery.validate.js', false, true);
	JHtml::_('script', 'system/loadingoverlay.min.js', false, true);
	JHtml::_('script', 'system/select2.min.js', false, true);
	JHtml::_('stylesheet', 'system/select2.css', false, true);
	
	$params = $this->state->get('params');
	$itemid = JRequest::getVar("itemid", FALSE);
	$form = $this->form;
	$data = $this->item;

	//JFactory::getApplication()->enqueueMessage(JText::_('SOME_CHECKING_OCCURRED'), 'error');
	
	$occupationListing = "";
	$occupationParentListing = "";
	
	foreach($this->occupation as $occupation) {
		$occupationListing .= "<option value=\"" . $occupation->occ_code . "\">" . ucwords(strtolower($occupation->occ_name)) . "</option>";
	}

	foreach($this->occupationParent as $occupationParent) {
		$occupationParentListing .= "<option value=\"" . $occupationParent->id . "\">" . ucwords(strtolower($occupationParent->occ_name)) . "</option>";
	}
	
	if(isset($data["carRegisterYear"])) {
		$regYear = date($data["carRegisterYear"]);
		$now = date("Y");
		
		$yearDiff = $now - $regYear;
	}
?>

<script type="text/javascript">
	jQuery(document).ready(function () {
		jQuery("#adminForm").validate({
			ignore: ".select-required", // <-- option so that hidden elements are validated
			//			rules: {
			//				jform_job_nature: {// <-- name of actual text input
			//					required: true,
			//					maxlength: 100
			//				}
			//			}
		});
		
		var startDate = jQuery("#jform_start_date");
		var endDate = jQuery("#jform_end_date");
		
		startDate.on("change", function (e) {
			var jThis = jQuery(this);
			
			var startfrom = jThis.val().split("-");
			
			var oneDay = 24*60*60*1000; // hours*minutes*seconds*milliseconds
			var firstDate = new Date();
			var secondDate = new Date(startfrom[2],startfrom[1] - 1, startfrom[0]);
			var diffDays = Math.round(Math.abs((firstDate.getTime() - secondDate.getTime())/(oneDay)));
			
			
			endDate.datepicker("option", "minDate", new Date(startfrom[2], startfrom[1] - 1, startfrom[0]));
			endDate.datepicker("option", "maxDate", " +"+(diffDays+182)+"D ");
			endDate.datepicker("refresh");
		});
	});

	var dateToday = new Date();
</script>

<!-- Google Tag Manager Data Layer-->
<script>
	/**
		* Call this function when a user clicks on a product link. This function uses the
		event
		* callback datalayer variable to handle navigation after the ecommerce data has
		been sent
		* to Google Analytics.
		* @param {Object} productObj An object representing a
		product.
		*/
		function pushProductClick(productObj) {
			dataLayer.push({
				'event': 'productClick',
				'ecommerce': {
					'click': {
						'actionField': {'list': productObj.list}, // Optional list property.
						'products': [{
							'name': productObj.name,
							// Name or ID is required.
							'id': productObj.id
						}]
					}
				},
				'eventCallback': function() {
				}
			});
		}
	</script>
	<!-- End of Google Tag Manager Data Layers-->
	
	<div class="insurediy-form montserat-font" style="background-color:#fff;">
		<div class="motor-header-top-wrapper">
			<?php echo InsureDIYMotorHelper::renderHeader('icon-travel', JText::_('COM_INSUREDIYMOTOR_PAGE_HEADING'), 1); ?>
		</div>
		<div class="edit<?php echo $this->pageclass_sfx; ?> insurediy-motor-form-content">
			<form action="<?php echo JRoute::_('index.php?option=com_insurediymotor&view=form'); ?>" method="post" name="adminForm" id="adminForm" class="form-vertical" enctype="multipart/form-data">
				<div class="motor-form-input-wrapper acc-1">
					<div class="motor-accordion car-accordion">CAR DETAILS</div>
					<div class="panel show">
						<div class="panel-left">
							<!-- <div class="input-wrapper">
								<span class="input-title">Off-Peak Car?</span>
								<?php if(isset($data["offPeakCar"])):?>
									<div class="input-container-inline">
										<input type="radio" id="radio1" class="input-radio" name="jform[offPeakCar]" value="true" <?php if($data["offPeakCar"] == "true"){ echo "checked"; }?> onclick="radioClicked(this);"/>
										<label class="radio-label" for="radio1">
											<span class="radio-label-title <?php if($data["offPeakCar"] == "true"){ echo "active"; }?>">Yes</span>
										</label>
									</div>
									<div class="input-container-inline">
										<input type="radio" id="radio2" class="input-radio" name="jform[offPeakCar]" value="false" <?php if($data["offPeakCar"] == "false"){ echo "checked"; }?> onclick="radioClicked(this);" required/>
										<label class="radio-label" for="radio2">
											<span class="radio-label-title <?php if($data["offPeakCar"] == "false"){ echo "active"; }?>">No</span>
										</label>
									</div>
								<?php  else: ?>
									<div class="input-container-inline">
										<input type="radio" id="radio1" class="input-radio" name="jform[offPeakCar]" value="true" onclick="radioClicked(this);"/>
										<label class="radio-label" for="radio1">
											<span class="radio-label-title">Yes</span>
										</label>
									</div>
									<div class="input-container-inline">
										<input type="radio" id="radio2" class="input-radio" name="jform[offPeakCar]" value="false" onclick="radioClicked(this);" required/>
										<label class="radio-label" for="radio2">
											<span class="radio-label-title">No</span>
										</label>
									</div>
								<?php endif; ?>
							</div> -->
							<div class="input-wrapper">
								<span class="input-title">What is your vehicle make?</span>
								<div class="input-container-block">
									<div class="select_mate" data-mate-select="active" >
										<select name="jform[carMake]" onchange="" onclick="return false;"  class="car-detail hide-select custom-select">
											<option value="" selected>Select Car Make</option>
											<?php if(isset($this->makeModel["make"])):?>
												<?php foreach($this->makeModel["make"] as $item):?>
														<option value="<?php echo $item->make_code; ?>" <?php if(isset($data["carMake"])) if($data["carMake"] == $item->make_code) echo 'selected="selected"'; ?>><?php echo ucwords($item->make_name); ?></option>
												<?php endforeach;?>
											<?php endif; ?>
										</select>
									</div>
								</div>
							</div>
							<div class="input-wrapper">
								<span class="input-title">What is your vehicle model?</span>
								<div class="input-container-block">
									<div class="select_mate car-model" data-mate-select="active" >
										<select name="jform[carModel]" onchange="" onclick="return false;"  class="car-detail hide-select custom-select">
											<option value="" selected>Select Car Model</option>
											<?php if(isset($this->makeModel["model"])):?>
												<?php foreach($this->makeModel["model"] as $item):?>
													<?php if($data["carMake"] == $item->make_code):?>
														<option value="<?php echo $item->model_code; ?>" <?php if($data["carModel"] == $item->model_code) echo 'selected="selected"'; ?>><?php echo ucfirst($item->model_name); ?></option>
													<?php endif; ?>
												<?php endforeach;?>
											<?php endif; ?>
										</select>
									</div>
								</div>
							</div>
							<div class="input-wrapper cc-input">
								<div class="cc-pointer">
									<img src="images/region.gif" id="region" style="top: -185px !important;">
									<img src="images/region-mark.gif" id="region-mark" style="top: 47px !important;">
									<div id="dir"></div>
								</div>
								<span class="input-title">What is your engine CC?</span>
								<div class="input-container-block">
									<input type="text" class="input-text front car-detail" name="jform[vehicleEngineCc]" value="<?php if(isset($data["vehicleEngineCc"])) echo $data["vehicleEngineCc"]; ?>" />
								</div>
							</div>
							<div class="input-wrapper">
								<span class="input-title">What is your vehicle body type?</span>
								<div class="input-container-block">
									<div class="select_mate" data-mate-select="active" >
										<select name="jform[carBodyType]" onchange="" onclick="return false;"  class="car-detail hide-select custom-select">
											<option value="" selected>Select Body Type</option>
											<option value="1" <?php if($data["carBodyType"] == "1") echo 'selected="selected"'; ?>>CONVERTIBLE</option>
											<option value="2" <?php if($data["carBodyType"] == "2") echo 'selected="selected"'; ?>>COUPE</option>
											<option value="3" <?php if($data["carBodyType"] == "3") echo 'selected="selected"'; ?>>HATCHBACK</option>
											<option value="5" <?php if($data["carBodyType"] == "5") echo 'selected="selected"'; ?>>SALOON</option>
											<option value="6" <?php if($data["carBodyType"] == "6") echo 'selected="selected"'; ?>>SPORTS</option>
											<option value="7" <?php if($data["carBodyType"] == "7") echo 'selected="selected"'; ?>>STATION WAGGON</option>
											<option value="9" <?php if($data["carBodyType"] == "9") echo 'selected="selected"'; ?>>STATION WAGGON (WA)</option>
											<option value="8" <?php if($data["carBodyType"] == "8") echo 'selected="selected"'; ?>>STATION WAGGON (HVY)</option>
										</select>
									</div>
								</div>
							</div>
							<!-- <div class="input-wrapper">
								<span class="input-title">Night Parking District</span>
								<div class="input-container-block">
									<div class="select_mate" data-mate-select="active" >
										<select name="jform[nightParking]" onchange="" onclick="return false;"  class="hide-select custom-select">
											<option value="" selected>Select</option>
											<option value="HK" <?php if(isset($data["nightParking"]) && $data["nightParking"] == "HK") echo "selected"; ?>>Hong Kong</option>
											<option value="KLN" <?php if(isset($data["nightParking"]) && $data["nightParking"] == "KLN") echo "selected"; ?>>Kowloon</option>
											<option value="NT" <?php if(isset($data["nightParking"]) && $data["nightParking"] == "NT") echo "selected"; ?>>New Territories</option>
										</select>
									</div>
								</div>
							</div> -->
							
							<div style="width:100%;height:50px;"></div>
						</div>
						<div class="panel-right">
							<div class="input-wrapper">
								<div class="reset-container-block">
									<span class="reset-button" onclick="resetInput(this);">RESET</span>
								</div>
							</div>
							<!-- <div class="input-wrapper">
								<span class="input-title">What is your vehicle registration no.?</span>
								<div class="input-container-block">
									<input type="text" class="input-text front car-detail" name="jform[vehicleRegNo]" value="<?php if(isset($data["vehicleRegNo"])) echo $data["vehicleRegNo"]; ?>" />
								</div>
							</div>
							<div class="input-wrapper" style="display:none;">
								<span class="input-title" style="margin-right:0px;">China Extension</span>
								<img class="hasTooltip option-question" data-original-title="Policy is extended to cover the Motor Vehicle whilst in use in Guangdong Province of the People's Republic of China. Liability is subject to limitations. Please refer to Brochure and Policy Terms that can be downloaded in the comparison table." src="images/websiteicon/question-mark.png">
								<div class="input-container-block">
									<div class="select_mate" data-mate-select="active" >
										<select name="jform[chinaExtension]" onchange="" onclick="return false;"  class="hide-select custom-select">
											<option value="N" <?php if(isset($data["chinaExtension"]) && $data["chinaExtension"] == "N") echo "selected"; ?>>No</option>
											<option value="Y" <?php if(isset($data["chinaExtension"]) && $data["chinaExtension"] == "Y") echo "selected"; ?>>Yes</option>
										</select>
									</div>
								</div>
							</div> -->
							<div class="input-wrapper radio-year">
								<span class="input-title">Which year was your car manufactured?</span>
								<div id="year-block-1" class="<?php if(isset($data["carMakeYear"]) && $data["carMakeYear"] < 2006) {echo "display-none";} else { echo "display-block";} ?>">
									<div class="input-container-inline">
										<input type="radio" id="radio2019" class="input-radio car-detail" name="jform[carMakeYear]" value="2019" <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2019") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="radio2019">
											<span class="radio-label-title <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2019") echo "active";} ?>">2019</span>
										</label>
									</div>
									<div class="input-container-inline">
										<input type="radio" id="radio2018" class="input-radio car-detail" name="jform[carMakeYear]" value="2018" <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2018") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="radio2018">
											<span class="radio-label-title <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2018") echo "active";} ?>">2018</span>
										</label>
									</div>
									<div class="input-container-inline">
										<input type="radio" id="radio2017" class="input-radio car-detail" name="jform[carMakeYear]" value="2017" <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2017") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="radio2017">
											<span class="radio-label-title <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2017") echo "active";} ?>">2017</span>
										</label>
									</div>
									<div class="spacer"></div>
									<div class="input-container-inline">
										<input type="radio" id="radio2016" class="input-radio car-detail" name="jform[carMakeYear]" value="2016" <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2016") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="radio2016">
											<span class="radio-label-title <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2016") echo "active";} ?>">2016</span>
										</label>
									</div>
									<div class="input-container-inline">
										<input type="radio" id="radio2015" class="input-radio car-detail" name="jform[carMakeYear]" value="2015" <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2015") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="radio2015">
											<span class="radio-label-title <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2015") echo "active";} ?>">2015</span>
										</label>
									</div>
									<div class="input-container-inline">
										<input type="radio" id="radio2014" class="input-radio car-detail" name="jform[carMakeYear]" value="2014" <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2014") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="radio2014">
											<span class="radio-label-title <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2014") echo "active";} ?>">2014</span>
										</label>
									</div>
									<div class="spacer"></div>
									<div class="input-container-inline">
										<input type="radio" id="radio2013" class="input-radio car-detail" name="jform[carMakeYear]" value="2013" <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2013") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="radio2013">
											<span class="radio-label-title <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2013") echo "active";} ?>">2013</span>
										</label>
									</div>
									<div class="input-container-inline">
										<input type="radio" id="radio2012" class="input-radio car-detail" name="jform[carMakeYear]" value="2012" <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2012") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="radio2012">
											<span class="radio-label-title <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2012") echo "active";} ?>">2012</span>
										</label>
									</div>
									<div class="input-container-inline">
										<input type="radio" id="radio2011" class="input-radio car-detail" name="jform[carMakeYear]" value="2011" <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2011") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="radio2011">
											<span class="radio-label-title <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2011") echo "active";} ?>">2011</span>
										</label>
									</div>
									<div class="spacer"></div>
									<div class="input-container-inline">
										<input type="radio" id="radio2010" class="input-radio car-detail" name="jform[carMakeYear]" value="2010" <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2010") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="radio2010">
											<span class="radio-label-title <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2010") echo "active";} ?>">2010</span>
										</label>
									</div>
									<div class="input-container-inline">
										<input type="radio" id="radio2009" class="input-radio car-detail" name="jform[carMakeYear]" value="2009" <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2009") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="radio2009">
											<span class="radio-label-title <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2009") echo "active";} ?>">2009</span>
										</label>
									</div>
									<div class="input-container-inline">
										<input type="radio" id="radio2008" class="input-radio car-detail" name="jform[carMakeYear]" value="2008" <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2008") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="radio2008">
											<span class="radio-label-title <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2008") echo "active";} ?>">2008</span>
										</label>
									</div>
									<div class="spacer"></div>
									<div class="input-container-inline">
										<input type="radio" id="radio2007" class="input-radio car-detail" name="jform[carMakeYear]" value="2007" <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2007") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="radio2007">
											<span class="radio-label-title <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2007") echo "active";} ?>">2007</span>
										</label>
									</div>
									<div class="input-container-inline">
										<input type="radio" id="radio2006" class="input-radio car-detail" name="jform[carMakeYear]" value="2006" <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2006") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="radio2006">
											<span class="radio-label-title <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2006") echo "active";} ?>">2006</span>
										</label>
									</div>
									<div class="input-container-inline">
										<label class="radio-label-long" for="radiomore">
											<span id="radio-next-button" class="radio-label-title"  onclick="nextButton(this);">>>></span>
										</label>
									</div>
								</div>
								<div id="reg-year-block-2" class="<?php if(isset($data["carMakeYear"]) && $data["carMakeYear"] < 2006) {echo "display-block";} else { echo "display-none";} ?>">
									<div class="input-container-inline">
										<label class="radio-label-long" for="radiomore">
											<span id="radio-back-button" class="radio-label-title"  onclick="backButton(this);"><<<</span>
										</label>
									</div>
									<div class="input-container-inline">
										<input type="radio" id="reg-radio2005" class="input-radio car-detail" name="jform[carMakeYear]" value="2005" <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2005") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="reg-radio2005">
											<span class="radio-label-title <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2005") echo "active";} ?>">2005</span>
										</label>
									</div>
									<div class="input-container-inline">
										<input type="radio" id="reg-radio2004" class="input-radio car-detail" name="jform[carMakeYear]" value="2004" <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2004") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="reg-radio2004">
											<span class="radio-label-title <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2004") echo "active";} ?>">2004</span>
										</label>
									</div>
									<div class="spacer"></div>
									<div class="input-container-inline">
										<input type="radio" id="reg-radio2003" class="input-radio car-detail" name="jform[carMakeYear]" value="2003" <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2003") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="reg-radio2003">
											<span class="radio-label-title <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2003") echo "active";} ?>">2003</span>
										</label>
									</div>
									<div class="input-container-inline">
										<input type="radio" id="reg-radio2002" class="input-radio car-detail" name="jform[carMakeYear]" value="2002" <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2002") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="reg-radio2002">
											<span class="radio-label-title <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2002") echo "active";} ?>">2002</span>
										</label>
									</div>
									<div class="input-container-inline">
										<input type="radio" id="reg-radio2001" class="input-radio car-detail" name="jform[carMakeYear]" value="2001" <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2001") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="reg-radio2001">
											<span class="radio-label-title <?php if(isset($data["carMakeYear"])){ if($data["carMakeYear"] == "2001") echo "active";} ?>">2001</span>
										</label>
									</div>
								</div>
							</div>
							
						<!-- <div class="input-wrapper" style="padding-top:0; height:200px;">
								<span class="input-title">Which year was your car registered?</span>
								<div id="reg-year-block-1" class="display-block">
									<div class="input-container-inline">
										<input type="radio" id="reg-radio2018" class="input-radio" name="jform[carRegisterYear]" value="2018" <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2018") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="reg-radio2018">
											<span class="radio-label-title <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2018") echo "active";} ?>">2018</span>
										</label>
									</div>
									<div class="input-container-inline">
										<input type="radio" id="reg-radio2017" class="input-radio" name="jform[carRegisterYear]" value="2017" <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2017") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="reg-radio2017">
											<span class="radio-label-title <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2017") echo "active";} ?>">2017</span>
										</label>
									</div>
									<div class="input-container-inline">
										<input type="radio" id="reg-radio2016" class="input-radio" name="jform[carRegisterYear]" value="2016" <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2016") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="reg-radio2016">
											<span class="radio-label-title <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2016") echo "active";} ?>">2016</span>
										</label>
									</div>
									<div class="spacer"></div>
									<div class="input-container-inline">
										<input type="radio" id="reg-radio2015" class="input-radio" name="jform[carRegisterYear]" value="2015" <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2015") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="reg-radio2015">
											<span class="radio-label-title <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2015") echo "active";} ?>">2015</span>
										</label>
									</div>
									<div class="input-container-inline">
										<input type="radio" id="reg-radio2014" class="input-radio" name="jform[carRegisterYear]" value="2014" <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2014") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="reg-radio2014">
											<span class="radio-label-title <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2014") echo "active";} ?>">2014</span>
										</label>
									</div>
									<div class="input-container-inline">
										<input type="radio" id="reg-radio2013" class="input-radio" name="jform[carRegisterYear]" value="2013" <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2013") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="reg-radio2013">
											<span class="radio-label-title <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2013") echo "active";} ?>">2013</span>
										</label>
									</div>
									<div class="spacer"></div>
									<div class="input-container-inline">
										<input type="radio" id="reg-radio2012" class="input-radio" name="jform[carRegisterYear]" value="2012" <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2012") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="reg-radio2012">
											<span class="radio-label-title <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2012") echo "active";} ?>">2012</span>
										</label>
									</div>
									<div class="input-container-inline">
										<input type="radio" id="reg-radio2011" class="input-radio" name="jform[carRegisterYear]" value="2011" <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2011") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="reg-radio2011">
											<span class="radio-label-title <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2011") echo "active";} ?>">2011</span>
										</label>
									</div>
									<div class="input-container-inline">
										<input type="radio" id="reg-radio2010" class="input-radio" name="jform[carRegisterYear]" value="2010" <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2010") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="reg-radio2010">
											<span class="radio-label-title <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2010") echo "active";} ?>">2010</span>
										</label>
									</div>
									<div class="spacer"></div>
									<div class="input-container-inline">
										<input type="radio" id="reg-radio2009" class="input-radio" name="jform[carRegisterYear]" value="2009" <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2009") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="reg-radio2009">
											<span class="radio-label-title <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2009") echo "active";} ?>">2009</span>
										</label>
									</div>
									<div class="input-container-inline">
										<input type="radio" id="reg-radio2008" class="input-radio" name="jform[carRegisterYear]" value="2008" <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2008") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="reg-radio2008">
											<span class="radio-label-title <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2008") echo "active";} ?>">2008</span>
										</label>
									</div>
									<div class="input-container-inline">
										<label class="radio-label-long" for="radiomore">
											<span id="radio-next-button" class="radio-label-title"  onclick="nextButton(this);">>>></span>
										</label>
									</div>
								</div>
								<div id="reg-year-block-2" class="display-none">
									<div class="input-container-inline">
										<label class="radio-label-long" for="radiomore">
											<span id="radio-back-button" class="radio-label-title"  onclick="backButton(this);"><<<</span>
										</label>
									</div>
									<div class="input-container-inline">
										<input type="radio" id="reg-radio2007" class="input-radio" name="jform[carRegisterYear]" value="2007" <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2007") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="reg-radio2007">
											<span class="radio-label-title <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2007") echo "active";} ?>">2007</span>
										</label>
									</div>
									<div class="input-container-inline">
										<input type="radio" id="reg-radio2006" class="input-radio" name="jform[carRegisterYear]" value="2006" <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2006") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="reg-radio2006">
											<span class="radio-label-title <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2006") echo "active";} ?>">2006</span>
										</label>
									</div>
									<div class="spacer"></div>
									<div class="input-container-inline">
										<input type="radio" id="reg-radio2005" class="input-radio" name="jform[carRegisterYear]" value="2005" <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2005") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="reg-radio2005">
											<span class="radio-label-title <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2005") echo "active";} ?>">2005</span>
										</label>
									</div>
									<div class="input-container-inline">
										<input type="radio" id="reg-radio2004" class="input-radio" name="jform[carRegisterYear]" value="2004" <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2004") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="reg-radio2004">
											<span class="radio-label-title <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2004") echo "active";} ?>">2004</span>
										</label>
									</div>
									<div class="input-container-inline">
										<input type="radio" id="reg-radio2003" class="input-radio" name="jform[carRegisterYear]" value="2003" <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2003") echo "checked"; } ?> onclick="radioClicked(this);" />
										<label class="radio-label-long" for="reg-radio2003">
											<span class="radio-label-title <?php if(isset($data["carRegisterYear"])){ if($data["carRegisterYear"] == "2003") echo "active";} ?>">2003</span>
										</label>
									</div>
								</div>
							</div> -->
						</div>
					</div>
				</div>
				<div class="motor-form-input-wrapper">
					<div class="driver-accordion motor-accordion active <?php echo ($data["Industry"] ? "" : "click-disable"); ?>">DRIVER DETAILS & DRIVING EXPERIENCE</div>
					<div class="panel noshow" style="">
							<div class="tab-header-container" style="width: 100%;display:none;">
						        <?php if(isset($data["Gender"])):?>
						        	<?php if(count($data["Gender"])>1):?>
						        		<div class="tab-pane"  onClick="changeTab(this);" id="driver-0" data-id="driver-panel-0">
								        	<img class="tab-pane-image" alt="add driver" src="images/websiteicon/driver-logo.png" />
								        	<span class="tab-pane-title">MAIN DRIVER</span>
										</div>
										<?php foreach ($data["Gender"] as $key => $val):?>
											<?php if($key != 0): ?>
												<div class="tab-pane <?php echo $key+1 == count($data["Gender"]) ? "active" : ""?>" onClick="changeTab(this);" id="driver-<?php echo $key; ?>" data-id="driver-panel-<?php echo $key; ?>">
										        	<img class="tab-pane-image" alt="add driver" src="images/websiteicon/driver-logo.png" />
										        	<span class="tab-pane-title">DRIVER <?php echo $key+1; ?></span>
												</div>
											<?php endif; ?>
										<?php endforeach; ?>
						        	<?php else: ?>
						        		<div class="tab-pane active" onClick="changeTab(this);" id="driver-0" data-id="driver-panel-0">
								        	<img class="tab-pane-image" alt="add driver" src="images/websiteicon/driver-logo.png" />
								        	<span class="tab-pane-title">MAIN DRIVER</span>
										</div>
						        	<?php endif; ?>
								<?php else: ?>
									<div class="tab-pane active" onClick="changeTab(this);" id="driver-0" data-id="driver-panel-0">
							        	<img class="tab-pane-image" alt="add driver" src="images/websiteicon/driver-logo.png" />
							        	<span class="tab-pane-title">MAIN DRIVER</span>
									</div>
								<?php endif; ?>
						    </div>
							<div class="tab-content-container">
								<?php if(isset($data["Gender"]) && true == false):?>
						        		<?php foreach ($data["Gender"] as $key => $val):?>
											<div class="tab-wrapper <?php echo (count($data["Gender"]) > 0 && $key < count($data["Gender"])-1) ? "display-none" : ""; ?>" id="driver-panel-<?php echo $key; ?>">
												<div class="input-sub-wrapper-2" style="width: 409px;margin-right:0;height: 129px;margin-top: 27px;">
													<span class="input-title">What is <?php echo $key > 0 ? "driver ".($key+1): "your"; ?> gender?</span>
													<br>
													<div class="input-container-inline">
														<input type="radio" id="radio-gender-<?php echo $key; ?>-M" class="input-radio" name="jform[Gender][<?php echo $key; ?>]" <?php if(isset($data["Gender"])){ if($data["Gender"][$key] == "MALE") echo "checked"; } ?> value="MALE" onclick="radioClicked(this);" />
														<label class="radio-label-logo <?php if(isset($data["Gender"])){ if($data["Gender"][$key] == "MALE") echo "active";} ?>" for="radio-gender-<?php echo $key; ?>-M">
															<img class="radio-label-gender-icon" alt="male" src="images/websiteicon/male-logo.png"/>
															<span class="radio-label-title-gender">Male</span>
														</label>
													</div>
													<div class="input-container-inline">
														<input type="radio" id="radio-gender-<?php echo $key; ?>-F" class="input-radio" name="jform[Gender][<?php echo $key; ?>]" <?php if(isset($data["Gender"])){ if($data["Gender"][$key] == "FEMALE") echo "checked"; } ?> value="FEMALE" onclick="radioClicked(this);" />
														<label class="radio-label-logo <?php if(isset($data["Gender"])){ if($data["Gender"][$key] == "FEMALE") echo "active";} ?>" for="radio-gender-<?php echo $key; ?>-F">
														<img class="radio-label-gender-icon" alt="female" src="images/websiteicon/female-logo.png"/>
															<span class="radio-label-title-gender">Female</span>
														</label>
													</div>
												</div>
												<div class="input-sub-wrapper-2" style="width: 409px;margin-right:0;height:89px;margin-top: 62px;padding-left: 30px;">
													<span class="input-title">Date Of Birth</span>
													<div class="input-container-block">
														<?php 
															if(isset($data["dateOfBirth"])) { 
																echo str_replace("jform_dateOfBirth", "jform[dateOfBirth][$key]", str_replace("jform[dateOfBirth][0]", "jform[dateOfBirth][$key]", $this->form->getInput('dateOfBirth')));
																
																echo '<script>jQuery(function() {
																			jQuery( "input[name=\'jform[dateOfBirth]['.$key.']\']" ).datepicker({
																				changeMonth: true,
																				changeYear: true,
																				showOn: "focus",
																				dateFormat : "dd-M-yy",
																				yearRange: "1947:1995",
																				onChangeMonthYear:function(y, m, i){                     
																					var d = i.selectedDay;
																					jQuery(this).datepicker("setDate", new Date(y, m-1, d));
																				}
																			}).datepicker("setDate", "' . $data["dateOfBirth"][$key]. '");
																		});</script>';
															}
															else {
																echo $this->form->getInput('dateOfBirth');
															}
														?>
													</div>
												</div>
												<div class="input-sub-wrapper-2" style="width: 409px;margin-right:0;height:89px;">
													<span class="input-title">Years of driving experience?</span><br>
													<span class="input-sub-title">Based on current driving licence issued date</span>
													<div class="input-container-block">
														<div class="select_mate" data-mate-select="active" >
															<select name="jform[yearsOfDrivingExp][<?php echo $key; ?>]" onchange="" onclick="return false;"  class="hide-select custom-select">
																<option value="" <?php if(!isset($data["yearsOfDrivingExp"])){ echo 'selected="selected"'; } else{ if($data["yearsOfDrivingExp"][$key] == "") echo 'selected="selected"'; }?>>Select Driving Experience</option>
																<option value="0" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "0") echo 'selected="selected"'; } ?>>Below 1 year</option>
																<option value="1" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "1") echo 'selected="selected"'; } ?>>1 year</option>
																<option value="2" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "2") echo 'selected="selected"'; } ?>>2 years</option>
																<option value="3" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "3") echo 'selected="selected"'; } ?>>3 years</option>
																<option value="4" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "4") echo 'selected="selected"'; } ?>>4 years</option>
																<option value="5" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "5") echo 'selected="selected"'; } ?>>5 years</option>
																<option value="6" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "6") echo 'selected="selected"'; } ?>>6 years</option>
																<option value="7" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "7") echo 'selected="selected"'; } ?>>7 years</option>
																<option value="8" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "8") echo 'selected="selected"'; } ?>>8 years</option>
																<option value="9" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "9") echo 'selected="selected"'; } ?>>9 years</option>
																<option value="10" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "10") echo 'selected="selected"'; } ?>>10 years</option>
																<option value="11" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "11") echo 'selected="selected"'; } ?>>11 years</option>
																<option value="12" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "12") echo 'selected="selected"'; } ?>>12 years</option>
																<option value="13" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "13") echo 'selected="selected"'; } ?>>13 years</option>
																<option value="14" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "14") echo 'selected="selected"'; } ?>>14 years</option>
																<option value="15" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "15") echo 'selected="selected"'; } ?>>15 years</option>
																<option value="16" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "16") echo 'selected="selected"'; } ?>>16 years</option>
																<option value="17" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "17") echo 'selected="selected"'; } ?>>17 years</option>
																<option value="18" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "18") echo 'selected="selected"'; } ?>>18 years</option>
																<option value="19" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "19") echo 'selected="selected"'; } ?>>19 years</option>
																<option value="20" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "20") echo 'selected="selected"'; } ?>>20 years</option>
																<option value="21" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "21") echo 'selected="selected"'; } ?>>21 years</option>
																<option value="22" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "22") echo 'selected="selected"'; } ?>>22 years</option>
																<option value="23" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "23") echo 'selected="selected"'; } ?>>23 years</option>
																<option value="24" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "24") echo 'selected="selected"'; } ?>>24 years</option>
																<option value="25" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "25") echo 'selected="selected"'; } ?>>25 years</option>
																<option value="26" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "26") echo 'selected="selected"'; } ?>>26 years</option>
																<option value="27" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "27") echo 'selected="selected"'; } ?>>27 years</option>
																<option value="28" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "28") echo 'selected="selected"'; } ?>>28 years</option>
																<option value="29" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "29") echo 'selected="selected"'; } ?>>29 years</option>
																<option value="30" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "30") echo 'selected="selected"'; } ?>>30 years</option>
																<option value="31" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "31") echo 'selected="selected"'; } ?>>31 years</option>
																<option value="32" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "32") echo 'selected="selected"'; } ?>>32 years</option>
																<option value="33" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "33") echo 'selected="selected"'; } ?>>33 years</option>
																<option value="34" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "34") echo 'selected="selected"'; } ?>>34 years</option>
																<option value="35" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "35") echo 'selected="selected"'; } ?>>35 years</option>
																<option value="36" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "36") echo 'selected="selected"'; } ?>>36 years</option>
																<option value="37" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "37") echo 'selected="selected"'; } ?>>37 years</option>
																<option value="38" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "38") echo 'selected="selected"'; } ?>>38 years</option>
																<option value="39" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "39") echo 'selected="selected"'; } ?>>39 years</option>
																<option value="40" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "40") echo 'selected="selected"'; } ?>>40 years</option>
																<option value="41" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "41") echo 'selected="selected"'; } ?>>41 years</option>
																<option value="42" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "42") echo 'selected="selected"'; } ?>>42 years</option>
																<option value="43" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "43") echo 'selected="selected"'; } ?>>43 years</option>
																<option value="44" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "44") echo 'selected="selected"'; } ?>>44 years</option>
																<option value="45" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][$key]== "45") echo 'selected="selected"'; } ?>>45 years</option>
															</select>
														</div>
													  	<!-- Custom select structure --> 
													</div><!-- End div center   -->
												</div>
												<div class="input-sub-wrapper-2" style="width: 409px;margin-right:0;height:89px;padding-left: 30px;">
													<span class="input-title">What is <?php echo $key > 0 ? "driver ". ($key+1): "your"; ?> job industry?</span>
													<div class="input-container-block">
														<div class="select_mate" data-mate-select="active" >
															<select name="jform[Industry][<?php echo $key; ?>]" onchange="" onclick="return false;"  class="hide-select custom-select">
																<option value="" disabled <?php if(!isset($data["Industry"])){ echo 'selected="selected"'; } else{ if($data["Industry"][$key] == "") echo 'selected="selected"'; }?>>Select Job Industry</option>
																<?php foreach($this->occupationParent as $occupation): ?>
																	<option value="<?php echo $occupation->id; ?>" <?php if(isset($data["Industry"])){ if($data["Industry"][$key]== $occupation->id) echo 'selected="selected"'; } ?>><?php echo ucwords(strtolower($occupation->occ_name)); ?></option>
																<?php endforeach; ?>
															</select>
														</div>
													  	<!-- Custom select structure --> 
													</div>
												</div>
												<div class="input-sub-wrapper-2" style="width: 409px;margin-right:0;height:89px;">
													<span class="input-title">What is <?php echo $key > 0 ? "driver ". ($key+1): "your"; ?> marital status?</span>
													<div class="input-container-block">
														<div class="select_mate" data-mate-select="active" >
															<select name="jform[maritalStatus][<?php echo $key; ?>]" onchange="" onclick="return false;"  class="hide-select custom-select">
																<option value="" disabled <?php if(!isset($data["maritalStatus"])){ echo 'selected="selected"'; } else{ if($data["maritalStatus"][$key] == "") echo 'selected="selected"'; }?>>Marital Status</option>
																<option value="Married" <?php if(isset($data["maritalStatus"])){ if($data["maritalStatus"][$key]== "Married") echo 'selected="selected"'; } ?>>Married</option>
															    <option value="Single" <?php if(isset($data["maritalStatus"])){ if($data["maritalStatus"][$key]== "Single") echo 'selected="selected"'; } ?>>Single</option>
																<option value="Divorced" <?php if(isset($data["maritalStatus"])){ if($data["maritalStatus"][$key]== "Divorced") echo 'selected="selected"'; } ?>>Divorced</option>
															</select>
														</div>
													  	<!-- Custom select structure --> 
													</div>
												</div>
												<div class="input-sub-wrapper-2" style="width: 409px;margin-right:0;height:89px;padding-left: 30px;">
													<span class="input-title">What is <?php echo $key > 0 ? "driver ". ($key+1): "your"; ?> job position?</span>
													<div class="input-container-block job-position">
														<div class="select_mate" data-mate-select="active" >
															<select name="jform[Occupation][<?php echo $key; ?>]" onchange="" onclick="return false;"  class="hide-select custom-select">
																<option value="" disabled <?php if(!isset($data["Occupation"])){ echo 'selected="selected"'; } else{ if($data["Occupation"][$key] == "") echo 'selected="selected"'; }?>>Select Job Position</option>
																<?php $i = 0; ?>
																<?php foreach($this->occupation as $occupation): ?>
																	<?php if($data["Occupation"][$key]== $occupation->occ_code && $i < 1): ?>
																	<option value="<?php echo $occupation->occ_code; ?>" <?php if(isset($data["Occupation"])){ if($data["Occupation"][$key]== $occupation->occ_code) echo 'selected="selected"'; } ?>><?php echo ucwords(strtolower($occupation->occ_name)); ?></option>
																	<?php $i++; ?>
																	<?php endif; ?>
																<?php endforeach; ?>
															</select>
														</div>
													  	<!-- Custom select structure --> 
													</div>
												</div>
												<?php if($key == 0): ?>
													<input type="hidden" name="jform[relationshipWithMainDriver][<?php echo $key; ?>]" value="I"/>
													<div class="input-sub-wrapper-2 add-driver">
														<div onClick="addDriver(this);" style="float:right">
															<img class="add-driver-img" alt="add driver" src="images/websiteicon/add-driver-logo.png"/>
															<span class="add-driver-title">ADD A DRIVER</span>
														</div>
													</div>
												<?php else: ?>
													<div class="input-sub-wrapper-2" style="width: 409px;margin-right:0;height:89px;">
														<span class="input-title">Relationship with main driver?</span>
														<div class="input-container-block">
															<div class="select_mate" data-mate-select="active" >
																<select name="jform[relationshipWithMainDriver][<?php echo $key; ?>]" onchange="" onclick="return false;"  class="hide-select custom-select">
																	<option value="" disabled <?php if(!isset($data["relationshipWithMainDriver"])){ echo 'selected="selected"'; } else{ if($data["relationshipWithMainDriver"][$key] == "") echo 'selected="selected"'; }?>>Relationship</option>
																	<option value="F" <?php if(isset($data["relationshipWithMainDriver"])){ if($data["relationshipWithMainDriver"][$key]== "F") echo 'selected="selected"'; } ?>>Friend</option>
																	<option value="R" <?php if(isset($data["relationshipWithMainDriver"])){ if($data["relationshipWithMainDriver"][$key]== "R") echo 'selected="selected"'; } ?>>Relative</option>
																	<option value="L" <?php if(isset($data["relationshipWithMainDriver"])){ if($data["relationshipWithMainDriver"][$key]== "L") echo 'selected="selected"'; } ?>>Siblings</option>
																	<option value="P" <?php if(isset($data["relationshipWithMainDriver"])){ if($data["relationshipWithMainDriver"][$key]== "P") echo 'selected="selected"'; } ?>>Parent</option>
																	<option value="S" <?php if(isset($data["relationshipWithMainDriver"])){ if($data["relationshipWithMainDriver"][$key]== "S") echo 'selected="selected"'; } ?>>Spouse</option>
																	<option value="C" <?php if(isset($data["relationshipWithMainDriver"])){ if($data["relationshipWithMainDriver"][$key]== "C") echo 'selected="selected"'; } ?>>Child</option>
																	<option value="I" <?php if(isset($data["relationshipWithMainDriver"])){ if($data["relationshipWithMainDriver"][$key]== "I") echo 'selected="selected"'; } ?>>Self</option>
																	<option value="O" <?php if(isset($data["relationshipWithMainDriver"])){ if($data["relationshipWithMainDriver"][$key]== "O") echo 'selected="selected"'; } ?>>Others</option>
																  </select>
															</div>
														</div>
													</div>
													<div class="input-wrapper add-driver" style="margin-top: 85px;display: block;">
															<div onClick="removeDriver(this);" style="float:left" data-id="<?php echo $key; ?>">
																<img class="add-driver-img" alt="remove driver" src="images/websiteicon/remove-driver-logo.png"/>
																<span class="remove-driver-title">REMOVE THIS DRIVER</span>
															</div>
															<div onClick="addDriver(this);" style="float:right">
																<img class="add-driver-img" alt="add driver" src="images/websiteicon/add-driver-logo.png"/>
																<span class="add-driver-title">ADD A DRIVER</span>
															</div>
													</div>
												<?php endif; ?>
											</div>
										<?php endforeach; ?>
								<?php else: ?>
									<div class="tab-wrapper" id="driver-panel-0">
										<!-- <div class="input-sub-wrapper-2" style="width: 409px;margin-right:0;height: 129px;margin-top: 27px;">
											<span class="input-title">What is your gender?</span>
											<br>
											<div class="input-container-inline">
												<input type="radio" id="radio-gender-0-M" class="input-radio" name="jform[Gender][0]" value="MALE" onclick="radioClicked(this);" />
												<label class="radio-label-logo" for="radio-gender-0-M">
													<img class="radio-label-gender-icon" alt="male" src="images/websiteicon/male-logo.png"/>
													<span class="radio-label-title-gender">Male</span>
												</label>
											</div>
											<div class="input-container-inline">
												<input type="radio" id="radio-gender-0-F" class="input-radio" name="jform[Gender][0]" value="FEMALE" onclick="radioClicked(this);" />
												<label class="radio-label-logo" for="radio-gender-0-F">
												<img class="radio-label-gender-icon" alt="female" src="images/websiteicon/female-logo.png"/>
													<span class="radio-label-title-gender">Female</span>
												</label>
											</div>
										</div> -->
										<div class="input-sub-wrapper-2" style="width: 409px;margin-right:0;height:89px;">
											<span class="input-title">Date Of Birth</span>
											<div class="input-container-block driver-birth">
														<?php 
															if(isset($data["dateOfBirth"])) { 
																echo str_replace("jform_dateOfBirth", "jform[dateOfBirth][0]", str_replace("jform[dateOfBirth][0]", "jform[dateOfBirth][0]", $this->form->getInput('dateOfBirth')));
																
																echo '<script>jQuery(function() {
																			jQuery( "input[name=\'jform[dateOfBirth][0]\']" ).datepicker({
																				changeMonth: true,
																				changeYear: true,
																				showOn: "focus",
																				dateFormat : "dd-M-yy",
																				yearRange: "1947:1995",
																				onChangeMonthYear:function(y, m, i){                     
																					var d = i.selectedDay;
																					jQuery(this).datepicker("setDate", new Date(y, m-1, d));
																				}
																			}).datepicker("setDate", "' . $data["dateOfBirth"][0]. '");
																		});</script>';
															}
															else {
																echo $this->form->getInput('dateOfBirth');
															}
														?>
											</div>
										</div>
										<div class="input-sub-wrapper-2" style="width: 409px;margin-right:0;height:89px;padding-left: 30px;margin-top: 8px;">
											<span class="input-title">What is your job industry?</span>
											<div class="input-container-block">
												<div class="select_mate" data-mate-select="active" >
													<select name="jform[Industry][0]" onchange="" onclick="return false;"  class="hide-select custom-select driver-detail">
														<option value="" disabled <?php if(!isset($data["Industry"])){ echo 'selected="selected"'; } else{ if($data["Industry"][0] == "") echo 'selected="selected"'; }?>>Select Job Industry</option>
														<?php foreach($this->occupationParent as $occupation): ?>
															<option value="<?php echo $occupation->id; ?>" <?php if(isset($data["Industry"])){ if($data["Industry"][0]== $occupation->id) echo 'selected="selected"'; } ?>><?php echo ucwords(strtolower($occupation->occ_name)); ?></option>
														<?php endforeach; ?>
													</select>
												</div>
												<!-- Custom select structure --> 
											</div>
										</div>
										<div class="input-sub-wrapper-2" style="width: 409px;margin-right:0;height:89px;">
											<span class="input-title" style="margin-bottom: 9px;">Years of driving experience?</span><br>
											<span class="input-sub-title">Based on current driving licence issued date</span>
											<div class="input-container-block">
												<div class="select_mate" data-mate-select="active" >
													<select name="jform[yearsOfDrivingExp][0]" onchange="" onclick="return false;"  class="hide-select custom-select driver-detail">
														<option value="" <?php if(!isset($data["yearsOfDrivingExp"])){ echo 'selected="selected"'; } else{ if($data["yearsOfDrivingExp"][0] == "") echo 'selected="selected"'; }?>>Select Driving Experience</option>
														<option value="0" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "0") echo 'selected="selected"'; } ?>>Below 1 year</option>
														<option value="1" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "1") echo 'selected="selected"'; } ?>>1 year</option>
														<option value="2" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "2") echo 'selected="selected"'; } ?>>2 years</option>
														<option value="3" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "3") echo 'selected="selected"'; } ?>>3 years</option>
														<option value="4" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "4") echo 'selected="selected"'; } ?>>4 years</option>
														<option value="5" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "5") echo 'selected="selected"'; } ?>>5 years</option>
														<option value="6" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "6") echo 'selected="selected"'; } ?>>6 years</option>
														<option value="7" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "7") echo 'selected="selected"'; } ?>>7 years</option>
														<option value="8" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "8") echo 'selected="selected"'; } ?>>8 years</option>
														<option value="9" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "9") echo 'selected="selected"'; } ?>>9 years</option>
														<option value="10" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "10") echo 'selected="selected"'; } ?>>10 years</option>
														<option value="11" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "11") echo 'selected="selected"'; } ?>>11 years</option>
														<option value="12" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "12") echo 'selected="selected"'; } ?>>12 years</option>
														<option value="13" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "13") echo 'selected="selected"'; } ?>>13 years</option>
														<option value="14" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "14") echo 'selected="selected"'; } ?>>14 years</option>
														<option value="15" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "15") echo 'selected="selected"'; } ?>>15 years</option>
														<option value="16" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "16") echo 'selected="selected"'; } ?>>16 years</option>
														<option value="17" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "17") echo 'selected="selected"'; } ?>>17 years</option>
														<option value="18" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "18") echo 'selected="selected"'; } ?>>18 years</option>
														<option value="19" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "19") echo 'selected="selected"'; } ?>>19 years</option>
														<option value="20" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "20") echo 'selected="selected"'; } ?>>20 years</option>
														<option value="21" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "21") echo 'selected="selected"'; } ?>>21 years</option>
														<option value="22" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "22") echo 'selected="selected"'; } ?>>22 years</option>
														<option value="23" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "23") echo 'selected="selected"'; } ?>>23 years</option>
														<option value="24" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "24") echo 'selected="selected"'; } ?>>24 years</option>
														<option value="25" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "25") echo 'selected="selected"'; } ?>>25 years</option>
														<option value="26" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "26") echo 'selected="selected"'; } ?>>26 years</option>
														<option value="27" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "27") echo 'selected="selected"'; } ?>>27 years</option>
														<option value="28" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "28") echo 'selected="selected"'; } ?>>28 years</option>
														<option value="29" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "29") echo 'selected="selected"'; } ?>>29 years</option>
														<option value="30" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "30") echo 'selected="selected"'; } ?>>30 years</option>
														<option value="31" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "31") echo 'selected="selected"'; } ?>>31 years</option>
														<option value="32" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "32") echo 'selected="selected"'; } ?>>32 years</option>
														<option value="33" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "33") echo 'selected="selected"'; } ?>>33 years</option>
														<option value="34" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "34") echo 'selected="selected"'; } ?>>34 years</option>
														<option value="35" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "35") echo 'selected="selected"'; } ?>>35 years</option>
														<option value="36" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "36") echo 'selected="selected"'; } ?>>36 years</option>
														<option value="37" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "37") echo 'selected="selected"'; } ?>>37 years</option>
														<option value="38" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "38") echo 'selected="selected"'; } ?>>38 years</option>
														<option value="39" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "39") echo 'selected="selected"'; } ?>>39 years</option>
														<option value="40" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "40") echo 'selected="selected"'; } ?>>40 years</option>
														<option value="41" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "41") echo 'selected="selected"'; } ?>>41 years</option>
														<option value="42" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "42") echo 'selected="selected"'; } ?>>42 years</option>
														<option value="43" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "43") echo 'selected="selected"'; } ?>>43 years</option>
														<option value="44" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "44") echo 'selected="selected"'; } ?>>44 years</option>
														<option value="45" <?php if(isset($data["yearsOfDrivingExp"])){ if($data["yearsOfDrivingExp"][0]== "45") echo 'selected="selected"'; } ?>>45 years</option>
													</select>
												</div>
											  	<!-- Custom select structure --> 
											</div><!-- End div center   -->
										</div>
										
										<!-- <div class="input-sub-wrapper-2" style="width: 409px;margin-right:0;height:89px;">
											<span class="input-title">What is your marital status?</span>
											<div class="input-container-block">
												<div class="select_mate" data-mate-select="active" >
													<select name="jform[maritalStatus][0]" onchange="" onclick="return false;"  class="hide-select custom-select">
														<option value="" disabled selected>Marital Status</option>
														<option value="Married">Married</option>
													    <option value="Single">Single</option>
														<option value="Divorced">Divorced</option>
													</select>
												</div>
											</div>
										</div> -->
										<div class="input-sub-wrapper-2" style="width: 409px;margin-right:0;height:89px;padding-left: 30px;margin-top: 9px;">
											<span class="input-title">What is your job position?</span>
											<div class="select_mate" data-mate-select="active" >
												<select name="jform[Occupation][0]" onchange="" onclick="return false;"  class="hide-select custom-select driver-detail">
													<option value="" disabled <?php if(!isset($data["Occupation"])){ echo 'selected="selected"'; } else{ if($data["Occupation"][0] == "") echo 'selected="selected"'; }?>>Select Job Position</option>
													<?php $i = 0; ?>
													<?php foreach($this->occupation as $occupation): ?>
														<?php if($data["Occupation"][0]== $occupation->occ_code && $i < 1): ?>
														<option value="<?php echo $occupation->occ_code; ?>" <?php if(isset($data["Occupation"])){ if($data["Occupation"][0]== $occupation->occ_code) echo 'selected="selected"'; } ?>><?php echo ucwords(strtolower($occupation->occ_name)); ?></option>
														<?php $i++; ?>
														<?php endif; ?>
													<?php endforeach; ?>
												</select>
											</div>
										</div>
										<input type="hidden" name="jform[relationshipWithMainDriver][0]" value="I"/>
										<!-- <div class="input-wrapper add-driver">
											<div onClick="addDriver(this);" style="float:right">
												<img class="add-driver-img" alt="add driver" src="images/websiteicon/add-driver-logo.png"/>
												<span class="add-driver-title">ADD A DRIVER</span>
											</div>
										</div> -->
									</div>
								<?php endif; ?>
							</div>
					</div>
				</div>
				<div class="motor-form-input-wrapper <?php echo ($data["Industry"] ? "" : "hidden"); ?>">
					<div class="motor-accordion active insurance-accordion">INSURANCE & PREFERENCES</div>
					<div class="panel insurance-panel noshow">
						<div class="panel-left mt20">
							<div class="left-align">
								<div class="input-wrapper">
									<span class="input-title">Cover Type</span>
									<div class="input-container-block">
										<div class="select_mate" data-mate-select="active" >
											<select name="jform[coverType]" onchange="" onclick="return false;"  class="hide-select custom-select insurance-detail">
												<option value="" disabled selected>Select Cover Type</option>
												<option value="01" <?php if(isset($data["coverType"]) && $data["coverType"] == "01") echo "selected"; ?>>COMPREHENSIVE</option>
												<option value="02" <?php if(isset($data["coverType"]) && $data["coverType"] == "02") echo "selected"; ?>>THIRD PARTY</option>
										  	</select>
										</div>
									  	<!-- Custom select structure --> 
									</div>
								</div>
								<div class="input-wrapper value-wrapper" style="<?php if(isset($data["coverType"]) && $data["coverType"] == "02") echo "display:none;"; ?>">
									<span class="input-title">What is your estimated car value ?</span>
									<div class="input-container-block">
										<input id="carValue" type="text" class="input-text front insurance-detail" name="carVal" value="<?php if(isset($data["vehicleValue"])) echo $data["vehicleValue"]; ?>" required/>
										<input id="carValueSend" type="text" class="input-text front" style="display:none;" name="jform[vehicleValue]" value="<?php if(isset($data["vehicleValue"])) echo $data["vehicleValue"]; ?>" />
										<label class="aigMin" style="display: none;font-size:10px;">*AIG minimum car value is $HK 150,000 the quotation will be based on this value</label>
									</div>
								</div>
								<div class="input-wrapper">
									<span class="input-title">Policy Start Date</span>
									<div class="input-container-block policy-date">
										<?php echo $this->form->getInput('policyStartDate'); ?>
										<?php if(isset($data["policyStartDate"])) { 
											echo '<script>
														jQuery(function(){
															jQuery( "#jform_policyStartDate" ).datepicker({
																changeMonth: true,
																changeYear: true,
																showOn: "focus",
																minDate: 0,
																dateFormat : "dd-M-yy",
																yearRange: "1920:1995",
																onChangeMonthYear:function(y, m, i){                     
																	var d = i.selectedDay;
																	jQuery(this).datepicker("setDate", new Date(y, m-1, d));
																}
															});
															jQuery( "#jform_policyStartDate" ).datepicker("setDate", "' . explode(" ", $data["policyStartDate"])[0] . '");
														});
													</script>';
										} 
										?>
									</div>
								</div>
							</div>
						</div>
						<div class="panel-right"  style="padding-left:40px;">
							<div class="left-align">
								<div class="input-wrapper">
									<div class="reset-container-block">
										<span class="reset-button" onclick="resetInput(this);">RESET</span>
									</div>
								</div>
								<div class="input-wrapper">
									<span class="input-title">Who is your Current Insurer?</span>
									<div class="input-container-block">
										<div class="select_mate" data-mate-select="active" >
											<select name="jform[currentInsurer]" onchange="" onclick="return false;"  class="hide-select custom-select">
												<option value="null" <?php if(!isset($data["currentInsurer"])){ echo 'selected="selected"'; }else{ if($data["currentInsurer"] == "null") echo 'selected="selected"'; }?>>Select Insurers</option>
											   	<?php foreach($this->insurer as $insurer): ?>
													<option value="<?php echo $insurer->insurer_code; ?>" <?php if(isset($data["currentInsurer"])){ if($data["currentInsurer"] == $insurer->insurer_code) echo 'selected="selected"'; } ?>><?php echo strtoupper($insurer->insurer_name); ?></option>
												<?php endforeach; ?>
										  	</select>
										</div>
									  	<!-- Custom select structure --> 
									</div>
								</div>
								<div class="input-wrapper radio-ncd">
								<span class="input-title display-block">What is your NCD% at Renewal?</span>
								<div class="input-container-inline">
									<input type="radio" id="radio60" class="input-radio insurance-detail" name="jform[NCDPoints]" <?php if(isset($data["NCDPoints"])){ if($data["NCDPoints"] == 60) echo "checked"; } ?> value="60" onclick="radioClicked(this);" />
									<label class="radio-label-long insurance-label" for="radio60">
										<span class="radio-label-title <?php if(isset($data["NCDPoints"])){ if($data["NCDPoints"] == 60) echo "active";} ?>">60%</span>
									</label>
								</div>
								<div class="input-container-inline">
									<input type="radio" id="radio50" class="input-radio insurance-detail" name="jform[NCDPoints]" <?php if(isset($data["NCDPoints"])){ if($data["NCDPoints"] == 50) echo "checked"; } ?> value="50" onclick="radioClicked(this);" />
									<label class="radio-label-long insurance-label" for="radio50">
										<span class="radio-label-title <?php if(isset($data["NCDPoints"])){ if($data["NCDPoints"] == 50) echo "active";} ?>">50%</span>
									</label>
								</div>
								<div class="input-container-inline">
									<input type="radio" id="radio40" class="input-radio insurance-detail" name="jform[NCDPoints]" <?php if(isset($data["NCDPoints"])){ if($data["NCDPoints"] == 40) echo "checked"; } ?> value="40" onclick="radioClicked(this);" />
									<label class="radio-label-long insurance-label" for="radio40">
										<span class="radio-label-title <?php if(isset($data["NCDPoints"])){ if($data["NCDPoints"] == 40) echo "active";} ?>">40%</span>
									</label>
								</div>
								<div class="spacer"></div>
								<div class="input-container-inline">
									<input type="radio" id="radio30" class="input-radio insurance-detail" name="jform[NCDPoints]" <?php if(isset($data["NCDPoints"])){ if($data["NCDPoints"] == 30) echo "checked"; } ?> value="30" onclick="radioClicked(this);" />
									<label class="radio-label-long insurance-label" for="radio30">
										<span class="radio-label-title <?php if(isset($data["NCDPoints"])){ if($data["NCDPoints"] == 30) echo "active";} ?>">30%</span>
									</label>
								</div>
								
								<div class="input-container-inline">
									<input type="radio" id="radio20" class="input-radio insurance-detail" name="jform[NCDPoints]" <?php if(isset($data["NCDPoints"])){ if($data["NCDPoints"] == 20) echo "checked"; } ?> value="20" onclick="radioClicked(this);" />
									<label class="radio-label-long insurance-label" for="radio20">
										<span class="radio-label-title <?php if(isset($data["NCDPoints"])){ if($data["NCDPoints"] == 20) echo "active";} ?>">20%</span>
									</label>
								</div>
								<div class="input-container-inline">
									<input type="radio" id="radio0" class="input-radio insurance-detail" name="jform[NCDPoints]" <?php if(isset($data["NCDPoints"])){ if($data["NCDPoints"] == 0) echo "checked"; } ?> value="0" onclick="radioClicked(this);" />
									<label class="radio-label-long insurance-label" for="radio0">
										<span class="radio-label-title <?php if(isset($data["NCDPoints"])){ if($data["NCDPoints"] == 0) echo "active";} ?>">0%</span>
									</label>
								</div>
							</div>
							<div class="input-wrapper otherNCDWrapper" <?php if(isset($data["NCDPoints"])){ if($data["NCDPoints"] != 0) echo 'style="display:none"'; }else { echo 'style="display:none"';} ?>>
								<span class="input-title">Any other insured Motor policy with NCD in force?</span>
								<div class="input-container-block">
									<div class="select_mate" data-mate-select="active" >
										<select name="jform[otherNCD]" onchange="" onclick="return false;"  class="hide-select custom-select">
											<option value="N" <?php if(isset($data["otherNCD"])){ if($data["otherNCD"] == "N") echo 'selected="selected"'; } ?>>No</option>
											<option value="Y" <?php if(isset($data["otherNCD"])){ if($data["otherNCD"] == "Y") echo 'selected="selected"'; } ?>>Yes</option>
										</select>
									</div>
								</div>
							</div>
							</div>
						</div>
					</div>
				</div>
				<div class="motor-form-input-wrapper <?php echo ($data["Industry"] ? "" : "hidden"); ?>">
						<div class="motor-accordion active declaration-accordion">DECLARATION</div>
						<div class="panel noshow" id="agreement" style="overflow-y:scroll;height:200px;">
						<div class="sub-panel-bottom">
								<div class="input-wrapper-block-top">
									<div class="input-panel-left">
										<div class="squaredFour">
									    	<input type="checkbox" id="squaredFour" name="jform[agreePDC]" <?php if(isset($data["agreePDC"])){ if($data["agreePDC"] == "true") echo "checked=\"checked\""; }else { echo "checked=\"checked\"";} ?> value="true"/>
									    	<label for="squaredFour"></label>
									    </div>
									</div>
									<div class="input-panel-right">
										<div class="input-h1">
											I Agree
										</div>
										<div class="input-h2">
											to the use and transfer of my personal data to InsureDIY Limited in accordance with the Personal Data Collection Statement, although I understand that InsureDIY Limited acknowledges that I may opt out of the use of my personal data for direct marketing purpose by indicating as such below.*
										</div>
									</div>
								</div>
								<div class="input-wrapper-block-top">
									<div class="input-panel-left">
										<div class="squaredFour">
									    	<input type="checkbox" id="squaredFour2" name="jform[agreeMarketing]" <?php if(isset($data["agreeMarketing"])){ if($data["agreeMarketing"] == "true") echo "checked=\"checked\""; }else { echo "checked=\"checked\"";} ?> value="true"/>
									    	<label for="squaredFour2"></label>
									    </div>
									</div>
									<div class="input-panel-right">
										<div class="input-h1">
											I Agree
										</div>
										<div class="input-h2">
											InsureDIY Limited and its affiliated companies and partners (insurance companies included) would like to send me information on relevant products and services that may be of interest to me. Often, InsureDIY Limited or its affiliated companies or partners (insurance companies included) are able to notify me on promotional prices that will give me good value. I agree to InsureDIY Limited and their affiliated companies using my personal data (email, telephone number and postal address) for direct marketing of products and services as set out in their Personal Data Collection Statement.*
										</div>
									</div>
								</div>
							</div>
							<div class="panel-top">
								<div class="input-h1" style="margin-bottom: 10px;">
										Declarations 
								</div>
								<ul class="dash-list">
									<li>
										I am the car owner and first named driver;
									</li>
									<li>
										I and all named driver(s) are aged 25 to 65, and the driving experience is 2 years or above, holding with valid driving license and incurred not more than 8 points in Driving-offence Points System;
									</li>
									<li>
										I and all named driver(s) have never been disqualified and have a clean claims record over the immediately preceding 3 year period.
									</li>
									<li>
										I and all named driver(s) did not involve any dangerous driving, drunk driving, drug driving or penalized with driving license suspension in the last 3 years;
									</li>
									<li>
										I and all named driver(s) have never been refused renewal by any insurers, cancel or additional terms in the last 3 years;
									</li>
									<li>
										I am not in one of the following occupations: an actor/actress and Entertainment Industry; journalist/reporter/ employee in the media industry
									</li>
									<li>
										My car is not used for commercial purpose or used for hire/reward.
									</li>
									<li>
										My car is registered and used only in Hong Kong.
									</li>
									<li>
										I understand that InsureDIY will review my information and if my application falls under a referral risk for the selected insurer, InsureDIY will contact me for more information. 
									</li>
									<li>
										I understand that my application may or may not be accepted by the insurer and if not accepted by the insurer, InsureDIY will fully refund the premium that I paid on the InsureDIY website.
									</li>
								</ul>
							</div>
							<div class="panel-below">
								<div class="input-wrapper-block-top">
									<div class="input-panel-left">
										<div class="squaredFour">
											<input type="checkbox" id="squaredFour3" name="jform[agreeDeclaration]" value="true" checked/>
											<label for="squaredFour3"></label>
										</div>
									</div>
									<div class="input-panel-right" style="width:auto;">
										<div class="input-h2">
											I agree with <span>ALL</span> of the above statements.
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				<input type="hidden" name="jform[validLicense]" value="" />
				<input type="hidden" name="task" value="form.step1save" />
				<input type="hidden" name="jform[chinaExtension]" value="N" />
				<?php echo $this->form->getInput('id'); ?>
				<?php if ($itemid): ?>
					<input type="hidden" name="Itemid" value="$itemid" />
				<?php endif; ?>
				<?php echo JHtml::_('form.token'); ?>
			
				<?php  echo InsureDIYMotorHelper::renderProceedButton(false);?>
				
				<div class="clear"></div>
			</form>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>
<div class="logo-slider"><?php echo MyHelper::load_module(102); ?></div>

<?php /* Popup Box */ ?>
<div class="insurediy-popup" id="insurediy-popup-box" style="display:none;">
	<div class="header">
		<div class="text-header"><?php echo JText::_("POP_IMPORTANT_NOTICE"); ?></div>
		<a class="close" href="javascript:cleanInsureDIYPopup('insurediy-popup-box');">&nbsp;</a>
		<div class="clear"></div>
	</div>
	<div class="padding">
		<div><?php echo JText::_("COM_INSUREDIYTRAVEL_MEDICAL_ISSUE_MSG"); ?></div>
		<div style="text-align:right;margin-top: 20px;">
			<input type="button" class="btn btn-primary" style="height:30px;padding:5px 20px;font-size:14px !important;" onclick="javascript:cleanInsureDIYPopup('insurediy-popup-box');
			return false;" value="<?php echo JText::_("JOK"); ?>" />
		</div>
	</div>
</div>
<?php if (FALSE): ?>
<div class="insurediy-popup" id="insurediy-popup-box-2" style="display:none;">
	<div class="header">
		<div class="text-header"><?php echo JText::_("COM_INSUREDIYTRAVEL_POP_UP_HEADER_MESSAGE"); ?></div>
		<a class="close" href="javascript:cleanInsureDIYPopup('insurediy-popup-box-2');">&nbsp;</a>
		<div class="clear"></div>
	</div>
	<div class="padding">
		<div><?php echo JText::_("COM_INSUREDIYTRAVEL_POP_UP_BODY_IS_NEW_QUOTE"); ?></div>
		<div style="margin-top: 20px;text-align:right;">
			<form action="<?php echo JRoute::_('index.php'); ?>" method="post" name="adminForm" id="getNew">
				<input type="hidden" name="option" value="com_insurediymotor" />
				<input type="hidden" name="task" value="form.startNewQuotation" />
				<?php echo JHtml::_('form.token'); ?>
				<input type="submit" class="btn btn-primary" style="height:30px;padding:5px 20px;font-size:14px !important;" value="<?php echo JText::_("JNEW"); ?>"/>
				<a href="<?php echo $this->continueURL; ?>" class="btn btn-primary" style="height:30px;padding:0px 20px;line-height: 30px;font-size:14px !important;"><?php echo JText::_("JCONTINUE"); ?></a>
			</form>
		</div>
	</div>
</div>
<?php endif; ?>

<!-- CERT OF MERIT MODAL -->
<div class="merit-cert-modal">
	<div class="modal-content">
		<span class="close">&times;</span>
		<div class="modal-header">
			SAFE DRIVER DISCOUNT
		</div>
		<div class="modal-body">
			<div class="modal-content-h1">
				Can you provide a certificate of merit?
			</div>
			<div class="modal-content-h2">
				If so you can enjoy an additional <span class="red-color">5% discount</span>, which will be deducted from your premium on the next page.
			</div>
		</div>
		<div class="modal-new-footer">
			<div class="modal-button" data-dismiss="modal" onclick="setCertMerit(false);">
				NO, I CAN'T
			</div>
			<div class="modal-button" data-dismiss="modal" onclick="setCertMerit(true);">
				YES, I CAN
			</div>
		</div>
	</div>
</div>
<!-- END CERT OF MERIT MODAL -->

<!-- OLD CAR MODAL -->
<div class="old-car-modal">
	<div class="modal-content">
		<span class="close">&times;</span>
		<div class="modal-header">
			Cover Types Overview
		</div>
		<div class="modal-body">
			<div class="modal-content-h1">
				We notice that your car is <span class="car-age-span"></span> years old.
			</div>
			<div class="modal-content-h2">
				Please note that some insurers only provide Third Party, Fire and Theft coverage for cars above 10 years old.
			</div>
		</div>
		<div class="modal-new-footer">
			<div class="modal-button" data-dismiss="modal">
				OK! GOT IT!
			</div>
		</div>
	</div>
</div>
<!-- END OLD CAR MODAL -->
<!-- NCD PRO MODAL -->
<div class="ncdpro-modal">
	<div class="modal-content">
		<span class="close">&times;</span>
		<div class="modal-header">
			No Claim Discount Protector 
		</div>
		<div class="modal-body">
			<div class="modal-content-h1">
				We notice that your NCD is <span class="ncdpro-span"></span>%.
			</div>
			<div class="modal-content-h2">
				Please note that only Sompo allows NCD Protector add on if your NCD is <span class="ncdpro-span"></span>%.
			</div>
		</div>
		<div class="modal-new-footer">
			<div class="modal-button" data-dismiss="modal">
				OK! GOT IT!
			</div>
		</div>
	</div>
</div>
<!-- END NCD PRO MODAL -->
<script type="text/javascript">
	
	jQuery(".car-detail").on("click change keyup", function() {
		var isValid = true;
		var radioValid = false;
		jQuery('.car-detail').each(function() {
			if ( jQuery(this).val() === '' ){
				isValid = false;
			}
		});

		jQuery('.radio-label-long span').each(function() {
			if ( jQuery(this).hasClass("active") ){
				radioValid = true;
			}
		});

		if (isValid && radioValid && !jQuery(".car-accordion").hasClass("done")){
			jQuery(".car-accordion").addClass("done");
			jQuery( ".motor-accordion" ).not(".driver-accordion").each(function( index ) {
				jQuery(this).addClass("active");
				jQuery(this).next().removeClass("show");
				jQuery(this).next().addClass("noshow");
				jQuery(this).next().slideUp();
			});
			jQuery(".click-disable").removeClass("click-disable");
			jQuery(".driver-accordion").removeClass("active");
			jQuery(".driver-accordion").next().addClass("show");
			jQuery(".driver-accordion").next().removeClass("noshow");
			jQuery(".driver-accordion").next().slideDown();
		}
		
	});

	jQuery(".insurance-detail").on("click change keyup", function() {
		var isValid = true;
		var radioValid = false;
		var policyDate = true;
		
		jQuery('.insurance-detail').each(function() {
			if ( jQuery(this).val() === '' ){
				isValid = false;
			}
		});

		if ( jQuery('.policy-date input').datepicker('getDate') === null ){
				policyDate = false;
			}

		jQuery('.insurance-label span').each(function() {
			if ( jQuery(this).hasClass("active") ){
				radioValid = true;
			}
		});

		if (isValid && radioValid && policyDate && !jQuery(".insurance-accordion").hasClass("done")){
			jQuery(".insurance-accordion").addClass("done");
			jQuery( ".motor-accordion" ).not(".declaration-accordion").each(function( index ) {
				jQuery(this).addClass("active");
				jQuery(this).next().removeClass("show");
				jQuery(this).next().addClass("noshow");
				jQuery(this).next().slideUp();
			});
			jQuery(".declaration-accordion").removeClass("active");
			jQuery(".declaration-accordion").next().addClass("show");
			jQuery(".declaration-accordion").next().removeClass("noshow");
			jQuery(".declaration-accordion").next().slideDown();

		
		}
		
	});


	jQuery(".driver-detail").on("click change keyup", function() {
		var isValid = true;
		var birth = true;
		jQuery('.driver-detail').each(function() {
			if ( jQuery(this).val() == '' || jQuery(this).val() == null){
				isValid = false;
			}
			if ( jQuery('.driver-birth input').datepicker('getDate') === null ){
				birth = false;
			}
		});
		if (isValid && birth && !jQuery(".driver-accordion").hasClass("done")){
			jQuery(".driver-accordion").addClass("done");
			jQuery(".motor-form-input-wrapper.hidden").removeClass("hidden");
			jQuery( ".motor-accordion" ).not(".insurance-accordion").each(function( index ) {
				jQuery(this).addClass("active");
				jQuery(this).next().removeClass("show");
				jQuery(this).next().addClass("noshow");
				jQuery(this).next().slideUp();
			});
			jQuery(".insurance-accordion").removeClass("active");
			jQuery(".insurance-accordion").next().addClass("show");
			jQuery(".insurance-accordion").next().removeClass("noshow");
			jQuery(".insurance-accordion").next().slideDown();
			jQuery(".insurance-panel").css("max-height", "1000px");
		}
		
	});

	jQuery(".driver-birth input").datepicker({
			changeMonth: true,
			changeYear: true,
			showOn: "focus",
			dateFormat : "dd-M-yy",
			yearRange: "1920:1995",
			defaultDate: "01-JAN-1980",
			onChangeMonthYear:function(y, m, i){                     
				var d = i.selectedDay;
				jQuery(this).datepicker("setDate", new Date(y, m-1, d));
			},
			onSelect: function() {
				var isValid = true;
				jQuery('.driver-detail').each(function() {
					if ( jQuery(this).val() === '' ){
						isValid = false;
					}
				});
				if (isValid && !jQuery(".driver-accordion").hasClass("done")){
					jQuery(".driver-accordion").addClass("done");
					jQuery(".motor-form-input-wrapper.hidden").removeClass("hidden");
					jQuery( ".motor-accordion" ).not(".insurance-accordion").each(function( index ) {
						jQuery(this).addClass("active");
						jQuery(this).next().removeClass("show");
						jQuery(this).next().addClass("noshow");
						jQuery(this).next().slideUp();
					});
					jQuery(".insurance-accordion").removeClass("active");
					jQuery(".insurance-accordion").next().addClass("show");
					jQuery(".insurance-accordion").next().removeClass("noshow");
					jQuery(".insurance-accordion").next().slideDown();
					jQuery(".insurance-panel").css("max-height", "1000px");
				}
			}
	});

	jQuery(".policy-date input").datepicker({
			changeMonth: true,
			changeYear: true,
			showOn: "focus",
			dateFormat : "dd-M-yy",
			yearRange: "2019:2050",
			minDate: '+3d',
			onChangeMonthYear:function(y, m, i){                     
				var d = i.selectedDay;
				jQuery(this).datepicker("setDate", new Date(y, m-1, d));
			},
			onSelect: function() {
				jQuery("form#adminForm").validate().element("input[name=jform\\[policyStartDate\\]]");
			}
	});
	

	jQuery.LoadingOverlaySetup({
    color           : "rgba(0, 0, 0, 0.4)"
	});

	var customElement = jQuery('\
			<div class="overlay-container">\
			<div class="overlay-content-top">\
				<img class="overlay-content-img" src="<?php echo JURI::base(); ?>/images/websiteicon/company-logo.png"/>\
			</div>\
			<div class="overlay-content-middle">\
				<div class="overlay-content-header">\
					<span class="overlay-content-title">Thank you for your patience, we are consolidating your details. <br><br></span>\
					<span class="overlay-content-brand">Validating . . .</span>\
				</div>\
			</div>\
			<div class="overlay-content-bottom">\
				<div class="spinner">\
				<div></div>\
				<div></div>\
				<div></div>\
				<div></div>\
				<div></div>\
				</div>\
			</div>\
		</div>\
	');

	jQuery("#squaredFour3, #submit_button").click(function(e){
		e.preventDefault();
		if (jQuery("#squaredFour3").is(":checked"))
		{
			jQuery.LoadingOverlay("show", {
			image   : "",
			custom  : customElement
			});
			jQuery(".panel").slideDown();
			jQuery("#adminForm").submit();
			setTimeout(
			function() 
			{
				jQuery.LoadingOverlay("hide", {
				image   : "",
				custom  : customElement
				});
			}, 2000);
		} else {
			if (jQuery("#agreement").hasClass("show")) {
				jQuery( ".motor-accordion" ).not(".declaration-accordion").each(function( index ) {
					jQuery(this).addClass("active");
					jQuery(this).next().removeClass("show");
					jQuery(this).next().addClass("noshow");
					jQuery(this).next().slideUp();
				});
				jQuery(".declaration-accordion").removeClass("active");
				jQuery(".declaration-accordion").next().addClass("show");
				jQuery(".declaration-accordion").next().removeClass("noshow");
				jQuery(".declaration-accordion").next().slideDown();
				jQuery("#adminForm").submit();
			} else {
				alert("Please fill in required field");
			}
			
		}
		

		
	});
	jQuery('#carValue').on('keyup click change paste input', function (event) {
		jQuery(this).val(function (index, value) {
			if (value != "") {
				//return '$' + value.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
				var decimalCount;
				value.match(/\./g) === null ? decimalCount = 0 : decimalCount = value.match(/\./g);

				if (decimalCount.length > 1) {
					value = value.slice(0, -1);
				}

				var components = value.toString().split(".");
				if (components.length === 1)
					components[0] = value;
				components[0] = components[0].replace(/\D/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ',');
				if (components.length === 2) {
					components[1] = components[1].replace(/\D/g, '').replace(/^\d{3}$/, '');
				}

				if (components.join('.') != ''){
					var ValueSend = components.toString().replace(/,/g, "");
					jQuery('#carValueSend').val(ValueSend);
					return 'HK$ ' + components.join('.');
				} else {
					return '';
				}
					
			}
		});
		jQuery("form#adminForm").validate().element("input[name=jform\\[vehicleValue\\]]");
	});


	

	jQuery("select[name=jform\\[coverType\\]]").change(function(){
		var coverType = jQuery("select[name=jform\\[coverType\\]]").val();
		if (coverType == "01") {
			jQuery(".value-wrapper").slideDown();
		} else {
			jQuery(".value-wrapper").slideUp();
		}
	});
	
	var jFamily, jGroup, groupType, endDateWrapper, endDateInput, tripType;
	
	jQuery(document).ready(function () {
		var jForm = jQuery("form#adminForm");
		var jSelects = jForm.find("select");
		var jInputs = jForm.find("input");
		
		jForm.submit(function () {
// 			jForm.find("label.error").each(function () {
// 				var jThis = jQuery(this);
// 				var jContainer = jThis.closest("div.control-group");
// 				var jControl = jContainer.find("div.controls");
// 				jControl.find(".error-container").append(jThis.clone());
// 				//jThis.remove();
// 			});
			
// 			var errors = jForm.find("label.error");

// 			if (errors.length > 0) {
// 				var label = jForm.find("label.error:first");
// 				jQuery('html, body').animate({
// 					'scrollTop': label.offset().top - 400
// 				}, 300);

// 				this.preventDefault();
// 			}
		});

// 		jQuery.validator.setDefaults({
// 	        ignore: []
// 	    });
	});



	jQuery.validator.addMethod("validDriver", function validDriver(str, element) {
		if(jQuery("input[name=jform\\[validLicense\\]]:checked").val() == "Yes"){
	        return true;
	    }
	    else
	    {
	        return false;
	    }
	},
        "Applicant must have a valid license"
    );

	jQuery.validator.addMethod("validValue", function validValue(str, element) {
		var carValue = parseInt(jQuery("input[name=jform\\[vehicleValue\\]]").val());
		if(carValue >= 80000 && carValue <= 999999 && jQuery.isNumeric(carValue)){
			if(carValue < 150000){
				jQuery(".aigMin").show();
			} else {
				jQuery(".aigMin").hide();
			}
	        return true;
	    }
	    else
	    {
	        return false;
	    }
	},
        "Must be between HK$ 80,000 - 999,999"
    );

	jQuery.validator.addMethod("validCc", function validCc(str, element) {
		var carCc = parseInt(jQuery("input[name=jform\\[vehicleEngineCc\\]]").val());
		if(carCc >= 500 && carCc <= 10000 && jQuery.isNumeric(carCc)){
	        return true;
	    }
	    else
	    {
	        return false;
	    }
	},
        "Enter a valid vehicle CC value"
    );

	jQuery.validator.addMethod("validRegNum", function validRegNum(str, element) {
		var carReg = jQuery("input[name=jform\\[vehicleRegNo\\]]").val();
		var carAlp = carReg.replace(/\d+/g, '');
		var carNum = carReg.replace(/\D/g,'');
		var numLen = carNum.length;
		var alpLen = carAlp.length;
		var firstNum = carNum.charAt(0);
		var lastNum = jQuery.isNumeric(carReg.substr(carReg.length - 1));
		if(firstNum != "0" && numLen >= 1 && numLen <= 4 && alpLen <= 2 && lastNum == true){
	        return true;
	    }
	    else
	    {
	        return false;
	    }
	},
        "Enter a valid registration number"
    );

	jQuery.validator.addMethod("validPeriod", function validPeriod(str, element) {
		var one_day=1000*60*60*24;
		var months;
		var d = jQuery("input[name=jform\\[policyStartDate\\]]").val();
		var d1 = (new Date()).getTime();
		var d2 = new Date(d.replace(/-/g, " ")).getTime();
		var difference_ms = d2 - d1;
		
		if(Math.round(difference_ms/one_day) <= 90 && Math.round(difference_ms/one_day) >= 2){
	        return true;
	    }
	    else
	    {
	        return false;
	    }
	},
        "Policy Start Date must not exceed 3 months or less than 2 days"
    );

	jQuery.validator.addMethod("confirmDemerit", function confirmDemerit(str, element) {
		var demeritPoint = jQuery("#demerit-point").val();
		var meritCert = jQuery("input[name='jform[meritCert]']:checked").val();
		
		if(meritCert == "true" & demeritPoint != 0) {
			return false;
		}
		else {
			return true;
		}
	},
        "You are not eligible for the COM discount if you currently hold demerit points."
    );

	jQuery.validator.addMethod("confirmMerit", function confirmMerit(str, element) {
		var ncd = jQuery("input[name=jform\\[NCDPoints\\]]:checked").val();
		var meritCert = jQuery("input[name='jform[meritCert]']:checked").val();
		
		if(meritCert == "true" & ncd < 30) {
			return false;
		}
		else {
			return true;
		}
	},
        "Only eligible for NCD 30% and above"
    );

	jQuery.validator.addMethod("restrictCarYear", function restrictCarYear(str, element) {
		var currentYear = (new Date()).getFullYear();
		var diff = (currentYear - str);

		if(diff < 10) {
			return true;
		}
		else {
			return false;
		}
		
	},
        "Only available for car less than 10 years"
    );
	
	// validate signup form on keyup and submit
	var validator = jQuery("form#adminForm").validate({
		rules: {
			"jform[agreeDeclaration]": {
                required: true
            },
			"jform[offPeakCar]": {
                required: false
            },
			"jform[vehicleRegNo]": {
                required: true,
				validRegNum: true
            },
			"jform[vehicleValue]": {
                required: true,
				validValue: true
			},
			"jform[offencePoint]": {
                required: true,
			},
			"jform[highestOffencePoint]": {
                required: true,
			},
			"jform[nightParking]": {
                required: true,
			},
			"jform[chinaExtension]": {
                required: true,
			},
			"jform[coverType]": {
                required: true,
			},
			"jform[vehicleEngineCc]": {
                required: true,
				validCc: true
            },
			"jform[carBodyType]": {
                required: true,
            },
            "jform[companyRegistered]": {
                required: false
            },
			"jform[carMake]": {
                required: true
            },
			"jform[carModel]": {
                required: true
            },
			"jform[carMakeYear]": {
                required: true
            },
            "jform[carRegisterYear]": {
				required : false,
				//restrictCarYear: true
			},
			"jform[Gender][0]": {
                required: true
            },
			"jform[dateOfBirth][0]": {
                required: true
            },
            "jform[occupationType][0]": {
                required: true
            },
			"jform[Occupation][0]": {
                required: true
            },
			"jform[maritalStatus][0]": {
                required: true
            },
			"jform[yearsOfDrivingExp][0]": {
                required: true
            },
			"jform[NCDPoints]": {
                required: true
            },
			"jform[policyStartDate]": {
                required: true,
                validPeriod: true
            },
			"jform[claimsPast3Years]": {
                required: true
            },
            "jform[validLicense]" : {
            	required: true,
            	validDriver: true
            },
            "jform[driversTerm]" : {
            	required: true
            },
			"jform[workshop]": {
                required: false
            },
			"jform[agreePDC]": {
                required: true
            },
			"jform[los]": {
                required: false
            },
			"jform[ncdPro]": {
                required: false
            }
		},
		messages: {
			"jform[offPeakCar]": {
				required : "Please select this option"
			},
			"jform[vehicleRegNo]": {
				required : "Please fill in this field"
			},
			"jform[companyRegistered]": {
				required : "Please select this option"
	        },
			"jform[carMake]": {
				required : "Please select vehicle make"
			},
			"jform[carModel]": {
				required : "Please select vehicle model"
			},
			"jform[carMakeYear]": {
				required : "Please select vehicle make year"
			},
			"jform[carRegisterYear]": {
				required : "Please select vehicle register year"
			},
			"jform[Gender][0]": {
				required : "Please select gender"
			},
			"jform[dateOfBirth][0]": {
				required : "Please fill in date of birth"
			},
			"jform[agreePDC]": {
				required : "Please check on this box"
			},
			"jform[occupationType][0]": {
				required : "Please select occupation"
			},
			"jform[Occupation][0]": {
				required : "Please select occupation"
			},
			"jform[maritalStatus][0]": {
				required : "Please select marital status"
			},
			"jform[yearsOfDrivingExp][0]": {
				required : "Please select driving experience"
			},
			"jform[NCDPoints]": {
				required : "Please select your NCD"
			},
			"jform[policyStartDate]": {
				required : "Please fill in policy start date"
			},
			"jform[claimsPast3Years]": {
				required : "Please select no. of claims"
			},
            "jform[validLicense]" : {
            	required : "Please select this option"
            },
            "jform[driversTerm]" : {
            	required: "Please agree on the term"
            },
			"jform[workshop]": {
				required : "Please select this option"
			},
			"jform[los]": {
				required : "Please select this option"
			},
			"jform[ncdPro]": {
				required : "Please select this option"
			}
			
		}
	});

	jQuery("form#adminForm").on("submit", function(obj) {
		var dob = jQuery("[name^=jform\\[dateOfBirth\\]\\[0\\]]");
		var drivingExp = jQuery("[name^=jform\\[yearsOfDrivingExp\\]\\[0\\]]");

		if (jQuery('#squaredFour3').is(":checked"))
		{}
		else
	    {
	        jQuery('#agreement').animate({scrollTop: jQuery('#agreement')[0].scrollHeight}, 1000);
			return false;
	    }

		
		flag = validateField(dob);
		flag = validateField(drivingExp);
		flag = validateRules();
		
		return flag;
	});

	function validateRules() {
		var gender = jQuery("[name^=jform\\[Gender\\]]");
		var dob = jQuery("[name^=jform\\[dateOfBirth\\]\\[0\\]]");
		var carMakeYear = jQuery("input[name=jform\\[carMakeYear\\]]:checked").val();
		var policyStartDate = jQuery("input[name=jform\\[policyStartDate\\]]").val().replace(/-/g,'/');
		var NCDPoints = jQuery("input[name=jform\\[NCDPoints\\]]:checked").val();
		var currentInsurer = jQuery("select[name=jform\\[currentInsurer\\]]").val();
		var drivingExp = jQuery("[name^=jform\\[yearsOfDrivingExp\\]\\[0\\]]");
	
		if(NCDPoints > 0 && currentInsurer == "null") {
			showErrorMsg("Please specify your current insurance company since your NCD is " + NCDPoints + "%");

			jQuery('html, body').animate({ scrollTop: 0 }, 'slow');
			
			return false;
		}
		
		
		
		for (var i = 0, len = drivingExp.length; i < len; i++) {
			if(jQuery(drivingExp[i]).val() < 2) {
				showErrorMsg("Insured drivers should have more than 2 years of driving experience.");

				jQuery('html, body').animate({ scrollTop: 0 }, 'slow');
				
				return false;
			}
		}

		
		for (var i = 0, len = dob.length; i < len; i++) {
			var birthday = new Date((jQuery(dob[i]).val()).replace(/-/g,'/'));
	        var today = new Date();
	        var age = Math.floor((today-birthday) / (365.25 * 24 * 60 * 60 * 1000));
	        
			if(age < 25) {
				showErrorMsg("Applicant age should not be less than 25 yrs");

				jQuery('html, body').animate({ scrollTop: 0 }, 'slow');
				
				return false;
			}

			if(age > 69) {
				showErrorMsg("Applicant age should not be more than 69 yrs");

				jQuery('html, body').animate({ scrollTop: 0 }, 'slow');
				
				return false;
			}
		}
	}

	function showErrorMsg(msg) {
			jQuery("#system-message-container").html('\
					<div id="system-message">\
						<div class="alert alert-error">\
							<a class="close" data-dismiss="alert">&#215;</a>\
							<h4 class="alert-heading">Error</h4>\
							<div>\
								<p class="alert-message">' + msg + '</p>\
							</div>\
						</div>\
					</div>\
			');
	}
	
	function validateField(Obj) {
		var flag = "";
		var counter = 0;
		for (var i = 0, len = Obj.length; i < len; i++) {
			
			if(jQuery(Obj[i]).attr('type') == "radio") {
				if(jQuery(Obj[i]).prop("checked")) {
					flag = jQuery(Obj[i]).attr("name");
				}
				else {
					if(jQuery(Obj[i]).attr("name") == flag) {
						flag = "";
						counter = 0;
					}
					else {
						counter = jQuery(Obj[i]).attr("name").replace("jform[Gender][", "").replace("]", "");
					}
				}
			}
			else {
				if(jQuery(Obj[i]).val() == "" || jQuery(Obj[i]).val() == null) {					
					jQuery(".tab-pane").removeClass("active");
					jQuery("#driver-"+i).addClass("active");
					jQuery('.tab-wrapper').addClass('display-none');
					jQuery("#driver-panel-"+i).removeClass("display-none");
					
					return false;
				}
			}
		}

		if(jQuery(Obj[0]).attr('type') == "radio") {
			jQuery(".tab-pane").removeClass("active");
			jQuery("#driver-"+counter).addClass("active");
			jQuery('.tab-wrapper').addClass('display-none');
			jQuery("#driver-panel-"+counter).removeClass("display-none");

			return false;
		}
		return true;
	}
</script>
<script>
var acc = document.getElementsByClassName("motor-accordion");
var i;

for (i = 0; i < acc.length; i++) {
	acc[i].nextElementSibling.style.maxHeight = (acc[i].nextElementSibling.scrollHeight + 200) + "px";
  	acc[i].onclick = function() {
	if (jQuery(this).hasClass("click-disable")) {
		return false;
	}
	jQuery( ".motor-accordion" ).not(this).each(function( index ) {
		jQuery(this).addClass("active");
		jQuery(this).next().addClass("noshow");
		jQuery(this).next().removeClass("show");
		jQuery(this).next().slideUp();
	});
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
	jQuery(panel).toggleClass("show");
	jQuery(panel).toggleClass("noshow");
	

	if (jQuery(panel).hasClass("show") && !jQuery(panel).hasClass("noshow")){
		jQuery(".insurance-panel").css("max-height", "1000px");
		jQuery(panel).slideDown();
	} else {
		jQuery(this).addClass("active");
		jQuery(panel).slideUp();
	}

  }
}

jQuery(".motor-accordion").next().slideDown();
jQuery(".motor-accordion.active").next().hide();


function radioClicked(obj) {
		var otherRadios = document.getElementsByName(obj.getAttribute('name'));
		var j;
		
		if(jQuery(obj).parent().find('label[class^="radio-label"]').hasClass("radio-label-logo")) {
			for(j=0;j < otherRadios.length; j++) {
				jQuery(otherRadios[j]).parent().find('label[class^="radio-label"]').removeClass("active");
			}

			jQuery(obj).parent().find('label[class^="radio-label"]').toggleClass("active");
		}
		else {
			for(j=0;j < otherRadios.length; j++) {
				jQuery(otherRadios[j]).parent().find('span.radio-label-title').removeClass("active");
			}

			jQuery(obj).parent().find('span.radio-label-title').toggleClass("active");
		}
		
		
// 		if(obj.nextElementSibling.children[0].classList[0].indexOf("icon") !== -1) {
// 			for(j=0;j < otherRadios.length; j++) {
// 				otherRadios[j].nextElementSibling.classList.remove("active");
// 			}
			
			
// 		}
// 		else {
// 			for(j=0;j < otherRadios.length; j++) {
// 				otherRadios[j].nextElementSibling.children[0].classList.remove("active");
// 			}
			
// 			obj.nextElementSibling.children[0].classList.toggle("active");
// 		}
}

function resetInput(obj) {
	if(checkFlag) {
		jQuery("input[name='jform[ncdPro]'][value='Yes']").next().remove();
		
		jQuery("input[name='jform[ncdPro]']").next().children().removeClass("disable");
	}

	checkFlag = false;

	jQuery('label.error').remove();
	
	var radioInputs = jQuery(obj).parents().eq(4).find(':input[type=radio]');
	var textInputs = jQuery(obj).parents().eq(4).find(':input[type=text]');
	var selectInputs = jQuery(obj).parents().eq(4).find('select');
	var hidden = jQuery(obj).parents().eq(4).find(":input[name='jform[meritCert]']");
	
	if(selectInputs.length > 1) {
		for(var i=0; i<selectInputs.length; i++) {
			var firstOption = jQuery(selectInputs[i]).find("option:first-child").html();
			jQuery(selectInputs[i]).prop('selectedIndex',0);
			jQuery(selectInputs[i]).next().find(".select2-selection__rendered").html(firstOption);
		}
	}
	else {
		var firstOption = jQuery(selectInputs).find("option:first-child").html();
		jQuery(selectInputs).prop('selectedIndex',0);
		jQuery(selectInputs).next().find(".select2-selection__rendered").html(firstOption);
	}

	if(radioInputs.length > 1) {
		for(var i=0; i<radioInputs.length; i++) {
			jQuery(radioInputs[i]).prop('checked',false);
			jQuery(radioInputs[i]).next().removeClass("active");
			jQuery(radioInputs[i]).next().children('.radio-label-title').removeClass("active");
		}
	}
	else {
		jQuery(radioInputs).prop('checked',false);
		jQuery(radioInputs).next().removeClass("active");
		jQuery(radioInputs).next().children('.radio-label-title').removeClass("active");
	}

	if(textInputs.length > 1) {
		for(var i=0; i<textInputs.length; i++) {
			jQuery(textInputs[i]).val('');
		}
	}
	else {
		jQuery(textInputs).val('');
	}

	if(typeof hidden.val() != "undefined") {
		hidden.val("false");
	}
}

function resetAll() {
	if(checkFlag) {
		jQuery("input[name='jform[ncdPro]'][value='Yes']").next().remove();
		
		jQuery("input[name='jform[ncdPro]']").next().children().removeClass("disable");
	}

	checkFlag = false;

	jQuery('label.error').remove();
	
	var radioInputs = jQuery(".insurediy-form").find(':input[type=radio]');
	var textInputs = jQuery(".insurediy-form").find(':input[type=text]');
	var selectInputs = jQuery(".insurediy-form").find('select');

	if(selectInputs.length > 1) {
		for(var i=0; i<selectInputs.length; i++) {
			var firstOption = jQuery(selectInputs[i]).find("option:first-child").html();
			jQuery(selectInputs[i]).prop('selectedIndex',0);
			jQuery(selectInputs[i]).next().find(".select2-selection__rendered").html(firstOption);
		}
	}
	else {
		var firstOption = jQuery(selectInputs).find("option:first-child").html();
		jQuery(selectInputs).prop('selectedIndex',0);
		jQuery(selectInputs[i]).next().find(".select2-selection__rendered").html(firstOption);
	}

	if(radioInputs.length > 1) {
		for(var i=0; i<radioInputs.length; i++) {
			jQuery(radioInputs[i]).prop('checked',false);
			jQuery(radioInputs[i]).next().removeClass("active");
			jQuery(radioInputs[i]).next().children('.radio-label-title').removeClass("active");
		}
	}
	else {
		jQuery(radioInputs).prop('checked',false);
		jQuery(radioInputs).next().removeClass("active");
		jQuery(radioInputs).next().children('.radio-label-title').removeClass("active");
	}

	if(textInputs.length > 1) {
		for(var i=0; i<textInputs.length; i++) {
			jQuery(textInputs[i]).val('');
		}
	}
	else {
		jQuery(textInputs).val('');
	}

	jQuery("input[name='jform[meritCert]']").val("false");
}

function nextButton(obj) {
	jQuery(obj).parents().eq(2).removeClass("display-block");
	jQuery(obj).parents().eq(2).addClass("display-none");
	jQuery(obj).parents().eq(2).next().removeClass("display-none");
	jQuery(obj).parents().eq(2).next().addClass("display-block");
}

function backButton(obj) {
	jQuery(obj).parents().eq(2).prev().removeClass("display-none");
	jQuery(obj).parents().eq(2).prev().addClass("display-block");
	jQuery(obj).parents().eq(2).removeClass("display-block");
	jQuery(obj).parents().eq(2).addClass("display-none");
}

jQuery(document).ready(function(){
	jQuery('.custom-select').select2({width: '255px'});
});



function changeTab(obj) {
	jQuery(".tab-pane").removeClass("active");
	jQuery(obj).addClass("active");
	jQuery('.tab-wrapper').addClass('display-none');
	jQuery("#"+jQuery(obj).attr("data-id")).removeClass("display-none");
}

function setCertMerit(value) {
	jQuery("input[name='jform[meritCert]']").val(value);
}

function letterToNumbers(string) {
    string = string.toUpperCase();
    var letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ', sum = 0, i;
    for (i = 0; i < string.length; i++) {
        sum += Math.pow(letters.length, i) * (letters.indexOf(string.substr(((i + 1) * -1), 1)) + 1);
    }
    return sum;
}

jQuery(window).on('click', function(e) {
	if(e.target) {
		if(e.target.className != "selecionado_opcion" && typeof e.target.className != "object") {
			//open_select(jQuery(this).find('p.selecionado_opcion'));
			var selectArr = jQuery('div.select_mate');
			for(var i=0; i<selectArr.length; i++) {
				if(jQuery(selectArr[i]).attr('data-selec-open') == 'true') {
					jQuery(selectArr[i]).find('p.selecionado_opcion').click();
				}
			}	
		}
		
		if(e.target.className == "close" || e.target.className == "modal-button") {
			jQuery( ".merit-cert-modal" ).css("display", "none");
			jQuery( ".old-car-modal" ).css("display", "none");
			jQuery( ".ncdpro-modal" ).css("display", "none");
		}
	}
});

//Block non-numeric chars.
jQuery('.calendar-input').on('keypress', function (event) {
    return (((event.which > 47) && (event.which < 58)) || (event.which == 13) || (event.which == 45));
});

var checkFlag = <?php if(isset($data["ncdPro"])){ if($data["NCDPoints"] >= 30) echo "false"; else echo "true"; } else { echo "false"; } ?>;

jQuery("input[name='jform[NCDPoints]']").change(function(){
	if(jQuery(this).val() == 0) {
		jQuery( ".otherNCDWrapper" ).css("display", "block");
	}

	if(jQuery(this).val() > 0) {
		
		jQuery( ".otherNCDWrapper" ).css("display", "none");
	}

	if(jQuery(this).val() < 30) {		
		if(!checkFlag) {
			jQuery("input[name='jform[ncdPro]']").next().children().removeClass("active");
			jQuery("input[name='jform[ncdPro]']").next().children().addClass("disable");
			jQuery("input[name='jform[ncdPro]']").prop('checked',false);
			jQuery("input[name='jform[ncdPro]'][value='No']").prop('checked',true);

			jQuery("input[name='jform[ncdPro]'][value='Yes']").after('<label for="jform[ncdPro]" class="error">Only for NCD above 30% </label>');

			checkFlag = true;
		}
	}
	else {
		if(checkFlag) {
			jQuery("input[name='jform[ncdPro]'][value='Yes']").next().remove();
			
			jQuery("input[name='jform[ncdPro]']").next().children().removeClass("disable");
			jQuery("input[name='jform[ncdPro]']").prop('checked',false);

			checkFlag = false;
		}		
	}
});

jQuery("select[name='jform[claimsPast3Years]']").change(function(){
	if(jQuery(this).val() == 0) {
		var firstOption = jQuery("select[name='jform[claimAmount]']").find("option:first-child").html();
		jQuery("select[name='jform[claimAmount]']").prop('selectedIndex',0);
		jQuery("select[name='jform[claimAmount]']").next().find(".select2-selection__rendered").html(firstOption);
		
		jQuery( ".claimAmtWrapper" ).css("display", "none");
	}

	if(jQuery(this).val() > 0) {
		jQuery( ".claimAmtWrapper" ).css("display", "block");
	}
});

jQuery("select[name='jform\[carMake\]']").on('change', function(obj){
	jQuery.ajax({
		type: "POST",
		url: "<?php echo JURI::root().'index.php?option=com_insurediymotor&task=ajax.getModelList&format=json'; ?>",
		data:'carMake='+jQuery("select[name='jform\[carMake\]']").val(),
		dataType: "json",
		beforeSend: function() {
			jQuery(".car-model").LoadingOverlay("show");
		},
		success: function(result){
			jQuery("select[name='jform\[carModel\]']").html('<option value="" disabled selected>Select Car Model</option>');
			result.forEach(function(item, index){
				jQuery("select[name='jform\[carModel\]']").append(
						'<option value="'+item.model_code+'">'+item.model_name+'</option>'
				);
			});

			jQuery(".car-model").LoadingOverlay("hide");
		},
		error: function(xhr, textStatus, errorThrown){
			jQuery(".car-model").LoadingOverlay("hide");
			alert('request failed : '+errorThrown);
		}
	});
});

jQuery("select[name='jform\[Industry\]\[0\]']").on('change', function(obj){
	jQuery.ajax({
		type: "POST",
		url: "<?php echo JURI::root().'index.php?option=com_insurediymotor&task=ajax.getJobList&format=json'; ?>",
		data:'jobIndustry='+jQuery("select[name='jform\[Industry\]\[0\]']").val(),
		dataType: "json",
		beforeSend: function() {
			jQuery(".job-position").LoadingOverlay("show");
		},
		success: function(result){
			jQuery("select[name='jform\[Occupation\]\[0\]']").html('<option value="" disabled selected>Select Job Position</option>');
			result.forEach(function(item, index){
				jQuery("select[name='jform\[Occupation\]\[0\]']").append(
						'<option value="'+item.occ_code+'">'+item.occ_name+'</option>'
				);
			});

			jQuery(".job-position").LoadingOverlay("hide");
		},
		error: function(xhr, textStatus, errorThrown){
			jQuery(".job-position").LoadingOverlay("hide");
			alert('request failed : '+errorThrown);
		}
	});
});
jQuery(document).on('change',"select[name='jform\[Industry\]\[1\]']", function(obj){
	jQuery.ajax({
		type: "POST",
		url: "<?php echo JURI::root().'index.php?option=com_insurediymotor&task=ajax.getJobList&format=json'; ?>",
		data:'jobIndustry='+jQuery("select[name='jform\[Industry\]\[1\]']").val(),
		dataType: "json",
		beforeSend: function() {
			jQuery(".job-position").LoadingOverlay("show");
		},
		success: function(result){
			jQuery("select[name='jform\[Occupation\]\[1\]']").html('<option value="" disabled selected>Select Job Position</option>');
			result.forEach(function(item, index){
				jQuery("select[name='jform\[Occupation\]\[1\]']").append(
						'<option value="'+item.occ_code+'">'+item.occ_name+'</option>'
				);
			});

			jQuery(".job-position").LoadingOverlay("hide");
		},
		error: function(xhr, textStatus, errorThrown){
			jQuery(".job-position").LoadingOverlay("hide");
			alert('request failed : '+errorThrown);
		}
	});
});
jQuery(document).on('change',"select[name='jform\[Industry\]\[2\]']", function(obj){
	jQuery.ajax({
		type: "POST",
		url: "<?php echo JURI::root().'index.php?option=com_insurediymotor&task=ajax.getJobList&format=json'; ?>",
		data:'jobIndustry='+jQuery("select[name='jform\[Industry\]\[2\]']").val(),
		dataType: "json",
		beforeSend: function() {
			jQuery(".job-position").LoadingOverlay("show");
		},
		success: function(result){
			jQuery("select[name='jform\[Occupation\]\[2\]']").html('<option value="" disabled selected>Select Job Position</option>');
			result.forEach(function(item, index){
				jQuery("select[name='jform\[Occupation\]\[2\]']").append(
						'<option value="'+item.occ_code+'">'+item.occ_name+'</option>'
				);
			});

			jQuery(".job-position").LoadingOverlay("hide");
		},
		error: function(xhr, textStatus, errorThrown){
			jQuery(".job-position").LoadingOverlay("hide");
			alert('request failed : '+errorThrown);
		}
	});
});
jQuery(document).on('change',"select[name='jform\[Industry\]\[3\]']", function(obj){
	jQuery.ajax({
		type: "POST",
		url: "<?php echo JURI::root().'index.php?option=com_insurediymotor&task=ajax.getJobList&format=json'; ?>",
		data:'jobIndustry='+jQuery("select[name='jform\[Industry\]\[3\]']").val(),
		dataType: "json",
		beforeSend: function() {
			jQuery(".job-position").LoadingOverlay("show");
		},
		success: function(result){
			jQuery("select[name='jform\[Occupation\]\[3\]']").html('<option value="" disabled selected>Select Job Position</option>');
			result.forEach(function(item, index){
				jQuery("select[name='jform\[Occupation\]\[3\]']").append(
						'<option value="'+item.occ_code+'">'+item.occ_name+'</option>'
				);
			});

			jQuery(".job-position").LoadingOverlay("hide");
		},
		error: function(xhr, textStatus, errorThrown){
			jQuery(".job-position").LoadingOverlay("hide");
			alert('request failed : '+errorThrown);
		}
	});
});
jQuery(document).on('change',"select[name='jform\[Industry\]\[4\]']", function(obj){
	jQuery.ajax({
		type: "POST",
		url: "<?php echo JURI::root().'index.php?option=com_insurediymotor&task=ajax.getJobList&format=json'; ?>",
		data:'jobIndustry='+jQuery("select[name='jform\[Industry\]\[4\]']").val(),
		dataType: "json",
		beforeSend: function() {
			jQuery(".job-position").LoadingOverlay("show");
		},
		success: function(result){
			jQuery("select[name='jform\[Occupation\]\[4\]']").html('<option value="" disabled selected>Select Job Position</option>');
			result.forEach(function(item, index){
				jQuery("select[name='jform\[Occupation\]\[4\]']").append(
						'<option value="'+item.occ_code+'">'+item.occ_name+'</option>'
				);
			});

			jQuery(".job-position").LoadingOverlay("hide");
		},
		error: function(xhr, textStatus, errorThrown){
			jQuery(".job-position").LoadingOverlay("hide");
			alert('request failed : '+errorThrown);
		}
	});
});
jQuery(document).on('change',"select[name='jform\[Industry\]\[5\]']", function(obj){
	jQuery.ajax({
		type: "POST",
		url: "<?php echo JURI::root().'index.php?option=com_insurediymotor&task=ajax.getJobList&format=json'; ?>",
		data:'jobIndustry='+jQuery("select[name='jform\[Industry\]\[5\]']").val(),
		dataType: "json",
		beforeSend: function() {
			jQuery(".job-position").LoadingOverlay("show");
		},
		success: function(result){
			jQuery("select[name='jform\[Occupation\]\[5\]']").html('<option value="" disabled selected>Select Job Position</option>');
			result.forEach(function(item, index){
				jQuery("select[name='jform\[Occupation\]\[5\]']").append(
						'<option value="'+item.occ_code+'">'+item.occ_name+'</option>'
				);
			});

			jQuery(".job-position").LoadingOverlay("hide");
		},
		error: function(xhr, textStatus, errorThrown){
			jQuery(".job-position").LoadingOverlay("hide");
			alert('request failed : '+errorThrown);
		}
	});
});
jQuery("input[name='jform[carRegisterYear]']").change(function(){
	var year = jQuery(this).val();
	var today = new Date();	
	var diff = diff_years(today.getFullYear(), new Date(year,1,1).getFullYear());

	if(diff > 9) {
		jQuery(".old-car-modal").css("display", "block");
		jQuery(".cover-type").html("Comprehensive / Third Party, Fire & Theft");
		jQuery(".cover-type").css("margin-right", "10px");
		jQuery(".car-age-span").html(diff);
	}
	else {
		jQuery(".cover-type").html("Comprehensive");
		jQuery(".cover-type").css("margin-right", "20%");
	}
});

jQuery("input[name='jform[ncdPro]']").change(function(){
	var input = jQuery("input[name=jform\\[NCDPoints\\]]:checked");
	var ncd = input.val();
	
	if(ncd != 50 && ncd >= 30 && jQuery(this).val() == "Yes") {
		jQuery(".ncdpro-span").html(ncd);
		jQuery(".ncdpro-modal").css("display", "block");
	}
});

jQuery("input[name='jform[vehicleEngineCc]']").focus(function(){
	jQuery(".select_mate").css("background", "transparent");
	jQuery(".cc-pointer").fadeIn();
});

jQuery("input[name='jform[vehicleEngineCc]']").focusout(function(){
	jQuery(".cc-pointer").fadeOut();
	jQuery(".select_mate").css("background", "#fff");
});

function diff_years(dt2, dt1) 
{
 return dt2-dt1;  
}
</script>
