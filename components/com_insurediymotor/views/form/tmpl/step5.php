<?php
	/**
		* @package     Joomla.Site
		* @subpackage  com_insurediymotor
		*
		* @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
		* @license     GNU General Public License version 2 or later; see LICENSE.txt
	*/
	defined('_JEXEC') or die;
	JHtml::_('formbehavior.chosen', '.insurediy-form-content select');
	JHtml::_('behavior.keepalive');
	JHtml::_('script', 'system/jquery.validate.js', false, true);
	JHtml::_('script', 'system/loadingoverlay.min.js', false, true);
	
	$params = $this->state->get('params');
	$currency = $this->currency;
	//$paymentData = & $this->paymentData;
	$session = JFactory::getSession();

	$data = $this->item;
	$payment = $this->paymentData;
	$motor = $this->makeModel;
	$encode = json_encode($data);
	$bodyType = $this->bodyType;
	$occ = $this->occupationParent;

	$regYear = date($data["carRegisterYear"]);
	$now = date("Y");
	
	$yearDiff = $now - $regYear;
	
	$premium_cost = $data["plan_premium"];
	$premium_cost = (number_format($premium_cost, 2, '.', ''));

	$checkPromoCode = false;
	$promo_code = $data["promoCode"];
	$premium_before_discount = $premium_cost;

	$originalHash = $payment["secureHash"];
	
	if($promo_code != '' && $data["coverType"] == "01") {
		$promo_type = 'motorch';
		$promocodeResult = InsurediypromocodesHelper::checkPromocode($promo_code, $promo_type);
		$checkCode = json_decode($promocodeResult, true);
		if($checkCode['success'] == 1){
			$discountCost = InsurediypromocodesHelper::calculateDiscountCost($promo_code, $premium_cost);
			
			$checkPromoCode = true;
			
			$premium_cost -= $discountCost;

			//updating asiapay amount to create HASH
			$app = JFactory::getApplication();
			$params = $app->getParams();
			//$request_id = InsureDIYMotorHelper::guid();
			//$newPaymentRef = InsureDIYMotorHelper::updatePaymentRef($request_id, $data["quotationId"]);
			$merchant_account_id = $params->get('merchantId', "88117268");
			$transaction_type = 'N';
			$discountedHash = InsureDIYMotorHelper::generatePaymentSecureHash($params->get('merchantId'), $payment["request_id"], $params->get('currCode', '344'), $premium_cost, $transaction_type, $params->get('hashCode'));
			// finish updating
		}
	} elseif ($promo_code != '' && $data["coverType"] == "02") {
		$promo_type = 'motortp';
		$promocodeResult = InsurediypromocodesHelper::checkPromocode($promo_code, $promo_type);
		$checkCode = json_decode($promocodeResult, true);
		if($checkCode['success'] == 1){
			$discountCost = InsurediypromocodesHelper::calculateDiscountCost($promo_code, $premium_cost);
			
			$checkPromoCode = true;
			
			$premium_cost -= $discountCost;

			//updating asiapay amount to create HASH
			$app = JFactory::getApplication();
			$params = $app->getParams();
			//$request_id = InsureDIYMotorHelper::guid();
			//$newPaymentRef = InsureDIYMotorHelper::updatePaymentRef($request_id, $data["quotationId"]);
			$merchant_account_id = $params->get('merchantId', "88117268");
			$transaction_type = 'N';
			$discountedHash = InsureDIYMotorHelper::generatePaymentSecureHash($params->get('merchantId'), $payment["request_id"], $params->get('currCode', '344'), $premium_cost, $transaction_type, $params->get('hashCode'));
			// finish updating
		}
	}
	
	
?>

<script type="text/javascript">
	function submitPaymentForm() {
		var params = [
		{'name': '<?php echo $session->getFormToken(); ?>', 'value': "1"},
		];
		jQuery.ajax({
			data: params,
			url: 'index.php?option=com_insurediymotor&task=form.savebeforepayment',
			dataType: 'json',
			type: 'post',
			beforeSend: function () {
			},
			success: function (json) {
				if (json['success']) {
					jQuery("#paymentForm").submit();
				}
			}
		});
	}
	//console.log('<?php //echo json_encode($this->paymentData); ?>//');

</script>


<div class="insurediy-form bb-form montserat-font">
	<div class="motor-header-top-wrapper">
		<?php echo InsureDIYMotorHelper::renderHeader('icon-travel', JText::_('COM_INSUREDIYMOTOR_PAGE_HEADING'), 5); ?>
	</div>
	<div class="edit<?php echo $this->pageclass_sfx; ?> insurediy-motor-form-content">
		<?php echo MyHelper::renderDefaultMessage(); ?>
		<div class="motor-form-input-wrapper">
			<div class="motor-accordion">APPLICATION SUMMARY</div>
			<div class="panel">
				<div class="center-panel">
					<table class="summary-table">
						<tr class="border-bottom">
							<th class="table-header">Provider</th>
							<th class="table-header border-left border-right">Description</th>
							<th class="table-header">Total Premium</th>
						</tr>
						<tr class="border-bottom">
							<td class="partner-title">
								<?php echo strtoupper($data["partnerId"]); ?>
							</td>
							<td class="border-left border-right content-column" style="vertical-align: top;">
								<ul class="summary-desc-list">
									<li class="desc-list">
										<b><?php echo $data["plan_name"]; ?></b>
									</li>
									<li class="desc-list">
										<?php
											if ($data["coverType"] == "01") {
												$cover = "COMPREHENSIVE";
											} else {
												$cover = "THIRD PARTY";
											}

										?>
										Cover Type : <span style="color:#5197d5;"><?php echo $cover; ?></span>
									</li>
									<li class="desc-list">
										<?php
											if ($data["coverType"] == "01") {
												$promoWord = 'Use code <span style="color:#fc5b6c;font-weight:bold;">CAR200</span> to get HK$200 off!';
											} else {
												$promoWord = 'Use code <span style="color:#fc5b6c;font-weight:bold;">CAR100</span> to get HK$100 off!';
											}

										?>
										<?php echo $promoWord; ?>
									</li>
									<li class="desc-list">
										Policy Start Date : <span class="font-bold"><?php echo $data["policyStartDate"]; ?></span>
									</li>
									
									<?php if($checkPromoCode): ?>
										<li class="desc-list summary-desc-promo-item">
											Promo Code Discount: <b>- <?php echo $currency; ?> <?php echo number_format($discountCost, 2) ?></b>
										</li>
									<?php endif; ?>
								</ul>
							</td>
							<td class="table-header font-bold">
								<ul class="summary-subtotal-list">
									<li class="desc-list"><?php echo $currency.number_format($data["plan_premium"], 2, '.', ','); ?></li>
									<li class="desc-list">&nbsp;</li>
									<li class="desc-list">&nbsp;</li>
									<?php if($data["partnerId"] == "hla"): ?>
										<li class="desc-list">&nbsp;</li>
									<?php endif; ?>
									<?php if($checkPromoCode): ?>
										<li class="desc-list summary-subtotal-promo-item">
											-<?php echo $currency; ?> <?php echo number_format($discountCost, 2) ?>
										</li>
									<?php endif; ?>
								</ul>
							</td>
						</tr>
						<tr>
							<td></td>
							<td class="border-left border-right table-header font-bold summary-total-amount-title" style="text-align: right">
							<?php if($data["selectedPartner"] == "sompo" && $checkPromoCode): ?>
								Total [Before promo code discount]
							<?php else: ?>
								Total
							<?php endif; ?>
							</td>
							<td class="table-header font-bold summary-total-amount"><?php 
								if($data["selectedPartner"] == "sompo" && $checkPromoCode) {
									echo $currency . number_format($premium_before_discount, 2, '.', ',');
								}
								else {
									echo $currency . number_format($premium_cost, 2, '.', ',');
								}
								
							?></td>
						</tr>
					</table>
					<div class="center-container">
						<div class="promo-code-container">
							<div class="promobox1">
								<form action="" method="post" id="jform_apply_promo_code" class="form-validate form-vertical">
									<img src="images/promo.png">
									<input name="jform[promo_code]" id="jform_promo_code" type="text" value="<?php echo $promo_code; ?>" placeholder="<?php echo JText::_("SOCIAL_PROMO_CODE_TITLE"); ?>" />
									<input type="submit" class="idd_blackbutton2 promo_button" value="<?php if($promo_code != ""){ echo "Cancel"; }else{ echo "Apply"; } ?>" />
								</form>
								<div class="error-container promo_code_error" <?php if($data["selectedPartner"] != "sompo" && $checkPromoCode): ?>style="display:none;"<?php endif; ?>><label for="jform_promo_code" class="" aria-invalid="false"><?php if($data["selectedPartner"] == "sompo" && $checkPromoCode): ?>Promo Code Applied.<br>We will contact you to process the discount.<br>Premium payable today is before discount<?php endif; ?></label></div>
								<div class="promo-no-login" style="color:red;display:none;margin-top:15px;"><a class="loginpopup" style="color:red;text-decoration:underline;" href="#">Login</a>/<a  class="regpopup" style="color:red;text-decoration:underline;display:inline;" href="#">Register</a> to use promo code</div>
								<div class="promo-no-login no-thanks" style="color:red;text-decoration:underline;cursor:pointer;display:none;">No thanks I will pay full amount</div>
							</div>
							<!--<img class="promo-code-accept" src="images/websiteicon/accept.png" />-->
							<!-- <img class="promo-code-question" src="images/websiteicon/question-mark.png" />-->
						</div>
						<div class="small-wordings">
							The amount shown above is the total premium payable for your policy.
						</div>
						<div class="bold-wordings">
							NOTE: Premium and excess amount may be subject to changes after review by the insurer. Addtional excess may apply to young/inexperienced/elderly drivers (if any). 
							All private car policyholders are responsible for an unnamed driver excess.
						</div>
						<div class="standard-wordings">
							On verification with your previous insurer and if NCD does not tally with the advice, the insurer shall proceed to recover NCD either via additional premium or via endorsement and shorten the period of insurance concurrently.
							Please note that NCD Protector (if any) is non-transferrable to another insurer. The NCD Protector will not necessarily protect you against non-renewal or cancellation of 
							your policy by your insurer. 
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="motor-form-input-wrapper">
			<div class="motor-accordion">ORDER SUMMARY</div>
			<div class="panel">
				<div class="panel-title">Car Details</div>
				<div class="panel-left" style="margin:0;">
					<div class="input-wrapper">
						<div class="field-title">Type of Body</div>
						<div class="field-value"><?php echo strtoupper($bodyType[$data["carBodyType"]]); ?></div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Cubic Capacity</div>
						<div class="field-value"><?php echo $data["vehicleEngineCc"]; ?></div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Engine No.</div>
						<div class="field-value"><?php echo $data["vehicleEngineNo"]; ?></div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Chassis No.</div>
						<div class="field-value"><?php echo $data["vehicleChassisNo"]; ?></div>
					</div>
					
					
				</div>
				<div class="panel-right">
					<div class="input-wrapper">
						<div class="field-title">Night Parking District</div>
						<div class="field-value"><?php echo $data["nightParking"]; ?></div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Vehicle Registration No.</div>
						<div class="field-value"><?php echo $data["vehicleRegNo"]; ?></div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Seating Capacity</div>
						<div class="field-value"><?php if(isset($motor->passenger)) {echo $motor->passenger;} else { echo "Not Applicable"; }?></div>
					</div>
					<div class="input-wrapper">
						<div class="field-title">Financial Loan Company</div>
						<div class="field-value"><?php echo $data["loanCompany"]; ?></div>
					</div>
				</div>
				<div class="panel-title" style="display: inline-block; margin: 30px 0px 20px 30px; width: 100%;">Driver Details</div>
				<div class="tab-header-container" style="width:92%; margin: 5px 0px 10px 30px;">
						<?php for($i=1; $i<= count($data["Gender"]); $i++):?>
							<?php if($i == 1):?>
								<div class="tab-pane active"  onClick="changeTab(this);" id="driver-0" data-id="driver-panel-0">
									<img class="tab-pane-image" src="images/websiteicon/driver-logo.png" />
									<span class="tab-pane-title">MAIN DRIVER</span>
								</div>
							<?php else: ?>
								<div class="tab-pane"  onClick="changeTab(this);" id="driver-0" data-id="driver-panel-<?php echo $i-1; ?>">
									<img class="tab-pane-image" src="images/websiteicon/driver-logo.png" />
									<span class="tab-pane-title">DRIVER <?php echo $i; ?></span>
								</div>
							<?php  endif; ?>
						<?php endfor; ?>
					</div>
					<div class="tab-content-container" style="margin-bottom:20px;">
						<?php for($i=0; $i< count($data["Gender"]); $i++):?>
								<div class="tab-wrapper <?php if($i != 0) echo "display-none"; ?>" id="driver-panel-<?php echo $i; ?>" style="margin-left: 10px;">
									<div class="panel-left" style="margin:0;padding:0;">
										<div class="input-wrapper">
											<div class="field-title">Gender</div>
											<div class="field-value"><?php echo $data["Gender"][$i]; ?></div>
										</div>
										<div class="input-wrapper">
											<div class="field-title">First Name</div>
											<div class="field-value"><?php echo $data["firstName"][$i]; ?></div>
										</div>
										<div class="input-wrapper">
											<div class="field-title">Last Name</div>
											<div class="field-value"><?php echo $data["lastName"][$i]; ?></div>
										</div>
										<div class="input-wrapper">
											<div class="field-title">Nationality</div>
											<div class="field-value"><?php echo $data["driverNationality"][$i]; ?></div>
										</div>
										<div class="input-wrapper">
											<div class="field-title">Driving License No.</div>
											<div class="field-value"><?php echo $data["validLicense"][$i]; ?></div>
										</div>
										<div class="input-wrapper">
											<div class="field-title">Date of Passing Driving Test</div>
											<div class="field-value"><?php echo $data["drivingTest"][$i]; ?></div>
										</div>
										<?php if($i == 0): ?>
										<div class="input-wrapper">
											<div class="field-title">Total driving offence points in the past 3 years</div>
											<div class="field-value"><?php echo $data["offencePoint"][$i]; ?></div>
										</div>
										<div class="input-wrapper">
											<div class="field-title">Highest driving offence points in the past 3 years</div>
											<div class="field-value"><?php echo $data["highestOffencePoint"][$i]; ?></div>
										</div>
										<?php endif; ?>
									</div>
									<div class="panel-right" style="margin:0;padding-left: 15px;">
										<div class="input-wrapper">
											<div class="field-title">Date of Birth</div>
											<div class="field-value"><?php echo $data["dateOfBirth"][$i]; ?></div>
										</div>
										<div class="input-wrapper">
											<div class="field-title">HKID</div>
											<div class="field-value"><?php echo $data["Nric"][$i]; ?></div>
										</div>
										<div class="input-wrapper">
											<?php 
											$driverOcc = "";
											foreach ($occ as $occu) {
												if ($data["Occupation"][$i] == $occu->id) {
													$driverOcc = $occu->occ_name;
												}
											} 
											?>
											<div class="field-title">Occupation</div>
											<div class="field-value"><?php echo $driverOcc; ?></div>
										</div>
										<div class="input-wrapper">
											<div class="field-title">Marital Status</div>
											<div class="field-value"><?php echo $data["maritalStatus"][$i]; ?></div>
										</div>
										<?php if($i == 0): ?>
										<div class="input-wrapper">
											<div class="field-title">Mobile No</div>
											<div class="field-value"><?php echo $data["mobileNo"]; ?></div>
										</div>
										<div class="input-wrapper">
											<div class="field-title">Address Line 1</div>
											<div class="field-value"><?php echo $data["addressOne"]; ?></div>
										</div>
										<div class="input-wrapper">
											<div class="field-title">Address Line 2</div>
											<div class="field-value"><?php echo $data["addressTwo"]; ?></div>
										</div>
										<div class="input-wrapper">
											<div class="field-title">Country</div>
											<div class="field-value">
											<?php 
													$countryList = $this->form->getInput('country_list');
													
													foreach($countryList as $country) {
														foreach($country as $key => $val) {
															if(isset($data["driverNationality"][$i])){ 
																if($data["driverNationality"][$i]== $key) {
 			
																	echo $val;
 																}
 																else {

 																}
 															}
 															else {

 															}
														}
														
													}
												?>
											</div>
										</div>
										<?php endif; ?>
									</div>
								</div>
						<?php endfor; ?>
						
						
					</div>
			</div>
		</div>
		<form method="post" action="<?php echo $payment['target_url']; ?>" id="adminForm" name="adminForm">
		<div class="motor-form-input-wrapper">
			<div class="motor-accordion">IMPORTANT NOTICE & POLICYHOLDER DECLARATIONS</div>
			<div class="panel">
				<div class="panel-top">
					<div class="panel-declaration">
						Important notice:<br><br>
						<ul>
						<li>My existing car insurance will be expired within 3 months’ time;</li>
						<li>I agree not to lend my car to a person to drive who is under 25 years of age or a person who has not held for a period of 2 years a driving license.</li>
						<li>I and all named drivers have not suffered from any physical or mental infirmity that may affect my/their ability to drive.</li>
						<li>I understand that any incorrect information provided may invalidate the quote and the insurance.</li>
						<li>I/We clearly understand this declaration shall be incorporated in and taken as the basis of a proposed contract.</li>
						</ul>
						<br>I / We hereby declare and agree that:<br><br>
						<ul>
							<li>No information or representation made or given by or to any person shall be binding on
						the Company unless it is in writing and is presented and approved by the Company.</li>
							<li>All written information, whether or not written by my own hand, submitted by me / us in this
						application form and in the Company issued questionnaires or other documents submitted by
						me / us in connection with this application	are true, complete and correct. In respect of any information
						or answers that I / we did not provide personally, I / we have checked their contents to ensure
						that they are true, correct and complete. I / We understand that the Company, believing them
						to be such, will rely and act on them, otherwise any policy issued hereunder may be void.</li>
							<li>All information and documents provided by me / us (as defined under (b)) together with the
						relevant policy issued shall constitute the entire contract between myself / ourselves and the
						Company.</li>
						</ul>
								
					</div>
				</div>
				<div class="panel-bottom">
					<div class="input-wrapper-block-top">
						<div class="input-panel-left">
							<div class="squaredFour">
								<input type="checkbox" id="squaredFour3" name="jform[agreePolicyDeclaration]" value="true" required/>
								<label for="squaredFour3"></label>
							</div>
						</div>
						<div class="input-panel-right" style="width:auto;">
							<div class="input-h2">
								I / We have read, understood and accepted the above statements which apply to all person covered under this policy.
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php if($data["selectedPartner"] == "pingan" || $data["selectedPartner"] == "aw" || $data["selectedPartner"] == "aig"): ?>
			<input type="hidden" name="orderRef" value="<?php echo $payment['request_id']; ?>">
			<input type="hidden" name="merchantId" value="<?php echo $payment['merchant_account_id']; ?>">
			<input type="hidden" name="payType" value="<?php echo $payment['transaction_type']; ?>">
			<input type="hidden" name="amount" value="<?php echo $premium_cost; ?>">
			<input type="hidden" name="currCode" value="<?php echo $payment['request_amount_currency']; ?>">
			<input type="hidden" name="mpsMode" value="<?php echo $payment['mps_mode']; ?>" >
			<input type="hidden" name="successUrl" value="<?php echo $payment['redirect_url']; ?>">
			<input type="hidden" name="failUrl" value="<?php echo $payment['failed_url']; ?>">
			<input type="hidden" name="cancelUrl" value="<?php echo $payment['cancel_url']; ?>">
			<input type="hidden" name="lang" value="<?php echo $payment['language']; ?>">
			<input type="hidden" name="payMethod" value="<?php echo $payment['payment_method']; ?>">
			<?php if(isset($discountedHash)): ?>
				<input type="hidden" name="secureHash" value="<?php echo $discountedHash; ?>">					
			<?php else: ?>
				<input type="hidden" name="secureHash" value="<?php echo $payment['secureHash']; ?>">
			<?php endif; ?>
			<input type="hidden" name="remark" value="<?php echo $payment['remark']; ?>">
			<input type="hidden" name="templateId" value="1">
		<?php endif; ?>

			<?php echo InsureDIYMotorHelper::renderProceedButton(true);?>
		<div class="clear"></div>
		</form>
	</div>
</div>
<div class="logo-slider"><?php echo MyHelper::load_module(102); ?></div>

<?php /* Popup Box */ ?>
<div class="insurediy-popup" id="insurediy-popup-box" style="display:none;">
	<div class="header">
		<div class="ico-warning">
			<div class="text-header2"><?php echo JText::_("POP_IMPORTANT_NOTICE"); ?></div>
		</div>
		<a class="close" href="javascript:cleanInsureDIYPopup('insurediy-popup-box');">&nbsp;</a>
		<div class="clear"></div>
	</div>
	<div class="padding">
		<div class="marginbottom20">
			<?php echo JText::_("COM_INSUREDIYMOTOR_WARNING_INSURANCE_REPLACEMENT"); ?>
		</div>
		<div style="text-align:right;">
			<input type="button" class="btn btn-primary" style="height:30px;padding:5px 20px;font-size:14px !important;" onclick="javascript:cleanInsureDIYPopup('insurediy-popup-box');
			return false;" value="Ok" />
		</div>
	</div>
</div>
<div id="dialog-alert-login" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="loginpopup" style="width: 350px;margin-left: -174px;top: 15%;padding: 20px;z-index:-1;">
  <div class="modal-dialog" role="document">
      <div class="modal-header" style="padding-bottom:0;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="loginpopup">Login and enjoy up to HK$200 off</h4>
      </div>
      <div class="modal-body" style="color:#222;margin-top: 0;overflow: hidden;">
	  	<div class="tab-header-container" style="width:auto;height:auto;margin:0;">
			<div class="tab-pane active" onclick="changeTab(this);" id="login-0" data-id="login-panel" data-title="Login and enjoy up to HK$200 off">
				<span class="tab-pane-title" style="font-size:12px;">LOGIN</span>
			</div>
			<div class="tab-pane" onclick="changeTab(this);" id="register-1" data-id="register-panel" data-title="Register and enjoy up to HK$200 off"> 
				<span class="tab-pane-title" style="font-size:12px;">REGISTER</span>
			</div>
		</div>
		<div class="tab-content-container" style="padding:0;margin-top: 20px;">
			<div class="tab-wrapper" id="login-panel">
				<form id="login-form-popup">
				<div class="insurediy-form" style="background-color: transparent;border: none;margin: 0;">
					<div class="input-wrapper" style="padding:0;">
						<span class="input-title" style="font-size:12px;">Email</span>
						<div class="input-container-block">
							<input type="text" class="input-text" style="width:100%;" name="username" />
						</div>
					</div>
					<div class="input-wrapper" style="padding:0;">
						<span class="input-title" style="font-size:12px;">Password</span>
						<div class="input-container-block">
							<input type="password" class="input-text" style="width:100%;" name="password" value="" />
							<input type="hidden" name="option" value="com_insurediymotor" />
							<input type="hidden" name="task" value="user.ajaxLogin" />
							<input type="hidden" name="return" value="" />
							<?php echo JHtml::_('form.token'); ?>
						</div>
					</div>
				</div>
				<div class="motor-btn-wrapper" style="width:auto;padding-bottom: 0;">
					<div class="motor-btn-group">
						<button type="submit" class="motor-btn validate" style="padding: 10px 20px;">LOGIN</button>
					</div>
				</div>
				</form>
			</div>
			<div class="tab-wrapper display-none" id="register-panel">
				<div class="insurediy-form" style="background-color: transparent;border: none;margin: 0;">
					<form id="register-form-popup">
					<div class="input-wrapper" style="padding:0;">
						<span class="input-title" style="font-size:12px;">Email</span>
						<div class="input-container-block">
							<input type="text" class="input-text" style="width:100%;" name="email" />
						</div>
					</div>
					<div class="input-wrapper" style="padding:0;">
						<span class="input-title" style="font-size:12px;">Password</span>
						<div class="input-container-block">
							<input type="password" class="input-text" style="width:100%;" name="password" value="" />
						</div>
					</div>
					<div class="input-wrapper" style="padding:0;">
						<span class="input-title" style="font-size:12px;">Confirm Password</span>
						<div class="input-container-block">
							<input type="password" class="input-text" style="width:100%;" name="password2" value="" />
							<input type="hidden" name="option" value="com_insurediymotor" />
							<input type="hidden" name="task" value="user.ajaxRegister" />
							<input type="hidden" name="return" value="" />
							<?php echo JHtml::_('form.token'); ?>
						</div>
					</div>
					<div class="motor-btn-wrapper" style="width:auto;padding-bottom: 0;">
						<div class="motor-btn-group">
							<button type="submit" class="motor-btn validate" style="padding: 10px 20px;">REGISTER</button>
						</div>
					</div>
					<div class="red-text no-thanks" style="text-align: center;text-decoration: underline;color: #FB5A6E;cursor:pointer;">No thanks, I will pay full amount.</div>
					</form>
				</div>
			</div>
		</div>	
      </div>
  </div>
</div>

<script>
	function changeTab(obj) {
		jQuery(".tab-pane").removeClass("active");
		jQuery(obj).addClass("active");
		jQuery('.tab-wrapper').addClass('display-none');
		jQuery("#"+jQuery(obj).attr("data-id")).removeClass("display-none");
		jQuery("#loginpopup").html(jQuery(obj).attr("data-title"));
	}
	jQuery('.loginpopup').on('click', function(e){
		e.preventDefault();
		jQuery('#login-0').click();
		jQuery('#loginpopup').html("Login and enjoy up to HK$200 off");
		jQuery('#dialog-alert-login').css("z-index", "1050");
		jQuery('#dialog-alert-login').modal('show');
	});
	jQuery('.regpopup').on('click', function(e){
		e.preventDefault();
		jQuery('#register-1').click();
		jQuery('#loginpopup').html("Register and enjoy up to HK$200 off");
		jQuery('#dialog-alert-login').css("z-index", "1050");
		jQuery('#dialog-alert-login').modal('show');
	});
	jQuery('.no-thanks').on('click', function(e){
		jQuery(".promo-no-login").fadeOut();
		jQuery("#jform_promo_code").val("");
		jQuery('#dialog-alert-login').modal('hide');
	});
var acc = document.getElementsByClassName("motor-accordion");
var i;

jQuery(document).ready(function(){
	jQuery("#login-form-popup").submit(function(e) {
		e.preventDefault();    
		var formData = new FormData(jQuery(this)[0]);
		jQuery("button").LoadingOverlay("show");
		jQuery.ajax({
			type: "POST",
			url: "<?php echo JRoute::_('index.php', true); ?>",
			data: formData,
			dataType: "json",
			contentType: false,
			processData: false,
			success: function(result){
				if (result["success"] == true) {
					location.reload();
				} else {
					alert(result["message"]);
					jQuery("button").LoadingOverlay("hide");
				}
			},
			error: function(xhr, textStatus, errorThrown){
				console.log(errorThrown);
				jQuery("button").LoadingOverlay("hide");
			}
		});
	});

	jQuery("#register-form-popup").submit(function(e) {
		e.preventDefault();    
		var formData = new FormData(jQuery(this)[0]);
		jQuery("button").LoadingOverlay("show");
		jQuery.ajax({
			type: "POST",
			url: "<?php echo JRoute::_('index.php', true); ?>",
			data: formData,
			dataType: "json",
			contentType: false,
			processData: false,
			success: function(result){
				if (result["success"] == true) {
					location.reload();
				} else {
					alert(result["message"]);
					jQuery("button").LoadingOverlay("hide");
				}
			},
			error: function(xhr, textStatus, errorThrown){
				console.log(errorThrown);
				jQuery("button").LoadingOverlay("hide");
			}
		});
	});
	jQuery("form#adminForm").validate();
});

for (i = 0; i < acc.length; i++) {
	acc[i].nextElementSibling.style.maxHeight = (acc[i].nextElementSibling.scrollHeight + 500) + "px";
//   acc[i].onclick = function() {
//     this.classList.toggle("active");
//     var panel = this.nextElementSibling;
//     if (panel.style.maxHeight){
//       panel.style.maxHeight = null;
//     } else {
//       panel.style.maxHeight = (panel.scrollHeight + 500) + "px";
//     } 
//   }
}

function changeTab(obj) {
	jQuery(".tab-pane").removeClass("active");
	jQuery(obj).addClass("active");
	jQuery('.tab-wrapper').addClass('display-none');
	jQuery("#"+jQuery(obj).attr("data-id")).removeClass("display-none");
}

jQuery("input.promo-code-input").on("blur", function(obj){
	var promoCode = jQuery(this).val();

	if(promoCode != "") {
		jQuery.ajax({
			type: "POST",
			url: "<?php echo JURI::root().'index.php?option=com_insurediymotor&task=form.savePromoCode'; ?>",
			data:'code='+promoCode+'&quotationId=<?php echo $data["quotationId"]; ?>',
			dataType: "json",
			success: function(result){
				console.log(result);
				if(result) {
					//jQuery("img.promo-code-accept").css("display", "block");
				}
				else {
					
				}
			},
			error: function(xhr, textStatus, errorThrown){
				jQuery("img.promo-code-accept").css("display", "none");
			}
		});
	}
});

jQuery("input.promo-code-input").on("focus", function(obj){
	jQuery("img.promo-code-accept").css("display", "none");
});

	dataLayer.push({
		'event': 'checkoutOption',
		'ecommerce': {
			'checkout': {
				'actionField': {'step': 3}
			}
		}
	});
</script>
<script type="text/javascript">

function addCommas(nStr) {
    nStr += '';
    var x = nStr.split('.');
    var x1 = x[0];
    var x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

jQuery(document).ready(function($){
	$('#jform_apply_promo_code').on("submit", function(e){
		e.preventDefault();
		var baseUrl = '<?php echo JURI::root(); ?>';
		var promo_code = $("#jform_promo_code").val();
		var quote_id = '<?php echo $data["quotationId"]; ?>';
		<?php if($data["coverType"] == "01"): ?>
			var ptype = 'motorch';
		<?php else: ?>
			var ptype = 'motortp';
		<?php endif; ?>
		
		if(promo_code != ''){
			$(this).removeClass('error');
			$(this).removeClass('invalid');
			$('.promo_code_error').hide();
			$('.promo_code_error label').hide();

			var action = $('.promo_button').val();

			$.ajax({
				url: "<?php echo JURI::root().'index.php?option=com_insurediymotor&task=ajax.applyPromoCode&format=json'; ?>",
				data:'promo_code='+promo_code+'&action='+action+'&quote_id='+quote_id+'&ptype='+ptype,
				cache: false,
				type: "POST",
				before: function(xhr) {
					
				},
				success: function(response) {
					if(response != '') {
						console.log(response);
						var res = JSON.parse(response);
						
						if(res.success == 0){
							$('#jform_promo_code').addClass('error');
							$('#jform_promo_code').addClass('invalid');
							$('.promo_code_error label').addClass('error');
							if (res.login == false) {
								$('.promo-no-login').fadeIn();
							} else {
								$('.promo_code_error label').html(res.message);
							}
						} else {
							$('.promo_code_error label').removeClass('error');
							
							if(action == "Apply") {
								$('.promo_button').val('Cancel');
								$('.summary-desc-promo-item').remove();
								$('.summary-subtotal-promo-item').remove();
								$('ul.summary-desc-list').append('\
									<li class="desc-list summary-desc-promo-item">\
										Promo Code Discount: <b>-<?php echo $currency; ?>'+res.discount+'</b>\
									</li>\
								');

								$('ul.summary-subtotal-list').append('\
											<li class="desc-list summary-subtotal-promo-item">\
												<b>-<?php echo $currency; ?>'+res.discount+'</b>\
											</li>\
								');

								<?php if($data["selectedPartner"] != "sompo"): ?>
									var summaryAmount = "<?php echo $currency; ?>" + parseFloat(<?php echo $premium_before_discount; ?>-res.discount).toFixed(2);
									var summaryAmount = addCommas(summaryAmount);
									$('.summary-total-amount').html(summaryAmount);
								<?php else: ?>
									$('.summary-total-amount-title').html("Total [Before promo code discount]");
								<?php endif; ?>
								
								$("input[name^=amount]").val(parseFloat(<?php echo $premium_before_discount; ?>-res.discount).toFixed(2)
								);	
								$("input[name^=secureHash]").val(res.hash);
								$("input[name^=orderRef]").val(res.reqid);

								<?php if($data["selectedPartner"] == "sompo"): ?>
									$('.promo_code_error label').html("Promo Code Applied.<br>We will contact you to process the discount.<br>Premium payable today is before discount");
								<?php else: ?>
									$('.promo_code_error label').html(res.message);
								<?php endif; ?>							
							}
							else {
								$("#jform_promo_code").val("");
								$('.promo_button').val('Apply');

								$('.summary-desc-promo-item').remove();
								$('.summary-subtotal-promo-item').remove();
								
								$('.summary-total-amount').html("<?php echo $currency; ?><?php echo number_format($premium_before_discount, 2); ?>");

								$("input[name^=secureHash]").val("<?php echo $originalHash; ?>");
								
								$("input[name^=amount]").val("<?php echo number_format($premium_before_discount, 2, '.', ''); ?>");
								$("input[name^=requested_amount]").val("<?php echo number_format($premium_before_discount, 2, '.', ','); ?>");

								$('.promo_code_error label').html(res.message);
								$("input[name^=secureHash]").val(res.hash);
								$("input[name^=orderRef]").val(res.reqid);
							}
						}
						
						$('.promo_code_error').show();
						$('.promo_code_error label').show();
						//$('.btn-toolbar .btn.btn-primary').attr('disabled', true);
					} 						
				},
				error: function(xhr, textStatus, errorThrown){
					console.log(xhr);
				}
			});
				/* } else {
					$(this).addClass('error');
					$(this).addClass('invalid');
					$('.promo_code_error label').html('Please enter minimum 3 characters.');
					$('.promo_code_error').show();
					$('.promo_code_error label').show();
					//$('.btn-toolbar .btn.btn-primary').attr('disabled', true);
				} */
		} else {
			//$('.btn-toolbar .btn.btn-primary').attr('disabled', false);
		}
	});
});
</script>
