<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_insurediymotor
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
JLoader::import('incs.form.field', JPATH_ROOT);

require_once JPATH_COMPONENT . '/helpers/insurediymotor.php';
require_once JPATH_ROOT . '/components/com_insurediypromocodes/helpers/insurediypromocodes.php';
require_once JPATH_ROOT . '/components/com_remarketing/helpers/remarketing.php';
// require_once JPATH_ROOT . '/components/com_joomailermailchimpintegration/helpers/joomailermailchimphelper.php';
JLoader::register('MotorQuotation', JPATH_COMPONENT . '/class/quotation.php');
new MotorQuotation();

$task = JFactory::getApplication()->input->get('task');

$controller = JControllerLegacy::getInstance('InsureDIYMotor');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
