<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_insurediymotor
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Function to build a InsurediyMotor URL route.
 *
 * @return  array  The array of query string values for which to build a route.
 * @return  array  The URL route with segments represented as an array.
 * @since    1.5
 */
function InsurediyMotorBuildRoute(&$query) {
	// Declare static variables.
	static $items;
	static $default;
	static $form;


	$segments = array();

	// Get the relevant menu items if not loaded.
	if (empty($items)) {
		// Get all relevant menu items.
		$app = JFactory::getApplication();
		$menu = $app->getMenu();
		$items = $menu->getItems('component', 'com_insurediymotor');

		// Build an array of serialized query strings to menu item id mappings.
		for ($i = 0, $n = count($items); $i < $n; $i++) {

			// Check to see if we have found the form menu item.
			if (empty($form) && !empty($items[$i]->query['view']) && ($items[$i]->query['view'] == 'form')) {
				$form = $items[$i]->id;
			}
		}

		// Set the default menu item to use for com_insurediymotor if possible.
		if ($form) {
			$default = $form;
		}
	}

	if (!empty($query['view'])) {
		switch ($query['view']) {
			default:
			case 'form':
				if ($query['Itemid'] = $form) {
					unset($query['view']);
				} else {
					$query['Itemid'] = $default;
				}
				break;
		}
	}
	
	if (!empty($query['task'])) {
		switch ($query['task']) {
			case 'ajax.createPolicyResult':
				unset($query['task']);
				unset($query['format']);
				$segments = array("ajax","createPolicyResult");
				break;
			case 'form.testSompoPolicyObj':
				unset($query['task']);
				unset($query['format']);
				$segments = array("form","testSompoPolicyObj");
				break;
			case 'form.sompoPaymentReturn':
				unset($query['task']);
				unset($query['format']);
				$segments = array("form","sompoPaymentReturn");
				break;
			case 'form.sompoPaymentIPN':
				unset($query['task']);
				unset($query['format']);
				$segments = array("form","sompoPaymentIPN");
				break;
			case 'form.msigPaymentSuccess':
				unset($query['task']);
				unset($query['format']);
				$segments = array("form","msigPaymentSuccess");
				break;
			case 'form.msigPaymentCancel':
				unset($query['task']);
				unset($query['format']);
				$segments = array("form","msigPaymentCancel");
				break;
			case 'form.msigPaymentFail':
					unset($query['task']);
					unset($query['format']);
					$segments = array("form","msigPaymentFail");
					break;
			case 'form.hlaPaymentSuccess':
				unset($query['task']);
				unset($query['format']);
				$segments = array("form","hlaPaymentSuccess");
				break;
			case 'form.hlaPaymentCancel':
				unset($query['task']);
				unset($query['format']);
				$segments = array("form","hlaPaymentCancel");
				break;
			case 'form.hlaPaymentFail':
				unset($query['task']);
				unset($query['format']);
				$segments = array("form","hlaPaymentFail");
				break;
			case 'form.aigPaymentSuccess':
				unset($query['task']);
				unset($query['format']);
				$segments = array("form","aigPaymentSuccess");
				break;
			case 'form.aigPaymentCancel':
				unset($query['task']);
				unset($query['format']);
				$segments = array("form","aigPaymentCancel");
				break;
			case 'form.aigPaymentFail':
				unset($query['task']);
				unset($query['format']);
				$segments = array("form","aigPaymentFail");
				break;
			case 'form.awPaymentSuccess':
				unset($query['task']);
				unset($query['format']);
				$segments = array("form","awPaymentSuccess");
				break;
			case 'form.awPaymentCancel':
				unset($query['task']);
				unset($query['format']);
				$segments = array("form","awPaymentCancel");
				break;
			case 'form.awPaymentFail':
				unset($query['task']);
				unset($query['format']);
				$segments = array("form","awPaymentFail");
				break;
			case 'form.pinganPaymentSuccess':
				unset($query['task']);
				unset($query['format']);
				$segments = array("form","pinganPaymentSuccess");
				break;
			case 'form.pinganPaymentCancel':
				unset($query['task']);
				unset($query['format']);
				$segments = array("form","pinganPaymentCancel");
				break;
			case 'form.pinganPaymentFail':
				unset($query['task']);
				unset($query['format']);
				$segments = array("form","pinganPaymentFail");
				break;
			default:
				break;
		}
	}

	return $segments;
}

/**
 * Function to parse a InsurediyMotor URL route.
 *
 * @return  array  The URL route with segments represented as an array.
 * @return  array  The array of variables to set in the request.
 * @since    1.5
 */
function InsurediyMotorParseRoute($segments) {
	$vars = array();

	// Only run routine if there are segments to parse.
	if (count($segments) < 1) {
		return;
	}
	
	if($segments[0] == "ajax") {
		switch ($segments[1]) {
			case 'createPolicyResult':
				return array("task" => "ajax.createPolicyResult", "format"=>"json");
				break;
			default:
				break;
		}
	}
	
	if($segments[0] == "form") {
		switch ($segments[1]) {
			case 'testSompoPolicyObj':
				return array("task" => "form.testSompoPolicyObj");
				break;
			case 'sompoPaymentReturn':
				return array("task" => "form.sompoPaymentReturn");
				break;
			case 'sompoPaymentIPN':
				return array("task" => "form.sompoPaymentIPN");
				break;
			case 'msigPaymentSuccess':
				return array("task" => "form.msigPaymentSuccess");
				break;
			case 'msigPaymentCancel':
				return array("task" => "form.msigPaymentCancel");
				break;
			case 'msigPaymentFail':
				return array("task" => "form.msigPaymentFail");
				break;
			case 'hlaPaymentSuccess':
				return array("task" => "form.hlaPaymentSuccess");
				break;
			case 'hlaPaymentCancel':
				return array("task" => "form.hlaPaymentCancel");
				break;
			case 'hlaPaymentFail':
				return array("task" => "form.hlaPaymentFail");
				break;
			case 'aigPaymentSuccess':
				return array("task" => "form.aigPaymentSuccess");
				break;
			case 'aigPaymentCancel':
				return array("task" => "form.aigPaymentCancel");
				break;
			case 'aigPaymentFail':
				return array("task" => "form.aigPaymentFail");
				break;
			case 'awPaymentSuccess':
				return array("task" => "form.awPaymentSuccess");
				break;
			case 'awPaymentCancel':
				return array("task" => "form.awPaymentCancel");
				break;
			case 'awPaymentFail':
				return array("task" => "form.awPaymentFail");
				break;
			case 'pinganPaymentSuccess':
				return array("task" => "form.pinganPaymentSuccess");
				break;
			case 'pinganPaymentCancel':
				return array("task" => "form.pinganPaymentCancel");
				break;
			case 'pinganPaymentFail':
				return array("task" => "form.pinganPaymentFail");
				break;
			default:
				break;
		}
	}

	// Get the package from the route segments.
	$form_id = array_pop($segments);

	if (!is_numeric($form_id)) {
		$vars['view'] = 'form';
		return $vars;
	}
	// Set the package id if present.
	if ($form_id) {
		// Set the package id.
		$vars['form_id'] = (int) $form_id;

		// Set the view to package if not already set.
		if (empty($vars['view'])) {
			$vars['view'] = 'form';
		}
	} else {
		JError::raiseError(404, JText::_('JGLOBAL_RESOURCE_NOT_FOUND'));
	}

	return $vars;
}


