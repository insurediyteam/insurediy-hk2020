<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_insurediymotor
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * InsurediyMotor Route Helper
 *
 * @package     Joomla.Site
 * @subpackage  com_insurediymotor
 * @since       1.6
 */
class InsurediyMotorHelperRoute {

	/**
	 * Method to get the menu items for the component.
	 *
	 * @return  array  	An array of menu items.
	 * @since   1.6
	 */
	public static function &getItems() {
		static $items;

		// Get the menu items for this component.
		if (!isset($items)) {
			$app = JFactory::getApplication();
			$menu = $app->getMenu();
			$com = JComponentHelper::getComponent('com_insurediymotor');
			$items = $menu->getItems('component_id', $com->id);

			// If no items found, set to empty array.
			if (!$items) {
				$items = array();
			}
		}

		return $items;
	}

	/**
	 * Method to get a route configuration for the form view.
	 *
	 * @return  mixed  	Integer menu id on success, null on failure.
	 * @since   1.6
	 * @static
	 */
	public static function getFormRoute() {
		// Get the items.
		$items = self::getItems();
		$itemid = null;

		// Search for a suitable menu id.
		foreach ($items as $item) {
			if (isset($item->query['view']) && $item->query['view'] === 'form') {
				$itemid = $item->id;
				break;
			}
		}

		return $itemid;
	}

}
