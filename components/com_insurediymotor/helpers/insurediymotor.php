<?php

defined('_JEXEC') or die;

class InsureDIYMotorHelper {

	public static function renderHeader($icon, $header, $active) {
		$classes = array();
		switch ($active) {
			case 1:
				$classes = array('-active', '-active', '', '', '', '', '', '','');
				break;
			case 2:
				$classes = array('', '', '-active', '-active', '', '', '', '', '');
				break;
			case 3:
				$classes = array('', '', '', '', '-active', '-active', '', '', '');
				break;
			case 4:
				$classes = array('', '', '', '', '', '', '-active', '-active', '');
				break;
			case 5:
				$classes = array('', '', '', '', '', '', '', '-active', '-active');
				break;
			default:
				break;
		}
		
		$buffer = '<div class="header-motor-top">';
		$buffer.= '<h1 class="insurediy-motor-heading-top">';
		$buffer.= $header;
		$buffer.= '</h1>';
		$buffer.= '<div class="insurediy-motor-heading-bottom">';
		$buffer.= '<a href="'.JRoute::_('index.php?option=com_insurediymotor&view=motor&Itemid=159&step=1').'"><div class="insurediy-motor-breadcrumb' . $classes[0] . '">';
		$buffer.= 'ABOUT YOU';
		$buffer.= '</div></a>';
		$buffer.= '<div class="insurediy-motor-breadcrumb-divider' . $classes[1] . '">';
		$buffer.= '</div>';
		$buffer.= '<a style="display:none;" href="'.JRoute::_('index.php?option=com_insurediymotor&view=motor&Itemid=159&step=2').'"><div class="insurediy-motor-breadcrumb' . $classes[2] . '">';
		$buffer.= 'T&Cs';
		$buffer.= '</div></a>';
		$buffer.= '<div style="display:none;" class="insurediy-motor-breadcrumb-divider' . $classes[3] . '">';
		$buffer.= '</div>';
		$buffer.= '<a href="'.JRoute::_('index.php?option=com_insurediymotor&view=motor&Itemid=159&step=3').'"><div class="insurediy-motor-breadcrumb' . $classes[4] . '">';
		$buffer.= 'CHOOSE PLANS';
		$buffer.= '</div></a>';
		$buffer.= '<div class="insurediy-motor-breadcrumb-divider' . $classes[5] . '">';
		$buffer.= '</div>';
		$buffer.= '<a href="'.JRoute::_('index.php?option=com_insurediymotor&view=motor&Itemid=159&step=4').'"><div class="insurediy-motor-breadcrumb' . $classes[6] . '">';
		$buffer.= 'DETAILS';
		$buffer.= '</div></a>';
		$buffer.= '	<div class="insurediy-motor-breadcrumb-divider' . $classes[7] . '">';
		$buffer.= '</div>';
		$buffer.= '<a href="'.JRoute::_('index.php?option=com_insurediymotor&view=motor&Itemid=159&step=5').'"><div class="insurediy-motor-breadcrumb' . $classes[8] . '">';
		$buffer.= 'CONFIRM ORDER';
		$buffer.= '</div></a>';
		$buffer.= '</div>';
		$buffer.= '</div>';

		// 		$buffer = '<div class="header-top">';
		// 		$buffer.= '<div class="h1-heading">';
		// 		$buffer.= '<i class="' . $icon . '" ></i>';
		// 		$buffer.= $header;
		// 		$buffer.='</div>';
		// 		$buffer.= '<div class="insurediy-breadcrumbs">';
		// 		$buffer.= '<div class="mybreadcrumb back"></div>';
		// 		$buffer.= '<div class="mybreadcrumb middle' . $classes[0] . '">';
		// 		$buffer.= JText::_("COM_INSUREDIYTRAVEL_BREADCRUMB_YOUR_DETAILS") . '</div>';
		// 		$buffer.='<div class="mybreadcrumb between' . $classes[1] . '">';
		// 		$buffer.= '</div><div class = "mybreadcrumb middle' . $classes[2] . '">';
		// 		$buffer.= JText::_("COM_INSUREDIYTRAVEL_BREADCRUMB_SELECT_PRODUCT");
		// 		$buffer.='</div>';
		// 		$buffer.='<div class="mybreadcrumb between' . $classes[3] . '">';
		// 		$buffer.='</div>';
		// 		$buffer.='<div class="mybreadcrumb middle' . $classes[4] . '">';
		// 		$buffer.= JText::_("COM_INSUREDIYTRAVEL_BREADCRUMB_APPLY") . '</div>';
		// 		$buffer.='<div class="mybreadcrumb between' . $classes[5] . '">';
		// 		$buffer.='</div>';
		// 		$buffer.='<div class="mybreadcrumb middle' . $classes[6] . '">';
		// 		$buffer.= JText::_("COM_INSUREDIYTRAVEL_BREADCRUMB_PAYMENT") . '</div>';
		// 		$buffer.='<div class="mybreadcrumb front' . $classes[7] . '"></div>';
		// 		$buffer.='<div class="clear"></div>';
		// 		$buffer.= '</div>';
		// 		$buffer.= '<div class="clear"></div></div>';
		return $buffer;
	}

	public static function renderProceedButton($includeBack) {
		
		$buffer = '<div class="motor-btn-wrapper">';
		$buffer.= '<div class="motor-btn-group">';
		if($includeBack) {
			$buffer.= '<button style="margin-right:20px; padding: 10px 18px 10px 10px;" type="button" onclick="window.history.back();" class="motor-btn validate"><span class="white-arrow-left"></span>BACK</button>';
			$buffer.= '<button type="submit" id="submit_button" class="motor-btn validate">CONTINUE<span class="white-arrow"></span></button>';
			$buffer.= '</div>';
		}
		else {
			$buffer.= '<button type="submit" id="submit_button" class="motor-btn validate">CONTINUE<span class="white-arrow"></span></button>';
			$buffer.= '</div>';
			$buffer.= '<div class="motor-btn-group" style="margin-top:10px;">';
			$buffer.= '<a href="javascript:resetAll();" class="motor-reset-btn">RESET ALL</a>';
			$buffer.= '</div>';
		}
		
		$buffer.= '</div>';

		return $buffer;
	}
	
	// PARAMS
	public static function getAppSummeryMod($default = "") {
		$params = JComponentHelper::getParams("com_insurediymotor");
		return $params->get("app_summery", $default);
	}

	public static function getDeclarationsMod($default = "") {
		$params = JComponentHelper::getParams("com_insurediymotor");
		return $params->get("declarations", $default);
	}

	public static function getCurrency($default = "HK$") {
		$params = JComponentHelper::getParams("com_insurediymotor");
		return $params->get("currency", $default);
	}

	public static function getFbTitle($default = "") {
		$params = JComponentHelper::getParams("com_insurediymotor");
		return $params->get("fbtitle", $default);
	}

	public static function getFbDesc($default = "") {
		$params = JComponentHelper::getParams("com_insurediymotor");
		return $params->get("fbdesc", $default);
	}

	public static function getFbImage($default = "") {
		$params = JComponentHelper::getParams("com_insurediymotor");
		return $params->get("fbimage", $default);
	}

	public static function getFbSiteName($default = FALSE) {
		$default = ($default) ? $default : JFactory::getConfig()->get("sitename", "");
		$params = JComponentHelper::getParams("com_insurediymotor");
		return $params->get("fbsitename", $default);
	}

	public static function getFbAppId($default = "") {
		$params = JComponentHelper::getParams("com_insurediymotor");
		return $params->get("fbappid", $default);
	}

	public static function getRefExp($default = "") {
		//		$params = JComponentHelper::getParams("com_insurediymotor");
		//		return $params->get("refExp", $default);
		return JText::_("COM_INSUREDIYMOTOR_REFERRAL_EXPLANATION");
	}

	public static function getRefMsg($default = "") {
		//		$params = JComponentHelper::getParams("com_insurediymotor");
		//		return str_replace("XXYY", JFactory::getUser()->referral_id, $params->get("refMsg", $default));
		return str_replace("XXYY", JFactory::getUser()->referral_id, JText::_("COM_INSUREDIYMOTOR_REFERRAL_MESSAGE"));
	}

	// QUOTATION STUFFS
	public static function isOldQuote() {
		return self::getQid();
	}

	public static function getCurrentQid() {
		return JFactory::getSession()->get("motor.quotation_id", 0);
	}

	public static function getInputQid() {
		return JFactory::getApplication()->input->get("quotation_id", 0, "integer");
	}

	public static function getQid() {
		return self::getInputQid() ? self::getInputQid() : self::getCurrentQid();
	}

	public static function canViewLayout($layout) {
		$user = JFactory::getUser();
		$stage = self::getStageFromLayout($layout);
		if ($stage > 1 && $user->id < 1) {
			return FALSE;
		}
		return $stage <= self::getCurrentStage(self::getQid());
	}
	
	public static function extractMotorQuotations($data) {
		set_time_limit(0);
		
		// build the individual requests as above, but do not execute them
		//$urlArr = ["AXA"=>'AXA.php', "MSIG"=>'MSIG.php', "SOMPO"=>'SOMPO.php'];
		//$urlArr = ["AXA"=>'AXA.php', "SOMPO"=>'SOMPO.php'];
		//$urlArr = ["MSIG"=>'MSIG.php'];
		$urlArr = ["AXA"=>'AXA.php'];
		
		$multiCh = curl_multi_init();
		$ch = array();
		
		foreach($urlArr as $key=>$val) {
			$url = JUri::base().$val;
			$ch[$key] = curl_init($url);
			
			curl_setopt($ch[$key], CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch[$key], CURLOPT_POSTFIELDS, $data);
			
			// build the multi-curl handle, adding $ch
			curl_multi_add_handle($multiCh, $ch[$key]);
		}
		
		$start = time();
		// execute all queries simultaneously, and continue when all are complete
		$running = null;
		do {
			curl_multi_exec($multiCh, $running);
		} while ($running);
		
		$result = array();
		
		foreach($urlArr as $key=>$val) {
			$result[$key] = json_decode(curl_multi_getcontent($ch[$key]));
			curl_multi_remove_handle($multiCh, $ch[$key]);
			curl_close($ch[$key]);
		}
		return $result;
	}

	public static function getLayout() {
		$user = JFactory::getUser();
		$app = JFactory::getApplication();

		$inputLayout = $app->input->get("layout", FALSE);
		$staticLayouts = array("error", "thankyou", "cancelled", "syserror", "SIT");
		if ($inputLayout && in_array($inputLayout, $staticLayouts)) {
			return $inputLayout;
		}
		
		$step = $app->input->get("step", FALSE);

		if(!$step) {
			return "step1";
		}
		
		if($step > 5 && $user->guest) {
			return "login";
		}
		else {
			return "step".$step;
		}
	}

	public static function checkOwnership($id) {
		$user = JFactory::getUser();
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("count(*)")
				->from("#__insure_motor_quotations")
				->where("user_id = " . $db->quote($user->id))
				->where("id = " . $db->quote($id));
		$db->setQuery($query);
		return $db->loadResult();
	}

	public static function getCurrentStage($id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("quote_stage")
				->from("#__insure_motor_quotations")
				->where("id = " . $db->quote($id));
		$db->setQuery($query);
		return $db->loadResult();
	}

	public static function getLayoutFromStage($step) {
		$stageToLayouts = array(1 => "step1", 2 => "step2", 3 => "step3", 4 => "step4", 5 => "step5", 6 => "thankyou");
		return isset($stageToLayouts[$step]) ? $stageToLayouts[$step] : "step1";
	}

	public static function getStageFromLayout($layout) {
		$layoutToStages = array("step1" => 1, "login" => 2, "step2" => 2, "step3" => 3, "step4" => 4, "step5" => 5, "thankyou" => 6);
		return isset($layoutToStages[$layout]) ? $layoutToStages[$layout] : 0;
	}

	public static function clearSessionData() {
		$session = JFactory::getSession();
		$session->set("motor.data", NULL);
		$session->set("motor.quotation_id", NULL);
	}

	public static function overrideSessionWithQid($qid) {
		$session = JFactory::getSession();
		$session->set("motor.quotation_id", $qid);
		$session->set("motor.data", self::getQuotation($qid));
	}

	public static function getQuotation($qid = FALSE, $checkUser = TRUE) {
		if (!$qid) {
			$qid = self::getQid();
		}
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_motor_quote_master")
				->where("id = " . $db->quote($qid));
		if ($checkUser) {
			$query->where("userId = " . $db->quote(JFactory::getUser()->id));
		}
		$quotation = $db->setQuery($query)->loadAssoc();
		if ($quotation) {
			if($quotation["quote_policy_selected_partner_code"] == "sompo") {
				$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_motor_quote_sompo")
				->where("quote_master_id = " . $db->quote($quotation["id"]));
				$db->setQuery($query);
				$sompoData = $db->loadObject();
				
				if($sompoData) {
					$planKey = array_search($quotation["quote_policy_selected_plan_code"], explode(",", $sompoData->sompo_plan_id));
					
					$planName = explode(",", $sompoData->sompo_plan_name)[$planKey];
					
					$planRisk = explode("#", $sompoData->sompo_plan_risk)[$planKey];
					
					$planPremium = explode(",", $sompoData->sompo_plan_premium_total)[$planKey];
					
					$quotation['selected_plan'] = (object)array(
							"plan_name" => $planName,
							"plan_access" => $planRisk,
							"premium" => $planPremium,
							"plan_model_variant" => $sompoData->quote_car_variant,
							"plan_ref_id" => $sompoData->sompo_app_quote_ref_id
					);
				}
			}
			else if($quotation["quote_policy_selected_partner_code"] == "msig") {
				$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_motor_quote_msig")
				->where("quote_master_id = " . $db->quote($quotation["id"]));
				$db->setQuery($query);
				$msigData = $db->loadObject();
				
				if($msigData) {
					$selectedPartnerFlag = true;
					
					$planKey = array_search($quotation["quote_policy_selected_plan_code"], explode(",", $msigData->msig_plan_id));
					
					$planName = explode(",", $msigData->msig_plan_name)[$planKey];
					
					$planRisk =  explode(",", $msigData->msig_plan_excess_standard)[$planKey];
					
					$planPremium = explode(",", $msigData->msig_plan_premium_total)[$planKey];
					
					$quotation['selected_plan'] = (object)array(
							"plan_name" => $planName,
							"plan_access" => $planRisk,
							"premium" => $planPremium,
							"plan_model_variant" => $msigData->quote_car_variant,
							"plan_ref_id" => $msigData->msig_ref_id
					);
				}
			}
			else if($quotation["quote_policy_selected_partner_code"] == "hla") {
				$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_motor_quote_hla")
				->where("quote_master_id = " . $db->quote($quotation["id"]));
				$db->setQuery($query);
				$hlaData = $db->loadObject();
				
				if($hlaData) {
					$selectedPartnerFlag = true;
					
					$planKey = array_search($quotation["quote_policy_selected_plan_code"], explode(",", $hlaData->hla_plan_id));
					
					$planName = explode(",", $hlaData->hla_plan_name)[$planKey];
					
					$planRisk = "<b>";
					
					$planRisk .= $savedInputData["workshop"] == "Yes" ? explode("#", $hlaData->hla_plan_excess_standard)[0] : explode("#", $hlaData->hla_plan_excess_standard)[1];
					
					$planRisk .= "</b></li><li class=\"desc-list\">Young and/or inexperienced driver excess : <b>" . $hlaData->hla_plan_excess_young . "</b>";
					
					$planPremium = explode(",", $hlaData->hla_plan_premium_total)[$planKey];
					
					$quotation['selected_plan'] = (object)array(
							"plan_name" => $planName,
							"plan_access" => $planRisk,
							"premium" => $planPremium,
							"plan_model_variant" => $hlaData->quote_car_variant,
							"plan_ref_id" => ""
					);
				}
			}
			else if($quotation["quote_policy_selected_partner_code"] == "aig") {
				$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_motor_quote_aig")
				->where("quote_master_id = " . $db->quote($quotation["id"]));
				$db->setQuery($query);
				$aigData = $db->loadObject();
				
				if($aigData) {
					$selectedPartnerFlag = true;
					
					$planKey = array_search($quotation["quote_policy_selected_plan_code"], explode(",", $aigData->aig_plan_id));
					
					$planName = explode(",", $aigData->aig_plan_name)[$planKey];
					
					$planRisk = "";

					$planPremium = explode(",", $aigData->aig_plan_premium_total)[$planKey];
					
					$quotation['selected_plan'] = (object)array(
							"plan_name" => $planName,
							"plan_access" => $planRisk,
							"premium" => $planPremium,
							"plan_model_variant" => $aigData->quote_car_variant,
							"plan_ref_id" => ""
					);
				}
			}
			else if($quotation["quote_policy_selected_partner_code"] == "aw") {
				$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_motor_quote_aw")
				->where("quote_master_id = " . $db->quote($quotation["id"]));
				$db->setQuery($query);
				$awData = $db->loadObject();
				
				if($awData) {
					$selectedPartnerFlag = true;
					
					$planKey = array_search($quotation["quote_policy_selected_plan_code"], explode(",", $awData->aw_plan_id));
					
					$planName = explode(",", $awData->aw_plan_name)[$planKey];
					
					$planRisk = "";

					$planPremium = explode(",", $awData->aw_plan_premium_total)[$planKey];
					
					$quotation['selected_plan'] = (object)array(
							"plan_name" => $planName,
							"plan_access" => $planRisk,
							"premium" => $planPremium,
							"plan_model_variant" => $awData->quote_car_variant,
							"plan_ref_id" => ""
					);
				}
			}
			else if($quotation["quote_policy_selected_partner_code"] == "pingan") {
				$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_motor_quote_pingan")
				->where("quote_master_id = " . $db->quote($quotation["id"]));
				$db->setQuery($query);
				$pinganData = $db->loadObject();
				
				if($pinganData) {
					$selectedPartnerFlag = true;
					
					$planKey = array_search($quotation["quote_policy_selected_plan_code"], explode(",", $pinganData->pingan_plan_id));
					
					$planName = explode(",", $pinganData->pingan_plan_name)[$planKey];
					
					$planRisk = "";

					$planPremium = explode(",", $pinganData->pingan_plan_premium_total)[$planKey];
					
					$quotation['selected_plan'] = (object)array(
							"plan_name" => $planName,
							"plan_access" => $planRisk,
							"premium" => $planPremium,
							"plan_model_variant" => $pinganData->quote_car_variant,
							"plan_ref_id" => ""
					);
				}
			}
		}
		return $quotation;
	}

	public static function getCompleteAddress($data) {
		$complete_address = '';
		$complete_address .= ($data['contact_block_no'] ? 'Blk ' . $data['contact_block_no'] : '');
		$complete_address .= ($data['contact_street_name'] ? ' ' . $data['contact_street_name'] : '');
		//$complete_address .= ($data['contact_room_no'] ? ' ' . $data['contact_room_no'] : '');
		$complete_address .= ($data['contact_unit'] ? '<br /> ' . $data['contact_unit'] : '');
		$complete_address .= ($data['contact_building_name'] ? '<br /> ' . $data['contact_building_name'] : '');
		//$complete_address .= ($data['contact_district_name'] ? ', ' . $data['contact_district_name'] : '');
		//$complete_address .= ($data['contact_country'] ? ', ' . $data['contact_country'] . ' ' : '');
		return $complete_address;
	}

	// Call this before Loading the quotation.
	public static function checkQuotation($quotation) {
		//		$lastCheck = $quotation['last_check_date'];
		if ($quotation['quote_stage'] <= 2) {
			return TRUE;
		}

		//		$age_atm = MyHelper::getAge($quotation['dob']);
		//		if ($lastCheck == "0000-00-00") {
		//			self::updateLastCheck($quotation['id']);
		//			$age_applied = MyHelper::getAge($quotation['dob']);
		//		} else {
		//			$age_applied = MyHelper::getAge($quotation['dob'], $lastCheck);
		//		}
		//		if ($age_atm != $age_applied) {
		//			return FALSE;
		//		}

		if (isset($quotation['selected_plans']) && is_array($quotation['selected_plans'])) {
			foreach ($quotation['selected_plans'] as $plan) {
				$db = JFactory::getDbo();
				$query = $db->getQuery(TRUE)
						->select("plan_index_code, price")
						->from("#__insure_motor_plans")
						->where("plan_index_code = " . $db->quote($plan->plan_index_code));
				$current_plan = $db->setQuery($query)->loadObject();
				if ($current_plan && isset($current_plan->price) && $current_plan->price != $plan->price) {
					return FALSE;
				}
			}
		}
		return TRUE;
	}

	public static function resetQuotation($quotation) {
		// clear selected quotation
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->delete("#__insure_motor_quotations_to_plans")
				->where("quotation_id = " . $db->quote($quotation['id']));
		$db->setQuery($query)->execute();

		// update to quotation table
		$today = JHtml::date(NULL, "Y-m-d");
		$query = $db->getQuery(TRUE)
				->update("#__insure_motor_quotations")
				->set("quote_stage = 2") // make them choose plans again
		//				->set("last_check_date = " . $db->quote($today))
				->where("id = " . $db->quote($quotation['id']));
		$db->setQuery($query)->execute();
	}

	public static function updateLastCheck($qid) {
		$db = JFactory::getDbo();
		$today = JHtml::date(NULL, "Y-m-d");
		$query = $db->getQuery(TRUE)
				->update("#__insure_motor_quotations")
				->set("last_check_date = " . $db->quote($today))
				->where("id = " . $db->quote($qid));
		$db->setQuery($query)->execute();
	}

	public static function getRenewalInfo($currentInsurer) {
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
		->select("*")
		->from("#__insure_motor_renewal_partner")
		->where("renewal_partner_code =" . $db->quote($currentInsurer));
		$db->setQuery($query);
		$renewal = $db->loadObject();
		
		if($renewal) {
			return array("partnerCode"=>$renewal->renewal_partner_name_short, "partnerName"=>$renewal->renewal_partner_name);
		}
		else {
			return false;
		}
	}
	
	public static function generatePaymentSecureHash($merchantId, $merchantReferenceNumber, $currencyCode, $amount, $paymentType, $secureHashSecret) {

		$buffer = $merchantId . '|' . $merchantReferenceNumber . '|' . $currencyCode . '|' . $amount . '|' . $paymentType . '|' . $secureHashSecret;
		return sha1($buffer);

	}
	public static function updatePaymentRef($ref, $quoteId){
		$db = JFactory::getDBO();
		
		$query = $db->getQuery(TRUE)
		->select("*")
		->from("#__insure_motor_quote_master")
		->where("id = " . $db->quote($quoteId) . " AND " . "quote_deleted_time IS NULL");
		$db->setQuery($query);
		$quotation = $db->loadResult();
		
		if(!$quotation) {
			return false;
		}

		$quoteData = (object) array(
			"id" => $quoteId,
			"quote_payment_request_id" => $ref
		);
	
		$update = $db->updateObject("#__insure_motor_quote_master", $quoteData, "id");

		if($update) {
			return true;
		}
		else {
			return false;
		}
	}
	
	// *** SOMPO FUNCTIONS ***
	public static function getSsoToken() {
		$adminParams = JComponentHelper::getParams("com_insurediymotor");
		$credential = array(
				"grant_type" => "password",
				"client_id" => $adminParams->get("sompoClientId",""),
				"username" => $adminParams->get("sompoUsername",""),
				"password" => $adminParams->get("sompoPassword","")
		);
		
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, $adminParams->get("sompoSSO",""));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'Accept: application/json'));
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($credential));
		
		$server_output = curl_exec ($ch);
		
		curl_close($ch);
		
		return json_decode($server_output);
	}
	
	private static function sompoGetVehicleData($variant) {
		$db = JFactory::getDBO();
		
		$query = $db->getQuery(TRUE)
		->select("*")
		->from("#__insure_motor_make_model_sompo")
		->where("model_code = ".$db->quote($variant));
		$db->setQuery($query);
		$carModelData = $db->loadObject();
		
		
		$result = array(
				"makeCode" => $carModelData->model_make_code,
				"modelCode" => $variant,
				"vehCc" => $carModelData->model_cc,
				"bodyType" => $carModelData->model_bodytype,
				"passengerNo" => $carModelData->model_no_passenger
		);
		
		return $result;
	}
	
	private static function sompoGetOccupationCode($occ) {
		return $occ;
	}
	
	public static function sompoEncryptData($string) {
		include_once('Crypt/RSA.php');
		
		$rsa = new Crypt_RSA();
		$key_public = file_get_contents(JPATH_BASE."/incs/helper/uat.sompo.public.pem");
		
		if(!$key_public) {
			return false;
		}
		
		$rsa->loadKey($key_public);
		$rsa->setEncryptionMode(CRYPT_RSA_ENCRYPTION_PKCS1);
		return base64_encode($rsa->encrypt($string));
		
		// Encrypt using the public key
		//openssl_public_encrypt($string, $encrypted, $key_public);

 		//return base64_encode($encrypted);
		//return $key_public;
	}
	
	public static function sompoQuoteRequestData($data) {
		$adminParams = JComponentHelper::getParams("com_insurediymotor");
		
		$gender = $data['Gender'][0] == "MALE" ? 'M' : 'F';
		$maritalStatus = $data['maritalStatus'][0] == "Single" ? 'S' : 'M';
		$occupation = self::sompoGetOccupationCode($data["Occupation"][0]);
		$date = new DateTime('now');
		$date->setTimezone(new DateTimeZone('Asia/Singapore'));
		$now = $date->format("Y-m-d\TH:i:s.u+08:00");
		$OFD = $data['NCDPoints'] >= 30 ? 5 : 0;
		$ncdPro = $data['NCDPoints'] >= 30 ? "Y" : "N";
		$vehData = self::sompoGetVehicleData($data["vehVariantSompo"]);
		$ncdCode = $data['NCDPoints']== 0 ? "" : "MTNCD0".$data['NCDPoints'];
		$driverArray = array();
		$isSpouse = "N";
		
		foreach($data["Gender"] as $key => $value) {
			$driverArray[] = (object)[
					"DRV_SEX" => $data['Gender'][$key] == "MALE" ? 'M' : 'F',
					"DRV_MAR_STATUS" => $data['maritalStatus'][$key] == "Single" ? 'S' : 'M',
					"DRV_DOB" => DateTime::createFromFormat('d-M-Y', $data['dateOfBirth'][$key])->format('Y-m-d')."T00:00:00",
					"DRV_EXP" => $data['yearsOfDrivingExp'][$key],
					"DRV_OCC_CODE" => self::sompoGetOccupationCode($data["Occupation"][$key]),
					"NOOFCLAIMS" => $key == 0 ? $data['claimsPast3Years'] : "0",
					"TOTALCLAIMAMT" => $key == 0 ? $data['claimAmount'] : "0",
					"DRV_RATING_APPL_YN" => $key == 0 ? "Y" : "N",
					"DRV_TRAF_POINTS" => 0,
			];
			
			if($data['relationshipWithMainDriver'][$key]== "S") {
				$isSpouse = "Y";
			}
		}
		
		$sompoQuoteRequestObj = (object)[
				"applicationId" => $adminParams->get("sompoAppId",""),
				"userId" => $adminParams->get("sompoUserId",""),
				"serviceId" => "getQuote",
				"requestUniqueId" =>  "INSDIY-QUOTE-".self::guid(),
				"serviceRequestObject" => (object)[
						"CURRENTPOLICY" => (object)[
								"POL_SC_CODE" => "MTPV01",
								"POL_FM_DT" => DateTime::createFromFormat('d-M-Y', $data['policyStartDate'])->format('Y-m-d')."T00:00:00",
								"POL_TO_DT" => DateTime::createFromFormat('d-M-Y', $data['policyStartDate'])->add(new DateInterval('P1Y'))->sub(new DateInterval('P1D'))->format('Y-m-d')."T00:00:00",
								"POL_SUB_PRD_CODE" => $adminParams->get("sompoProdCode",""),
								"POL_DOB" => DateTime::createFromFormat('d-M-Y', $data['dateOfBirth'][0])->format('Y-m-d')."T00:00:00",
								"POL_GENDER" => $gender,
								"POL_OCC_CODE" => $occupation,
								"POL_RATING_EFF_DT" => $now,
								"TRIP_TYPE" => "MTCPC",
								"POL_MAR_STATUS" => $maritalStatus,
								"DRV_VALID_LIC_YN" => $data['validLicense'] == "Yes" ? 'Y' : 'N',
								"NOOFCLAIMS" => $data['claimsPast3Years'],
								"TOTALCLAIMAMT" => $data['claimAmount'],
								"MOTOROFD" => $OFD,
								"MOTORISEXCESSEXISTS" => "",
								"NO_OF_PERSONS" => count($data['Gender']),
								"OPTIONALCOVERAGELIST" => [],
								"VOLUNTARYEXCESSLIST" => [],
								"PROMO_CODE" => null,
								"VEHICLEINFO" => (object)[
										"VEH_BODY_TYPE" => $vehData["bodyType"],
										"VEH_CC" =>  $vehData["vehCc"],
										"VEH_COE_FLG" => "Y",
										"VEH_INSURED_DRIVING_YN" => "Y",
										"VEH_MAKE_CODE" => $vehData["makeCode"],
										"VEH_MODEL_CODE" => $vehData["modelCode"],
										//"VEH_HP_DTLS" => "",
										"VEH_NCD_PROT_ELIG" => $ncdPro,
										"VEH_NO_DRV" => count($data['Gender']),
										"VEH_NO_PASS" => $vehData["passengerNo"],
										"VEH_PRV_CLM_FREE_YR" => $data['NCDPoints'] == 0 ? "" : $data['NCDPoints']/10,
										"VEH_REGN_NO" => strtoupper($data['vehicleRegNo']),
										"VEH_REGN_YEAR" => $data["carRegisterYear"],
										"VEH_SPOUSE_DRIVING_YN" => $isSpouse,
										"VEH_YEAR" => $data["carMakeYear"],
										"VEH_ONT_PARKING" => $data['companyRegistered'] == "Yes" ? 'Y' : 'N',
										"VEH_CERT_EXP_DT" => DateTime::createFromFormat('d-M-Y', $data['coeExpireDate'])->format('Y-m-d')."T00:00:00",
										"REASON_FOR_NCD" => $data["ncdReason"],
										"IS_NAMEDRIVERS_TERMS_AGREED" => $data["agreeDeclaration"] == "true" ? "Y" : "N",
										"NCD_DISC_CODE" => $ncdCode
								],
								"DRIVERSLIST" => $driverArray
						]
				]
		];
		
		return $sompoQuoteRequestObj;
	}
	
	public static function sompoProposalRequestData($data) {
		$adminParams = JComponentHelper::getParams("com_insurediymotor");
		
		date_default_timezone_set('Asia/Singapore');
		$gender = $data['Gender'][0] == "MALE" ? 'M' : 'F';
		$maritalStatus = $data['maritalStatus'][0] == "Single" ? 'S' : 'M';
		$occupation = self::sompoGetOccupationCode($data["Occupation"][0]);
		$date = new DateTime('now');
		$date->setTimezone(new DateTimeZone('Asia/Singapore'));
		$now = $date->format("Y-m-d\TH:i:s.u+08:00");
		$OFD = $data['NCDPoints'] >= 30 ? 5 : 0;
		$ncdPro = $data['NCDPoints'] >= 30 && $data['plan_id'] != "EDF" ? "Y" : "N";
		$vehData = self::sompoGetVehicleData($data["vehVariantSompo"]);
		$ncdCode = $data['NCDPoints']== 0 ? "" : "MTNCD0".$data['NCDPoints'];
		$driverArray = array();
		$isSpouse = "N";
		
		foreach($data["Gender"] as $key => $value) {
			$driverArray[] = (object)[
					"DRV_SEX" => $data['Gender'][$key] == "MALE" ? 'M' : 'F',
					"DRV_MAR_STATUS" => $data['maritalStatus'][$key] == "Single" ? 'S' : 'M',
					"DRV_DOB" => DateTime::createFromFormat('d-M-Y', $data['dateOfBirth'][$key])->format('Y-m-d')."T00:00:00",
					"DRV_EXP" => $data['yearsOfDrivingExp'][$key],
					"DRV_OCC_CODE" => self::sompoGetOccupationCode($data["Occupation"][$key]),
					"NOOFCLAIMS" => $key == 0 ? $data['claimsPast3Years'] : "0",
					"TOTALCLAIMAMT" => $key == 0 ? $data['claimAmount'] : "0",
					"DRV_RATING_APPL_YN" => $key == $data["driverApplicant"]? "Y" : "N",
					"DRV_TRAF_POINTS" => $data['demeritPoint'][$key],
					"DRV_NAME" => self::sompoEncryptData(strtoupper($data['firstName'][$key] . " " . $data['lastName'][$key])),
					"DRV_CITIZENSHIP" =>  $data['icType'][$key],
					"DRV_ID" => self::sompoEncryptData(strtoupper($data['Nric'][$key])),
					"DRV_RELATION" => $data['relationshipWithMainDriver'][$key]	
			];
			
			if($data['relationshipWithMainDriver'][$key]== "S") {
				$isSpouse = "Y";
			}
		}
		
		$sompoProposalRequestObj = (object)[
				"applicationId" => $adminParams->get("sompoAppId",""),
				"userId" => $adminParams->get("sompoUserId",""),
				"serviceId" => "getProposal",
				"requestUniqueId" => "INSDIY-PROPOSAL-".self::guid(),
				"serviceRequestObject" => (object)[
						"CURRENTPOLICY" => (object)[
								"APPQUOTEPOLICYREFID" => $data['ref_id'],
								"POL_SC_CODE" => "MTPV01",
								"POL_FM_DT" => DateTime::createFromFormat('d-M-Y', $data['policyStartDate'])->format('Y-m-d')."T00:00:00",
								"POL_TO_DT" => DateTime::createFromFormat('d-M-Y', $data['policyStartDate'])->add(new DateInterval('P1Y'))->sub(new DateInterval('P1D'))->format('Y-m-d')."T00:00:00",
								"POL_SUB_PRD_CODE" => $adminParams->get("sompoProdCode",""),
								"POL_DOB" => DateTime::createFromFormat('d-M-Y', $data['dateOfBirth'][0])->format('Y-m-d')."T00:00:00",
								"POL_GENDER" => $gender,
								"POL_OCC_CODE" => $occupation,
								"POL_RATING_EFF_DT" => $now,
								"TRIP_TYPE" => "MTCPC",
								"POL_MAR_STATUS" => $maritalStatus,
								"DRV_VALID_LIC_YN" => $data['validLicense'] == "Yes" ? 'Y' : 'N',
								"NOOFCLAIMS" => $data['claimsPast3Years'],
								"TOTALCLAIMAMT" => $data['claimAmount'],
								"MOTOROFD" => $OFD,
								"MOTORISEXCESSEXISTS" => "",
								"NO_OF_PERSONS" => count($data['Gender']),
								"OPTIONALCOVERAGELIST" => [],
								"VOLUNTARYEXCESSLIST" => [],
								"PROMO_CODE" => null,
								
								"POL_ASSURED_NAME" => self::sompoEncryptData(strtoupper($data['firstName'][$data["driverApplicant"]] . " " . $data['lastName'][$data["driverApplicant"]])),
								"POL_INSURED_ID" => self::sompoEncryptData(strtoupper($data["Nric"][$data["driverApplicant"]])),
								"POL_IDENTITY_TYPE" => $data["icType"][$data["driverApplicant"]],
								"POL_POSTAL_CODE" => $data["zipCode"],
								"POL_ADDR1" => strtoupper($data["blockNo"] . " " . $data["streetName"]),
								"POL_ADDR2" => strtoupper($data["unitNo"] . " " . $data["buildingName"]),
								"POL_ADDR3" => "",
								"POL_PHONE" => self::sompoEncryptData($data["mobileNo"][0]),
								"POL_EMAIL" => self::sompoEncryptData(strtoupper($data["Email"][$data["driverApplicant"]])),
								"PREVIOUSINSURER" => strtoupper($data["currentInsurer"]),
								"PREVIOUSPOLICYNO" => $data["previousPolicyNo"] == "" ? null : strtoupper($data["previousPolicyNo"]),
								"PREVIOUSREGISTRATIONNO" => $data["previousRegNo"] == "" ? null: strtoupper($data["previousRegNo"]),
								
								"VEHICLEINFO" => (object)[
										"VEH_BODY_TYPE" => $vehData["bodyType"],
										"VEH_CC" =>  $vehData["vehCc"],
										"VEH_COE_FLG" => "Y",
										"VEH_INSURED_DRIVING_YN" => "Y",
										"VEH_MAKE_CODE" => $vehData["makeCode"],
										"VEH_MODEL_CODE" => $vehData["modelCode"],
										"VEH_NCD_PROT_ELIG" => $ncdPro,
										"VEH_NO_DRV" => count($data['Gender']),
										"VEH_NO_PASS" => $vehData["passengerNo"],
										"VEH_PRV_CLM_FREE_YR" => $data['NCDPoints'] == 0 ? "" : $data['NCDPoints']/10,
										"VEH_REGN_NO" => strtoupper($data['vehicleRegNo']),
										"VEH_REGN_YEAR" => $data["carRegisterYear"],
										"VEH_SPOUSE_DRIVING_YN" => $isSpouse,
										"VEH_YEAR" => $data["carMakeYear"],
										"VEH_ONT_PARKING" => $data['companyRegistered'] == "Yes" ? 'Y' : 'N',
										"VEH_CERT_EXP_DT" => DateTime::createFromFormat('d-M-Y', $data['coeExpireDate'])->format('Y-m-d')."T00:00:00",
										"REASON_FOR_NCD" => $data["ncdReason"],
										"IS_NAMEDRIVERS_TERMS_AGREED" => $data["agreeDeclaration"] == "true" ? "Y" : "N",
										"NCD_DISC_CODE" => $ncdCode,
										"VEH_ENGINE_NO" => strtoupper($data['vehicleEngineNo']),
										"VEH_CHASSIS_NO" => strtoupper($data['vehicleChassisNo']),
										"VEH_HP_DTLS" => $data["loan"] == "No" ? "" : strtoupper($data['loanCompany'])
								],
								"DRIVERSLIST" => $driverArray,
								"PLANLIST" => array((object)["PLANCODE" => $data['plan_id']])
						]
				]
		];
		
		if($data["currentInsurer"] == "null") {
			unset($sompoPolicyRequestObj->serviceRequestObject->CURRENTPOLICY->PREVIOUSINSURER);
			unset($sompoPolicyRequestObj->serviceRequestObject->CURRENTPOLICY->PREVIOUSPOLICYNO);
			unset($sompoPolicyRequestObj->serviceRequestObject->CURRENTPOLICY->PREVIOUSREGISTRATIONNO);
		}
		
		return $sompoProposalRequestObj;
	}
	
	public static function sompoPolicyRequestData($data) {
		$adminParams = JComponentHelper::getParams("com_insurediymotor");
		
		$gender = $data['Gender'][0] == "MALE" ? 'M' : 'F';
		$maritalStatus = $data['maritalStatus'][0] == "Single" ? 'S' : 'M';
		$occupation = self::sompoGetOccupationCode($data["Occupation"][0]);
		$date = new DateTime('now');
		$date->setTimezone(new DateTimeZone('Asia/Singapore'));
		$now = $date->format("Y-m-d\TH:i:s.u+08:00");
		$OFD = $data['NCDPoints'] >= 30 ? 5 : 0;
		$ncdPro = $data['NCDPoints'] >= 30 && $data['plan_id'] != "EDF" ? "Y" : "N";
		$vehData = self::sompoGetVehicleData($data["vehVariantSompo"]);
		$ncdCode = $data['NCDPoints']== 0 ? "" : "MTNCD0".$data['NCDPoints'];
		$driverArray = array();
		$isSpouse = "N";
		
		foreach($data["Gender"] as $key => $value) {
			$driverArray[] = (object)[
					"DRV_SEX" => $data['Gender'][$key] == "MALE" ? 'M' : 'F',
					"DRV_MAR_STATUS" => $data['maritalStatus'][$key] == "Single" ? 'S' : 'M',
					"DRV_DOB" => DateTime::createFromFormat('d-M-Y', $data['dateOfBirth'][$key])->format('Y-m-d')."T00:00:00",
					"DRV_EXP" => $data['yearsOfDrivingExp'][$key],
					"DRV_OCC_CODE" => self::sompoGetOccupationCode($data["Occupation"][$key]),
					"DRV_OCC_DESC" => null,
					"NOOFCLAIMS" => $key == 0 ? $data['claimsPast3Years'] : "0",
					"TOTALCLAIMAMT" => $key == 0 ? $data['claimAmount'] : "0",
					"DRV_RATING_APPL_YN" => $key == $data["driverApplicant"]? "Y" : "N",
					"DRV_TRAF_POINTS" => $data['demeritPoint'][$key],
					"DRV_NAME" => self::sompoEncryptData(strtoupper($data['firstName'][$key] . " " . $data['lastName'][$key])),
					"DRV_CITIZENSHIP" =>  $data['icType'][$key],
					"DRV_ID" => self::sompoEncryptData(strtoupper($data['Nric'][$key])),
					"DRV_RELATION" =>  $data['relationshipWithMainDriver'][$key]
			];
			
			if($data['relationshipWithMainDriver'][$key]== "S") {
				$isSpouse = "Y";
			}
		}
		
		$planList = array((object)[
				"PLANCODE" => $data["plan_id"]
		// 				"PLAN_ORDER" => $data["plan_order"],
		// 				"PLAN_CATEGORY" => $data["plan_category"],
		// 				"PLANXRISKLIST" => $data["plan_risk"]
		]);
		
		$sompoPolicyRequestObj = (object)[
				"applicationId" => $adminParams->get("sompoAppId",""),
				"userId" => $adminParams->get("sompoUserId",""),
				"serviceId" => "createPolicy",
				"requestUniqueId" => "INSDIY-POLICY-".self::guid(),
				"serviceRequestObject" => (object)[
						"CURRENTPOLICY" => (object)[
								"APPQUOTEPOLICYREFID" => $data['ref_id'],
								"POL_SC_CODE" => "MTPV01",
								"POL_FM_DT" => DateTime::createFromFormat('d-M-Y', $data['policyStartDate'])->format('Y-m-d')."T00:00:00",
								"POL_TO_DT" => DateTime::createFromFormat('d-M-Y', $data['policyStartDate'])->add(new DateInterval('P1Y'))->sub(new DateInterval('P1D'))->format('Y-m-d')."T00:00:00",
								"POL_SUB_PRD_CODE" => $adminParams->get("sompoProdCode",""),
								"POL_AGENT_REF_NO" => $data['sompo_agent_ref_id'],
								"POL_DOB" => DateTime::createFromFormat('d-M-Y', $data['dateOfBirth'][0])->format('Y-m-d')."T00:00:00",
								"POL_GENDER" => $gender,
								"POL_OCC_CODE" => $occupation,
								"POL_RATING_EFF_DT" => $now,
								"POL_WP_TXN_ID" => $data['transactionId'],
								"TRIP_TYPE" => "MTCPC",
								"POL_MAR_STATUS" => $maritalStatus,
								"DRV_VALID_LIC_YN" => $data['validLicense'] == "Yes" ? 'Y' : 'N',
								"NOOFCLAIMS" => $data['claimsPast3Years'],
								"TOTALCLAIMAMT" => $data['claimAmount'],
								"MOTOROFD" => $OFD,
								"MOTORISEXCESSEXISTS" => "",
								"NO_OF_PERSONS" => count($data['Gender']),
								"OPTIONALCOVERAGELIST" => [],
								"VOLUNTARYEXCESSLIST" => [],
								"PROMO_CODE" => null,
								
								"POL_ASSURED_NAME" => self::sompoEncryptData(strtoupper($data['firstName'][$data["driverApplicant"]] . " " . $data['lastName'][$data["driverApplicant"]])),
								"POL_INSURED_ID" => self::sompoEncryptData(strtoupper($data["Nric"][$data["driverApplicant"]])),
								"POL_IDENTITY_TYPE" => $data["icType"][$data["driverApplicant"]],
								"POL_POSTAL_CODE" => $data["zipCode"],
								"POL_ADDR1" => strtoupper($data["blockNo"]." ".$data["streetName"]),
								"POL_ADDR2" => strtoupper($data["unitNo"]." ".$data["buildingName"]),
								"POL_ADDR3" => "",
								"POL_PHONE" => self::sompoEncryptData($data["mobileNo"][0]),
								"POL_EMAIL" => self::sompoEncryptData(strtoupper($data["Email"][$data["driverApplicant"]])),
								"PREVIOUSINSURER" => strtoupper($data["currentInsurer"]),
								"PREVIOUSPOLICYNO" => strtoupper($data["previousPolicyNo"]),
								"PREVIOUSREGISTRATIONNO" => strtoupper($data["previousRegNo"]),
								
								"POL_PREMIUMBEFORETAX" => number_format($data["plan_premium_before_tax"], 2, '.', ''),
								"POL_PLANTAXAMOUNT" => number_format($data["plan_premium_tax_amt"], 2, '.', ''),
								"POL_PLANTOTALPREMIUM" => number_format($data["plan_premium"], 2, '.', ''),
								
								"VEHICLEINFO" => (object)[
										"VEH_BODY_TYPE" => $vehData["bodyType"],
										"VEH_CC" =>  $vehData["vehCc"],
										"VEH_COE_FLG" => "Y",
										"VEH_INSURED_DRIVING_YN" => "Y",
										"VEH_MAKE_CODE" => $vehData["makeCode"],
										"VEH_MODEL_CODE" => $vehData["modelCode"],
										//"VEH_HP_DTLS" => "",
										"VEH_NCD_PROT_ELIG" => $ncdPro,
										"VEH_NO_DRV" => count($data['Gender']),
										"VEH_NO_PASS" => $vehData["passengerNo"],
										"VEH_PRV_CLM_FREE_YR" => $data['NCDPoints'] == 0 ? "" : $data['NCDPoints']/10,
										"VEH_REGN_NO" => strtoupper($data['vehicleRegNo']),
										"VEH_REGN_YEAR" => $data["carRegisterYear"],
										"VEH_SPOUSE_DRIVING_YN" => $isSpouse,
										"VEH_YEAR" => $data["carMakeYear"],
										"VEH_ONT_PARKING" => $data['companyRegistered'] == "Yes" ? 'Y' : 'N',
										"VEH_CERT_EXP_DT" => DateTime::createFromFormat('d-M-Y', $data['coeExpireDate'])->format('Y-m-d')."T00:00:00",
										"REASON_FOR_NCD" => $data["ncdReason"],
										"IS_NAMEDRIVERS_TERMS_AGREED" => $data["agreeDeclaration"] == "true" ? "Y" : "N",
										"NCD_DISC_CODE" => $ncdCode,
										"VEH_ENGINE_NO" => strtoupper($data['vehicleEngineNo']),
										"VEH_CHASSIS_NO" => strtoupper($data['vehicleChassisNo']),
										"VEH_HP_DTLS" => $data["loan"] == "No" ? "" : strtoupper($data['loanCompany'])
								],
								"DRIVERSLIST" => $driverArray,
								"PLANLIST" => $planList
						]
				]
		];
		
		if($data["currentInsurer"] == "null") {
			unset($sompoPolicyRequestObj->serviceRequestObject->CURRENTPOLICY->PREVIOUSINSURER);
			unset($sompoPolicyRequestObj->serviceRequestObject->CURRENTPOLICY->PREVIOUSPOLICYNO);
			unset($sompoPolicyRequestObj->serviceRequestObject->CURRENTPOLICY->PREVIOUSREGISTRATIONNO);
		}
		
		return $sompoPolicyRequestObj;
	}
	
	// *** MSIG FUNCTIONS ***
	public static function getMSIGAccessToken() {
		$adminParams = JComponentHelper::getParams("com_insurediymotor");
		
		$username = $adminParams->get("msigUsername","");
		$password = $adminParams->get("msigPassword","");
		$credential = array(
				"grant_type" => "client_credentials"
		);
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, $adminParams->get("msigTokenLink",""));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'grant_type: '));
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($credential));
		
		$server_output = curl_exec ($ch);
		
		curl_close($ch);
		
		return json_decode($server_output);
	}
	
	public static function validateInputChanged($driverDb, $input, $flag) {
		if($flag) {
			// if($input["mobileNo"][0] != $driverDb->quote_driver_contact) {
			// 	return false;
			// }
			// if($input["Email"][0] != explode(",", $driverDb->quote_driver_email)[0]) {
			// 	return false;
			// }
			// if($input["icType"][0] != explode(",", $driverDb->quote_driver_nric_type)[0]) {
			// 	return false;
			// }
			// if($input["Nric"][0] != explode(",", $driverDb->quote_driver_nric)[0]) {
			// 	return false;
			// }
		}
		if($input["policyStartDate"] != $driverDb->quote_policy_start_date) {
			return false;
		}
		if($input["carMake"] != $driverDb->quote_car_make) {
			return false;
		}
		if($input["carModel"] != $driverDb->quote_car_model) {
			return false;
		}
		// if(implode(",", $input["relationshipWithMainDriver"]) != $driverDb->quote_driver_main_driver_relation) {
		// 	return false;
		// }
		if(implode(",", $input["dateOfBirth"]) != $driverDb->quote_driver_dob) {
			return false;
		}
		// if(implode(",", $input["Gender"]) != $driverDb->quote_driver_gender) {
		// 	return false;
		// }
		// if(implode(",", $input["maritalStatus"]) != $driverDb->quote_driver_marital_status) {
		// 	return false;
		// }
		if(implode(",", $input["yearsOfDrivingExp"]) != $driverDb->quote_driver_exp) {
			return false;
		}
		// if($input["demeritPoint"][0] != explode(",", $driverDb->quote_driver_demerit_point)[0]) {
		// 	return false;
		// }
		if(implode(",", $input["Occupation"]) != $driverDb->quote_driver_occ) {
			return false;
		}
		if(implode(",", $input["Industry"]) != $driverDb->quote_driver_industry) {
			return false;
		}
		// if($input["claimsPast3Years"] != $driverDb->quote_driver_claim_no) {
		// 	return false;
		// }
		// if($input["coeExpireDate"] != $driverDb->quote_car_coe_expire_date) {
		// 	return false;
		// }
		// if($input["claimAmount"] != $driverDb->quote_driver_claim_amount) {
		// 	return false;
		// }
		// if($input["offPeakCar"] != $driverDb->quote_off_peak) {
		// 	return false;
		// }
		if($input["vehicleRegNo"] != $driverDb->quote_car_reg_no) {
			return false;
		}
		if($input["vehicleValue"] != $driverDb->quote_car_value) {
			return false;
		}
		// if($input["highestOffencePoint"] != $driverDb->quote_highest_offence_point) {
		// 	return false;
		// }
		// if($input["offencePoint"] != $driverDb->quote_offence_point) {
		// 	return false;
		// }
		// if($input["nightParking"] != $driverDb->quote_night_parking) {
		// 	return false;
		// }
		// if($input["chinaExtension"] != $driverDb->quote_china_extension) {
		// 	return false;
		// }
		if($input["coverType"] != $driverDb->quote_cover_type) {
			return false;
		}
		if($input["vehicleEngineCc"] != $driverDb->quote_car_engine_cc) {
			return false;
		}
		if($input["carBodyType"] != $driverDb->quote_car_body_type) {
			return false;
		}
		if($input["carMakeYear"] != $driverDb->quote_car_make_year) {
			return false;
		}
		if($input["NCDPoints"] != $driverDb->quote_car_ncd) {
			return false;
		}
		// if($input["meritCert"] != $driverDb->quote_driver_merit_cert) {
		// 	return false;
		// }
		// if($input["ncdPro"] != $driverDb->quote_plan_ncdpro) {
		// 	return false;
		// }
		// if($input["los"] != $driverDb->quote_plan_los) {
		// 	return false;
		// }
		// if($input["workshop"] != $driverDb->quote_plan_workshop) {
		// 	return false;
		// }
		if($input["currentInsurer"] != $driverDb->quote_driver_current_insurer) {
			// if($input["currentInsurer"] == "NCD000016" || $input["currentInsurer"] == "NCD000017" || $input["currentInsurer"] == "NCD000030") {
			// 	return false;
			// }
			return false;
		}

		return true;
	}
	
	public static function validateMsigQuotation($data) {
		$db = JFactory::getDbo();
		$user = JFactory::getUser();
		
		$query = $db->getQuery(TRUE)
		->select("*")
		->from("#__insure_motor_quote_master")
		->where("userId = " . $db->quote($user->id) . " AND " . "quote_deleted_time IS NULL AND quote_payment_txn_id= ''");
		$db->setQuery($query);
		$quoteData = $db->loadObject();
		
		if(!$quoteData) {
			return false;
		}
		
		// 		$query = $db->getQuery(TRUE)
		// 		->select("*")
		// 		->from("#__insure_motor_driver_info")
		// 		->where("quote_master_id = " . $quoteData->id);
		// 		$db->setQuery($query);
		// 		$driverData = $db->loadObject();
		
		$query = $db->getQuery(TRUE)
		->select("*")
		->from("#__insure_motor_quote_msig")
		->where("quote_master_id = " . $db->quote($quoteData->id));
		$db->setQuery($query);
		$msigData = $db->loadObject();
		
		if(!$msigData) {
			return false;
		}
		
		date_default_timezone_set('Asia/Singapore');
		
		$date = new DateTime($msigData->msig_quote_valid_through);
		$now = new DateTime("");
		//return date('m/d/Y H:i:s', strtotime($date));
		
		if($date->diff($now)->format('%R') == "+") {
			return false;
		}
		
		if($data["vehVariantMsig"] != $msigData->quote_car_variant) {
			return false;
		}
		
		// 		if($data["los"] != $quoteData->quote_plan_los) {
		// 			return false;
		// 		}
		
		if($data["ncdPro"] != $quoteData->quote_plan_ncdpro) {
			return false;
		}
		
		// 		if($data["workshop"] != $quoteData->quote_plan_workshop) {
		// 			return false;
		// 		}
		
		if($quoteData->quote_pre_quote_change_flag == "true") {
			return false;
		}
		
		return $msigData;
	}
	
	private static function msigGetOccupationCode($occ) {
		return "INDO";
	}
	
	private static function msigGetVehicleData($variant) {
		$db = JFactory::getDBO();
		
		$query = $db->getQuery(TRUE)
		->select("*")
		->from("#__insure_motor_make_model_msig")
		->where("model_code = ".$db->quote($variant));
		$db->setQuery($query);
		$carModelData = $db->loadObject();

		$result = array(
				"modelCode" => $variant,
				"vehCc" => $carModelData->model_cc,
				"passengerNo" => 5
		);
		
		return $result;
	}
	
	// 	private static function msigGetPrevInsurerData($insurer) {
	// 		return $insurer == "" ? "" :"AXA";
	// 	}
	
	public static function msigQuoteRequestData($data) {
		$adminParams = JComponentHelper::getParams("com_insurediymotor");
		
		$date = new DateTime('now');
		$date->setTimezone(new DateTimeZone('Asia/Singapore'));
		$now = $date->format("Y-m-d");
		$vehData = self::msigGetVehicleData($data["vehVariantMsig"]);
		$driverArray = array();
		
		$driverArray = (object)[
					"gender" => $data['Gender'][0] == "MALE" ? 'M' : 'F',
					"maritalStatus" => $data['maritalStatus'][0] == "Single" ? 'S' : 'M',
					"dateOfBirth" => DateTime::createFromFormat('d-M-Y', $data['dateOfBirth'][0])->format('Y-m-d'),
					"mobileNo" => "",
					"email" => "",
					"identificationType" => ($data['icType'][0] == "N" || $data['icType'][0] == "B") ? "NRIC" : "FIN",
					"identificationNo" => $data['Nric'][0],
					"drivingExperience" => $data['yearsOfDrivingExp'][0] >= 3 ? 3 : $data['yearsOfDrivingExp'][0],
					"offenceFree" => ($data["meritCert"] == "true" && $data['NCDPoints'] >= 30) ? "Y" : "N",
					"occupationType" => $data["occupationType"][0],
					"noOfClaimsInLast3Years" => $data['claimsPast3Years'],
					"claimAmountInLast3Years" => $data['claimAmount'],
					"currencyCode" => "SGD"					
		];
		
		
		$msigQuoteRequestObj = (object)[
				"type" => "NB",
				"startDate" => DateTime::createFromFormat('d-M-Y', $data['policyStartDate'])->format('Y-m-d'),
				"endDate" => DateTime::createFromFormat('d-M-Y', $data['policyStartDate'])->modify('+365 days')->format('Y-m-d'),
				"quoteReqDate" => $now,
				"referenceCode" => $adminParams->get("msigRefId",""),
				"coverage" => "C",
				"promoCode" => "",
				"planCode" => "",
				"driver" => $driverArray,
				"vehicle" => (object)[
								"offPeakCar" => $data["offPeakCar"] == "true" ? "Y" : "N",
								"yearOriginalReg" => $data["carRegisterYear"],
								"makeModelCode" => $vehData["modelCode"],
								"registrationNum" => $data['vehicleRegNo'],
								"seatNo" => "5",
								"ncd" => $data['NCDPoints'],
								"ncdProtectionIndicator" => ($data["ncdPro"] == "Yes" && $data['NCDPoints'] == 50) ? "Y" : "N"
				]
		];
		
		return $msigQuoteRequestObj;
	}
	
	public static function msigApplicationRequestData($data) {
		$adminParams = JComponentHelper::getParams("com_insurediymotor");
		
		$date = new DateTime('now');
		$date->setTimezone(new DateTimeZone('Asia/Singapore'));
		$now = $date->format("Y-m-d");
		$vehData = self::msigGetVehicleData($data["vehVariantMsig"]);
		$driverArray = array();
		
		if($data["loan"] == "Yes") {
			$db = JFactory::getDbo();
			$query = $db->getQuery(TRUE)
			->select("mortgagee_name")
			->from('#__insure_motor_loan_company')
			->where("mortgagee_code = " . $db->quote($data["loanCompany"]));
			$db->setQuery($query);
			$bankName = $db->loadResult();
			$bankCode = $data["loanCompany"];
		}
		else {
			$bankName = "";
			$bankCode = "NA";
		}
		
		// 		file_put_contents('./test-msig.html', $bankName." - ".$bankCode);
		
		$driverArray = (object)[
				"lastName" => $data["lastName"][0],
				"firstName" => $data["firstName"][0],
				"gender" => $data['Gender'][0] == "MALE" ? 'M' : 'F',
				"maritalStatus" => $data['maritalStatus'][0] == "Single" ? 'S' : 'M',
				"dateOfBirth" => DateTime::createFromFormat('d-M-Y', $data['dateOfBirth'][0])->format('Y-m-d'),
				"mobileNo" => $data["mobileNo"][0],
				"email" => $data["Email"][0],
				"identificationType" => ($data['icType'][0] == "N" || $data['icType'][0] == "B") ? "NRIC" : "FIN",
				"identificationNo" => $data['Nric'][0],
				"drivingExperience" => $data['yearsOfDrivingExp'][0] >= 3 ? 3 : $data['yearsOfDrivingExp'][0],
				"nationality" => $data["driverNationality"][0],
				"race" => $data["driverRace"][0],
				"hasCurrentMotorPolicy" => $data["currentInsurer"] != "" ? "Y" : "N",
				"previousInsurer" => $data["currentInsurer"],
				"clientConsent" => "Y",
				"offenceFree" => ($data["meritCert"] == "true" && $data['NCDPoints'] >= 30) ? "Y" : "N",
				"occupationType" => $data["occupationType"][0],
				"noOfClaimsInLast3Years" => $data['claimsPast3Years'],
				"claimAmountInLast3Years" => $data['claimAmount'],
				"currencyCode" => "SGD",
				"address" => (object)[
						"streetName" => $data['streetName'],
						"postalCode" => $data['zipCode'],
						"blockNo" => $data['blockNo'],
						"unitNo" => $data['unitNo'],
						"buildingName" => $data['buildingName'],
						"countryOfResidence" => $data['country']
				]
		];
		
		$msigApplicationRequestObj= (object)[
				"type" => "NB",
				"quoteReference" => $data["ref_id"],
				"startDate" => DateTime::createFromFormat('d-M-Y', $data['policyStartDate'])->format('Y-m-d'),
				"endDate" => DateTime::createFromFormat('d-M-Y', $data['policyStartDate'])->modify('+365 days')->format('Y-m-d'),
				"quoteReqDate" => $now,
				"referenceCode" => $adminParams->get("msigRefId",""),
				"coverage" => "C",
				"promoCode" => "",
				"planCode" => $data["plan_id"],
				"finalPremium" => $data["plan_premium"],
				"currencyCode" => "SGD",
				"driver" => $driverArray,
				"vehicle" => (object)[
						"offPeakCar" => $data["offPeakCar"] == "true" ? "Y" : "N",
						"yearOriginalReg" => $data["carRegisterYear"],
						"makeModelCode" => $vehData["modelCode"],
						"registrationNum" => $data['vehicleRegNo'],
						"seatNo" => "5",
						"ncd" => $data['NCDPoints'],
						"ncdProtectionIndicator" => ($data["ncdPro"] == "Yes" && $data['NCDPoints'] == 50) ? "Y" : "N",
						"engineNo" => $data['vehicleEngineNo'],
						"chassisNo" => $data['vehicleChassisNo'],
						"mortgageeCode" => $bankCode,
						"mortgageeName" => $bankName
				]
		];
		
		return $msigApplicationRequestObj;
	}
	
	public static function msigPolicyRequestData($data) {
		$adminParams = JComponentHelper::getParams("com_insurediymotor");
		
		$date = new DateTime('now');
		$date->setTimezone(new DateTimeZone('Asia/Singapore'));
		$now = $date->format("Y-m-d");
		$vehData = self::msigGetVehicleData($data["vehVariantMsig"]);
		$driverArray = array();
		
		if($data["loan"] == "Yes") {
			$db = JFactory::getDbo();
			$query = $db->getQuery(TRUE)
			->select("mortgagee_name")
			->from('#__insure_motor_loan_company')
			->where("mortgagee_code = " . $db->quote($data["loanCompany"]));
			$db->setQuery($query);
			$bankName = $db->loadResult();
			$bankCode = $data["loanCompany"];
		}
		else {
			$bankName = "";
			$bankCode = "NA";
		}
		
		$driverArray = (object)[
				"lastName" => $data["lastName"][0],
				"firstName" => $data["firstName"][0],
				"gender" => $data['Gender'][0] == "MALE" ? 'M' : 'F',
				"maritalStatus" => $data['maritalStatus'][0] == "Single" ? 'S' : 'M',
				"dateOfBirth" => DateTime::createFromFormat('d-M-Y', $data['dateOfBirth'][0])->format('Y-m-d'),
				"mobileNo" => $data["mobileNo"][0],
				"email" => $data["Email"][0],
				"identificationType" => ($data['icType'][0] == "N" || $data['icType'][0] == "B") ? "NRIC" : "FIN",
				"identificationNo" => $data['Nric'][0],
				"drivingExperience" => $data['yearsOfDrivingExp'][0] >= 3 ? 3 : $data['yearsOfDrivingExp'][0],
				"nationality" => $data["driverNationality"][0],
				"race" => $data["driverRace"][0],
				"hasCurrentMotorPolicy" => $data["currentInsurer"] != "" ? "Y" : "N",
				"previousInsurer" => $data["currentInsurer"],
				"clientConsent" => "Y",
				"offenceFree" => ($data["meritCert"] == "true" && $data['NCDPoints'] >= 30) ? "Y" : "N",
				"occupationType" => $data["occupationType"][0],
				"noOfClaimsInLast3Years" => $data['claimsPast3Years'],
				"claimAmountInLast3Years" => $data['claimAmount'],
				"currencyCode" => "SGD",
				"address" => (object)[
						"streetName" => $data['streetName'],
						"postalCode" => $data['zipCode'],
						"blockNo" => $data['blockNo'],
						"unitNo" => $data['unitNo'],
						"buildingName" => $data['buildingName'],
						"countryOfResidence" => $data['country']
				]
		];
		
		$msigPolicyRequestObj = (object)[
				"type" => "NB",
				"applicationReference" => $data["ref_id"],
				"startDate" => DateTime::createFromFormat('d-M-Y', $data['policyStartDate'])->format('Y-m-d'),
				"endDate" => DateTime::createFromFormat('d-M-Y', $data['policyStartDate'])->modify('+365 days')->format('Y-m-d'),
				"referenceCode" => $adminParams->get("msigRefId",""),
				"coverage" => "C",
				"promoCode" => "",
				"planCode" => $data["plan_id"],
				"finalPremium" => $data["plan_premium"],
				"currencyCode" => "SGD",
				"driver" => $driverArray,
				"vehicle" => (object)[
						"offPeakCar" => $data["offPeakCar"] == "true" ? "Y" : "N",
						"yearOriginalReg" => $data["carRegisterYear"],
						"makeModelCode" => $vehData["modelCode"],
						"registrationNum" => $data['vehicleRegNo'],
						"seatNo" => "5",
						"ncd" => $data['NCDPoints'],
						"ncdProtectionIndicator" => ($data["ncdPro"] == "Yes" && $data['NCDPoints'] == 50) ? "Y" : "N",
						"engineNo" => $data['vehicleEngineNo'],
						"chassisNo" => $data['vehicleChassisNo'],
						"mortgageeCode" => $bankCode,
						"mortgageeName" => $bankName
				]
		];
		
		return $msigPolicyRequestObj;
	}
	
	// *** HLA FUNCTIONS ***
	private static function hlaGetVehicleData($variant) {
		$db = JFactory::getDBO();
		
		$query = $db->getQuery(TRUE)
		->select("*")
		->from("#__insure_motor_make_model_hla")
		->where("model_code = ".$db->quote($variant));
		$db->setQuery($query);
		$carModelData = $db->loadObject();
		
		$result = array(
				"id" => $carModelData->id,
				"makeCode" => $carModelData->make_code,
				"modelCode" => $variant,
				"vehCc" => $carModelData->model_cc,
				"passengerNo" => 5
		);
		
		return $result;
	}
	private static function aigGetVehicleData($variant) {
		$db = JFactory::getDBO();

		$query = $db->getQuery(TRUE)
		->select("*")
		->from("#__insure_motor_make_model_aig")
		->where("id = ".$db->quote($variant));
		$db->setQuery($query);
		$carModelData = $db->loadObject();

		// $query = $db->getQuery(TRUE)
		// ->select("*")
		// ->from("#__insure_motor_make_master")
		// ->where("id = ".$db->quote($carModelData->make_name)); // was make_code
		// $db->setQuery($query);
		// $carMakeData = $db->loadObject();
		
		$result = array(
				"id" => $variant,
				//"makeCode" => $carModelData->make_code,
				"makeName" => $carModelData->make_name, // was  $carMakeData->make_name
				//"modelCode" => $variant,
				"modelName" => $carModelData->model_name,
				"vehCc" => $carModelData->model_cc,
				"passengerNo" => 5
		);
		
		return $result;
	}

	private static function awGetVehicleData($variant) {
		$db = JFactory::getDBO();

		$query = $db->getQuery(TRUE)
		->select("*")
		->from("#__insure_motor_make_model_aw")
		->where("id = ".$db->quote($variant));
		$db->setQuery($query);
		$carModelData = $db->loadObject();

		// $query = $db->getQuery(TRUE)
		// ->select("*")
		// ->from("#__insure_motor_make_master")
		// ->where("id = ".$db->quote($carModelData->make_name)); // was make_code
		// $db->setQuery($query);
		// $carMakeData = $db->loadObject();
		
		$result = array(
				"id" => $variant,
				//"makeCode" => $carModelData->make_code,
				"makeName" => $carModelData->make_name, // was  $carMakeData->make_name
				"modelCode" => $carModelData->code,
				"modelYear" => $carModelData->year,
				"modelName" => $carModelData->model_name,
				"vehCc" => $carModelData->model_cc,
				"passengerNo" => $carModelData->passenger,
				"modelBody" => $carModelData->body,
		);
		
		return $result;
	}
	
	private static function pinganGetVehicleData($variant) {
		$db = JFactory::getDBO();

		$query = $db->getQuery(TRUE)
		->select("*")
		->from("#__insure_motor_make_model_pingan")
		->where("id = ".$db->quote($variant));
		$db->setQuery($query);
		$carModelData = $db->loadObject();

		
		$result = array(
				"id" => $variant,
				"makeCode" => $carModelData->make_id,
				"makeName" => $carModelData->make_name, // was  $carMakeData->make_name
				"modelName" => $carModelData->model_name,
				"modelCode" => $carModelData->model_id,
				"passengerNo" => 5
		);
		
		return $result;
	}

	private static function hlaStoreVehicleData($vehData) {
		$db = JFactory::getDBO();
		
		$vehObj = (object) array(
				"id" => $vehData["id"],
				"model_cc" => $vehData["vehCc"]
		);
		
		$check = $db->updateObject("#__insure_motor_make_model_hla", $vehObj, "id");
		
		return $check;
	}
	
	private static function hlaGetOccupationData($occ_code) {
		$db = JFactory::getDBO();
		
		$query = $db->getQuery(TRUE)
		->select("occ_hla_code")
		->from("#__insure_motor_occupation")
		->where("occ_code = ".$db->quote($occ_code));
		$db->setQuery($query);
		$occData = $db->loadResult();
		
		return $occData;
	}
	
	public static function hlaBrokerLogin() {
		$adminParams = JComponentHelper::getParams("com_insurediymotor");
		
		$agent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)';
		
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, 'https://www.hlas.com.sg/HLASPartners/Login.aspx?ReturnUrl=%2fHLASPartners');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HEADER, 1);
		curl_setopt($ch, CURLOPT_USERAGENT, $agent);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		
		// execute!
		$response = curl_exec($ch);
		
		if($response !== false) {
			$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
			$header = substr($response, 0, $header_size);
			$body = substr($response, $header_size);
			$html = str_get_html($body);
			
			//Grab cookies out from response
			preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $header, $matches);
			$cookies = array();
			
			foreach($matches[1] as $item) {
				$cookie = self::parse_qs($item);
				$cookies = array_merge($cookies, $cookie);
			}
			
			// do anything you want with your response
			$params = array(
					"ScriptManager" => 'dnn$ctr1409$dnn$ctr1409$Login_UPPanel|dnn$ctr1409$Login$Login_DNN$cmdLogin',
					'dnn$ctr1409$Login$Login_DNN$txtUsername' => $adminParams->get("hlaUsername",""),
					'dnn$ctr1409$Login$Login_DNN$txtPassword' => $adminParams->get("hlaPassword",""),
					"__ASYNCPOST" => "true",
					"RadAJAXControlID" => "dnn_ctr1409_Login_UP"
			);
			
			foreach ($html->find("input[type=hidden]") as $node) {
				$params[$node->name] = $node->value;
			}
			
			$html->clear();
			
			$params["StylesheetManager_TSSM"] = ";Telerik.Web.UI, Version=2013.2.717.40, Culture=neutral, PublicKeyToken=121fae78165ba3d4:en-US:dae8717e-3810-4050-96d3-31018e70c6e4:45085116:27c5704c";
			$params["ScriptManager_TSM"] = ";;System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35:en:93a6b8ed-f453-4cc5-9080-8017894b33b0:ea597d4b:b25378d2;Telerik.Web.UI, Version=2013.2.717.40, Culture=neutral, PublicKeyToken=121fae78165ba3d4:en:dae8717e-3810-4050-96d3-31018e70c6e4:16e4e7cd:f7645509:ed16cbdc";
			$params["__EVENTTARGET"] = 'dnn$ctr1409$Login$Login_DNN$cmdLogin';
			
			$query = '';
			
			foreach ($params as $param => $value) {
				$query .= urlencode($param).'='.urlencode($value) .'&';
			}
			
			//Send Quotation Request
			curl_setopt($ch, CURLOPT_URL, 'https://www.hlas.com.sg/HLASPartners/Login.aspx?ReturnUrl=%2fHLASPartners');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, substr($query, 0, -1));
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('content-type: application/x-www-form-urlencoded'));
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_HEADER, 1);
			curl_setopt($ch, CURLOPT_USERAGENT, $agent);
			
			$cookie_string="";
			
			foreach( $cookies as $key => $value ) {
				$cookie_string .= "$key=$value;";
			};
			
			curl_setopt($ch, CURLOPT_COOKIE, $cookie_string);
			
			$server_output = curl_exec ($ch);
			
			$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
			$header = substr($server_output, 0, $header_size);
			$body = substr($server_output, $header_size);
			
			//Grab cookies out from response
			preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $header, $matches);
			
			foreach($matches[1] as $item) {
				$cookie = self::parse_qs($item);
				$cookies = array_merge($cookies, $cookie);
			}
			
			curl_close($ch);
			
			return $cookies;
		}
		else {
			return false;
		}
	}
	public static function requestAigQuotations($data, $save) {
		require_once 'simple_html_dom.php';
		$adminParams = JComponentHelper::getParams("com_insurediymotor");
		$vehData = $data["vehVariantAig"];
		$carData = InsureDIYMotorHelper::aigGetVehicleData($vehData);
		$vehMake = $carData["makeName"];
		$vehModel = $carData["modelName"];
		$vehYear = $data["carMakeYear"];
		$coverType = $data["coverType"];
		$vehCc = $carData["vehCc"];
		$driverExp = $data["yearsOfDrivingExp"][0];
		$birth = strtotime($data["dateOfBirth"][0]);
		$birthDate = date('Y-m-d',$birth);
		$now = new DateTime();
		$now = $now->format('Y-m-d'); 
		$age = date_diff(date_create($birthDate), date_create('today'))->y;
		$vehNcBonus = $data["NCDPoints"];
		$vehValue = $data["vehicleValue"];
		if($vehValue < 150000){
			$vehValue = 150000;
		}

		//exit(json_encode($carData, true));
		//login
		// $url = 'https://www-400.aig.com.hk/AIUEJB/b2b/AgentLogin.jsp';
		$username = $adminParams->get("aigUsername","");
		$password = $adminParams->get("aigPassword","");
		// $post = '{"sentData":"{\"mobile_no\":\"'.$username.'\",\"password\":\"'.$password.'\"}"}';
		// $post_login = InsureDIYMotorHelper::getCurlDataPost($url, $post);
		// $session_idx = json_decode($post_login, true);

		// // send session
		// $url = 'https://www-400.aig.com.hk/AIUEJB/b2b/AgentSession.jsp';
		// $post = '{"sentData":"{\"reg_mobile_no\":\"'.$username.'\",\"session_idx\":\"'.$session_idx["session_idx"].'\"}"}';
		// $res = InsureDIYMotorHelper::getCurlDataPost($url, $post);

		/* skip the login part, but still send the username with auto data */ 

		// send auto data
		//$url = 'https://buy.aig.com.hk/aigb2c/auto/ServerCall/GetQuote.jsp'; // public website
		$url = 'https://www-400.aig.com.hk/AIUEJB/b2b/ServerCall/Auto/GetQuote.jsp'; // B2B
		$post = '{"sentData":"{';
		// $post .= '\"session_idx\":\"'. $session_idx["session_idx"];
		$post .= '\"session_idx\":\"';       
		$post .= '\",\"mobile_no\":\"'.$username;
		$post .= '\",\"product_cd\":\"000080\",\"tran_id\":\"\",\"quote_no\":\"\",';
		$post .= '\"section_cd\":\"'.$coverType;
		$post .= '\",\"veh_make_name\":\"'. $vehMake;
		$post .= '\",\"uw_veh_mdl_name\":\"'. $vehModel;
		$post .= '\",\"veh_year_no\":\"'. $vehYear;
		$post .= '\",\"veh_body_type\":\"SALOON\",\"driver_age\":\"'.$age.'\",\"driver_birth_date\":\"'.$birthDate.'\",\"driver_exp_cnt\":\"'.$driverExp.'\",\"driver_gender_cd\":\"\",\"';
		$post .= 'kw_cc_no\":\"'. $vehCc;
		$post .= '\",\"no_clm_bonus_pct\":\"'. $vehNcBonus;
		if ($coverType == "01") {
			$post .= '\",\"risk_liab_amt\":\"'.$vehValue.'\",\"prdr_comm_amt\":\"0\",\"deducts\":[],\"pol_incept_date\":\"\",\"veh_chassis_no\":\"\",\"veh_engine_no\":\"\",\"intrst_name\":\"\",\"veh_mdl_name\":\"\",\"registration_no\":\"\",\"fax_no\":\"\",\"calc_prem_amt\":\"0\",\"comm_plan_selected\":\"PLANA\",\"after_adj_prem_amt\":\"0\",\"mib_amt\":\"0\",\"total_prem_amt\":\"0\",\"discount\":\"0\",\"prdr_comm_value\":\"0\",\"insured_name\":\"\",\"ins_type\":\"\",\"producer_cd\":\"017270M01\",\"business_channel\":\"B2B\",\"ref_date\":\"'.$now.'\"}"}';
		} else {
			$post .= '\",\"risk_liab_amt\":\"0\",\"prdr_comm_amt\":\"0\",\"deducts\":[],\"pol_incept_date\":\"\",\"veh_chassis_no\":\"\",\"veh_engine_no\":\"\",\"intrst_name\":\"\",\"veh_mdl_name\":\"\",\"registration_no\":\"\",\"fax_no\":\"\",\"calc_prem_amt\":\"0\",\"comm_plan_selected\":\"PLAN1-0601\",\"after_adj_prem_amt\":\"0\",\"mib_amt\":\"0\",\"total_prem_amt\":\"0\",\"discount\":\"0\",\"prdr_comm_value\":\"0\",\"insured_name\":\"\",\"ins_type\":\"\",\"producer_cd\":\"017270M01\",\"business_channel\":\"B2B\",\"ref_date\":\"'.$now.'\"}"}';
		}
		
		$header = "";
		$res = InsureDIYMotorHelper::getCurlDataPost($url, $post, $header);

		$planResult = json_decode($res, true);

		if($coverType == "01"){
			$parkingExcess = $planResult["deducts"][1]["ded_value"];
			$unamedDriver = $planResult["deducts"][2]["ded_value"];
			$youngDriver = $planResult["deducts"][3]["ded_value"];
			$inexperienceDriver = $youngDriver;
			$theftLoss = $planResult["deducts"][4]["ded_value"];
			$thirdParty = $planResult["deducts"][5]["ded_value"];
			$generalExcess = $planResult["deducts"][0]["ded_value"];
		} else {
			$parkingExcess = "0";
			$unamedDriver = "0";
			$youngDriver = "0";
			$inexperienceDriver = "0";
			$theftLoss = "0";
			$thirdParty = "0";
			$generalExcess = "0";
		}
		

		$plan = (object) array(
			"annualizedPremium" => $planResult["after_adj_prem_amt"],
			"mib" => $planResult["mib_amt"],
			"producerCommAmount" => $planResult["prdr_comm_amt"],
			"finalPremium" => $planResult["total_prem_amt"],
			"planName" => "AIG Motor Insurance",
			"productCode" => "AIG",
			"generalExcess" => $generalExcess,
			"theftLoss" => $theftLoss,
			"thirdParty" => $thirdParty,
			"youngDriver" => $youngDriver,
			"inexperienceDriver" => $inexperienceDriver,
			"unamedDriver" => $unamedDriver,
			"parkingDamage" => $parkingExcess
		);


		if (isset($planResult["total_prem_amt"]) && $planResult["total_prem_amt"] > 0 && $vehYear > (date("Y")-15) && $vehYear < (date("Y")-1)) {
			$result["success"] = true;
			if($coverType == "01" && $vehValue < 150000){
				$result["success"] = false;
			}
		} else {
			$result["success"] = false;
		}
	
		
		$result["plan_list"] = $plan;

		return $result;	
		
	}

	public static function requestAwQuotations($data, $save) {
		require_once 'simple_html_dom.php';
		$adminParams = JComponentHelper::getParams("com_insurediymotor");
		$username = $adminParams->get("awUsername","");
		$password = $adminParams->get("awPassword","");

		$vehData = $data["vehVariantAw"];
		$carData = InsureDIYMotorHelper::awGetVehicleData($vehData);
		$carMake = $carData["makeName"];
		$carKey = $carData["modelCode"];
		$carModel = $carData["modelName"];
		$carYear = $carData["modelYear"];
		$carBody = $carData["modelBody"];
		$carCc = $carData["vehCc"];
		$vehNcBonus = $data["NCDPoints"];
		$vehValue = $data["vehicleValue"];
		$coverType = $data["coverType"];
		$chinaExt = $data["chinaExtension"];
		$otNCD = $data["otherNCD"];
		if ($coverType == "01") {
			$cover = "C";
		} else {
			$cover = "T";
		}


		// go to quote page and find hash

		$url = 'https://ionline.alliedworldgroup.com.hk/eapp/app';
		$post = 'action=go_page&to_page=%2Fmotor%2Fmenu.jsp&module=&user='.$username.'&secure=true&portal=ionline&targetURL=%2Fncp%2Fhome&proposalform=&leaflet=';
		$header = 'Content-Type:application/x-www-form-urlencoded';
		$urlResult = InsureDIYMotorHelper::getCurlDataPost($url, $post, $header);
		
		$url = 'https://ionline.alliedworldgroup.com.hk/eapp/app';
		$post = 'action=quickquote&module=motor&to_page=';
		$header = 'Content-Type:application/x-www-form-urlencoded';
		$urlResult = InsureDIYMotorHelper::getCurlDataPost($url, $post, $header);
		$html = str_get_html($urlResult);
		$hashCount = count($html->find('input[name=hash_code]'));
		

		if ($hashCount < 1){
			// login
			$url = 'https://ionline.alliedworldgroup.com.hk/eapp/app';
			$post = 'action=login&to_page=&login_id='.$username.'&password='.$password;
			$header = 'Content-Type:application/x-www-form-urlencoded';
			$post_login = InsureDIYMotorHelper::getCurlDataPost($url, $post, $header);
		

			// go to quote page and find hash
			$url = 'https://ionline.alliedworldgroup.com.hk/eapp/app';
			$post = 'action=go_page&to_page=%2Fmotor%2Fmenu.jsp&module=&user='.$username.'&secure=true&portal=ionline&targetURL=%2Fncp%2Fhome&proposalform=&leaflet=';
			$header = 'Content-Type:application/x-www-form-urlencoded';
			$urlResult = InsureDIYMotorHelper::getCurlDataPost($url, $post, $header);

			$url = 'https://ionline.alliedworldgroup.com.hk/eapp/app';
			$post = 'action=quickquote&module=motor&to_page=';
			$header = 'Content-Type:application/x-www-form-urlencoded';
			$urlResult = InsureDIYMotorHelper::getCurlDataPost($url, $post, $header);
			$html = str_get_html($urlResult);
			$hashCode = $html->find('input[name=hash_code]', 0)->value;
			

		} else {
			$hashCode = $html->find('input[name=hash_code]', 0)->value;
		}
		
		
		// send car request
		$url = 'https://ionline.alliedworldgroup.com.hk/eapp/app';
		$post = 'module=motor&action=get_car_detail&car_key='.$carKey;
		$header = 'Content-Type:application/x-www-form-urlencoded';
		$urlResult = InsureDIYMotorHelper::getCurlDataPost($url, $post, $header);

		// send quote request
		if ($cover == "C") {
			$ce = '&china_ext='.$chinaExt;
		} else {
			$ce = "";
		}

		if ($vehNcBonus == "0") {
			$otherNCD = "&OthNCD=".$otNCD;
		} else {
			$otherNCD = "";
		}
		$url = 'https://ionline.alliedworldgroup.com.hk/eapp/app';
		$post = 'action=quickquoteresult&module=motor&to_page=&hash_code='.$hashCode.'&plan_code='.$cover.'&ncd='.$vehNcBonus.'&car_make='.$carMake.'&car_model='.$carModel.'&car_year='.$carYear.'&car_body_type='.$carBody.'&car_cc='.$carCc.'&car_desc='.$carKey.'&car_value_input='.$vehValue.$ce.$otherNCD;
		//exit($post);
		$header = 'Content-Type:application/x-www-form-urlencoded';
		$urlResult = InsureDIYMotorHelper::getCurlDataPost($url, $post, $header);
		$html = str_get_html($urlResult);

		if (count($html->find('table', 2)) > 0 && count($html->find('table', 2)->find('tr', 4)) > 0 && count($html->find('table', 2)->find('tr', 4)->find('td', 3)) > 0) {
			$finalPremium = InsureDIYMotorHelper::stripAw($html->find("table", 2)->find("tr", 4)->find("td", 3)->plaintext);
			$mib = InsureDIYMotorHelper::stripAw($html->find('table', 2)->find('tr', 2)->find('td', 3)->plaintext);
			if($cover == "C"){
				$generalExcess = InsureDIYMotorHelper::stripAw($html->find('table', 3)->find('tr', 3)->find('td', 3)->plaintext);
				$theftLoss = InsureDIYMotorHelper::stripAw($html->find('table', 3)->find('tr', 4)->find('td', 3)->plaintext);
				$thirdParty = InsureDIYMotorHelper::stripAw($html->find('table', 3)->find('tr', 5)->find('td', 3)->plaintext);
				$youngDriver = InsureDIYMotorHelper::stripAw($html->find('table', 3)->find('tr', 6)->find('td', 3)->plaintext);
				$inexperienceDriver = InsureDIYMotorHelper::stripAw($html->find('table', 3)->find('tr', 7)->find('td', 3)->plaintext);
				$unamedDriver = InsureDIYMotorHelper::stripAw($html->find('table', 3)->find('tr', 8)->find('td', 3)->plaintext);
				$parkingDamage = InsureDIYMotorHelper::stripAw($html->find('table', 3)->find('tr', 9)->find('td', 3)->plaintext);
			} else {
				$generalExcess = "0";
				$theftLoss = "0";
				$thirdParty = "0";
				$youngDriver = "0";
				$inexperienceDriver = "0";
				$unamedDriver = "0";
				$parkingDamage = "0";
			}
			$planName = "Motorguard";
			$result["success"] = true;
		} else {
			$finalPremium = 0;
			$mib = 0;
			$planName = "Manual Quotation";
			$result["success"] = false;
		}
		
		// logout
		// $url = 'https://ionline.alliedworldgroup.com.hk/eapp/app?action=logout';
		// $urlResult = InsureDIYMotorHelper::getCurlData($url);



		$plan = (object) array(
			"annualizedPremium" => "0",
			"mib" => $mib,
			"producerCommAmount" => "0",
			"finalPremium" => $finalPremium,
			"planName" => $planName,
			"productCode" => "AW",
			"generalExcess" => $generalExcess,
			"theftLoss" => $theftLoss,
			"thirdParty" => $thirdParty,
			"youngDriver" => $youngDriver,
			"inexperienceDriver" => $inexperienceDriver,
			"unamedDriver" => $unamedDriver,
			"parkingDamage" => $parkingDamage
		);
		
		$result["plan_list"] = $plan;

		return $result;	
		
	}

	public static function getCaptcha(){
		require_once 'simple_html_dom.php';
		$url = 'https://www.pingan.com.hk/home/generateCaptcha';
		$urlResult = InsureDIYMotorHelper::getCurlData($url);
		$result = [];
		$result["captcha"] = base64_encode($urlResult);
		echo json_encode($result);
	}

	public static function stripAw($string){
		$string = str_replace(',','', $string);
		$string = str_replace('$','', $string);

		return $string;
	}

	public static function validateCaptcha($captcha){
		require_once 'simple_html_dom.php';
		$session = JFactory::getSession();
		$url = 'https://www.pingan.com.hk/home/validateCaptcha';
		$post = 'validateValue='.$captcha.'&validateId=partnerLogin_captcha_code&validateError=ajaxCaptcha&extraData=';
		$header = 'Content-Type:application/x-www-form-urlencoded';
		$urlResult = InsureDIYMotorHelper::getCurlDataPost($url, $post, $header);
		$res = json_decode($urlResult, true);
		$res = $res["jsonValidateReturn"][2];
		if ($res == "true") {
			$session->set('captcha', $captcha);
		}
		$result = [];
		$result["result"] = $res;
		echo json_encode($result);
	}


	public static function requestPinganQuotations($data, $save) {
		require_once 'simple_html_dom.php';
		$adminParams = JComponentHelper::getParams("com_insurediymotor");
		$username = $adminParams->get("pinganUsername","");
		$password = $adminParams->get("pinganPassword","");
		$session = JFactory::getSession();
		$captcha = $session->get('captcha');
		$vehData = $data["vehVariantPingan"];
		$carData = InsureDIYMotorHelper::pinganGetVehicleData($vehData);
		$makeId = $carData["makeCode"];
		$modelId = $carData["modelCode"];
		$carYear = $data["carMakeYear"];
		$carCc = $data["vehicleEngineCc"];
		$bodyType = $data["carBodyType"];
		$vehNcBonus = $data["NCDPoints"];
		$vehValue = $data["vehicleValue"];
		$driverIndustry = $data["Industry"][0];
		$driverJob = $data["Occupation"][0];
		$driverExp = $data["yearsOfDrivingExp"][0];
		$birth = strtotime($data["dateOfBirth"][0]);
		$birthDate = date('Y-m-d',$birth);
		$policyDate = strtotime($data["policyStartDate"]);
		$policyDate = date('Y-m-d',$policyDate);
		$coverType = $data["coverType"];
		if ($coverType == "01") {
			$cover = "1";
		} else {
			$cover = "2";
		}

		// is logged in?
		$url = 'https://www.pingan.com.hk/?lang=en';
		$urlResult = InsureDIYMotorHelper::getCurlData($url);
		$html = str_get_html($urlResult);
		$menuCount = count($html->find('.wrapper .menu ul li'));

		// if user logged in, there will be only 1 menu
		if ($menuCount != 1) {
			//getting csrf
			$url = 'https://www.pingan.com.hk/partner/login';
			$urlResult = InsureDIYMotorHelper::getCurlData($url);
			$html = str_get_html($urlResult);
			$csrf = $html->find('#partnerLogin__csrf_token', 0)->value;

			//login
			$url = 'https://www.pingan.com.hk/partner/loginProcess';
			$post = 'partnerLogin%5Bidentity%5D='.$username.'&partnerLogin%5Bpassword%5D='.$password.'&partnerLogin%5Bcaptcha_code%5D='.$captcha.'&partnerLogin%5B_csrf_token%5D='.$csrf;
			$header = 'Content-Type:application/x-www-form-urlencoded';
			$urlResult = InsureDIYMotorHelper::getCurlDataPost($url, $post, $header);
		}
		

		//accept agreement
		$url = 'https://www.pingan.com.hk/motor/qualification/group/'.$cover;
		$urlResult = InsureDIYMotorHelper::getCurlData($url);
		$html = str_get_html($urlResult);

		//accept get quotation csrf
		$url = 'https://www.pingan.com.hk/motor/fillForQuote';
		$urlResult = InsureDIYMotorHelper::getCurlData($url);
		$html = str_get_html($urlResult);
		$quoteCsrf = $html->find('#fillforquote__csrf_token', 0)->value;

		//post quotation
		$url = 'https://www.pingan.com.hk/motor/fillForQuote';
		$post = 'fillforquote%5Bbrands%5D='.$makeId.'&fillforquote%5Bmotor_model_id%5D='.$modelId.'&fillforquote%5Byear_of_manufacturing%5D='.$carYear.'&fillforquote%5Bengine_size%5D='.$carCc.'&fillforquote%5Bmotor_category_id%5D='.$bodyType.'&fillforquote%5Bmotor_owner_type_id%5D=1&fillforquote%5Bncb%5D=0.'.$vehNcBonus.'00&fillforquote%5Beffective_date%5D='.$policyDate.'&fillforquote%5Bday_of_birth%5D='.$birthDate.'&fillforquote%5Bdriving_exp%5D='.$driverExp.'&fillforquote%5Boccupation%5D='.$driverJob.'&fillforquote%5Bindustry%5D='.$driverIndustry.'&fillforquote%5Binsured_amount%5D='.$vehValue.'&fillforquote%5Boffer_code%5D=&fillforquote%5Bcaptcha_code%5D='.$captcha.'&fillforquote%5B_csrf_token%5D='.$quoteCsrf;
		$header = 'Content-Type:application/x-www-form-urlencoded';
		$urlResult = InsureDIYMotorHelper::getCurlDataPost($url, $post, $header);

		//get quotation
		$url = 'https://www.pingan.com.hk/motor/quoteInfo';
		$urlResult = InsureDIYMotorHelper::getCurlData($url);
		$html = str_get_html($urlResult);
		$checkRes = count($html->find('b'));
		if ($checkRes < 2) {
			$finalPremium = 0;
		} else {
			$finalPremium = InsureDIYMotorHelper::stripPingan($html->find('b', 0)->plaintext);
			if($cover == "1"){
				$generalExcess = InsureDIYMotorHelper::stripPingan($html->find('table', 0)->find('tbody', 1)->find('tr', 0)->find('td', 0)->plaintext);
				$theftLossExcess = InsureDIYMotorHelper::stripPingan($html->find('table', 0)->find('tbody', 1)->find('tr', 1)->find('td', 0)->plaintext);
				$parkingExcess = InsureDIYMotorHelper::stripPingan($html->find('table', 0)->find('tbody', 1)->find('tr', 2)->find('td', 0)->plaintext);
				$thirdPartyExcess = InsureDIYMotorHelper::stripPingan($html->find('table', 0)->find('tbody', 1)->find('tr', 3)->find('td', 0)->plaintext);
				$youngExcess = InsureDIYMotorHelper::stripPingan($html->find('table', 0)->find('tbody', 1)->find('tr', 4)->find('td', 0)->plaintext);
				$inexperienceExcess = InsureDIYMotorHelper::stripPingan($html->find('table', 0)->find('tbody', 1)->find('tr', 5)->find('td', 0)->plaintext);
				$unammedExcess = InsureDIYMotorHelper::stripPingan($html->find('table', 0)->find('tbody', 1)->find('tr', 6)->find('td', 0)->plaintext);
			} else {
				$generalExcess = 0;
				$theftLossExcess = 0;
				$parkingExcess = 0;
				$thirdPartyExcess = 0;
				$youngExcess = 0;
				$inexperienceExcess = 0;
				$unammedExcess = 0;
			}
		}
	
		$plan = (object) array(
			"annualizedPremium" => "0",
			"mib" => "0",
			"producerCommAmount" => "0",
			"finalPremium" => $finalPremium,
			"planName" => "Partner Package 1",
			"productCode" => "PINGAN",
			"generalExcess" => $generalExcess,
			"theftLossExcess" => $theftLossExcess,
			"parkingExcess" => $parkingExcess,
			"thirdPartyExcess" => $thirdPartyExcess,
			"youngExcess" => $youngExcess,
			"inexperienceExcess" => $inexperienceExcess,
			"unammedExcess" => $unammedExcess

		);

		
		if ($checkRes < 2) {
			$result["success"] = false;
		} else {
			$result["success"] = true;
		}
		
		$result["plan_list"] = $plan;

		return $result;	
		
	}
	
	public static function getCurlData($url) {
		$agent= 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:66.0) Gecko/20100101 Firefox/66.0';
		$curl_log = fopen("curl.txt", 'w+');
		$options = array(
			CURLOPT_HTTPHEADER => array(),
			CURLOPT_RETURNTRANSFER => 1,     // return web page
			CURLOPT_HEADER         => false,    // don't return headers
			CURLOPT_NOBODY         => false,    // don't return headers
			CURLOPT_FOLLOWLOCATION => true,     // follow redirects
			CURLOPT_ENCODING       => "",       // handle all encodings
			CURLOPT_USERAGENT      => $agent, // who am i
			CURLOPT_AUTOREFERER    => true,     // set referer on redirect
			CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
			CURLOPT_TIMEOUT        => 120,      // timeout on response
			CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
			CURLOPT_COOKIEFILE      => dirname(__FILE__)."/cookie.txt",       
			CURLOPT_COOKIEJAR      => dirname(__FILE__)."/cookie.txt",       
			CURLOPT_STDERR      => $curl_log,       
			CURLOPT_VERBOSE      => true,       
			CURLOPT_SSL_VERIFYHOST      => false,       
			CURLOPT_SSL_VERIFYPEER => false     // Disabled SSL Cert checks
		);
		$ch = curl_init( $url );
		curl_setopt_array( $ch, $options );
		$content = curl_exec( $ch );
		$err     = curl_errno( $ch );
		$errmsg  = curl_error( $ch );
		$header  = curl_getinfo( $ch );
		curl_close( $ch );
		if (!$content) {
			throw new \Exception('Curl error: ' . curl_error($ch));
		}
		return $content;
	}

	public static function getCurlDataPost($url, $postdata, $header) {
		$curl_log = fopen("curl.txt", 'w+');
		$options = array(

			CURLOPT_HTTPHEADER => array('application/json;charset=ISO-8859-1', $header),
			CURLOPT_CUSTOMREQUEST      => "POST",       
			CURLOPT_POST => true,     
			CURLOPT_POSTFIELDS => $postdata,     
			CURLOPT_RETURNTRANSFER => 1,     // return web page
			CURLOPT_HEADER         => false,    // don't return headers
			CURLOPT_NOBODY         => false,    // don't return headers
			CURLOPT_FOLLOWLOCATION => true,     // follow redirects
			CURLOPT_ENCODING       => "",       // handle all encodings
			CURLOPT_AUTOREFERER    => true,     // set referer on redirect
			CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
			CURLOPT_TIMEOUT        => 120,      // timeout on response
			CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
			CURLOPT_COOKIEFILE      => dirname(__FILE__)."/cookie.txt",       
			CURLOPT_COOKIEJAR      => dirname(__FILE__)."/cookie.txt",       
			CURLOPT_VERBOSE      => true,       
			CURLOPT_STDERR      => $curl_log,        
			CURLOPT_SSL_VERIFYHOST      => false,       
			CURLOPT_SSL_VERIFYPEER => false     // Disabled SSL Cert checks
		);
		$ch = curl_init( $url );
		curl_setopt_array( $ch, $options );
		$content = curl_exec( $ch );
		$err     = curl_errno( $ch );
		$errmsg  = curl_error( $ch );
		$header  = curl_getinfo( $ch );
		curl_close( $ch );
		if (!$content) {
			throw new \Exception('Curl error: ' . curl_error($ch));
		}
		return $content;
	}
	public static function stripPingan($string){
		$string = str_replace('HK$','', $string);
		$string = str_replace(' ','', $string);
		$string = str_replace(',','', $string);

		return $string;
	}
	public static function requestHlaQuotations($data, $save) {
		include("simple_html_dom.php");
		
		$cookies = InsureDIYMotorHelper::hlaBrokerLogin();
		$agent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)';
		
		if($cookies) {
			$ch = curl_init();
			
			//Send Quotation Request
			curl_setopt($ch, CURLOPT_URL, 'https://www.hlas.com.sg/hlaspartners');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HEADER, 1);
			curl_setopt($ch, CURLOPT_USERAGENT, $agent);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			
			$cookie_string="";
			foreach( $cookies as $key => $value ) {
				$cookie_string .= "$key=$value;";
			};
			
			curl_setopt($ch, CURLOPT_COOKIE, $cookie_string);
			
			$server_output = curl_exec ($ch);
			
			$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
			$header = substr($server_output, 0, $header_size);
			$body = substr($server_output, $header_size);
			$html = str_get_html($body);
			
			//Grab cookies out from response
			preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $header, $matches);
			
			foreach($matches[1] as $item) {
				$cookie = InsureDIYMotorHelper::parse_qs($item);
				$cookies = array_merge($cookies, $cookie);
			}
			
			$params = array();
			
			// do anything you want with your response
			foreach ($html->find("input[type=hidden]") as $node) {
				$params[$node->name] = $node->value;
			}
			
			$html->clear();
			
			curl_close($ch);
			
			$ch = curl_init();
			
			//Send Quotation Request
			curl_setopt($ch, CURLOPT_URL, 'https://www.hlas.com.sg/HLASPartners/PersonalInsurance/CarInsurance.aspx');
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_HEADER, 1);
			curl_setopt($ch, CURLOPT_USERAGENT, $agent);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			
			$cookie_string="";
			foreach( $cookies as $key => $value ) {
				$cookie_string .= "$key=$value;";
			};
			
			curl_setopt($ch, CURLOPT_COOKIE, $cookie_string);
			
			$server_output = curl_exec ($ch);
			
			$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
			$header = substr($server_output, 0, $header_size);
			$body = substr($server_output, $header_size);
			$html = str_get_html($body);
			
			//Grab cookies out from response
			preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $header, $matches);
			
			foreach($matches[1] as $item) {
				$cookie = InsureDIYMotorHelper::parse_qs($item);
				$cookies = array_merge($cookies, $cookie);
			}
			
			$params = array();
			
			// do anything you want with your response
			foreach ($html->find("input[type=hidden]") as $node) {
				$params[$node->name] = $node->value;
			}
			
			$html->clear();
			
			curl_close($ch);

			$params['__EVENTTARGET'] = 'dnn$ctr999$Requirements$NextButton';
			$params['dnn_ctr999_Requirements_Q1_ClientState'] = '';
			$params['dnn_ctr999_Requirements_Q2_ClientState'] = '';
			$params['dnn_ctr999_Requirements_DnnToolTip1_ClientState'] = '';
			$params['dnn_ctr999_Requirements_DnnToolTip2_ClientState'] = '';
			$params['dnn_ctr999_Requirements_DnnToolTip6_ClientState'] = '';
			$params['dnn_ctr999_Requirements_DnnToolTip7_ClientState'] = '';
			$params['dnn$ctr999$Requirements$MeetRequirementsField'] = 'True';
			
			
			$ch = curl_init();
			
			//Agree Term Next button
			curl_setopt($ch, CURLOPT_URL, "https://www.hlas.com.sg/HLASPartners/PersonalInsurance/CarInsurance.aspx");
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
			curl_setopt($ch, CURLOPT_HEADER, 1);
			curl_setopt($ch, CURLOPT_USERAGENT, $agent);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			
			$cookie_string="";
			foreach( $cookies as $key => $value ) {
				$cookie_string .= "$key=$value;";
			};
			
			curl_setopt($ch, CURLOPT_COOKIE, $cookie_string);
			
			$server_output = curl_exec ($ch);
			
			$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
			$header = substr($server_output, 0, $header_size);
			$body = substr($server_output, $header_size);
			
			//Grab cookies out from response
			preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $header, $matches);
			
			foreach($matches[1] as $item) {
				$cookie = InsureDIYMotorHelper::parse_qs($item);
				$cookies = array_merge($cookies, $cookie);
			}
			
			$body = substr($body, strpos($body, "<div class=\"aspNetHidden\">"), strpos($body, "<div id=\"dnn_ctr999_DriversDetails_ActionsPanel\">"));
			$html = str_get_html($body);
			
			if(!$html) {
				//file_put_contents('./check2.html', $html);
				curl_close($ch);
				
				return false;
			}
			
			$params = array();
			foreach ($html->find("input[type=hidden]") as $node) {
				$params[$node->name] = $node->value;
			}
			
			$link = curl_getinfo($ch)["url"];
			
			curl_close($ch);
			
			$vehData = InsureDIYMotorHelper::hlaGetVehicleData($data["vehVariantHla"]);

			//update Make
			$ch = curl_init();
			
			$params['dnn$ctr999$DriversDetails$MakeField'] = $vehData["makeCode"];
			$params['__EVENTTARGET'] = 'dnn$ctr999$DriversDetails$MakeField';
			$params['__dnnVariable'] = '`{`__scdoff`:`1`,`sf_siteRoot`:`/`,`sf_tabId`:`230`}';
			$params['__LASTFOCUS'] = '';
			$params['__ASYNCPOST'] = 'true';
			$params["ScriptManager"] = 'ScriptManager|dnn$ctr999$DriversDetails$MakeField';
			
			$query = '';
			
			foreach ($params as $param => $value) {
				$query .= urlencode($param).'='.urlencode($value) .'&';
			}
			
			//Send Quotation Request
			curl_setopt($ch, CURLOPT_URL, $link);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, substr($query, 0, -1));
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('content-type: application/x-www-form-urlencoded'));
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_HEADER, 1);
			curl_setopt($ch, CURLOPT_USERAGENT, $agent);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			
			$cookie_string="";
			foreach( $cookies as $key => $value ) {
				$cookie_string .= "$key=$value;";
			};
			
			curl_setopt($ch, CURLOPT_COOKIE, $cookie_string);
			
			$server_output = curl_exec ($ch);
			
			$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
			$header = substr($server_output, 0, $header_size);
			$body = substr($server_output, $header_size);
			
			$dataArr = explode("|", substr($body, strpos($body, "__EVENTTARGET") + 1));
			$viewstate = "";
			$eventvalidation = "";
			
			foreach($dataArr as $key=>$val) {
				if($val == "__VIEWSTATE") {
					$viewstate = $dataArr[$key+1];
				}
				if($val == "__EVENTVALIDATION") {
					$eventvalidation = $dataArr[$key+1];
				}
			}
			
			curl_close($ch);
			
			//update Model
			$ch = curl_init();
			
			$params['__VIEWSTATE'] = $viewstate;
			$params['__EVENTVALIDATION'] = $eventvalidation;
			$params['dnn$ctr999$DriversDetails$MakeField'] = $vehData["makeCode"];
			$params['dnn$ctr999$DriversDetails$ModelField'] = $vehData["modelCode"];
			$params['__EVENTTARGET'] = 'dnn$ctr999$DriversDetails$ModelField';
			$params['__dnnVariable'] = '`{`__scdoff`:`1`,`sf_siteRoot`:`/`,`sf_tabId`:`230`}';
			$params['__LASTFOCUS'] = '';
			$params['__ASYNCPOST'] = 'true';
			$params["ScriptManager"] = 'dnn$ctr999$DriversDetails$MakeModelUpdatePanel|dnn$ctr999$DriversDetails$ModelField';
			
			$query = '';
			
			foreach ($params as $param => $value) {
				$query .= urlencode($param).'='.urlencode($value) .'&';
			}
			
			//Send Quotation Request
			curl_setopt($ch, CURLOPT_URL, $link);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, substr($query, 0, -1));
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('content-type: application/x-www-form-urlencoded'));
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_HEADER, 1);
			curl_setopt($ch, CURLOPT_USERAGENT, $agent);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			
			$cookie_string="";
			foreach( $cookies as $key => $value ) {
				$cookie_string .= "$key=$value;";
			};
			
			curl_setopt($ch, CURLOPT_COOKIE, $cookie_string);
			
			$server_output = curl_exec ($ch);
			
			$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
			$header = substr($server_output, 0, $header_size);
			$body = substr($server_output, $header_size);
			$html = str_get_html(substr($body, strpos($body, "<input"),strpos($body, "data-numeric") + 1).">");
			
			$dataArr = explode("|", substr($body, strpos($body, "__EVENTTARGET") + 1));
			$viewstate = "";
			$eventvalidation = "";
			
			if($html) {
				$vehData["vehCc"] = $html->find("input")[0]->value;
				
				$html->clear();
				
				InsureDIYMotorHelper::hlaStoreVehicleData($vehData);
			}
			
			foreach($dataArr as $key=>$val) {
				if($val == "__VIEWSTATE") {
					$viewstate = $dataArr[$key+1];
				}
				if($val == "__EVENTVALIDATION") {
					$eventvalidation = $dataArr[$key+1];
				}
			}
			
			curl_close($ch);
			
			//update NCD
			$ch = curl_init();
			
			$requestObj = InsureDIYMotorHelper::hlaQuoteRequestData($params, $data, $vehData);
			
			$requestObj['__VIEWSTATE'] = $viewstate;
			$requestObj['__EVENTVALIDATION'] = $eventvalidation;
			$requestObj['__EVENTTARGET'] = 'dnn$ctr999$DriversDetails$NoClaimsDiscountField';
			$requestObj['__dnnVariable'] = '`{`__scdoff`:`1`,`sf_siteRoot`:`/`,`sf_tabId`:`230`}';
			$requestObj["ScriptManager"] = 'ScriptManager|dnn$ctr999$DriversDetails$NoClaimsDiscountField';
			
			//Send Quotation Request
			curl_setopt($ch, CURLOPT_URL, $link);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $requestObj);
			curl_setopt($ch, CURLOPT_HEADER, 1);
			curl_setopt($ch, CURLOPT_USERAGENT, $agent);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			
			$cookie_string="";
			foreach( $cookies as $key => $value ) {
				$cookie_string .= "$key=$value;";
			};
			
			curl_setopt($ch, CURLOPT_COOKIE, $cookie_string);
			
			$server_output = curl_exec ($ch);
			
			$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
			$header = substr($server_output, 0, $header_size);
			$body = substr($server_output, $header_size);
			
			$dataArr = explode("|", substr($body, strpos($body, "__EVENTTARGET") + 1));
			$viewstate = "";
			$eventvalidation = "";
			
			foreach($dataArr as $key=>$val) {
				if($val == "__VIEWSTATE") {
					$viewstate = $dataArr[$key+1];
				}
				if($val == "__EVENTVALIDATION") {
					$eventvalidation = $dataArr[$key+1];
				}
			}
			
			curl_close($ch);
			
			//file_put_contents('./check3.html', $body);
			
			$requestObj = InsureDIYMotorHelper::hlaQuoteRequestData($params, $data, $vehData);
			
			$ch = curl_init();
			
			unset($requestObj["ScriptManager"]);
			unset($requestObj["__ASYNCPOST"]);
			$requestObj['__VIEWSTATE'] = $viewstate;
			$requestObj['__EVENTVALIDATION'] = $eventvalidation;
			$requestObj['__EVENTTARGET'] = 'dnn$ctr999$DriversDetails$NextButton';
			
			$html->clear();
			
			curl_close($ch);
			
			$ch = curl_init();
			
			//Policy holder Next button
			curl_setopt($ch, CURLOPT_URL, $link);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $requestObj);
			curl_setopt($ch, CURLOPT_HEADER, 1);
			curl_setopt($ch, CURLOPT_USERAGENT, $agent);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			
			$cookie_string="";
			foreach( $cookies as $key => $value ) {
				$cookie_string .= "$key=$value;";
			};
			
			curl_setopt($ch, CURLOPT_COOKIE, $cookie_string);
			
			$server_output = curl_exec ($ch);
			
			$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
			$header = substr($server_output, 0, $header_size);
			$body = substr($server_output, $header_size);
			$html = str_get_html($body);
			
			//Grab cookies out from response
			preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $header, $matches);
			
			foreach($matches[1] as $item) {
				$cookie = InsureDIYMotorHelper::parse_qs($item);
				$cookies = array_merge($cookies, $cookie);
			}
			
			if(!$html) {
				//file_put_contents('./check3.html', $body);
				
				curl_close($ch);
				
				return false;
			}
			
			$params = array();
			foreach ($html->find("input[type=hidden]") as $node) {
				$params[$node->name] = $node->value;
			}
			
			$link = curl_getinfo($ch)["url"];
			
			if(!$html->find("#dnn_ctr999_QuoteDetails_ExcessField", 0)) {
				//file_put_contents('./check4.html', $body);
				curl_close($ch);
				
				return false;
			}
			
			$plan = (object) array(
					"excessStandard" => array(
							0 => $html->find("#dnn_ctr999_QuoteDetails_ExcessField", 0)->plaintext,
							1 => $html->find("#dnn_ctr999_QuoteDetails_ExcessField2", 0)->plaintext
					),
					"excessYoung" => $html->find("#dnn_ctr999_QuoteDetails_DriverExcessField", 0)->plaintext,
					"excessWS" => $html->find("#dnn_ctr999_QuoteDetails_WindScreenField", 0)->plaintext,
					"finalPremium" => preg_replace("/([^0-9\\.])/i", "", $html->find("#dnn_ctr999_QuoteDetails_PremiumAmtField", 0)->plaintext),
					"planName" => "Car Protect360",
					"productCode" => "HLA"
			);
			
			$result["success"] = true;
			$result["quote_ref_id"] = "";
			$result["plan_list"] = $plan;
			
			$html->clear();
			
			curl_close($ch);
			
			if($save) {
				$params["__EVENTTARGET"] = 'dnn$ctr999$QuoteDetails$SaveQuoteButton';
				
				$ch = curl_init();
				
				//Policy holder Next button
				curl_setopt($ch, CURLOPT_URL, $link);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
				curl_setopt($ch, CURLOPT_HEADER, 1);
				curl_setopt($ch, CURLOPT_USERAGENT, $agent);
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
				curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
				
				$cookie_string="";
				foreach( $cookies as $key => $value ) {
					$cookie_string .= "$key=$value;";
				};
				
				curl_setopt($ch, CURLOPT_COOKIE, $cookie_string);
				
				$server_output = curl_exec ($ch);
				
				$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
				$header = substr($server_output, 0, $header_size);
				$body = substr($server_output, $header_size);
				$html = str_get_html($body);
				
				//Grab cookies out from response
				preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $header, $matches);
				
				foreach($matches[1] as $item) {
					$cookie = InsureDIYMotorHelper::parse_qs($item);
					$cookies = array_merge($cookies, $cookie);
				}
				
				$httpStatus = curl_getinfo($ch, CURLINFO_HTTP_CODE);
				
				if($httpStatus == 200) {
					return true;
				}
				else {
					//file_put_contents('./check5.html', $html);
					return false;
				}
			}
			else {
				return $result;
			}
		}
	}
	
	public static function hlaQuoteRequestData($params, $data, $vehData) {
		$adminParams = JComponentHelper::getParams("com_insurediymotor");
		$occ_code = InsureDIYMotorHelper::hlaGetOccupationData($data["Occupation"][0]);
		$occ_type = $data["occupationType"][0] == "INDO" ? 52 : 53;
		
		$params['ScriptManager_TSM'] = ';;System.Web.Extensions, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35:en:93a6b8ed-f453-4cc5-9080-8017894b33b0:ea597d4b:b25378d2';
		$params["StylesheetManager_TSSM"] = ';Telerik.Web.UI, Version=2013.2.717.40, Culture=neutral, PublicKeyToken=121fae78165ba3d4:en-US:dae8717e-3810-4050-96d3-31018e70c6e4:53e1db5a;Telerik.Web.UI.Skins, Version=2013.2.717.40, Culture=neutral, PublicKeyToken=121fae78165ba3d4:en-US:98d23577-27ad-4f20-8a16-623848846194:8f70baae';
		$params['dnn$ctr999$DriversDetails$PolicyHolderSurnameField'] = $data["lastName"][0];
		$params['dnn$ctr999$DriversDetails$PolicyHolderGivenNameField'] = $data["firstName"][0];
		$params['dnn$ctr999$DriversDetails$PolicyHolderDisplayFullNameField'] = $data["lastName"][0] . " " . $data["firstName"][0];
		$params['dnn$ctr999$DriversDetails$PolicyHolderIDNumberField'] = $data['Nric'][0];
		$params['dnn$ctr999$DriversDetails$PolicyHolderDateOfBirthField'] = DateTime::createFromFormat('d-M-Y', $data['dateOfBirth'][0])->format('Y-m-d');
		$params['dnn$ctr999$DriversDetails$PolicyHolderDateOfBirthField$dateInput'] = DateTime::createFromFormat('d-M-Y', $data['dateOfBirth'][0])->format('d-m-Y');
		$params['dnn_ctr999_DriversDetails_PolicyHolderDateOfBirthField_dateInput_ClientState'] = '{"enabled":true,"emptyMessage":"","validationText":"'. DateTime::createFromFormat('d-M-Y', $data['dateOfBirth'][0])->format('Y-m-d') . '-00-00-00","valueAsString":"'. DateTime::createFromFormat('d-M-Y', $data['dateOfBirth'][0])->format('Y-m-d') . '-00-00-00","minDateStr":"1753-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'. DateTime::createFromFormat('d-M-Y', $data['dateOfBirth'][0])->format('d-m-Y') . '"}';
		$params['dnn_ctr999_DriversDetails_PolicyHolderDateOfBirthField_calendar_SD'] = '[]';
		$params['dnn_ctr999_DriversDetails_PolicyHolderDateOfBirthField_calendar_AD'] = '[[1753,1,1],[2099,12,30],[2018,3,9]]';
		$params['dnn_ctr999_DriversDetails_PolicyHolderDateOfBirthField_ClientState'] = '{"minDateStr":"1753-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00"}';
		$params['dnn$ctr999$DriversDetails$PolicyHolderGenderField'] = $data['Gender'][0] == "MALE" ? 1 : 2;
		$params['dnn$ctr999$DriversDetails$PolicyHolderMobileTelephoneField'] = $data["mobileNo"][0];
		// 		$params['dnn$ctr999$DriversDetails$PolicyHolderEmailField'] = $data["Email"][0];
		//		$params['dnn$ctr999$DriversDetails$PolicyHolderEmailField'] = $adminParams->get('service_email', "sg_service@insurediy.com");
		$params['dnn$ctr999$DriversDetails$PolicyHolderEmailField'] = "insurediy.com@gmail.com";
		$params['dnn$ctr999$DriversDetails$IsRegisteredOwnerVehicleField'] = 'True';
		$params['dnn$ctr999$DriversDetails$MainDriverSurnameField'] = $data["lastName"][0];
		$params['dnn$ctr999$DriversDetails$MainDriverGivenNameField'] = $data["firstName"][0];
		$params['dnn$ctr999$DriversDetails$MainDriverIDNumberField'] = $data['Nric'][0];
		$params['dnn$ctr999$DriversDetails$MainDriverDateOfBirthField'] = DateTime::createFromFormat('d-M-Y', $data['dateOfBirth'][0])->format('Y-m-d');
		$params['dnn$ctr999$DriversDetails$MainDriverDateOfBirthField$dateInput'] = DateTime::createFromFormat('d-M-Y', $data['dateOfBirth'][0])->format('d-m-Y');
		$params['dnn_ctr999_DriversDetails_MainDriverDateOfBirthField_dateInput_ClientState'] = '{"enabled":true,"emptyMessage":"","validationText":"'. DateTime::createFromFormat('d-M-Y', $data['dateOfBirth'][0])->format('Y-m-d') . '-00-00-00","valueAsString":"'. DateTime::createFromFormat('d-M-Y', $data['dateOfBirth'][0])->format('Y-m-d') . '-00-00-00","minDateStr":"1753-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'. DateTime::createFromFormat('d-M-Y', $data['dateOfBirth'][0])->format('d-m-Y') . '"}';
		$params['dnn_ctr999_DriversDetails_MainDriverDateOfBirthField_calendar_SD'] = '[]';
		$params['dnn_ctr999_DriversDetails_MainDriverDateOfBirthField_calendar_AD'] = '[[1753,1,1],[2099,12,30],[2018,3,9]]';
		$params['dnn_ctr999_DriversDetails_MainDriverDateOfBirthField_ClientState'] = '{"minDateStr":"1753-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00"}';
		$params['dnn$ctr999$DriversDetails$MainDriverGenderField'] = $data['Gender'][0] == "MALE" ? 1 : 2;
		$params['dnn$ctr999$DriversDetails$MainDriverMaritalStatusField'] = $data['maritalStatus'][0] == "Single" ? 51 : 50;
		$params['dnn$ctr999$DriversDetails$MainDriverDrivingExperienceField'] = $data['yearsOfDrivingExp'][0];
		$params['dnn$ctr999$DriversDetails$MainDriverOccupationTypeField'] = $occ_type;
		$params['dnn$ctr999$DriversDetails$MainDriverOccupationField'] = $occ_code;
		$params['dnn$ctr999$DriversDetails$PlanTypeHiddenField'] = 'Comprehensive';
		$params['dnn$ctr999$DriversDetails$PlateNumberField'] = $data['vehicleRegNo'];
		$params['dnn$ctr999$DriversDetails$CarUsageField'] = 138;
		$params['dnn$ctr999$DriversDetails$MakeField'] = $vehData["makeCode"];
		$params['dnn$ctr999$DriversDetails$ModelField'] = $vehData["modelCode"];
		$params['dnn$ctr999$DriversDetails$EngineCapacityField'] = $vehData["vehCc"];
		$params['dnn$ctr999$DriversDetails$YearOfRegistrationField'] = $data["carRegisterYear"];
		$params['dnn$ctr999$DriversDetails$MotorOffPeakFactorField'] = $data["offPeakCar"] == "true" ? 136 : 137;
		$params['dnn$ctr999$DriversDetails$WorkShopPanelField'] = $data["workshop"] == "Yes" ? 2097: 2096;
		$params['dnn$ctr999$DriversDetails$NoClaimsDiscountField'] = 2100 + ($data['NCDPoints']/10);
		$params['dnn$ctr999$DriversDetails$OffenceFreeDiscountField'] = ($data["meritCert"] == "true" && $data['NCDPoints']>= 30) ? 'True': 'False';
		$params['dnn$ctr999$DriversDetails$PolicyStartDateField'] = DateTime::createFromFormat('d-M-Y', $data['policyStartDate'])->format('Y-m-d');
		$params['dnn$ctr999$DriversDetails$PolicyStartDateField$dateInput'] = DateTime::createFromFormat('d-M-Y', $data['policyStartDate'])->format('d-m-Y');
		$params['dnn_ctr999_DriversDetails_PolicyStartDateField_dateInput_ClientState'] = '{"enabled":true,"emptyMessage":"","validationText":"'.DateTime::createFromFormat('d-M-Y', $data['policyStartDate'])->format('Y-m-d').'-00-00-00","valueAsString":"'.DateTime::createFromFormat('d-M-Y', $data['policyStartDate'])->format('Y-m-d').'-00-00-00","minDateStr":"1753-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.DateTime::createFromFormat('d-M-Y', $data['policyStartDate'])->format('d-m-Y').'"}';
		$params['dnn_ctr999_DriversDetails_PolicyStartDateField_calendar_SD'] = '[]';
		$params['dnn_ctr999_DriversDetails_PolicyStartDateField_calendar_AD'] = '[[1753,1,1],[2099,12,30],['.DateTime::createFromFormat('d-M-Y', $data['policyStartDate'])->format('Y,m,d').']]';
		$params['dnn_ctr999_DriversDetails_PolicyStartDateField_ClientState'] = '{"minDateStr":"1753-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00"}';
		$params['dnn_ctr999_DriversDetails_Q1_ClientState'] = '';
		$params['dnn$ctr999$DriversDetails$PolicyEndDateField'] = DateTime::createFromFormat('d-M-Y', $data['policyStartDate'])->modify('+364 days')->format('Y-m-d');
		$params['dnn_ctr999_DriversDetails_PolicyEndDateField_dateInput_ClientState'] = '{"enabled":true,"emptyMessage":"","validationText":"'.DateTime::createFromFormat('d-M-Y', $data['policyStartDate'])->modify('+364 days')->format('Y-m-d').'-00-00-00","valueAsString":"'.DateTime::createFromFormat('d-M-Y', $data['policyStartDate'])->modify('+364 days')->format('Y-m-d').'-00-00-00","minDateStr":"1753-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00","lastSetTextBoxValue":"'.DateTime::createFromFormat('d-M-Y', $data['policyStartDate'])->modify('+364 days')->format('d-m-Y').'"}';
		$params['dnn_ctr999_DriversDetails_PolicyEndDateField_calendar_SD'] = '[]';
		$params['dnn_ctr999_DriversDetails_PolicyEndDateField_calendar_AD'] = '[[1753,1,1],[2099,12,30],['.DateTime::createFromFormat('d-M-Y', $data['policyStartDate'])->modify('+364 days')->format('Y,m,d').']]';
		$params['dnn_ctr999_DriversDetails_PolicyEndDateField_ClientState'] = '{"minDateStr":"1753-01-01-00-00-00","maxDateStr":"2099-12-31-00-00-00"}';
		$params['__dnnVariable'] = '`{`__scdoff`:`1`,`sf_siteRoot`:`/`,`sf_tabId`:`230`}';
		$params['__EVENTTARGET'] = 'dnn$ctr999$DriversDetails$NextButton';
		
		if($data['NCDPoints'] >= 50) {
			$params['dnn$ctr999$DriversDetails$NCDProtectorField'] = ($data["ncdPro"] == "Yes" && $data['NCDPoints'] == 50) ? 'True': 'False';
		}
		
		return $params;
	}
	
	public static function parse_qs($data){
		$data = preg_replace_callback('/(?:^|(?<=&))[^=[]+/', function($match) {
			return bin2hex(urldecode($match[0]));
		}, $data);
			
		parse_str($data, $values);
			
		return array_combine(array_map('hex2bin', array_keys($values)), $values);
	}
	
	public static function check_nric($nricArr) {
		// $result = false;
		
		// foreach ($nricArr as $nric) {
		// 	if(self::isNricValid($nric)) {
		// 		$result = true;
		// 	}
		// 	else {
		// 		$result = self::isFinValid($nric);
		// 	}
		// }
		
		// return $result;

		// disable NRIC check for HK ID Card

		return true;
	}
	
	private static function isNricValid ($theNric) {
		
		$theNric = strtoupper($theNric);
		
		$multiples = array( 2, 7, 6, 5, 4, 3, 2 );
		
		if (!$theNric || $theNric == '')
		{
			return false;
		}
		
		if (strlen($theNric) != 9)
		{
			return false;
		}
		
		$total = 0;
		$count = 0;
		$numericNric = 0;
		
		$first = $theNric[0];
		$last = $theNric[strlen($theNric) - 1];
		
		if ($first != 'S' && $first != 'T')
		{
			return false;
		}
		
		$numericNric = substr($theNric, 1, strlen($theNric) - 2);
		
		if (!is_numeric ($numericNric)) {
			return false;
		}
		
		while ($numericNric != 0)
		{
			$total += ($numericNric % 10) * $multiples[sizeof($multiples) - (1 + $count++)];
			
			$numericNric /= 10;
			$numericNric = floor($numericNric);
		}
		
		$outputs = '';
		if (strcmp($first, "S") == 0)
		{
			$outputs = array( 'J', 'Z', 'I', 'H', 'G', 'F', 'E', 'D', 'C', 'B', 'A' );
		}
		else
		{
			$outputs = array( 'G', 'F', 'E', 'D', 'C', 'B', 'A', 'J', 'Z', 'I', 'H' );
		}
		
		return $last == $outputs[$total % 11];
		
	}
	
	private static function isFinValid ($fin){
		$multiples = array( 2, 7, 6, 5, 4, 3, 2 );
		if (!$fin || $fin == '')
		{
			return false;
		}
		
		if (strlen($fin) != 9)
		{
			return false;
		}
		
		$total = 0;
		$count = 0;
		$numericNric = 0;
		$first = $fin[0];
		$last = $fin[strlen($fin) - 1];
		
		if ($first != 'F' && $first != 'G')
		{
			return false;
		}
		
		$numericNric = substr($fin, 1, strlen($fin) - 2);
		
		if (!is_numeric ($numericNric)) {
			return false;
		}
		
		while ($numericNric != 0)
		{
			$total += ($numericNric % 10) * $multiples[sizeof($multiples) - (1 + $count++)];
			
			$numericNric /= 10;
			$numericNric = floor($numericNric);
		}
		
		$outputs = array();
		
		if (strcmp($first, 'F') == 0)
		{
			$outputs = array( 'X', 'W', 'U', 'T', 'R', 'Q', 'P', 'N', 'M', 'L', 'K' );
		}
		else
		{
			$outputs = array( 'R', 'Q', 'P', 'N', 'M', 'L', 'K', 'X', 'W', 'U', 'T' );
		}
		
		return $last == $outputs[$total % 11];
	}
	
	public static function guid(){
		if (function_exists('com_create_guid')){
			return com_create_guid();
		}else{
			mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
			$charid = strtoupper(md5(uniqid(rand(), true)));
			$hyphen = chr(45);// "-"
			// 			$uuid = chr(123)// "{"
			// 			.substr($charid, 0, 8).$hyphen
			// 			.substr($charid, 8, 4).$hyphen
			// 			.substr($charid,12, 4).$hyphen
			// 			.substr($charid,16, 4).$hyphen
			// 			.substr($charid,20,12)
			// 			.chr(125);// "}"

			$uuid = substr($charid,20,12);
			return $uuid;
		}
	}
	
	public static function fullGuid(){
		if (function_exists('com_create_guid')){
			return com_create_guid();
		}else{
			mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
			$charid = strtoupper(md5(uniqid(rand(), true)));
			$hyphen = chr(45);// "-"
			$uuid = substr($charid, 0, 8).$hyphen
						.substr($charid, 8, 4).$hyphen
						.substr($charid,12, 4).$hyphen
						.substr($charid,16, 4).$hyphen
						.substr($charid,20,12);
			
			return $uuid;
		}
	}
}
