<?php

defined('_JEXEC') or die;

class InsureDIYMotorControllerForm extends JControllerForm {

	private $base_layout_url = "index.php?option=com_insurediymotor&view=form&layout=";
	private $doc_renewal_url = "images/motor/renewal-form-";
	private $form_url = "index.php?option=com_insurediymotor&view=motor&Itemid=159";

	public function getModel($name = 'form', $prefix = '', $config = array('ignore_request' => true)) {
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}

	public function step1save() {
		$session = JFactory::getSession();
		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		$model = $this->getModel();

		$post = $app->input->get('jform', '', 'array');

		$data = $session->get("motor.data", FALSE);
		if(!$data) {
			
			$quotation_id = $model->step1save($post);
			$result = $model->step2save($post);
		}
		else {
			$quotation_id = $model->step1save(array_merge($data,$post));
			$result = $model->step2save(array_merge($data,$post));
		}
		
		$layout = InsureDIYMotorHelper::getLayout();
		
		if($layout == 'login') {
			$app->redirect(JRoute::_($this->form_url.'&step=sign_in'));
		} else {
			$app->redirect(JRoute::_($this->form_url.'&step=3'));
		}		
	}

	public function step2save() {
		JSession::checkToken() or die( 'Invalid Token' );
		
		$layout = InsureDIYMotorHelper::getLayout();
		$app = JFactory::getApplication();
		
		$session = JFactory::getSession();
		
		$model = $this->getModel();
		$check = TRUE;
		$post = $app->input->get('jform', '', 'array');
		$customerData = $session->get("motor.data", FALSE);
		$session->set("motor.data", $customerData);
		
		if($layout == 'login') {
			$app->redirect(JRoute::_($this->form_url.'&step=sign_in'));
		}
		
		$data = array_merge($customerData,$post);
		
		if(InsureDIYMotorHelper::check_nric($post["Nric"])) {
			$result = $model->step2save($data);
				
			$app->redirect(JRoute::_($this->form_url.'&step=3'));
		}
		else {
			$app->redirect(JRoute::_($this->form_url.'&step=2'), "Nric check Failed.");
		}
		
	}

	public function step3save() {
		JSession::checkToken() or die( 'Invalid Token' );
		
		$layout = InsureDIYMotorHelper::getLayout();
		$app = JFactory::getApplication();
		
		if($layout == 'login') {
			$app->redirect(JRoute::_($this->form_url.'&step=sign_in'));
		}
		
		//$session = JFactory::getSession();
		$model = $this->getModel();
		$post = $app->input->get('jform', '', 'array');
		
		$quotation_id = $model->step3save($post);
			
		$app->redirect(JRoute::_($this->form_url.'&step=4'));
	}

	public function step4save() {
		JSession::checkToken() or die( 'Invalid Token' );
		
		$layout = InsureDIYMotorHelper::getLayout();
		$app = JFactory::getApplication();
		
		if($layout == 'login') {
			$app->redirect(JRoute::_($this->form_url.'&step=sign_in'));
		}
		
		$session = JFactory::getSession();
		$model = $this->getModel();
		$post = $app->input->get('jform', '', 'array');
		$customerData = $session->get("motor.data", FALSE);
		$data = array_merge($customerData,$post);
		
		$check = $model->step4save($data);
			
		if ($check) {
			$session->set("motor.redirected", true);
			$app->redirect(JRoute::_($this->form_url.'&step=5&method='.$data["partnerId"]));
		}
		else {
			$app->redirect(JRoute::_($this->form_url.'&step=4'));
		}
	}

	public function step5save() {
		JSession::checkToken() or die( 'Invalid Token' );
		
		$layout = InsureDIYMotorHelper::getLayout();
		$app = JFactory::getApplication();
		
		if($layout == 'login') {
			$app->redirect(JRoute::_($this->form_url.'&step=sign_in'));
		}
		
		$app->redirect($this->getRedirectUrl("thankyou"));
	}
	
	public function renewalcase() {
		JSession::checkToken() or die( 'Invalid Token' );
		
		$layout = InsureDIYMotorHelper::getLayout();
		$app = JFactory::getApplication();
		
		if($layout == 'login') {
			$app->redirect(JRoute::_($this->form_url.'&step=sign_in'));
		}
		
		$model = $this->getModel();
		$post = $app->input->get('jform', '', 'array');
		
		$check = $model->renewalcase($post);
			
		if ($check) {
			$app->redirect(JURI::base() . $this->doc_renewal_url . $post["partner_id"] . '.pdf');
		}
		else {
			$app->redirect(JRoute::_($this->form_url.'&step=3'));
		}
	}
	

	public function testSompoPolicyObj() {
		$model = $this->getModel();

		$data = $model->getDataByRequestId("7542A52E-F176-4F51-EEFE-3E4886717098");
		$requestObj = InsureDIYMotorHelper::sompoPolicyRequestData($data);
		
		echo json_encode($requestObj);
		JFactory::getApplication()->close();
	}
	
	public function sompoPaymentReturn() {
		$requestData = json_decode(file_get_contents('php://input'));
		$app = JFactory::getApplication();
		$session = JFactory::getSession();
		$model = $this->getModel();
		$data = $app->input->post->getArray();

		if(empty($data)) {
			$app->redirect($this->getRedirectUrl("cancelled"));
		}
		
		$check = $model->validatePaymentStatus($data["request_id"]);
		
		if ($check) {
			$check = $model->createPolicy($data["request_id"]);
			
			if ($check) {
				// After payment stuffs
				$pdfarr = array(array()); // empty attachments

				$model->sendEmails($pdfarr, TRUE, $data["request_id"]);
				$model->sendEmails($pdfarr, FALSE, $data["request_id"]);
				
				$app->redirect($this->getRedirectUrl("thankyou"));
			}
			else {
				$model->sendEmails(FALSE, FALSE, $data["request_id"]);
				
				$app->redirect($this->getRedirectUrl("error"));
			}
		}
		else {
			$app->redirect($this->getRedirectUrl("error"));
		}
	}
	
	public function sompoPaymentIPN() {
		$app = JFactory::getApplication();
		$session = JFactory::getSession();
		$model = $this->getModel();
		$data = $app->input->post->getArray();
		
		$requestData = json_decode(file_get_contents('php://input'), true);
		$date = new DateTime('now');
		$date->setTimezone(new DateTimeZone('Asia/Singapore'));
		$now = $date->format("Y-m-d\TH:i:s.u+08:00");
		$data = JFactory::getApplication()->input->post->getArray();
		//Something to write to txt log
		$log  = $now. ": SUCCESS".PHP_EOL.
		"-------------------------".PHP_EOL.
		"POST = ".json_encode($data).PHP_EOL.
		"request = ".json_encode($requestData).PHP_EOL;
		//Save string to log, use FILE_APPEND to append.
		//file_put_contents('./wirecardIPN.log', $log, FILE_APPEND);
		
// 		$check = $model->validatePaymentStatus('d7855010-64c1-4e66-9ab3-d98b309a3d8c', $data["request_id"]);
		
// 		if ($check) {
// 			$check = $model->createPolicy($data["request_id"]);
			
// 		}
		
		JFactory::getApplication()->close(); // or jexit();
	}
	
	public function msigPaymentSuccess() {
		$app = JFactory::getApplication();
		$jInput = $app->input;
		$model = $this->getModel();
		
		$session = JFactory::getSession();
		$result= array();
		$result["success"] = true;
		$result["data"] =  (object) array(
				"transactionId" => $jInput->get('PayRef'),
				"acknowledgementId" => "",
				"proposalId" => $jInput->get('Ref')
		);
		$session->set("motor.result", $result);
		
		$app->redirect($this->getRedirectUrl("thankyou"));
		
// 		ASIA PAY DOES NOT RETURN ANY DATA ON THIS PART
// 		$ref = $jInput->get('Ref');
// 		$payRef = $jInput->get('PayRef');
		
// 		if($ref != "" && $payRef != "") {
// 			$result = $model->validateAsiaPayPaymentStatus($payRef, $ref);
			
// 			if($result) {
// 				$check = $model->createPolicy($ref);
				
// 				if($check) {
// 					// After payment stuffs
// 					$pdfarr = array(array()); // empty attachments
					
// 					$model->sendEmails($pdfarr, TRUE, $ref);
// 					$model->sendEmails($pdfarr, FALSE, $ref);
// 				}
// 				else {
// 					$model->sendEmails(FALSE, FALSE, $ref);
					
// 					$app->redirect($this->getRedirectUrl("error"));
// 				}
// 			}
			
// 			if($check) {
// 				$app->redirect($this->getRedirectUrl("thankyou"));
// 			}
// 			else {
// 				$model->sendEmails(FALSE, FALSE, $ref);
				
// 				$app->redirect($this->getRedirectUrl("error"));
// 			}
// 		}
// 		else {
// 			$model->saveErrorMessage(array("quotationId"=>$ref, "requestData"=>js_encode($jInput),"errorMsg"=>"MSIG payment return error"));
			
// 			$model->sendEmails(FALSE, FALSE, $ref);
			
// 			$app->redirect($this->getRedirectUrl("error"));
// 		}
	}
	
	public function msigPaymentCancel() {
		$app = JFactory::getApplication();
		$jInput = $app->input;
		
		$session = JFactory::getSession();
		$result= array();
		$result["success"] = false;
		$result["data"] =  (object) array(
				"transactionId" => $jInput->get('Ref')
		);
		$session->set("motor.result", $result);
		
		$app->redirect($this->getRedirectUrl("cancelled"));
	}
	
	public function msigPaymentFail() {
		$app = JFactory::getApplication();
		$jInput = $app->input;
		
		$session = JFactory::getSession();
		$result= array();
		$result["success"] = false;
		$result["data"] =  (object) array(
				"transactionId" => $jInput->get('Ref'),
				"acknowledgementId" => "",
				"proposalId" => ""
		);
		$session->set("motor.result", $result);
		
		$model = $this->getModel();
		$model->saveErrorMessage(array("quotationId"=>$jInput->get('Ref'), "requestData"=>js_encode($jInput),"errorMsg"=>"MSIG payment return failed"));
		
		$model->sendEmails(FALSE, FALSE, $jInput->get('Ref'));
		
		$app->redirect($this->getRedirectUrl("error"));
	}
	
	public function hlaPaymentSuccess() {
		$app = JFactory::getApplication();
		$jInput = $app->input;
		$model = $this->getModel();
		
		$session = JFactory::getSession();
		$result= array();
		$result["success"] = true;
		$result["data"] =  (object) array(
				"transactionId" => $jInput->get('PayRef'),
				"acknowledgementId" => "",
				"proposalId" => $jInput->get('Ref')
		);
		$session->set("motor.result", $result);
		
		$app->redirect($this->getRedirectUrl("thankyou"));
	}

	public function aigPaymentSuccess() {
		$app = JFactory::getApplication();
		$jInput = $app->input;
		$model = $this->getModel();
		
		$session = JFactory::getSession();
		$result= array();
		$result["success"] = true;
		$result["data"] =  (object) array(
				"transactionId" => $jInput->get('PayRef'),
				"acknowledgementId" => "",
				"proposalId" => $jInput->get('Ref')
		);
		$session->set("motor.result", $result);
		
		$app->redirect($this->getRedirectUrl("thankyou"));
	}

	public function awPaymentSuccess() {
		$app = JFactory::getApplication();
		$jInput = $app->input;
		$model = $this->getModel();
		
		$session = JFactory::getSession();
		$result= array();
		$result["success"] = true;
		$result["data"] =  (object) array(
				"transactionId" => $jInput->get('PayRef'),
				"acknowledgementId" => "",
				"proposalId" => $jInput->get('Ref')
		);
		$session->set("motor.result", $result);
		
		$app->redirect($this->getRedirectUrl("thankyou"));
	}
	
	public function pinganPaymentSuccess() {
		$app = JFactory::getApplication();
		$jInput = $app->input;
		$model = $this->getModel();
		
		$session = JFactory::getSession();
		$result= array();
		$result["success"] = true;
		$result["data"] =  (object) array(
				"transactionId" => $jInput->get('PayRef'),
				"acknowledgementId" => "",
				"proposalId" => $jInput->get('Ref')
		);
		$session->set("motor.result", $result);
		
		$app->redirect($this->getRedirectUrl("thankyou"));
	}
	public function hlaPaymentCancel() {
		$app = JFactory::getApplication();
		$jInput = $app->input;
		
		$session = JFactory::getSession();
		$result= array();
		$result["success"] = false;
		$result["data"] =  (object) array(
				"transactionId" => $jInput->get('Ref')
		);
		$session->set("motor.result", $result);
		
		$app->redirect($this->getRedirectUrl("cancelled"));
	}

	public function aigPaymentCancel() {
		$app = JFactory::getApplication();
		$jInput = $app->input;
		
		$session = JFactory::getSession();
		$result= array();
		$result["success"] = false;
		$result["data"] =  (object) array(
				"transactionId" => $jInput->get('Ref')
		);
		$session->set("motor.result", $result);
		
		$app->redirect($this->getRedirectUrl("cancelled"));
	}

	public function awPaymentCancel() {
		$app = JFactory::getApplication();
		$jInput = $app->input;
		
		$session = JFactory::getSession();
		$result= array();
		$result["success"] = false;
		$result["data"] =  (object) array(
				"transactionId" => $jInput->get('Ref')
		);
		$session->set("motor.result", $result);
		
		$app->redirect($this->getRedirectUrl("cancelled"));
	}

	public function pinganPaymentCancel() {
		$app = JFactory::getApplication();
		$jInput = $app->input;
		
		$session = JFactory::getSession();
		$result= array();
		$result["success"] = false;
		$result["data"] =  (object) array(
				"transactionId" => $jInput->get('Ref')
		);
		$session->set("motor.result", $result);
		
		$app->redirect($this->getRedirectUrl("cancelled"));
	}
	
	public function hlaPaymentFail() {
		$app = JFactory::getApplication();
		$jInput = $app->input;
		
		$session = JFactory::getSession();
		$result= array();
		$result["success"] = false;
		$result["data"] =  (object) array(
				"transactionId" => $jInput->get('Ref'),
				"acknowledgementId" => "",
				"proposalId" => ""
		);
		$session->set("motor.result", $result);
		
		$model = $this->getModel();
		$model->saveErrorMessage(array("quotationId"=>$jInput->get('Ref'), "requestData"=>js_encode($jInput),"errorMsg"=>"HLA payment return failed"));
		
		$model->sendEmails(FALSE, FALSE, $jInput->get('Ref'));
		
		$app->redirect($this->getRedirectUrl("error"));
	}

	public function aigPaymentFail() {
		$app = JFactory::getApplication();
		$jInput = $app->input;
		
		$session = JFactory::getSession();
		$result= array();
		$result["success"] = false;
		$result["data"] =  (object) array(
				"transactionId" => $jInput->get('Ref'),
				"acknowledgementId" => "",
				"proposalId" => ""
		);
		$session->set("motor.result", $result);
		
		$model = $this->getModel();
		$model->saveErrorMessage(array("quotationId"=>$jInput->get('Ref'), "requestData"=>json_encode($jInput),"errorMsg"=>"AIG payment return failed"));
		
		$model->sendEmails(FALSE, FALSE, $jInput->get('Ref'));
		
		$app->redirect($this->getRedirectUrl("error"));
	}

	public function awPaymentFail() {
		$app = JFactory::getApplication();
		$jInput = $app->input;
		
		$session = JFactory::getSession();
		$result= array();
		$result["success"] = false;
		$result["data"] =  (object) array(
				"transactionId" => $jInput->get('Ref'),
				"acknowledgementId" => "",
				"proposalId" => ""
		);
		$session->set("motor.result", $result);
		
		$model = $this->getModel();
		$model->saveErrorMessage(array("quotationId"=>$jInput->get('Ref'), "requestData"=>json_encode($jInput),"errorMsg"=>"Allied World payment return failed"));
		
		$model->sendEmails(FALSE, FALSE, $jInput->get('Ref'));
		
		$app->redirect($this->getRedirectUrl("error"));
	}

	public function pinganPaymentFail() {
		$app = JFactory::getApplication();
		$jInput = $app->input;
		
		$session = JFactory::getSession();
		$result= array();
		$result["success"] = false;
		$result["data"] =  (object) array(
				"transactionId" => $jInput->get('Ref'),
				"acknowledgementId" => "",
				"proposalId" => ""
		);
		$session->set("motor.result", $result);
		
		$model = $this->getModel();
		$model->saveErrorMessage(array("quotationId"=>$jInput->get('Ref'), "requestData"=>json_encode($jInput),"errorMsg"=>"Pingan payment return failed"));
		
		$model->sendEmails(FALSE, FALSE, $jInput->get('Ref'));
		
		$app->redirect($this->getRedirectUrl("error"));
	}
	
	public function savePromoCode() {
		$jinput = JFactory::getApplication()->input;
		
		$model = $this->getModel();
		
		$result = $model->savePromoCode($jinput->get("code"), $jinput->get("quotationId"));
		
		echo json_encode($result);
		JFactory::getApplication()->close(); // or jexit();
	}

// 	public function changeToAnuual() {
// 		$app = JFactory::getApplication();
// 		$session = JFactory::getSession();
// 		$quotation_id = InsureDIYMotorHelper::getQid();
// 		$model = $this->getModel();

// 		$data = $session->get("motor.data");
// 		$data['trip_type'] = "AT";
// 		$session->set("motor.data", $data);
// 		if (!$model->changeToAnuual($quotation_id)) {
// 			$app->redirect($this->form_url, JText::_("DB_SAVE_FAIL"));
// 		}
// 		$app->redirect(JRoute::_($this->form_url.'&step=2'));
// 	}

// 	public function saveBeforePayment() {
// 		JSession::checkToken() or jexit(JText::_("JINVALID_TOKEN"));
// 		$quotation_id = InsureDIYMotorHelper::getQid();
// 		$db = JFactory::getDbo();
// 		$query = $db->getQuery(TRUE)
// 				->update("#__insure_motor_quotations")
// 				->set("payment_status = " . $db->quote("P"))
// 				->where("id = " . $db->quote($quotation_id));
// 		$db->setQuery($query);
// 		$json = array();
// 		if ($db->execute()) {
// //			InsureDIYMotorHelper::clearSessionData(); // Clear the session data.
// 			$json['success'] = "1";
// 		} else {
// 			$json['error'] = "1";
// 		}
// 		echo json_encode($json);
// 		exit;
// 	}

// 	public function startNewQuotation() {
// 		JSession::checkToken() or jexit(JText::_("JINVALID_TOKEN"));
// 		$app = JFactory::getApplication();
// 		InsureDIYMotorHelper::clearSessionData();
// 		$app->redirect(JRoute::_($this->form_url));
// 	}

	public function getRedirectUrl($layout) {
		$url = $this->base_layout_url . $layout;
		return $url;
	}

// 	public function back() {
// 		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
// 		$app = JFactory::getApplication();
// 		$quotation_id = $app->input->post->get("quotation_id", FALSE, "integer");
// 		$backstage = 0;
// 		if ($quotation_id) {
// 			$model = $this->getModel();
// 			$backstage = $model->back($quotation_id);
// 		}
// 		$app->redirect(JRoute::_($this->form_url.'&step='.$backstage));
// 	}

}
