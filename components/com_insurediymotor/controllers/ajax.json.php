<?php
defined('_JEXEC') or die;

class InsureDIYMotorControllerAjax extends JControllerLegacy{
	//pdf/img file uploaded path , make sure the path is writable and can be accessed in frontend
	protected $mediafile_path='/media/com_insurediymotor/upload';
	
	//Allowed upload file types, all lowercase
	protected $allow_ext_list = array('.jpeg','.pdf','.jpg','.png','.doc','.docx','.gif');

	public function testEmail() {
		$mailer = JFactory::getMailer();
		$mailer->sendMail("no-reply@insurediy.com", "InsureDIY", "vcckaskus@gmail.com", "hello world", "test message", TRUE);
		$params = JComponentHelper::getParams('com_insurediymotor');
		echo print_r($params);
		exit();
	}
	public function getCaptcha() {
		InsureDIYMotorHelper::getCaptcha();
	}
	public function validateCaptcha() {
		$jinput = JFactory::getApplication()->input;
		InsureDIYMotorHelper::validateCaptcha($jinput->get("captcha"));
	}

	public function generateToken() {
		echo json_encode(array("token" => JHtml::_('form.token')));
		exit;
	}
	public function getQuote() {		
		$jinput = JFactory::getApplication()->input;
		$session = JFactory::getSession();
		$result = array();
		$user = Jfactory::getUser();
		$encodeuser = json_encode($user);
		$decodeuser = json_decode($encodeuser,true);
		if($session) {
			$userData = $session->get("motor.data", FALSE);
			
			
			if($userData) {
				// 				$userData["workshop"] = $jinput->get("workshop");
				// 				$userData["los"]= $jinput->get("los");
				// 				$userData["ncdPro"]= $jinput->get("ncdPro");
				$userData["vehVariantHla"]= $jinput->get("variant-hla");
				$userData["vehVariantMsig"]= $jinput->get("variant-msig");
				$userData["vehVariantSompo"]= $jinput->get("variant-sompo");
				$userData["vehVariantAig"]= $jinput->get("variant-aig");
				$userData["vehVariantPingan"]= $jinput->get("variant-pingan");
				$userData["vehVariantAw"]= $jinput->get("variant-aw");
				$session->set("motor.data", $userData);
				
				$model = $this->getModel();
				
				$result = $model->getQuotations();
				
// 				$email = $decodeuser["email"];
// 				$name = trim($userData["firstName"]);
// 				$lastname = trim($userData["lastName"]);
// 				$quotationid = $userData["quotationId"];
// 				$userid = $decodeuser["id"];
// 				$root = JUri::root();
// 				$url = $root."buy-now/buy-motor-insurance.html?step=3";
// 				$product_id = "motor";
// 				$storeid = "insurediysingapore";
// 				$beforestoreid = "beforeinsurediytermsingapore";
// 				if ($result["success"] == true){
// 					$definecart = JoomailerMailchimpHelper::defineCart($email,$name,$lastname,$quotationid,$userid,$url,$product_id,$storeid);
// 					if ($definecart == 'A cart with the provided ID already exists in the account.'){
// 						$delcart = JoomailerMailchimpHelper::delCart($quotationid,$beforestoreid);
// 						if ($delcart == '204'){
// 							$sendmailchimp = JoomailerMailchimpHelper::addCart($email,$name,$lastname,$quotationid,$userid,$url,$product_id,$storeid);
// 						}
// 						//                $display = $sendmailchimp;
// 					} else {
// 						$sendmailchimp = JoomailerMailchimpHelper::addCart($email,$name,$lastname,$quotationid,$userid,$url,$product_id,$storeid);
						
// 						//                $display = 'no cart'.$sendmailchimp;
// 					}
					
// 					//            $ids = "";
// 					//            $cartid = $decodegetcart['carts'];
// 					//            foreach($cartid as $key => $value){
// 					//                $ids .= $value['id'].',';
// 					//            }
					
// 					$sendmailchimp = JoomailerMailchimpHelper::addCart($email,$name,$lastname,$quotationid,$userid,$url,$product_id);
					
// 				} else {
// 					$sendmailchimp = "Result false";
// 				}
			}
			else {
				$result["success"] = false;
				$result["error"] = "Motor data not found.";
			}
		}
		else {
			$result["success"] = false;
			$result["error"] = "Session data not found.";
		}
		
		
		$check = json_encode($result);
		//JFactory::getDocument()->setMimeEncoding( 'application/json' );
		
		echo $check;
		JFactory::getApplication()->close(); // or jexit();
	}
	
	public function specialContact() {
		$jinput = JFactory::getApplication()->input;
		$result = array();
		
		$user = JFactory::getUser();
		
		if($user->id)
		{
			$model = $this->getModel();
			
			$result["success"] = $model->specialContact($jinput->get("quotationId"));
			
		}
		else {
			$result["success"] = false;
			$result["error"] = "User data not found.";
		}

		
		
		$check = json_encode($result);
		
		//JFactory::getDocument()->setMimeEncoding( 'application/json' );
		
		echo $check;
		JFactory::getApplication()->close(); // or jexit();
	}
	
	public function getModelList() {
		$jinput = JFactory::getApplication()->input;
		
		$model = $this->getModel();
		
		$result = $model->getModelList($jinput->get("carMake"));
		
		echo json_encode($result);
		JFactory::getApplication()->close(); // or jexit();
	}

	public function getJobList() {
		$jinput = JFactory::getApplication()->input;

		$model = $this->getModel();
		
		$result = $model->getJobList($jinput->get("jobIndustry"));
		
		echo json_encode($result);
		JFactory::getApplication()->close(); // or jexit();
	}
	
	public function createPolicyResult() {
		$requestData = json_decode(file_get_contents('php://input'));
		$result = array();
		$date = new DateTime('now');
		$date->setTimezone(new DateTimeZone('Asia/Singapore'));
		$now = $date->format("Y-m-d\TH:i:s.u+08:00");
		$data = JFactory::getApplication()->input->post->getArray();
		
		if($requestData!= null) {
			$model = $this->getModel();
			
			$result = $model->sompoPolicyAckCallback($requestData);
			
			//Something to write to txt log
			//$log  = $now. ": SUCCESS".PHP_EOL.
			//"-------------------------".PHP_EOL.
			//"RESULT = ".json_encode($result).PHP_EOL.
			//"POST = ".json_encode($requestData).PHP_EOL;
			//Save string to log, use FILE_APPEND to append.
			//file_put_contents('./sompo.log', $log, FILE_APPEND);
		}
		else {
			//Something to write to txt log
			// 			$log  = $now. ": No Input".PHP_EOL.
			// 			"-------------------------".PHP_EOL.
			// 			"CHECK = ".json_encode($data).PHP_EOL.
			// 			"POST = ".json_encode($_POST).PHP_EOL;
			// 			//Save string to log, use FILE_APPEND to append.
			// 			file_put_contents('./sompo.log', $log, FILE_APPEND);
			
			$result = (object)array(
					"applicationId" => "SOMPO_ESB",
					"serviceId" => "createPolicyResult",
					"requestUniqueId" => InsureDIYMotorHelper::fullGuid(),
					"serviceRequestObject" => (object)array(
							"updatePolicyCreationReply" => (object)array(
									"statusCd" => 99,
									"statusDescription" => "No data received"
							)
					)
			);
			// 			header('HTTP/1.1 500 Internal Server Error');
			// 			header('Content-Type: application/json; charset=UTF-8');
			// 			die(json_encode(array('message' => 'No Data Received from JSON')));
		}
		
		echo json_encode($result);
		
		JFactory::getApplication()->close(); // or jexit();
	}
	
	public function applyPromoCode() {
		$user = JFactory::getUser();
		$jinput = JFactory::getApplication()->input;
		$session = JFactory::getSession();
		$promoCode = $jinput->get("promo_code");
		$action = $jinput->get("action");
		$quoteId = $jinput->get("quote_id");
		$promoType = $jinput->get("ptype");

		if(!$user->id || $user->id == 0){
			echo json_encode(array("success" => 0, "login" => false));
			exit;
		}
		
		if($action == "Apply") {
			if($promoCode != '') {
				$promo_type = $promoType;
				$old_promo_code = $session->get("motor.promo_code");
				
				//remove old promo code
				// 			if(!empty($old_promo_code) && $old_promo_code != $post['promo_code']) {
				// 				InsurediypromocodesHelper::unUsedPromoCode($old_promo_code);
				// 			}
				$lang = JFactory::getLanguage();
				$lang->load('com_insurediypromocodes',JPATH_ROOT);
				$promocodeResult = InsurediypromocodesHelper::checkPromocode($promoCode, $promo_type);
				
				$checkCode = json_decode($promocodeResult, true);
				
				if($checkCode['success'] == 1){
					$motorData = $session->get("motor.data", FALSE);
					
					$plan_amount = $motorData["plan_premium"];
					
					$promoCodeDiscount = InsurediypromocodesHelper::calculateDiscountCost($promoCode, $plan_amount);

					$promoCodeDiscount = str_replace(",", "", $promoCodeDiscount);
					
					$checkCode["discount"] = $promoCodeDiscount;

					//updating asiapay amount to create HASH
					$app = JFactory::getApplication();
					$params = $app->getParams();
					$request_id = InsureDIYMotorHelper::guid();
					$newPaymentRef = InsureDIYMotorHelper::updatePaymentRef($request_id, $quoteId);
					$merchant_account_id = $params->get('merchantId', "88117268");
					$transaction_type = 'N';
					$discountedAmount = number_format(($plan_amount-$checkCode["discount"]), 2, '.', '');
					$newHash = InsureDIYMotorHelper::generatePaymentSecureHash($params->get('merchantId'), $request_id, $params->get('currCode', '344'), $discountedAmount, $transaction_type, $params->get('hashCode'));
					$checkCode["hash"] = $newHash;
					$checkCode["reqid"] = $request_id;
					$checkCode["newamt"] = $discountedAmount;
					// finish updating
					
					$db = JFactory::getDbo();
					
					$objData = (object) array(
							"id" => $quoteId,
							"quote_promo_code" => $promoCode,
							"quote_promo_code_discount" => $promoCodeDiscount,
							"quote_updated_time" => JFactory::getDate()->toSql()
					);
					
					$db->updateObject("#__insure_motor_quote_master", $objData, "id");
					
					//$session->set("domestic.promo_code", $post['promo_code']);
				}
				
				echo json_encode($checkCode);
			}
		}
		else {
			$motorData = $session->get("motor.data", FALSE);
			$plan_amount = number_format($motorData["plan_premium"], 2, '.', '');
			$result = array();

			//updating asiapay amount to create HASH
			$app = JFactory::getApplication();
			$params = $app->getParams();
			$request_id = InsureDIYMotorHelper::guid();
			$newPaymentRef = InsureDIYMotorHelper::updatePaymentRef($request_id, $quoteId);
			$merchant_account_id = $params->get('merchantId', "88117268");
			$transaction_type = 'N';
			$newHash = InsureDIYMotorHelper::generatePaymentSecureHash($params->get('merchantId'), $request_id, $params->get('currCode', '344'), $plan_amount, $transaction_type, $params->get('hashCode'));
			$result["hash"] = $newHash;
			$result["reqid"] = $request_id;
			$result["newamt"] = $plan_amount;
			// finish updating

			
			$result['success'] = 1;
			$result['message'] = 'Promo Code is cancelled.';
			
			$db = JFactory::getDbo();
			
			$objData = (object) array(
					"id" => $quoteId,
					"quote_promo_code" => "",
					"quote_updated_time" => JFactory::getDate()->toSql()
			);
			
			$db->updateObject("#__insure_motor_quote_master", $objData, "id");
			
			echo json_encode($result);
		}
		
		JFactory::getApplication()->close(); // or jexit();
	}
	
	public function getModel($name = 'ajax', $prefix = '', $config = array('ignore_request' => true)) {
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}
	
	public function populateMakeModel() {
		$hlaFile = fopen(JURI::base()."/180609_MAKE-MODEL_HLA.csv","r");
		$sompoFile = fopen(JURI::base()."/180609_MAKE-MODEL_SOMPO.csv","r");
		$msigFile = fopen(JURI::base()."/180609_MAKE-MODEL_MSIG.csv","r");
		$db = JFactory::getDBO();
		
		$hlaMasterMakeList = array();
		$msigMasterMakeList = array();
		$sompoMasterMakeList = array();
		
		$hlaMasterModelList = array();
		$msigMasterModelList = array();
		$sompoMasterModelList = array();
		
		$hlaVariantList = array();
		$msigVariantList = array();
		$sompoVariantList = array();
		
		$query = $db->getQuery(TRUE)
		->delete("#__insure_motor_make_master");
		$db->setQuery($query);
		$check = $db->execute();
		
		$query = "ALTER TABLE #__insure_motor_make_master AUTO_INCREMENT=1";
		$db->setQuery($query);
		$check = $db->execute();
		
		$query = $db->getQuery(TRUE)
		->delete("#__insure_motor_model_master");
		$db->setQuery($query);
		$check = $db->execute();
		
		$query = "ALTER TABLE #__insure_motor_model_master AUTO_INCREMENT=1";
		$db->setQuery($query);
		$check = $db->execute();
		
		$query = $db->getQuery(TRUE)
		->delete("#__insure_motor_make_model_hla");
		$db->setQuery($query);
		$check = $db->execute();
		
		$query = "ALTER TABLE #__insure_motor_make_model_hla AUTO_INCREMENT=1";
		$db->setQuery($query);
		$check = $db->execute();
		
		$query = $db->getQuery(TRUE)
		->delete("#__insure_motor_make_model_msig");
		$db->setQuery($query);
		$check = $db->execute();
		
		$query = "ALTER TABLE #__insure_motor_make_model_msig AUTO_INCREMENT=1";
		$db->setQuery($query);
		$check = $db->execute();
		
		$query = $db->getQuery(TRUE)
		->delete("#__insure_motor_make_model_sompo");
		$db->setQuery($query);
		$check = $db->execute();
		
		$query = "ALTER TABLE #__insure_motor_make_model_sompo AUTO_INCREMENT=1";
		$db->setQuery($query);
		$check = $db->execute();
		
		$counter = 0;
		while(!feof($hlaFile))
		{
			$row = fgetcsv($hlaFile);
			
			if($counter > 0) {
				$masterMakeData = (object) array(
						"make_code" => str_replace(' ', '_', strtoupper($row[5])),
						"make_name" => strtoupper($row[5]),
				);
				
				$hlaMasterMakeList[$masterMakeData->make_code] = $masterMakeData;
				
				$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_motor_make_master")
				->where("make_code = ".$db->quote($masterMakeData->make_code));
				$db->setQuery($query);
				$check = $db->loadObject();
				
				if(!$check) {
					$check = $db->insertObject("#__insure_motor_make_master", $masterMakeData);
				}
				
				$masterModelData = (object) array(
						"make_code" => $masterMakeData->make_code,
						"model_code" => $masterMakeData->make_code."-".strtoupper(str_replace(' ', '_', $row[6])),
						"model_name" => strtoupper($row[6]),
						"model_cc" => $row[4],
				);
				
				$hlaMasterModelList[$masterModelData->model_code] = $masterModelData->model_code;
				
				$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_motor_model_master")
				->where("model_code = ".$db->quote($masterModelData->model_code));
				$db->setQuery($query);
				$check = $db->loadObject();
				
				if(!$check) {
					$check = $db->insertObject("#__insure_motor_model_master", $masterModelData);
				}
				
				$hlaModelData = (object) array(
						"make_code" => $row[0],
						"model_code" => $row[2],
						"model_name" => strtoupper($row[1]),
						"model_master_code" => $masterModelData->model_code,
						"model_cc" => $row[4]
				);
				
				$hlaVariantList[$hlaModelData->model_code] = $hlaModelData;
				
				$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_motor_make_model_hla")
				->where("model_code = ".$db->quote($hlaModelData->model_code));
				$db->setQuery($query);
				$check = $db->loadObject();
				
				if(!$check) {
					$check = $db->insertObject("#__insure_motor_make_model_hla", $hlaModelData);
				}
			}
			
			$counter++;
		}
		
		fclose($hlaFile);
		
		$counter = 0;
		while(!feof($msigFile))
		{
			$row = fgetcsv($msigFile);
			
			if($counter > 0) {
				$masterMakeData = (object) array(
						"make_code" => str_replace(' ', '_', strtoupper($row[0])),
						"make_name" => strtoupper($row[0]),
				);
				
				$msigMasterMakeList[$masterMakeData->make_code] = $masterMakeData;
				
				$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_motor_make_master")
				->where("make_code = ".$db->quote($masterMakeData->make_code));
				$db->setQuery($query);
				$check = $db->loadObject();
				
				if(!$check) {
					$check = $db->insertObject("#__insure_motor_make_master", $masterMakeData);
				}
				
				$masterModelData = (object) array(
						"make_code" => $masterMakeData->make_code,
						"model_code" => $masterMakeData->make_code."-".strtoupper(str_replace(' ', '_', $row[1])),
						"model_name" => strtoupper($row[1]),
						"model_cc" => $row[5],
				);
				
				$msigMasterModelList[$masterModelData->model_code] = $masterModelData->model_code;
				
				$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_motor_model_master")
				->where("model_code = ".$db->quote($masterModelData->model_code));
				$db->setQuery($query);
				$check = $db->loadObject();
				
				if(!$check) {
					$check = $db->insertObject("#__insure_motor_model_master", $masterModelData);
				}
				
				$msigModelData = (object) array(
						"model_cc" => $row[6],
						"model_code" => $row[3],
						"model_name" => strtoupper($row[4]),
						"model_master_code" => $masterModelData->model_code,
						"model_cc" => $row[5]
				);
				
				$msigVariantList[$msigModelData->model_code] = $msigModelData;
				
				$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_motor_make_model_msig")
				->where("model_code = ".$db->quote($msigModelData->model_code));
				$db->setQuery($query);
				$check = $db->loadObject();
				
				if(!$check) {
					$check = $db->insertObject("#__insure_motor_make_model_msig", $msigModelData);
				}
			}
			
			$counter++;
		}
		
		fclose($msigFile);
		
		$counter = 0;
		while(!feof($sompoFile))
		{
			$row = fgetcsv($sompoFile);
			
			if($counter > 0) {
				$masterMakeData = (object) array(
						"make_code" => str_replace(' ', '_', strtoupper($row[0])),
						"make_name" => strtoupper($row[0]),
				);
				
				$sompoMasterMakeList[$masterMakeData->make_code] = $masterMakeData;
				
				$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_motor_make_master")
				->where("make_code = ".$db->quote($masterMakeData->make_code));
				$db->setQuery($query);
				$check = $db->loadObject();
				
				if(!$check) {
					$check = $db->insertObject("#__insure_motor_make_master", $masterMakeData);
				}
				
				$masterModelData = (object) array(
						"make_code" => $masterMakeData->make_code,
						"model_code" => $masterMakeData->make_code."-".strtoupper(str_replace(' ', '_', $row[1])),
						"model_name" => strtoupper($row[1]),
						"model_cc" => $row[7],
				);
				
				$sompoMasterModelList[$masterModelData->model_code] = $masterModelData->model_code;
				
				$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_motor_model_master")
				->where("model_code = ".$db->quote($masterModelData->model_code));
				$db->setQuery($query);
				$check = $db->loadObject();
				
				if(!$check) {
					$check = $db->insertObject("#__insure_motor_model_master", $masterModelData);
				}
				
				$sompoModelData = (object) array(
						"model_code" => $row[3],
						"model_name" => strtoupper($row[4]),
						"model_master_code" => $masterModelData->model_code,
						"model_cc" => $row[7],
						"model_bodytype" => $row[10],
						"model_body_desc" => $row[16],
						"model_no_passenger" => $row[11],
						"model_make_code" => $row[5]
				);
				
				$sompoVariantList[$sompoModelData->model_code] = $sompoModelData;
				
				$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_motor_make_model_sompo")
				->where("model_code = ".$db->quote($sompoModelData->model_code));
				$db->setQuery($query);
				$check = $db->loadObject();
				
				if(!$check) {
					$check = $db->insertObject("#__insure_motor_make_model_sompo", $sompoModelData);
				}
			}
			
			$counter++;
		}
		
		fclose($sompoFile);
		
		//Checking section
		// 		$sompoMakeUniqList= array_diff_key($sompoMasterMakeList, $hlaMasterMakeList, $msigMasterMakeList);
		// 		$hlaMakeUniqList= array_diff_key($hlaMasterMakeList, $sompoMasterMakeList, $msigMasterMakeList);
		// 		$msigMakeUniqList= array_diff_key($msigMasterMakeList, $sompoMasterMakeList, $hlaMasterMakeList);
		
		// 		$sompoModelUniqList = array_diff($sompoMasterModelList, $hlaMasterModelList, $msigMasterModelList);
		// 		$hlaModelUniqList = array_diff($hlaMasterModelList, $sompoMasterModelList, $msigMasterModelList);
		// 		$msigModelUniqList = array_diff($msigMasterModelList, $sompoMasterModelList, $hlaMasterModelList);
		
		// 		ksort($sompoModelUniqList);
		// 		ksort($hlaModelUniqList);
		// 		ksort($msigModelUniqList);
		
		// 		echo json_encode((object)array("sompo"=>$sompoModelUniqList, "hla"=>$hlaModelUniqList, "msig"=>$msigModelUniqList));
		echo "OK";
	}
	
	// 	public function populateMakeModelHla() {
	// 		$file = fopen(JURI::base()."/180504-Make-Model_HLA.csv","r");
	// 		$db = JFactory::getDBO();
	// 		$counter = 0;
	
	// 		while(!feof($file))
		// 		{
		// 			$row = fgetcsv($file);
	
		// 			if($counter > 0) {
		// 				$masterMakeData = (object) array(
		// 						"make_code" => str_replace(' ', '_', $row[5]),
		// 						"make_name" => $row[5],
		// 				);
	
		// 				$query = $db->getQuery(TRUE)
		// 				->select("*")
		// 				->from("#__insure_motor_make_master")
		// 				->where("make_code = ".$db->quote($masterMakeData->make_code));
		// 				$db->setQuery($query);
		// 				$check = $db->loadObject();
	
		// 				if(!$check) {
		// 					$check = $db->insertObject("#__insure_motor_make_master", $masterMakeData);
		// 				}
	
		// 				$masterModelData = (object) array(
		// 						"make_code" => str_replace(' ', '_', $row[5]),
		// 						"model_code" => str_replace(' ', '_', $row[5])."-".str_replace(' ', '_', $row[6]),
		// 						"model_name" => $row[6]
		// 				);
	
		// 				$query = $db->getQuery(TRUE)
		// 				->select("*")
		// 				->from("#__insure_motor_model_master")
		// 				->where("model_code = ".$masterModelData->model_code);
		// 				$db->setQuery($query);
		// 				$check = $db->loadObject();
	
		// 				if(!$check) {
		// 					$check = $db->insertObject("#__insure_motor_model_master", $masterModelData);
		// 				}
	
		// 				$hlaModelData = (object) array(
		// 						"make_code" => $row[0],
		// 						"model_code" => $row[2],
		// 						"model_name" => $row[1],
		// 						"model_master_code" => $masterModelData->model_code,
		// 						"model_cc" => $row[4]
		// 				);
	
		// 				$query = $db->getQuery(TRUE)
		// 				->select("*")
		// 				->from("#__insure_motor_make_model_hla")
		// 				->where("model_code = ".$hlaModelData->model_code);
		// 				$db->setQuery($query);
		// 				$check = $db->loadObject();
	
		// 				if(!$check) {
		// 					$check = $db->insertObject("#__insure_motor_make_model_hla", $hlaModelData);
		// 				}
	
		// 				print_r($hlaModelData);
		// 			}
	
		// 			$counter++;
		// 		}
	
	// 		fclose($file);
	// 	}
	
	// 	public function populateMakeModelMsig() {
	// 		$file = fopen(JURI::base()."/180401_MAKE-MODEL_MSIG.csv","r");
	// 		$db = JFactory::getDBO();
	// 		$counter = 0;
	
	// 		while(!feof($file))
		// 		{
		// 			$row = fgetcsv($file);
	
		// 			if($counter > 0) {
		// 				$masterMakeData = (object) array(
		// 						"make_code" => str_replace(' ', '_', $row[0]),
		// 						"make_name" => $row[0],
		// 				);
	
		// 				$query = $db->getQuery(TRUE)
		// 				->select("*")
		// 				->from("#__insure_motor_make_master")
		// 				->where("make_code = ".$masterMakeData->make_code);
		// 				$db->setQuery($query);
		// 				$check = $db->loadObject();
	
		// 				if(!$check) {
		// 					$check = $db->insertObject("#__insure_motor_make_master", $masterMakeData);
		// 				}
	
		// 				$masterModelData = (object) array(
		// 						"make_code" => str_replace(' ', '_', $row[0]),
		// 						"model_code" => str_replace(' ', '_', $row[0])."-".str_replace(' ', '_', $row[1]),
		// 						"model_name" => $row[1],
		// 						"model_cc" => $row[5],
		// 				);
	
		// 				$query = $db->getQuery(TRUE)
		// 				->select("*")
		// 				->from("#__insure_motor_model_master")
		// 				->where("model_code = ".$masterModelData->model_code);
		// 				$db->setQuery($query);
		// 				$check = $db->loadObject();
	
		// 				if(!$check) {
		// 					$check = $db->insertObject("#__insure_motor_model_master", $masterModelData);
		// 				}
	
		// 				$msigModelData = (object) array(
		// 						"model_code" => $row[3],
		// 						"model_name" => $row[4],
		// 						"model_master_code" => $masterModelData->model_code,
		// 						"model_cc" => $row[5]
		// 				);
	
		// 				$query = $db->getQuery(TRUE)
		// 				->select("*")
		// 				->from("#__insure_motor_make_model_msig")
		// 				->where("model_code = ".$msigModelData->model_code);
		// 				$db->setQuery($query);
		// 				$check = $db->loadObject();
	
		// 				if(!$check) {
		// 					$check = $db->insertObject("#__insure_motor_make_model_msig", $msigModelData);
		// 				}
	
		// 				print_r($msigModelData);
		// 			}
	
		// 			$counter++;
		// 		}
	
	// 		fclose($file);
	// 	}
	
	// 	public function populateMakeModelSompo() {
	// 		$file = fopen(JURI::base()."/180417_MAKE-MODEL_SOMPO.csv","r");
	// 		$db = JFactory::getDBO();
	// 		$counter = 0;
	
	// 		while(!feof($file))
		// 		{
		// 			$row = fgetcsv($file);
	
		// 			if($counter > 0) {
		// 				$masterMakeData = (object) array(
		// 						"make_code" => str_replace(' ', '_', $row[0]),
		// 						"make_name" => $row[0],
		// 				);
	
		// 				$query = $db->getQuery(TRUE)
		// 						->select("*")
		// 						->from("#__insure_motor_make_master")
		// 						->where("make_code = ".$masterMakeData->make_code);
		// 				$db->setQuery($query);
		// 				$check = $db->loadObject();
	
		// 				if(!$check) {
		// 					$check = $db->insertObject("#__insure_motor_make_master", $masterMakeData);
		// 				}
	
		// 				$masterModelData = (object) array(
		// 						"make_code" => str_replace(' ', '_', $row[0]),
		// 						"model_code" => str_replace(' ', '_', $row[0])."-".str_replace(' ', '_', $row[1]),
		// 						"model_name" => $row[1],
		// 						"model_cc" => $row[7],
		// 				);
	
		// 				$query = $db->getQuery(TRUE)
		// 				->select("*")
		// 				->from("#__insure_motor_model_master")
		// 				->where("model_code = ".$masterModelData->model_code);
		// 				$db->setQuery($query);
		// 				$check = $db->loadObject();
	
		// 				if(!$check) {
		// 					$check = $db->insertObject("#__insure_motor_model_master", $masterModelData);
		// 				}
	
		// 				$sompoModelData = (object) array(
		// 						"model_code" => $row[3],
		// 						"model_name" => $row[2],
		// 						"model_master_code" => $masterModelData->model_code,
		// 						"model_cc" => $row[7],
		// 						"model_bodytype" => $row[10],
		// 						"model_body_desc" => $row[16],
		// 						"model_no_passenger" => $row[11],
		// 						"model_make_code" => $row[5],
		// 				);
	
		// 				$query = $db->getQuery(TRUE)
		// 				->select("*")
		// 				->from("#__insure_motor_make_model_sompo")
		// 				->where("model_code = ".$sompoModelData->model_code);
		// 				$db->setQuery($query);
		// 				$check = $db->loadObject();
	
		// 				if(!$check) {
		// 					$check = $db->insertObject("#__insure_motor_make_model_sompo", $sompoModelData);
		// 				}
	
		// 				print_r($sompoModelData);
		// 			}
	
		// 			$counter++;
		// 		}
	
	// 		fclose($file);
	// 	}
	
	public function populateOccupation() {
		$file = fopen(JURI::base()."/occupation_listing_complete.csv","r");
		$db = JFactory::getDBO();
		$counter = 0;
		
		while(!feof($file))
		{
			$row = fgetcsv($file);
			if($counter > 0) {
				$occupationData = (object) array(
						"occ_code" => $row[2],
						"occ_name" => $row[0],
						"occ_hla_code" => $row[1]
				);
				
				$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_motor_occupation")
				->where("occ_code = ".$db->quote($occupationData->occ_code));
				$db->setQuery($query);
				$check = $db->loadObject();
				
				if(!$check) {
					$check = $db->insertObject("#__insure_motor_occupation", $occupationData);
					
					print_r($occupationData);
				}
			}
			
			$counter++;
		}
		
		fclose($file);
	}
	
	public function populateCurrentInsurer() {
		$file = fopen(JURI::base()."/currentInsurer.csv","r");
		$db = JFactory::getDBO();
		$counter = 0;
		
		while(!feof($file))
		{
			$row = fgetcsv($file);
			
			$insurerData = (object) array(
					"insurer_code" => $row[0],
					"insurer_name" => $row[1],
					"insurer_sequence" => $row[2],
			);
			
			$query = $db->getQuery(TRUE)
			->select("*")
			->from("#__insure_motor_current_insurer")
			->where("insurer_code = ".$db->quote($insurerData->insurer_code));
			$db->setQuery($query);
			$check = $db->loadObject();
			
			if(!$check) {
				$check = $db->insertObject("#__insure_motor_current_insurer", $insurerData);
				
				print_r($insurerData);
			}
		}
		
		fclose($file);
	}
	
	// 	public function populateRenewalInsurer() {
	// 		$db = JFactory::getDBO();
	
	// 		$insurerData = (object) array(
	// 				"renewal_partner_code" => "NCD000013",
	// 				"renewal_partner_name" => "LIBERTY INSURANCE",
	// 				"renewal_partner_name_short" => "liberty",
	// 		);
	
	// 		$check = $db->insertObject("#__insure_motor_renewal_partner", $insurerData);
	
	// 		if($check)
		// 		echo "OK";
		// 		else {
		// 			echo "failed";
		// 		}
		// 	}
	
	public function cleanMakeModel() {
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		
		$master = "#__insure_motor_make_master";
		$model = "#__insure_motor_model_master";
		$hla = "#__insure_motor_make_model_hla";
		$msig = "#__insure_motor_make_model_msig";
		$sompo = "#__insure_motor_make_model_sompo";
		
		$query->delete($master)->where("id=1003911");
		$db->setQuery($query);
		
		// 		$result = $db->execute();
		
		// 		$query = $db->getQuery(true);
		// 		$query->delete($hla);
		// 		$db->setQuery($query);
		
		// 		$result = $db->execute();
		
		// 		$query = $db->getQuery(true);
		// 		$query->delete($msig);
		// 		$db->setQuery($query);
		
		// 		$result = $db->execute();
		
		// 		$query = $db->getQuery(true);
		// 		$query->delete($sompo);
		// 		$db->setQuery($query);
		
		// 		$result = $db->execute();
		
		// 		$query = $db->getQuery(true);
		// 		$query->delete($model);
		// 		$db->setQuery($query);
		
		// 		$result = $db->execute();
	}
	
	public function CheckList() {
		
		// 		$db = JFactory::getDBO();
		// 		$query = $db->getQuery(TRUE)
		// 		->select("*")
		// 		->from("#__insure_motor_make_master")
		// 		->where("make_code > ''")
		// 		->order("make_name ASC");
		// 		$db->setQuery($query);
		// 		$carMakeData = $db->loadObjectList();
		
		// 		print_r($carMakeData);
	}
	
	public function getUserFromReferral() {
		$jinput = JFactory::getApplication()->input;
		
		if($jinput->get("referral") && $jinput->get("referral") != "") {
			$db = JFactory::getDBO();
			$query = $db->getQuery(TRUE)
			->select("id")
			->from("#__users")
			->where("referral_id=".$db->quote($jinput->get("referral")));
			$db->setQuery($query);
			$userData = $db->loadObjectList();
			
			print_r($userData);
		}
	}
	
	public function resetUserQuote() {
		$db = JFactory::getDBO();
		$jinput = JFactory::getApplication()->input;
		
		if($jinput->get("quote_id") && $jinput->get("quote_id") != "") {
			$quoteData = (object) array(
					"id" => $jinput->get("quote_id"),
					"quote_pre_quote_change_flag" => "true"
			);
			
			$check = $db->updateObject("#__insure_motor_quote_master", $quoteData, "id");
			
			print_r($check);
		}
		
		JFactory::getApplication()->close();
	}
	
	public function updateLexus() {
		// 		$db = JFactory::getDBO();
		
		// 		$query = $db->getQuery(TRUE)
		// 		->select("id")
		// 		->from("#__insure_motor_make_model_sompo")
		// 		->where("model_code='LEXU019A'");
		// 		$db->setQuery($query);
		// 		$id = $db->loadResult();
		
		// 		print_r($id);
		// 		echo "<br>";
		// 		echo "<br>";
		
		// 		$lexusData = (object) array(
		// 				"id" => $id,
		// 				"model_make_code" => "VMLEXU"
		// 		);
		
		// 		$check = $db->updateObject("#__insure_motor_make_model_sompo", $lexusData, "id");
		
		// 		$query = $db->getQuery(TRUE)
		// 		->select("*")
		// 		->from("#__insure_motor_make_model_sompo")
		// 		->where("model_code='LEXU019A'");
		// 		$db->setQuery($query);
		// 		$data = $db->loadObjectList();
		
		// 		print_r($data);
	}
	
	public function CheckError() {
		
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
		->select("*")
		->from("#__insure_motor_policy_error");
		$db->setQuery($query);
		$carMakeData = $db->loadObjectList();
		
		print_r($carMakeData);
	}
	
	public function CheckQuote() {
		// 		$db = JFactory::getDBO();
		
		// 		$query = $db->getQuery(TRUE)
		// 		->select("*")
		// 		->from("#__insure_motor_quote_master")
		// 		->where("id=5000012");
		// 		$db->setQuery($query);
		// 		$quoteData = $db->loadObject();
		
		// 		print_r($quoteData);
		// 		echo "<br><br>";
		
		// 		$query = $db->getQuery(TRUE)
		// 		->select("*")
		// 		->from("#__insure_motor_quote_msig")
		// 		->where("quote_master_id=5000012");
		// 		$db->setQuery($query);
		// 		$msigData = $db->loadObject();
		
		// 		print_r($msigData);
		// 		echo "<br><br>";
		
		// 		$query = $db->getQuery(TRUE)
		// 		->select("*")
		// 		->from("#__insure_motor_driver_info")
		// 		->where("quote_master_id=5000012");
		// 		$db->setQuery($query);
		// 		$driverData = $db->loadObject();
		
		// 		print_r($driverData);
		// 		echo "<br><br>";
	}
	
	public function updateReferenceId() {
		$jinput = JFactory::getApplication()->input;
		
		if($jinput->get("quotationId") && $jinput->get("quotationId") != "") {
			$db = JFactory::getDBO();
			$query = $db->getQuery(TRUE)
			->select("id")
			->from("#__insure_motor_quote_master")
			->where("id=".$db->quote($jinput->get("quotationId")));
			$db->setQuery($query);
			$userData = $db->loadResult();
			
			if($userData) {
				$data = (object) array(
						"id" => $jinput->get("quotationId"),
						"quote_payment_request_id" => $jinput->get("requestId")
				);
				
				$check = $db->updateObject("#__insure_motor_quote_master", $data, "id");
				
				if($check) {
					echo "OK";
				}
				else {
					echo "failed";
				}
			}
		}
	}

	public function deleteFile() {
		$jinput = JFactory::getApplication()->input;
		$session = JFactory::getSession();
		$uid = $session->get("user.id", false);
		$uploaddir = JPATH_BASE.$this->mediafile_path;
		$fileName = $_POST['filename'];
		$fileType = "quote_".$_POST['filetype'];

		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
		->select("id")
		->from("#__insure_motor_quote_master")
		->where("userId=".$db->quote($uid))
		->where($fileType."=".$db->quote('media/com_insurediymotor/upload/'.$fileName))
		->where("quote_payment_request_id=".$db->quote($jinput->get("pid")));

		$db->setQuery($query);
		$userData = $db->loadResult();

		if(file_exists('media/com_insurediymotor/upload/'.$fileName)) unlink('media/com_insurediymotor/upload/'.$fileName);

		$data = (object) array(
			"id" => $userData,
			$fileType => ""
		);
	
		$update = $db->updateObject("#__insure_motor_quote_master", $data, "id");
		if($update) {
			echo json_encode(array("success" => true));
		}
		else {
			echo json_encode(array("success" => false));
		}
	}

	public function uploadFinish() {
		$jinput = JFactory::getApplication()->input;
		$session = JFactory::getSession();
		$uid = $session->get("user.id", false);

		if($uid == 0){
			echo json_encode(array("login" => false));
			exit();
		}

		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
		->select("id")
		->from("#__insure_motor_quote_master")
		->where("userId=".$db->quote($uid))
		->where("quote_payment_request_id=".$db->quote($jinput->get("pid")));
		$db->setQuery($query);
		$userData = $db->loadResult();

		$data = (object) array(
			"id" => $userData,
			"quote_file_finish" => '1'
		);
	
		$update = $db->updateObject("#__insure_motor_quote_master", $data, "id");

		if($update) {
			echo json_encode(array("success" => true));
		}
		else {
			echo json_encode(array("success" => false));
		}
	}

	public function uploadFile() {
		$jinput = JFactory::getApplication()->input;
		$session = JFactory::getSession();
		$uid = $session->get("user.id", false);

		$customerData = $session->get("motor.data", FALSE);
		$qid = $customerData["quotationId"];

		if($uid == 0){
			if($qid) {
				$db = JFactory::getDBO();
				$query = $db->getQuery(TRUE)
				->select("id")
				->from("#__insure_motor_quote_master")
				->where("id=".$db->quote($qid))
				->where("quote_payment_request_id=".$db->quote($jinput->get("pid")));
				$db->setQuery($query);
				$userData = $db->loadResult();
			} else {
				echo json_encode(array("login" => false));
				exit();
			}
		
		} else {
			$db = JFactory::getDBO();
			$query = $db->getQuery(TRUE)
			->select("id")
			->from("#__insure_motor_quote_master")
			->where("userId=".$db->quote($uid))
			->where("quote_payment_request_id=".$db->quote($jinput->get("pid")));
			$db->setQuery($query);
			$userData = $db->loadResult();
		}

		


		$uploaddir = JPATH_BASE.$this->mediafile_path;
		$siteuploadurl=MyUri::getUrls("site").$this->mediafile_path;
		$file_vehicleReg_error = "";
		$file_insured_error = "";
		$file_hkid_error = "";
		$file_renewal_error = "";
		$file_ncd_error = "";
		
		if(isset($_FILES['file_vehicleReg'])){
			$ext = strtolower(strrchr($_FILES['file_vehicleReg']['name'],'.'));
			
			if(!in_array($ext,$this->allow_ext_list)){
				$file_vehicleReg_error= 'Invalid File Format';
			}

			if($file_vehicleReg_error==''){
				$realfilename = "VEHICLE_REG_DOC_".$uid."_".str_replace(" ", "_", $_FILES['file_vehicleReg']['name']);
				
				if(move_uploaded_file($_FILES['file_vehicleReg']['tmp_name'], $uploaddir.'/'.$realfilename)){
					
					$data = (object) array(
						"id" => $userData,
						"quote_file_vehicle_reg" => 'media/com_insurediymotor/upload/'.$realfilename
					);
				
					$update = $db->updateObject("#__insure_motor_quote_master", $data, "id");
					if($update) {
						echo json_encode(array("success" => true, "file" => $realfilename, "type" => "file_vehicle_reg"));
					}
					else {
						echo json_encode(array("success" => false));
					}
				}
				else {
					echo json_encode(array("message" => "Failed to move file to destination", "variable" => $uploaddir.'/'.$realfilename));
					
					JFactory::getApplication()->close(); // or jexit();
				}
			}
			else {
				echo json_encode(array("message", $file_vehicleReg_error));
				
				JFactory::getApplication()->close(); // or jexit();
			}

		}

		if(isset($_FILES['file_insured'])){
			$ext = strtolower(strrchr($_FILES['file_insured']['name'],'.'));
			
			if(!in_array($ext,$this->allow_ext_list)){
				$file_insured_error= 'Invalid File Format';
			}

			if($file_insured_error==''){
				$realfilename = "DRIVING_LICENSE_".$uid."_".str_replace(" ", "_", $_FILES['file_insured']['name']);
				
				if(move_uploaded_file($_FILES['file_insured']['tmp_name'], $uploaddir.'/'.$realfilename)){
					$data = (object) array(
						"id" => $userData,
						"quote_file_driving_license" => 'media/com_insurediymotor/upload/'.$realfilename
					);
				
					$update = $db->updateObject("#__insure_motor_quote_master", $data, "id");
					if($update) {
						echo json_encode(array("success" => true, "file" => $realfilename, "type" => "file_driving_license"));
					}
					else {
						echo json_encode(array("success" => false));
					}
				}
				else {
					echo json_encode(array("message" => "Failed to move file to destination", "variable" => $uploaddir.'/'.$realfilename));
					
					JFactory::getApplication()->close(); // or jexit();
				}
			}
			else {
				echo json_encode(array("message", $file_insured_error));
				
				JFactory::getApplication()->close(); // or jexit();
			}

		}

		if(isset($_FILES['file_insured_2'])){
			$ext = strtolower(strrchr($_FILES['file_insured_2']['name'],'.'));
			
			if(!in_array($ext,$this->allow_ext_list)){
				$file_insured_2_error= 'Invalid File Format';
			}

			if($file_insured_2_error==''){
				$realfilename = "DRIVING_LICENSE_2_".$uid."_".str_replace(" ", "_", $_FILES['file_insured_2']['name']);
				
				if(move_uploaded_file($_FILES['file_insured_2']['tmp_name'], $uploaddir.'/'.$realfilename)){
					$data = (object) array(
						"id" => $userData,
						"quote_file_driving_license_2" => 'media/com_insurediymotor/upload/'.$realfilename
					);
				
					$update = $db->updateObject("#__insure_motor_quote_master", $data, "id");
					if($update) {
						echo json_encode(array("success" => true, "file" => $realfilename, "type" => "file_driving_license"));
					}
					else {
						echo json_encode(array("success" => false));
					}
				}
				else {
					echo json_encode(array("message" => "Failed to move file to destination", "variable" => $uploaddir.'/'.$realfilename));
					
					JFactory::getApplication()->close(); // or jexit();
				}
			}
			else {
				echo json_encode(array("message", $file_insured_error));
				
				JFactory::getApplication()->close(); // or jexit();
			}

		}

		if(isset($_FILES['file_insured_3'])){
			$ext = strtolower(strrchr($_FILES['file_insured_3']['name'],'.'));
			
			if(!in_array($ext,$this->allow_ext_list)){
				$file_insured_3_error= 'Invalid File Format';
			}

			if($file_insured_3_error==''){
				$realfilename = "DRIVING_LICENSE_3_".$uid."_".str_replace(" ", "_", $_FILES['file_insured_3']['name']);
				
				if(move_uploaded_file($_FILES['file_insured_3']['tmp_name'], $uploaddir.'/'.$realfilename)){
					$data = (object) array(
						"id" => $userData,
						"quote_file_driving_license_3" => 'media/com_insurediymotor/upload/'.$realfilename
					);
				
					$update = $db->updateObject("#__insure_motor_quote_master", $data, "id");
					if($update) {
						echo json_encode(array("success" => true, "file" => $realfilename, "type" => "file_driving_license"));
					}
					else {
						echo json_encode(array("success" => false));
					}
				}
				else {
					echo json_encode(array("message" => "Failed to move file to destination", "variable" => $uploaddir.'/'.$realfilename));
					
					JFactory::getApplication()->close(); // or jexit();
				}
			}
			else {
				echo json_encode(array("message", $file_insured_error));
				
				JFactory::getApplication()->close(); // or jexit();
			}

		}

		if(isset($_FILES['file_hkid'])){
			$ext = strtolower(strrchr($_FILES['file_hkid']['name'],'.'));
			
			if(!in_array($ext,$this->allow_ext_list)){
				$file_hkid_error= 'Invalid File Format';
			}

			if($file_hkid_error==''){
				$realfilename = "HKID_".$uid."_".str_replace(" ", "_", $_FILES['file_hkid']['name']);
				
				if(move_uploaded_file($_FILES['file_hkid']['tmp_name'], $uploaddir.'/'.$realfilename)){
					$data = (object) array(
						"id" => $userData,
						"quote_file_hkid" => 'media/com_insurediymotor/upload/'.$realfilename
					);
				
					$update = $db->updateObject("#__insure_motor_quote_master", $data, "id");
					if($update) {
						echo json_encode(array("success" => true, "file" => $realfilename, "type" => "file_hkid"));
					}
					else {
						echo json_encode(array("success" => false));
					}
				}
				else {
					echo json_encode(array("message" => "Failed to move file to destination", "variable" => $uploaddir.'/'.$realfilename));
					
					JFactory::getApplication()->close(); // or jexit();
				}
			}
			else {
				echo json_encode(array("message", $file_hkid_error));
				
				JFactory::getApplication()->close(); // or jexit();
			}

		}

		if(isset($_FILES['file_hkid_2'])){
			$ext = strtolower(strrchr($_FILES['file_hkid_2']['name'],'.'));
			
			if(!in_array($ext,$this->allow_ext_list)){
				$file_hkid_2_error= 'Invalid File Format';
			}

			if($file_hkid_2_error==''){
				$realfilename = "HKID_2_".$uid."_".str_replace(" ", "_", $_FILES['file_hkid_2']['name']);
				
				if(move_uploaded_file($_FILES['file_hkid_2']['tmp_name'], $uploaddir.'/'.$realfilename)){
					$data = (object) array(
						"id" => $userData,
						"quote_file_hkid_2" => 'media/com_insurediymotor/upload/'.$realfilename
					);
				
					$update = $db->updateObject("#__insure_motor_quote_master", $data, "id");
					if($update) {
						echo json_encode(array("success" => true, "file" => $realfilename, "type" => "file_hkid_2"));
					}
					else {
						echo json_encode(array("success" => false));
					}
				}
				else {
					echo json_encode(array("message" => "Failed to move file to destination", "variable" => $uploaddir.'/'.$realfilename));
					
					JFactory::getApplication()->close(); // or jexit();
				}
			}
			else {
				echo json_encode(array("message", $file_hkid_error));
				
				JFactory::getApplication()->close(); // or jexit();
			}

		}

		if(isset($_FILES['file_hkid_3'])){
			$ext = strtolower(strrchr($_FILES['file_hkid_3']['name'],'.'));
			
			if(!in_array($ext,$this->allow_ext_list)){
				$file_hkid_3_error= 'Invalid File Format';
			}

			if($file_hkid_3_error==''){
				$realfilename = "HKID_3_".$uid."_".str_replace(" ", "_", $_FILES['file_hkid_3']['name']);
				
				if(move_uploaded_file($_FILES['file_hkid_3']['tmp_name'], $uploaddir.'/'.$realfilename)){
					$data = (object) array(
						"id" => $userData,
						"quote_file_hkid_3" => 'media/com_insurediymotor/upload/'.$realfilename
					);
				
					$update = $db->updateObject("#__insure_motor_quote_master", $data, "id");
					if($update) {
						echo json_encode(array("success" => true, "file" => $realfilename, "type" => "file_hkid_3"));
					}
					else {
						echo json_encode(array("success" => false));
					}
				}
				else {
					echo json_encode(array("message" => "Failed to move file to destination", "variable" => $uploaddir.'/'.$realfilename));
					
					JFactory::getApplication()->close(); // or jexit();
				}
			}
			else {
				echo json_encode(array("message", $file_hkid_error));
				
				JFactory::getApplication()->close(); // or jexit();
			}

		}

		if(isset($_FILES['file_renewal'])){
			$ext = strtolower(strrchr($_FILES['file_renewal']['name'],'.'));
			
			if(!in_array($ext,$this->allow_ext_list)){
				$file_renewal_error= 'Invalid File Format';
			}

			if($file_renewal_error==''){
				$realfilename = "RENEWAL_NOTICE_".$uid."_".str_replace(" ", "_", $_FILES['file_renewal']['name']);
				
				if(move_uploaded_file($_FILES['file_renewal']['tmp_name'], $uploaddir.'/'.$realfilename)){
					$data = (object) array(
						"id" => $userData,
						"quote_file_renewal_notice" => 'media/com_insurediymotor/upload/'.$realfilename
					);
				
					$update = $db->updateObject("#__insure_motor_quote_master", $data, "id");
					if($update) {
						echo json_encode(array("success" => true, "file" => $realfilename, "type" => "file_renewal_notice"));
					}
					else {
						echo json_encode(array("success" => false));
					}
				}
				else {
					echo json_encode(array("message" => "Failed to move file to destination", "variable" => $uploaddir.'/'.$realfilename));
					
					JFactory::getApplication()->close(); // or jexit();
				}
			}
			else {
				echo json_encode(array("message", $file_renewal_error));
				
				JFactory::getApplication()->close(); // or jexit();
			}

		}

		if(isset($_FILES['file_ncd'])){
			$ext = strtolower(strrchr($_FILES['file_ncd']['name'],'.'));
			
			if(!in_array($ext,$this->allow_ext_list)){
				$file_ncd_error= 'Invalid File Format';
			}

			if($file_ncd_error==''){
				$realfilename = "NCD_".$uid."_".str_replace(" ", "_", $_FILES['file_ncd']['name']);
				
				if(move_uploaded_file($_FILES['file_ncd']['tmp_name'], $uploaddir.'/'.$realfilename)){
					$data = (object) array(
						"id" => $userData,
						"quote_file_ncd" => 'media/com_insurediymotor/upload/'.$realfilename
					);
				
					$update = $db->updateObject("#__insure_motor_quote_master", $data, "id");
					if($update) {
						echo json_encode(array("success" => true, "file" => $realfilename, "type" => "file_ncd"));
					}
					else {
						echo json_encode(array("success" => false));
					}
				}
				else {
					echo json_encode(array("message" => "Failed to move file to destination", "variable" => $uploaddir.'/'.$realfilename));
					
					JFactory::getApplication()->close(); // or jexit();
				}
			}
			else {
				echo json_encode(array("message", $file_ncd_error));
				
				JFactory::getApplication()->close(); // or jexit();
			}

		}
		
	}
	
	
	public function changeDb() {
		$db = JFactory::getDBO();
		// 		$query = "ALTER TABLE `insurediy_SG_live_2017`.`ulp91_insure_motor_partner_setting`
		// CHANGE COLUMN `partner_id` `partner_id` VARCHAR(1000) NOT NULL DEFAULT '' ,
		// CHANGE COLUMN `partner_trip_type` `partner_trip_type` VARCHAR(1000) NOT NULL DEFAULT '' ,
		// CHANGE COLUMN `partner_plan_damage_coverage` `partner_plan_damage_coverage` VARCHAR(1000) NOT NULL DEFAULT '' ,
		// CHANGE COLUMN `partner_plan_windscreen_cover` `partner_plan_windscreen_cover` VARCHAR(1000) NOT NULL DEFAULT '' ,
		// CHANGE COLUMN `partner_plan_windscreen_excess` `partner_plan_windscreen_excess` VARCHAR(1000) NOT NULL DEFAULT '' ,
		// CHANGE COLUMN `partner_plan_roadside_assist` `partner_plan_roadside_assist` VARCHAR(1000) NOT NULL DEFAULT '' ,
		// CHANGE COLUMN `partner_plan_evacuation_oversea` `partner_plan_evacuation_oversea` VARCHAR(1000) NOT NULL DEFAULT '' ,
		// CHANGE COLUMN `partner_plan_personal_accident_policyholder` `partner_plan_personal_accident_policyholder` VARCHAR(1000) NOT NULL DEFAULT '' ,
		// CHANGE COLUMN `partner_plan_personal_accident_authorized` `partner_plan_personal_accident_authorized` VARCHAR(1000) NOT NULL DEFAULT '' ,
		// CHANGE COLUMN `partner_plan_medic_expense_policyholder` `partner_plan_medic_expense_policyholder` VARCHAR(1000) NOT NULL DEFAULT '' ,
		// CHANGE COLUMN `partner_plan_medic_expense_authorized` `partner_plan_medic_expense_authorized` VARCHAR(1000) NOT NULL DEFAULT '' ,
		// CHANGE COLUMN `partner_plan_third_party_liability_death` `partner_plan_third_party_liability_death` VARCHAR(1000) NOT NULL DEFAULT '' ,
		// CHANGE COLUMN `partner_plan_third_party_liability_property` `partner_plan_third_party_liability_property` VARCHAR(1000) NOT NULL DEFAULT '' ,
		// CHANGE COLUMN `partner_plan_third_party_liability_legal` `partner_plan_third_party_liability_legal` VARCHAR(1000) NOT NULL DEFAULT '' ,
		// CHANGE COLUMN `partner_plan_other_benefit` `partner_plan_other_benefit` VARCHAR(1000) NOT NULL DEFAULT '' ,
		// CHANGE COLUMN `partner_plan_ncd_protector` `partner_plan_ncd_protector` VARCHAR(1000) NOT NULL DEFAULT '' ,
		// CHANGE COLUMN `partner_plan_loss_of_use_benefit` `partner_plan_loss_of_use_benefit` VARCHAR(1000) NOT NULL DEFAULT '' ,
		// CHANGE COLUMN `partner_plan_workshop` `partner_plan_workshop` VARCHAR(1000) NOT NULL DEFAULT '' ,
		// CHANGE COLUMN `partner_plan_geographical_area` `partner_plan_geographical_area` VARCHAR(1000) NOT NULL DEFAULT '' ;
		// ";
		// 		$query = "UPDATE `insurediy_SG_live_2017`.`ulp91_insure_motor_loan_company` SET `mortgagee_name`='Volkswagen Financial Services S\'pore Ltd' WHERE `mortgagee_code`='10071450';";
		// 		$db->setQuery($query);
		
		$query = "ALTER TABLE `insurediy_SG_live_2017`.`ulp91_insure_motor_quote_master`
ADD COLUMN `quote_promo_code_discount` VARCHAR(255) NOT NULL AFTER `quote_promo_code`;";
		
		$db->setQuery($query);
		
		$result = $db->execute();
		echo $result;
	}
	
	public function populatePartnerSetting() {
		$db = JFactory::getDBO();
		
		$sompoData = (object) array(
				"id" => "1",
				// 				"partner_plan_roadside_assist" => "24/7 Emergency Mobile Accident Response Service for advice, photo taking, accident statement taking, e-filing of claims, workshop referral and towing arrangement.",
		// 				"partner_plan_evacuation_oversea" => "Not covered",
		// 				"partner_plan_personal_accident_policyholder" => "Up to $20,000",
		// 				"partner_plan_personal_accident_authorized" => "Up to $10,000",
		// 				"partner_plan_other_benefit" => "Waiver of Standard Excess up to $1,000 if accident repair is done at ExcelDrive Workshops for the first claim per policy year - applicable to ExcelDrive Prestige and Gold Plans only;<br>For ExcelDrive Focus Plan, Standard Excess is halved up to max. of $600.<br>(Note: voluntary excess will not be wavied for repairs at ExcelDrive Workshops.) ",
		// 				"partner_plan_loss_of_use_benefit" => "$100 per day of approved repair period, up to 10 days (applicable to ExcelDrive Prestige and Gold Plans only)",
		// 				"partner_plan_geographical_area" => "Includes: (1) Singapore, (2) West Malaysia and (3) that part of Thailand within 80.5km of the border between Thailand and West Malaysia."
				"partner_plan_ncd_protector" => "Included at no charge for NCD 30% and above for ExcelDrive Prestige and ExcelDrive Gold Plans."
		);
		
		$check = $db->updateObject("#__insure_motor_partner_setting", $sompoData, "id");
		
		$msigData = (object) array(
				"id" => "2",
				// 				"partner_plan_roadside_assist" => "24/7 Automobile and Medical Assistance Services for towing, roadside assistance, vehicle repatriation, accident & police report notification, locksmith referral, etc.",
		// 				"partner_plan_evacuation_oversea" => "Up to $50,000 per accident and in aggregate for each Period of Insurance, Emergency Medical Evacuation and Repatriation of the Policyholder for accidents within the Geographical area (excl. Singapore) in connection with the Insured Vehicle.",
		// 				"partner_plan_personal_accident_policyholder" => "Up to $20,000 (MotorMax);<br>Up to $100,000 (MotorMax Plus)",
		// 				"partner_plan_personal_accident_authorized" => "Up tp $10,000 Each (MotorMax);<br>Up to $50,000 Each (MotorMax Plus)",
		// 				"partner_plan_other_benefit" => "Car insured under the MotorMax Plus Plan enjoys New for Old Replacement of your car (less than 12 months old) in the event of an accident,<br>and up to $100,000 of the outstanding loan of your car in the event of accidental death of the car owner. (T&Cs apply)",
		// 				"partner_plan_loss_of_use_benefit" => "$50 per day, up to 10 days (applicable to MotorMax Plus Plan only)",
		// 				"partner_plan_geographical_area" => "Includes: (1) Singapore, (2) West Malaysia and (3) that part of Thailand within 80 kilometres of the border between Thailand and West Malaysia."
				"partner_plan_ncd_protector" => "Optional at additional premium<br>This is included in the above quote if you have selected to add NCD Protector Benefit and your NCD is at 50%."
		);
		
		$check = $db->updateObject("#__insure_motor_partner_setting", $msigData, "id");
		
		$hlaData = (object) array(
				"id" => "3",
				// 				"partner_plan_roadside_assist" => "24/7 Automobile and Medical Assistance Services for towing, roadside repair, claims procedure / reporting, workshop referral, service centre referral in Malaysia, car replacement / rental arrangement, hotel accomodation arrangement and emergency message transmission assistance.",
		// 				"partner_plan_evacuation_oversea" => "Up to $50,000  per accident and in the aggregate for each Period of Insurance, Emergency Medical Evacuation / Repatriation of Policyholder / Authorised Driver and/or passenger(s) for accidents within the Geographical area (excl. Singapore) in connection with the Insured Vehicle.",
		// 				"partner_plan_personal_accident_policyholder" => "Up to $20,000",
		// 				"partner_plan_personal_accident_authorized" => "Up to $10,000",
		// 				"partner_plan_other_benefit" => "For accident repairs at any HL Assurance Approved workshops, Own Damage Excess is halved and be assured that genuine manufacturers� parts will be used that comes with a full 9-month repair warranty.",
		// 				"partner_plan_loss_of_use_benefit" => "Daily transport allowance of $50 (up to ten days if repairs exceed three days and subject to max of $1,000 per policy year)",
		// 				"partner_plan_geographical_area" => "Includes: (1) Singapore, (2) West Malaysia, (3) Transit by direct sea route across the straits between Penang and the mainland of West Malaysia, Direct sea route across the straits between Changi Point, Singapore and Tanjong Belungkor, Johor and (4) Part of Thailand within 80 kilometres of the border between Southern Thailand and West Malaysia subject to a maximum duration of 7 days for each and every trip."
				"partner_plan_ncd_protector" => "Optional at additional premium<br>This is included in the above quote if you have selected to add NCD Protector Benefit and your NCD is at 50%."
		);
		
		$check = $db->updateObject("#__insure_motor_partner_setting", $hlaData, "id");
		
		if($check) {
			echo "OK";
		}
	}
	
	
	
	//Test Section
	
	public function testGetToken() {
		//$model = $this->getModel();
		
		//$result = $model->testGetToken();
		
		//echo json_encode($result);
		JFactory::getApplication()->close();
	}
	
	public function testGetQuote() {
		$model = $this->getModel();
		
		//$result1 = $model->testGetQuotations();
		//$result2 = $model->testGetProposals($result1["response"]->serviceReplyObject->CURRENTPOLICY->APPQUOTEPOLICYREFID);
		//$result3 = $model->testCreatePolicy($result1["response"]->serviceReplyObject->CURRENTPOLICY->APPQUOTEPOLICYREFID, $result2["response"]->serviceReplyObject->CURRENTPOLICY);
		//echo json_encode($result3);
		
		JFactory::getApplication()->close();
	}
	
	public static function getMSIGAccessToken() {
		$username = "idapiuat";
		$password = "p1a88VVPbeZMoDvwTQAiexrR33zn9Wyru5pSLPx5n0E9GL1eaDuH3NK3ztE2";
		$credential = array(
				"grant_type" => "client_credentials"
		);
		$ch = curl_init();
		
		curl_setopt($ch, CURLOPT_URL, "https://61.8.193.183/api/oauth/token");
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded', 'grant_type: '));
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($credential));
		
		$server_output = curl_exec ($ch);
		
		curl_close($ch);
		
		print_r($server_output);
		
		JFactory::getApplication()->close();
		//return json_decode($server_output);
	}
	
	public static function testSompoEncryption() {
		print_r(InsureDIYMotorHelper::sompoEncryptData("waihong"));
	}
	
	public static function testMsigValidation() {
		print_r(InsureDIYMotorHelper::validateMsigQuotation("test"));
	}
	
	public function testGetProposal() {
		$model = $this->getModel();
		
		$result = $model->testGetProposals();
		
		echo json_encode($result);
		JFactory::getApplication()->close();
	}
	
	public function testCreatePolicy() {
		$model = $this->getModel();
		
		$result = $model->testCreatePolicy();
		
		echo json_encode($result);
		JFactory::getApplication()->close();
	}
	
	public function testMsigQuote(){
		$result = InsureDIYMotorHelper::getMSIGAccessToken();
		
		//$accessToken = $this->getMSIGAccessToken();
		print_r($result) ;
		// 		if(!$accessToken) {
		// 			echo "failed token";
		// 		}
		
		// 		$requestObj = (object) array(
		// 				"type" => "NB",
		// 				"startDate" => "2018-02-24",
		// 				"endDate" => "2019-02-23",
		// 				"quoteReqDate" => "2018-02-02",
		// 				"referenceCode" => "IDAPI",
		// 				"coverage" => "C",
		// 				"promoCode" => "",
		// 				"planCode" => "",
		// 				"driver" => (object) array(
		// 					"dateOfBirth" => "1980-06-27",
		// 					"gender" => "F",
		// 					"maritalStatus" => "M",
		// 					"mobileNo" => "87815037",
		// 					"email" => "jone@mail1.com",
		// 					"identificationType" => "NRIC",
		// 					"identificationNo" => "S2172055J",
		// 					"drivingExperience" => "3",
		// 					"offenceFree" => "N",
		// 					"occupationType" => "OUTD",
		// 					"noOfClaimsInLast3Years" => "1",
		// 					"claimAmountInLast3Years" => "1000.23",
		// 					"currencyCode" => "SGD"
		// 				),
		// 				"vehicle" => (object) array(
		// 					"offPeakCar" => "N",
		// 					"yearOriginalReg" => "2017",
		// 					"makeModelCode" => "ALF1",
		// 					"registrationNum" => "",
		// 					"seatNo" => "5",
		// 					"ncd" => "50",
		// 					"ncdProtectionIndicator" => "Y"
		// 				)
		// 		);
		
		// 		$ch = curl_init();
		// 		$ua = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13';
		
		// 		curl_setopt($ch, CURLOPT_URL, "https://61.8.193.183/api/motor/v1/products/QMX/quotes");
		// 		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// 		curl_setopt($ch, CURLOPT_POST, 1);
		// 		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		// 		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		// 				'Content-Type: application/json',
		// 				'Accept: application/json',
		// 				'Data-type: application/json',
		// 				'Authorization: bearer '.$accessToken->access_token
		// 		)
		// 				);
		// 		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestObj));
		
		// 		$curl_result = curl_exec ($ch);
		// 		$server_output = json_decode($curl_result);
		// 		$error = curl_error($ch);
		
		// 		curl_close($ch);
		
		// 		$result= array();
		
		// 		if($server_output) {
		// 			if(isset($server_output->quoteReference)) {
		
		// 				$planList = $server_output->premium;
		// 				$quoteRefId = $server_output->quoteReference;
		
		// 				$result["success"] = true;
		// 				$result["quote_ref_id"] = $quoteRefId;
		// 				$result["plan_list"] = $planList;
		// 				$result["request_id"] = $requestObj->requestUniqueId;
		// 			}
		
		// 			if(isset($server_output->errorCode)) {
		// 				$error = "";
		// 				foreach($server_output->fieldErrors as $errorItem) {
		// 					$error.= " [".$errorItem->fieldCode ."]-". $errorItem->errorDesc ."<br>";
		// 				}
		
		// 				$result["success"] = false;
		
		// 				$result["error_message"] = $error;
		// 			}
		// 		}
		// 		else {
		// 			echo "failed";
		// 		}
		
		// 		echo json_encode($result);
		// 		echo "<br><Br><------------------------><br><br>";
		
		// 		$requestObj = (object) array(
		// 				"type" => "NB",
		// 				"startDate" => "2018-02-24",
		// 				"endDate" => "2019-02-23",
		// 				"quoteReference" => $result["quote_ref_id"],
		// 				"referenceCode" => "IDAPI",
		// 				"coverage" => "C",
		// 				"promoCode" => "",
		// 				"planCode" => "1",
		// 				"finalPremium" => "1269.95",
		// 				"currencyCode" => "SGD",
		// 				"driver" => (object) array(
		// 						"lastName" => "Doe",
		// 						"firstName" => "Jane",
		// 						"nationality" => "SG",
		// 						"race" => "MAL",
		// 						"hasCurrentMotorPolicy" => "Y",
		// 						"previousInsurer" => "AXA",
		// 						"clientConsent" => "Y",
		// 						"address" => (object) array(
		// 							"streetName" => "Alexandra Road",
		// 							"postalCode" => "159942",
		// 							"blockNo" => "305",
		// 							"unitNo" => "#05-05",
		// 							"buildingName" => "Optus",
		// 							"countryOfResidence" => "SG"
		// 						),
		// 						"dateOfBirth" => "1980-06-27",
		// 						"gender" => "F",
		// 						"maritalStatus" => "M",
		// 						"mobileNo" => "87815037",
		// 						"email" => "jone@mail1.com",
		// 						"identificationType" => "NRIC",
		// 						"identificationNo" => "S2172055J",
		// 						"drivingExperience" => "3",
		// 						"offenceFree" => "N",
		// 						"occupationType" => "OUTD",
		// 						"noOfClaimsInLast3Years" => "1",
		// 						"claimAmountInLast3Years" => "1000.23",
		// 						"currencyCode" => "SGD"
		// 				),
		// 				"vehicle" => (object) array(
		// 						"engineNo" => "12712781181",
		// 						"chassisNo" => "781278127",
		// 						"mortgageeCode" => "O",
		// 						"mortgageeName" => "New Bank in Singapore",
		// 						"offPeakCar" => "N",
		// 						"yearOriginalReg" => "2017",
		// 						"makeModelCode" => "ALF1",
		// 						"registrationNum" => "SKE6395P",
		// 						"seatNo" => "5",
		// 						"ncd" => "50",
		// 						"ncdProtectionIndicator" => "Y"
		// 				)
		// 		);
		
		// 		$ch = curl_init();
		// 		$ua = 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US) AppleWebKit/525.13 (KHTML, like Gecko) Chrome/0.A.B.C Safari/525.13';
		
		// 		curl_setopt($ch, CURLOPT_URL, "https://61.8.193.183/api/motor/v1/products/QMX/applications");
		// 		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		// 		curl_setopt($ch, CURLOPT_POST, 1);
		// 		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		// 		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
		// 				'Content-Type: application/json',
		// 				'Accept: application/json',
		// 				'Data-type: application/json',
		// 				'Authorization: bearer '.$accessToken->access_token
		// 		)
		// 				);
		// 		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($requestObj));
		
		// 		$curl_result = curl_exec ($ch);
		// 		$server_output = json_decode($curl_result);
		// 		$error = curl_error($ch);
		
		// 		curl_close($ch);
		
		// 		$result= array();
		
		// 		if($server_output) {
		// 			if(isset($server_output->quoteReference)) {
		// 				$planList = $server_output->premium;
		// 				$quoteRefId = $server_output->quoteReference;
		
		// 				$result["success"] = true;
		// 				$result["quote_ref_id"] = $quotePolicyRefId;
		// 				$result["plan_list"] = $planList;
		// 				$result["request_id"] = $requestObj->requestUniqueId;
		// 			}
		
		// 			if(isset($server_output->errorCode)) {
		// 				$error = "";
		// 				foreach($server_output->fieldErrors as $errorItem) {
		// 					$error.= " [".$errorItem->fieldCode ."]-". $errorItem->errorDesc ."<br>";
		// 				}
		
		// 				$result["success"] = false;
		
		// 				$result["error_message"] = $error;
		// 			}
		// 		}
		// 		else {
		// 			echo "application failed";
		// 		}
		// 		echo json_encode($result);
		
		JFactory::getApplication()->close();
	}
}
?>