<?php

defined('_JEXEC') or die;

class InsureDIYMotorController extends JControllerLegacy {

	protected $default_view = 'form';

	public function display($cachable = false, $urlparams = Array()) {
		$this->input->set('view', $this->default_view);
		return parent::display(false);
	}

}
