<?php

defined('_JEXEC') or die;

class ProductControllerItem extends JControllerForm {

	protected $_error_url = "index.php?option=com_product";
	protected $_product_url = "index.php?option=com_product&view=item&id=";

	public function getModel($name = 'Item', $prefix = 'ProductModel', $config = array('ignore_request' => true)) {
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}

	public function redeem() {
		JSession::checkToken('post') or jexit(JText::_('JInvalid_Token'));
		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		$model = $this->getModel();

		$id = $app->input->post->get("id", 0, "integer");
		if (!$id) {
			$app->redirect($this->_error_url, ERR_WHAT_ARE_YOU_TRYING_TO_DO);
		}

		$product = $model->getItem($id);
		if (!$product) {
			$app->redirect($this->_error_url, JText::_("COM_PRODUCT_ERR_REDEEM_PRODUCT_NOT_FOUND"));
		}

		if ($user->get("guest")) {
			$return = "index.php?option=com_product&view=item&id=" . $id;
			$app->redirect("index.php?option=com_users&view=login&return=" . base64_encode($return), JText::_("COM_PRODUCT_ERR_NEED_TO_LOGIN"));
		}

		$points = InsureDIYHelper::getUserPoints($user->id);
		if ($points < $product->points) {
			$app->redirect($this->_product_url . $id, JText::_("COM_PRODUCT_ERR_NOT_ENOUGH_POINTS"));
		}

		// All checking passed
		// create point record first
		$pointRecord = array();
		$pointRecord['user_id'] = $user->id;
		$pointRecord['type'] = 0;
		$pointRecord['points'] = -$product->points;
		$pointRecord['description'] = "Product Redeem";
		$pointRecord['status'] = "A";
		$point_id = $model->addPointRecord($pointRecord);

		// create product history
		$pointHistory = array();
		$pointHistory['user_id'] = $user->id;
		$pointHistory['product_id'] = $product->id;
		$pointHistory['point_id'] = $point_id;
		$pointHistory['date'] = $point_id;
		$order_ref = $model->addPointHistory($pointHistory);

		// prepare stuffs for mail
		$config = JFactory::getConfig();
		$from = $config->get("mailfrom", "admin@globalsecasia.com");
		$fromName = $config->get("fromname", "");
		$productDesc = $product->name . " (" . $product->description . ")";
		$subject = "Product redemption: " . $productDesc;

		$mailer = JFactory::getMailer();
		$param = JComponentHelper::getParams("com_product");

		// prepare mail data
		$mailData = array();
		$mailData['order_ref'] = $order_ref;
		$mailData['date'] = JHtml::date('now', "d F Y H:i:s");
		$mailData['first_name'] = $user->name;
		$mailData['last_name'] = $user->lastname;
		$mailData['product'] = $productDesc;
		$mailData['points'] = $product->points;
		$mailData['user_email'] = $user->email;

		// send emails
		$mailer->ClearAllRecipients();
		$userEmail = $user->email;
		$userEmailFormat = $param->get("email_format", "");
		$userEmailBody = InsureDIYHelper::replaceVariables($userEmailFormat, $mailData);

//		$userEmailBody = sprintf(JText::_("COM_PRODUCT_REDEEM_EMAIL_BODY_USER"), $productDesc, $product->points);
		$mailer->sendMail($from, $fromName, $userEmail, $subject, $userEmailBody, TRUE);

		$mailer->ClearAllRecipients();
		$adminEmail = $param->get("admin_email", "zanhlahtet@outlook.com");
		$adminEmailFormat = $param->get("email_format_admin", "");
		$adminEmailBody = InsureDIYHelper::replaceVariables($adminEmailFormat, $mailData);
//		$adminEmailBody = sprintf(JText::_("COM_PRODUCT_REDEEM_EMAIL_BODY_ADMIN"), $productDesc, $product->points);
//		$adminEmailBody = "Dear Admin,<br/>A user have successfully purchased " . $productDesc . " by using " . $product->points . ".<br>This is sample.";

		$mailer->sendMail($from, $fromName, $adminEmail, $subject, $adminEmailBody, TRUE);

		$mailer->ClearAllRecipients();
//		$messageToUser = $param->get("message", "");
		$bpEmail = $product->bp_email;
		$bpEmailFormat = $param->get("email_format_bp", "");
		$bpEmailBody = InsureDIYHelper::replaceVariables($bpEmailFormat, $mailData);
//		$bpEmailBody = sprintf(JText::_("COM_PRODUCT_REDEEM_EMAIL_BODY"), $date->format("d F Y H:i:s"), $productDesc, $order_ref, $user->email, $user->name, $user->lastname, $messageToUser);
		$mailer->sendMail($from, $fromName, $bpEmail, $subject, $bpEmailBody, TRUE);

		$app->redirect($this->_product_url . $id, JText::_("COM_PRODUCT_MSG_SUCESSFULLY_REDEEMED"));
	}

}
