<?php

defined('_JEXEC') or die;

class ProductViewList extends JViewLegacy {

	protected $state;

	public function display($tpl = null) {
		$model = $this->getModel();
		$this->items = $model->getItems();
		$this->state = $model->getState();
		$this->pagination = $model->getPagination();
//		$model2 = $this->getModel("Search");
//		$this->form = $model2->getForm();
//		$this->data = $model2->getData();
		$params = JComponentHelper::getParams("com_product");
		$this->page_desc = $params->get("page_desc", "");

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		parent::display($tpl);
	}

}
