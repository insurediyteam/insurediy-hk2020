<?php defined('_JEXEC') or die; ?>
<?php
$urls = MyUri::getUrls();
$items = $this->items;
?>

<div class="item-page faq com_product list" style="background-color:#f2f2f2;">
	<div class="page-header ">
		<h1>
			<i class="icon-get"></i><?php echo JText::_("COM_PRODUCT_LIST_HEADER"); ?>
		</h1>
		<div class="clear"></div>
	</div>
	<div class="spacer-2"></div>
	<div style="margin:0 20px;"><?php echo $this->page_desc; ?></div>
	<div class="product-container border-box clearfix">
		<?php
		foreach ($items as $item):
			$itemUrl = "index.php?option=com_product&view=item&id=" . $item->id;
			?>
			<div class="product-wrapper border-box">
				<div class="product-image">
					<?php if (strlen($item->image) > 0): ?>
						<a href="<?php echo $itemUrl; ?>" >
							<img src="<?php echo $item->image; ?>" />
						</a>
					<?php endif; ?>
				</div>
				<div class="product-text">
					<div class="spacer-1"></div>
					<a href="<?php echo $itemUrl; ?>"><span class="product-name"><?php echo $item->name; ?></span></a><br>
					<span class="product-description"><?php echo $item->description; ?></span>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</div>
<?php if (FALSE): ?>
	<?php if (isset($this->pagination) && count($this->pagination->getPagesLinks())): ?>
		<div class="pagination pagination-toolbar">
			<?php echo $this->pagination->getPagesLinks(); ?>
			<div class="clr"></div>
		</div>
	<?php endif; ?>
<?php endif; ?>


