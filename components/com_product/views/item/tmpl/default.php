<?php defined('_JEXEC') or die; ?>
<?php
$urls = MyUri::getUrls();
$item = $this->item;
?>
<div class="item-page faq com_product item" style="background-color:#f2f2f2;">
	<div class="page-header ">
		<h1>
			<i class="icon-get"></i><?php echo JText::_("COM_PRODUCT_LIST_HEADER"); ?>
		</h1>
		<div class="clear"></div>
	</div>
	<div class="padding10">
		<div class="clearfix">
			<div class="cpan2 padding10 border-box">
				<div class="product-info">
					<div class="product-name-desc border-box">
						<span class="product-name blue-title-text"><?php echo $item->name; ?></span><br>
						<span class="product-description"><?php echo $item->description; ?></span>
					</div>
					<div class="product-address clearfix border-box" style="position: relative;">
						<div class="cpan45">
							<span class="product-address-text"><?php echo $item->address; ?></span>
						</div>
						<div class="cpan4" style="bottom: 20px; position: absolute; left: 100%;margin-left: -126px;">
							<img src="images/DIY_Rewards.png" alt="Rewards">
						</div>
					</div>
				</div>
				<div class="spacer-1"></div>
				<div class="spacer-2"></div>
				<div class="product-about">
					<span class="blue-title-text product-about-title"><?php echo JText::_("COM_PRODUCT_ITEM_ABOUT"); ?></span><br><br>
					<?php echo $item->about; ?>
				</div>
			</div>
			<div class="cpan2 padding10 border-box">
				<div class="product-image">
					<img src="<?php echo $item->image; ?>" />
				</div>
				<div class="product-redeem clearfix">
					<div class="product-redeem-text blue-title-text">
						<?php echo JText::_("COM_PRODUCT_ITEM_POINTS_NEEDED_TO_REDEEM"); ?>
					</div>
					<div class="big-slash" style="float: left;"></div>
					<div class="product-redeem-points ">
						<span class="blue-title-text" style="font-size: 36px;"><?php echo $item->points; ?></span>
					</div>
					<div>
						<form action="index.php" method="post">
							<input type="hidden" name="option" value="com_product"/>
							<input type="hidden" name="task" value="item.redeem"/>
							<input type="hidden" name="id" value="<?php echo $item->id; ?>" />
							<?php echo JHtml::_('form.token'); ?>
							<button onclick="javascript:jQuery('#confirmation_popup').modal('show');" type="button" class="btn btn-primary" style="height:34px;padding:5px 20px;font-size:14px !important;"><?php echo JText::_("BTN_REDEEM_NOW"); ?></button>
							<div id="confirmation_popup" class="modal hide fade custom-modal">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
									<div class="modal-title">
										<h3><?php echo JText::_("COM_PRODUCT_MSG_CONFIRM"); ?></h3>
									</div>
								</div>
								<div class="modal-body custom-modal-body-1">
									<?php echo sprintf(JText::_("COM_PRODUCT_MSG_CONFIRM_TEXT"), $this->item->name . " (" . $this->item->description . ")", JFactory::getUser()->email); ?>
									<div class="spacer-2"></div>
									<button type="button" class="btn btn-primary" data-dismiss="modal" style="height:34px;padding:5px 20px;font-size:14px !important;"><?php echo JText::_("JNO"); ?></button>
									<button type="submit" class="btn btn-primary validate" style="height:34px;padding:5px 20px;font-size:14px !important;"><?php echo JText::_("JYES"); ?></button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

