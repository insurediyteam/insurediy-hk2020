<?php

defined('_JEXEC') or die;

jimport('joomla.application.component.modelform');
jimport('joomla.event.dispatcher');

class ProductModelSearch extends JModelForm {

	protected $data;

	public function __construct($config = array()) {
		parent::__construct($config);
	}

	public function getData() {
		if ($this->data === null) {
			$this->data = array();
			$fdata = JRequest::getVar('jform', array(), 'post', 'array');
			if (!isset($fdata['reset'])) {
				$this->data = $fdata;
			}
		}
		return $this->data;
	}

	public function getForm($data = array(), $loadData = true) {
		$form = $this->loadForm('search', 'search', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}
		return $form;
	}

	protected function loadFormData() {
		return $this->getData();
	}

	protected function preprocessForm(JForm $form, $data, $group = 'standard') {
		parent::preprocessForm($form, $data, $group);
	}

}
