<?php

defined('_JEXEC') or die;

jimport('joomla.application.component.modellist');

class ProductModelList extends JModelList {

	protected $_items;
	protected $_query;
	protected $_pagination = null;
	protected $_tb_product = "#__products";

	public function __construct($config = array()) {
		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 */
	protected function populateState($ordering = null, $direction = null) {
		$app = JFactory::getApplication('site');
		$maxRow = 99;
		$this->setState('list.links', 0);
		$this->setState('list.limit', $maxRow);

		$offset = JRequest::getUInt('limitstart', 0);
		if ($offset < 1) {
			$offset = 0;
		}
		$this->setState('list.start', $offset);

		// Load the parameters
		$params = $app->getParams();
		$this->setState('params', $params);
	}

	public function getTotal() {
		if (!isset($this->_total)) {
			$db = $this->getDbo();
			$db->setQuery($this->getQuery(TRUE));
			$results = $db->loadAssocList();
			$this->_total = count($results);
		}
		return $this->_total;
	}

	public function getItems($offset = 0, $limit = 100) {// no pagination design so
		if ($this->getTotal() < 0) {
			return array();
		}
		$db = $this->getDbo();
		$query = $this->getQuery(TRUE);

		$offset = (is_null($offset)) ? $this->getState('list.start') : $offset;
		$limit = (is_null($limit)) ? $this->getState('list.limit') : $limit;

		$db->setQuery($query, $offset, $limit);
		return $db->loadObjectList();
	}

	public function getQuery($new = TRUE) {
		$db = $this->getDbo();
		$query = $db->getQuery($new)
				->select("*")
				->from($this->_tb_product . " AS p")
				->where("p.publish=1 AND p.trash=0");
		$query->order("p.ordering ASC");
		return $query;
	}

	public function getPagination() {
		jimport("joomla.html.pagination");
		$count = $this->getTotal();

		$limitstart = $this->getState("list.start", 0);
		$limit = $this->getState("list.limit", 0);

		$pagination = new JPagination($count, $limitstart, $limit);
		return $pagination;
	}

}
