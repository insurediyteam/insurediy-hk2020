<?php

defined('_JEXEC') or die;

class ProductModelItem extends JModelItem {

	protected $_item;
	protected $_tb_product = "#__products";

	public function __construct($config = array()) {
		parent::__construct($config);
	}

	public function getItem($id) {
		$db = $this->getDbo();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from($this->_tb_product . " AS p")
				->where("p.publish=1 AND p.trash=0 AND p.id=" . $db->quote($id));
		$db->setQuery($query);
		return $db->loadObject();
	}

	public function addPointHistory($data = array()) {
		$table = JTable::getInstance("ProductHistory", "InsureTable");
		$data = (is_object($data)) ? $data : MyHelper::array2jObject($data);
		$table->bind($data);
		$table->store();
		return $table->id;
	}

	public function addPointRecord($data = array()) {
		$table = JTable::getInstance("Point", "InsureTable");
		$data = (is_object($data)) ? $data : MyHelper::array2jObject($data);
		$table->bind($data);
		$table->store();
		return $table->point_id;
	}

}
