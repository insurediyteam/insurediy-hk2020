<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_calculator
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.modal', 'a.modal_jform_contenthistory');

// Create shortcut to parameters.
$params = $this->state->get('params');
?>

<div class="item-page faq">
	<div class="page-header ">
		<h1>
			<i class="icon-calculator"></i><?php echo JText::_("COM_CALCULATOR_CI_HEADER"); ?></h1>
		<div class="clear"></div>
	</div>
	<form action="<?php echo JRoute::_('index.php'); ?>" method="post" name="ciCalculator" id="ciCalculator" class="c-form form-validate form-vertical">
		<div class="padding1520 clearfix">
			<div style="width:22%;float:left;"><?php echo MyHelper::load_module(114); ?></div>
			<div style="width: 78%; float: left; padding: 20px;" class="border-box">
				<div class="clearfix">
					<div class="c-form-number">1.</div>
					<div class="c-form-label c-form-label-bold"><?php echo $this->form->getLabel("annual_salary"); ?></div>
					<div class="c-form-input border-box clearfix"><div style="width: 20%;float: left;line-height: 28px;"><?php echo InsureDIYHelper::getCurrency(); ?></div><div style="width: 78%;float: left;"><?php echo $this->form->getInput("annual_salary"); ?></div></div>
				</div>
				<div class="spacer-1"></div>
				<div class="clearfix">
					<div class="c-form-number">2.</div>
					<div class="c-form-label c-form-label-bold"><?php echo $this->form->getLabel("years"); ?></div>
					<div class="c-form-input border-box clearfix"><div style="width: 20%;float: left;line-height: 28px;">&nbsp;</div><div style="width: 78%;float: left;"><?php echo $this->form->getInput("years"); ?></div></div>
				</div>
				<div class="spacer-1"></div>
				<div class="clearfix">
					<div class="c-form-number">3.</div>
					<div class="c-form-label c-form-label-bold"><?php echo $this->form->getLabel("medical_cost"); ?></div>
					<div class="c-form-input border-box clearfix"><div style="width: 20%;float: left;line-height: 28px;"><?php echo InsureDIYHelper::getCurrency(); ?></div><div style="width: 78%;float: left;"><?php echo $this->form->getInput("medical_cost"); ?></div></div>

				</div>
				<div class="spacer-1"></div>
				<div class="clearfix">
					<div class="c-form-number">4.</div>
					<div class="c-form-label c-form-label-bold"><?php echo $this->form->getLabel("existing_ci"); ?></div>
					<div class="c-form-input border-box clearfix"><div style="width: 20%;float: left;line-height: 28px;"><?php echo InsureDIYHelper::getCurrency(); ?></div><div style="width: 78%;float: left;"><?php echo $this->form->getInput("existing_ci"); ?></div></div>
				</div>
				<div class="spacer-2"></div>
				<div class="clearfix">
					<div class="c-form-label c-form-label-bold"><?php echo $this->form->getLabel("total_ci"); ?></div>
					<div class="c-form-number">&nbsp;</div>
					<div class="c-form-input border-box clearfix"><div style="width: 20%;float: left;line-height: 28px;"><?php echo InsureDIYHelper::getCurrency(); ?></div><div style="width: 78%;float: left;"><?php echo $this->form->getInput("total_ci"); ?></div></div>
				</div>
				<div class="clearfix">
					<div class="c-form-label c-form-label-bold"><?php echo $this->form->getLabel("monthly_premium"); ?></div>
					<div class="c-form-number">&nbsp;</div>
					<div class="c-form-input border-box clearfix"><div style="width: 20%;float: left;line-height: 28px;"><?php echo InsureDIYHelper::getCurrency(); ?></div><div style="width: 78%;float: left;"><?php echo $this->form->getInput("monthly_premium"); ?></div></div>
				</div>
				<div class="spacer-2"></div>
				<div class="clearfix">
					<?php if (FALSE): ?>
						<a class="pull-right" href="<?php echo JRoute::_("index.php?Itemid=157") ?>" style="margin-left: 20px; margin-top: 2.5px;"><img src="images/icon-cart-buy.png" alt=""/></a>
					<?php endif; ?>
					<input type="submit" class="btn btn-primary pull-right" name="Save" value="<?php echo JText::_("COM_CALCULATOR_SAVE_AND_BUY"); ?>" style="padding: 5px 20px;"/>
				</div>
				<div class="spacer-2"></div>
				<div class="clearfix">
					<p>
						<strong><?php echo JText::_("COM_CALCULATOR_DISCLAIMER_TITLE"); ?></strong>
						<br />
						<?php echo JText::_("COM_CALCULATOR_DISCLAIMER_TEXT"); ?>
					</p>
				</div>
			</div>
		</div>

		<input type="hidden" name="return" value="<?php echo $this->return_page; ?>" />
		<input type="hidden" name="option" value="com_calculator" />
		<input type="hidden" name="task" value="ci.save" />
		<?php echo JHtml::_('form.token'); ?>
	</form>
</div>

<script type="text/javascript">

//	function numberFormat(number) {
//		number = parseFloat(number.replace(/,/g, ''));
//		return number.formatMoney(0, '.', ',');
//	}
//
//	Number.prototype.formatMoney = function (c, d, t) {
//		var n = this;
//		c = isNaN(c = Math.abs(c)) ? 2 : c;
//		d = d === undefined ? "." : d;
//		t = t === undefined ? "," : t;
//		var s = n < 0 ? "-" : "";
//		var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "";
//		var j = (j = i.length) > 3 ? j % 3 : 0;
//		return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
//	}



	jQuery(document).ready(function () {
		var annualSalary, years, medicalCost, existingCi;

		annualSalary = jQuery("#jform_annual_salary");
		years = jQuery("#jform_years");
		medicalCost = jQuery("#jform_medical_cost");
		existingCi = jQuery("#jform_existing_ci");

		annualSalary.on("change keyup", function () {

			var yearsVal = years.find("option:selected").val();
			updateCi(parseFloat(annualSalary.val().replace(/,/g, '')), parseFloat(yearsVal.replace(/,/g, '')), parseFloat(medicalCost.val().replace(/,/g, '')), parseFloat(existingCi.val().replace(/,/g, '')));
			var jThis = $jq(this);
			var val = jThis.val();
			if (val.length > 0) {
				jThis.val(numberFormat(val));
			}
		});

		years.on("change", function () {
			var yearsVal = years.find("option:selected").val();
			updateCi(parseFloat(annualSalary.val().replace(/,/g, '')), parseFloat(yearsVal.replace(/,/g, '')), parseFloat(medicalCost.val().replace(/,/g, '')), parseFloat(existingCi.val().replace(/,/g, '')));
		});

		medicalCost.on("change keyup", function () {

			var yearsVal = years.find("option:selected").val();
			updateCi(parseFloat(annualSalary.val().replace(/,/g, '')), parseFloat(yearsVal.replace(/,/g, '')), parseFloat(medicalCost.val().replace(/,/g, '')), parseFloat(existingCi.val().replace(/,/g, '')));
			var jThis = $jq(this);
			var val = jThis.val();
			if (val.length > 0) {
				jThis.val(numberFormat(val));
			}
		});

		existingCi.on("change keyup", function () {

			var yearsVal = years.find("option:selected").val();
			updateCi(parseFloat(annualSalary.val().replace(/,/g, '')), parseFloat(yearsVal.replace(/,/g, '')), parseFloat(medicalCost.val().replace(/,/g, '')), parseFloat(existingCi.val().replace(/,/g, '')));
			var jThis = $jq(this);
			var val = jThis.val();
			if (val.length > 0) {
				jThis.val(numberFormat(val));
			}
		});

		jQuery("#ciCalculator").find("input[type='text']").each(function () {
			var jThis = $jq(this);
			var val = jThis.val();

			jThis.keydown(function (event) {
				var hasDot = val.indexOf(".");
				// Allow only backspace and delete
				if (event.keyCode == 9 || event.keyCode == 46 || event.keyCode == 8 || (event.keyCode == 190 && hasDot == -1)) {
					// let it happen, don't do anything
				}
				else {
					// Ensure that it is a number and stop the keypress
					if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105)) {
						// 0-9
					} else {
						event.preventDefault();
					}
				}
			});

		});
	});

	function numberFormat(number) {
		number = parseFloat(number.replace(/,/g, ''));
		return number.formatMoney(0, '.', ',');
	}

	Number.prototype.formatMoney = function (c, d, t) {
		var n = this;
		c = isNaN(c = Math.abs(c)) ? 2 : c;
		d = d === undefined ? "." : d;
		t = t === undefined ? "," : t;
		var s = n < 0 ? "-" : "";
		var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "";
		var j = (j = i.length) > 3 ? j % 3 : 0;
		return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	};

	function updateCi(v1, v2, v3, v4) {
		var total, totalCi, mPremium, premium;
		if (isNaN(v1)) {
			v1 = 0;
		}
		if (isNaN(v2)) {
			v2 = 0;
		}
		if (isNaN(v3)) {
			v3 = 0;
		}
		if (isNaN(v4)) {
			v4 = 0;
		}

		total = (v1 * v2) + v3 - v4;
		totalCi = jQuery("#jform_total_ci");
		mPremium = jQuery("#jform_monthly_premium");
//		premium = (total * 0.00257) / 12;
		premium = (total * 0.00041);

		premium = Math.round(premium * 100) / 100;
		totalCi.val(numberFormat(total + ""));
		mPremium.val(numberFormat(premium + ""));
	}


</script>