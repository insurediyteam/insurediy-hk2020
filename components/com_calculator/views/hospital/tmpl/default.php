<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_calculator
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('behavior.modal', 'a.modal_jform_contenthistory');

// Create shortcut to parameters.
$params = $this->state->get('params');
?>

<div class="item-page faq">
	<div class="page-header ">
		<h1>
			<i class="icon-calculator"></i><?php echo JText::_("COM_CALCULATOR_HOSPITAL_HEADER"); ?></h1>
		<div class="clear"></div>
	</div>
	<form action="<?php echo JRoute::_('index.php'); ?>" method="post" name="hospitalCalculator" id="hospitalCalculator" class="c-form form-validate form-vertical">
		<div class="padding1520 clearfix">
			<div style="width:22%;float:left;"><?php echo MyHelper::load_module(114); ?></div>
			<div style="width: 78%; float: left; padding: 20px;" class="border-box">
				<div class="clearfix">
					<div class="c-form-number">1.</div>
					<div class="c-form-label c-form-label-bold"><?php echo $this->form->getLabel("employer_insurance"); ?></div>
					<div class="c-form-input border-box clearfix"><div style="width: 20%;float: left;line-height: 28px;">&nbsp;</div><div style="width: 78%;float: left;"><?php echo $this->form->getInput("employer_insurance"); ?></div></div>
				</div>
				<div class="spacer-1"></div>
				<div class="clearfix">
					<div class="c-form-number">2.</div>
					<div class="c-form-label c-form-label-bold"><?php echo $this->form->getLabel("has_deductible"); ?></div>
					<div class="c-form-input border-box clearfix"><div style="width: 20%;float: left;line-height: 28px;">&nbsp;</div><div style="width: 78%;float: left;"><?php echo $this->form->getInput("has_deductible"); ?></div></div>
				</div>
				<div class="spacer-1"></div>
				<div class="clearfix">
					<div class="c-form-number">3.</div>
					<div class="c-form-label c-form-label-bold"><?php echo $this->form->getLabel("has_limit"); ?></div>
					<div class="c-form-input border-box clearfix"><div style="width: 20%;float: left;line-height: 28px;">&nbsp;</div><div style="width: 78%;float: left;"><?php echo $this->form->getInput("has_limit"); ?></div></div>

				</div>
				<div class="spacer-1"></div>
				<div class="clearfix">
					<div class="c-form-number">4.</div>
					<div class="c-form-label c-form-label-bold"><?php echo $this->form->getLabel("has_private"); ?></div>
					<div class="c-form-input border-box clearfix"><div style="width: 20%;float: left;line-height: 28px;">&nbsp;</div><div style="width: 78%;float: left;"><?php echo $this->form->getInput("has_private"); ?></div></div>
				</div>
				<div class="spacer-1"></div>
				<div class="clearfix">
					<div class="c-form-number">5.</div>
					<div class="c-form-label c-form-label-bold"><?php echo $this->form->getLabel("has_international"); ?></div>
					<div class="c-form-input border-box clearfix"><div style="width: 20%;float: left;line-height: 28px;">&nbsp;</div><div style="width: 78%;float: left;"><?php echo $this->form->getInput("has_international"); ?></div></div>
				</div>
				<div class="spacer-2"></div>
				<div class="clearfix">
					<div class="c-form-label c-form-label-bold" style="width: 40%;"><?php echo $this->form->getLabel("type"); ?></div>
					<div class="c-form-input border-box clearfix" style="width: 59%;"><?php echo $this->form->getInput("type"); ?></div>
				</div>
				<div class="spacer-2">
				</div>
				<div class="clearfix">
					<?php if (FALSE): ?>
						<a class="pull-right" href="<?php echo JRoute::_("index.php?Itemid=158") ?>" style="margin-left: 20px; margin-top: 2.5px;"><img src="images/icon-cart-buy.png" alt=""/></a>
					<?php endif; ?>
					<input type="submit" class="btn btn-primary pull-right" name="Save" value="<?php echo JText::_("COM_CALCULATOR_SAVE_AND_BUY"); ?>" style="padding: 5px 20px;"/>
				</div>
				<div class="spacer-2"></div>
				<div class="clearfix">
					<p>
						<strong><?php echo JText::_("COM_CALCULATOR_DISCLAIMER_TITLE"); ?></strong>
						<br />
						<?php echo JText::_("COM_CALCULATOR_DISCLAIMER_TEXT"); ?>
					</p>
				</div>
			</div>
		</div>

		<input type="hidden" name="return" value="<?php echo $this->return_page; ?>" />
		<input type="hidden" name="option" value="com_calculator" />
		<input type="hidden" name="task" value="hospital.save" />
		<?php echo JHtml::_('form.token'); ?>
	</form>
</div>

<script type="text/javascript">
	var employerInsurance, hasDeductible, hasLimit, hasPrivate, hasInternational, insuranceType;
	function updateHospital(v1, v2, v3, v4, v5) {
		var v1, v2, v3, v4, v5;
		v1 = employerInsurance.find("option:selected").val();
		v2 = hasDeductible.find("option:selected").val();
		v3 = hasLimit.find("option:selected").val();
		v4 = hasPrivate.find("option:selected").text();
		v5 = hasInternational.find("option:selected").val();

		var type = "Hospital Insurance with";
		if (v2 == 1) {
			type += " a deductible,";
		}

		if (v3 == 1) {
			type += " limits on the hospital costs,";
		}

		type += " covers up to " + v4 + " Wards";

		if (v5 == 1) {
			type += ", and provides international cover";
		}
		type += ".";
		insuranceType.val(type);
	}

	jQuery(document).ready(function () {
		employerInsurance = jQuery("#jform_employer_insurance");
		hasDeductible = jQuery("#jform_has_deductible");
		hasLimit = jQuery("#jform_has_limit");
		hasPrivate = jQuery("#jform_has_private");
		hasInternational = jQuery("#jform_has_international");
		insuranceType = jQuery("#jform_type");

		employerInsurance.on("change", function () {
			updateHospital();
		});
		hasDeductible.on("change", function () {
			updateHospital();
		});
		hasLimit.on("change", function () {
			updateHospital();
		});
		hasPrivate.on("change", function () {
			updateHospital();
		});
		hasInternational.on("change", function () {
			updateHospital();
		});
	});

</script>