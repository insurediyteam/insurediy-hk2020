<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_calculator
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Calculator Component Controller
 *
 * @package     Joomla.Site
 * @subpackage  com_calculator
 * @since       1.5
 */
class CalculatorController extends JControllerLegacy {

	/**
	 * Method to display a view.
	 *
	 * @param   boolean			If true, the view output will be cached
	 * @param   array  An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return  JController		This object to support chaining.
	 * @since   1.5
	 */
	public function display($cachable = false, $urlparams = false) {
		$cachable = true; // Huh? Why not just put that in the constructor?
		$user = JFactory::getUser();

		$vName = $this->input->get('view', 'hospital');
	
		if (!$user->get('guest')) {
			$this->input->set('view', $vName);
		} else {
			$this->input->set('view', 'user');
		}
		
		$safeurlparams = array(
			'id' => 'INT',
			'limit' => 'UINT',
			'limitstart' => 'UINT',
			'filter_order' => 'CMD',
			'filter_order_Dir' => 'CMD',
			'lang' => 'CMD'
		);

		return parent::display($cachable, $safeurlparams);
	}

}
