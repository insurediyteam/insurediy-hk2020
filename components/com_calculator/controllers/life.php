<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_insurediyci
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

class CalculatorControllerLife extends JControllerForm {

	public function getModel($name = 'Life', $prefix = 'CalculatorModel', $config = array('ignore_request' => true)) {
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}

	public function save() {
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$post = $app->input->get('jform', '', 'array');
		$user = JFactory::getUser();
		if ($user->get('guest')) {
			$app->redirect("index.php?option=com_calculator&view=user&Itemid=147");
		}

		$post['user_id'] = $user->id;
		$model->save($post);
		$total_life = Myhelper::numberFormat($post['total_life'], TRUE);
		$rounded = round($total_life / 1000000) * 1000000;
		$maxSumInsured = InsureDIYHelper::getMaxLifeSumInsured();
		$data = array();
		if ($rounded < $maxSumInsured) {
			$data['cover_amt'] = $rounded;
		} else {
			$data['cover_amt'] = $maxSumInsured;
		}
		$data['protection_needs_analysis'] = 1;

		$session = JFactory::getSession();
		$session->set('life.protection_needs_analysis', 1);
		$session->set("life.data", $data);
		$session->set("life.quotation_id", NULL);
		$session->set('life.step1CriteriaPass', NULL);

		$app->redirect("index.php?Itemid=156");
	}

}
