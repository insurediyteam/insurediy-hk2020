<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_insurediyci
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

class CalculatorControllerCi extends JControllerForm {

	public function getModel($name = 'Ci', $prefix = 'CalculatorModel', $config = array('ignore_request' => true)) {
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}

	public function save() {
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$post = $app->input->get('jform', '', 'array');
		$user = JFactory::getUser();
		if ($user->get('guest')) {
			$app->redirect("index.php?option=com_calculator&view=user&Itemid=147");
		}

		$post['user_id'] = $user->id;
		$model->save($post);
		$total_ci = Myhelper::numberFormat($post['total_ci'], TRUE);
		$rounded = round($total_ci / 1000000) * 1000000;
		$maxSumInsured = InsureDIYHelper::getMaxCiSumInsured();
		$data = array();
		if ($rounded < $maxSumInsured) {
			$data['cover_amt'] = $rounded;
		} else {
			$data['cover_amt'] = $maxSumInsured;
		}
		$data['protection_needs_analysis'] = 1;
//		$app->redirect("index.php?option=com_calculator&view=ci&Itemid=147");
		$session = JFactory::getSession();
		$session->set('ci.protection_needs_analysis', 1);
		$session->set("ci.data", $data);
		$session->set("ci.quotation_id", NULL);
		$session->set('ci.step1CriteriaPass', NULL);

		$app->redirect("index.php?Itemid=157");
	}

}
