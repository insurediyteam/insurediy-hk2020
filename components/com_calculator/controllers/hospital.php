<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_insurediyci
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

class CalculatorControllerHospital extends JControllerForm {

	public function getModel($name = 'Hospital', $prefix = 'CalculatorModel', $config = array('ignore_request' => true)) {
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}

	public function save() {
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$post = $app->input->get('jform', '', 'array');
		$user = JFactory::getUser();
		if ($user->get('guest')) {
			$app->redirect("index.php?option=com_calculator&view=user&Itemid=147");
		}

		$post['user_id'] = $user->id;
		$model->save($post);

		$data = array();
		if ($post['has_international']) {
			$data['cover_country'] = "WW";
		}
		$data['room_type'] = ($post['has_private']) ? 3 : 2;
		$data['cover_amt'] = ($post['has_limit']) ? 1 : 2;
		$data['protection_needs_analysis'] = 1;

		$session = JFactory::getSession();
		$session->set('hospitalself.protection_needs_analysis', 1);
		$session->set("hospitalself.data", $data);
		$session->set("hospitalself.quotation_id", NULL);

		$app->redirect("index.php?Itemid=170");
	}

}
