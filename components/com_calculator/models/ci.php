<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_calculator
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Calculator model.
 *
 * @package     Joomla.Site
 * @subpackage  com_calculator
 * @since       1.6
 */
class CalculatorModelCi extends JModelForm {

	/**
	 * Model typeAlias string. Used for version history.
	 *
	 * @var        string
	 */
	public $typeAlias = 'com_calculator.ci';
	public $_tb_ci = "#__calculator_ci";

	/**
	 * Get the return URL.
	 *
	 * @return  string	The return URL.
	 * @since   1.6
	 */
	public function getReturnPage() {
		return base64_encode($this->getState('return_page'));
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since   1.6
	 */
	protected function populateState() {
		$app = JFactory::getApplication();

		$return = $app->input->get('return', null, 'base64');

		if (!JUri::isInternal(base64_decode($return))) {
			$return = null;
		}

		$this->setState('return_page', base64_decode($return));

		// Load the parameters.
		$params = $app->getParams();
		$this->setState('params', $params);

		$this->setState('layout', $app->input->get('layout'));
	}

	public function getForm($data = array(), $loadData = TRUE) {
		// Get the form.
		$form = $this->loadForm('com_calculator.form', 'ci', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}

		return $form;
	}

	public function getItem() {
		$user = JFactory::getUser();
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__calculator_ci")
				->where("user_id = " . $user->id);
		$result = $db->setQuery($query)->loadObject();
		if ($result) {
			$result->annual_salary = Myhelper::numberFormat($result->annual_salary, FALSE, 0);
			$result->medical_cost = Myhelper::numberFormat($result->medical_cost, FALSE, 0);
			$result->existing_ci = Myhelper::numberFormat($result->existing_ci, FALSE, 0);
			$result->total_ci = Myhelper::numberFormat($result->total_ci, FALSE, 0);
			$result->monthly_premium = Myhelper::numberFormat($result->monthly_premium, FALSE, 0);
		}
		return $result;
	}

	public function save($post) {
		$db = JFactory::getDBO();
		$user = JFactory::getUser();

		$post['annual_salary'] = Myhelper::numberFormat($post['annual_salary'], TRUE);
		$post['medical_cost'] = Myhelper::numberFormat($post['medical_cost'], TRUE);
		$post['existing_ci'] = Myhelper::numberFormat($post['existing_ci'], TRUE);
		$post['total_ci'] = Myhelper::numberFormat($post['total_ci'], TRUE);
		$post['monthly_premium'] = Myhelper::numberFormat($post['monthly_premium'], TRUE);


		$query = $db->getQuery(TRUE)
				->select("count(*)")
				->from($this->_tb_ci)
				->where("user_id = " . $user->id);

		$isNew = $db->setQuery($query)->loadResult();

		$postObj = MyHelper::array2jObject($post);

		if ($isNew) {
			$db->updateObject($this->_tb_ci, $postObj, "user_id");
		} else {
			$db->insertObject($this->_tb_ci, $postObj, "id");
		}
		return TRUE;
	}

}
