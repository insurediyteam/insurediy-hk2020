<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_calculator
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Calculator model.
 *
 * @package     Joomla.Site
 * @subpackage  com_calculator
 * @since       1.6
 */
class CalculatorModelLife extends JModelForm {

	/**
	 * Model typeAlias string. Used for version history.
	 *
	 * @var        string
	 */
	public $typeAlias = 'com_calculator.life';
	public $_tb = "#__calculator_life";

	/**
	 * Get the return URL.
	 *
	 * @return  string	The return URL.
	 * @since   1.6
	 */
	public function getReturnPage() {
		return base64_encode($this->getState('return_page'));
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since   1.6
	 */
	protected function populateState() {
		$app = JFactory::getApplication();

		$return = $app->input->get('return', null, 'base64');

		if (!JUri::isInternal(base64_decode($return))) {
			$return = null;
		}

		$this->setState('return_page', base64_decode($return));

		// Load the parameters.
		$params = $app->getParams();
		$this->setState('params', $params);

		$this->setState('layout', $app->input->get('layout'));
	}

	public function getForm($data = array(), $loadData = TRUE) {
		// Get the form.
		$form = $this->loadForm('com_calculator.form', 'life', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}

		return $form;
	}

	public function getItem() {
		$user = JFactory::getUser();
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from($this->_tb)
				->where("user_id = " . $user->id);
		$result = $db->setQuery($query)->loadObject();
		if ($result) {
			$result->expense = Myhelper::numberFormat($result->expense, FALSE, 0);
			$result->child_education = Myhelper::numberFormat($result->child_education, FALSE, 0);
			$result->outstanding_loan = Myhelper::numberFormat($result->outstanding_loan, FALSE, 0);
			$result->addtional_sum = Myhelper::numberFormat($result->addtional_sum, FALSE, 0);
			$result->existing_life = Myhelper::numberFormat($result->existing_life, FALSE, 0);
			$result->total_life = Myhelper::numberFormat($result->total_life, FALSE, 0);
			$result->monthly_premium = Myhelper::numberFormat($result->monthly_premium, FALSE, 0);
		}
		return $result;
	}

	public function save($post) {
		$db = JFactory::getDBO();
		$user = JFactory::getUser();

		$post['expense'] = Myhelper::numberFormat($post['expense'], TRUE);
		$post['child_education'] = Myhelper::numberFormat($post['child_education'], TRUE);
		$post['outstanding_loan'] = Myhelper::numberFormat($post['outstanding_loan'], TRUE);
		$post['addtional_sum'] = Myhelper::numberFormat($post['addtional_sum'], TRUE);
		$post['existing_life'] = Myhelper::numberFormat($post['existing_life'], TRUE);
		$post['total_life'] = Myhelper::numberFormat($post['total_life'], TRUE);
		$post['monthly_premium'] = Myhelper::numberFormat($post['monthly_premium'], TRUE);


		$query = $db->getQuery(TRUE)
				->select("count(*)")
				->from($this->_tb)
				->where("user_id = " . $user->id);

		$isNew = $db->setQuery($query)->loadResult();

		$postObj = MyHelper::array2jObject($post);

		if ($isNew) {
			$db->updateObject($this->_tb, $postObj, "user_id");
		} else {
			$db->insertObject($this->_tb, $postObj, "id");
		}
		return TRUE;
	}

}
