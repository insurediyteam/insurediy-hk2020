<?php
defined('_JEXEC') or die;

class PopupController extends JControllerLegacy {
	public function display($cachable = false, $urlparams = false) {
		$jInput = JFactory::getApplication()->input;
		$reqView = $jInput->get('view');
		$reqLayout = $jInput->get('layout');

		switch ($reqView) {
			case 'plugins':
				$model = FALSE;
				break;

			case 'blank':
				$model = FALSE;
				break;

			default:
				MyUri::toHome();
		}

		if (($view = $this->getView($reqView, 'html'))) {
			if(isset($model)) {
				if($model) {
					$model = $this->getModel($model);
					$view->setModel($model, TRUE);
				}
			} else {
				$model = $this->getModel($reqView);
				$view->setModel($model, TRUE);
			}

			if(isset($xtraModel) && $xtraModel) {
				$view->setModel($this->getModel($xtraModel), FALSE);
			}

			$view->setLayout($reqLayout);
			$view->display();
		}
	}

}
