<?php
defined('_JEXEC') or die;

$eName = JFactory::getApplication()->input->get('e_name', '');
if (empty($eName)) {
	return;
}

$siteUrl = MyUri::getUrls('site');

$js = 'function insertVariables(val){'
		. 'var jForm = jQuery("#form-variables"), winP = window.parent, variable; '
//		. 'val = jForm.find("input[name=\'variable\']:checked").val();'
		. 'if(val) {'
		. 'var patterned = \'{\' + val + \'}\';'
		. 'winP.jInsertEditorText(patterned, \'' . $eName . '\');'
		. 'winP.SqueezeBox.close();'
		. '}'
		. 'return false;'
		. '}';
JFactory::getDocument()->addScriptDeclaration($js);
?>
<form id="form-variables">
	<h3><?php echo JText::_('Variables'); ?></h3>
	<div class="spacer-1">
	</div>
	<table class="table">
		<tr>
			<td style="width: 20px;">
				<input id="variable1" type="radio" name="variable" value="first_name" onclick="javascript:insertVariables('first_name');" />
			</td>
			<td>
				<label for="variable1">First Name</label>
			</td>
		</tr>
		<tr>
			<td>
				<input id="variable2" type="radio" name="variable" value="last_name" onclick="javascript:insertVariables('last_name');" />
			</td>
			<td>
				<label for="variable2">Last Name</label>
			</td>
		</tr>
		<tr>
			<td>
				<input id="variable3" type="radio" name="variable" value="order_no" onclick="javascript:insertVariables('order_no');" />
			</td>
			<td>
				<label for="variable3">Order Number</label>
			</td>
		</tr>
		<tr>
			<td>
				<input id="variable5" type="radio" name="variable" value="attach_form_list" onclick="javascript:insertVariables('attach_form_list');" />
			</td>
			<td>
				<label for="variable5">Attached Form List</label>
			</td>
		</tr>
		<tr>
			<td>
				<input id="variable6" type="radio" name="variable" value="address" onclick="javascript:insertVariables('address');" />
			</td>
			<td>
				<label for="variable6">Address</label>
			</td>
		</tr>
		<tr>
			<td>
				<input id="variable7" type="radio" name="variable" value="date_of_appointment" onclick="javascript:insertVariables('date_of_appointment');" />
			</td>
			<td>
				<label for="variable7">Date of Appointment</label>
			</td>
		</tr>
		<tr>
			<td>
				<input id="variable8" type="radio" name="variable" value="time_of_appointment" onclick="javascript:insertVariables('time_of_appointment');" />
			</td>
			<td>
				<label for="variable8">Time of Appointment</label>
			</td>
		</tr>
		<tr>
			<td>
				<input id="variable9" type="radio" name="variable" value="contact_number" onclick="javascript:insertVariables('contact_number');" />
			</td>
			<td>
				<label for="variable9">Contact Number</label>
			</td>
		</tr>
		<tr>
			<td>
				<input id="variable4" type="radio" name="variable" value="insurer" onclick="javascript:insertVariables('insurer');" />
			</td>
			<td>
				<label for="variable4">Insurer (only for AXA and Sun Life)</label>
			</td>
		</tr>
		<tr>
			<td>
				<input id="variable10" type="radio" name="variable" value="points" onclick="javascript:insertVariables('points');" />
			</td>
			<td>
				<label for="variable10">Points (For redeem product notification)</label>
			</td>
		</tr>
		<tr>
			<td>
				<input id="variable11" type="radio" name="variable" value="product" onclick="javascript:insertVariables('product');" />
			</td>
			<td>
				<label for="variable11">Product Name (For redeem product notification)</label>
			</td>
		</tr>
		<tr>
			<td>
				<input id="variable12" type="radio" name="variable" value="order_ref" onclick="javascript:insertVariables('order_ref');" />
			</td>
			<td>
				<label for="variable12">Order Reference No (ID of point record) (For redeem product notification)</label>
			</td>
		</tr>
		<tr>
			<td>
				<input id="variable13" type="radio" name="variable" value="user_email" onclick="javascript:insertVariables('user_email');" />
			</td>
			<td>
				<label for="variable13">User's email</label>
			</td>
		</tr>
		<tr>
			<td>
				<input id="variable14" type="radio" name="variable" value="date" onclick="javascript:insertVariables('date');" />
			</td>
			<td>
				<label for="variable14">Date</label>
			</td>
		</tr>
	</table>
</form>

<button class="btn" onclick="insertVariables();"><?php echo JText::_('Insert Variables'); ?></button>
