<?php
	defined('_JEXEC') or die;
	
	JHtml::_('behavior.keepalive');
	JHtml::_('behavior.formvalidation');
	JHtml::_('formbehavior.chosen', '.insurediy-form select');
	JHtml::_('MyBehavior.jsInsurediy');
	JHtml::_('script', 'system/jquery.validate.js', false, true);
	
	
	$currency = $this->currency;
	$quotation = $this->quotation;
	$form = & $this->form;
	$fldGroup = "contact-details";
?>

<script type="text/javascript">
	function toggleEdit(div_id, isEdit) {
		//do edit
		if (isEdit === true) {
			$(div_id).toggleClass("readonly", true);
			} else if (isEdit === false) {
			$(div_id).toggleClass("readonly", false);
			} else {
			$(div_id).toggleClass("readonly");
		}
		if (jQuery("#" + div_id).hasClass("readonly")) {
			$$("#" + div_id + " input:not('.ignore-edit')").set("readonly", "readonly");
			//			$$("#" + div_id + " select:not('.ignore-edit')").set("disabled", "disabled");
			jQuery("#" + div_id + " select:not('.ignore-edit')").prop('disabled', true).trigger("liszt:updated");
			jQuery("#" + div_id + " select:not('.ignore-edit')").prop('disabled', false);
			} else {
			$$("#" + div_id + " input:not('.ignore-edit')").set("readonly", "");
			//			$$("#" + div_id + " select:not('.ignore-edit')").set("disabled", "");
			jQuery("#" + div_id + " select:not('.ignore-edit')").prop('disabled', false).trigger("liszt:updated");
		}
		//		$$("#" + div_id + " .chzn-container").toggleClass("chzn-disabled");
	}
	
	function toggleThis(div_id, btn_id) {
		new Fx.Slide(div_id, {resetHeight: true}).toggle();
		//		jQuery("#" + div_id).toggle(100);
		$(btn_id).toggleClass("active");
	}
	
	function nameChanged(showMsg) {
		var fname = jQuery("#jform_contact_firstname");
		var lname = jQuery("#jform_contact_lastname");
		var accName = jQuery("#jform_bank_acct_holder_name");
		accName.val(fname.val() + " " + lname.val());
		if (showMsg) {
			$('insurediy-popup-box').setStyle('display', 'block');
		}
	}
	
	function checkData(div_id) {
		//		var jDiv = jQuery("#" + div_id);
		var firstTime = <?php echo json_encode(($this->current_user->is_firsttime) ? 1 : 0) ?>;
		if (firstTime == "1") {
			return false;
		}
		//		jDiv.find("input").each(function() {
		//			var jThis = jQuery(this);
		//			if (jThis.val().length > 0) {
		//				hasData = true;
		//			}
		//		});
		return true;
	}
	
	window.addEvent('domready', function () {
		jQuery("#adminForm").validate({
			ignore: [] // <-- option so that hidden elements are validated
		});
		var jForm = jQuery("form#adminForm");
		jForm.submit(function () {
			jForm.find("label.error").each(function () {
				var jThis = jQuery(this);
				var jContainer = jThis.closest("div.control-group");
				var jControl = jContainer.find("div.controls");
				jControl.find(".error-container").append(jThis.clone());
				jThis.remove();
			});
			var errors = jForm.find("label.error");
			if (errors.length > 0) {
				var label = jForm.find("label.error:first");
				jQuery('html, body').animate({
					'scrollTop': label.offset().top - 400
				}, 300);
			}
		});
		
		//		addRow('ajax-load-existing-insurance-policy');
		var fname = jQuery("#jform_contact_firstname");
		var lname = jQuery("#jform_contact_lastname");
		var hasData = checkData('contact-details-fields');
		//		nameChanged(false);
		//		fname.on("change", function () {
		//			nameChanged(hasData);
		//		});
		//		lname.on("change", function () {
		//			nameChanged(hasData);
		//		});
		if (hasData) {
			toggleEdit('contact-details-fields', true);
			} else {
			jQuery("#edit-btn").hide();
		}
		var idType = jQuery("#jform_contact_identity_type");
		var doe = jQuery("#contact_identity_doe");
		var expirtyDateCal = jQuery("#jform_contact_expiry_date");
		expirtyDateCal.prop("required", '');
		if (idType.val() === "passport") {
			doe.fadeIn();
		}
		idType.on('change', function () {
			var typeValue = idType.val();
			if (typeValue === "passport") {
				doe.fadeIn();
				expirtyDateCal.prop("required", 'required');
				} else {
				doe.fadeOut();
				expirtyDateCal.prop("required", '');
			}
			jQuery("#jform_bank_id_type option[value=" + typeValue + "]").attr("selected", "selected").trigger("liszt:updated");
		});
		jQuery("#jform_contact_identity_no").keyup(function (e) {
			jQuery("#jform_bank_id_no").val(jQuery("#jform_contact_identity_no").val());
		});
	});
</script>

<div class="insurediy-form bb-form">
	<div class="header-top-wrapper">
		<div class="header-top">
			<?php echo InsureDIYTravelHelper::renderHeader('icon-travel', JText::_('COM_INSUREDIYTRAVEL_PAGE_HEADING_CONTACT_DETAILS'), 3); ?>
		</div>
	</div>
	<div style="padding:20px;margin-top: 58px;">
		<?php echo MyHelper::renderDefaultMessage(); ?>
		<form action="<?php echo JRoute::_('index.php?option=com_insurediytravel&view=form'); ?>" method="post" name="adminForm" id="adminForm" novalidate class="form-validate form-vertical">
			<fieldset class="insurediy-travel-data">
				<div id="contact-details-fields" class="contact-details-fields">
					<div class="row-left">
						<div class="control-group">
							<div class="control-label"><?php echo $this->form->getLabel('contact_firstname'); ?></div>
							<div class="controls">
								<?php echo $this->form->getInput('contact_firstname'); ?>
								<div class="error-container"></div>
							</div>
						</div>
						<div class="control-group">
							<div class="control-label"><?php echo $this->form->getLabel('contact_lastname'); ?></div>
							<div class="controls">
								<?php echo $this->form->getInput('contact_lastname'); ?>
								<div class="error-container"></div>
							</div>
						</div>
						<div class="control-group">
							<div class="control-label"><?php echo $this->form->getLabel('contact_contact_no'); ?></div>
							<div class="controls">
								<?php echo $this->form->getInput('contact_contact_no'); ?>
								<div class="error-container"></div>
							</div>
						</div>
						<div class="control-group">
							<div class="control-label"><?php echo $this->form->getLabel('contact_identity_no'); ?></div>
							<div style="width: 29%;margin-right: 1%;float: left;">
								<div class="controls">
									<?php echo $this->form->getInput('contact_identity_type'); ?>
									<div class="error-container"></div>
								</div>
							</div>
							<div style="width: 70%;float: left;">
								<div class="control-group">
									<div class="controls">
										<?php echo $this->form->getInput('contact_identity_no'); ?>
										<div class="error-container"></div>
									</div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
						<div id="contact_identity_doe" style="margin-left: 0;display: none;">
							<div class="control-label"><?php echo $this->form->getLabel('contact_expiry_date'); ?></div>
							<div class="controls">
								<?php echo $this->form->getInput('contact_expiry_date'); ?>
								<div class="error-container"></div>
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<div class="row-mid"></div>
					<div class="row-right">
						<div>
							<div class="span4 control-group">
								<div class="control-label"><?php echo $this->form->getLabel('contact_room_no'); ?></div>
								<div class="controls">
									<?php echo $this->form->getInput('contact_room_no'); ?>
									<div class="error-container"></div>
								</div>
							</div>
							<div class="span4 control-group">
								<div class="control-label"><?php echo $this->form->getLabel('contact_floor_no'); ?></div>
								<div class="controls">
									<?php echo $this->form->getInput('contact_floor_no'); ?>
									<div class="error-container"></div>
								</div>
							</div>
							<div class="span4 control-group">
								<div class="control-label"><?php echo $this->form->getLabel('contact_block_no'); ?></div>
								<div class="controls">
									<?php echo $this->form->getInput('contact_block_no'); ?>
									<div class="error-container"></div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
						<div>
							<div class="span6 control-group">
								<div class="control-label"><?php echo $this->form->getLabel('contact_building_name'); ?></div>
								<div class="controls">
									<?php echo $this->form->getInput('contact_building_name'); ?>
									<div class="error-container"></div>
								</div>
							</div>
							<div class="span6 control-group">
								<div class="control-label"><?php echo $this->form->getLabel('contact_street_name'); ?></div>
								<div class="controls">
									<?php echo $this->form->getInput('contact_street_name'); ?>
									<div class="error-container"></div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
						<div>
							<div class="span6 control-group">
								<div class="control-label"><?php echo $this->form->getLabel('contact_district_name'); ?></div>
								<div class="controls">
									<?php echo $this->form->getInput('contact_district_name'); ?>
									<div class="error-container"></div>
								</div>
							</div>
							<div class="span6 control-group">
								<div class="control-label"><?php echo $this->form->getLabel('contact_postalcode'); ?></div>
								<div class="controls">
									<?php echo $this->form->getInput('contact_postalcode'); ?>
									<div class="error-container"></div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
						<div>
							<div class="span6 control-group">
								<div class="control-label"><?php echo $this->form->getLabel('contact_country'); ?></div>
								<div class="controls">
									<?php echo $this->form->getInput('contact_country'); ?>
									<div class="error-container"></div>
								</div>
							</div>
							<div class="span6 control-group">
								<div class="controls" style="padding-top: 30px;text-align: right;">
									<a id="edit-btn" href="javascript:toggleEdit('contact-details-fields')" class="btn btn-primary" style="padding: 5px 20px;"><?php echo JText::_("BTN_EDIT"); ?></a>
								</div>
							</div>
							<div class="clear"></div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
				
				<!-- Social Stuffs Start -->
				<div class="insurediy-contact-detail-promotion-code" style="margin-bottom: 10px;">
					<div class="title-text"><?php echo JText::_("SOCIAL_PROMO_CODE_TITLE"); ?></div>
					<div class="title-text2">
						<span style="font-size:14px;line-height: 30px;"><strong><?php echo JText::_("SOCIAL_PROMO_CODE_LABEL"); ?></strong></span>&nbsp;&nbsp;&nbsp;
					</div>
					<div style="float:right;width: 262px;">
						<input name="jform[promo_code]" id="jform_promo_code" type="text" /> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("SOCIAL_PROMO_CODE_DESC"); ?>">
					</div>
					<div class="clear"></div>
				</div>
				<div class="grey-hrule"></div>
				<div class="container5 clearfix" style="padding: 20px 10px; margin-top:10px;">
					<div class="clearfix">
						<div class="cpan5"><img src="images/DIY_Rewards.png" style="margin: 10px auto;display: block;" alt="Rewards" /></div>
						<div class="cpan55">
							<?php if (!$this->user->referred_by): ?>
							<div class="clearfix">
								<div class="cpan2">
									<div class="blue-title-text"><?php echo JText::_("SOCIAL_REFERRAL_TITLE"); ?> <img class="hasTooltip" alt="help-quote" style="margin-top: -5px;" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("SOCIAL_REFERRAL_DESC"); ?>"/></div>
									<span><?php echo JText::_("SOCIAL_REFERRAL_MSG_HEADER"); ?> <?php echo JText::_("SOCIAL_REFERRAL_MSG"); ?></span>
								</div>
								<div style="float:right;width: 262px;">
									<?php echo $form->getInput("referred_by"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("SOCIAL_REFERRAL_DESC"); ?>" />
								</div>
							</div>
							<div class="spacer-1"></div>
							<div class="grey-hrule"></div>
							<div class="spacer-1"></div>
							<?php endif; ?>
							
							<div class="clearfix">
								<div class="cpan2">
									<span class="blue-title-text" style="font-size: 19px;font-family:'sourcesanspro-semibold' "><?php echo JText::_("SOCIAL_USE_YOUR_SOCIAL_NETWORK"); ?><br/>
									<?php echo JText::_("SOCIAL_SHARE_TITLE"); ?></span>
									<div class="spacer-1"></div>
									<span style="font-size: 13px;"><?php echo InsureDIYTravelHelper::getRefExp(); ?></span>
								</div>
								<div class="cpan2">
									<div style="float:right;" class="social-fb-msg">
										<?php echo InsureDIYTravelHelper::getRefMsg(); ?>
									</div>
									<div class="clearfix">
										<div style="float:right;">
											<div class="social-btn-wrapper">
												<script type="text/javascript">var sharerwb = "<?php echo InsureDIYHelper::getWeiboSharer() ?>";</script>
												<a href="javascript: void(0)" target="_parent" onclick="window.open(sharerwb, 'sharer', 'width=626,height=436');"><img src="images/layout-icons/weibo.png"></a>
											</div>
										</div>
										<div style="float:right;margin-right: 20px;padding-top:6px;">
											<div class="social-btn-wrapper">
												<script type="text/javascript">var sharer = "<?php echo InsureDIYHelper::getFbSharer() ?>";</script>
												<a href="javascript: void(0)" target="_parent" onclick="window.open(sharer, 'sharer', 'width=626,height=436');"><img src="images/layout-icons/facebook.png"></a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Social Stuffs End-->
				<input type="hidden" name="task" value="form.step4save" />
				<input type="hidden" name="quotation_id" value="<?php echo $quotation['id']; ?>" />
				
				<?php echo JHtml::_('form.token'); ?>
				<div style="float:left;margin:15px 0;">
					<div>{modulepos insurediy-secured-ssl}</div>
				</div>
				<div class="btn-toolbar" style="float:right">
					<div>
						<button style="margin-right:10px;" type="button" onclick="javascript:document.adminForm.task.value = 'form.back';
						document.adminForm.submit();" class="btn btn-primary validate"><?php echo JText::_('BTN_BACK') ?></button>
						<button type="submit" class="btn btn-primary validate"><?php echo JText::_('JCONTINUE') ?></button>
					</div>
				</div>
				<div class="clear"></div>
			</fieldset>
		</form>
	</div>
</div>
<div class="logo-slider"><?php echo MyHelper::load_module(102); ?></div>

<?php /* Popup Box */ ?>
<div class="insurediy-popup" id="insurediy-popup-box" style="display:none;">
	<div class="header">
		<div class="text-header"><?php echo JText::_("POP_IMPORTANT_NOTICE"); ?></div>
		<a class="close" href="javascript:cleanInsureDIYPopup('insurediy-popup-box');">&nbsp;</a>
		<div class="clear"></div>
	</div>
	<div class="padding">
		<div><?php echo JText::_("COM_INSUREDIYTRAVEL_NAME_CHANGE_NOTICE_MSG"); ?></div>
		<div style="text-align:right;"><input type="button" class="btn btn-primary" style="height:30px;padding:5px 20px;font-size:14px !important;" onclick="javascript:cleanInsureDIYPopup('insurediy-popup-box');
			return false;" value="<?php echo JText::_("JOK"); ?>" /></div>
	</div>
</div>

<script>
	dataLayer.push({
		'event': 'checkoutOption',
		'ecommerce': {
			'checkout': {
				'actionField': {'step': 2}
			}
		}
	});
</script>


