<?php
	defined('_JEXEC') or die;
	
	JHtml::_('behavior.keepalive');
	JHtml::_('behavior.formvalidation');
	JHtml::_('formbehavior.chosen', '.insurediy-form-content select');
	JHtml::_('script', 'system/jquery.validate.js', false, true);
	
	$params = $this->state->get('params');
	$itemid = JRequest::getVar("itemid", FALSE);
	$form = $this->form;
?>

<script type="text/javascript">
	jQuery(document).ready(function () {
		jQuery("#adminForm").validate({
			ignore: ".select-required", // <-- option so that hidden elements are validated
			//			rules: {
			//				jform_job_nature: {// <-- name of actual text input
			//					required: true,
			//					maxlength: 100
			//				}
			//			}
		});
		
		var startDate = jQuery("#jform_start_date");
		var endDate = jQuery("#jform_end_date");
		
		startDate.on("change", function (e) {
			var jThis = jQuery(this);
			var startfrom = jThis.val().split("-");
			endDate.datepicker("option", "minDate", new Date(new Date(startfrom[2], startfrom[1] - 1, startfrom[0])));
			endDate.datepicker("refresh");
		});
	});
</script>
<!-- Google Tag Manager Data Layer-->
<script>
	/**
		* Call this function when a user clicks on a product link. This function uses the
		event
		* callback datalayer variable to handle navigation after the ecommerce data has
		been sent
		* to Google Analytics.
		* @param {Object} productObj An object representing a
		product.
		*/
		function pushProductClick(productObj) {
			dataLayer.push({
				'event': 'productClick',
				'ecommerce': {
					'click': {
						'actionField': {'list': productObj.list}, // Optional list property.
						'products': [{
							'name': productObj.name,
							// Name or ID is required.
							'id': productObj.id
						}]
					}
				},
				'eventCallback': function() {
				}
			});
		}
	</script>
	<!-- End of Google Tag Manager Data Layer-->
	<div class="insurediy-form">
		<div class="header-top-wrapper">
			<?php echo InsureDIYTravelHelper::renderHeader('icon-travel', JText::_('COM_INSUREDIYTRAVEL_PAGE_HEADING_ABOUT_YOUR_TRIP'), 1); ?>
		</div>
		<div class="edit<?php echo $this->pageclass_sfx; ?> insurediy-form-content">
			<?php echo MyHelper::renderDefaultMessage(); ?>
			<form action="<?php echo JRoute::_('index.php?option=com_insurediytravel&view=form'); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-vertical" enctype="multipart/form-data">
				<fieldset class="insurediy-form-fields">
					<div class="control-row">
						<div class="span7">
							<div class="control-group span8">
								<div class="control-label"><?php echo $this->form->getLabel('country'); ?>
									<div id="error-container"></div>
								</div>
								<div class="controls"><?php echo $this->form->getInput('country'); ?>
									<div class="error-container"></div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
						<div class="span5" style="margin:0px;">
							<div class="control-group">
								<div class="control-label"><?php echo $this->form->getLabel('trip_type'); ?></div>
								<div class="controls"><?php echo $this->form->getInput('trip_type'); ?>
									<div class="error-container"></div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<div class="control-row">
						<div class="span7">
							<div class="span5">
								<div class="control-group">
									<div class="control-label"><?php echo $this->form->getLabel('start_date'); ?></div>
									<div class="controls"><?php echo $this->form->getInput('start_date'); ?>
										<div class="error-container"></div>
									</div>
								</div>
							</div>
							<div class="span7">
								<div id="end-date-wrapper" class="control-group">
									<div class="control-label"><?php echo $this->form->getLabel('end_date'); ?></div>
									<div class="controls"><?php echo $this->form->getInput('end_date'); ?>
										<div class="error-container"></div>
									</div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
						<div class="span5" style="margin:0px;">
							<div class="control-group">
								<div class="control-label"><?php echo $this->form->getLabel('is_group'); ?></div>
								<div class="controls" style="width: 50%;"><?php echo $this->form->getInput('is_group'); ?>
									<div class="error-container"></div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<div class="control-row">
						<div class="span7">
							<div class="control-group">
								<div class="control-label"><?php echo $this->form->getLabel('group_type'); ?></div>
								<div class="controls"><?php echo $this->form->getInput('group_type'); ?>
									<div class="error-container"></div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
						<div class="span5" style="margin:0px;">
							<div id='travellers_for_group' class="control-group">
								<div class="control-label"><?php echo $this->form->getLabel('no_of_travellers'); ?></div>
								<div class="controls" style="width: 50%;"><?php echo $this->form->getInput('no_of_travellers'); ?>
									<div class="error-container"></div>
								</div>
							</div>
							<div id='travellers_for_family' class="control-group">
								<div class="control-label"><?php echo $this->form->getLabel('no_of_adults'); ?></div>
								<div class="controls" style="width: 50%;"><?php echo $this->form->getInput('no_of_adults'); ?>
									<div class="error-container"></div>
								</div>
								<div class="control-label"><?php echo $this->form->getLabel('no_of_children'); ?></div>
								<div class="controls" style="width: 50%;"><?php echo $this->form->getInput('no_of_children'); ?>
									<div class="error-container"></div>
								</div>
								<div class="control-label"><?php echo $this->form->getLabel('no_of_s_children'); ?></div>
								<div class="controls" style="width: 50%;"><?php echo $this->form->getInput('no_of_s_children'); ?>
									<div class="error-container"></div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<div class="clear"></div>
				</fieldset>
				<fieldset class="insurediy-agreements">
					<?php echo MyHelper::load_module_pos("agreements"); ?>
					<?php if (FALSE): ?>
					<?php foreach ($this->form->getFieldset("agree_fields") as $field): ?>
					<div>
						<div class="control-group">
							<div class="control-label" style="float:left;width:4.5%;">
								<div style="position:relative;">
									<?php echo $field->input; ?>
								</div>
							</div>
							<div class="controls" style="float:left;width:95%">
								<?php echo $field->label; ?>
								<div class="error-container"></div>
							</div>
							<div style="clear:both"></div>
						</div>
					</div>
					<?php endforeach; ?>
					<?php endif; ?>
				</fieldset>
				<input type="hidden" name="task" value="form.step1save" />
				<?php echo $this->form->getInput('id'); ?>
				<?php if ($itemid): ?>
				<input type="hidden" name="Itemid" value="$itemid" />
				<?php endif; ?>
				<?php echo JHtml::_('form.token'); ?>
				
				<div style="float:left;margin:15px 0;">
					{modulepos insurediy-secured-ssl}
				</div>
				<div class="btn-toolbar" style="float:right">
					<div class="btn-group">
						<button type="submit" class="btn btn-primary validate" onclick="pushProductClick({name:'Travel',id:'travel01', list:'mainpage'})"><?php echo JText::_('JCONTINUE') ?></button>
					</div>
				</div>
				<div class="clear"></div>
			</form>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="logo-slider"><?php echo MyHelper::load_module(102); ?></div>
	
	<?php /* Popup Box */ ?>
	<div class="insurediy-popup" id="insurediy-popup-box" style="display:none;">
		<div class="header">
			<div class="text-header"><?php echo JText::_("POP_IMPORTANT_NOTICE"); ?></div>
			<a class="close" href="javascript:cleanInsureDIYPopup('insurediy-popup-box');">&nbsp;</a>
			<div class="clear"></div>
		</div>
		<div class="padding">
			<div><?php echo JText::_("COM_INSUREDIYTRAVEL_MEDICAL_ISSUE_MSG"); ?></div>
			<div style="text-align:right;margin-top: 20px;">
				<input type="button" class="btn btn-primary" style="height:30px;padding:5px 20px;font-size:14px !important;" onclick="javascript:cleanInsureDIYPopup('insurediy-popup-box');
				return false;" value="<?php echo JText::_("JOK"); ?>" />
			</div>
		</div>
	</div>
	<?php if (FALSE): ?>
	<div class="insurediy-popup" id="insurediy-popup-box-2" style="display:none;">
		<div class="header">
			<div class="text-header"><?php echo JText::_("COM_INSUREDIYTRAVEL_POP_UP_HEADER_MESSAGE"); ?></div>
			<a class="close" href="javascript:cleanInsureDIYPopup('insurediy-popup-box-2');">&nbsp;</a>
			<div class="clear"></div>
		</div>
		<div class="padding">
			<div><?php echo JText::_("COM_INSUREDIYTRAVEL_POP_UP_BODY_IS_NEW_QUOTE"); ?></div>
			<div style="margin-top: 20px;text-align:right;">
				<form action="<?php echo JRoute::_('index.php'); ?>" method="post" name="adminForm" id="getNew">
					<input type="hidden" name="option" value="com_insurediytravel" />
					<input type="hidden" name="task" value="form.startNewQuotation" />
					<?php echo JHtml::_('form.token'); ?>
					<input type="submit" class="btn btn-primary" style="height:30px;padding:5px 20px;font-size:14px !important;" value="<?php echo JText::_("JNEW"); ?>"/>
					<a href="<?php echo $this->continueURL; ?>" class="btn btn-primary" style="height:30px;padding:0px 20px;line-height: 30px;font-size:14px !important;"><?php echo JText::_("JCONTINUE"); ?></a>
				</form>
			</div>
		</div>
	</div>
	<?php endif; ?>
	
	
	<script type="text/javascript">
		var jFamily, jGroup, groupType, endDateWrapper, endDateInput, tripType;
		
		jQuery(document).ready(function () {
			var jForm = jQuery("form#adminForm");
			var jSelects = jForm.find("select");
			var jInputs = jForm.find("input");
			
			jFamily = jQuery("#travellers_for_family");
			jGroup = jQuery("#travellers_for_group");
			groupType = jQuery("input[name='jform[group_type]']");
			endDateWrapper = jQuery("#end-date-wrapper");
			endDateInput = endDateWrapper.find("#jform_end_date");
			tripType = jQuery("input[name='jform[trip_type]']");
			
			jSelects.each(function () {
				var jThis = jQuery(this);
				jThis.on("change", selectOnChange);
			});
			jInputs.each(function () {
				var jThis = jQuery(this);
				jThis.on("change", selectOnChange);
			});
			
			
			jForm.submit(function () {
				jForm.find("label.error").each(function () {
					var jThis = jQuery(this);
					var jContainer = jThis.closest("div.control-group");
					var jControl = jContainer.find("div.controls");
					jControl.find(".error-container").append(jThis.clone());
					jThis.remove();
				});
				var errors = jForm.find("label.error");
				if (errors.length > 0) {
					var label = jForm.find("label.error:first");
					jQuery('html, body').animate({
						'scrollTop': label.offset().top - 400
					}, 300);
				}
			});
			
			groupType.each(function () {
				var jThis = jQuery(this);
				jThis.on("change", function () {
					groupOnChange(jThis);
				});
			});
			var selectedGroupType = jQuery("input[name='jform[group_type]']:checked");
			groupOnChange(selectedGroupType);
			
			tripType.each(function () {
				var jThis = jQuery(this);
				jThis.on("change", function () {
					tripOnChange(jThis);
				});
			});
			var selectedTripType = jQuery("input[name='jform[trip_type]']:checked");
			tripOnChange(selectedTripType);
		});
		
		function groupOnChange(element) {
			var id = element.prop("id");
			if (id === "insurediy-with-group") {
				jFamily.fadeOut(function () {
					jGroup.fadeIn(500);
				});
				jGroup.find("select").each(function () {
					jQuery(this).toggleClass("select-required", false);
				});
				jFamily.find("select").each(function () {
					jQuery(this).toggleClass("select-required", true);
				});
				} else if (id === "insurediy-with-family") {
				jGroup.fadeOut(function () {
					if (jFamily.is(":hidden")) {
						jFamily.fadeIn(500);
					}
				});
				jGroup.find("select").each(function () {
					jQuery(this).toggleClass("select-required", true);
				});
				jFamily.find("select").each(function () {
					jQuery(this).toggleClass("select-required", false);
				});
				} else {
				jGroup.fadeOut(500);
				jFamily.fadeOut(500);
				jGroup.find("select").each(function () {
					jQuery(this).toggleClass("select-required", true);
				});
				jFamily.find("select").each(function () {
					jQuery(this).toggleClass("select-required", true);
				});
			}
			return true;
		}
		
		function tripOnChange(element) {
			var id = element.prop("id");
			if (id == "insurediy-single-trip") {
				endDateWrapper.fadeIn(500);
				endDateInput.attr("required", "required");
				endDateInput.attr("aria-required", "true");
				} else {
				endDateWrapper.fadeOut(500);
				endDateInput.removeAttr("required");
				endDateInput.removeAttr("aria-required");
			}
			return true;
		}
		
		function selectOnChange() {
			var jThis = jQuery(this);
			var jControl = jThis.closest("div.controls");
			if (jThis.val()) {
				jControl.find(".error-container label.error").remove();
				} else {
				jControl.find("label.error").each(function () {
					var jThis = jQuery(this);
					var jContainer = jThis.closest("div.control-group");
					var jControl = jContainer.find("div.controls");
					jControl.find(".error-container").append(jThis.clone());
					jThis.remove();
				});
			}
		}
	</script>
