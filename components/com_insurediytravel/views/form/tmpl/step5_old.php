<?php
	/**
		* @package     Joomla.Site
		* @subpackage  com_insurediytravel
		*
		* @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
		* @license     GNU General Public License version 2 or later; see LICENSE.txt
	*/
	defined('_JEXEC') or die;
	
	JHtml::_('behavior.keepalive');
	//JHtml::_('behavior.formvalidation');
	JHtml::_('formbehavior.chosen', '.insurediy-form-content select');
	JHtml::_('script', 'system/jquery.validate.js', false, true);
	
	//JHtml::_('behavior.modal', 'a.modal_jform_contenthistory');
	$params = $this->state->get('params');
	$currency = $this->currency;
	$quotation = $this->quotation;
	$paymentData = & $this->paymentData;
	$session = JFactory::getSession();
?>

<script type="text/javascript">
	function checkAgreementForm() {
		var agree1 = jQuery('#agree_1');
		if (agree1.prop('checked') == true) {
			$('agree_1_text').removeClass("error");
			} else {
			$('agree_1_text').addClass("error");
			jQuery('html, body').animate({
				'scrollTop': agree1.offset().top - 300
			}, 300);
			return false;
		}
		return true;
	}
	
	function submitPaymentForm() {
		var params = [
		{'name': '<?php echo $session->getFormToken(); ?>', 'value': "1"},
		];
		jQuery.ajax({
			data: params,
			url: 'index.php?option=com_insurediytravel&task=form.savebeforepayment',
			dataType: 'json',
			type: 'post',
			beforeSend: function () {
			},
			success: function (json) {
				if (json['success']) {
					jQuery("#paymentForm").submit();
				}
			}
		});
	}
	
	window.addEvent('load', function () {
		jQuery("#adminForm").validate({
			ignore: [] // <-- option so that hidden elements are validated
		});
		jQuery("#paymentForm").validate({
			ignore: [] // <-- option so that hidden elements are validated
		});
		var jForm = jQuery("form#paymentForm");
		jForm.submit(function () {
			jForm.find("label.error").each(function () {
				var jThis = jQuery(this);
				var jContainer = jThis.closest("div.control-group");
				var jControl = jContainer.find("div.controls");
				jControl.find(".error-container").append(jThis.clone());
				jThis.remove();
			});
			var errors = jForm.find("label.error");
			if (errors.length > 0) {
				var label = jForm.find("label.error:first");
				jQuery('html, body').animate({
					'scrollTop': label.offset().top - 400
				}, 300);
			}
		});
	})
</script>


<div class="insurediy-form">
	<div class="header-top-wrapper">
		<?php echo InsureDIYTravelHelper::renderHeader('icon-travel', JText::_('COM_INSUREDIYTRAVEL_PAGE_HEADING_CONFIRM_ORDER'), 4); ?>
	</div>
	<div style="padding:30px;margin-top: 58px;">
		<?php echo MyHelper::renderDefaultMessage(); ?>
		<div class="edit<?php echo $this->pageclass_sfx; ?>">
			<div class="whitebg radius-top border-all marginbottom20">
				<div class="header2bg">
					<div class="ico-cart">
						<?php echo JText::_("COM_INSUREDIYTRAVEL_PAGE_SUB_HEADING_APPLICATION_SUMMARY"); ?>
					</div>
				</div>
				<div class="padding20">
					<!-- must display quotation plans I choose on previous page -->
					<table class="insurediy-order-summary-confirm-page">
						<tr>
							<th class="column1"><?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_PROVIDER_LABEL"); ?></th>
							<th class="column2"><?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_DESCRIPTION_LABEL"); ?></th>
							<th class="column3"><?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_TOTAL_PREMIUM_LABEL"); ?></th>
						</tr>
						<tr>
							<td class="column1"><?php echo $this->my_plan->company_name ?></td>
							<td class="column2"><?php echo $this->my_plan->plan_name ?></td>
							<td class="column3"><?php echo $currency; ?> <?php echo number_format($this->my_plan->premium, 2) ?></td>
						</tr>
						<tr>
							<td colspan="2" align="right"><b><?php echo JText::_("COM_INSUREDIYTRAVEL_TOTAL"); ?></b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
							<td class="column3"><?php echo $currency; ?>  <?php echo number_format($this->my_plan->premium, 2) ?></td>
						</tr>
					</table>
				</div>
			</div>
			
			<div style="margin-bottom:20px;">
				<?php echo MyHelper::load_module_pos(InsureDIYTravelHelper::getAppSummeryMod()); ?>
			</div>
			
			<div class="whitebg radius-top border-all marginbottom20">
				<div class="header2bg">
					<div class="ico-contact">
						<?php echo JText::_("COM_INSUREDIYTRAVEL_PAGE_HEADING_CONTACT_DETAILS"); ?>
					</div>
				</div>
				<div class="padding20">
					<div class="insurediy-contact-detail-confirm-order">
						<div class="left">
							<div class="label"><?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_CONTACT_FIRSTNAME_LABEL"); ?></div>
							<div class="field"><?php echo ($this->my_detail->contact_firstname) ? $this->my_detail->contact_firstname : JText::_("JNA"); ?></div>
							<div class="clear"></div>
							<div class="label"><?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_CONTACT_LASTNAME_LABEL"); ?></div>
							<div class="field"><?php echo ($this->my_detail->contact_lastname) ? $this->my_detail->contact_lastname : JText::_("JNA"); ?></div>
							<div class="clear"></div>
							<div class="label"><?php echo JText::_("COM_INSUREDIYTRAVEL_ADDRESS"); ?></div>
							<div class="field">
								<?php echo $this->complete_address; ?>
							</div>
							<div class="clear"></div>
							<div class="label"><?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_CONTACT_POSTALCODE_LABEL"); ?></div>
							<div class="field"><?php echo ($this->my_detail->contact_postalcode) ? $this->my_detail->contact_postalcode : JText::_("JNA"); ?></div>
							<div class="clear"></div>
						</div>
						<div class="right">
							<div class="label2"><?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_CONTACT_COUNTRY_LABEL"); ?></div>
							<div class="field2"><?php echo ($this->my_detail->contact_country) ? $this->my_detail->contact_country : JText::_("JNA"); ?></div>
							<div class="clear"></div>
							<div class="label2"><?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_HANDPHONE_NUMBER_LABEL"); ?></div>
							<div class="field2"><?php echo ($this->my_detail->contact_contact_no) ? $this->my_detail->contact_country_code . ' ' . $this->my_detail->contact_contact_no : JText::_("JNA"); ?></div>
							<div class="clear"></div>
							<div class="label2"><?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_ID_OR_PASSPORT_LABEL"); ?></div>
							<div class="field2"><?php echo ($this->my_detail->contact_identity_no) ? $this->my_detail->contact_identity_no : JText::_("JNA"); ?></div>
							<div class="clear"></div>
						</div>
						<div class="clear"></div>
					</div>
					
				</div>
			</div>
			<form action="<?php echo $paymentData->action; ?>"  method="post" name="insurediyPaymentForm" id="paymentForm" class="form-validate form-vertical">
				<div class="whitebg radius-top border-all marginbottom20">
					<div class="header2bg">
						<div class="ico-declarations"><?php echo JText::_("COM_INSUREDIYTRAVEL_DECLARATION_HEADER"); ?></div>
					</div>
					<div class="padding20 line-height20 color666666 marginbottom20">
						<!-- must display declarrations -->
						<div class="insurediy-declarations-confirm-order border-all padding20 marginbottom20">
							<?php echo MyHelper::load_module_pos(InsureDIYTravelHelper::getDeclarationsMod()); ?>
						</div>
						<div class="marginbottom30 color2f2f2f relative">
							<div class="control-group">
								<div class="controls" style="width: 100%;">
									<input type="checkbox" class="styled" required aria-required="true" name="agree" id="agree_1" value="1" checked="checked" />
									<label for="agree_1"  style="margin-left: 25px;" class="size14" id="agree_1_text"><?php echo JText::_("COM_INSUREDIYTRAVEL_DECLARATION_AGREE_1"); ?></label>
									<div class="error-container"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div style="float:left;margin:15px 0;">{modulepos insurediy-secured-ssl}</div>
				<div class="btn-toolbar" style="float:right">
					<div>
						<button style="margin-right:10px;" type="button" onclick="javascript:document.adminForm.task.value = 'form.back';
						document.adminForm.submit();" class="btn btn-primary validate"><?php echo JText::_('BTN_BACK') ?></button>
						<button type="button" onclick="submitPaymentForm()" class="btn btn-primary validate"><?php echo JText::_('JCONTINUE') ?></button>
					</div>
				</div>
				
				<input type="hidden" name="merchantId" value="<?php echo $paymentData->merchantId; ?>">
				<input type="hidden" name="amount" value="<?php echo $paymentData->amount; ?>" >
				<input type="hidden" name="orderRef" value="<?php echo $paymentData->orderRef; ?>">
				<input type="hidden" name="currCode" value="<?php echo $paymentData->currCode; ?>" >
				<input type="hidden" name="mpsMode" value="<?php echo $paymentData->mpsMode; ?>" >
				<input type="hidden" name="successUrl" value="<?php echo $paymentData->successUrl; ?>">
				<input type="hidden" name="failUrl" value="<?php echo $paymentData->failUrl; ?>">
				<input type="hidden" name="cancelUrl" value="<?php echo $paymentData->cancelUrl; ?>">
				<input type="hidden" name="payType" value="<?php echo $paymentData->payType; ?>">
				<input type="hidden" name="lang" value="<?php echo $paymentData->lang; ?>">
				<input type="hidden" name="payMethod" value="<?php echo $paymentData->payMethod; ?>">
				<input type="hidden" name="remark" value="<?php echo $paymentData->remark; ?>">
				<input type="hidden" name="templateId" value="1">
			</form>
			<form action="<?php echo JRoute::_('index.php?option=com_insurediytravel&view=form'); ?>" method="post" name="adminForm" id="adminForm">
				<input type="hidden" name="task" value="" />
				<input type="hidden" name="quotation_id" value="<?php echo $quotation['id']; ?>" />
				<?php echo JHtml::_('form.token'); ?>
			</form>
			<div class="clear"></div>
		</div>
	</div>
</div>
<div class="logo-slider"><?php echo MyHelper::load_module(102); ?></div>

<?php /* Popup Box */ ?>
<div class="insurediy-popup" id="insurediy-popup-box" style="display:none;">
	<div class="header">
		<div class="ico-warning">
			<div class="text-header2"><?php echo JText::_("POP_IMPORTANT_NOTICE"); ?></div>
		</div>
		<a class="close" href="javascript:cleanInsureDIYPopup('insurediy-popup-box');">&nbsp;</a>
		<div class="clear"></div>
	</div>
	<div class="padding">
		<div class="marginbottom20">
			<?php echo JText::_("COM_INSUREDIYTRAVEL_WARNING_INSURANCE_REPLACEMENT"); ?>
		</div>
		<div style="text-align:right;">
			<input type="button" class="btn btn-primary" style="height:30px;padding:5px 20px;font-size:14px !important;" onclick="javascript:cleanInsureDIYPopup('insurediy-popup-box');
			return false;" value="Ok" />
		</div>
	</div>
</div>

<script>
	dataLayer.push({
		'event': 'checkoutOption',
		'ecommerce': {
			'checkout': {
				'actionField': {'step': 3}
			}
		}
	});
</script>
