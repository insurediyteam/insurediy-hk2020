<?php
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');
JHtml::_('bootstrap.tooltip');
JHtml::_('script', 'system/jquery.validate.js', false, true);

$params = $this->state->get('params');
$form = $this->form;
$quotation = $this->quotation;
$travellers = $quotation['travellers'];
$loadTravellers = !empty($travellers);

// remarketing email
$user = $this->user;
$products = [];
$products[0]["premium"] = $quotation["selected_plan"]->premium;
$products[0]["code"] = $quotation["selected_plan"]->insurer_code;

$mailQueue = (object)array(
	"email" => $user->email,
	"products" => $products,
	"type" => "travel-after",
	"parent" => "travel"
);

RemarketingHelpersRemarketing::newRemarketing($mailQueue);
// end of remarketing email
?>

<div class="insurediy-form">
	<div class="header-top-wrapper">
		<div class="header-top">
			<?php echo InsureDIYTravelHelper::renderHeader('icon-travel', JText::_('COM_INSUREDIYTRAVEL_PAGE_HEADING_YOUR_TRIP'), 3); ?>
		</div>
	</div>
	<div class="edit<?php echo $this->pageclass_sfx; ?> insurediy-form-content">
		<?php echo MyHelper::renderDefaultMessage(); ?>
		<form action="<?php echo JRoute::_('index.php?option=com_insurediytravel'); ?>" method="post" name="adminForm" id="adminForm" novalidate class="form-validate form-vertical">
			<div id="fields-holder">
				<?php
				for ($i = 0; $i < $this->no_of_travellers; $i++):
					if ($i) {
						$form->setFormControl("jform" . ($i + 1));
					}
					if ($loadTravellers) {
						$form->setValue("firstname", "", $travellers[$i]->firstname);
						$form->setValue("lastname", "", $travellers[$i]->lastname);
						$form->setValue("gender", "", $travellers[$i]->gender);
						$form->setValue("id_no", "", $travellers[$i]->id_no);
						$form->setValue("relation", "", $travellers[$i]->relation);
						$form->setValue("dob", "", $travellers[$i]->dob);
					}
					?>
					<fieldset id="fields" class="insurediy-form-fields">
						<legend>
							<?php echo JText::_("COM_INSUREDIYTRAVEL_TEXT_TRAVELLER") . " " . ($i + 1); ?>
						</legend>
						<div class="control-row">
							<div class="span4">
								<div class="control-group">
									<div class="control-label"><?php echo $form->getLabel('firstname'); ?></div>
									<div class="controls"><?php echo $form->getInput('firstname'); ?>
										<div class="error-container"></div>
									</div>
								</div>
								<div class="clear"></div>
							</div>
							<div class="span4" style="margin:0px;">
								<div class="control-group">
									<div class="control-label"><?php echo $form->getLabel('lastname'); ?></div>
									<div class="controls"><?php echo $form->getInput('lastname'); ?>
										<div class="error-container"></div>
									</div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
						<div class="control-row">
							<div class="span4">
								<div class="control-label"><?php echo $this->form->getLabel('gender'); ?>
									<div id="error-container"></div>
								</div>
								<div class="controls"><?php echo $this->form->getInput('gender'); ?>
									<div class="error-container"></div>
								</div>
								<div class="clear"></div>
							</div>
							<div class="span4" style="margin:0px;">
								<div class="control-group">
									<div class="control-label"><?php echo $this->form->getLabel('id_no'); ?></div>
									<div class="controls"><?php echo $this->form->getInput('id_no'); ?>
										<div class="error-container"></div>
									</div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
						<div class="control-row">
							<div class="span4">
								<div class="control-label"><?php echo $this->form->getLabel('relation'); ?>
									<div id="error-container"></div>
								</div>
								<div class="controls"><?php echo $this->form->getInput('relation'); ?>
									<div class="error-container"></div>
								</div>
								<div class="clear"></div>
							</div>
							<div class="span4" style="margin:0px;">
								<div class="control-group">
									<div class="control-label"><?php echo $this->form->getLabel('dob'); ?></div>
									<div class="controls"><?php echo $this->form->getInput('dob'); ?>
										<div class="error-container"></div>
									</div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
						<div class="clear"></div>
					</fieldset>
				<?php endfor; ?>
			</div>
			<div class="clear"></div>
			<input type="hidden" name="task" value="form.step3save" />
			<input type="hidden" name="quotation_id" value="<?php echo $quotation['id']; ?>" />

			<?php echo JHtml::_('form.token'); ?>
			<div style="float:left;margin:15px 0;">
				<div>{modulepos insurediy-secured-ssl}</div>
			</div>
			<div class="btn-toolbar" style="float:right">
				<div>
					<button style="margin-right:10px;" type="button" onclick="javascript:document.adminForm.task.value = 'form.back';
							document.adminForm.submit();" class="btn btn-primary validate"><?php echo JText::_('BTN_BACK') ?></button>
					<button type="submit" class="btn btn-primary validate" ><?php echo JText::_('JCONTINUE') ?></button>
				</div>
			</div>
			<div class="clear"></div>
		</form>
	</div>
</div>

<script>
	jQuery(document).ready(function () {
		jQuery("#adminForm").validate({
			ignore: [] // <-- option so that hidden elements are validated
		});
		var jForm = jQuery("form#adminForm");
		jForm.submit(function () {
			jForm.find("label.error").each(function () {
				var jThis = jQuery(this);
				var jContainer = jThis.closest("div.control-group");
				var jControl = jContainer.find("div.controls");
				jControl.find(".error-container").append(jThis.clone());
				jThis.remove();
			});
			var errors = jForm.find("label.error");
			if (errors.length > 0) {
				var label = jForm.find("label.error:first");
				jQuery('html, body').animate({
					'scrollTop': label.offset().top - 400
				}, 300);
			}
		});
	});
</script>