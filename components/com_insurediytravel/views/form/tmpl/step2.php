<?php
	defined('_JEXEC') or die;
	
	JHtml::_('behavior.keepalive');
	JHtml::_('behavior.formvalidation');
	JHtml::_('bootstrap.tooltip');
	
	$params = $this->state->get('params');
	$quotation = $this->quotation;
	
	//$this->policy_plans[] = $this->policy_plans[0];
	$total_plans = count($this->policy_plans);
	$btn_font_size = "";
	if ($total_plans == 5) {
		$btn_font_size = "font-size: 12px!important;";
		} elseif ($total_plans > 5) {
		$btn_font_size = "font-size: 12px!important; padding: 0 12px;";
	}
	$width = ($total_plans) ? 670 / $total_plans : 670;

	// remarketing email
	$user = $this->user;
	$products = [];
	$pol = [];
	foreach ($this->policy_plans as $key => $value) {
		$pol["premium"] = $value->premium;
		$pol["code"] = $value->insurer_code;
		array_push($products, $pol);
	}
	$mailQueue = (object)array(
		"email" => $user->email,
		"products" => $products,
		"type" => "travel-before",
		"parent" => "travel"
	);
	RemarketingHelpersRemarketing::newRemarketing($mailQueue);
	//end of remarketing email
?>

<script type="text/javascript">
	
	var checkboxHeight = "33";
	var radioHeight = "18";
	var selectWidth = "190";
	
	function chooseYourPlans() {
		var has_chosen = 0;
		
		$$('input[type="checkbox"]:checked').each(function (elem) {
			has_chosen = 1;
		});
		
		if (has_chosen == 0) {
			$('insurediy-popup-box').setStyle('display', 'block');
			} else {
			document.choosePlanForm.submit();
		}
	}
</script>

<script>
	//Measure a view of product details. This example assumes the detail view occurson pageload,
	//and also tracks a standard pageview of the details page.
	dataLayer.push({
		'ecommerce': {
			'impressions': [<?php $position = 1; foreach ($this->policy_plans as $key =>
			$policy) :?>
			{
				'name': '<?php echo $policy->plan_name; ?>', //Name or ID is required.
				'id': '<?php echo $policy->plan_index_code; ?>',
				'price': '<?php echo $policy->premium; ?>',
				'brand': '<?php echo $policy->insurer_code; ?>',
				'category': 'Travel',
				'list': 'Travel Quotation',
				'variant': '<?php echo $policy->trip_type; ?>',
				'position': <?php echo $position; ?>
			},
			<?php $position++; endforeach; ?>]
		}
	});
	//Measure adding a product to a shopping cart by using an 'add' actionFieldObject
	//and a list of productFieldObjects.
	function addToCart(productObj) {
		dataLayer.push({
			'event': 'productClick',
			'ecommerce': {
				'click': {
					'actionField': {'list': 'Travel Quotation'}, // Optional list property.
					'products': [{
						'name': productObj.plan_name,
						'id': productObj.plan_index_code,
						'price': productObj.premium,
						'brand': productObj.insurer_code,
						'category': 'Travel',
						'variant': productObj.trip_type
					}]
				}
			}
		});
		dataLayer.push({
			'event': 'addToCart',
			'ecommerce': {
				'currencyCode': 'SGD',
				'add': { // 'add' actionFieldObject measures.
					'products': [{ // adding a product to a shopping cart.
						'name': productObj.plan_name,
						'id': productObj.plan_index_code,
						'price': productObj.premium,
						'brand': productObj.insurer_code,
						'category': 'Travel',
						'variant': productObj.trip_type,
						'quantity': 1
					}]
				}
			}
		});
	}	
</script>

<div class="insurediy-form">
	<div class="header-top-wrapper">
		<?php echo InsureDIYTravelHelper::renderHeader('icon-travel', JText::_('COM_INSUREDIYTRAVEL_PAGE_HEADING_YOUR_TRIP'), 2); ?>
	</div>
	<div class="insurediy-form-content">
		<?php echo MyHelper::renderDefaultMessage(); ?>
		<div class="edit<?php echo $this->pageclass_sfx; ?>">
			<?php if ($this->isST): ?>
			<div id="show-annual-price">
				<div class="show-annual-price-left">
					
				</div>
				<div class="show-annual-price-mid">
					<div class="show-annual-price-text">
						<?php echo JText::_("COM_INSUREDIYTRAVEL_ANNUAL_PRICE_NOTIFICATION"); ?>
					</div>
					<div class="show-annual-price-button">
						<form action="index.php">
							<input type="hidden" name="task" value="form.changetoanuual"/>
							<input type="hidden" name="option" value="com_insurediytravel"/>
							<input type="submit" name="submit" class="btn" value="<?php echo JText::_("COM_INSUREDIYTRAVEL_BUTTONS_SHOW_ANNUAL_PRICES"); ?>"/>
						</form>
					</div>
				</div>
				<div class="show-annual-price-right">
					
				</div>
				<div class="clear"></div>
			</div>
			<?php endif; ?>
			<form action="<?php echo JRoute::_('index.php?option=com_insurediytravel'); ?>" method="post" name="choosePlanForm" id="choosePlanForm" class="form-validate form-vertical">
				<?php if (!empty($this->policy_plans)) : ?>
				<table class="insurediy-table-plans">
					<tr>
						<td class="td-first">
							<div class="ico-quotation-mark" style="padding-left: 30px;"><?php echo JText::_("COM_INSUREDIYTRAVEL_YOUR_QUOTATIONS"); ?></div>
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner" style="position: relative;width: <?php echo $width ?>px;">
							<?php if ($policy->logo) : ?>
							<div class="center">
								<img src="<?php echo $policy->logo ?>" />
							</div>
							<?php endif; ?>
							<?php if (isset($policy->flag_banner) && strlen($policy->flag_banner) > 0): ?>
							<div style="position:absolute;top:0;"><img src="<?php echo $this->flag_banner_path . '/' . $policy->flag_banner ?>" /></div>
							<?php endif; ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<?php if (FALSE): ?>
					<tr>
						<td class="td-first"><?php echo JText::_("COM_INSUREDIYTRAVEL_PLANS"); ?></td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<div class="center" style="font-family:montserrat-regular;font-size:14px;"><?php echo $policy->plan_name; ?></div>
						</td>
						<?php endforeach; ?>
					</tr>
					<?php endif; ?>
					<tr>
						<td class="td-first premuim-text" >
							<?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_PREMIUM_TITLE"); ?> (<?php echo $this->currency; ?>) <?php echo InsureDIYHelper::generateQuestionMark("COM_INSUREDIYTRAVEL_FIELD_PREMIUM_DESC"); ?>
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner premuim-text">
							<?php echo MyHelper::numberFormat($policy->premium); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td class="td-first">
							<?php echo JText::_("COM_INSUREDIYTRAVEL_PLAN_DIY_POINTS_TITLE"); ?> <?php echo InsureDIYHelper::generateQuestionMark("COM_INSUREDIYTRAVEL_PLAN_DIY_POINTS_DESC"); ?>
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo MyHelper::numberFormat($policy->pur_points); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td class="td-first">
							<?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_MEDICAL_TITLE"); ?> <?php echo InsureDIYHelper::generateQuestionMark("COM_INSUREDIYTRAVEL_FIELD_TERMINAL_ILLNESS_DESC"); ?>
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->medical_expenses); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td class="td-first" >
							<?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_EMERGENCY_TITLE"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_GUARANTEED_DESC"); ?>">
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->emergency_me); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					
					<tr>
						<td class="td-first" >
							<?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_PERSONAL_ACCIDENT_TITLE"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_OTHER_BENEFITS_DESC"); ?>">
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->personal_accident); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td class="td-first">
							<?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_PERSONAL_PROPERTY_TITLE"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_PERSONAL_PROPERTY_TOOLTIPS"); ?>">
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->personal_property); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td class="td-first">
							<?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_BAGGAGE_DELAY_TITLE"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_BAGGAGE_DELAY_TOOLTIPS"); ?>">
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->baggage_delay); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td class="td-first" >
							<?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_TRIP_INTERRUPTION_TITLE"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_TRIP_INTERRUPTION_TOOLTIPS"); ?>">
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->interruption); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td class="td-first" >
							<?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_TRIP_CANCELLATION_TITLE"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_TRIP_CANCELLATION_TOOLTIPS") ?>">
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->cancellation); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					
					<tr>
						<td class="td-first" >
							<?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_TRIP_REROUTING_TITLE"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_TRIP_REROUTING_TOOLTIPS"); ?>">
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->trip_rerouting); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td class="td-first">
							<?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_RENTAL_VEHICLE_EXCESS_TITLE"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_RENTAL_VEHICLE_EXCESS_TOOLTIPS"); ?>">
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->rental_vehicle); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td class="td-first">
							<?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_PERSONAL_LIABILITY_TITLE"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_PERSONAL_LIABILITY_TOOLTIPS"); ?>">
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->liability); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td class="td-first" >
							<?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_AMATEUR_SPORT_TITLE"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_AMATEUR_SPORT_TOOLTIPS"); ?>">
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->amateur_sport); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td class="td-first">
							<?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_OTHER_BENEFIT_TITLE"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_OTHER_BENEFIT_TOOLTIPS"); ?>">
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->other_benefits); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td class="td-first" >
							<?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_PRODUCT_BROCHURE_TITLE"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_PRODUCT_BROCHURE_TOOLTIPS"); ?>">
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo ($policy->tnc) ? '<a href="' . $policy->tnc . '" target="_blank"><img src="templates/protostar/images/icon/ico-downloadfile.png" /></a>' : 'N/A' ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td class="td-first">
							<?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_PURCHASE_TITLE"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYTRAVEL_FIELD_PURCHASE_TOOLTIPS"); ?>">
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<button class="btn btn-primary btn-rounded" onclick="addToCart({plan_name:'<?php echo $policy->plan_name; ?>', plan_index_code:'<?php echo $policy->plan_index_code; ?>', premium:'<?php echo $policy->premium; ?>', insurer_code:'<?php echo $policy->insurer_code; ?>', trip_type:'<?php echo $policy->trip_type; ?>'})" style="<?php echo $btn_font_size; ?>" type="submit" name="plan_id" value="<?php echo $policy->plan_id; ?>"><?php echo JText::_("BTN_BUY_NOW"); ?></button>
						</td>
						<?php endforeach; ?>
					</tr>
				</table>
				<?php else : ?>
				<div><?php echo JText::_("COM_INSUREDIYTRAVEL_NO_PLANS_MSG"); ?></div>
				<?php endif; ?>
				<input type="hidden" name="task" value="form.step2save" />
				<input type="hidden" name="quotation_id" value="<?php echo $quotation['id']; ?>" />
				<?php echo JHtml::_('form.token'); ?>
				
				<div style="float:left;margin:15px 0;">
					<div>{modulepos insurediy-download-adobe}</div>
					<div>{modulepos insurediy-secured-ssl}</div>
				</div>
				<div class="btn-toolbar" style="float:right">
					<button style="margin-right:10px;" type="button" onclick="javascript:document.choosePlanForm.task.value = 'form.back';
					document.choosePlanForm.submit();" class="btn btn-primary validate"><?php echo JText::_('BTN_BACK') ?></button>
				</div>
				<div class="clear"></div>
			</form>
		</div>
		
	</div>
</div>

<?php /* Popup Box */ ?>
<div class="insurediy-popup" id="insurediy-popup-box" style="display:none;">
	<div class="header">
		<div class="text-header"><?php echo JText::_("POP_IMPORTANT_NOTICE"); ?></div>
		<a class="close" href="javascript:cleanInsureDIYPopup('insurediy-popup-box');">&nbsp;</a>
		<div class="clear"></div>
	</div>
	<div class="padding">
		<div><?php echo JText::_("COM_INSUREDIYTRAVEL_AT_LEAST_1_PLAN_MSG"); ?></div>
		<div style="text-align:right;"><input type="button" class="btn btn-primary" style="height:30px;padding:5px 20px;font-size:14px !important;" onclick="javascript:cleanInsureDIYPopup('insurediy-popup-box');
		return false;" value="Ok" /></div>
	</div>
</div>
