<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_insurediytravel
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * HTML Article View class for the component
 *
 * @package     Joomla.Site
 * @subpackage  com_insurediytravel
 * @since       1.5
 */
class InsureDIYTravelViewForm extends JViewLegacy {

	protected $form;
	protected $item;
	protected $state;

	public function display($tpl = null) {
		$session = JFactory::getSession();
		$model = $this->getModel();
		$params = JComponentHelper::getParams('com_insurediytravel');
		$user = JFactory::getUser();
		$form = $this->get("Form");
		$quotation = InsureDIYTravelHelper::getQuotation();
		$quotation_id = InsureDIYTravelHelper::getQid();
		$currentUser = MyHelper::getCurrentUser();

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseWarning(500, implode("\n", $errors));
			return false;
		}

		$layout = InsureDIYTravelHelper::getLayout();
		//Re Generate Unique Order No
		$db = JFactory::getDBO();
		$dataobj = new stdClass();
		$dataobj->id = $quotation['id'];
		$dataobj->unique_order_no = InsureDIYHelper::getUniqueOrderNo($user->id);
		switch ($layout) {
			case 'step1':

				break;
			case 'step2':
				$this->policy_plans = $this->get('PolicyPlans');
				$trip_type = $model->getTripType($quotation_id);
				$this->isST = ($trip_type && $trip_type == "ST");
				break;
			case 'step3':
				$this->isJM = ($quotation['group_type'] && $quotation['group_type'] == "JM");
				$this->no_of_travellers = $quotation['no_of_travellers'];
				break;
			case 'step4':
				// FB and referral stuffs
				$referred_by = $session->get(SESSION_KEY_REFID, FALSE);
				if (!$currentUser->referred_by && $referred_by) {
					$form->setValue('referred_by', "", $referred_by);
				}
				break;
			case 'step5':
				$db->updateObject('#__insure_travel_quotations', $dataobj, "id");
				$this->my_plan = $this->get('MyPlans');
				$this->my_detail = $this->get('MyInfo');
				$this->paymentData = $this->get('PDPayment');
				$this->complete_address = InsureDIYTravelHelper::getCompleteAddress($quotation);
				break;
			default:
				break;
		}

		$this->quotation = $quotation;
		$this->setLayout($layout);
		$this->form = $form;
		$this->state = $this->get('State');
		$this->currency = $params->get("currency", "HK$");
		$this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));
		$this->params = $params;
		$this->user = $user;
		$this->current_user = $currentUser;
		$this->flag_banner_path = 'images/flag_banners';
		$this->_prepareDocument();
		
		switch ($layout) {
			case "thankyou":
			InsureDIYTravelHelper::clearSessionData();
			break;
		}
		
		parent::display($tpl);
	}

	/**
	 * Prepares the document
	 */
	protected function _prepareDocument() {
		$app = JFactory::getApplication();
		$menus = $app->getMenu();
		$title = null;

		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();

		if (empty($this->item->id)) {
			$head = JText::_('COM_INSUREDIYTRAVEL_FORM_PAGE_HEADING');
		} else {
			$head = JText::_('COM_INSUREDIYTRAVEL_FORM_EDIT_PAGE_HEADING');
		}

		if ($menu) {
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		} else {
			$this->params->def('page_heading', $head);
		}

		$title = $this->params->def('page_title', $head);
		if ($app->getCfg('sitename_pagetitles', 0) == 1) {
			$title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
		} elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
			$title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
		}
		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description')) {
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords')) {
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots')) {
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}

		//facebook stuffs
		$this->document->setMetaData("og:title", InsureDIYTravelHelper::getFbTitle());
		$this->document->setMetaData("og:description", InsureDIYTravelHelper::getFbDesc());
		$this->document->setMetaData("og:image", InsureDIYTravelHelper::getFbImage());
		$this->document->setMetaData("og:app_id", InsureDIYTravelHelper::getFbAppId());
		$this->document->setMetaData("og:site_name", InsureDIYTravelHelper::getFbSiteName());
	}

	protected function getComparisonTbRows() {
		$rows = array(
		);
		return $rows;
	}

}
