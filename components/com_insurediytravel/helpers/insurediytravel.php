<?php

defined('_JEXEC') or die;

class InsureDIYTravelHelper {

	public static function renderHeader($icon, $header, $active) {
		$classes = array();
		switch ($active) {
			case 1:
				$classes = array('-active', '', '', '-inactive', '', '-inactive', '', '');
				break;
			case 2:
				$classes = array('-active', '-active', '-active', '', '', '-inactive', '', '');
				break;
			case 3:
				$classes = array('-active', '-active', '-active', '-active', '-active', '', '', '');
				break;
			case 4:
				$classes = array('-active', '-active', '-active', '-active', '-active', '-active', '-active', '-active');
				break;
			default:
				break;
		}

		$buffer = '<div class="header-top">';
		$buffer.= '<div class="h1-heading">';
		$buffer.= '<i class="' . $icon . '" ></i>';
		$buffer.= $header;
		$buffer.='</div>';
		$buffer.= '<div class="insurediy-breadcrumbs">';
		$buffer.= '<div class="mybreadcrumb back"></div>';
		$buffer.= '<div class="mybreadcrumb middle' . $classes[0] . '">';
		$buffer.= JText::_("COM_INSUREDIYTRAVEL_BREADCRUMB_YOUR_DETAILS") . '</div>';
		$buffer.='<div class="mybreadcrumb between' . $classes[1] . '">';
		$buffer.= '</div><div class = "mybreadcrumb middle' . $classes[2] . '">';
		$buffer.= JText::_("COM_INSUREDIYTRAVEL_BREADCRUMB_SELECT_PRODUCT");
		$buffer.='</div>';
		$buffer.='<div class="mybreadcrumb between' . $classes[3] . '">';
		$buffer.='</div>';
		$buffer.='<div class="mybreadcrumb middle' . $classes[4] . '">';
		$buffer.= JText::_("COM_INSUREDIYTRAVEL_BREADCRUMB_APPLY") . '</div>';
		$buffer.='<div class="mybreadcrumb between' . $classes[5] . '">';
		$buffer.='</div>';
		$buffer.='<div class="mybreadcrumb middle' . $classes[6] . '">';
		$buffer.= JText::_("COM_INSUREDIYTRAVEL_BREADCRUMB_PAYMENT") . '</div>';
		$buffer.='<div class="mybreadcrumb front' . $classes[7] . '"></div>';
		$buffer.='<div class="clear"></div>';
		$buffer.= '</div>';
		$buffer.= '<div class="clear"></div></div>';
		return $buffer;
	}

	// PARAMS
	public static function getAppSummeryMod($default = "") {
		$params = JComponentHelper::getParams("com_insurediytravel");
		return $params->get("app_summery", $default);
	}

	public static function getDeclarationsMod($default = "") {
		$params = JComponentHelper::getParams("com_insurediytravel");
		return $params->get("declarations", $default);
	}

	public static function getCurrency($default = "HK$") {
		$params = JComponentHelper::getParams("com_insurediytravel");
		return $params->get("currency", $default);
	}

	public static function getFbTitle($default = "") {
		$params = JComponentHelper::getParams("com_insurediytravel");
		return $params->get("fbtitle", $default);
	}

	public static function getFbDesc($default = "") {
		$params = JComponentHelper::getParams("com_insurediytravel");
		return $params->get("fbdesc", $default);
	}

	public static function getFbImage($default = "") {
		$params = JComponentHelper::getParams("com_insurediytravel");
		return $params->get("fbimage", $default);
	}

	public static function getFbSiteName($default = FALSE) {
		$default = ($default) ? $default : JFactory::getConfig()->get("sitename", "");
		$params = JComponentHelper::getParams("com_insurediytravel");
		return $params->get("fbsitename", $default);
	}

	public static function getFbAppId($default = "") {
		$params = JComponentHelper::getParams("com_insurediytravel");
		return $params->get("fbappid", $default);
	}

	public static function getRefExp($default = "") {
//		$params = JComponentHelper::getParams("com_insurediytravel");
//		return $params->get("refExp", $default);
		return JText::_("COM_INSUREDIYTRAVEL_REFERRAL_EXPLANATION");
	}

	public static function getRefMsg($default = "") {
//		$params = JComponentHelper::getParams("com_insurediytravel");
//		return str_replace("XXYY", JFactory::getUser()->referral_id, $params->get("refMsg", $default));
		return str_replace("XXYY", JFactory::getUser()->referral_id, JText::_("COM_INSUREDIYTRAVEL_REFERRAL_MESSAGE"));
	}

	// QUOTATION STUFFS
	public static function isOldQuote() {
		return self::getQid();
	}

	public static function getCurrentQid() {
		return JFactory::getSession()->get("travel.quotation_id", 0);
	}

	public static function getInputQid() {
		return JFactory::getApplication()->input->get("quotation_id", 0, "integer");
	}

	public static function getQid() {
		return self::getInputQid() ? self::getInputQid() : self::getCurrentQid();
	}

	public static function canViewLayout($layout) {
		$user = JFactory::getUser();
		$stage = self::getStageFromLayout($layout);
		if ($stage > 1 && $user->id < 1) {
			return FALSE;
		}
		return $stage <= self::getCurrentStage(self::getQid());
	}

	public static function getLayout() {
		$user = JFactory::getUser();
		$app = JFactory::getApplication();

		$inputLayout = $app->input->get("layout", FALSE);
		$staticLayouts = array("error", "thankyou", "canceled", "syserror");
		if ($inputLayout && in_array($inputLayout, $staticLayouts)) {
			return $inputLayout;
		}

		$qid = self::getQid();
		if (!$qid) {
			return "step1";
		}
		$stage = self::getCurrentStage($qid);
		if ($stage > 1 && $user->id < 1) {
			return "login";
		}
		return self::getLayoutFromStage($stage);
	}

	public static function checkOwnership($id) {
		$user = JFactory::getUser();
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("count(*)")
				->from("#__insure_travel_quotations")
				->where("user_id = " . $db->quote($user->id))
				->where("id = " . $db->quote($id));
		$db->setQuery($query);
		return $db->loadResult();
	}

	public static function getCurrentStage($id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("quote_stage")
				->from("#__insure_travel_quotations")
				->where("id = " . $db->quote($id));
		$db->setQuery($query);
		return $db->loadResult();
	}

	public static function getLayoutFromStage($step) {
		$stageToLayouts = array(1 => "step1", 2 => "step2", 3 => "step3", 4 => "step4", 5 => "step5", 6 => "thankyou");
		return isset($stageToLayouts[$step]) ? $stageToLayouts[$step] : "step1";
	}

	public static function getStageFromLayout($layout) {
		$layoutToStages = array("step1" => 1, "login" => 2, "step2" => 2, "step3" => 3, "step4" => 4, "step5" => 5, "thankyou" => 6);
		return isset($layoutToStages[$layout]) ? $layoutToStages[$layout] : 0;
	}

	public static function clearSessionData() {
		$session = JFactory::getSession();
		$session->set("travel.data", NULL);
		$session->set("travel.quotation_id", NULL);
	}

	public static function overrideSessionWithQid($qid) {
		$session = JFactory::getSession();
		$session->set("travel.quotation_id", $qid);
		$session->set("travel.data", self::getQuotation($qid));
	}

	public static function getQuotation($qid = FALSE, $checkUser = TRUE) {
		if (!$qid) {
			$qid = self::getQid();
		}
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("q.*, u.gender ")
				->from("#__insure_travel_quotations AS q")
				->join('left','#__users AS u ON u.id = q.user_id ')
				->where("q.id = " . $db->quote($qid));

		if ($checkUser) {
			$query->where("user_id = " . $db->quote(JFactory::getUser()->id));
		}
		$quotation = $db->setQuery($query)->loadAssoc();
		if ($quotation) {
			$planQuery = $db->getQuery(TRUE)
					->select("*")
					->from("#__insure_travel_quotations_to_plans")
					->where("quotation_id = " . $db->quote($quotation['id']));
			$quotation['selected_plan'] = $db->setQuery($planQuery)->loadObject();

			$travellerQuery = $db->getQuery(TRUE)
					->select("*")
					->from("#__insure_travel_quotations_to_travellers")
					->where("quotation_id = " . $db->quote($quotation['id']));
			$travellers = $db->setQuery($travellerQuery)->loadObjectList();
			$quotation['travellers'] = ($travellers) ? $travellers : array();
		}
		return $quotation;
	}

	public static function getCompleteAddress($data) {
		$complete_address = '';
		$complete_address .= ($data['contact_room_no'] ? 'Flat ' . $data['contact_room_no'] : '');
		$complete_address .= ($data['contact_floor_no'] ? ', ' . $data['contact_floor_no'] . '/F' : '');
		$complete_address .= ($data['contact_block_no'] ? ', Blk ' . $data['contact_block_no'] : '');
		$complete_address .= ($data['contact_building_name'] ? ', ' . $data['contact_building_name'] : '');
		$complete_address .= ($data['contact_street_name'] ? ', ' . $data['contact_street_name'] : '');
		$complete_address .= ($data['contact_district_name'] ? ', ' . $data['contact_district_name'] : '');
		$complete_address .= ($data['contact_country'] ? ', ' . $data['contact_country'] . ' ' : '');
		return $complete_address;
	}

	// Call this before Loading the quotation.
	public static function checkQuotation($quotation) {
//		$lastCheck = $quotation['last_check_date'];
		if ($quotation['quote_stage'] <= 2) {
			return TRUE;
		}

//		$age_atm = MyHelper::getAge($quotation['dob']);
//		if ($lastCheck == "0000-00-00") {
//			self::updateLastCheck($quotation['id']);
//			$age_applied = MyHelper::getAge($quotation['dob']);
//		} else {
//			$age_applied = MyHelper::getAge($quotation['dob'], $lastCheck);
//		}
//		if ($age_atm != $age_applied) {
//			return FALSE;
//		}

		if (isset($quotation['selected_plans']) && is_array($quotation['selected_plans'])) {
			foreach ($quotation['selected_plans'] as $plan) {
				$db = JFactory::getDbo();
				$query = $db->getQuery(TRUE)
						->select("plan_index_code, price")
						->from("#__insure_travel_plans")
						->where("plan_index_code = " . $db->quote($plan->plan_index_code));
				$current_plan = $db->setQuery($query)->loadObject();
				if ($current_plan && isset($current_plan->price) && $current_plan->price != $plan->price) {
					return FALSE;
				}
			}
		}
		return TRUE;
	}

	public static function resetQuotation($quotation) {
		// clear selected quotation
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->delete("#__insure_travel_quotations_to_plans")
				->where("quotation_id = " . $db->quote($quotation['id']));
		$db->setQuery($query)->execute();

		// update to quotation table
		$today = JHtml::date(NULL, "Y-m-d");
		$query = $db->getQuery(TRUE)
				->update("#__insure_travel_quotations")
				->set("quote_stage = 2") // make them choose plans again
//				->set("last_check_date = " . $db->quote($today))
				->where("id = " . $db->quote($quotation['id']));
		$db->setQuery($query)->execute();
	}

	public static function updateLastCheck($qid) {
		$db = JFactory::getDbo();
		$today = JHtml::date(NULL, "Y-m-d");
		$query = $db->getQuery(TRUE)
				->update("#__insure_travel_quotations")
				->set("last_check_date = " . $db->quote($today))
				->where("id = " . $db->quote($qid));
		$db->setQuery($query)->execute();
	}

}
