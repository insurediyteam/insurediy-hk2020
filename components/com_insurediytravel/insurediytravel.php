<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_insurediytravel
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
JLoader::import('incs.form.field', JPATH_ROOT);

require_once JPATH_COMPONENT . '/helpers/insurediytravel.php';

JLoader::register('TravelQuotation', JPATH_COMPONENT . '/class/quotation.php');
new TravelQuotation();

$controller = JControllerLegacy::getInstance('InsureDIYTravel');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
