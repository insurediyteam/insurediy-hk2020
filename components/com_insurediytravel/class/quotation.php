<?php

/**
 * @package    Travel.Quotation
 *
 * @copyright  Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license    GNU General Public License version 2 or later; see LICENSE
 */
defined('JPATH_PLATFORM') or die;

/**
 * Static class to handle travel Quotation.
 *
 * @package  Travel.Quotation
 * @since    11.1
 */
class TravelQuotation {

	/**
	 * Container for already imported library paths.
	 *
	 * @var    array
	 * @since  11.1
	 */
	protected static $fields;
	protected static $resetFields = array();
	protected static $changedFields = array();

	/**
	 * Construct Method
	 *
	 * @return  array  The array of fields.
	 *
	 * @since   12.3
	 */
	public function __construct() {
		self::$fields = new stdClass();

		$session = JFactory::getSession();
		$inputQid = InsureDIYTravelHelper::getInputQid();
		$quotation = array();
		if ($inputQid) {
			$quotation = InsureDIYTravelHelper::getQuotation($inputQid);

			$session->set("travel.quotation_id", $inputQid);
			$session->set("travel.data", $quotation);
		} else {
			$sessQid = InsureDIYTravelHelper::getCurrentQid();
			$quotation = $session->get("travel.data", array());
			$currentUser = MyHelper::getCurrentUser();

			if (!$sessQid && isset($currentUser->id) && $currentUser->id > 0) { // New quote
				$quotation['dob'] = $currentUser->dob;
				$quotation['gender'] = $currentUser->gender;
				$quotation['monthly_income'] = $currentUser->monthly_salary;
				$quotation['occupation'] = $currentUser->occupation;
				$quotation['country_residence'] = $currentUser->country;
				$quotation['nationality'] = $currentUser->nationality;
				$quotation['marital_status'] = $currentUser->marital_status;
			}
		}

		self::$fields = $quotation;
	}

	/**
	 * Method to get the list of fields.
	 *
	 * @return  array  The array of fields.
	 *
	 * @since   12.3
	 */
	public static function getFields() {
		return self::$fields;
	}

	/**
	 * Method to check the validity of the quotation
	 *
	 * @return  boolean. 
	 *
	 * @since   12.3
	 */
	public static function checkValidity() {
		return TRUE;
	}

	/**
	 * Method to reset the quotation when it is no longer valid
	 *
	 * @return  boolean. 0 on fail, 1 on success;
	 *
	 * @since   12.3
	 */
	protected function resetQuotation() {
		return TRUE;
	}

	/**
	 * Method to update the quotation // quotation must initially be in database
	 *
	 * @return  boolean.
	 *
	 * @since   12.3
	 */
	public static function saveToDb($data, $changedFieldOnly = TRUE) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);

		
		$query->update("#__insure_travel_quotations");
		foreach ($data as $k => $v) {
			$query->set();
		}

		$query->where("id=" . $db->quote(self::$fields['id']));
		$db->setQuery($query);
		// save the data in fields to database
		return TRUE;
	}

	/**
	 * Method to update the quotation
	 *
	 * @return  boolean.
	 *
	 * @since   12.3
	 */
	public static function saveToSession() {
		$session = JFactory::getSession();
		// save the data in fields to session
		return TRUE;
	}

	/**
	 * Method to set key in fields
	 *
	 * @return  boolean.
	 *
	 * @since   12.3
	 */
	public static function setField($k, $v) {
		self::$fields[$k] = $v;
		self::$changedFields[$k] = $k;
	}

}
