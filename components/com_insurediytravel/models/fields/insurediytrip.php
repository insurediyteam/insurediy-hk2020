<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_banners
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('JPATH_BASE') or die;

/**
 * Impressions Field class for the Joomla Framework.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_banners
 * @since       1.6
 */
JLoader::import('incs.form.fields.customlabel', JPATH_ROOT);

class JFormFieldInsureDIYTrip extends JformFieldCustomLabel {

	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since   1.6
	 */
	protected $type = 'InsureDIYTrip';

	protected function getInput() {

		$value = empty($this->value) ? $this->default : $this->value;
		$checked = ' checked="checked" ';
		
		return '<div class="insurediy-custom-radio">'
				. '<div class="insurediy-custom-radio-single-trip"><input type="radio" id="insurediy-single-trip" ' . (($value == 'ST') ? $checked : '') . ' value="ST" name="' . $this->name . '" /><label for="insurediy-single-trip">&nbsp;</label></div>'
				. '<div class="insurediy-custom-radio-annual-trip"><input type="radio" id="insurediy-annual-trip" ' . (($value == 'AT') ? $checked : '') . ' value="AT" name="' . $this->name . '" /><label for="insurediy-annual-trip">&nbsp;</label></div>'
				. '<div style="clear:both"></div>'
				. '</div>';
	}

}
