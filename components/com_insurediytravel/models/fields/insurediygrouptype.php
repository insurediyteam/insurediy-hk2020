<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_banners
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('JPATH_BASE') or die;

/**
 * Impressions Field class for the Joomla Framework.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_banners
 * @since       1.6
 */
JLoader::import('incs.form.fields.customlabel', JPATH_ROOT);

class JFormFieldInsureDIYGroupType extends JformFieldCustomLabel {

	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since   1.6
	 */
	protected $type = 'InsureDIYGroupType';

	protected function getInput() {

		$value = empty($this->value) ? $this->default : $this->value;
		$checked = ' checked="checked" ';

		return '<div class="insurediy-custom-radio">'
				. '<div class="insurediy-custom-radio-just-me"><input type="radio" id="insurediy-just-me" ' . (($value == 'JM') ? $checked : '') . ' value="JM" name="' . $this->name . '" /><label for="insurediy-just-me">&nbsp;</label></div>'
				. '<div class="insurediy-custom-radio-with-group"><input type="radio" id="insurediy-with-group" ' . (($value == 'WG') ? $checked : '') . ' value="WG" name="' . $this->name . '" /><label for="insurediy-with-group">&nbsp;</label></div>'
				. '<div class="insurediy-custom-radio-with-family"><input type="radio" id="insurediy-with-family" ' . (($value == 'WF') ? $checked : '') . ' value="WF" name="' . $this->name . '" /><label for="insurediy-with-family">&nbsp;</label></div>'
				. '<div style="clear:both"></div>'
				. '</div>';
	}

}
