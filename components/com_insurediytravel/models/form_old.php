<?php

defined('_JEXEC') or die;

JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . '/tables');

class InsureDIYTravelModelForm extends JModelForm {

	protected $_context = 'com_insurediytravel.quotation';
	protected $_tb_plan = "#__insure_travel_plans";
	protected $_tb_company = "#__insure_companies";
	protected $_tb_quotation = "#__insure_travel_quotations";
	protected $_tb_quotation_plan = "#__insure_travel_quotations_to_plans";
	protected $_tb_quotation_traveller = "#__insure_travel_quotations_to_travellers";

	public function getReturnPage() {
		return base64_encode($this->getState('return_page'));
	}

	protected function populateState() {
		$app = JFactory::getApplication();
		$params = $app->getParams();
		$this->setState('params', $params);
	}

	public function getItem($id = null) {
		// nothing to do
	}

	public function getTable($type = 'Quotation', $prefix = 'InsureDIYTravelTable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}

	public function getForm($data = array(), $loadData = TRUE) {
		// Get the form.
		$form = $this->loadForm('com_insurediytravel.form', 'form', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}

		return $form;
	}

	protected function loadFormData() {
		$data = $this->getData();
		$this->preprocessData('com_insurediytravel.form', $data);

		return $data;
	}

	protected function getData() {
		$user = JFactory::getUser();
		$session = JFactory::getSession();
		$data = new stdClass();
		$inputQid = InsureDIYTravelHelper::getInputQid();
		$quotation = array();

		if ($user->get('guest')) {
			if ($inputQid) {
				$this->setError(JText::_("ERR_NO_ACCESS_TO_VIEW"));
				return FALSE;
			}
		} else {
			if ($inputQid) {
				$quotation = InsureDIYTravelHelper::getQuotation($inputQid);
				if (!InsureDIYTravelHelper::checkQuotation($quotation)) {
					InsureDIYTravelHelper::resetQuotation($quotation);
					$quotation = InsureDIYTravelHelper::getQuotation($quotation['id']);
				}
				if (empty($quotation) || $quotation['user_id'] != $user->id) {
					$this->setError(JText::_("ERR_NO_ACCESS_TO_VIEW"));
					return FALSE;
				}
				$session->set("travel.quotation_id", $inputQid);
				$session->set("travel.data", $quotation);
			} else {
				$quotation = $session->get("travel.data", array());
			}
		}
		foreach ($quotation as $k => $v) {
			$data->$k = $v;
		}
		return $data;
	}

	public function getCurrentUser() {

		$db = JFactory::getDBO();
		$user = JFactory::getUser();

		if ($user->guest) {
			return;
		}

		$query = " SELECT * FROM #__users WHERE id = " . $db->Quote($user->id, false) . " LIMIT 1 ";
		$db->setQuery($query);
		$result = $db->loadObject();

		return $result;
	}

	public function getPolicyPlans() {
		$db = JFactory::getDBO();
		$session = JFactory::getSession();
		$data = $session->get("travel.data");
		$query = $db->getQuery(TRUE);

		$query->select("p.*, c.logo");
		$query->from($this->_tb_plan . " AS p");
		$query->innerJoin($this->_tb_company . " AS c ON c.insurer_code = p.insurer_code");

		// filters
		$trip_type = $data['trip_type'];
		$group_type = $data['group_type'];
		$no_of_days = $data['no_of_days'];
		$no_of_travellers = $data['no_of_travellers'];

		$no_of_adults = $data['no_of_adults'];
		$no_of_children = $data['no_of_children'];
		$no_of_s_children = $data['no_of_s_children'];

		$query->where("p.trip_type = " . $db->Quote($trip_type));
		$query->where("p.group_type = " . $db->Quote($group_type));

		if ($trip_type == "ST") {
			$query->where("p.no_of_days = " . $db->Quote($no_of_days));
		}

		if ($group_type == "JM" || $group_type == "WG") {
			$query->where("p.no_of_travellers = " . $db->Quote($no_of_travellers));
		} else {
			$query->where("p.no_of_adults = " . $db->Quote($no_of_adults));
			$query->where("p.no_of_children = " . $db->Quote($no_of_children));
			$query->where("p.no_of_s_children = " . $db->Quote($no_of_s_children));
		}
		$query->where("p.state = 1");
		$db->setQuery($query, 0, 6);
		$rows = $db->loadObjectList();
		return $rows;
	}

	public function getMyPlans() {
		$db = JFactory::getDBO();
		$quotation_id = InsureDIYTravelHelper::getQid();
		$query = $db->getQuery(TRUE);
		$query->select("qtp.*, c.company_name");
		$query->from($this->_tb_quotation_plan . " AS qtp");
		$query->from($this->_tb_company . " AS c");
		$query->where("qtp.quotation_id = " . $quotation_id);
		$query->where("c.insurer_code = qtp.insurer_code");
		$db->setQuery($query);
		$rows = $db->loadObject();
		return $rows;
	}

	public function getMyInfo($id = FALSE) {
		$db = JFactory::getDBO();
		$quotation_id = ($id) ? $id : InsureDIYTravelHelper::getQid();
		$query = $db->getQuery(TRUE);
		$query->select("q.*, c.insurer_code, c.plan_name ");
		$query->from($this->_tb_quotation . " AS q");
		$query->innerJoin($this->_tb_quotation_plan . " AS c ON c.quotation_id = q.id");
		$query->where("q.id = " . $quotation_id);
		$db->setQuery($query);
		$row = $db->loadObject();
		return $row;
	}

	public function getPDPayment() {
		$app = JFactory::getApplication();
		$params = $app->getParams();
		$session = JFactory::getSession();
		$config_com_insureDIY	= JComponentHelper::getParams('com_insurediy');
		$quotation_id = $session->get('travel.quotation_id', JRequest::getVar('quotation_id'));
		$data = new stdClass();

		if ($this->hasPayment($quotation_id)) {
			$data->hasPayment = TRUE;
			$quotation = $this->getQuotation($quotation_id);
			if (!$quotation) {
				return FALSE;
			}
			$data->action = $params->get('pdUrl', "https://test.paydollar.com/b2cDemo/eng/payment/payForm.jsp");
			$data->mpsMode = $params->get('mpsMode', 'NIL');
			$data->currCode = $params->get('currCode', '344');
			$data->lang = $params->get('lang', 'E');
			$data->merchantId = $params->get('merchantId', FALSE);
			$data->orderRef = $quotation->unique_order_no;
			$data->amount = $quotation->premium;
			$data->cancelUrl = JURI::base() . 'index.php?option=com_insurediytravel&view=form&layout=canceled&Itemid=159';
			$data->failUrl = JURI::base() . 'index.php?option=com_insurediytravel&view=form&layout=error&Itemid=159';
			$data->successUrl = JURI::base() . 'index.php?option=com_insurediytravel&view=form&layout=thankyou&Itemid=159';
			$data->payType = "N";
			$data->payMethod = "ALL";
			$data->secureHash = $config_com_insureDIY->get('secureHash', '');
			$data->remark = "travel"; // Using remark to check for different type of insurances. Do not use this for other purpose.
			if (!$data->merchantId) {
				return FALSE;
			}
		} else {
			$data->hasPayment = FALSE;
		}
		return $data;
	}

	public function getQuotation($quotation_id, $user_id = FALSE) {
		if (!$quotation_id) {
			return FALSE;
		}
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE);

		$query->select("q.*, qp.premium, qp.insurer_code");
		$query->from($this->_tb_quotation . " AS q");
		$query->leftJoin($this->_tb_quotation_plan . " AS qp ON q.id = qp.quotation_id");
		$query->where("q.id = " . $quotation_id);
		if ($user_id) {
			$query->where("q.user_id = " . (int) $user_id);
		}
		$db->setQuery($query);
		$quotation = $db->loadObject();
		return $quotation;
	}

	private function hasPayment($quotation_id) { // quotation_id(int), return boolean
		$has = TRUE;
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from($this->_tb_quotation)
				->where("id = " . (int) $quotation_id);
		$db->setQuery($query);
		$quotation = $db->loadObject();
		if ($quotation->payment_status != "N" && $quotation->payment_status != "P") {
			$has = FALSE;
		}
		if ($quotation->quote_stage < 5) {
			$has = FALSE;
		}
		if ($quotation->unique_order_no == "") {
			$has = FALSE;
		}
		return $has;
	}

	public function getMySumInsuredTotal() {
		return $this->checkSumInsuredQuotationTotal();
	}

	public function getContactDetails() {
		$user = JFactory::getUser();
		$data = array();

		if ($user->id > 0) {
			$db = JFactory::getDbo();
			$query = $db->getQuery(TRUE);
			$query->select("u.name AS contact_firstname, "
					. "u.lastname AS contact_lastname, "
					. "u.room_no AS contact_room_no,"
					. "u.floor_no AS contact_floor_no,"
					. "u.block_no AS contact_block_no,"
					. "u.building_name AS contact_building_name,"
					. "u.street_no AS contact_street_name,"
					. "u.country_code AS contact_country_code,"
					. "u.contact_no AS contact_contact_no,"
					. "u.district AS contact_district_name,"
					. "u.postal_code AS contact_postalcode,"
					. "u.country AS contact_country");
			$query->from("#__users u");
			$query->where("id=" . $db->quote($user->id));
			$db->setQuery($query);
			$results = $db->loadAssoc();

			if (!is_null($results)) {
				$session = JFactory::getSession();
				$this->my_detail = $session->get('details');
				$this->my_detail['contact_country_code'] = isset($results["contact_country_code"]) ? $results["contact_country_code"] : "";
				$session->set('details', $this->my_detail);
			}

			return ($results) ? $results : $data;
		}
		return $data;
	}

	public function step1save($data) {
		$db = JFactory::getDBO();
		$user = JFactory::getUser();
		$session = JFactory::getSession();

		unset($data['agree_company']);
		unset($data['read_terms']);
		unset($data['read_policy']);
		unset($data['read_personal']);
		unset($data['agree_product']);

		$data['user_id'] = 0;
		if (!$user->get('guest')) {
			$data['user_id'] = $user->id;
			$data['email'] = $user->email;
			$data['unique_order_no'] = InsureDIYHelper::getUniqueOrderNo($user->id);
		}
		if ($data['group_type'] == "JM") {
			$data['no_of_travellers'] = 1;
			$data['no_of_adults'] = 1;
			$data['no_of_children'] = 0;
			$data['no_of_s_children'] = 0;
		}
		if ($data['group_type'] == "WG") {
			$data['no_of_adults'] = $data['no_of_travellers'];
			$data['no_of_children'] = 0;
			$data['no_of_s_children'] = 0;
		}
		if ($data['group_type'] == "WF") {
			$data['no_of_travellers'] = $data['no_of_adults'] + $data['no_of_children'] + $data['no_of_s_children'];
		}

		$data['start_date'] = date("Y-m-d", strtotime($data['start_date']));
		if ($data['trip_type'] == "ST") {
			$data['end_date'] = date("Y-m-d", strtotime($data['end_date']));
			$data['no_of_days'] = InsureDIYHelper::getNumOfDays($data['start_date'], $data['end_date']);
		} else {
			unset($data['end_date']);
			$data['no_of_days'] = 0;
		}
		$data['quote_stage'] = 2; // step 2 now
		if (isset($data['id']) && $data['id'] == 0) {
			$data['created_date'] = JFactory::getDate()->toSql();
			$dataobj = MyHelper::array2jObject($data);
			$db->insertObject($this->_tb_quotation, $dataobj, "id");
		} else {
			$dataobj = MyHelper::array2jObject($data);
			$db->updateObject($this->_tb_quotation, $dataobj, "id");
		}
		$quotation = InsureDIYTravelHelper::getQuotation($dataobj->id);
		$session->set("travel.data", $quotation);
		return $dataobj->id;
	}

	public function step2save($quotation_id, $plan_id) {
		$db = JFactory::getDbo();
		$session = JFactory::getSession();

		$plan = $this->getPlan($plan_id);
		$plan->quotation_id = $quotation_id;
		$query = $db->getQuery(TRUE)
				->select("count(*)")
				->from($this->_tb_quotation_plan)
				->where("quotation_id = " . $quotation_id);
		$db->setQuery($query);
		$count = $db->loadResult();

		if ($count) {
			$check = $db->updateObject($this->_tb_quotation_plan, $plan, "quotation_id");
		} else {
			$check = $db->insertObject($this->_tb_quotation_plan, $plan);
		}
		if ($check) {
			$this->updateStage($quotation_id, "3");
			$quotation = InsureDIYTravelHelper::getQuotation($quotation_id);
			$session->set("travel.data", $quotation);
		}

		return $check;
	}

	public function step3save($data) {
		$db = JFactory::getDbo();
		$data['dob'] = date("Y-m-d", strtotime($data['dob']));
		$obj = MyHelper::array2jObject($data);
		$db->insertObject($this->_tb_quotation_traveller, $obj);
		return $db->insertid();
	}

	public function step4save($data) {
		$db = JFactory::getDbo();
		$user = JFactory::getUser();
		$session = JFactory::getSession();
		$data['unique_order_no'] = InsureDIYHelper::getUniqueOrderNo($user->id);
		$obj = MyHelper::array2jObject($data);
		$result = $db->updateObject($this->_tb_quotation, $obj, 'id');
		
		$query = $db->getQuery(TRUE)
				->select("is_firsttime")
				->from("#__users")
				->where("id = " . $user->id);
		$is_firsttime = $db->setQuery($query)->loadResult();
		if ($is_firsttime) {
			$fieldTmp = array();
			$fieldTmp[] = " name = " . $db->Quote($data['contact_firstname'], false);
			$fieldTmp[] = " lastname = " . $db->Quote($data['contact_lastname'], false);
			$fieldTmp[] = " country_code = " . $db->Quote($data['contact_country_code'], false);
			$fieldTmp[] = " contact_no = " . $db->Quote($data['contact_contact_no'], false);
			$fieldTmp[] = " room_no = " . $db->Quote($data['contact_room_no'], false);
			$fieldTmp[] = " floor_no = " . $db->Quote($data['contact_floor_no'], false);
			$fieldTmp[] = " block_no = " . $db->Quote($data['contact_block_no'], false);
			$fieldTmp[] = " building_name = " . $db->Quote($data['contact_building_name'], false);
			$fieldTmp[] = " street_no = " . $db->Quote($data['contact_street_name'], false);
			$fieldTmp[] = " district = " . $db->Quote($data['contact_district_name'], false);
			$fieldTmp[] = " postal_code = " . $db->Quote($data['contact_postalcode'], false);
			$fieldTmp[] = " country = " . $db->Quote($data['contact_country'], false);
			$fieldTmp[] = " is_firsttime = " . $db->Quote(0, false);
			$query = " UPDATE #__users SET " . implode(",", $fieldTmp) . " WHERE id = '" . $user->id . "' ";
			$db->setQuery($query);
			$db->query();
		}
		$quotation = InsureDIYTravelHelper::getQuotation($obj->id);
		$session->set("travel.data", $quotation);
		return $result;
	}

	public function saveContactDetails($qid) {
		$contact = $this->getContactDetails();
		$db = JFactory::getDbo();
		$contact['id'] = $qid;
		return $db->updateObject($this->_tb_quotation, MyHelper::array2jObject($contact), "id");
	}

	public function deleteAllTravellers($quotation_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->delete($this->_tb_quotation_traveller)
				->where("quotation_id = " . $quotation_id);
		$db->setQuery($query);
		return $db->execute();
	}

	public function getPlan($plan_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from($this->_tb_plan)
				->where("plan_id = " . $plan_id);
		$db->setQuery($query);
		return $db->loadObject();
	}

	public function updateStage($quotation_id, $stage) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->update($this->_tb_quotation)
				->set("quote_stage = " . $stage)
				->where("id = " . $quotation_id);
		$db->setQuery($query);
		$check = $db->execute();
		return $check;
	}

	public function getStep($quotation_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("quote_stage")
				->from($this->_tb_quotation)
				->where("id = " . $quotation_id);
		$db->setQuery($query);
		$check = $db->loadResult();
		return $check;
	}

	public function changeToAnuual($quotation_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->update($this->_tb_quotation)
				->set("trip_type = " . $db->quote("AT"))
				->where("id = " . $quotation_id);
		$db->setQuery($query);
		$check = $db->execute();
		return $check;
	}

	public function getTripType($quotation_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("trip_type")
				->from($this->_tb_quotation)
				->where("id = " . $quotation_id);
		$db->setQuery($query);
		$check = $db->loadResult();
		return $check;
	}

	public function savePDFPaths($quotation_id, $array = array(), $client = TRUE) {
		if (empty($array)) {
			return;
		}
		$toJson = new stdClass();
		foreach ($array as $k => $v) {
			if (is_array($v)) {
				$toJson->$k = implode(" | ", $v);
			} else {
				$toJson->$k = $v;
			}
		}
		$db = JFactory::getDBO();
		if ($client) {
			$query = " UPDATE " . $this->_tb_quotation . " SET `file_json_pdfpath` = " . $db->Quote(json_encode($toJson), false) . " WHERE id = '$quotation_id' ";
		} else {
			$query = " UPDATE " . $this->_tb_quotation . " SET `file_json_pdfpath2` = " . $db->Quote(json_encode($toJson), false) . " WHERE id = '$quotation_id' ";
		}
		$db->setQuery($query);
		$db->query();
	}

	// Mail Stuffs - start
	public function sendEmails($pdfarrs, $client = TRUE, $id = FALSE) {
		$mailer = JFactory::getMailer();
		$params = JComponentHelper::getParams('com_insurediytravel');
		$myInfo = $this->getMyInfo($id);
		$legit = $this->checkBlacklist($myInfo);
		$maildata = $this->getMailData($myInfo);
		$emails = $this->getMails($params, $legit, $myInfo->email);
		$emailbodies = $this->getEmailBodies($params, $maildata);

		$mode = TRUE;
		foreach ($pdfarrs as $pdfarr) {
			$attachments = $this->prepareAttachments($pdfarr, $myInfo->id);
			if ($client && $legit) {
				$mailer->ClearAttachments();
				$mailer->ClearAllRecipients();
				$mailer->sendMail($emails['from'], $emails['fromName'], $emails['recipient'], $emails['subject'], $emailbodies['ce_customer'], $mode, NULL, $emails['bcc'], $attachments);
			} else {
				$mailer->ClearAttachments();
				$mailer->ClearAllRecipients();
				$mailer->sendMail($emails['from'], $emails['fromName'], $emails['service_recipient'], $emails['subject'], $emailbodies['ce_service'], $mode, NULL, $emails['bcc'], $attachments);
			}
		}
	}

	public function prepareAttachments($pdfarr, $id) {
		$temp = array();
		foreach ($pdfarr as $pdf) {
			$temp[] = MyHelper::getDeepPath(QUOTATION_PDF_SAVE_PATH, $id) . $pdf;
		}
		return $temp;
	}

	public function getEmailBodies($params, $data) {
		$array = array();
		$array['cover_letter'] = InsureDIYHelper::replaceVariables($params->get("clformat"), $data);
		$array['ce_customer'] = InsureDIYHelper::replaceVariables($params->get("cecformat"), $data);
		$array['ce_service'] = InsureDIYHelper::replaceVariables($params->get("cesformat"), $data);
		return $array;
	}

	public function getMails($params, $legit, $user_email) {
		$confg = JFactory::getConfig();
		$array = array();
		if ($legit) {
			$array['recipient'] = $user_email;
			$sales_email = $params->get('sales_email');
			$array['bcc'] = array("0" => "zan@ifoundries.com", "1" => $sales_email, "2" => "rylaicf77@gmail.com");
			$array['subject'] = 'InsureDIY - Application Form';
		} else {
			$array['recipient'] = $params->get('admin_email');
			$sales_email = $params->get('sales_email');
			$array['bcc'] = array("0" => "zan@ifoundries.com", "1" => $sales_email, "2" => "rylaicf77@gmail.com");
			$array['subject'] = 'InsureDIY - Application Form[Blacklist]';
		}
		$array['service_recipient'] = $params->get('service_email', "zan@ifoundries.com");
		$array['from'] = $confg->get("mailfrom");
		$array['fromName'] = $confg->get("fromname");
		return $array;
	}

	public function getMailData($myInfo) {
		$session = JFactory::getSession();

		$data = array();
		$data['first_name'] = $myInfo->contact_firstname;
		$data['last_name'] = $myInfo->contact_lastname;
		$data['order_no'] = $myInfo->unique_order_no;
		$data['product'] = $myInfo->plan_name;
		$data['insurer'] = $myInfo->insurer_code;
		$data['start_date'] = date("d-M-Y", strtotime($myInfo->start_date));
		$data['address'] = $session->get("maildata.address." . $myInfo->id, FALSE);
		$data['contact_number'] = $session->get("maildata.contact_no." . $myInfo->id, FALSE);

		return $data;
	}

	public function checkBlacklist($data) {
		$name = $data->contact_firstname . " " . $data->contact_lastname;
//		$nationality = $data->contact_country;
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$query->select("count(*)");
		$query->from("#__insure_blacklist");
		$query->where("UPPER(name) = UPPER(" . $db->quote($name) . ")");
//		$query->where("UPPER(name) = UPPER(" . $db->quote($name) . ") AND nationality = " . $db->quote($nationality));
		$db->setQuery($query);
		$result = $db->loadResult();
		if ($result > 0) {
			return FALSE;
		}
		return TRUE;
	}

	// Mail Stuffs - end

	public function createPoints($quotation_id) {
		$quotation = InsureDIYTravelHelper::getQuotation($quotation_id, FALSE);
		$plan = $quotation['selected_plan'];
		$total_pur_points = $plan->pur_points;
		$total_ref_points = $plan->ref_points;
		if (InsureDIYHelper::checkReferralWithQuotation($quotation)) {
			InsureDIYHelper::updateReferredByWithQuotation($quotation);
			InsureDIYHelper::createReferralWithQuotation($quotation, "travel." . $quotation_id, $quotation['unique_order_no'], $total_ref_points, sprintf(JText::_("REFERRAL_POINT_RECORD_DESCRIPTION"), $quotation['email']));
		}
		$desc = sprintf(JText::_("PURCHASE_POINT_RECORD_DESCRIPTION"), $quotation['unique_order_no']);
		$percent = InsureDIYHelper::getPromoCode($quotation['promo_code']);
		if ($percent) {
			$total_pur_points = $total_pur_points * (100 + $percent) / 100;
			InsureDIYHelper::usePromoCode($quotation['promo_code']);
			$desc .= ". With promo code " . $quotation['promo_code'];
		}
		InsureDIYHelper::createPoint($quotation['user_id'], $total_pur_points, "travel." . $quotation_id, $quotation['unique_order_no'], "", $desc, 1);
		return TRUE;
	}

	public function createPolicyRecord($quotation_id) {
		$db = JFactory::getDbo();
		$quotation = InsureDIYTravelHelper::getQuotation($quotation_id, FALSE);
		$obj = new stdClass();
		$obj->user_id = $quotation['user_id'];
		$obj->type = "4"; // travel is 4
		$obj->insurer = $quotation['selected_plan']->insurer_code;
		$obj->currency = 344; // HK$
		$obj->status = 2; // Set as pending. 1 => active, 0 => inactive, 2 => pending
		$obj->quotation_id = $quotation_id;
		$db->insertObject("#__insure_policies", $obj);
	}

	public function back($quotation_id) {
		$db = JFactory::getDBO();
		$user = JFactory::getUser();
		$current_stage = InsureDIYTravelHelper::getCurrentStage($quotation_id);
		$backables = array(2 => 1, 3 => 2, 4 => 3, 5 => 4);
		$backstage = (in_array($current_stage, array_keys($backables))) ? $backables[$current_stage] : FALSE;
		if ($backstage && !$user->get('guest')) {
			$query = $db->getQuery(TRUE)
					->update($this->_tb_quotation)
					->set("quote_stage = " . $backstage)
					->where("id = " . $quotation_id)
					->where("quote_stage > 1")
					->where("user_id = " . $user->id);
			$result = $db->setQuery($query)->execute();
		}

		return $backstage;
	}

}
