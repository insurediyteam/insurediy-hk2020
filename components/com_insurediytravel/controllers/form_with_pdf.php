<?php
	
	defined('_JEXEC') or die;
	
	class InsureDIYTravelControllerForm extends JControllerForm {
		
		private $base_layout_url = "index.php?option=com_insurediytravel&view=form&layout=";
		private $form_url = "index.php?option=com_insurediytravel&view=travel";
		
		public function getModel($name = 'form', $prefix = '', $config = array('ignore_request' => true)) {
			$model = parent::getModel($name, $prefix, $config);
			return $model;
		}
		
		public function step1save() {
			$session = JFactory::getSession();
			$app = JFactory::getApplication();
			$user = JFactory::getUser();
			$model = $this->getModel();
			
			$post = $app->input->get('jform', '', 'array');
			
			$quotation_id = $model->step1save($post);
			
			if ($quotation_id) {
				$session->set('travel.quotation_id', $quotation_id);
			}
			$app->redirect(JRoute::_($this->form_url));
		}
		
		public function step2save() {
			$app = JFactory::getApplication();
			$model = $this->getModel();
			$check = TRUE;
			$plan_id = JFactory::getApplication()->input->post->get('plan_id', FALSE, 'int');
			$quotation_id = InsureDIYTravelHelper::getQid();
			
			if ($quotation_id && $plan_id) {
				$model->step2save($quotation_id, $plan_id);
			}
			$app->redirect(JRoute::_($this->form_url));
		}
		
		public function step3save() {
			$app = JFactory::getApplication();
			$session = JFactory::getSession();
			$model = $this->getModel();
			$data = $session->get("travel.data");
			$rows = isset($data['no_of_travellers']) ? $data['no_of_travellers'] : 1;
			$quotation_id = InsureDIYTravelHelper::getQid();
			$model->deleteAllTravellers($quotation_id);
			$hasMyself = FALSE;
			for ($i = 0; $i < $rows; $i++) {
				$form = ($i) ? 'jform' . ($i + 1) : 'jform';
				$post = $app->input->get($form, '', 'array');
				if ($post['relation'] == "1") {
					$hasMyself = TRUE;
				}
				$post['quotation_id'] = $quotation_id;
				$model->step3save($post);
			}
			if ($hasMyself) {
				$model->saveContactDetails($quotation_id);
			}
			$model->updateStage($quotation_id, "4");
			InsureDIYTravelHelper::overrideSessionWithQid($quotation_id);
			$app->redirect(JRoute::_($this->form_url));
		}
		
		public function step4save() {
			$app = JFactory::getApplication();
			$model = $this->getModel();
			$quotation_id = InsureDIYTravelHelper::getQid();
			$post = $app->input->get('jform', '', 'array');
			$post['id'] = $quotation_id;
			
			$referred_by = $post['referred_by'];
			if (!InsureDIYHelper::checkReferral($referred_by)) { // referral is valid
				unset($post['referred_by']);
			}
			$check = $model->step4save($post);
			if ($check) {
				
				/*
					* PDF will be created here
				*/ 
				if ($quotation_id) { // Check for save and quotation ID
					
					$quotation = InsureDIYTravelHelper::getQuotation($quotation_id);
					//$hasLargeSum = $model->checkSumInsuredQuotationTotal($quotation_id) >= 5000000;
					
					$plan = $quotation['selected_plan'];
					
					JLoader::import('joomla.filesystem.folder');
					JLoader::import('joomla.filesystem.file');
					
					$userPath = MyHelper::getDeepPath(TRAVEL_PDF_SAVE_PATH, $quotation['user_id']);
					
					$pdfarrs = array();
					$pdfarrs2 = array();
					$pdfData = InsureDIYTravelHelper::getPdfData($quotation_id);
					
					$forms = InsureDIYHelper::getCompanyForms($plan->insurer_code);
					if (!$forms['travel_generate_pdf']) {
						continue;
					}
					$gOrigin = (isset($forms['travel_generic']) && strlen($forms['travel_generic'])) ? $forms['travel_generic'] : "";
					if (JFile::exists($gOrigin)) {
						$filename = InsureDIYHelper::generateFileName($forms['travel_generic']);
						$dest = $userPath . DS . $filename;
						
						$result = InsureDIYHelper::generatePdfForm($gOrigin, $dest, $pdfData);
						$pdfarrs[$plan->plan_index_code] = $result;
					}
					
					/*if (!$hasLargeSum) {
						continue;
						}
						$lsOrigin = (isset($forms['life_large_sum']) && strlen($forms['life_large_sum'])) ? $forms['life_large_sum'] : "";
						if (JFile::exists($lsOrigin)) {
						$filename = InsureDIYHelper::generateFileName($forms['life_large_sum']);
						$dest = $userPath . DS . $filename;
						
						$result = InsureDIYHelper::generatePdfForm($lsOrigin, $dest, $pdfData);
						$pdfarrs[$plan->plan_index_code] = $result;
						$pdfarrs2[$plan->plan_index_code] = $result;
						}
						
						$exOrigin = (isset($forms['life_extra_info']) && strlen($forms['life_extra_info'])) ? $forms['life_extra_info'] : "";
						if (JFile::exists($exOrigin)) {
						$filename = InsureDIYHelper::generateFileName($forms['life_extra_info']);
						$dest = $userPath . DS . $filename;
						
						$result = InsureDIYHelper::generatePdfForm($exOrigin, $dest, $pdfData);
						$pdfarrs[$plan->plan_index_code] = $result;
						$pdfarrs2[$plan->plan_index_code] = $result;
					}*/
					
				}
				
				$model->savePDFPaths($quotation_id, $pdfarrs);
				$model->sendEmails($pdfarrs);
				/*
					* PDF will be created here - ENDED
				*/ 
				
				$model->updateStage($quotation_id, '5');
				InsureDIYTravelHelper::overrideSessionWithQid($quotation_id);
			}
			
			$app->redirect(JRoute::_($this->form_url));
		}
		
		public function step5save() {
			$app = JFactory::getApplication();
			$model = $this->getModel();
			$quotation_id = InsureDIYTravelHelper::getQid();
			$post = $app->input->get('jform', '', 'array');
			$post['id'] = $quotation_id;
			
			InsureDIYLifeHelper::clearSessionData(); // woot~ all done. clear the session
			
			$app->redirect($this->getRedirectUrl("thankyou"));
		}
		
		public function changeToAnuual() {
			$app = JFactory::getApplication();
			$session = JFactory::getSession();
			$quotation_id = InsureDIYTravelHelper::getQid();
			$model = $this->getModel();
			
			$data = $session->get("travel.data");
			$data['trip_type'] = "AT";
			$session->set("travel.data", $data);
			if (!$model->changeToAnuual($quotation_id)) {
				$app->redirect($this->form_url, JText::_("DB_SAVE_FAIL"));
			}
			$app->redirect(JRoute::_($this->form_url));
		}
		
		public function saveBeforePayment() {
			JSession::checkToken() or jexit(JText::_("JINVALID_TOKEN"));
			$quotation_id = InsureDIYTravelHelper::getQid();
			$db = JFactory::getDbo();
			$query = $db->getQuery(TRUE)
			->update("#__insure_travel_quotations")
			->set("payment_status = " . $db->quote("P"))
			->where("id = " . $db->quote($quotation_id));
			$db->setQuery($query);
			$json = array();
			if ($db->execute()) {
				//			InsureDIYTravelHelper::clearSessionData(); // Clear the session data.
				$json['success'] = "1";
				} else {
				$json['error'] = "1";
			}
			echo json_encode($json);
			exit;
		}
		
		public function startNewQuotation() {
			JSession::checkToken() or jexit(JText::_("JINVALID_TOKEN"));
			$app = JFactory::getApplication();
			InsureDIYTravelHelper::clearSessionData();
			$app->redirect(JRoute::_($this->form_url));
		}
		
		public function getRedirectUrl($layout) {
			$url = $this->base_layout_url . $layout;
			return $url;
		}
		
		public function back() {
			JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
			$app = JFactory::getApplication();
			$quotation_id = $app->input->post->get("quotation_id", FALSE, "integer");
			if ($quotation_id) {
				$model = $this->getModel();
				$model->back($quotation_id);
			}
			$app->redirect(JRoute::_($this->form_url));
		}
		
	}
