<?php

defined('_JEXEC') or die;

class InsureDIYTravelControllerForm extends JControllerForm {

	private $base_layout_url = "index.php?option=com_insurediytravel&view=form&layout=";
	private $form_url = "index.php?option=com_insurediytravel&view=travel&Itemid=159";

	public function getModel($name = 'form', $prefix = '', $config = array('ignore_request' => true)) {
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}

	public function step1save() {
		$session = JFactory::getSession();
		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		$model = $this->getModel();

		$post = $app->input->get('jform', '', 'array');

		$quotation_id = $model->step1save($post);

		if ($quotation_id) {
			$session->set('travel.quotation_id', $quotation_id);
		}
		
		$layout = InsureDIYTravelHelper::getLayout();
		
		if($layout == 'login') {
			$app->redirect(JRoute::_($this->form_url.'&step=sign_in'));
		} else {
			$app->redirect(JRoute::_($this->form_url.'&step=2'));
		}
		
	}

	public function step2save() {
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$check = TRUE;
		$plan_id = JFactory::getApplication()->input->post->get('plan_id', FALSE, 'int');
		$quotation_id = InsureDIYTravelHelper::getQid();

		if ($quotation_id && $plan_id) {
			$model->step2save($quotation_id, $plan_id);
		}
		$app->redirect(JRoute::_($this->form_url.'&step=3'));
	}

	public function step3save() {
		$app = JFactory::getApplication();
		$session = JFactory::getSession();
		$model = $this->getModel();
		$data = $session->get("travel.data");
		$rows = isset($data['no_of_travellers']) ? $data['no_of_travellers'] : 1;
		$quotation_id = InsureDIYTravelHelper::getQid();
		$model->deleteAllTravellers($quotation_id);
		$hasMyself = FALSE;
		for ($i = 0; $i < $rows; $i++) {
			$form = ($i) ? 'jform' . ($i + 1) : 'jform';
			$post = $app->input->get($form, '', 'array');
			if ($post['relation'] == "1") {
				$hasMyself = TRUE;
			}
			$post['quotation_id'] = $quotation_id;
			$model->step3save($post);
		}
		if ($hasMyself) {
			$model->saveContactDetails($quotation_id);
		}
		$model->updateStage($quotation_id, "4");
		InsureDIYTravelHelper::overrideSessionWithQid($quotation_id);
		$app->redirect(JRoute::_($this->form_url.'&step=4'));
	}

	public function step4save() {
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$quotation_id = InsureDIYTravelHelper::getQid();
		$post = $app->input->get('jform', '', 'array');
		$post['id'] = $quotation_id;

		$referred_by = $post['referred_by'];
		if (!InsureDIYHelper::checkReferral($referred_by)) { // referral is valid
			unset($post['referred_by']);
		}
		$check = $model->step4save($post);
		if ($check) {
			$model->updateStage($quotation_id, '5');
			InsureDIYTravelHelper::overrideSessionWithQid($quotation_id);
		}
		$app->redirect(JRoute::_($this->form_url.'&step=5'));
	}

	public function step5save() {
		$app = JFactory::getApplication();
		$app->redirect($this->getRedirectUrl("thankyou"));
	}

	public function changeToAnuual() {
		$app = JFactory::getApplication();
		$session = JFactory::getSession();
		$quotation_id = InsureDIYTravelHelper::getQid();
		$model = $this->getModel();

		$data = $session->get("travel.data");
		$data['trip_type'] = "AT";
		$session->set("travel.data", $data);
		if (!$model->changeToAnuual($quotation_id)) {
			$app->redirect($this->form_url, JText::_("DB_SAVE_FAIL"));
		}
		$app->redirect(JRoute::_($this->form_url.'&step=2'));
	}

	public function saveBeforePayment() {
		JSession::checkToken() or jexit(JText::_("JINVALID_TOKEN"));
		$quotation_id = InsureDIYTravelHelper::getQid();
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->update("#__insure_travel_quotations")
				->set("payment_status = " . $db->quote("P"))
				->where("id = " . $db->quote($quotation_id));
		$db->setQuery($query);
		$json = array();
		if ($db->execute()) {
//			InsureDIYTravelHelper::clearSessionData(); // Clear the session data.
			$json['success'] = "1";
		} else {
			$json['error'] = "1";
		}
		echo json_encode($json);
		exit;
	}

	public function getUnique() {
		$uid = JFactory::getUser()->id;
		$jinput = JFactory::getApplication()->input;
		$quoteId = $jinput->get("quote_id");
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE);
		$query->select("*");
		$query->from("#__insure_travel_quotations");
		$query->where("id=" . $db->quote($quoteId))->where("user_id=" . $db->quote($uid));
		$db->setQuery($query);
		$uniq = $db->loadObject();
		$result = array();
		$result['uniq'] = $uniq->unique_order_no;
		echo json_encode($result);
		exit;

	}

	public function startNewQuotation() {
		JSession::checkToken() or jexit(JText::_("JINVALID_TOKEN"));
		$app = JFactory::getApplication();
		InsureDIYTravelHelper::clearSessionData();
		$app->redirect(JRoute::_($this->form_url));
	}

	public function getRedirectUrl($layout) {
		$url = $this->base_layout_url . $layout;
		return $url;
	}

	public function back() {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$app = JFactory::getApplication();
		$quotation_id = $app->input->post->get("quotation_id", FALSE, "integer");
		$backstage = 0;
		if ($quotation_id) {
			$model = $this->getModel();
			$backstage = $model->back($quotation_id);
		}
		$app->redirect(JRoute::_($this->form_url.'&step='.$backstage));
	}

}
