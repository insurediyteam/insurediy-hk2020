<?php
/**
 * @copyright	(C) 2010-2018 James Milo. All rights reserved.
 * @license	GNU/GPLv2 http://www.gnu.org/licenses/gpl-2.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php
jimport('joomla.application.component.controller');
class InsurediypromocodesControllerPromocode extends InsurediypromocodesController {
	public function checkCodes() {
		$app = JFactory::getApplication();
		$jinputs = $app->input;
		//print_r($jinputs);
		$promo_code = $jinputs->get('promo_code', '');
		$promo_type = $jinputs->get('promo_type', '');

		$message = InsurediypromocodesHelper::checkPromocode($promo_code, $promo_type);
		echo $message;
		exit();
	}
}