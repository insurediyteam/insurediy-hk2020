<?php
/**
 * @copyright	(C) 2010-2018 James Milo. All rights reserved.
 * @license	GNU/GPLv2 http://www.gnu.org/licenses/gpl-2.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php
jimport('joomla.application.component.controller');
class InsurediypromocodesControllerQuotation extends InsurediypromocodesController {
	
	public function saveQuotation() {
		$app = JFactory::getApplication();
		$jinputs = $app->input;
		$user = JFactory::getUser();
		$data = array();

		$quotation_id = (int) $jinputs->get('quotation_id', '');
		$data['id'] = $quotation_id;
		$data['contact_firstname'] = $jinputs->get('contact_firstname', '');
		$data['contact_lastname'] = $jinputs->get('contact_lastname', '');
		$data['contact_block_no'] = $jinputs->get('contact_block_no', '');
		$data['contact_street_name'] = $jinputs->get('contact_street_name', '');
		$data['contact_nationality'] = $jinputs->get('contact_nationality', '');
		$data['contact_country'] = $jinputs->get('contact_country', '');
		$data['contact_identity_type'] = $jinputs->get('contact_identity_type', '');
		$data['contact_identity_no'] = $jinputs->get('contact_identity_no', '');
		$data['contact_contact_no'] = $jinputs->get('contact_contact_no', '');
		$data['contact_country_code'] = $jinputs->get('contact_country_code', '');
		$data['contact_marital_status'] = $jinputs->get('contact_marital_status', '');
		$data['contact_race'] = $jinputs->get('contact_race', '');
		$data['contact_building_name'] = $jinputs->get('contact_building_name', '');
		$data['contact_postalcode'] = $jinputs->get('contact_postalcode', '');
		$data['contact_unit'] = $jinputs->get('contact_unit', '');
		$data['contact_dob'] = date("Y-m-d", strtotime($jinputs->get('contact_dob', '')));
		$obj = MyHelper::array2jObject($data);
		$db    		=  JFactory::getDBO();
		$id = 0;
		if($quotation_id) {			
			//print_r($obj);exit;
			$db->updateObject('#__insure_travel_quotations', $obj, 'id');
			$id = $quotation_id;
		}
		
		$fieldTmp = array();
		$fieldTmp[] = " name = " . $db->Quote($data['contact_firstname'], false);
		$fieldTmp[] = " lastname = " . $db->Quote($data['contact_lastname'], false);
		$fieldTmp[] = " country_code = " . $db->Quote($data['contact_country_code'], false);
		$fieldTmp[] = " contact_no = " . $db->Quote($data['contact_contact_no'], false);
		//$fieldTmp[] = " room_no = " . $db->Quote($data['contact_room_no'], false);
		//$fieldTmp[] = " floor_no = " . $db->Quote($data['contact_floor_no'], false);
		$fieldTmp[] = " block_no = " . $db->Quote($data['contact_block_no'], false);
		$fieldTmp[] = " building_name = " . $db->Quote($data['contact_building_name'], false);
		$fieldTmp[] = " unit = " . $db->Quote($data['contact_unit'], false);
		$fieldTmp[] = " street_no = " . $db->Quote($data['contact_street_name'], false);
		//$fieldTmp[] = " district = " . $db->Quote($data['contact_district_name'], false);
		$fieldTmp[] = " postal_code = " . $db->Quote($data['contact_postalcode'], false);
		$fieldTmp[] = " country = " . $db->Quote($data['contact_country'], false);
		$fieldTmp[] = " marital_status = " . $db->Quote($data['contact_marital_status'], false);
		$fieldTmp[] = " race = " . $db->Quote($data['contact_race'], false);
		$fieldTmp[] = " dob = " . $db->Quote($data['contact_dob'], false);
		//$fieldTmp[] = " is_firsttime = " . $db->Quote(0, false);
		$query = " UPDATE #__users SET " . implode(",", $fieldTmp) . " WHERE id = '" . $user->id . "' ";
		$db->setQuery($query);
		$db->query();
		
		echo $id;
		exit();
	}
	
}