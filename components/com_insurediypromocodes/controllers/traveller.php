<?php
/**
 * @copyright	(C) 2010-2018 James Milo. All rights reserved.
 * @license	GNU/GPLv2 http://www.gnu.org/licenses/gpl-2.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php
jimport('joomla.application.component.controller');
class InsurediypromocodesControllerTraveller extends InsurediypromocodesController {
	public function getTraveller() {
		$app = JFactory::getApplication();
		$jinputs = $app->input;
		$traveller_id = (int) $jinputs->get('traveller_id', '');
		$db    		=  JFactory::getDBO();
		$query 		= $db->getQuery(true);
		$query->select('*')
				->from($db->quoteName('#__insure_travel_travellers'))
				->where($db->quoteName('traveller_id') . ' = ' . $traveller_id);			
		$db->setQuery($query);
		$traveller = $db->loadObject();
		echo json_encode($traveller);
		exit();
	}
	
	public function saveTraveller() {
		$app = JFactory::getApplication();
		$jinputs = $app->input;
		$user = JFactory::getUser();
		$data = array();
		$traveller_id = (int) $jinputs->get('traveller_id', '');
		$sys_traveller_id = (int) $jinputs->get('sys_traveller_id', '');
		$quotation_id = (int) $jinputs->get('quotation_id', '');
		$data['quotation_id'] = $quotation_id;
		$data['firstname'] = $jinputs->get('firstname', '');
		$data['lastname'] = $jinputs->get('lastname', '');
		$data['gender'] = $jinputs->get('gender', '');
		$data['identity_type'] = $jinputs->get('identity_type', '');
		$data['id_no'] = $jinputs->get('id_no', '');
		$data['nationality'] = $jinputs->get('nationality', '');
		$data['relation'] = $jinputs->get('relation', '');
		$data['dob'] = date("Y-m-d", strtotime($jinputs->get('dob', '')));
		$id = 0;
		$db    		=  JFactory::getDBO();
		$response = array();
		if($traveller_id == 0) {
			if($sys_traveller_id == 0){
				$obj = MyHelper::array2jObject($data);
				$db->insertObject('#__insure_travel_quotations_to_travellers', $obj);
				$sys_traveller_id = $db->insertid();
			} else {
				$data['traveller_id'] = $sys_traveller_id;
				$obj = MyHelper::array2jObject($data);
				//print_r($obj);exit;
				$db->updateObject('#__insure_travel_quotations_to_travellers', $obj, 'traveller_id');
			}
			
			unset($data['traveller_id']);
			unset($data['quotation_id']);
			$traveller_id1 = InsurediypromocodesHelper::getTravellerId($user->id, $data['id_no']);
			$data['user_id'] = $user->id;
			$obj2 = MyHelper::array2jObject($data);

			if($traveller_id1) {
				$obj2->traveller_id = $traveller_id1;
				$db->updateObject('#__insure_travel_travellers', $obj2, 'traveller_id');
				$id = $traveller_id1;
			} else {
				$db->insertObject('#__insure_travel_travellers', $obj2);
				$id = $db->insertid();
			}
			
		} else {
			$query 		= $db->getQuery(true);
			$query->select('*')
					->from($db->quoteName('#__insure_travel_quotations_to_travellers'))
					->where($db->quoteName('quotation_id') . ' = ' . $quotation_id)			
					->where($db->quoteName('traveller_id') . ' = ' . $sys_traveller_id);
			$db->setQuery($query);
			$traveller = $db->loadObject();
			
			if(!empty($traveller)) {
				$data['traveller_id'] = $sys_traveller_id;
				$obj = MyHelper::array2jObject($data);
				//print_r($obj);exit;
				$db->updateObject('#__insure_travel_quotations_to_travellers', $obj, 'traveller_id');
				//$id = $sys_traveller_id;
			} else {
				$obj = MyHelper::array2jObject($data);
				$db->insertObject('#__insure_travel_quotations_to_travellers', $obj);
				//$id = $db->insertid();
			}
			unset($data['quotation_id']);
			$data['traveller_id'] = $traveller_id;
			$data['user_id'] = $user->id;
			$obj2 = MyHelper::array2jObject($data);
			$db->updateObject('#__insure_travel_travellers', $obj2, 'traveller_id');
			$id = $traveller_id;
			//$traveller_id1 = InsurediypromocodesHelper::getTravellerId($user->id, $data['id_no']);
		}
		$response['sys_traveller_id'] = $sys_traveller_id;
		$response['traveller_id'] = $id;
		echo json_encode($response);
		exit();
	}
	
	public function getTravellers(){
		$user = JFactory::getUser();

		if ($user->guest) {
			return;
		}
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)->select("*")
				->from("#__insure_travel_travellers")
				->where("user_id = " . (int) $user->id);
		$db->setQuery($query);
		//Load all quotations with the account holder 
		$travellers = $db->loadObjectList();
		$tmplists = array(
			'optiontext' => JText::_('COM_INSUREDIYPROMOCODES_COMPANION_SELECT_OPTION'),
			'optionvalue' => ''
		);
		$tmplists[] = array(
			'optiontext' => JText::_('COM_INSUREDIYPROMOCODES_COMPANION_ADD_NEW'),
			'optionvalue' => '0'
		);
		//$tmplists = array();
		if(!empty($travellers) && count($travellers)) {
			foreach($travellers as $traveller) {
				$tmplist = array();
				$text = $traveller->firstname . ' ' . $traveller->lastname;
				$value = $traveller->traveller_id;							
			
				$tmplist['optiontext'] = $text;
				$tmplist['optionvalue'] = $value;
				$tmplists[] = $tmplist;
			}
		}
		echo json_encode($tmplists);
		exit;
	}
	
	public function formatDate(){
		$app = JFactory::getApplication();
		$jinputs = $app->input;
		$user = JFactory::getUser();
		$data = array();
		$traveller_id = $jinputs->get('traveller_id', '');
		$date_str = $jinputs->get('date_str', '');
		$date = '';
		if(!empty($date_str)) {
			$date = date('d-m-Y', strtotime(str_replace('/', '-', $date_str)));
			//$date = strtotime(str_replace('/', '-', $date_str));
		}
		$data['date_str'] = $date;
		$data['traveller_id'] = $traveller_id;
		echo json_encode($data);
		exit;
	}
	
}