<?php
/**
 * @copyright	(C) 2010-2018 James Milo. All rights reserved.
 * @license	GNU/GPLv2 http://www.gnu.org/licenses/gpl-2.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php
jimport('joomla.application.component.controller');
class InsurediypromocodesControllerCustomlist extends InsurediypromocodesController {
	public function updateField() {
		$app = JFactory::getApplication();
		
		$jinputs = $app->input;
		$group = $jinputs->get('group', 'insurediy-with-group');
		$results = array();
		if (isset($group) && !empty($group))
		{
			if($group == 'insurediy-with-group') {
				$results = array(
					'2' => 2,
					'3' => 3,
					'4' => 4,
					'5' => 5,
					'6' => 6,
					'7' => 7,
					'8' => 8,
					'9' => 9,
					'10' => 10,
				);
			} else {
				$results = array(
					'11' => 11,
					'12' => 12,
					'13' => 13,
					'14' => 14,
					'15' => 15,
					'16' => 16,
					'17' => 17,
					'18' => 18,
					'19' => 19,
					'20' => 20,
				);
			}
		}
		echo json_encode($results);
		exit();
	}
	
	public function updateCountry(){
		$app = JFactory::getApplication();
		$travelParams = JComponentHelper::getParams('com_insurediytravel');
		$jinputs = $app->input;
		$type = $jinputs->get('type', 'ST');
		$selectedCountryCode = $jinputs->get('selectedCountry', 'none');
		$groups = '';

		$data = array();
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$lists = "";
		$destinations = $travelParams->get('annual_destinations_list');
		$lists = implode("','", $destinations);
		$query->select(" * ");
		$query->from("#__insure_travel_countries");
		if($type == 'AT') {
			$query->where($db->qn('variable') . " IN ('$lists')");
		} else {
			$query->where($db->qn('variable') . " NOT IN ('$lists')");
		} 

		$db->setQuery($query);
		$results = $db->loadAssocList();
		if($type == 'ST'){
			$group_name = '';
			$data[$group_name][$k]['value'] = '';
			$data[$group_name][$k]['text'] = 'Select an option';
			foreach ($results as $k => $result) {
				//$data[$k]['value'] = $result['variable'];
				//$data[$k]['text'] = $result['value'];
				if($result['variable'] == 'GROUP') { $group_name = $result['value'];continue;}
				$data[$group_name][$k]['value'] = $result['variable'];
				$data[$group_name][$k]['text'] = $result['value'];
			}
			
			if(!empty($data)) {
				foreach($data as $name => $group) {
					if(!empty($group)) {
						$groups .= '<optgroup label="'. $name .'">';
						foreach($group as $value) {
							$selected = ($selectedCountryCode != 'none' && $selectedCountryCode == $value['value']) ? 'selected="selected"' : '';
							$groups .= '<option value="'. $value['value'] .'" '. $selected .'>'. $value['text'] .'</option>';
						}
						
						$groups .= '</optgroup>';
					}
				}
			}
		} else {
			foreach ($results as $k => $result) {
				$selected = ($selectedCountryCode != 'none' && $selectedCountryCode == $result['variable']) ? 'selected="selected"' : '';
				$groups .= '<option value="'. $result['variable'] .'" '. $selected .'>'. $result['value'] .'</option>';
			}
		}

		//echo json_encode($data);
		echo $groups;
		exit;
	}
	
}