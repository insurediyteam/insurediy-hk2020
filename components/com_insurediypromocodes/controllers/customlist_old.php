<?php
/**
 * @copyright	(C) 2010-2018 James Milo. All rights reserved.
 * @license	GNU/GPLv2 http://www.gnu.org/licenses/gpl-2.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php
jimport('joomla.application.component.controller');
class InsurediypromocodesControllerCustomlist extends InsurediypromocodesController {
	public function updateField() {
		$app = JFactory::getApplication();
		$jinputs = $app->input;
		$group = $jinputs->get('group', 'insurediy-with-group');
		$results = array();
		if (isset($group) && !empty($group))
		{
			if($group == 'insurediy-with-group') {
				$results = array(
					'2' => 2,
					'3' => 3,
					'4' => 4,
					'5' => 5,
					'6' => 6,
					'7' => 7,
					'8' => 8,
					'9' => 9,
					'10' => 10,
				);
			} else {
				$results = array(
					'11' => 11,
					'12' => 12,
					'13' => 13,
					'14' => 14,
					'15' => 15,
					'16' => 16,
					'17' => 17,
					'18' => 18,
					'19' => 19,
					'20' => 20,
				);
			}
		}
		echo json_encode($results);
		exit();
	}
	
	public function updateCountry(){
		$app = JFactory::getApplication();
		$jinputs = $app->input;
		$type = $jinputs->get('type', 'ST');

		$data = array();
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$lists = "";
		if($type == 'AT') {
			$lists = "('ASEAN', 'ASIA', 'AFRICA/EUROPE', 'WORLDWIDE')";
		}
		$query->select(" * ");
		$query->from("#__insure_travel_countries");
		if($lists != '') {
			$query->where($db->qn('variable') . ' IN ' . $lists);
		} else {
			$query->where($db->qn('variable') . " NOT IN ('ASEAN', 'ASIA', 'AFRICA/EUROPE', 'WORLDWIDE')");
		} 
		$db->setQuery($query);
		$results = $db->loadAssocList();

		foreach ($results as $k => $result) {
			$data[$k]['value'] = $result['variable'];
			$data[$k]['text'] = $result['value'];
		}

		echo json_encode($data);
		exit;
	}
	
}