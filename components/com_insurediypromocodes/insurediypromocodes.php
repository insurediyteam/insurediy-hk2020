<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_insurediypromocodes
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');
jimport('joomla.application.component.view');

// Get an instance of the controller prefixed by HelloWorld
JLoader::registerPrefix('Insurediypromocodes', JPATH_COMPONENT);
JLoader::register('InsurediypromocodesController', JPATH_COMPONENT . '/controller.php');
require_once JPATH_SITE.'/components/com_insurediypromocodes/helpers/insurediypromocodes.php';
require_once JPATH_SITE . '/components/com_insurediydomestic/helpers/insurediydomestic.php';
require_once JPATH_SITE . '/components/com_insurediytravel/helpers/insurediytravel.php';
require_once JPATH_SITE . '/components/com_insurediydomesticph/helpers/insurediydomesticph.php';
require_once JPATH_SITE . '/components/com_insurediylife/helpers/insurediylife.php';
// Initialize component
InsurediypromocodesHelper::initialize();
$controller = JControllerLegacy::getInstance('insurediypromocodes');

// Perform the Request task
$input = JFactory::getApplication()->input;
$controller->execute($input->getCmd('task'));

// Redirect if set by the controller
$controller->redirect();