<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_insurediypromocodes
 * @author     DEVN <admin@gmail.com>
 * @copyright  Copyright (C) 2017. All rights reserved.
 * @license    http://www.gnu.org/copyleft/gpl.html GNU/GPL
 */
defined('_JEXEC') or die;

/**
 * Class InsurediypromocodesHelper
 *
 * @since  1.6
 */
class InsurediypromocodesHelper
{
	// Get component configuration
	public static function getConfig($name = null) {
		static $config;
		
		if (empty($config)) {
			$db = JFactory::getDbo();
			$query = $db->getQuery(true)->select($db->qn('params'))
						->from($db->qn('#__extensions'))
						->where($db->qn('type') . ' = ' . $db->q('component'))
						->where($db->qn('element') . ' = ' . $db->q('com_insurediypromocodes'));
			$db->setQuery($query);
			$params = $db->loadResult();
			
			$registry = new JRegistry;
			$registry->loadString($params);
			$config = $registry->toObject();
		}
		
		if ($name != null) {
			if (isset($config->$name)) { 
				return $config->$name;
			} else return false;
		} else return $config;
	}
	
	public static function initialize() {
		$app	= JFactory::getApplication();
		$doc	= JFactory::getDocument();			
		// Clean cache
		$cache = JFactory::getCache('com_insurediypromocodes');
		$cache->clean();
	}
	
	public static function dateDays($date, $format = "d-m-Y", $offset = 0, $type = 'year') {
		if(strpos($date,'/') !== false){
			$date = str_replace('/','-',$date);
		}
		# split the date up into individual parts
		$date2 = strtotime($date . "$offset $type");
		return date($format, $date2);
	}
	
	//Check promo code
	public static function checkPromocode($promo_code, $promo_type) {
		$session = JFactory::getSession();
		$result = array();
		$result['success'] = 0;
		$result['message'] = '';
		$message = '';
		if($promo_code != '' && $promo_type != ''){
			$user = JFactory::getUser();
			$db    		=  JFactory::getDBO();
			$query 		= $db->getQuery(true);
			$today = date('Y-m-d');
			$query->select('*')
					->from($db->quoteName('#__insure_promocodes'))
					->where($db->quoteName('code') . ' = ' . $db->quote($promo_code))		
					->where($db->quoteName('publish') . ' <> 0');		
			$db->setQuery($query);
			
			$promocode = $db->loadObject();

			//Check Promocode is existing
			if(!empty($promocode)){
				//Get current insurance type data
				switch ($promo_type){
					case 'travel':
						$insuranceData = $session->get('travel.data');
						$insuranceQuotationId = $session->get("travel.quotation_id", 0);
						break;
					case 'life':								
						$insuranceQuotationId = $session->get("life.quotation_id", 0);
						$insuranceData = InsureDIYLifeHelper::getQuotation($insuranceQuotationId);
						break;
					case 'domestic':								
						$insuranceQuotationId = $session->get("domestic.quotation_id", 0);
						$insuranceData = InsureDIYDomesticHelper::getQuotation($insuranceQuotationId);
						break;
					case 'domesticbeta':
						$insuranceQuotationId = $session->get("domestic.quotation_id", 0);
						$insuranceData = InsureDIYDomesticBetaHelper::getQuotation($insuranceQuotationId);
						break;
					case 'domesticph':								
						$insuranceQuotationId = $session->get("domesticph.quotation_id", 0);
						$insuranceData = InsureDIYDomesticPHHelper::getQuotation($insuranceQuotationId, FALSE);
						break;
					case 'motor':
						$insuranceQuotationId = $session->get("motor.data")["quotationId"];
						$insuranceData = InsureDIYMotorHelper::getQuotation($insuranceQuotationId, FALSE);
						break;
					case 'motorch':
						$insuranceQuotationId = $session->get("motor.data")["quotationId"];
						$insuranceData = InsureDIYMotorHelper::getQuotation($insuranceQuotationId, FALSE);
						break;
					case 'motortp':
						$insuranceQuotationId = $session->get("motor.data")["quotationId"];
						$insuranceData = InsureDIYMotorHelper::getQuotation($insuranceQuotationId, FALSE);
						break;
					default:
						$insuranceData = array();
						$insuranceQuotationId = 0;
						break;
				}
				
				//Check Insurance Data
				if(!empty($insuranceData)){	
					$usability = $promocode->usability;
									
					//Check uses number
					$uses = true;
					
					$used_by = json_decode($promocode->used_by, true);
					if(empty($used_by)){
						$used_by = [];
					}

					if(count($used_by) > 0 && count($used_by) < $promocode->uses && $usability == 0){
						$columns = array_column($used_by, 'user_id');
						$k = array_search($user->id, $columns);
						
						if(isset($k) && $k >= 0 && $used_by[$k]['user_id'] == $user->id) {
							$uses = false;
						}						
					} elseif($usability == 0 && count($used_by) >= $promocode->uses){	
						$uses = false;
					} elseif($usability == 1){						
						$countUsed = InsurediypromocodesHelper::countUsed($user->id, $used_by);
						$uses = ($countUsed < $promocode->uses && count($used_by) < $promocode->uses) ? true : false;
					} else {
						$uses = true;
					}

					if($uses){						
						//check applicable products
						$applicable_products = explode(',', $promocode->applicable_products);
						if(in_array($promo_type, $applicable_products)){
							//Check Cross-Selling
							$check_bought = $promocode->check_bought;
							$prerequisite_sale = ($promocode->prerequisite_sale == 'domesticph') ? 'domestic_ph' : $promocode->prerequisite_sale;
							$bought = true;
							if($check_bought == 1 && $prerequisite_sale != 'none') {
								$boughts = InsurediypromocodesHelper::checkBought($user->id, $prerequisite_sale);
								if(empty($boughts) ) {
									$bought = false;
									$message .= JText::_('COM_INSUREDIYPROMOCODES_NOT_APPLIED_TO_YOU');
								}
							}
							if($bought){
								$expiry_of = $promocode->expiry_of;
								$start_date = $promocode->start_date;
								$end_date = $promocode->end_date;
								//Check promocode for new user
								$continue = true;
								$check_new_user = $promocode->check_new_user;
								if($check_new_user == 1) {
									$expiredTime = strtotime(date('Y-m-d', strtotime($user->registerDate)) . ' '. $expiry_of .' month');
									if(strtotime($today) > $expiredTime) {
										$continue = false;
									} else {
										if(strtotime($today) < strtotime($start_date)) {
											$continue = false;
											$message .= JText::_('COM_INSUREDIYPROMOCODES_NOT_APPLY_FOR_TODAY');
										} elseif(strtotime($today) >= strtotime($start_date) && strtotime($today) > strtotime($end_date)) {
											$endYear = date('Y', strtotime($end_date));
											$curYear = date('Y') - $endYear;
											if(strtotime($today) <= $expiredTime && strtotime($user->registerDate) >= strtotime($start_date) && strtotime($user->registerDate) <= strtotime($end_date)) {
												$continue = true;
											} else {
												$continue = false;
												$message .= JText::sprintf('COM_INSUREDIYPROMOCODES_IS_EXPIRED_FROM_DATE', date('d-m-Y', strtotime($end_date)));
											}
										}
									}
								} 
								if($continue) {
									//Validate Promocode is expired
									$expired = false;
									
									if($check_new_user == 0 && strtotime($today) < strtotime($start_date)) {
										$expired = true;
										$message .= JText::_('COM_INSUREDIYPROMOCODES_NOT_APPLY_FOR_TODAY');
									} elseif($check_new_user == 0 && strtotime($today) >= strtotime($start_date) && strtotime($today) > strtotime($end_date)) {
										$expired = true;
										$message .= JText::sprintf('COM_INSUREDIYPROMOCODES_IS_EXPIRED_FROM_DATE', date('d-m-Y', strtotime($end_date)));
									}
									if(!$expired) {
										//Check Min Spend
										$selected_plan = $insuranceData['selected_plan'];
										$planAmount = ($promo_type == 'life') ? InsurediypromocodesHelper::getLifeAmount($insuranceQuotationId) : $selected_plan->premium;
										//$message .= json_encode($insuranceData);
										if($planAmount < $promocode->minimum_spend) {
											$message .= JText::sprintf('COM_INSUREDIYPROMOCODES_MINIMUM_SPEND', $promocode->minimum_spend);
										} else {
											//Check specified users
											$promo_code_type = $promocode->promo_type;
											$validated = false;
											if($promo_code_type == 'restricted') {
												if(!empty($promocode->specified_users) && $promocode->specified_users != ','){
													$specified_users = explode(',', $promocode->specified_users);
													if(!empty($specified_users) && in_array($user->email, $specified_users)) {
														$validated = true;
													} else {
														$message .= JText::_('COM_INSUREDIYPROMOCODES_NOT_APPLIED_FOR_USER');
													}
												} else {
													$message .= JText::_('COM_INSUREDIYPROMOCODES_NOT_APPLIED_FOR_USER');
												}
											} else {
												$validated = true;												
											}
											if($validated) {
												$result['success'] = 1;
												$message .= JText::_('COM_INSUREDIYPROMOCODES_PROMOCODE_AVAIABLE');
											}
										}										
									}
								} else {
									$message .= JText::sprintf('COM_INSUREDIYPROMOCODES_NEW_USER_ONLY', $expiry_of);
								}
							}
						} else {
							$message .= JText::_('COM_INSUREDIYPROMOCODES_NOT_SUPPORT_FOR_THIS_INSURANCE');
						}
					} else {
						$message .= JText::_('COM_INSUREDIYPROMOCODES_IS_USED_UP');
					}
				} else {
					$message .= JText::_('COM_INSUREDIYPROMOCODES_NO_INSURANCE_DATA');
				}
			} else {
				$message .= JText::_('COM_INSUREDIYPROMOCODES_NOT_EXISTING');
			}
		} else {
			$message .= JText::_('COM_INSUREDIYPROMOCODES_NOT_EXISTING');
		}
		$result['message'] = $message;
		return json_encode($result);
	}
	//calculate premium cost
	public static function calculateDiscountCost($promo_code, $premium_cost) {
		$db    		=  JFactory::getDBO();
		$query 		= $db->getQuery(true);
		$cost = 0;
		$query->select('*')
				->from($db->quoteName('#__insure_promocodes'))
				->where($db->quoteName('code') . ' = ' . $db->quote($promo_code))		
				->where($db->quoteName('publish') . ' <> 0');			
		$db->setQuery($query);
		$promocode = $db->loadObject();
		if(!empty($promocode)) {
			$promo_method = $promocode->promo_method;
			$promo_value = $promocode->promo_value;
			if($promo_method == 'percentage') {
				//$firstCost = ($premium_cost*$promocode->max_value) / 100;
				//$cost = (!empty($promocode->max_value) && $promocode->max_value > 0 && $firstCost > 0) ? $firstCost : ($premium_cost*$promo_value) / 100;
				$cost = ($premium_cost*$promo_value) / 100;
			} elseif($promo_method == 'absolute') {
				//$firstCost = $premium_cost - $promocode->max_value;
				//$cost = (!empty($promocode->max_value) && $promocode->max_value > 0 && $firstCost > 0) ? $promocode->max_value : $promo_value;
				$cost = $promo_value;
			} else {
				$firstCost = ($premium_cost*$promo_value) / 100;
				$cost = (!empty($promocode->max_value) && $promocode->max_value > 0 && $firstCost > $promocode->max_value) ? $promocode->max_value : $firstCost;
			}
		} 
		return number_format($cost, 2);
	}
	//Check bought insurance
	public static function checkBought($user_id, $prerequisite_sale) {
		$db    		=  JFactory::getDBO();
		$table = '#__insure_'. $prerequisite_sale .'_quotations';
		$query 		= $db->getQuery(true);
		
		$query->select('id')
				->from($db->quoteName($table))
				->where($db->quoteName('user_id') . ' = ' . (int) $user_id)
				->where($db->quoteName('quote_status') . ' = 1');			
		$db->setQuery($query);

		$boughts = $db->loadObjectList();
		return $boughts;
	}
	
	public static function usePromoCode($promo_code, $promo_type = 'travel', $quotation_id){
		echo $promo_code;
		echo "-";
		echo $promo_type;
		echo "-";
		echo $quotation_id;
		exit();

		$db    		=  JFactory::getDBO();
		$user = JFactory::getUser();
		$query 		= $db->getQuery(true);
		$cost = 0;
		$query->select('*')
				->from($db->quoteName('#__insure_promocodes'))
				->where($db->quoteName('code') . ' = ' . $db->quote($promo_code))		
				->where($db->quoteName('publish') . ' <> 0');
		$db->setQuery($query);
		
		$promocode = $db->loadObject();
		$data = array();
		//$used = false;
		$used_by = json_decode($promocode->used_by, true);
		/* if(!empty($promocode)) {			
			if(!empty($used_by)) {
				$columns = array_column($used_by, 'user_id');
				$k = array_search($user->id, $columns);
				if(isset($k) && $k >= 0 && $used_by[$k]['user_id'] == $user->id) {
					$used = true;
				}
			}
		} */
		
		$saved = false;
		//if(!$used) {
			$data['user_id'] = $user->id;
			$data['promo_type'] = $promo_type;
			$data['quotation_id'] = $quotation_id;
			$used_by[] = $data;
			
			$query = $db->getQuery(TRUE)
				->update("#__insure_promocodes")
				->set("used_by = " . $db->quote(json_encode($used_by)))
				->where("id = " . (int) $promocode->id);
			$db->setQuery($query);
			if ($db->execute()) {
				$saved = true;
			} 
		//}
		return $saved;
	}
	//Un use promotion code
	public static function unUsedPromoCode($promo_code) {
		$db    		=  JFactory::getDBO();
		$user = JFactory::getUser();
		$query 		= $db->getQuery(true);
		$cost = 0;
		$query->select('*')
				->from($db->quoteName('#__insure_promocodes'))
				->where($db->quoteName('code') . ' = ' . $db->quote($promo_code))		
				->where($db->quoteName('publish') . ' <> 0');		
		$db->setQuery($query);
		
		$promocode = $db->loadObject();
		$data = array();
		$used = false;
		$used_by = json_decode($promocode->used_by, true);
		if(!empty($promocode)) {			
			if(!empty($used_by)) {
				$columns = array_column($used_by, 'user_id');
				$k = array_search($user->id, $columns);
				if(isset($k) && $k >= 0 && $used_by[$k]['user_id'] == $user->id) {
					unset($used_by[$k]);
					$i = 0;
					foreach($used_by as $used){
						$data[$i]['user_id'] = $used['id'];
						$data[$i]['promo_type'] = $used['promo_type'];
						$data[$i]['quotation_id'] = $used['quotation_id'];
						$i++;
					}
					$query = $db->getQuery(TRUE)
							->update("#__insure_promocodes")
							->set("used_by = " . $db->quote(json_encode($data)))
							->where("id = " . $db->quote($promocode->id));
					$db->setQuery($query);
					$db->execute();
				}
			}
		}
	}
	
	public static function getZone($country_code) {
		$db    =  JFactory::getDBO();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select('c.value');
		$query->from($db->quoteName('#__insure_travel_countries') . ' AS c');
		$query->where($db->qn('c.variable') . ' = ' . $db->q($country_code));
		$db->setQuery($query);
		$country = $db->loadResult();
		
		return $country;
	}
	public static function dateDiffNumbers($start_date, $end_date) {
		$diff = 0;
		$start = strtotime(str_replace('/','-',$start_date));
		$end = strtotime(str_replace('/','-',$end_date));
		if($end > $start) {
			$count = floor($end - $start)/3600/24;
			$diff = ceil($count);
		}
		return $diff;
	}
	public static function getLifeAmount($quotation_id) {
		$lifeParams = JComponentHelper::getParams('com_insurediylife');
		$month = $lifeParams->get('premium_month', 1);
		$query = " SELECT price FROM #__insure_life_quotation_plan_xref WHERE quotation_id = '$quotation_id' ";
		$db = JFactory::getDbo();
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		
		$amount = 0.00;
		foreach ($rows as $r):
			$amount += (float) $r->price;
		endforeach;
		
		return $amount * (float) $month;
	}
	
	//Check bought insurance
	public static function getTravellerId($user_id, $id_no) {
		$db    		=  JFactory::getDBO();
		$table = '#__insure_travel_travellers';
		$query 		= $db->getQuery(true);
		
		$query->select('traveller_id')
				->from($db->quoteName($table))
				->where($db->quoteName('user_id') . ' = ' . (int) $user_id)
				->where($db->quoteName('id_no') . ' = ' . $db->q($id_no));
		$db->setQuery($query);

		$traveller = $db->loadResult();
		return $traveller;
	}
	
	public static function countUsed($id, $array) {
		$numbers = array_filter($array, function($var) use ($id) {
			return $id === $var['user_id'];
		});
		return count($numbers);
	}
	
}
