<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

require_once JPATH_COMPONENT . '/controller.php';

/**
 * Profile controller class for Users.
 *
 * @package     Joomla.Site
 * @subpackage  com_users
 * @since       1.6
 */
class UsersControllerPendingQuo extends UsersController {

	/**
	 * Method to check out a user for editing and redirect to the edit form.
	 *
	 * @since   1.6
	 */
	public function edit() {
		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		$loginUserId = (int) $user->get('id');

		// Get the previous user id (if any) and the current user id.
		$previousId = (int) $app->getUserState('com_users.edit.profile.id');
		$userId = $this->input->getInt('user_id', null, 'array');

		// Check if the user is trying to edit another users profile.
		if ($userId != $loginUserId) {
			JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
			return false;
		}

		$cookieLogin = $user->get('cookieLogin');

		// Check if the user logged in with a cookie
		if (!empty($cookieLogin)) {
			// If so, the user must login to edit the password and other data.
			$app->enqueueMessage(JText::_('JGLOBAL_REMEMBER_MUST_LOGIN'), 'message');
			$this->setRedirect(JRoute::_('index.php?option=com_users&view=login', false));

			return false;
		}

		// Set the user id for the user to edit in the session.
		$app->setUserState('com_users.edit.profile.id', $userId);

		// Get the model.
		$model = $this->getModel('Profile', 'UsersModel');

		// Check out the user.
		if ($userId) {
			$model->checkout($userId);
		}

		// Check in the previous user.
		if ($previousId) {
			$model->checkin($previousId);
		}

		// Redirect to the edit screen.
		$this->setRedirect(JRoute::_('index.php?option=com_users&view=profile&layout=edit', false));
	}

	/**
	 * Method to delete quotation 
	 *
	 * There are 6 sections. Please delete corresponding section
	 *
	 * 1. Life Insurance  -> 'life'
	 * 2. Critical Illness Insurance -> 'ci'
	 * 3. Hospital Insurance -> 'hospital'
	 * 4. Travel Insurance -> 'travel'
	 * 5. Motor Insurance -> 'motor'
	 * 6. Domestic Helper Insurance -> 'domestic'
	 *
	 * @return  void
	 * @since   1.6
	 */
	public function delete() {
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$app = JFactory::getApplication();
		$model = $this->getModel('PendingQuo', 'UsersModel');
		$user = JFactory::getUser();

		if (!$user->get('guest')) { // not guest
			//$tmp = 	$this->input->getVar('quote', null,'array');
			$data = $app->input->post->get('quote', array(), 'array');

			if ($model->delete($data)) {
				$msg = 'Quotation has been removed';
			} else {
				$msg = 'Quotation has not been removed';
			}
		} else {
			$msg = ' Invalid URL. Cannot directly remove from URL';
		}

		$this->setRedirect(JRoute::_('index.php?option=com_users&view=pendingquo', false), $msg);
		return false;
	}

	protected function postSaveHook(JModelLegacy $model, $validData = array()) {
		$item = $model->getData();
		$tags = $validData['tags'];

		if ($tags) {
			$item->tags = new JHelperTags;
			$item->tags->getTagIds($item->id, 'com_users.user');
			$item->metadata['tags'] = $item->tags;
		}
	}

}
