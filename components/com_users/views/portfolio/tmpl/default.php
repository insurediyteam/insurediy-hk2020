<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::_('jquery.framework');
$data = $this->data;
$user = JFactory::getUser();
?>
<script type="text/javascript">
	window.addEvent('domready', function () {

	});

	function deletePolicy(id) {
		window.PolicyForm.task.value = "policy.delete";
		window.PolicyForm.id.value = id;
		window.PolicyForm.submit();
	}

	function deleteAll() {
		window.PolicyForm.task.value = "policy.deleteall";
		window.PolicyForm.submit();
	}
</script>
<div class="portfolio <?php echo $this->pageclass_sfx ?>">
	<?php if ($this->params->get('show_page_heading')) : ?>
		<div class="header-top">
			<div class="h1-heading"><?php echo $this->escape($this->params->get('page_heading')); ?></div>
			<div class="customer-dashboard-menu">{modulepos insurediy-my-dashboard-menu}</div>
		</div>
	<?php else: ?>
		<div class="header-top">
			<div class="h1-heading"><?php echo JText::_("COM_USERS_PAGE_TITLE_PORTFOLIO"); ?></div>
			<div class="customer-dashboard-menu">{modulepos insurediy-my-dashboard-menu}</div>
			<div class="clear"></div>
		</div>
	<?php endif; ?>

	<form id="PolicyForm" name="PolicyForm" action="<?php echo JRoute::_('index.php'); ?>" method="post" class="form-validate form-horizontal" enctype="multipart/form-data">
		<div class="padding20">
			<div class="btn-group" style="margin-bottom:20px;">
				<a style="background:transparent;border:0;" href="<?php echo JRoute::_("index.php?Itemid=205") ?>">
					<img src="images/websiteicon/add-new-policy.png" />
				</a>
				<button onclick="javascript:jQuery('#emailall_popup').modal('show');" type="button" style="background:transparent;border:0;" >
					<img src="images/websiteicon/email-all.png" />
				</button>
				<a style="background:transparent;border:0;" href="javascript:void(0);" onclick="javascript:deleteAll();">
					<img src="images/websiteicon/delete-policy.png" />
				</a>
			</div>

			<div id="term" class="customer-ins customer-insurance-life"><?php echo JText::_("COM_USERS_INSURANCE_NAMES_TERM"); ?></div>
			<div class="whitebg radius-top border-all marginbottom20">
				<div class="header2bg">
					<div class="item-chxbox">&nbsp;</div>
					<div class="item-insurer"><?php echo JText::_('COM_USERS_HEADER_TEXT_INSURER'); ?></div>
					<div class="item-policy"><?php echo JText::_('COM_USERS_HEADER_TEXT_POLICY_NO'); ?></div>
					<div class="item-cover-amt"><?php echo JText::_('COM_USERS_HEADER_TEXT_COVER_AMOUNT'); ?></div>
					<div class="item-start-date"><?php echo JText::_('COM_USERS_HEADER_TEXT_START_DATE'); ?></div>
					<div class="item-end-date"><?php echo JText::_('COM_USERS_HEADER_TEXT_END_DATE'); ?></div>
					<div class="item-status"><?php echo JText::_('COM_USERS_HEADER_TEXT_STATUS'); ?></div>
					<div class="item-action"><?php echo JText::_('COM_USERS_HEADER_TEXT_ACTION'); ?></div>
					<div class="clear"></div>
				</div>
				<?php
				if (!empty($data['life'])) :
					foreach ($data['life'] as $key => $value) :
						$edit_url = "index.php?option=com_users&view=policy&layout=edit&id=" . $value->id;
						$status = (in_array($value->status, array(0, 1, 2))) ? JText::_("LD_POLICY_STATUS_OPTION_" . $value->status) : "N/A";
						?>
						<div class="<?php echo ($key % 2) ? 'even' : 'odd'; ?>">
							<div class="item-chxbox"><input type="checkbox" class="customer-chxbox-quotation" id="cid" name="cid[]" value="<?php echo $value->id ?>" /></div>
							<div class="item-insurer"><?php echo $value->insurer; ?></div>
							<div class="item-policy"><?php echo $value->policy_number; ?></div>
							<div class="item-cover-amt"><?php echo "HK$ " . $value->cover_amt; ?></div>
							<div class="item-start-date"><?php echo ($value->start_date == "0000-00-00") ? "N/A" : JHtml::date(strtotime($value->start_date), 'd-m-Y'); ?></div>
							<div class="item-end-date"><?php echo ($value->end_date == "0000-00-00") ? "N/A" : JHtml::date(strtotime($value->end_date), 'd-m-Y'); ?></div>
							<div class="item-status"><?php echo $status; ?></div>
							<div class="item-action"><a style="margin-right:5px;" href="<?php echo $edit_url; ?>"><img src="images/websiteicon/edit.png"></a><a href="javascript:void(0);" onclick="javascript:deletePolicy('<?php echo $value->id ?>');"><img src="images/websiteicon/delete.png"></a></div>
							<div class="clear"></div>
						</div>
					<?php endforeach; ?>
				<?php else : ?>
					<div class="odd">
						<div class="no-items"><?php echo JText::_("JNO") . " " . JText::_("COM_USERS_INSURANCE_NAMES_TERM"); ?></div>
					</div>
				<?php endif; ?>
			</div>

			<div id="ci" class="customer-ins customer-insurance-ci"><?php echo JText::_("COM_USERS_INSURANCE_NAMES_CI"); ?></div>

			<div class="whitebg radius-top border-all marginbottom20">
				<div class="header2bg">
					<div class="item-chxbox">&nbsp;</div>
					<div class="item-insurer"><?php echo JText::_('COM_USERS_HEADER_TEXT_INSURER'); ?></div>
					<div class="item-policy"><?php echo JText::_('COM_USERS_HEADER_TEXT_POLICY_NO'); ?></div>
					<div class="item-cover-amt"><?php echo JText::_('COM_USERS_HEADER_TEXT_COVER_AMOUNT'); ?></div>
					<div class="item-start-date"><?php echo JText::_('COM_USERS_HEADER_TEXT_START_DATE'); ?></div>
					<div class="item-end-date"><?php echo JText::_('COM_USERS_HEADER_TEXT_END_DATE'); ?></div>
					<div class="item-status"><?php echo JText::_('COM_USERS_HEADER_TEXT_STATUS'); ?></div>
					<div class="item-action"><?php echo JText::_('COM_USERS_HEADER_TEXT_ACTION'); ?></div>
					<div class="clear"></div>
				</div>
				<?php
				if (!empty($data['ci'])) :
					foreach ($data['ci'] as $key => $value) :
						$edit_url = "index.php?option=com_users&view=policy&id=" . $value->id;
						$status = (in_array($value->status, array(0, 1, 2))) ? JText::_("LD_POLICY_STATUS_OPTION_" . $value->status) : "N/A";
						?>
						<div class="<?php echo ($key % 2) ? 'even' : 'odd'; ?>">
							<div class="item-chxbox"><input type="checkbox" class="customer-chxbox-quotation" id="ci-<?php echo $value->id ?>" name="cid[]" value="<?php echo $value->id ?>" /></div>
							<div class="item-insurer"><?php echo $value->insurer; ?></div>
							<div class="item-policy"><?php echo $value->policy_number; ?></div>
							<div class="item-cover-amt"><?php echo "HK$ " . $value->cover_amt; ?></div>
							<div class="item-start-date"><?php echo ($value->start_date == "0000-00-00") ? "N/A" : JHtml::date(strtotime($value->start_date), 'd-m-Y'); ?></div>
							<div class="item-end-date"><?php echo ($value->end_date == "0000-00-00") ? "N/A" : JHtml::date(strtotime($value->end_date), 'd-m-Y'); ?></div>
							<div class="item-status"><?php echo $status; ?></div>
							<div class="item-action"><a style="margin-right:5px;" href="<?php echo $edit_url; ?>"><img src="images/websiteicon/edit.png"></a><a href="javascript:void(0);" onclick="javascript:deletePolicy('<?php echo $value->id ?>');"><img src="images/websiteicon/delete.png"></a></div>
							<div class="clear"></div>
						</div>
					<?php endforeach; ?>
				<?php else : ?>
					<div class="odd">
						<div class="no-items"><?php echo JText::_("JNO") . " " . JText::_("COM_USERS_INSURANCE_NAMES_CI"); ?></div>
					</div>
				<?php endif; ?>
			</div>

			<div class="customer-ins customer-insurance-hospital"><?php echo JText::_("COM_USERS_INSURANCE_NAMES_HOSPITAL"); ?></div>

			<div class="whitebg radius-top border-all marginbottom20">
				<div class="header2bg">
					<div class="item-chxbox">&nbsp;</div>
					<div class="item-insurer"><?php echo JText::_('COM_USERS_HEADER_TEXT_INSURER'); ?></div>
					<div class="item-policy"><?php echo JText::_('COM_USERS_HEADER_TEXT_POLICY_NO'); ?></div>
					<div class="item-cover-amt"><?php echo JText::_('COM_USERS_HEADER_TEXT_COVER_AMOUNT'); ?></div>
					<div class="item-start-date"><?php echo JText::_('COM_USERS_HEADER_TEXT_START_DATE'); ?></div>
					<div class="item-end-date"><?php echo JText::_('COM_USERS_HEADER_TEXT_END_DATE'); ?></div>
					<div class="item-status"><?php echo JText::_('COM_USERS_HEADER_TEXT_STATUS'); ?></div>
					<div class="item-action"><?php echo JText::_('COM_USERS_HEADER_TEXT_ACTION'); ?></div>
					<div class="clear"></div>
				</div>
				<?php
				if (!empty($data['hospital'])) :
					foreach ($data['hospital'] as $key => $value) :
						$edit_url = "index.php?option=com_users&view=policy&id=" . $value->id;
						$status = (in_array($value->status, array(0, 1, 2))) ? JText::_("LD_POLICY_STATUS_OPTION_" . $value->status) : "N/A";
						$cover_type = (in_array($value->cover_type, array(1, 2, 3, 4))) ? JText::_("LD_COVER_TYPE_OPTION_" . $value->cover_type) : "N/A";
						?>
						<div class="<?php echo ($key % 2) ? 'even' : 'odd'; ?>">
							<div class="item-chxbox"><input type="checkbox" class="customer-chxbox-quotation" id="hospital-<?php echo $value->id ?>" name="cid[]" value="<?php echo $value->id ?>" /></div>
							<div class="item-insurer"><?php echo $value->insurer; ?></div>
							<div class="item-policy"><?php echo $value->policy_number; ?></div>
							<div class="item-cover-amt"><?php echo $cover_type; ?></div>
							<div class="item-start-date"><?php echo ($value->start_date == "0000-00-00") ? "N/A" : JHtml::date(strtotime($value->start_date), 'd-m-Y'); ?></div>
							<div class="item-end-date"><?php echo ($value->end_date == "0000-00-00") ? "N/A" : JHtml::date(strtotime($value->end_date), 'd-m-Y'); ?></div>
							<div class="item-status"><?php echo $status; ?></div>
							<div class="item-action"><a style="margin-right:5px;" href="<?php echo $edit_url; ?>"><img src="images/websiteicon/edit.png"></a><a href="javascript:void(0);" onclick="javascript:deletePolicy('<?php echo $value->id ?>');"><img src="images/websiteicon/delete.png"></a></div>
							<div class="clear"></div>
						</div>
					<?php endforeach; ?>
				<?php else : ?>
					<div class="odd">
						<div class="no-items"><?php echo JText::_("JNO") . " " . JText::_("COM_USERS_INSURANCE_NAMES_HOSPITAL"); ?></div>
					</div>
				<?php endif; ?>
			</div>

			<div id="travel" class="customer-ins customer-insurance-travel"><?php echo JText::_("COM_USERS_INSURANCE_NAMES_TRAVEL"); ?></div>

			<div class="whitebg radius-top border-all marginbottom20">
				<div class="header2bg">
					<div class="item-chxbox">&nbsp;</div>
					<div class="item-insurer"><?php echo JText::_('COM_USERS_HEADER_TEXT_INSURER'); ?></div>
					<div class="item-policy"><?php echo JText::_('COM_USERS_HEADER_TEXT_POLICY_NO'); ?></div>
					<div class="item-cover-amt"><?php echo JText::_('COM_USERS_HEADER_TEXT_COVER_AMOUNT'); ?></div>
					<div class="item-start-date"><?php echo JText::_('COM_USERS_HEADER_TEXT_START_DATE'); ?></div>
					<div class="item-end-date"><?php echo JText::_('COM_USERS_HEADER_TEXT_END_DATE'); ?></div>
					<div class="item-status"><?php echo JText::_('COM_USERS_HEADER_TEXT_STATUS'); ?></div>
					<div class="item-action"><?php echo JText::_('COM_USERS_HEADER_TEXT_ACTION'); ?></div>
					<div class="clear"></div>
				</div>
				<?php
				if (!empty($data['travel'])) :
					foreach ($data['travel'] as $key => $value) :
						$edit_url = "index.php?option=com_users&view=policy&id=" . $value->id;
						$status = (in_array($value->status, array(0, 1, 2))) ? JText::_("LD_POLICY_STATUS_OPTION_" . $value->status) : "N/A";
						?>
						<div class="<?php echo ($key % 2) ? 'even' : 'odd'; ?>">
							<div class="item-chxbox"><input type="checkbox" class="customer-chxbox-quotation" id="travel-<?php echo $value->id ?>" name="cid[]" value="<?php echo $value->id ?>" /></div>
							<div class="item-insurer"><?php echo $value->insurer; ?></div>
							<div class="item-policy"><?php echo $value->policy_number; ?></div>
							<div class="item-cover-amt"><?php echo JText::_("JNA"); ?></div>
							<div class="item-start-date"><?php echo ($value->start_date == "0000-00-00") ? "N/A" : JHtml::date(strtotime($value->start_date), 'd-m-Y'); ?></div>
							<div class="item-end-date"><?php echo ($value->end_date == "0000-00-00") ? "N/A" : JHtml::date(strtotime($value->end_date), 'd-m-Y'); ?></div>
							<div class="item-status"><?php echo $status; ?></div>
							<div class="item-action"><a style="margin-right:5px;" href="<?php echo $edit_url; ?>"><img src="images/websiteicon/edit.png"></a><a href="javascript:void(0);" onclick="javascript:deletePolicy('<?php echo $value->id ?>');"><img src="images/websiteicon/delete.png"></a></div>
							<div class="clear"></div>
						</div>
					<?php endforeach; ?>
				<?php else : ?>
					<div class="odd">
						<div class="no-items"><?php echo JText::_("JNO") . " " . JText::_("COM_USERS_INSURANCE_NAMES_TRAVEL"); ?></div>
					</div>
				<?php endif; ?>
			</div>

			<div id="motor" class="customer-ins customer-insurance-motor"><?php echo JText::_("COM_USERS_INSURANCE_NAMES_MOTOR"); ?></div>

			<div class="whitebg radius-top border-all marginbottom20">
				<div class="header2bg">
					<div class="item-chxbox">&nbsp;</div>
					<div class="item-insurer"><?php echo JText::_('COM_USERS_HEADER_TEXT_INSURER'); ?></div>
					<div class="item-policy"><?php echo JText::_('COM_USERS_HEADER_TEXT_POLICY_NO'); ?></div>
					<div class="item-cover-amt"><?php echo JText::_('COM_USERS_HEADER_TEXT_COVER_AMOUNT'); ?></div>
					<div class="item-start-date"><?php echo JText::_('COM_USERS_HEADER_TEXT_START_DATE'); ?></div>
					<div class="item-end-date"><?php echo JText::_('COM_USERS_HEADER_TEXT_END_DATE'); ?></div>
					<div class="item-status"><?php echo JText::_('COM_USERS_HEADER_TEXT_STATUS'); ?></div>
					<div class="item-action"><?php echo JText::_('COM_USERS_HEADER_TEXT_ACTION'); ?></div>
					<div class="clear"></div>
				</div>
				<?php
				if (!empty($data['motor'])) :
					foreach ($data['motor'] as $key => $value) :
						$edit_url = "index.php?option=com_users&view=policy&id=" . $value->id;
						$status = (in_array($value->status, array(0, 1, 2))) ? JText::_("LD_POLICY_STATUS_OPTION_" . $value->status) : "N/A";
						?>
						<div class="<?php echo ($key % 2) ? 'even' : 'odd'; ?>">
							<div class="item-chxbox"><input type="checkbox" class="customer-chxbox-quotation" id="motor-<?php echo $value->id ?>" name="cid[]" value="<?php echo $value->id ?>" /></div>
							<div class="item-insurer"><?php echo $value->insurer; ?></div>
							<div class="item-policy"><?php echo $value->policy_number; ?></div>
							<div class="item-cover-amt"><?php echo JText::_("JNA"); ?></div>
							<div class="item-start-date"><?php echo ($value->start_date == "0000-00-00") ? "N/A" : JHtml::date(strtotime($value->start_date), 'd-m-Y'); ?></div>
							<div class="item-end-date"><?php echo ($value->end_date == "0000-00-00") ? "N/A" : JHtml::date(strtotime($value->end_date), 'd-m-Y'); ?></div>
							<div class="item-status"><?php echo $status; ?></div>
							<div class="item-action"><a style="margin-right:5px;" href="<?php echo $edit_url; ?>"><img src="images/websiteicon/edit.png"></a><a href="javascript:void(0);" onclick="javascript:deletePolicy('<?php echo $value->id ?>');"><img src="images/websiteicon/delete.png"></a></div>
							<div class="clear"></div>
						</div>
					<?php endforeach; ?>
				<?php else : ?>
					<div class="odd">
						<div class="no-items"><?php echo JText::_("JNO") . " " . JText::_("COM_USERS_INSURANCE_NAMES_MOTOR"); ?></div>
					</div>
				<?php endif; ?>
			</div>

			<div id="domestic" class="customer-ins customer-insurance-maid"><?php echo JText::_("COM_USERS_INSURANCE_NAMES_DOMESTIC"); ?></div>

			<div class="whitebg radius-top border-all marginbottom20">
				<div class="header2bg">
					<div class="item-chxbox">&nbsp;</div>
					<div class="item-insurer"><?php echo JText::_('COM_USERS_HEADER_TEXT_INSURER'); ?></div>
					<div class="item-policy"><?php echo JText::_('COM_USERS_HEADER_TEXT_POLICY_NO'); ?></div>
					<div class="item-cover-amt"><?php echo JText::_('COM_USERS_HEADER_TEXT_COVER_AMOUNT'); ?></div>
					<div class="item-start-date"><?php echo JText::_('COM_USERS_HEADER_TEXT_START_DATE'); ?></div>
					<div class="item-end-date"><?php echo JText::_('COM_USERS_HEADER_TEXT_END_DATE'); ?></div>
					<div class="item-status"><?php echo JText::_('COM_USERS_HEADER_TEXT_STATUS'); ?></div>
					<div class="item-action"><?php echo JText::_('COM_USERS_HEADER_TEXT_ACTION'); ?></div>
					<div class="clear"></div>
				</div>
				<?php
				if (!empty($data['domestic'])) :
					foreach ($data['domestic'] as $key => $value) :
						$edit_url = "index.php?option=com_users&view=policy&id=" . $value->id;
						$status = (in_array($value->status, array(0, 1, 2))) ? JText::_("LD_POLICY_STATUS_OPTION_" . $value->status) : "N/A";
						?>
						<div class="<?php echo ($key % 2) ? 'even' : 'odd'; ?>">
							<div class="item-chxbox"><input type="checkbox" class="customer-chxbox-quotation" id="domestic-<?php echo $value->id ?>" name="cid[]" value="<?php echo $value->id ?>" /></div>
							<div class="item-insurer"><?php echo $value->insurer; ?></div>
							<div class="item-policy"><?php echo $value->policy_number; ?></div>
							<div class="item-cover-amt"><?php echo JText::_("JNA"); ?></div>
							<div class="item-start-date"><?php echo ($value->start_date == "0000-00-00") ? "N/A" : JHtml::date(strtotime($value->start_date), 'd-m-Y'); ?></div>
							<div class="item-end-date"><?php echo ($value->end_date == "0000-00-00") ? "N/A" : JHtml::date(strtotime($value->end_date), 'd-m-Y'); ?></div>
							<div class="item-status"><?php echo $status; ?></div>
							<div class="item-action"><a style="margin-right:5px;" href="<?php echo $edit_url; ?>"><img src="images/websiteicon/edit.png"></a><a href="javascript:void(0);" onclick="javascript:deletePolicy('<?php echo $value->id ?>');"><img src="images/websiteicon/delete.png"></a></div>
							<div class="clear"></div>
						</div>
					<?php endforeach; ?>
				<?php else : ?>
					<div class="odd">
						<div class="no-items"><?php echo JText::_("JNO") . " " . JText::_("COM_USERS_INSURANCE_NAMES_DOMESTIC"); ?></div>
					</div>
				<?php endif; ?>
			</div>

			<div id="others" class="customer-ins customer-insurance-maid"><?php echo JText::_("COM_USERS_INSURANCE_NAMES_OTHERS"); ?></div>

			<div class="whitebg radius-top border-all marginbottom20">
				<div class="header2bg">
					<div class="item-chxbox">&nbsp;</div>
					<div class="item-insurer"><?php echo JText::_('COM_USERS_HEADER_TEXT_INSURER'); ?></div>
					<div class="item-policy"><?php echo JText::_('COM_USERS_HEADER_TEXT_POLICY_NO'); ?></div>
					<div class="item-cover-amt"><?php echo JText::_('COM_USERS_HEADER_TEXT_COVER_AMOUNT'); ?></div>
					<div class="item-start-date"><?php echo JText::_('COM_USERS_HEADER_TEXT_START_DATE'); ?></div>
					<div class="item-end-date"><?php echo JText::_('COM_USERS_HEADER_TEXT_END_DATE'); ?></div>
					<div class="item-status"><?php echo JText::_('COM_USERS_HEADER_TEXT_STATUS'); ?></div>
					<div class="item-action"><?php echo JText::_('COM_USERS_HEADER_TEXT_ACTION'); ?></div>
					<div class="clear"></div>
				</div>
				<?php
				if (!empty($data['others'])) :
					foreach ($data['others'] as $key => $value) :
						$edit_url = "index.php?option=com_users&view=policy&id=" . $value->id;
						$status = (in_array($value->status, array(0, 1, 2))) ? JText::_("LD_POLICY_STATUS_OPTION_" . $value->status) : "N/A";
						?>
						<div class="<?php echo ($key % 2) ? 'even' : 'odd'; ?>">
							<div class="item-chxbox"><input type="checkbox" class="customer-chxbox-quotation" id="others-<?php echo $value->id ?>" name="cid[]" value="<?php echo $value->id ?>" /></div>
							<div class="item-insurer"><?php echo $value->insurer; ?></div>
							<div class="item-policy"><?php echo $value->policy_number; ?></div>
							<div class="item-cover-amt"><?php echo JText::_("JNA"); ?></div>
							<div class="item-start-date"><?php echo ($value->start_date == "0000-00-00") ? "N/A" : JHtml::date(strtotime($value->start_date), 'd-m-Y'); ?></div>
							<div class="item-end-date"><?php echo ($value->end_date == "0000-00-00") ? "N/A" : JHtml::date(strtotime($value->end_date), 'd-m-Y'); ?></div>
							<div class="item-status"><?php echo $status; ?></div>
							<div class="item-action"><a style="margin-right:5px;" href="<?php echo $edit_url; ?>"><img src="images/websiteicon/edit.png"></a><a href="javascript:void(0);" onclick="javascript:deletePolicy('<?php echo $value->id ?>');"><img src="images/websiteicon/delete.png"></a></div>
							<div class="clear"></div>
						</div>
					<?php endforeach; ?>
				<?php else : ?>
					<div class="odd">
						<div class="no-items"><?php echo JText::_("JNO") . " " . JText::_("COM_USERS_INSURANCE_NAMES_OTHERS"); ?></div>
					</div>
				<?php endif; ?>
			</div>

			<div class="form-actions" style="padding:20px 0;">
				<div style="float:left;">
					{modulepos insurediy-secured-ssl}
				</div>
				<input type="hidden" name="id" value/>
				<input type="hidden" name="option" value="com_users" />
				<input type="hidden" name="task" value="policy.delete" />
				<?php echo JHtml::_('form.token'); ?>
				<div class="clear"></div>
			</div>
		</div>
	</form>
</div>


<div id="emailall_popup" class="modal hide fade custom-modal">
	<div class="modal-body custom-modal-body-1">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<form id="EmailAllForm" name="EmailAllForm" action="<?php echo JRoute::_('index.php'); ?>" method="post" enctype="multipart/form-data">
			<span style="font-family: 'sourcesanspro-regular'; font-size:15px;">
				<?php echo JText::_("COM_USERS_PORTFOLIO_EMAIL_ALL_EXPLANATION"); ?> 
			</span>
			<br/>
			<br/>
			<span><?php echo JText::_("COM_USERS_PORTFOLIO_EMAIL_ALL_INPUT_LABEL"); ?> </span>
			<input style="" validate="email" value="<?php echo $user->email; ?>" type="text" name="email" id="life-multiplier" />
			<input type="hidden" name="option" value="com_users" />
			<input type="hidden" name="task" value="policy.emailall" />
			<br/>
			<?php echo JHtml::_('form.token'); ?>
			<a class="btn btn-info" href="javascript: void(0);" onclick="javascript:window.EmailAllForm.submit();" data-dismiss="modal"><?php echo JText::_("JSUBMIT"); ?></a>
		</form>
		<br>
	</div>
</div>