<?php

defined('_JEXEC') or die;

class UsersViewDashboard extends JViewLegacy {

	protected $data;
	protected $form;
	protected $params;
	protected $state;

	public function display($tpl = null) {

		$this->state = $this->get('State');
		$this->params = $this->state->get('params');
		$this->user = JFactory::getUser();
		$this->data = $this->get('data');
		$this->points = $this->get('points');

		$this->ciInfo = $this->get('CiInfo');
		$this->lifeInfo = $this->get('lifeInfo');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		$this->prepareDocument();

		return parent::display($tpl);
	}

	protected function prepareDocument() {
		$app = JFactory::getApplication();
		$menus = $app->getMenu();
		$user = JFactory::getUser();
		$title = null;

		$menu = $menus->getActive();

		if ($menu) {
			$this->params->def('page_heading', $this->params->get('page_title', $user->name));
		} else {
			$this->params->def('page_heading', JText::_('COM_USERS_PROFILE'));
		}

		$title = $this->params->get('page_title', '');

		if (empty($title)) {
			$title = $app->getCfg('sitename');
		} elseif ($app->getCfg('sitename_pagetitles', 0) == 1) {
			$title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
		} elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
			$title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
		}

		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description')) {
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords')) {
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots')) {
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}

		$urls = MyUri::getUrls();
		$this->document->addScript($urls['js'] . 'gauge/gauge.js');

		$this->document->addScript($urls['js'] . 'justgage/raphael.2.1.0.min.js');
		$this->document->addScript($urls['js'] . 'justgage/justgage.1.0.1.js');
	}

}
