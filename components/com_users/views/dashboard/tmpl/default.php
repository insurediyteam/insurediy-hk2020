<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
JHtml::_('jquery.framework');
$this->pageclass_sfx = isset($this->pageclass_sfx) ? $this->pageclass_sfx : "";
$data = $this->data;
// Prepare some stuffs
$life_ideal_amt = (isset($this->lifeInfo->total_life)) ? $this->lifeInfo->total_life : InsureDIYHelper::getMonthlySalary() * 12 * 10;
$ci_ideal = $this->ciInfo->annual_salary * $this->ciInfo->years;
$points = $this->points;
?>
<?php if($data["3"] <= 1):  ?>
<style>
	#h-gauge{
		background-image: url("<?php echo JURI::root() ?>images/dashboard/hospital-chart-grey.png");
	}
</style>
<?php endif; ?>
<div class="dashboard <?php echo $this->pageclass_sfx ?>">
	<?php if ($this->params->get('show_page_heading')) : ?>
		<div class="header-top">
			<div class="h1-heading"><?php echo $this->escape($this->params->get('page_heading')); ?></div>
			<div class="customer-dashboard-menu">{modulepos insurediy-my-dashboard-menu}</div>
		</div>
	<?php else: ?>
		<div class="header-top">
			<div class="h1-heading"><?php echo JText::_("COM_USERS_HEADER_TEXT_DASHBOARD"); ?></div>
			<div class="customer-dashboard-menu">{modulepos insurediy-my-dashboard-menu}</div>
			<div class="clear"></div>
		</div>
	<?php endif; ?>
	<div style="padding:10px 5px;" class="clearfix">
		<div id="dashboard-area-1" class="clearfix">
			<div id="white-container-1" class="border-box">
				<div class="white-container radius-top">
					<div class="white-container-header2 radius-top">
						<?php echo JText::_("COM_USERS_DASHBOARD_HEADER_MESSAGE"); ?>
					</div>
					<div class="white-container-body">
						<?php echo MyHelper::load_module("120"); ?>
					</div>
				</div>
			</div>
			<div id="white-container-2" class="border-box">
				<div class="white-container radius-top">
					<div class="white-container-header2 radius-top">
						<?php echo JText::_("COM_USERS_DASHBOARD_HEADER_REWARD_POINTS"); ?>
					</div>
					<div class="white-container-body">
						<div class="centralized" style="height:100px;text-align: center;">
							<div class="points">
								<?php echo MyHelper::numberFormat(InsureDIYHelper::getUserPoints($this->user->id), FALSE, 0); ?>
							</div><br>
							<a class="btn-primary btn-custom-small" style="" href="<?php echo JRoute::_("index.php?Itemid=184") ?>"><?php echo JText::_("COM_USERS_DASHBOARD_MISC_REDEEM_NOW"); ?></a> &nbsp;
							<a class="btn-primary btn-custom-small" style="" onclick="javascript:jQuery('#points_popup').modal('show');" href="#"><?php echo JText::_("COM_USERS_DASHBOARD_MISC_POINT_HISTORY"); ?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="spacer-1"></div>
		<div id="dashboard-area-2" class="clearfix">
			<div class="cpan3">
				<div class="white-container">
					<div class="white-container-header clearfix">
						<?php echo JText::_("POLICY_TYPE_1"); ?> <a class="pull-right" href="<?php echo JRoute::_("index.php?Itemid=156") ?>"><img src="images/icon-cart-buy.png" alt=""/></a>
					</div>
					<div class="white-container-body" style="padding: 0px;">
						<div class="chart-area-1">
							<div id="life-gauge" style="width:263px;height:152px;margin: auto;"></div>
						</div>
						<div class="white-container-footer center">
							<?php echo JText::_("MISC_IDEAL"); ?>&nbsp;
							<img class="hasTooltip" style="margin-bottom: 5px;" src="images/help-quote.png" alt="help-quote" data-original-title="<?php echo JText::_("COM_USERS_TERM_INSURANCE_IDEAL_TOOLTIPS"); ?>" />
							&nbsp;:&nbsp;<span id="life-ideal-text" class="blue-curreny-text"><?php echo InsureDIYHelper::toMoneyFormat($life_ideal_amt, 0) ?></span>
							<a href="javascript: void(0);" onclick="javascript:jQuery('#life_popup').modal('show');" style="float: right;"><img src="images/icon-edit-2.png" /></a>
						</div>
					</div>
				</div>
			</div>
			<div class="cpan3">
				<div class="white-container">
					<div class="white-container-header clearfix">
						<?php echo JText::_("POLICY_TYPE_2"); ?> <a class="pull-right" href="<?php echo JRoute::_("index.php?Itemid=157") ?>"><img src="images/icon-cart-buy.png" alt=""/></a>
					</div>
					<div class="white-container-body" style="padding: 0px;">
						<div class="chart-area-1">
							<div id="ci-gauge" style="width:263px;height:152px;margin: auto;"></div>
						</div>
						<div class="white-container-footer center">
							<?php echo JText::_("MISC_IDEAL"); ?>&nbsp;
							<img class="hasTooltip" style="margin-bottom: 5px;" src="images/help-quote.png" alt="help-quote" data-original-title="<?php echo JText::_("COM_USERS_CI_INSURANCE_IDEAL_TOOLTIPS"); ?>" />
							&nbsp;:&nbsp;<span id="ci-ideal-text" class="blue-curreny-text"><?php echo InsureDIYHelper::toMoneyFormat($ci_ideal, 0) ?></span>
							<a href="javascript: void(0);" onclick="javascript:jQuery('#ci_popup').modal('show');" style="float: right;"><img src="images/icon-edit-2.png" /></a>
						</div>
					</div>
				</div>
			</div>
			<div class="cpan3">
				<div class="white-container">
					<div class="white-container-header clearfix">
						<?php echo JText::_("POLICY_TYPE_3"); ?> <a class="pull-right" href="<?php echo JRoute::_("index.php?Itemid=213") ?>"><img src="images/icon-cart-buy.png" alt=""/></a>
					</div>
					<div class="white-container-body" style="padding: 0px;">
						<div class="chart-area-2">
							<canvas id="h-gauge" style="width:263px;height:132px;"></canvas>
						</div>
						<div class="clearfix" style="padding: 10px 30px; font-size:13px; font-family:'Arial'; color: #2f2f2f; ">
							<div>
								<div class="span6">
									<span class="colorlegend" style="background-color: #e14343;"></span><?php echo JText::_("MISC_WARD"); ?>
								</div>
								<div class="span6">
									<span class="colorlegend" style="background-color: #e18243;"></span><?php echo JText::_("MISC_SEMI_PRIVATE"); ?>
								</div>
							</div>
							<div>
								<div class="span6">
									<span class="colorlegend" style="background-color: #fad400;"></span><?php echo JText::_("MISC_PRIVATE"); ?>
								</div>
								<div class="span6">
									<span class="colorlegend" style="background-color: #81d047;"></span><?php echo JText::_("MISC_INTERNATIONAL"); ?>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="spacer-1"></div>
		<div id="dashboard-area-3" class="clearfix">
			<div class="cpan3">
				<div class="white-container">
					<div class="white-container-header clearfix">
						<?php echo JText::_("POLICY_TYPE_4"); ?> <a class="pull-right" href="<?php echo JRoute::_("index.php?Itemid=159") ?>"><img src="images/icon-cart-buy.png" alt=""/></a>
					</div>
					<div class="white-container-body">
						<?php if ($data["4"]): ?>
							<div class="centralized center" style="height:160px;width:110px;">
								<div style="height: 109px">
									<img src="images/dashboard/travel-covered.png" style="height:102px;width: 110px"/>
								</div>
								<div class="spacer-2"></div>
								<div class="spacer-1"></div>
								<span class="black-uppercased"><?php echo JText::_("MISC_COVERED"); ?></span>
							</div>
						<?php else: ?>
							<div class="centralized center" style="height:160px;width:110px;">
								<div style="height: 109px">
									<img  src="images/dashboard/travel.png" style="height:102px;width: 110px" />
								</div>
								<div class="spacer-2"></div>
								<div class="spacer-1"></div>
								<span class="gray-uppercased"><?php echo JText::_("MISC_NOT_COVERED"); ?></span>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="cpan3">
				<div class="white-container">
					<div class="white-container-header clearfix">
						<?php echo JText::_("POLICY_TYPE_5"); ?> <a class="pull-right" href="#"><img src="images/icon-cart-buy.png" alt=""/></a>
					</div>
					<div class="white-container-body">
						<?php if ($data["5"]): ?>
							<div class="centralized center" style="height:160px;width:115px;">
								<div style="height: 109px">
									<img src="images/dashboard/motor-covered.png" style="height:97px;width: 111px"/>
								</div>
								<div class="spacer-2"></div>
								<div class="spacer-1"></div>
								<span class="black-uppercased"><?php echo JText::_("MISC_COVERED"); ?></span>
							</div>
						<?php else: ?>
							<div class="centralized center" style="height:160px;width:115px;">
								<div style="height: 109px">
									<img  src="images/dashboard/motor.png" style="height:114px;width: 111px" />
								</div>
								<div class="spacer-2"></div>
								<div class="spacer-1"></div>
								<span class="gray-uppercased"><?php echo JText::_("MISC_NOT_COVERED"); ?></span>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<div class="cpan3">
				<div class="white-container">
					<div class="white-container-header clearfix">
						<?php echo JText::_("POLICY_TYPE_6"); ?> <a class="pull-right" href="<?php echo JRoute::_("index.php?Itemid=161") ?>"><img src="images/icon-cart-buy.png" alt=""/></a>
					</div>
					<div class="white-container-body">
						<?php if ($data["6"]): ?>
							<div class="centralized center" style="height:160px;width:100px;">
								<div style="height: 109px">
									<img src="images/dashboard/domestic-covered.png" style="height:114px;width: 87px"/>
								</div>
								<div class="spacer-2"></div>
								<div class="spacer-1"></div>
								<span class="black-uppercased"><?php echo JText::_("MISC_COVERED"); ?></span>
							</div>
						<?php else: ?>
							<div class="centralized center" style="height:160px;width:100px;">
								<div style="height: 109px">
									<img  src="images/dashboard/domestic.png" style="height:114px;width: 87px" />
								</div>
								<div class="spacer-2"></div>
								<div class="spacer-1"></div>
								<span class="gray-uppercased"><?php echo JText::_("MISC_NOT_COVERED"); ?></span>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
		<div style="float:left;margin:15px 5px;">
			{modulepos insurediy-secured-ssl}
		</div>
	</div>
</div>

<div id="points_popup" class="modal hide fade custom-modal">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<div class="modal-title">
			<h3><?php echo JText::_("COM_USERS_DASHBOARD_HEADER_POINT_HISTORY"); ?></h3>
		</div>
	</div>
	<div class="modal-body">
		<?php if (count($points) > 0): ?>
			<table class="table">
				<thead>
					<tr>
						<th>No</th>
						<th>Date</th>
						<th>Description</th>
						<th>Points</th>
					</tr>
				</thead>
				<tbody>
					<?php
					$count = 1;
					foreach ($points as $point):
						?>
						<tr>
							<td>
								<?php echo $count; ?>
							</td>
							<td>
								<?php echo JHtml::date($point['created'], "d / m / Y H:i:s"); ?>
							</td>
							<td>
								<?php echo $point['description']; ?>
							</td>
							<td>
								<?php echo $point['points']; ?>
							</td>
						</tr>
						<?php
						$count++;
					endforeach;
					?>
				</tbody>
			</table>
		<?php else: ?>
			<?php echo JText::_("COM_USERS_DASHBOARD_MISC_NO_POINT"); ?>
		<?php endif; ?>
	</div>
</div>


<div id="life_popup" class="modal hide fade custom-modal">
	<div class="modal-body custom-modal-body-1">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="center" id="life-ideal-text2"><?php echo InsureDIYHelper::toMoneyFormat($life_ideal_amt, 0) ?></h4>
		<span style="font-family: 'sourcesanspro-regular'; font-size:15px;">
			<?php echo JText::_("MISC_COVERAGE_BASED_1"); ?> <input style="width:29px;text-align: center;" value="10" type="text" name="life-multiplier" id="life-multiplier" /> x <?php echo JText::_("MISC_COVERAGE_BASED_2"); ?>
		</span>
		<br>
		<span style="font-family: 'sourcesanspro-regular'; font-size:15px;text-align: left">
			<?php echo JText::_("MISC_FOR_ESTIMATE_1"); ?> <a href='<?php echo JRoute::_("index.php?Itemid=405"); ?>'><?php echo JText::_("MISC_FOR_ESTIMATE_2"); ?></a>
		</span>
		<br>
		<a class="btn btn-info pull-right" href="javascript: void(0);" onclick="javascript:updateLifeGauge();" data-dismiss="modal"><?php echo JText::_("BTN_CALCULATE"); ?></a>
	</div>
</div>

<div id="ci_popup" class="modal hide fade custom-modal">
	<div class="modal-body custom-modal-body-1">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="center" id="ci-ideal-text2"><?php echo InsureDIYHelper::toMoneyFormat($ci_ideal, 0) ?></h4>
		<span style="font-family: 'sourcesanspro-regular'; font-size:15px;">
			<?php echo JText::_("MISC_COVERAGE_BASED_1"); ?> <input style="width:29px;text-align: center;" value="<?php echo $this->ciInfo->years ?>" type="text" name="ci-multiplier" id="ci-multiplier" /> x <?php echo JText::_("MISC_COVERAGE_BASED_2"); ?>
		</span>
		<br />
		<span style="font-family: 'sourcesanspro-regular'; font-size:15px;">
			For better estimate, use our <a href="<?php echo JRoute::_("index.php?Itemid=407"); ?>">Critical Illness Insurance Calculator</a>
		</span>
		<br />
		<a class="btn btn-info pull-right" href="javascript: void(0);" onclick="javascript:updateCiGauge();" data-dismiss="modal"><?php echo JText::_("BTN_CALCULATE"); ?></a>
	</div>
</div>

<script type="text/javascript">
	var life, ci, hg;
	jQuery(document).ready(function () {
		life = new JustGage({
			id: "life-gauge",
			value: <?php echo $data["1"] ?>,
			gaugeWidthScale: 1,
			min: 0,
			max: <?php echo $life_ideal_amt; ?>,
			title: " ",
			labelFontColor: "#b5b3b1",
			labelFontSize: 14,
			levelColors: ["#e14343", "#e18243", "#fad400", "#81d047"],
			minMaxSurfix: "%",
			valuePrefix: "HK$ ",
			valueFontSize: 18
		});
		ci = new JustGage({
			id: "ci-gauge",
			value: <?php echo $data["2"] ?>,
			gaugeWidthScale: 1,
			min: 0,
			max: <?php echo $ci_ideal; ?>,
			title: " ",
			labelFontColor: "#b5b3b1",
			labelFontSize: 14,
			levelColors: ["#e14343", "#e18243", "#fad400", "#81d047"],
			minMaxSurfix: "%",
			valuePrefix: "HK$ ",
			valueFontSize: 18
		});

		// Hospital gauge
		var opts = {
			lines: 12, // The number of lines to draw
			angle: 0, // The length of each line
			lineWidth: 1, // The line thickness
			pointer: {
				length: 0.9, // The radius of the inner circle
				strokeWidth: 0.035, // The rotation offset
				color: '#000000' // Fill color
			},
			limitMax: 'false', // If true, the pointer will not go past the end of the gauge
			colorStart: '#6FADCF', // Colors
			colorStop: '#8FC0DA', // just experiment with them
			strokeColor: '#E0E0E0', // to see which ones work best for you
			generateGradient: true
		};
		var target = document.getElementById('h-gauge'); // your canvas element
		var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
		gauge.maxValue = 4000; // set max gauge value
		gauge.animationSpeed = 32; // set animation speed (32 is default value)
		gauge.set(<?php echo $data["3"] ?>); // set actual value

	});

	function updateLifeGauge() {
		jQuery("#life-gauge").html("");
		var max = <?php echo InsureDIYHelper::getMonthlySalary() * 12; ?> * jQuery("#life-multiplier").val();
		var maxformatted = "<?php echo InsureDIYHelper::getCurrency(); ?>" + " " + numberWithCommas(max);
		jQuery("#life-ideal-text").html(maxformatted);
		jQuery("#life-ideal-text2").html(maxformatted);
		life = new JustGage({
			id: "life-gauge",
			value: <?php echo $data["1"] ?>,
			gaugeWidthScale: .9,
			min: 0,
			max: max,
			title: " ",
			labelFontColor: "#b5b3b1",
			labelFontSize: 14,
			levelColors: ["#e14343", "#e18243", "#fad400", "#81d047"],
			minMaxSurfix: "%",
			valuePrefix: "HK$ ",
			valueFontSize: 18
		});
	}
	function updateCiGauge() {
		jQuery("#ci-gauge").html("");
		var max = <?php echo $this->ciInfo->annual_salary; ?> * jQuery("#ci-multiplier").val();
		var maxformatted = "<?php echo InsureDIYHelper::getCurrency(); ?>" + " " + numberWithCommas(max);
		jQuery("#ci-ideal-text").html(maxformatted);
		jQuery("#ci-ideal-text2").html(maxformatted);
		ci = new JustGage({
			id: "ci-gauge",
			value: <?php echo $data["2"] ?>,
			gaugeWidthScale: .9,
			min: 0,
			max: max,
			title: " ",
			labelFontColor: "#b5b3b1",
			labelFontSize: 14,
			levelColors: ["#e14343", "#e18243", "#fad400", "#81d047"],
			minMaxSurfix: "%",
			valuePrefix: "HK$ ",
			valueFontSize: 18
		});
	}
	function numberWithCommas(x) {
		return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}
</script>