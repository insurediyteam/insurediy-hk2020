<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
JHtml::_('behavior.keepalive');
JHtml::_('formbehavior.chosen', '.portfolio select');
JHtml::_('MyBehavior.jsInsurediy');
$form = $this->form;
?>

<script type="text/javascript">
	window.addEvent('domready', function () {
		jsInsurediy.onNumberOnlyAndFormat("#jform_cover_amt");
	});
</script>
<div id="policy-form" class="portfolio policy-form <?php echo $this->pageclass_sfx ?>">
	<?php if ($this->params->get('show_page_heading')) : ?>
		<div class="header-top">
			<div class="h1-heading"><?php echo $this->escape($this->params->get('page_heading')); ?></div>
			<div class="customer-dashboard-menu">{modulepos insurediy-my-dashboard-menu}</div>
		</div>
	<?php else: ?>
		<div class="header-top">
			<div class="h1-heading"><?php echo JText::_("COM_USERS_PAGE_TITLE_PORTFOLIO"); ?></div>
			<div class="customer-dashboard-menu">{modulepos insurediy-my-dashboard-menu}</div>
			<div class="clear"></div>
		</div>
	<?php endif; ?>
	<div class="padding20">
		<form action="<?php echo JRoute::_('index.php?option=com_users&task=policy.submit'); ?>" method="post" class="form-vertical">
			<div class="span6">
				<fieldset>
					<div class="clearfix policy-form-row">
						<div class="control-group">
							<div class="control-label">
								<?php echo $form->getLabel("insurer"); ?>
							</div>
							<div class="controls">
								<?php echo $form->getInput("insurer"); ?>
							</div>
						</div>
					</div>
					<div class="clearfix policy-form-row">
						<div class="control-group">
							<div class="control-label">
								<?php echo $form->getLabel("type"); ?>
							</div>
							<div class="controls">
								<?php echo $form->getInput("type"); ?>
							</div>
						</div>
					</div>
					<div id="jcurrency" class="clearfix policy-form-row">
						<div class="control-group">
							<div class="control-label">
								<?php echo $form->getLabel("currency"); ?>
							</div>
							<div class="controls">
								<?php echo $form->getInput("currency"); ?>
							</div>
						</div>
					</div>
					<div id="jroom_type" class="clearfix policy-form-row">
						<div class="control-group">
							<div class="control-label">
								<?php echo $form->getLabel("room_type"); ?>
							</div>
							<div class="controls">
								<?php echo $form->getInput("room_type"); ?>
							</div>
						</div>
					</div>
					<div class="clearfix policy-form-row">
						<div class="control-group">
							<div class="control-label">
								<?php echo $form->getLabel("status"); ?>
							</div>
							<div class="controls">
								<?php echo $form->getInput("status"); ?>
							</div>
						</div>
					</div>
				</fieldset>
			</div>
			<div class="span6">
				<fieldset>
					<div class="clearfix policy-form-row">
						<div class="control-group">
							<div class="control-label">
								<?php echo $form->getLabel("policy_number"); ?>
							</div>
							<div class="controls">
								<?php echo $form->getInput("policy_number"); ?>
							</div>
						</div>
					</div>
					<div class="clearfix policy-form-row">
						<div class="control-group span6">
							<div class="control-label">
								<?php echo $form->getLabel("start_date"); ?>
							</div>
							<div class="controls">
								<?php echo $form->getInput("start_date"); ?>
							</div>
						</div>
						<div class="control-group span6">
							<div class="control-label">
								<?php echo $form->getLabel("end_date"); ?>
							</div>
							<div class="controls">
								<?php echo $form->getInput("end_date"); ?>
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<div id="jcover_amt" class="clearfix policy-form-row">
						<div class="control-group">
							<div class="control-label">
								<?php echo $form->getLabel("cover_amt"); ?>
							</div>
							<div class="controls">
								<?php echo $form->getInput("cover_amt"); ?>
							</div>
						</div>
					</div>
					<div id="jcover_type" class="clearfix policy-form-row">
						<div class="control-group">
							<div class="control-label">
								<?php echo $form->getLabel("cover_type"); ?>
							</div>
							<div class="controls">
								<?php echo $form->getInput("cover_type"); ?>
							</div>
						</div>
					</div>
				</fieldset>
			</div>
			<div class="clear"></div>
			<div style="float:left;margin:15px 0;">
				{modulepos insurediy-secured-ssl}
			</div>
			<div class="pull-right">
				<a class="btn btn-primary" href="index.php?option=com_users&view=portfolio&Itemid=136">Cancel</a>
				<button type="submit" value="submit" class="btn btn-primary" >Save changes</button>
			</div>
			<div class="clear"></div>
			<?php echo $form->getInput("id"); ?>
			<?php echo JHtml::_('form.token'); ?>
		</form>
	</div>
	<div class="clear"></div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function () {
		var jCover_type, jRoom_type, jCurrency, jCover_amt, jDiv, jType;
		jDiv = jQuery("#policy-form");
		jCover_type = jDiv.find("#jcover_type");
		jCover_amt = jDiv.find("#jcover_amt");
		jRoom_type = jDiv.find("#jroom_type");
		jCurrency = jDiv.find("#jcurrency");
		jType = jDiv.find("#jform_type");
		jType.on("change", function () {
			var jThis = jQuery(this);
			onTypeChange(jThis.chosen().val());
		});
		onTypeChange(jType.chosen().val());

		function onTypeChange(type) {
			switch (type) {

				case "3":
					jCover_type.fadeIn();
					jRoom_type.fadeIn();
					jCover_amt.hide();
					jCurrency.hide();
					break;
				case "4":
					jCover_type.hide();
					jRoom_type.hide();
					jCover_amt.hide();
					jCurrency.hide();
					break;
				case "5":
					jCover_type.hide();
					jRoom_type.hide();
					jCover_amt.hide();
					jCurrency.hide();
					break;
				case "6":
					jCover_type.hide();
					jRoom_type.hide();
					jCover_amt.hide();
					jCurrency.hide();
					break;
				case "1":
				case "2":
				case "8":
				default:
					jCover_type.hide();
					jRoom_type.hide();
					jCover_amt.fadeIn();
					jCurrency.fadeIn();
					break;
			}
		}

	});


</script>

