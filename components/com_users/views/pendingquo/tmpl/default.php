<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::_('jquery.framework');

$payment_data = $this->data['payment'];

$lifeUrl = "index.php?option=com_insurediylife&view=form&quotation_id=%s&Itemid=156";
$ciUrl = "index.php?option=com_insurediyci&view=form&quotation_id=%s&Itemid=157";
$travelUrl = "index.php?option=com_insurediytravel&view=form&quotation_id=%s&Itemid=159";
$hsUrl = "index.php?option=com_insurediyhospitalself&view=form&quotation_id=%s&Itemid=170";
$hcUrl = "index.php?option=com_insurediyhospitalchild&view=form&quotation_id=%s&Itemid=172";
$domesticUrl = "index.php?option=com_insurediydomestic&view=form&quotation_id=%s&Itemid=161";
?>
<script type="text/javascript">
	window.addEvent('domready', function () {
	});
	function deleteCustomerQuotationList() {
		if (confirm("Are you sure you want to proceed the deletion?")) {
			document.customerQuotationForm.submit();
		}
	}
	function deleteCustomerQuotationItem(id) {
		$$('.customer-chxbox-quotation').setProperty('checked', '');
		$(id).setProperty('checked', 'checked');
		if (confirm("Are you sure you want to proceed the deletion?")) {
			document.customerQuotationForm.submit();
		}
	}

	function submitPayment(success_url, amount, orderRef, remark) {
		var paymentForm = jQuery("div.pending-quo #paymentForm");
		var successUrlInput = paymentForm.find("input[name='successUrl']");
		var orderRefInput = paymentForm.find("input[name='orderRef']");
		var amountInput = paymentForm.find("input[name='amount']");
		var remarkInput = paymentForm.find("input[name='remark']");
		successUrlInput.val(success_url);
		orderRefInput.val(orderRef);
		amountInput.val(amount);
		remarkInput.val(remark);
		paymentForm.submit();
	}

</script>
<div class="pending-quo">
	<?php if ($this->params->get('show_page_heading')) : ?>
		<div class="header-top">
			<div class="h1-heading"><?php echo $this->escape($this->params->get('page_heading')); ?></div>
			<div class="customer-dashboard-menu">{modulepos insurediy-my-dashboard-menu}</div>
		</div>
	<?php else: ?>
		<div class="header-top">
			<div class="h1-heading"><?php echo JText::_("COM_USERS_PAGE_TITLE_PENDING_QUOTES"); ?></div>
			<div class="customer-dashboard-menu">{modulepos insurediy-my-dashboard-menu}</div>
			<div class="clear"></div>
		</div>
	<?php endif; ?>
	<form id="customerQuotationForm" name="customerQuotationForm" action="<?php echo JRoute::_('index.php'); ?>" method="post" class="form-validate form-horizontal" enctype="multipart/form-data">
		<div class="padding20">
			<div class="btn-group" style="margin-bottom:20px;">
				<button type="button" style="background:transparent;border:0;" onclick="javascript:deleteCustomerQuotationList();">
					<img src="images/websiteicon/delete-pending-quotes.png" />
				</button>
			</div>

			<div class="customer-ins customer-insurance-life">Term Insurance</div>

			<div class="whitebg radius-top border-all marginbottom20">
				<div class="header2bg">
					<div class="item-chxbox">&nbsp;</div>
					<div class="item-quotation"><?php echo JText::_('COM_USERS_HEADER_TEXT_QUOTATION'); ?> <?php echo JText::_('COM_USERS_HEADER_TEXT_DATE'); ?></div>
					<div class="item-quotation-action"><?php echo JText::_('COM_USERS_HEADER_TEXT_ACTION'); ?></div>
					<div class="clear"></div>
				</div>
				<?php
				if (!empty($this->data['life'])) :
					foreach ($this->data['life'] as $key => $value) :
						$gotolink = JRoute::_(sprintf($lifeUrl, $value->id));
						?>
						<div class="<?php echo ($key % 2) ? 'even' : 'odd'; ?>">
							<div class="item-chxbox"><input type="checkbox" class="customer-chxbox-quotation" id="life-<?php echo $value->id ?>" name="quote[]" value="life|<?php echo $value->id ?>" /></div>
							<div class="item-quotation"><?php echo JHtml::date($value->created_date, "d-m-Y, H:i:s"); ?></div>
							<div class="item-quotation-action">
								<a href="<?php echo $gotolink ?>"><img src="images/websiteicon/click-here-to-complete-quotation.png" /></a>
								&nbsp;&nbsp;|&nbsp;&nbsp;
								<a href="javascript:void(0);" onclick="javascript:deleteCustomerQuotationItem('<?php echo 'life-' . $value->id ?>');"><img src="images/websiteicon/click-here-to-delete.png" /></a>
								<?php if ($value->hasPayment) : ?>
									&nbsp;&nbsp;|&nbsp;&nbsp;
									<a href="javascript:void(0);" onclick="javascript:submitPayment('<?php echo $value->successUrl; ?>', '<?php echo $value->amount; ?>', '<?php echo $value->orderRef; ?>', '<?php echo $value->remark; ?>')" class="btn btn-info"><?php echo JText::_("BTN_PAY"); ?></a>
								<?php endif; ?>
							</div>
							<div class="clear"></div>
						</div>
					<?php endforeach; ?>
				<?php else : ?>
					<div class="odd">
						<div class="no-items"><?php echo JText::_("MSG_NO_LIFE_QUOTES"); ?></div>
					</div>
				<?php endif; ?>
			</div>

			<div class="customer-ins customer-insurance-ci">Critical Illness Insurance</div>

			<div class="whitebg radius-top border-all marginbottom20">
				<div class="header2bg">
					<div class="item-chxbox">&nbsp;</div>
					<div class="item-quotation"><?php echo JText::_('COM_USERS_HEADER_TEXT_QUOTATION'); ?> <?php echo JText::_('COM_USERS_HEADER_TEXT_DATE'); ?></div>
					<div class="item-quotation-action"><?php echo JText::_('COM_USERS_HEADER_TEXT_ACTION'); ?></div>
					<div class="clear"></div>
				</div>

				<?php if (!empty($this->data['ci'])) : ?>
					<?php
					foreach ($this->data['ci'] as $key => $value) :
						$gotolink = JRoute::_(sprintf($ciUrl, $value->id));
						?>
						<div class="<?php echo ($key % 2) ? 'even' : 'odd'; ?>">
							<div class="item-chxbox"><input type="checkbox" class="customer-chxbox-quotation" id="ci-<?php echo $value->id ?>" name="quote[]" value="ci|<?php echo $value->id ?>" /></div>
							<div class="item-quotation"><?php echo JHtml::date($value->created_date, "d-m-Y, H:i:s"); ?></div>
							<div class="item-quotation-action">
								<a href="<?php echo $gotolink ?>"><img src="images/websiteicon/click-here-to-complete-quotation.png" /></a>
								&nbsp;&nbsp;|&nbsp;&nbsp;
								<a href="javascript:void(0);" onclick="javascript:deleteCustomerQuotationItem('<?php echo 'ci-' . $value->id ?>');"><img src="images/websiteicon/click-here-to-delete.png" /></a>
								<?php if ($value->hasPayment) : ?>
									&nbsp;&nbsp;|&nbsp;&nbsp;
									<a href="javascript:void(0);" onclick="javascript:submitPayment('<?php echo $value->successUrl; ?>', '<?php echo $value->amount; ?>', '<?php echo $value->orderRef; ?>', '<?php echo $value->remark; ?>')" class="btn btn-info"><?php echo JText::_("BTN_PAY"); ?></a>
								<?php endif; ?>
							</div>
							<div class="clear"></div>
						</div>
					<?php endforeach; ?>
				<?php else : ?>
					<div class="odd">
						<div class="no-items"><?php echo JText::_("MSG_NO_CI_QUOTES"); ?></div>
					</div>
				<?php endif; ?>
			</div>

			<div class="customer-ins customer-insurance-hospital">Hospital Insurance</div>

			<div class="whitebg radius-top border-all marginbottom20">
				<div class="header2bg">
					<div class="item-chxbox">&nbsp;</div>
					<div class="item-quotation"><?php echo JText::_('COM_USERS_HEADER_TEXT_QUOTATION'); ?> <?php echo JText::_('COM_USERS_HEADER_TEXT_DATE'); ?></div>
					<div class="item-quotation-action"><?php echo JText::_('COM_USERS_HEADER_TEXT_ACTION'); ?></div>
					<div class="clear"></div>
				</div>
				<?php
				if (!empty($this->data['hospitalself']) && !empty($this->data['hospitalchild'])) :
					foreach ($this->data['hospitalself'] as $key => $value) :
						$gotolink = JRoute::_(sprintf($hsUrl, $value->id));
						?>
						<div class="<?php echo ($key % 2) ? 'even' : 'odd'; ?>">
							<div class="item-chxbox"><input type="checkbox" class="customer-chxbox-quotation" id="hospitalself-<?php echo $value->id ?>" name="quote[]" value="hospitalself|<?php echo $value->id ?>" /></div>
							<div class="item-quotation"><?php echo JHtml::date($value->created_date, "d-m-Y, H:i:s"); ?></div>
							<div class="item-quotation-action">
								<a href="<?php echo $gotolink ?>"><img src="images/websiteicon/click-here-to-complete-quotation.png" /></a>
								&nbsp;&nbsp;|&nbsp;&nbsp;
								<a href="javascript:void(0);" onclick="javascript:deleteCustomerQuotationItem('<?php echo 'hospitalself-' . $value->id ?>');"><img src="images/websiteicon/click-here-to-delete.png" /></a>
								<?php if ($value->hasPayment) : ?>
									&nbsp;&nbsp;|&nbsp;&nbsp;
									<a href="javascript:void(0);" onclick="javascript:submitPayment('<?php echo $value->successUrl; ?>', '<?php echo $value->amount; ?>', '<?php echo $value->orderRef; ?>', '<?php echo $value->remark; ?>')" class="btn btn-info"><?php echo JText::_("BTN_PAY"); ?></a>
								<?php endif; ?>
							</div>
							<div class="clear"></div>
						</div>
					<?php endforeach; ?>
					<?php
					foreach ($this->data['hospitalchild'] as $key => $value) :
						$gotolink = JRoute::_(sprintf($hcUrl, $value->id));
						?>
						<div class="<?php echo ($key % 2) ? 'even' : 'odd'; ?>">
							<div class="item-chxbox"><input type="checkbox" class="customer-chxbox-quotation" id="hospitalchild-<?php echo $value->id ?>" name="quote[]" value="hospitalchild|<?php echo $value->id ?>" /></div>
							<div class="item-quotation"><?php echo JHtml::date($value->created_date, "d-m-Y, H:i:s"); ?></div>
							<div class="item-quotation-action">
								<a href="<?php echo $gotolink ?>"><img src="images/websiteicon/click-here-to-complete-quotation.png" /></a>
								&nbsp;&nbsp;|&nbsp;&nbsp;
								<a href="javascript:void(0);" onclick="javascript:deleteCustomerQuotationItem('<?php echo 'hospitalchild-' . $value->id ?>');"><img src="images/websiteicon/click-here-to-delete.png" /></a>
								<?php if ($value->hasPayment) : ?>
									&nbsp;&nbsp;|&nbsp;&nbsp;
									<a href="javascript:void(0);" onclick="javascript:submitPayment('<?php echo $value->successUrl; ?>', '<?php echo $value->amount; ?>', '<?php echo $value->orderRef; ?>', '<?php echo $value->remark; ?>')" class="btn btn-info"><?php echo JText::_("BTN_PAY"); ?></a>
								<?php endif; ?>
							</div>
							<div class="clear"></div>
						</div>
					<?php endforeach; ?>
				<?php else : ?>
					<div class="odd">
						<div class="no-items"><?php echo JText::_("MSG_NO_HOSPITAL_QUOTES"); ?></div>
					</div>
				<?php endif; ?>
			</div>

			<div class="customer-ins customer-insurance-travel">Travel Insurance</div>

			<div class="whitebg radius-top border-all marginbottom20">
				<div class="header2bg">
					<div class="item-chxbox">&nbsp;</div>
					<div class="item-quotation"><?php echo JText::_('COM_USERS_HEADER_TEXT_QUOTATION'); ?> <?php echo JText::_('COM_USERS_HEADER_TEXT_DATE'); ?></div>
					<div class="item-quotation-action"><?php echo JText::_('COM_USERS_HEADER_TEXT_ACTION'); ?></div>
					<div class="clear"></div>
				</div>
				<?php
				if (!empty($this->data['travel'])) :
					foreach ($this->data['travel'] as $key => $value) :
						$gotolink = JRoute::_(sprintf($travelUrl, $value->id));
						?>
						<div class="<?php echo ($key % 2) ? 'even' : 'odd'; ?>">
							<div class="item-chxbox"><input type="checkbox" class="customer-chxbox-quotation" id="travel-<?php echo $value->id ?>" name="quote[]" value="travel|<?php echo $value->id ?>" /></div>
							<div class="item-quotation"><?php echo JHtml::date($value->created_date, "d-m-Y, H:i:s"); ?></div>
							<div class="item-quotation-action">
								<a href="<?php echo $gotolink ?>"><img src="images/websiteicon/click-here-to-complete-quotation.png" /></a>
								&nbsp;&nbsp;|&nbsp;&nbsp;
								<a href="javascript:void(0);" onclick="javascript:deleteCustomerQuotationItem('<?php echo 'travel-' . $value->id ?>');"><img src="images/websiteicon/click-here-to-delete.png" /></a>
								<?php if ($value->hasPayment) : ?>
									&nbsp;&nbsp;|&nbsp;&nbsp;
									<a href="javascript:void(0);" onclick="javascript:submitPayment('<?php echo $value->successUrl; ?>', '<?php echo $value->amount; ?>', '<?php echo $value->orderRef; ?>', '<?php echo $value->remark; ?>')" class="btn btn-info"><?php echo JText::_("BTN_PAY"); ?></a>
								<?php endif; ?>
							</div>
							<div class="clear"></div>
						</div>
					<?php endforeach; ?>
				<?php else : ?>
					<div class="odd">
						<div class="no-items"><?php echo JText::_("MSG_NO_TRAVEL_QUOTES"); ?></div>
					</div>
				<?php endif; ?>
			</div>

			<div class="customer-ins customer-insurance-motor">Motor Insurance</div>

			<div class="whitebg radius-top border-all marginbottom20">
				<div class="header2bg">
					<div class="item-chxbox">&nbsp;</div>
					<div class="item-quotation"><?php echo JText::_('COM_USERS_HEADER_TEXT_QUOTATION'); ?> <?php echo JText::_('COM_USERS_HEADER_TEXT_DATE'); ?></div>
					<div class="item-quotation-action"><?php echo JText::_('COM_USERS_HEADER_TEXT_ACTION'); ?></div>
					<div class="clear"></div>
				</div>
				<?php if (!empty($this->data['motor'])) : ?>
					<?php foreach ($this->data['motor'] as $key => $value) : ?>
						<div class="<?php echo ($key % 2) ? 'even' : 'odd'; ?>">
							<div class="item-chxbox"><input type="checkbox" value="" /></div>
							<div class="item-quotation">30-09-2013, A123456</div>
							<div class="item-quotation-action">
								<a href="#"><img src="images/websiteicon/click-here-to-complete-quotation.png" /></a>
								&nbsp;&nbsp;|&nbsp;&nbsp;
								<a href="#"><img src="images/websiteicon/click-here-to-delete.png" /></a>
							</div>
							<div class="clear"></div>
						</div>
					<?php endforeach; ?>
				<?php else : ?>
					<div class="odd">
						<div class="no-items"><?php echo JText::_("MSG_NO_MOTOR_QUOTES"); ?></div>
					</div>
				<?php endif; ?>
			</div>

			<div class="customer-ins customer-insurance-maid">Domestic Helper Insurance</div>

			<div class="whitebg radius-top border-all marginbottom20">
				<div class="header2bg">
					<div class="item-chxbox">&nbsp;</div>
					<div class="item-quotation"><?php echo JText::_('COM_USERS_HEADER_TEXT_QUOTATION'); ?><?php echo JText::_('COM_USERS_HEADER_TEXT_DATE'); ?></div>
					<div class="item-quotation-action"><?php echo JText::_('COM_USERS_HEADER_TEXT_ACTION'); ?></div>
					<div class="clear"></div>
				</div>

				<?php
				if (!empty($this->data['domestic'])) :
					foreach ($this->data['domestic'] as $key => $value) :
						$gotolink = JRoute::_(sprintf($domesticUrl, $value->id));
						?>
						<div class="<?php echo ($key % 2) ? 'even' : 'odd'; ?>">
							<div class="item-chxbox"><input type="checkbox" class="customer-chxbox-quotation" id="domestic-<?php echo $value->id ?>" name="quote[]" value="domestic|<?php echo $value->id ?>" /></div>
							<div class="item-quotation"><?php echo JHtml::date($value->created_date, "d-m-Y, H:i:s"); ?></div>
							<div class="item-quotation-action">
								<a href="<?php echo $gotolink ?>"><img src="images/websiteicon/click-here-to-complete-quotation.png" /></a>
								&nbsp;&nbsp;|&nbsp;&nbsp;
								<a href="javascript:void(0);" onclick="javascript:deleteCustomerQuotationItem('<?php echo 'domestic-' . $value->id ?>');"><img src="images/websiteicon/click-here-to-delete.png" /></a>
								<?php if ($value->hasPayment) : ?>
									&nbsp;&nbsp;|&nbsp;&nbsp;
									<a href="javascript:void(0);" onclick="javascript:submitPayment('<?php echo $value->successUrl; ?>', '<?php echo $value->amount; ?>', '<?php echo $value->orderRef; ?>', '<?php echo $value->remark; ?>')" class="btn btn-info"><?php echo JText::_("BTN_PAY"); ?></a>
								<?php endif; ?>
							</div>
							<div class="clear"></div>
						</div>
					<?php endforeach; ?>
				<?php else : ?>
					<div class="odd">
						<div class="no-items"><?php echo JText::_("MSG_NO_DOMESTIC_QUOTES"); ?></div>
					</div>
				<?php endif; ?>
			</div>

			<div class="form-actions" style="padding:20px 0;">
				<div style="float:left;">
					{modulepos insurediy-secured-ssl}
				</div>
				<input type="hidden" name="option" value="com_users" />
				<input type="hidden" name="task" value="pendingquo.delete" />
				<?php echo JHtml::_('form.token'); ?>
				<div class="clear"></div>
			</div>
		</div>
	</form>
	<form action="<?php echo $payment_data->action ?>"  method="post" name="insurediyPaymentForm" id="paymentForm">
		<input type="hidden" name="merchantId" value="<?php echo $payment_data->merchantId; ?>">
		<input type="hidden" name="amount" value="">
		<input type="hidden" name="orderRef" value="">
		<input type="hidden" name="currCode" value="<?php echo $payment_data->currCode; ?>" >
		<input type="hidden" name="mpsMode" value="<?php echo $payment_data->mpsMode; ?>" >
		<input type="hidden" name="successUrl" value="">
		<input type="hidden" name="failUrl" value="<?php echo $payment_data->failUrl; ?>">
		<input type="hidden" name="cancelUrl" value="<?php echo $payment_data->cancelUrl; ?>">
		<input type="hidden" name="payType" value="<?php echo $payment_data->payType; ?>">
		<input type="hidden" name="lang" value="<?php echo $payment_data->lang; ?>">
		<input type="hidden" name="payMethod" value="<?php echo $payment_data->payMethod; ?>">
		<input type="hidden" name="templateId" value="1">
		<input type="hidden" name="remark" value="">
	</form>
</div>
