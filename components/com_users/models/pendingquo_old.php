<?php

defined('_JEXEC') or die;

class UsersModelPendingQuo extends JModelForm {

	protected $data;

	public function __construct($config = array()) {
		parent::__construct($config);

		if (!defined('FOF_INCLUDED')) {
			include_once JPATH_LIBRARIES . '/fof/include.php';
		}

		// Load the helper and model used for two factor authentication
		require_once JPATH_ADMINISTRATOR . '/components/com_users/models/user.php';
		require_once JPATH_ADMINISTRATOR . '/components/com_users/helpers/users.php';
	}

	public function getData() {
		$db = JFactory::getDBO();
		$user = JFactory::getUser();

		// set user id
		$user_id = $user->id;

		$this->data['life'] = array();
		$this->data['ci'] = array();
		$this->data['hospitalself'] = array();
		$this->data['hospitalchild'] = array();
		$this->data['travel'] = array();
		$this->data['motor'] = array();
		$this->data['domestic'] = array();
		$this->data['payment'] = $this->getPDPayment();

		// Life Insurance Data
		$query = $db->getQuery(TRUE)
				->select("id, quote_stage, payment_status, created_date, unique_order_no As orderRef")
				->from("#__insure_life_quotations")
				->where("user_id = " . $db->quote($user_id))
				->where("quote_status = 0")
				->where("quote_stage < 8")
				->order("created_date ASC");
		$lifeQuotations = $db->setQuery($query)->loadObjectList();
		$lifeparams = JComponentHelper::getParams('com_insurediylife');
		$lifemonth = $lifeparams->get('premium_month', 1);

		foreach ($lifeQuotations as $lifeQuotation) {
			$lifeQuotation->hasPayment = (($lifeQuotation->payment_status == "P") && $lifeQuotation->quote_stage == 6);
			if ($lifeQuotation->hasPayment) {
				$lifeQuotation->successUrl = JURI::base() . 'index.php?option=com_insurediylife&view=form&quotation_id=' . $lifeQuotation->id;
				$lifeQuotation->amount = $this->getAmount($lifeQuotation->id, $lifemonth);
				$lifeQuotation->remark = "life";
			}
		}


		// Critical Illness Insurance Data
		$query->clear();
		$query->select("id, quote_stage, payment_status, created_date, unique_order_no As orderRef")
				->from("#__insure_ci_quotations")
				->where("user_id = " . $db->quote($user_id))
				->where("quote_status = 0")
				->where("quote_stage < 8")
				->order("created_date ASC");

		$ciQuotations = $db->setQuery($query)->loadObjectList();
		$ciparams = JComponentHelper::getParams('com_insurediyci');
		$month = $ciparams->get('premium_month', 1);

		foreach ($ciQuotations as $ciQuotation) {
			$ciQuotation->hasPayment = (($ciQuotation->payment_status == "P") && $ciQuotation->quote_stage == 6);
			if ($ciQuotation->hasPayment) {
				$ciQuotation->successUrl = JURI::base() . 'index.php?option=com_insurediyci&view=form&quotation_id=' . $ciQuotation->id;
				$ciQuotation->amount = $this->getAmount($ciQuotation->id, $month);
				$ciQuotation->remark = "ci";
			}
		}

		// Travel Insurance Data
		$query->clear();
		$query->select("q.id, q.quote_stage, q.payment_status, q.created_date, q.unique_order_no, qp.premium AS amount, q.unique_order_no AS orderRef")
				->from("#__insure_travel_quotations AS q")
				->leftJoin("#__insure_travel_quotations_to_plans AS qp ON q.id = qp.quotation_id")
				->where("q.user_id = " . $db->quote($user_id))
				->where("quote_status = 0")
				->where("q.quote_stage < 6")
				->order("created_date ASC");
		$travelQuotations = $db->setQuery($query)->loadObjectList();
		foreach ($travelQuotations as $travelQuotation) {
			$travelQuotation->hasPayment = (($travelQuotation->payment_status == "P") && $travelQuotation->quote_stage == 5);
			if ($travelQuotation->hasPayment) {
				$travelQuotation->successUrl = JURI::base() . 'index.php?option=com_insurediytravel&view=form&layout=thankyou';
				$travelQuotation->remark = "travel";
			}
		}

		// Hospital self Insurance Data
		$query->clear();
		$query->select("q.id, q.quote_stage, q.payment_status, q.created_date, q.unique_order_no, qp.premium AS amount, q.unique_order_no AS orderRef")
				->from("#__insure_hospitalself_quotations AS q")
				->leftJoin("#__insure_hospitalself_quotations_to_plans AS qp ON q.id = qp.quotation_id")
				->where("q.user_id = " . $db->quote($user_id))
				->where("q.quote_stage < 7")
				->order("created_date ASC");
		$hsparams = JComponentHelper::getParams('com_insurediyhospitalself');
		$hsmonth = $hsparams->get("premium_month", 1);
		$hospitalselfQuotations = $db->setQuery($query)->loadObjectList();
		foreach ($hospitalselfQuotations as $hospitalselfQuotation) {
			$hospitalselfQuotation->hasPayment = (($hospitalselfQuotation->payment_status == "P") && $hospitalselfQuotation->quote_stage == 5);
			if ($hospitalselfQuotation->hasPayment) {
				$hospitalselfQuotation->amount = $hospitalselfQuotation->amount * $hsmonth;
				$hospitalselfQuotation->successUrl = JURI::base() . 'index.php?option=com_insurediyhospitalself&view=form&quotation_id=' . $hospitalselfQuotation->id;
				$hospitalselfQuotation->remark = "hospitalself";
			}
		}

		// Hospital child Insurance Data
		$query->clear();
		$query->select("q.id, q.quote_stage, q.payment_status, q.created_date, q.unique_order_no, qp.premium AS amount, q.unique_order_no AS orderRef")
				->from("#__insure_hospitalchild_quotations AS q")
				->leftJoin("#__insure_hospitalchild_quotations_to_plans AS qp ON q.id = qp.quotation_id")
				->where("q.user_id = " . $db->quote($user_id))
				->where("q.quote_stage < 8")
				->order("created_date ASC");
		$hcparams = JComponentHelper::getParams('com_insurediyhospitalchild');
		$hcmonth = $hcparams->get("premium_month", 1);
		$hospitalChildQuotations = $db->setQuery($query)->loadObjectList();
		foreach ($hospitalChildQuotations as $hospitalChildQuotation) {
			$hospitalChildQuotation->option = "com_insurediyhospitalchild";
			$hospitalChildQuotation->hasPayment = (($hospitalChildQuotation->payment_status == "P") && ($hospitalselfQuotation->quote_stage == 6));
			if ($hospitalChildQuotation->hasPayment) {
				$hospitalChildQuotation->amount = $hospitalChildQuotation->amount * $hcmonth;
				$hospitalChildQuotation->successUrl = JURI::base() . 'index.php?option=com_insurediyhospitalself&view=form&quotation_id=' . $hospitalChildQuotation->id;
				$hospitalChildQuotation->remark = "hospitalchild";
			}
		}

		// Domestic Insurance Data
		$query->clear();
		$query->select("q.id, q.quote_stage, q.payment_status, q.created_date, q.unique_order_no, qp.premium AS amount, q.unique_order_no AS orderRef")
				->from("#__insure_domestic_quotations AS q")
				->leftJoin("#__insure_domestic_quotations_to_plans AS qp ON q.id = qp.quotation_id")
				->where("q.user_id = " . $db->quote($user_id))
				->where("q.quote_stage < 6")
				->order("created_date ASC");
		$domesticQuotations = $db->setQuery($query)->loadObjectList();
		foreach ($domesticQuotations as $domesticQuotation) {
			$domesticQuotation->hasPayment = (($domesticQuotation->payment_status == "P") && $domesticQuotation->quote_stage == 5);
			if ($domesticQuotation->hasPayment) {
				$domesticQuotation->successUrl = JURI::base() . 'index.php?option=com_insurediydomestic&view=form&layout=thankyou';
				$domesticQuotation->remark = "domestic";
			}
		}

		$this->data['life'] = $lifeQuotations;
		$this->data['ci'] = $ciQuotations;
		$this->data['travel'] = $travelQuotations;
		$this->data['hospitalself'] = $hospitalselfQuotations;
		$this->data['hospitalchild'] = $hospitalChildQuotations;
		$this->data['domestic'] = $domesticQuotations;

//		Myhelper::debug($domesticQuotations);

		return $this->data;
	}

	public function getForm($data = array(), $loadData = true) {
		// Get the form.
		$form = $this->loadForm('com_users.profile', 'profile', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}

		return $form;
	}

	protected function loadFormData() {
		$data = $this->getData();

		$this->preprocessData('com_users.profile', $data);

		return $data;
	}

	protected function preprocessForm(JForm $form, $data, $group = 'user') {
		if (JComponentHelper::getParams('com_users')->get('frontend_userparams')) {
			$form->loadFile('frontend', false);
			if (JFactory::getUser()->authorise('core.login.admin')) {
				$form->loadFile('frontend_admin', false);
			}
		}
		parent::preprocessForm($form, $data, $group);
	}

	protected function populateState() {
		// Get the application object.
		$params = JFactory::getApplication()->getParams('com_users');

		// Get the user id.
		$userId = JFactory::getApplication()->getUserState('com_users.edit.profile.id');
		$userId = !empty($userId) ? $userId : (int) JFactory::getUser()->get('id');

		// Set the user id.
		$this->setState('user.id', $userId);

		// Load the parameters.
		$this->setState('params', $params);
	}

	public function delete($data = array()) {
		$user = JFactory::getUser();
		if (empty($data) || $user->get("guest")) {
			return false;
		}

		foreach ($data as $item) :

			$temp = explode('|', $item);

			$section = $temp[0];
			$quotation_id = $temp[1];

			$db = JFactory::getDBO();

			switch ($section) {
				case 'life': // Life Insurance
					$testQuery = $db->getQuery(TRUE)
									->select("count(*)")
									->from("#__insure_life_quotations")
									->where("id = " . $db->quote($quotation_id))->where("user_id = " . $db->quote($user->id));
					$check = $db->setQuery($testQuery)->loadResult();
					if ($check) {
						$query = " DELETE FROM #__insure_life_quotation_existing_insurances WHERE quotation_id = '$quotation_id' ";
						$db->setQuery($query);
						$db->query();

						$query = " DELETE FROM #__insure_life_quotation_plan_xref WHERE quotation_id = '$quotation_id' ";
						$db->setQuery($query);
						$db->query();

						$query = " DELETE FROM #__insure_life_quotation_questionnaire WHERE quotation_id = '$quotation_id' ";
						$db->setQuery($query);
						$db->query();

						$query = " DELETE FROM #__insure_life_quotation_cars WHERE quotation_id = '$quotation_id' ";
						$db->setQuery($query);
						$db->query();

						$query = " DELETE FROM #__insure_life_quotation_properties WHERE quotation_id = '$quotation_id' ";
						$db->setQuery($query);
						$db->query();

						$query = " DELETE FROM #__insure_life_quotations WHERE id = '$quotation_id' ";
						$db->setQuery($query);
						$db->query();
					}
					break;
				case 'ci': // Critical Illness
					$testQuery = $db->getQuery(TRUE)
									->select("count(*)")
									->from("#__insure_ci_quotations")
									->where("id = " . $db->quote($quotation_id))->where("user_id = " . $db->quote($user->id));
					$check = $db->setQuery($testQuery)->loadResult();
					if ($check) {
						$query = " DELETE FROM #__insure_ci_quotation_existing_insurances WHERE quotation_id = '$quotation_id' ";
						$db->setQuery($query);
						$db->query();

						$query = " DELETE FROM #__insure_ci_quotation_plan_xref WHERE quotation_id = '$quotation_id' ";
						$db->setQuery($query);
						$db->query();

						$query = " DELETE FROM #__insure_ci_quotation_questionnaire WHERE quotation_id = '$quotation_id' ";
						$db->setQuery($query);
						$db->query();

						$query = " DELETE FROM #__insure_ci_quotation_cars WHERE quotation_id = '$quotation_id' ";
						$db->setQuery($query);
						$db->query();

						$query = " DELETE FROM #__insure_ci_quotation_properties WHERE quotation_id = '$quotation_id' ";
						$db->setQuery($query);
						$db->query();

						$query = " DELETE FROM #__insure_ci_quotations WHERE id = '$quotation_id' ";
						$db->setQuery($query);
						$db->query();
					}
					break;
				case 'travel':
					$testQuery = $db->getQuery(TRUE)
									->select("count(*)")
									->from("#__insure_travel_quotations")
									->where("id = " . $db->quote($quotation_id))->where("user_id = " . $db->quote($user->id));
					$check = $db->setQuery($testQuery)->loadResult();
					if ($check) {
						$query = " DELETE FROM #__insure_travel_quotations_to_travellers WHERE quotation_id = '$quotation_id' ";
						$db->setQuery($query);
						$db->query();

						$query = " DELETE FROM #__insure_travel_quotations_to_plans WHERE quotation_id = '$quotation_id' ";
						$db->setQuery($query);
						$db->query();

						$query = " DELETE FROM #__insure_travel_quotations WHERE id = '$quotation_id' ";
						$db->setQuery($query);
						$db->query();
					}
					break;
				case 'hospitalself': // Hospital
					$testQuery = $db->getQuery(TRUE)
									->select("count(*)")
									->from("#__insure_hospitalself_quotations")
									->where("id = " . $db->quote($quotation_id))->where("user_id = " . $db->quote($user->id));
					$check = $db->setQuery($testQuery)->loadResult();
					if ($check) {
						$query = " DELETE FROM #__insure_hospitalself_quotation_existing_insurances WHERE quotation_id = '$quotation_id' ";
						$db->setQuery($query);
						$db->query();

						$query = " DELETE FROM #__insure_hospitalself_quotations_to_plans WHERE quotation_id = '$quotation_id' ";
						$db->setQuery($query);
						$db->query();

						$query = " DELETE FROM #__insure_hospitalself_quotation_questionnaires WHERE quotation_id = '$quotation_id' ";
						$db->setQuery($query);
						$db->query();

						$query = " DELETE FROM #__insure_hospitalself_quotations WHERE id = '$quotation_id' ";
						$db->setQuery($query);
						$db->query();
					}
					break;
				case 'hospitalchild': // Hospital
					$testQuery = $db->getQuery(TRUE)
									->select("count(*)")
									->from("#__insure_hospitalchild_quotations")
									->where("id = " . $db->quote($quotation_id))->where("user_id = " . $db->quote($user->id));
					$check = $db->setQuery($testQuery)->loadResult();
					if ($check) {
						$query = " DELETE FROM #__insure_hospitalchild_quotation_existing_insurances WHERE quotation_id = '$quotation_id' ";
						$db->setQuery($query);
						$db->query();

						$query = " DELETE FROM #__insure_hospitalchild_quotations_to_plans WHERE quotation_id = '$quotation_id' ";
						$db->setQuery($query);
						$db->query();

						$query = " DELETE FROM #__insure_hospitalchild_quotation_questionnaires WHERE quotation_id = '$quotation_id' ";
						$db->setQuery($query);
						$db->query();

						$query = " DELETE FROM #__insure_hospitalchild_quotations WHERE id = '$quotation_id' ";
						$db->setQuery($query);
						$db->query();
					}
					break;
				case 'motor':
					break;
				case 'domestic':
					$testQuery = $db->getQuery(TRUE)
									->select("count(*)")
									->from("#__insure_domestic_quotations")
									->where("id = " . $db->quote($quotation_id))->where("user_id = " . $db->quote($user->id));
					$check = $db->setQuery($testQuery)->loadResult();
					if ($check) {
						$query = " DELETE FROM #__insure_domestic_quotations_to_helpers WHERE quotation_id = '$quotation_id' ";
						$db->setQuery($query);
						$db->query();

						$query = " DELETE FROM #__insure_domestic_quotations_to_plans WHERE quotation_id = '$quotation_id' ";
						$db->setQuery($query);
						$db->query();

						$query = " DELETE FROM #__insure_domestic_quotations WHERE id = '$quotation_id' ";
						$db->setQuery($query);
						$db->query();
					}
					break;
				default:
					return FALSE;
			}

		endforeach;
		return TRUE;
	}

	public function getPDPayment() {
		$params = JComponentHelper::getParams('com_insurediylife');
		$data = new stdClass();
		$data->action = $params->get('pdUrl', "https://test.paydollar.com/b2cDemo/eng/payment/payForm.jsp");
		$data->mpsMode = $params->get('mpsMode', 'NIL');
		$data->currCode = $params->get('currCode', '344');
		$data->lang = $params->get('lang', 'E');
		$data->merchantId = $params->get('merchantId', FALSE);
		$data->cancelUrl = JURI::base() . 'index.php?option=com_insurediylife&view=form&layout=canceled';
		$data->failUrl = JURI::base() . 'index.php?option=com_insurediylife&view=form&layout=error';
		$data->payType = "N";
		$data->payMethod = "ALL";
		//$data->amount
		//$data->successUrl
		//$data->orderRef
		return $data;
	}

	private function hasPayment($quotation_id) { // not using anymore. [TO DO: remove]
		$has = TRUE;
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_life_quotations")
				->where("id = " . (int) $quotation_id);
		$db->setQuery($query);
		$quotation = $db->loadObject();
		if ($quotation->payment_status != "N") {
			$has = FALSE;
		}
		if ($quotation->quote_stage != 6) {
			$has = FALSE;
		}
		if ($quotation->unique_order_no == "") {
			$has = FALSE;
		}
		return $has;
	}

	private function getAmount($quotation_id, $month) {
		$query = " SELECT price FROM #__insure_life_quotation_plan_xref WHERE quotation_id = '$quotation_id' ";
		$db = JFactory::getDbo();
		$db->setQuery($query);
		$rows = $db->loadObjectList();

		$amount = 0.00;
		foreach ($rows as $r):
			$amount += (float) $r->price;
		endforeach;

		return $amount * (float) $month;
	}

}
