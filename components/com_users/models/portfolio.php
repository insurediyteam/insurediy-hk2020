<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Profile model class for Users.
 *
 * @package     Joomla.Site
 * @subpackage  com_users
 * @since       1.6
 */
class UsersModelPortfolio extends JModelForm {

	/**
	 * @var		object	The user profile data.
	 * @since   1.6
	 */
	protected $data;

	public function __construct($config = array()) {
		parent::__construct($config);

		// Load the Joomla! RAD layer
		if (!defined('FOF_INCLUDED')) {
			include_once JPATH_LIBRARIES . '/fof/include.php';
		}

		// Load the helper and model used for two factor authentication
		require_once JPATH_ADMINISTRATOR . '/components/com_users/models/user.php';
		require_once JPATH_ADMINISTRATOR . '/components/com_users/helpers/users.php';
	}

	public function getData() {
		$db = JFactory::getDBO();
		$user = JFactory::getUser();

		// set user id
		$user_id = $user->id;

		$tmp = array();
//		$this->data['life'] = array();
//		$this->data['ci'] = array();
//		$this->data['hospital'] = array();
//		$this->data['travel'] = array();
//		$this->data['motor'] = array();
//		$this->data['domestic'] = array();

		$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_policies")
				->where("user_id = " . $user_id);

		$db->setQuery($query);

		$results = $db->loadObjectList();
		foreach ($results as $result) {
			switch ($result->type) {
				case "1":
					$tmp['life'][] = $result;
					break;
				case "2":
					$tmp['ci'][] = $result;
					break;
				case "3":
					$tmp['hospital'][] = $result;
					break;
				case "4":
					$tmp['travel'][] = $result;
					break;
				case "5":
					$tmp['motor'][] = $result;
					break;
				case "6":
					$tmp['domestic'][] = $result;
					break;
				case "8":
					$tmp['others'][] = $result;
					break;
				default:
					break;
			}
		}
		$this->data = $tmp;
		return $this->data;
	}

	/**
	 * Method to get the profile form.
	 *
	 * The base form is loaded from XML and then an event is fired
	 * for users plugins to extend the form with extra fields.
	 *
	 * @param   array  $data		An optional array of data for the form to interogate.
	 * @param   boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return  JForm	A JForm object on success, false on failure
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true) {
		// Get the form.
		$form = $this->loadForm('com_users.profile', 'profile', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 * @since   1.6
	 */
	protected function loadFormData() {
		$data = $this->getData();

		$this->preprocessData('com_users.profile', $data);

		return $data;
	}

	/**
	 * Override preprocessForm to load the user plugin group instead of content.
	 *
	 * @param   object	A form object.
	 * @param   mixed	The data expected for the form.
	 * @throws	Exception if there is an error in the form event.
	 * @since   1.6
	 */
	protected function preprocessForm(JForm $form, $data, $group = 'user') {
		if (JComponentHelper::getParams('com_users')->get('frontend_userparams')) {
			$form->loadFile('frontend', false);
			if (JFactory::getUser()->authorise('core.login.admin')) {
				$form->loadFile('frontend_admin', false);
			}
		}
		parent::preprocessForm($form, $data, $group);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since   1.6
	 */
	protected function populateState() {
		// Get the application object.
		$params = JFactory::getApplication()->getParams('com_users');

		// Get the user id.
		$userId = JFactory::getApplication()->getUserState('com_users.edit.profile.id');
		$userId = !empty($userId) ? $userId : (int) JFactory::getUser()->get('id');

		// Set the user id.
		$this->setState('user.id', $userId);

		// Load the parameters.
		$this->setState('params', $params);
	}

	/**
	 * Method to delete permanently from DB
	 *
	 * Sample : Array ( [0] => life|4 [1] => life|5 )
	 *
	 * @param   array  The form data.
	 * @return  mixed  	The user id on success, false on failure.
	 * @since   1.6
	 */
	public function delete($data = array()) {

		if (empty($data))
			return false;

		foreach ($data as $item) :

			$temp = explode('|', $item);

			$section = $temp[0];
			$quotation_id = $temp[1];

			$db = JFactory::getDBO();

			switch ($section) {
				case 'life': // Life Insurance

					$query = " DELETE FROM #__insure_life_quotation_existing_insurances WHERE quotation_id = '$quotation_id' ";
					$db->setQuery($query);
					$db->query();

					$query = " DELETE FROM #__insure_life_quotation_plan_xref WHERE quotation_id = '$quotation_id' ";
					$db->setQuery($query);
					$db->query();

					$query = " DELETE FROM #__insure_life_quotation_questionnaire WHERE quotation_id = '$quotation_id' ";
					$db->setQuery($query);
					$db->query();

					$query = " DELETE FROM #__insure_life_quotation_cars WHERE quotation_id = '$quotation_id' ";
					$db->setQuery($query);
					$db->query();

					$query = " DELETE FROM #__insure_life_quotation_properties WHERE quotation_id = '$quotation_id' ";
					$db->setQuery($query);
					$db->query();

					$query = " DELETE FROM #__insure_life_quotations WHERE id = '$quotation_id' ";
					$db->setQuery($query);
					$db->query();

					break;

				case 'ci': // Critical Illness
				case 'hospital': // Hospital
				case 'travel':
				case 'motor':
				case 'domestic':
					return TRUE;
				default:
					return FALSE;
			}

		endforeach;
		return TRUE;
	}

	public function getPDPayment() {
		$params = JComponentHelper::getParams('com_insurediylife');
		$data = new stdClass();
		$data->action = $params->get('pdUrl', "https://test.paydollar.com/b2cDemo/eng/payment/payForm.jsp");
		$data->mpsMode = $params->get('mpsMode', 'NIL');
		$data->currCode = $params->get('currCode', '344');
		$data->lang = $params->get('lang', 'E');
		$data->merchantId = $params->get('merchantId', FALSE);
		$data->cancelUrl = JURI::base() . 'index.php?option=com_insurediylife&view=form&layout=canceled';
		$data->failUrl = JURI::base() . 'index.php?option=com_insurediylife&view=form&layout=error';
		$data->payType = "N";
		$data->payMethod = "ALL";
		//$data->amount
		//$data->successUrl
		//$data->orderRef
		return $data;
	}

	private function hasPayment($quotation_id) {
		$has = TRUE;
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_life_quotations")
				->where("id = " . (int) $quotation_id);
		$db->setQuery($query);
		$quotation = $db->loadObject();
		if ($quotation->payment_status != "N") {
			$has = FALSE;
		}
		if ($quotation->quote_stage != 6) {
			$has = FALSE;
		}
		if ($quotation->unique_order_no == "") {
			$has = FALSE;
		}
		return $has;
	}

	private function getAmount($quotation_id, $month) {
		$query = " SELECT price FROM #__insure_life_quotation_plan_xref WHERE quotation_id = '$quotation_id' ";
		$db = JFactory::getDbo();
		$db->setQuery($query);
		$rows = $db->loadObjectList();

		$amount = 0.00;
		foreach ($rows as $r):
			$amount += (float) $r->price;
		endforeach;

		return $amount * (float) $month;
	}

}
