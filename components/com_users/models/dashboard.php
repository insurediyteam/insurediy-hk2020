<?php

defined('_JEXEC') or die;

class UsersModelDashboard extends JModelForm {

	protected $data;
	protected $_tb_points = "#__insure_points";
	protected $_tb_calculator_ci = "#__calculator_ci";
	protected $_tb_calculator_life = "#__calculator_life";

	public function __construct($config = array()) {
		parent::__construct($config);

		// Load the Joomla! RAD layer
		if (!defined('FOF_INCLUDED')) {
			include_once JPATH_LIBRARIES . '/fof/include.php';
		}

		// Load the helper and model used for two factor authentication
		require_once JPATH_ADMINISTRATOR . '/components/com_users/models/user.php';
		require_once JPATH_ADMINISTRATOR . '/components/com_users/helpers/users.php';
	}

	public function getData() {
		$db = JFactory::getDBO();
		$user = JFactory::getUser();
		$data = array();
		// set user id
		$user_id = $user->id;

		$query = " SELECT * FROM #__insure_policies WHERE `user_id` = '$user_id' AND `status` = 1";
		$db->setQuery($query);
		$policies = $db->loadObjectList();
		$data["1"] = 0; // Life Terms
		$data["2"] = 0; // CI
		$data["3"] = 0;
		$data["4"] = 0;
		$data["5"] = 0;
		$data["6"] = 0;
		foreach ($policies as $v) {
			$sum = MyHelper::numberFormat($v->cover_amt, TRUE);
			if ($v->type == 8) {
				continue;
			}
			if ($sum) {
				$data[$v->type] += $sum;
			} else {
				$data[$v->type] += 1;
			}
		}
		// For Life
		$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_life_quotations")
				->where("`user_id` =" . $user_id)
				->where("`quote_status` = 1");
		
		$db->setQuery($query);
		$life_quotes = $db->loadObjectList();
		
		foreach ($life_quotes as $life_quote) {
			$data['1'] += (int) $life_quote->cover_amt;
		}
		
		// for CI
		$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_ci_quotations")
				->where("`user_id` =" . $user_id)
				->where("`quote_status` = 1");
		
		$db->setQuery($query);
		$ci_quotes = $db->loadObjectList();
		
		foreach ($ci_quotes as $ci_quote) {
			$data['2'] += (int) $ci_quote->cover_amt;
		}

		// for hospital
		$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_policies")
				->where("`user_id` =" . $user_id)
				->where("`status` = 1")
				->where("`type` = 3");

		$db->setQuery($query);
		$hpolicies = $db->loadObjectList();
		$hroomtype = 0;
		$hcovertype = 0;
		$data['3'] = 0;
		foreach ($hpolicies as $hpolicy) {
			$hroomtype = ($hpolicy->room_type > $hroomtype) ? $hpolicy->room_type : $hroomtype;
			$hcovertype = ($hpolicy->cover_type > $hcovertype) ? $hpolicy->cover_type : $hcovertype;
		}
		if ($hroomtype) {
			$data['3'] = ($hroomtype - 1) * 1000;
		}
		if ($hcovertype) {
			$data['3'] += (($hcovertype) * 250);
		}
		if ($data['3'] == 0) {
			$data['3'] = 1;
		}
		if ($data['3'] == 4000) {
			$data['3'] = 3960;
		}
		return $data;
	}

	public function getCiInfo() {
		$user = JFactory::getUser();
		if ($user->get('guest')) {
			return FALSE;
		}
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
				->select("annual_salary, years")
				->from($this->_tb_calculator_ci)
				->where("user_id = " . $user->id);
		$result = $db->setQuery($query)->loadObject();
		if (!$result) {
			$result = new stdClass();
			$result->annual_salary = InsureDIYHelper::getMonthlySalary() * 12;
			$result->years = 3;
		}
		return $result;
	}

	public function getLifeInfo() {
		$user = JFactory::getUser();
		if ($user->get('guest')) {
			return FALSE;
		}
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from($this->_tb_calculator_life)
				->where("user_id = " . $user->id);
		$result = $db->setQuery($query)->loadObject();
		return $result;
	}

	public function getPoints() {
		$user = JFactory::getUser();
		if ($user->get('guest')) {
			return FALSE;
		}
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from($this->_tb_points)
				->where("user_id=" . $user->id . " AND status = 'A'");
		return $db->setQuery($query)->loadAssocList();
	}

	public function getForm($data = array(), $loadData = true) {
		// Get the form.
		$form = $this->loadForm('com_users.profile', 'profile', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}

		return $form;
	}

	protected function loadFormData() {
		$data = $this->getData();

		$this->preprocessData('com_users.profile', $data);

		return $data;
	}

	protected function preprocessForm(JForm $form, $data, $group = 'user') {
		if (JComponentHelper::getParams('com_users')->get('frontend_userparams')) {
			$form->loadFile('frontend', false);
			if (JFactory::getUser()->authorise('core.login.admin')) {
				$form->loadFile('frontend_admin', false);
			}
		}
		parent::preprocessForm($form, $data, $group);
	}

	protected function populateState() {
		// Get the application object.
		$params = JFactory::getApplication()->getParams('com_users');

		// Get the user id.
		$userId = JFactory::getApplication()->getUserState('com_users.edit.profile.id');
		$userId = !empty($userId) ? $userId : (int) JFactory::getUser()->get('id');

		// Set the user id.
		$this->setState('user.id', $userId);

		// Load the parameters.
		$this->setState('params', $params);
	}

}
