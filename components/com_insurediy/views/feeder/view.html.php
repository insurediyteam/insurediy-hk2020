<?php

defined('_JEXEC') or die;

class InsureDIYViewFeeder extends JViewLegacy {

	public $form;

	public function display($tpl = null) {
		$this->form = $this->get("Form");

		parent::display($tpl);
	}

}
