<?php

defined('_JEXEC') or die;

class InsureDIYControllerUser extends JControllerForm {

	public function gen() {
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
				->select("id")
				->from("#__users")
				->where("referral_id = ''");

		$users = $db->setQuery($query)->loadAssocList();
		foreach ($users AS $user) {
			$referral_id = MyHelper::generateRandomId(7);
			$query = $db->getQuery(TRUE)
					->update("#__users")
					->set("referral_id = " . $db->quote($referral_id))
					->where("id = " . $user['id']);
			$db->setQuery($query)->execute();
		}
		exit;
	}

}
