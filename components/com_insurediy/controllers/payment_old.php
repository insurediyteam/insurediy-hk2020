<?php

defined('_JEXEC') or die;
JLoader::import("components.com_insurediytravel.models.form", JPATH_ROOT);
JLoader::import("components.com_insurediytravel.helpers.insurediytravel", JPATH_ROOT);

JLoader::import("components.com_insurediydomestic.models.form", JPATH_ROOT);
JLoader::import("components.com_insurediydomestic.helpers.insurediydomestic", JPATH_ROOT);

class InsureDIYControllerPayment extends JControllerForm {

	public function feedme() {
		echo "OK"; // let AsiaPay know we are cool.
		// Payment Statuses  (payment_status or payment_status)
		// C = Complete
		// N = No payment yet (default)
		// X = Cancelled/Error
		// P = Pending
		$app = JFactory::getApplication();
		$params = $app->getParams();
		$jInput = $app->input;
		$post = $app->input->post->getArray();
		$src = $jInput->get('src','');
		$prc = $jInput->get('prc','');
		$ord = $jInput->get('Ord','');
		$holder = $jInput->get('Holder','');
		$successCode = $jInput->get('successcode','');
		$ref = $jInput->get('Ref','');
		$payRef = $jInput->get('PayRef','');
		$amt = $jInput->get('Amt','');
		$cur = $jInput->get('Cur','');
		$remark = $jInput->get('remark','');
		$authId = $jInput->get('AuthId','');
		$eci = $jInput->get('eci','');
		$payerAuth = $jInput->get('payerAuth','');
		$sourceIp = $jInput->get('sourceIp','');
		$ipCountry = $jInput->get('ipCountry','');
		$secureHash = $jInput->get('secureHash','');
		$secure_hash_secret = $params->get('secureHash', '');
		/* echo '<pre>';
		print_r($post);
		echo '</pre>';
		exit; */
		if( isset($ref) && isset($successCode) && isset($src) && isset($prc)){
			// Save the feed
			$feedTable = JTable::getInstance("PaydollarFeed", "InsureTable");
			$feedTable->bind($post);
			$feedTable->store();
			//validate secure hash
			$checkSecureHash = false;
			$secureHashArr = explode ( ',', $secureHash );
			if(isset($secureHash) && $secure_hash_secret != ''){
				while ( list ( $key, $value ) = each ( $secureHashArr ) ) {
					$checkSecureHash = $this->verifyPaymentDatafeed($src, $prc, $successCode, $ref, $payRef, $cur, $amt, $payerAuth, $secure_hash_secret, $value);
					if($checkSecureHash){
						break;
					}
				}
			}

			$successcode = $jInput->get('successcode');
			if ($successcode != "0") { // unsuccessful payment, ignored.
				exit;
			}

			// Successful Payment, save the transaction record.
			$remark = $jInput->get('remark');
			$ref = $jInput->get('Ref');

			$validRemarks = array("life", "hospitalself", "hospitalchild", "travel", "domestic", "ci");
			$quote_stages = array(// this is last stage of each insurances
				"life" => 7,
				"ci" => 7,
				"hospitalself" => 6,
				"hospitalchild" => 7,
				"travel" => 6,
				"domestic" => 5,
			);

			$transactionTable = JTable::getInstance("Transaction", "InsureTable");
			$transactionTable->feed_id = $feedTable->id;
			$transactionTable->unique_order_no = $ref;
			$transactionTable->successcode = $successcode;
			$transactionTable->PayRef = $jInput->get('PayRef');
			$transactionTable->Amt = $jInput->get('Amt');
			$transactionTable->TxTime = $jInput->get('TxTime');
			$transactionTable->remark = $remark;
			
			if(!$checkSecureHash) {
				$transactionTable->comment = 'Secure Hash checking failed! Payment reference no: '.$payRef;
				$transactionTable->store();
				exit;
			} 
			if (!in_array($remark, $validRemarks)) { // error with remark
				$transactionTable->comment = "Error with remark!";
				$transactionTable->store();
				exit;
			}

			$table = JTable::getInstance($remark, "InsureTable");
			if (!$table->load(array("unique_order_no" => $ref))) { // quotation not found
				$transactionTable->comment = "Quotation Not Found!";
				$transactionTable->store();
				exit;
			}
			$quotation_id = $table->id;
			// All checkings are done.
			$transactionTable->success = 1;
			$transactionTable->store();
			$table->payment_status = "C";
			$table->quote_status = 1;
			$table->quote_stage = $quote_stages[$remark];
			$table->store();

			// After payment stuffs
			$pdfarr = array(array()); // empty attachments

			/*
			 * Need to send the ack mails here, since travel insurance flow ends after payment status
			 */
			if (strtolower($remark) == "travel") {
				$model = $this->getModel("Form", "InsureDIYTravelModel");
				$model->sendEmails($pdfarr, TRUE, $quotation_id);
				$model->sendEmails($pdfarr, FALSE, $quotation_id);
				$model->createPolicyRecord($quotation_id);
				$model->createPoints($quotation_id);
				exit;
			}

			/*
			 * Need to send the ack mails here, since domestic insurance flow ends after payment status
			 */
			if (strtolower($remark) == "domestic") {
				$model = $this->getModel("Form", "InsureDIYDomesticModel");
				$model->sendEmails($pdfarr, TRUE, $quotation_id);
				$model->sendEmails($pdfarr, FALSE, $quotation_id);
				$model->createPolicyRecord($quotation_id);
				$model->createPoints($quotation_id);
				exit;
			}
		}
		exit;
	}
	public function verifyPaymentDatafeed($src, $prc, $successCode, $merchantReferenceNumber, $paydollarReferenceNumber, $currencyCode, $amount, $payerAuthenticationStatus, $secureHashSecret, $secureHash) {
		$buffer = $src . '|' . $prc . '|' . $successCode . '|' . $merchantReferenceNumber . '|' . $paydollarReferenceNumber . '|' . $currencyCode . '|' . $amount . '|' . $payerAuthenticationStatus . '|' . $secureHashSecret;
		$verifyData = sha1($buffer);
		if ($secureHash == $verifyData) {
			return true;
		}
		return false;
	}
}
