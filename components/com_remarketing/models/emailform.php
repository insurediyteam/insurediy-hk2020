<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Remarketing
 * @author     Yossava A.S <yossava.adhi@insurediy.com>
 * @copyright  2019
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access.
defined('_JEXEC') or die;

jimport('joomla.application.component.modelform');
jimport('joomla.event.dispatcher');

use \Joomla\CMS\Factory;
use \Joomla\Utilities\ArrayHelper;
use \Joomla\CMS\Language\Text;
use \Joomla\CMS\Table\Table;

/**
 * Remarketing model.
 *
 * @since  1.6
 */
class RemarketingModelEmailForm extends \Joomla\CMS\MVC\Model\FormModel
{
    private $item = null;

    

    

    /**
     * Method to auto-populate the model state.
     *
     * Note. Calling getState in this method will result in recursion.
     *
     * @return void
     *
     * @since  1.6
     *
     * @throws Exception
     */


    public function newRecord($data) {
        $value = get_object_vars($data);
        $db = JFactory::getDBO();
        $query = $db->getQuery(true);

        // delete all queue for current user with same type.
        $conditions = array(
            $db->quoteName('email') . ' = ' . $db->quote($value['email']), 
            $db->quoteName('parent') . ' = ' . $db->quote($value['parent'])
        );

        $query->delete($db->quoteName('#__remarketing_email'));
        $query->where($conditions);

        $db->setQuery($query);

        $delete = $db->execute();

        $table = "#__remarketing_email";
        $check = $db->insertObject($table, $data);
    }

    public function queueMail($subject, $body, $id, $time) {

        $adminParams = JComponentHelper::getParams("com_remarketing");

        $startTime = date("Y-m-d H:i:s");
        $convertedTime = date('Y-m-d H:i:s',strtotime('+'.$time.' minutes',strtotime($startTime)));
        $object = new stdClass();

        $object->identifier = $id;
        $object->subject = $subject;
        if ($time == "disable"){
            $object->status = "disabled";
            $object->schedule = "";
        } else {
            $object->status = "queued";
            $object->schedule = $convertedTime;
        }
        
        $object->body = $body;
        $object->state = 1;

        $result = JFactory::getDbo()->updateObject('#__remarketing_email', $object, 'identifier');

    }

    public function queueCarAfterMail($products, $email, $id) {
        $db = JFactory::getDBO();
        $adminParams = JComponentHelper::getParams("com_remarketing");
        $time = $adminParams["car_live"];
        $subject = $adminParams["car_after_subject"];
        $body = $this->productReplace($adminParams["car_after_body"], $products);
        $queue =  $this->queueMail($subject, $body, $id, $time);
    }

    public function queueCarBeforeMail($email, $id) {
        $db = JFactory::getDBO();
        $adminParams = JComponentHelper::getParams("com_remarketing");
        $time = $adminParams["car_live"];
        $subject = $adminParams["car_before_subject"];
        $body = $adminParams["car_before_body"];
        $queue =  $this->queueMail($subject, $body, $id, $time);
    }

    public function queueLifeBeforeMail($products, $email, $id) {
        $db = JFactory::getDBO();
        $adminParams = JComponentHelper::getParams("com_remarketing");
        $time = $adminParams["life_live"];
        $subject = $adminParams["life_before_subject"];
        $products = $this->getInsLogo($products, "Life Insurance");
        $body = $this->productReplace($adminParams["life_before_body"], $products);
        $queue =  $this->queueMail($subject, $body, $id, $time);
    }

    public function queueLifeAfterMail($products, $email, $id) {
        $db = JFactory::getDBO();
        $adminParams = JComponentHelper::getParams("com_remarketing");
        $time = $adminParams["life_live"];
        $subject = $adminParams["life_after_subject"];
        $products = $this->getInsLogo($products, "Life Insurance");
        $body = $this->productReplace($adminParams["life_after_body"], $products);
        $queue =  $this->queueMail($subject, $body, $id, $time);
    }

    public function queueCiBeforeMail($products, $email, $id) {
        $db = JFactory::getDBO();
        $adminParams = JComponentHelper::getParams("com_remarketing");
        $time = $adminParams["ci_live"];
        $subject = $adminParams["ci_before_subject"];
        $products = $this->getInsLogo($products, "Critical Illness Insurance");
        $body = $this->productReplace($adminParams["ci_before_body"], $products);
        $queue =  $this->queueMail($subject, $body, $id, $time);
    }

    public function queueCiAfterMail($products, $email, $id) {
        $db = JFactory::getDBO();
        $adminParams = JComponentHelper::getParams("com_remarketing");
        $time = $adminParams["ci_live"];
        $subject = $adminParams["ci_after_subject"];
        $products = $this->getInsLogo($products, "Critical Illness Insurance");
        $body = $this->productReplace($adminParams["ci_after_body"], $products);
        $queue =  $this->queueMail($subject, $body, $id, $time);
    }

    public function queueHospitalSelfBeforeMail($products, $email, $id) {
        $db = JFactory::getDBO();
        $adminParams = JComponentHelper::getParams("com_remarketing");
        $time = $adminParams["med_live"];
        $subject = $adminParams["med_before_subject"];
        $products = $this->getInsLogo($products, "Medical Insurance");
        $body = $this->productReplace($adminParams["med_before_body"], $products);
        $queue =  $this->queueMail($subject, $body, $id, $time);
    }

    public function queueHospitalSelfAfterMail($products, $email, $id) {
        $db = JFactory::getDBO();
        $adminParams = JComponentHelper::getParams("com_remarketing");
        $time = $adminParams["med_live"];
        $subject = $adminParams["med_after_subject"];
        $products = $this->getInsLogo($products, "Medical Insurance");
        $body = $this->productReplace($adminParams["med_after_body"], $products);
        $queue =  $this->queueMail($subject, $body, $id, $time);
    }

    public function queueHospitalChildBeforeMail($products, $email, $id) {
        $db = JFactory::getDBO();
        $adminParams = JComponentHelper::getParams("com_remarketing");
        $time = $adminParams["child_live"];
        $subject = $adminParams["child_before_subject"];
        $products = $this->getInsLogo($products, "Medical Insurance");
        $body = $this->productReplace($adminParams["child_before_body"], $products);
        $queue =  $this->queueMail($subject, $body, $id, $time);
    }

    public function queueHospitalChildAfterMail($products, $email, $id) {
        $db = JFactory::getDBO();
        $adminParams = JComponentHelper::getParams("com_remarketing");
        $time = $adminParams["child_live"];
        $subject = $adminParams["child_after_subject"];
        $products = $this->getInsLogo($products, "Medical Insurance");
        $body = $this->productReplace($adminParams["child_after_body"], $products);
        $queue =  $this->queueMail($subject, $body, $id, $time);
    }

    public function queueTravelBeforeMail($products, $email, $id) {
        $db = JFactory::getDBO();
        $adminParams = JComponentHelper::getParams("com_remarketing");
        $time = $adminParams["travel_live"];
        $subject = $adminParams["travel_before_subject"];
        $products = $this->getInsLogo($products, "Travel Insurance");
        $body = $this->productReplace($adminParams["travel_before_body"], $products);
        $queue =  $this->queueMail($subject, $body, $id, $time);
    }

    public function queueTravelAfterMail($products, $email, $id) {
        $db = JFactory::getDBO();
        $adminParams = JComponentHelper::getParams("com_remarketing");
        $time = $adminParams["travel_live"];
        $subject = $adminParams["travel_after_subject"];
        $products = $this->getInsLogo($products, "Travel Insurance");
        $body = $this->productReplace($adminParams["travel_after_body"], $products);
        $queue =  $this->queueMail($subject, $body, $id, $time);
    }

    public function queueDhiBeforeMail($products, $email, $id) {
        $db = JFactory::getDBO();
        $adminParams = JComponentHelper::getParams("com_remarketing");
        $time = $adminParams["dhi_live"];
        $subject = $adminParams["dhi_before_subject"];
        $products = $this->getInsLogo($products, "Domestic Helper Insurance");
        $body = $this->productReplace($adminParams["dhi_before_body"], $products);
        $queue =  $this->queueMail($subject, $body, $id, $time);
    }

    public function queueDhiAfterMail($products, $email, $id) {
        $db = JFactory::getDBO();
        $adminParams = JComponentHelper::getParams("com_remarketing");
        $time = $adminParams["dhi_live"];
        $subject = $adminParams["dhi_after_subject"];
        $products = $this->getInsLogo($products, "Domestic Helper Insurance");
        $body = $this->productReplace($adminParams["dhi_after_body"], $products);
        $queue =  $this->queueMail($subject, $body, $id, $time);
    }

    public function getInsLogo($products, $ins){
        $pol = [];
        $prod = [];
        $list = [];
        $count = 0;
        usort($products, function($a, $b) {
            return $a['premium'] - $b['premium'];
        });
        foreach ($products as $key => $value) {
            switch (strtoupper($value["code"])) {
                case 'AIA':
                    $name = "AIA " . $ins;
                    $logo = JURI::root()."images/logo_companies/f7bg/f7bg-aia.jpg";
                    break;
                case 'AIG':
                    $name = "AIG " . $ins;
                    $logo = JURI::root()."images/logo_companies/f7bg/f7bg-aig.jpg";
                    break;
                case 'ALL':
                    $name = "ALLIED WORLD " . $ins;
                    $logo = JURI::root()."images/logo_companies/f7bg/f7bg-aw.jpg";
                    break;
                case 'AXA':
                    $name = "AXA " . $ins;
                    $logo = JURI::root()."images/logo_companies/f7bg/f7bg-axa.jpg";
                    break;
                case 'BUP':
                    $name = "BUPA " . $ins;
                    $logo = JURI::root()."images/logo_companies/f7bg/f7bg-bupa.jpg";
                    break;
                case 'CHU':
                    $name = "CHUBB " . $ins;
                    $logo = JURI::root()."images/logo_companies/f7bg/f7bg-chubb.jpg";
                    break;
                case 'CHB':
                    $name = "CHUBB " . $ins;
                    $logo = JURI::root()."images/logo_companies/f7bg/f7bg-chubb.jpg";
                    break;
                case 'CTP':
                    $name = "CHINA TAIPING " . $ins;
                    $logo = JURI::root()."images/logo_companies/f7bg/f7bg-ctp.jpg";
                    break;
                case 'MSI':
                    $name = "MSIG " . $ins;
                    $logo = JURI::root()."images/logo_companies/f7bg/f7bg-msig.jpg";
                    break;
                case 'PIN':
                    $name = "PINGAN " . $ins;
                    $logo = JURI::root()."images/logo_companies/f7bg/f7bg-pingan.jpg";
                    break;
                case 'SUN':
                    $name = "SUN LIFE " . $ins;
                    $logo = JURI::root()."images/logo_companies/f7bg/f7bg-sunlife.jpg";
                    break;
                case 'LIB':
                    $name = "LIBERTY " . $ins;
                    $logo = JURI::root()."images/logo_companies/f7bg/f7bg-liberty.jpg";
                    break;
                case 'ZUR':
                    $name = "ZURICH " . $ins;
                    $logo = JURI::root()."images/logo_companies/f7bg/f7bg-zurich.jpg";
                    break;
                default:
                    $name = "InsureDIY " . $ins;
                    $logo = JURI::root()."images/logo_companies/f7bg/insurediy-logo.jpg";
                    break;
            }

            $pol["name"] = $name;
            $pol["image"] = $logo;
            $pol["premium"] = $value["premium"];
            if (!in_array($value["code"], $list)) {
                if ($count < 3) {
                    array_push($prod, $pol);
                }
                $count++;
            }
            array_push($list, $value["code"]);
        }

         return $prod;
        
    }

    public function productReplace($body, $products) {
        $adminParams = JComponentHelper::getParams("com_remarketing");
        $tr = $adminParams["product_table"];
        $product = "";
        foreach ($products as $key => $value) {
            $table = $tr;
            $table = str_replace('{name}', $value["name"], $table);
            $table = str_replace('{image}', $value["image"], $table);
            if ($value["premium"] == 0) {
                $table = str_replace('{price}', "RENEW NOW", $table);
            } else {
                $table = str_replace('{price}', "HK$ " . number_format($value["premium"], 2), $table);
            }
            $product .= $table;
        }
        return str_replace('{products}', $product, $body);
    }

    protected function populateState()
    {
        $app = Factory::getApplication('com_remarketing');

        // Load state from the request userState on edit or from the passed variable on default
        if (Factory::getApplication()->input->get('layout') == 'edit')
        {
                $id = Factory::getApplication()->getUserState('com_remarketing.edit.email.id');
        }
        else
        {
                $id = Factory::getApplication()->input->get('id');
                Factory::getApplication()->setUserState('com_remarketing.edit.email.id', $id);
        }

        $this->setState('email.id', $id);

        // Load the parameters.
        $params       = $app->getParams();
        $params_array = $params->toArray();

        if (isset($params_array['item_id']))
        {
                $this->setState('email.id', $params_array['item_id']);
        }

        $this->setState('params', $params);
    }

    /**
     * Method to get an ojbect.
     *
     * @param   integer $id The id of the object to get.
     *
     * @return Object|boolean Object on success, false on failure.
     *
     * @throws Exception
     */
    public function getItem($id = null)
    {
        if ($this->item === null)
        {
            $this->item = false;

            if (empty($id))
            {
                    $id = $this->getState('email.id');
            }

            // Get a level row instance.
            $table = $this->getTable();

            if ($table !== false && $table->load($id))
            {
                $user = Factory::getUser();
                $id   = $table->id;
                

                $canEdit = $user->authorise('core.edit', 'com_remarketing') || $user->authorise('core.create', 'com_remarketing');

                if (!$canEdit && $user->authorise('core.edit.own', 'com_remarketing'))
                {
                        $canEdit = $user->id == $table->created_by;
                }

                if (!$canEdit)
                {
                        throw new Exception(Text::_('JERROR_ALERTNOAUTHOR'), 403);
                }

                // Check published state.
                if ($published = $this->getState('filter.published'))
                {
                        if (isset($table->state) && $table->state != $published)
                        {
                                return $this->item;
                        }
                }

                // Convert the JTable to a clean JObject.
                $properties = $table->getProperties(1);
                $this->item = ArrayHelper::toObject($properties, 'JObject');
                

                
            }
        }

        return $this->item;
    }

    /**
     * Method to get the table
     *
     * @param   string $type   Name of the JTable class
     * @param   string $prefix Optional prefix for the table class name
     * @param   array  $config Optional configuration array for JTable object
     *
     * @return  JTable|boolean JTable if found, boolean false on failure
     */
    public function getTable($type = 'Email', $prefix = 'RemarketingTable', $config = array())
    {
        $this->addTablePath(JPATH_ADMINISTRATOR . '/components/com_remarketing/tables');

        return Table::getInstance($type, $prefix, $config);
    }

    /**
     * Get an item by alias
     *
     * @param   string $alias Alias string
     *
     * @return int Element id
     */
    public function getItemIdByAlias($alias)
    {
        $table      = $this->getTable();
        $properties = $table->getProperties();

        if (!in_array('alias', $properties))
        {
                return null;
        }

        $table->load(array('alias' => $alias));


        
            return $table->id;
        
    }

    /**
     * Method to check in an item.
     *
     * @param   integer $id The id of the row to check out.
     *
     * @return  boolean True on success, false on failure.
     *
     * @since    1.6
     */
    public function checkin($id = null)
    {
        // Get the id.
        $id = (!empty($id)) ? $id : (int) $this->getState('email.id');
        
        if ($id)
        {
            // Initialise the table
            $table = $this->getTable();

            // Attempt to check the row in.
            if (method_exists($table, 'checkin'))
            {
                if (!$table->checkin($id))
                {
                    return false;
                }
            }
        }

        return true;
        
    }

    /**
     * Method to check out an item for editing.
     *
     * @param   integer $id The id of the row to check out.
     *
     * @return  boolean True on success, false on failure.
     *
     * @since    1.6
     */
    public function checkout($id = null)
    {
        // Get the user id.
        $id = (!empty($id)) ? $id : (int) $this->getState('email.id');
        
        if ($id)
        {
            // Initialise the table
            $table = $this->getTable();

            // Get the current user object.
            $user = Factory::getUser();

            // Attempt to check the row out.
            if (method_exists($table, 'checkout'))
            {
                if (!$table->checkout($user->get('id'), $id))
                {
                    return false;
                }
            }
        }

        return true;
        
    }

    /**
     * Method to get the profile form.
     *
     * The base form is loaded from XML
     *
     * @param   array   $data     An optional array of data for the form to interogate.
     * @param   boolean $loadData True if the form is to load its own data (default case), false if not.
     *
     * @return    JForm    A JForm object on success, false on failure
     *
     * @since    1.6
     */
    public function getForm($data = array(), $loadData = true)
    {
        // Get the form.
        $form = $this->loadForm('com_remarketing.email', 'emailform', array(
                        'control'   => 'jform',
                        'load_data' => $loadData
                )
        );

        if (empty($form))
        {
                return false;
        }

        return $form;
    }

    /**
     * Method to get the data that should be injected in the form.
     *
     * @return    mixed    The data for the form.
     *
     * @since    1.6
     */
    protected function loadFormData()
    {
        $data = Factory::getApplication()->getUserState('com_remarketing.edit.email.data', array());

        if (empty($data))
        {
            $data = $this->getItem();
        }
        

        return $data;
    }

    /**
     * Method to save the form data.
     *
     * @param   array $data The form data
     *
     * @return bool
     *
     * @throws Exception
     * @since 1.6
     */
    public function save($data)
    {
        $id    = (!empty($data['id'])) ? $data['id'] : (int) $this->getState('email.id');
        $state = (!empty($data['state'])) ? 1 : 0;
        $user  = Factory::getUser();

        
        if ($id)
        {
            // Check the user can edit this item
            $authorised = $user->authorise('core.edit', 'com_remarketing') || $authorised = $user->authorise('core.edit.own', 'com_remarketing');
        }
        else
        {
            // Check the user can create new items in this section
            $authorised = $user->authorise('core.create', 'com_remarketing');
        }

        if ($authorised !== true)
        {
            throw new Exception(Text::_('JERROR_ALERTNOAUTHOR'), 403);
        }

        $table = $this->getTable();

        if ($table->save($data) === true)
        {
            return $table->id;
        }
        else
        {
            return false;
        }
        
    }

    /**
     * Method to delete data
     *
     * @param   int $pk Item primary key
     *
     * @return  int  The id of the deleted item
     *
     * @throws Exception
     *
     * @since 1.6
     */
    public function delete($pk)
    {
        $user = Factory::getUser();

        
            if (empty($pk))
            {
                    $pk = (int) $this->getState('email.id');
            }

            if ($pk == 0 || $this->getItem($pk) == null)
            {
                    throw new Exception(Text::_('COM_REMARKETING_ITEM_DOESNT_EXIST'), 404);
            }

            if ($user->authorise('core.delete', 'com_remarketing') !== true)
            {
                    throw new Exception(Text::_('JERROR_ALERTNOAUTHOR'), 403);
            }

            $table = $this->getTable();

            if ($table->delete($pk) !== true)
            {
                    throw new Exception(Text::_('JERROR_FAILED'), 501);
            }

            return $pk;
        
    }

    /**
     * Check if data can be saved
     *
     * @return bool
     */
    public function getCanSave()
    {
        $table = $this->getTable();

        return $table !== false;
    }
    
}
