<?php

/**
 * @version    CVS: 1.0.0
 * @package    Com_Remarketing
 * @author     Yossava A.S <yossava.adhi@insurediy.com>
 * @copyright  2019
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JLoader::register('RemarketingHelper', JPATH_ADMINISTRATOR . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_remarketing' . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'remarketing.php');

use \Joomla\CMS\Factory;
use \Joomla\CMS\MVC\Model\BaseDatabaseModel;

/**
 * Class RemarketingFrontendHelper
 *
 * @since  1.6
 */
class RemarketingHelpersRemarketing
{
	/**
	 * Get an instance of the named model
	 *
	 * @param   string  $name  Model name
	 *
	 * @return null|object
	 */
	public static function getModel($name)
	{
		$model = null;

		// If the file exists, let's
		if (file_exists(JPATH_SITE . '/components/com_remarketing/models/' . strtolower($name) . '.php'))
		{
			require_once JPATH_SITE . '/components/com_remarketing/models/' . strtolower($name) . '.php';
			$model = BaseDatabaseModel::getInstance($name, 'RemarketingModel');
		}

		return $model;
	}

	public static function newRemarketing($data){
		$value = get_object_vars($data);
		$data->identifier = sha1($value['email'].$value['type']);
		$model = RemarketingHelpersRemarketing::getModel('EmailForm', 'RemarketingModel');
		$model->newRecord($data);
		switch ($data->type) {
			case "car-before":
				$model->queueCarBeforeMail($data->email, $data->identifier);
				break;
			case "car-after":
				$model->queueCarAfterMail($data->products, $data->email, $data->identifier);
				break;
			case "life-before":
				$model->queueLifeBeforeMail($data->products, $data->email, $data->identifier);
				break;
			case "life-after":
				$model->queueLifeAfterMail($data->products, $data->email, $data->identifier);
				break;
			case "ci-before":
				$model->queueCiBeforeMail($data->products, $data->email, $data->identifier);
				break;
			case "ci-after":
				$model->queueCiAfterMail($data->products, $data->email, $data->identifier);
				break;
			case "hospital-self-before":
				$model->queueHospitalSelfBeforeMail($data->products, $data->email, $data->identifier);
				break;
			case "hospital-self-after":
				$model->queueHospitalSelfAfterMail($data->products, $data->email, $data->identifier);
				break;
			case "hospital-child-before":
				$model->queueHospitalChildBeforeMail($data->products, $data->email, $data->identifier);
				break;
			case "hospital-child-after":
				$model->queueHospitalChildAfterMail($data->products, $data->email, $data->identifier);
				break;
			case "travel-before":
				$model->queueTravelBeforeMail($data->products, $data->email, $data->identifier);
				break;
			case "travel-after":
				$model->queueTravelAfterMail($data->products, $data->email, $data->identifier);
				break;
			case "dhi-before":
				$model->queueDhiBeforeMail($data->products, $data->email, $data->identifier);
				break;
			case "dhi-after":
				$model->queueDhiAfterMail($data->products, $data->email, $data->identifier);
				break;
			default:
				
		}

	}

	/**
	 * Gets the files attached to an item
	 *
	 * @param   int     $pk     The item's id
	 *
	 * @param   string  $table  The table's name
	 *
	 * @param   string  $field  The field's name
	 *
	 * @return  array  The files
	 */
	public static function getFiles($pk, $table, $field)
	{
		$db = Factory::getDbo();
		$query = $db->getQuery(true);

		$query
			->select($field)
			->from($table)
			->where('id = ' . (int) $pk);

		$db->setQuery($query);

		return explode(',', $db->loadResult());
	}

    /**
     * Gets the edit permission for an user
     *
     * @param   mixed  $item  The item
     *
     * @return  bool
     */
    public static function canUserEdit($item)
    {
        $permission = false;
        $user       = Factory::getUser();

        if ($user->authorise('core.edit', 'com_remarketing'))
        {
            $permission = true;
        }
        else
        {
            if (isset($item->created_by))
            {
                if ($user->authorise('core.edit.own', 'com_remarketing') && $item->created_by == $user->id)
                {
                    $permission = true;
                }
            }
            else
            {
                $permission = true;
            }
        }

        return $permission;
    }
}
