<?php

defined('_JEXEC') or die;

class InsureDIYHospitalChildControllerForm extends JControllerForm {

	private $base_layout_url = "index.php?option=com_insurediyhospitalchild&view=form&layout=";
	private $form_url = "index.php?option=com_insurediyhospitalchild&Itemid=215";

	public function getModel($name = 'form', $prefix = '', $config = array('ignore_request' => true)) {
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}

	public function step1save() {
		$session = JFactory::getSession();
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$post = $app->input->get('jform', '', 'array');
		$step1CriteriaPass = $this->step1CriteriaCheck($post);
		$quotation_id = $model->step1save($post);
		if ($quotation_id) {
			$session->set('hospitalchild.step1CriteriaPass', $step1CriteriaPass);
			$session->set('hospitalchild.quotation_id', $quotation_id);
		}
		
		$layout = InsureDIYHospitalChildHelper::getLayout();
		
		if($layout == 'login') {
			$app->redirect(JRoute::_($this->form_url.'&step=sign_in'));
		} else {
			$app->redirect(JRoute::_($this->form_url.'&step=2'));
		}
		
	}

	public function step1CriteriaCheck($data) {
		return !($data['has_weight_change'] || $data['has_used_tobacco'] || $data['has_used_alcohol'] || $data['has_used_drugs'] || $data['has_reside_overseas'] || $data['has_claimed_insurance']);
	}

	public function step2save() {
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$post = $app->input->get('jform', '', 'array');
		if ($model->step2save($post)) {
			$continueWithError = $app->input->get('continueWithError', TRUE);
			if (!$continueWithError) {
				InsureDIYHospitalSelfHelper::clearSessionData();
				$app->redirect("index.php");
			}
		}
		$app->redirect(JRoute::_($this->form_url.'&step=3'));
	}

	public function step3save() {
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$check = TRUE;
		$plan_id = $app->input->post->get('plan_id', FALSE, 'int');
		$quotation_id = InsureDIYHospitalChildHelper::getQid();

		if ($quotation_id && $plan_id) {
			$check = $model->step3save($quotation_id, $plan_id);
		} else {
			$check = FALSE;
		}
		if ($check) {
			$app->redirect(JRoute::_($this->form_url.'&step=4'));
		} else {
			$app->redirect(JRoute::_($this->form_url.'&step=3'), JText::_("DB_SAVE_FAIL"));
		}
	}

	public function step4save() {
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$post = $app->input->get('jform', '', 'array');
		$quotation_id = InsureDIYHospitalChildHelper::getQid();
		if (!$model->step4save($post)) {
			$app->redirect(JRoute::_($this->form_url.'&step=4'), JText::_("DB_SAVE_FAIL"));
		}
		$app->redirect(JRoute::_($this->form_url.'&step=5'));
	}

	public function step5save() {
		$app = JFactory::getApplication();
		$session = JFactory::getSession();
		$model = $this->getModel();
		$post = $app->input->get('jform', '', 'array');
		$quotation_id = InsureDIYHospitalChildHelper::getQid();
		$referred_by = $post['referred_by'];
		if (!InsureDIYHelper::checkReferral($referred_by)) { // referral is valid
			unset($post['referred_by']);
		}
		if (!$model->step5save($post)) {
			$app->redirect(JRoute::_($this->form_url.'&step=5'), JText::_("DB_SAVE_FAIL"));
		}
		$app->redirect(JRoute::_($this->form_url.'&step=6'));
	}

	public function step7save() {
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$quotation_id = InsureDIYHospitalChildHelper::getQid();

		$post = $app->input->get('jform', '', 'array');
		$post['id'] = $quotation_id;
		$this->saveFileUpload(FALSE);
		$check = $model->step7save($post);
		if ($check) {
			$model->createPolicyRecord($quotation_id);
			$model->createPoints($quotation_id);
			// Send email n generate pdf
//			$pdfarr = array();
//			$pdfarr[] = array();
//			$model->sendEmails($pdfarr);
//			$model->sendEmails($pdfarr, FALSE);
		} else {

		}
		//InsureDIYHospitalChildHelper::clearSessionData(); // woot~ all done. clear the session
		$app->redirect($this->getRedirectUrl('thankyou'));
	}

	public function saveBeforePayment() {
		$app = JFactory::getApplication();
		$post = $app->input->post->getArray();
		$model = $this->getModel();
		echo json_encode($model->saveConfirmationDetail($post));
		exit;
	}

	public function saveFileUpload($redirect = TRUE) {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$app = JFactory::getApplication();
		$files = $app->input->files->get('jform', null);
		if ($files['file_identity_card']['error'] > 0 && $files['file_birth_cert']['error'] > 0 && $files['file_proof_address']['error'] > 0) {
			$msg = 'No File Uploaded';
		} else {
			$model = $this->getModel();
			$path = 'media/com_insurediyhospitalchild/documents';
			$row1 = $model->savePhotoIdentityFileUpload($files['file_identity_card'], $path, 'file_identity_card');
			$row3 = $model->savePhotoIdentityFileUpload($files['file_proof_address'], $path, 'file_proof_address');
		}
		if ($redirect) {
			$app->redirect(JRoute::_($this->form_url.'&step=7'), $msg);
		}
	}

	public function deleteDocuments() {
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$session = JFactory::getSession();
		$app = JFactory::getApplication();
		$db = JFactory::getDBO();
		$quotation_id = InsureDIYHospitalChildHelper::getQid();
		$file_type = JRequest::getVar('file_type');
		$query = " SELECT " . $file_type . " FROM #__insure_hospitalchild_quotations WHERE id = '$quotation_id' LIMIT 1 ";
		$file_entry = $db->setQuery($query)->loadResult();
		$file_entry = explode("|", $file_entry);
		$filename = (isset($file_entry[0])) ? $file_entry[0] : FALSE;
		if ($filename) {
			$query = " UPDATE #__insure_hospitalchild_quotations SET " . $file_type . " = '' WHERE id = '$quotation_id' ";
			$db->setQuery($query);
			$db->query();
			unlink(JPATH_BASE . '/media/com_insurediyhospitalchild/documents/' . $filename);
			$oriname = (isset($file_entry[1])) ? $file_entry[1] : $filename;
			$msg = $oriname . ' has been deleted ';
		} else {
			$msg = 'Error deleting file.';
		}
		$quotation = InsureDIYHospitalChildHelper::getQuotation($quotation_id);
		$session->set("hospitalchild.data", $quotation);
		$app->redirect(JRoute::_($this->form_url.'&step=7'), $msg);
	}

	public function startNewQuotation() {
		JSession::checkToken() or jexit(JText::_("JINVALID_TOKEN"));
		$app = JFactory::getApplication();
		InsureDIYHospitalChildHelper::clearSessionData();
		$app->redirect($this->base_layout_url . "&Itemid=172");
	}

	public function getRedirectUrl($layout) {
		$url = $this->base_layout_url . $layout;
		return $url;
	}

	public function updatePlanOptions() {
		JSession::checkToken() or jexit(JText::_("JINVALID_TOKEN"));
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$quotation_id = InsureDIYHospitalChildHelper::getQid();
		$post = $app->input->get('jform', '', 'array');
		if ($model->updateQuotation($quotation_id, $post)) {
			$app->redirect(JRoute::_($this->form_url.'&step=3'));
		} else {
			$app->redirect(JRoute::_($this->form_url.'&step=3'), JText::_("DB_SAVE_FAIL"));
		}
	}

	public function back() {
		JSession::checkToken() or jexit(JText::_("JINVALID_TOKEN"));
		$app = JFactory::getApplication();
		$quotation_id = $app->input->post->get("quotation_id", FALSE, "integer");
		$backstage = 0;
		if ($quotation_id) {
			$model = $this->getModel();
			$backstage = $model->back($quotation_id);
		}
		$app->redirect(JRoute::_($this->form_url.'&step='.$backstage));
	}

}
