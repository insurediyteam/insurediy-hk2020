<?php
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', '.insurediy-form select');
JHtml::_('MyBehavior.jsInsurediy');
JHtml::_('script', 'system/jquery.validate.js', false, true);


$currency = InsureDIYHospitalChildHelper::getCurrency();

$form = & $this->form;
$quotation = $this->quotation;
$fldGroup = "contact-details";
$exInsurances = $this->quotation["exInsurances"];

// remarketing email
$user = $this->user;
$products = [];
$products[0]["premium"] = $quotation["selected_plan"]->premium;
$products[0]["code"] = $quotation["selected_plan"]->insurer_code;

$mailQueue = (object)array(
	"email" => $user->email,
	"products" => $products,
	"type" => "hospital-child-after",
	"parent" => "child"
);

RemarketingHelpersRemarketing::newRemarketing($mailQueue);
// end of remarketing email
?>

<script type="text/javascript">
	var row = <?php echo count($exInsurances) + 1; ?>;
	var jRow;
	function duplicateRow(div_id) {
		row = row + 1;
		var jOriginal = jQuery("#" + div_id);
		var jClone = jRow.clone();
		var newId = jOriginal.prop("id") + "_" + row;
		jClone.prop("id", newId);
		jClone.addClass("epi-contact-detail-wrapper");
		jClone.find("#jform_policy_type_chzn").remove();

		jClone.find("input").each(function () {
			var jThis = jQuery(this);
			jThis.prop("id", jThis.prop("id") + "_" + row);
			jThis.prop("readonly", "");
			jThis.val("");
			if (jThis.hasClass("hasDatepicker")) {
				jClone.find(".ui-datepicker-trigger").remove();
				jThis.removeClass("hasDatepicker").datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "media/system/images/ico-calendar.png",
					dateFormat: "dd-mm-yy",
					yearRange: "1984:2014",
					minDate: new Date(1984, 0, 1)
				});
			}
		});
		jClone.prepend("<button type=\"button\" class=\"close\" aria-hidden=\"true\" onclick=\"javascript:removeExInsurace('#" + newId + "');\">×</button>");
		jQuery("#ajax-load-existing-insurance-policy").append(jClone);

		var jSelect = jQuery("#" + newId + " select");
		var newSelectId = jSelect.prop("id") + "_" + row;
		jSelect.prop("id", newSelectId);
		jSelect.css('display', 'block');
		var jTypeId = jClone.find("input[name^='jform[sum_insured]']").attr("id");
		var jSum = jQuery("#" + jTypeId);

		jQuery("#" + newSelectId).removeClass("chzn-done");
		jQuery("#" + newSelectId).chosen({
			disable_search_threshold: 10,
			allow_single_deselect: true
		}).change(function (event) {
			if (event.target == this) {
				var val = jQuery(this).val();
				if (val == "Hospital Insurance") {
					jSum.val("");
					jSum.prop("readonly", "readyonly");
				} else {
					jSum.prop("readonly", "");
				}
			}
		});
		jQuery("#" + newSelectId).val('').trigger('liszt:updated');

		onNumberOnlyAndFormat("#" + jTypeId);
	}

	function removeExInsurace(id) {
		jQuery(id).remove();
	}

	function toggleEdit(div_id, isEdit) {
		//do edit
		if (isEdit === true) {
			$(div_id).toggleClass("readonly", true);
		} else if (isEdit === false) {
			$(div_id).toggleClass("readonly", false);
		} else {
			$(div_id).toggleClass("readonly");
		}
		if (jQuery("#" + div_id).hasClass("readonly")) {
			$$("#" + div_id + " input:not('.ignore-edit')").set("readonly", "readonly");
//			$$("#" + div_id + " select:not('.ignore-edit')").set("disabled", "disabled");
			jQuery("#" + div_id + " select:not('.ignore-edit')").prop('disabled', true).trigger("liszt:updated");
			jQuery("#" + div_id + " select:not('.ignore-edit')").prop('disabled', false);
		} else {
			$$("#" + div_id + " input:not('.ignore-edit')").set("readonly", "");
//			$$("#" + div_id + " select:not('.ignore-edit')").set("disabled", "");
			jQuery("#" + div_id + " select:not('.ignore-edit')").prop('disabled', false).trigger("liszt:updated");
		}
//		$$("#" + div_id + " .chzn-container").toggleClass("chzn-disabled");
	}

	function toggleThis(div_id, btn_id) {
		new Fx.Slide(div_id, {resetHeight: true}).toggle();
//		jQuery("#" + div_id).toggle(100);
		$(btn_id).toggleClass("active");
	}

	function selectOnChange() {
		var jThis = jQuery(this);
		var jControl = jThis.closest("div.controls");
		if (jThis.val()) {
			jControl.find(".error-container label.error").remove();
		} else {
			jControl.find("label.error").each(function () {
				var jThis = jQuery(this);
				var jContainer = jThis.closest("div.control-group");
				var jControl = jContainer.find("div.controls");
				jControl.find(".error-container").append(jThis.clone());
				jThis.remove();
			});
		}
	}

	window.addEvent('domready', function () {
		jQuery("#adminForm").validate({
			ignore: [] // <-- option so that hidden elements are validated
		});
		var jForm = jQuery("form#adminForm");
		var jSelects = jForm.find("select");
		var jInputs = jForm.find("input");
		jForm.submit(function () {
			jForm.find("label.error").each(function () {
				var jThis = jQuery(this);
				var jContainer = jThis.closest("div.control-group");
				var jControl = jContainer.find("div.controls");
				jControl.find(".error-container").append(jThis.clone());
				jThis.remove();
			});
			var errors = jForm.find("label.error");
			if (errors.length > 0) {
				var label = jForm.find("label.error:first");
				jQuery('html, body').animate({
					'scrollTop': label.offset().top - 400
				}, 300);
			}
		});
		jSelects.each(function () {
			var jThis = jQuery(this);
			jThis.on("change", selectOnChange);
		});
		jInputs.each(function () {
			var jThis = jQuery(this);
			jThis.on("change", selectOnChange);
		});


		var jOriginal = jQuery("#existing-policy-insurance-contact-detail");
		jRow = jOriginal.clone();

		onNumberOnlyAndFormat("#jform_sum_insured");
		onTypeOfPolicyCheck("#jform_policy_type", "#jform_sum_insured");
		if (row > 1) {
			for (var i = 2; i <= row; i++) {
				onNumberOnlyAndFormat("#jform_sum_insured_" + i);
				onTypeOfPolicyCheck("#jform_policy_type_" + i, "#jform_sum_insured_" + i);
			}
		}
	});

	function onTypeOfPolicyCheck(type_id, sum_id) {
		var jInput = jQuery(type_id);
		var jSum = jQuery(sum_id);
		jInput.change(function (e) {
			var val = jInput.chosen().val();
			if (val == "Hospital Insurance") {
				jSum.val("");
				jSum.prop("readonly", "readyonly");
			} else {
				jSum.prop("readonly", "");
			}
		});
	}

	function onNumberOnlyAndFormat(input_id) {
		var jInput = jQuery(input_id);
		jInput.keydown(function (event) {
			var jThis = jQuery(this);
			var val = jThis.val();
			var hasDot = val.indexOf(".");
			// Allow only backspace and delete
			if (event.keyCode == 9 || event.keyCode == 46 || event.keyCode == 8 || (event.keyCode == 190 && hasDot == -1)) {
				// let it happen, don't do anything
			}
			else {
				// Ensure that it is a number and stop the keypress
				if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105)) {
					// 0-9
				} else {
					event.preventDefault();
				}
			}
		});
		jInput.on("keyup change", function () {
			var jThis = jQuery(this);
			var val = jThis.val();
			if (val.length > 0) {
				jThis.val(numberFormat(val));
			}
		});
	}

	function numberFormat(number) {
		number = parseFloat(number.replace(/,/g, ''));
		return number.formatMoney(0, '.', ',');
	}

	Number.prototype.formatMoney = function (c, d, t) {
		var n = this;
		c = isNaN(c = Math.abs(c)) ? 2 : c;
		d = d === undefined ? "." : d;
		t = t === undefined ? "," : t;
		var s = n < 0 ? "-" : "";
		var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "";
		var j = (j = i.length) > 3 ? j % 3 : 0;
		return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	}
</script>

<div class="insurediy-form">
	<div class="header-top-wrapper">
		<?php echo InsureDIYHospitalChildHelper::renderHeader('icon-contact-details', JText::_('COM_INSUREDIYHOSPITALCHILD_PAGE_HEADING_CHILD_CONTACT_DETAILS'), 3); ?>
	</div>
	<div class="insurediy-form-content">
		<?php echo MyHelper::renderDefaultMessage(); ?>
		<form action="<?php echo JRoute::_('index.php?option=com_insurediyhospitalchild&view=form'); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-vertical">
			<fieldset class="insurediy-form-fields mychild-fields">
				<div id="contact-details-fields" class="contact-details-fields clearfix mychild-fields">
					<div class="clearfix">
						<div class="cpan3 clearfix">
							<div class="control-group">
								<div class="control-label"><?php echo $this->form->getLabel('child_firstname'); ?></div>
								<div class="controls"><?php echo $this->form->getInput('child_firstname'); ?>
									<div class="error-container"></div>
								</div>
							</div>
						</div>
						<div class="cpan3 clearfix">
							<div class="control-group">
								<div class="control-label"><?php echo $this->form->getLabel('child_lastname'); ?></div>
								<div class="controls"><?php echo $this->form->getInput('child_lastname'); ?>
									<div class="error-container"></div>
								</div>
							</div>
						</div>
						<div class="cpan3 clearfix">

						</div>
					</div>
					<div class="clearfix">
						<div class="cpan3 clearfix">
							<div class="control-group">
								<div class="control-label"><?php echo $this->form->getLabel('child_identity_type'); ?></div>
								<div class="controls"><?php echo $this->form->getInput('child_identity_type'); ?>
									<div class="error-container"></div>
								</div>
							</div>
						</div>
						<div class="cpan3 clearfix">
							<div class="control-group">
								<div class="control-label"><?php echo $this->form->getLabel('child_identity_no'); ?></div>
								<div class="controls"><?php echo $this->form->getInput('child_identity_no'); ?>
									<div class="error-container"></div>
								</div>
							</div>
						</div>
						<div class="cpan3 clearfix">
							<div class="control-group">
								<div class="control-label"><?php echo $this->form->getLabel('child_id_expiry_date'); ?></div>
								<div class="controls"><?php echo $this->form->getInput('child_id_expiry_date'); ?>
									<div class="error-container"></div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="insurediy-contact-detail-insurance-subheader">
					<div class="insurediy-contact-detail-subtext"><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_FIELD_EXISTING_INSURANCE_TITLE"); ?></div>
					<a href="javascript:toggleThis('toggleExistingInsurance','EIToggleBtn')"><div id="EIToggleBtn" class="insurediy-contact-detail-btn-collapse">&nbsp;</div></a>
					<div class="clear"></div>
				</div>
				<div id="toggleExistingInsurance">
					<div id="ajax-load-existing-insurance-policy">
						<?php if (empty($exInsurances)): ?>
							<div id="existing-policy-insurance-contact-detail" class="epi-contact-detail-wrapper">
								<div>
									<div class="control-group-contact-detail-insurer-name">
										<div class="control-label"><?php echo $form->getLabel("insurer_name"); ?></div>
										<div class="controls"><?php echo $form->getInput("insurer_name"); ?></div>
									</div>
									<div class="control-group-contact-detail-policy-type">
										<div class="control-label"><?php echo $form->getLabel("policy_type"); ?></div>
										<div class="controls" style="width:100%;"><?php echo $form->getInput("policy_type"); ?></div>
									</div>
									<div class="clear"></div>
								</div>
								<div>
									<div class="control-group-contact-detail-sum-insured">
										<div class="control-label"><?php echo $form->getLabel("sum_insured"); ?></div>
										<div class="controls" style="position:relative;">
											<div class="hkd-word"><?php echo $currency; ?></div>
											<?php echo $form->getInput("sum_insured"); ?>
										</div>
									</div>
									<div class="control-group-contact-detail-issuance-date">
										<div class="control-label"><?php echo $form->getLabel("issuance_date"); ?></div>
										<div class="controls"><?php echo $form->getInput("issuance_date"); ?></div>
									</div>
									<div class="clear"></div>
								</div>
								<div class="clear"></div>
							</div>
						<?php else: ?>
							<?php
							foreach ($exInsurances as $k => $exInsurance):
								$form->setValue("insurer_name", "", $exInsurance->insurer_name);
								$form->setValue("policy_type", "", $exInsurance->policy_type);
								$form->setValue("sum_insured", "", $exInsurance->sum_insured);
								$form->setValue("issuance_date", "", $exInsurance->issuance_date);
								if ($k) {
									$row = $k + 1;
									$form->setFieldAttribute("insurer_name", "id", "insurer_name_" . $row);
									$form->setFieldAttribute("policy_type", "id", "policy_type_" . $row);
									$form->setFieldAttribute("sum_insured", "id", "sum_insured_" . $row);
									$form->setFieldAttribute("issuance_date", "id", "issuance_date_" . $row);
								}
								?>
								<div id="existing-policy-insurance-contact-detail<?php echo ($k) ? "_" . $k : ""; ?>" class="epi-contact-detail-wrapper">
									<?php if ($k): ?>
										<button type="button" class="close" aria-hidden="true" onclick="javascript:removeExInsurace('#existing-policy-insurance-contact-detail<?php echo ($k) ? "_" . $k : ""; ?>');">×</button>
									<?php endif; ?>
									<div>
										<div class="control-group-contact-detail-insurer-name">
											<div class="control-label">
												<?php echo $form->getLabel("insurer_name"); ?>
											</div>
											<div class="controls"><?php echo $form->getInput("insurer_name"); ?></div>
										</div>
										<div class="control-group-contact-detail-policy-type">
											<div class="control-label"><?php echo $form->getLabel("policy_type"); ?></div>
											<div class="controls" style="width:100%;"><?php echo $form->getInput("policy_type"); ?></div>
										</div>
										<div class="clear"></div>
									</div>
									<div>
										<div class="control-group-contact-detail-sum-insured">
											<div class="control-label"><?php echo $form->getLabel("sum_insured"); ?></div>
											<div class="controls" style="position:relative;">
												<div class="hkd-word"><?php echo $currency; ?></div>
												<?php echo $form->getInput("sum_insured"); ?>
											</div>
										</div>
										<div class="control-group-contact-detail-issuance-date">
											<div class="control-label"><?php echo $form->getLabel("issuance_date"); ?></div>
											<div class="controls"><?php echo $form->getInput("issuance_date"); ?></div>
										</div>
										<div class="clear"></div>
									</div>
									<div class="clear"></div>
								</div>
							<?php endforeach; ?>
						<?php endif; ?>
					</div>

					<div class="insurediy-contact-detail-add-another-policy">
						<div class="title-text"><a href="javascript:duplicateRow('existing-policy-insurance-contact-detail')"><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_ADD_ANOTHER_POLICY"); ?></a></div>
					</div>
				</div>


				<?php if (isset($this->has_physicians) && $this->has_physicians) : ?>
					<div class="insurediy-contact-detail-physician-subheader">
						<div class="insurediy-contact-detail-subtext"><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_FIELD_PHYSICIAN_DETAILS_TITLE"); ?></div>
						<a href="javascript:toggleThis('togglePhysicianName','PNToggleBtn')"><div id="PNToggleBtn" class="insurediy-contact-detail-btn-collapse">&nbsp;</div></a>
						<div class="clear"></div>
					</div>
					<div id="togglePhysicianName">
						<div>
							<div class="control-group-contact-detail-physician-name">
								<div class="control-label"><?php echo $this->form->getLabel('physician_name'); ?></div>
								<div class="controls"><?php echo $this->form->getInput('physician_name'); ?></div>
							</div>
							<div class="control-group-contact-detail-physician-address">
								<div class="control-label"><?php echo $this->form->getLabel('physician_address'); ?></div>
								<div class="controls"><?php echo $this->form->getInput('physician_address'); ?></div>
							</div>
							<div class="clear"></div>
						</div>
						<div>
							<div class="control-group-contact-detail-physician-result">
								<div class="control-label"><?php echo $this->form->getLabel('physician_consult_result'); ?></div>
								<div class="controls"><?php echo $this->form->getInput('physician_consult_result'); ?></div>
							</div>
							<div class="control-group-contact-detail-physician-reason">
								<div class="control-label"><?php echo $this->form->getLabel('physician_consult_reason'); ?></div>
								<div class="controls"><?php echo $this->form->getInput('physician_consult_reason'); ?></div>
							</div>
							<div class="clear"></div>
						</div>
					</div>
				<?php endif; ?>

				<!-- Existing Insurance Policies here - END -->
				<input type="hidden" name="task" value="form.step4save" />
				<input type="hidden" name="quotation_id" value="<?php echo $quotation['id']; ?>" />
				<?php echo JHtml::_('form.token'); ?>
				<div style="float:left;margin:15px 0;">
					<div>{modulepos insurediy-secured-ssl}</div>
				</div>
				<div class="btn-toolbar" style="float:right">
					<div>
						<button style="margin-right:10px;" type="button" onclick="javascript:document.adminForm.task.value = 'form.back';
								document.adminForm.submit();" class="btn btn-primary validate"><?php echo JText::_('BTN_BACK') ?></button>
						<button type="submit" class="btn btn-primary validate"><?php echo JText::_('JCONTINUE') ?></button>
					</div>
				</div>
				<div class="clear"></div>
			</fieldset>
		</form>
	</div>
</div>
<div class="logo-slider"><?php echo MyHelper::load_module(102); ?></div>

<?php /* Popup Box */ ?>
<div class="insurediy-popup" id="insurediy-popup-box" style="display:none;">
	<div class="header">
		<div class="text-header"><?php echo JText::_("POP_IMPORTANT_NOTICE"); ?></div>
		<a class="close" href="javascript:cleanInsureDIYPopup('insurediy-popup-box');">&nbsp;</a>
		<div class="clear"></div>
	</div>
	<div class="padding">
		<div><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_NAME_CHANGE_NOTICE_MSG"); ?></div>
		<div style="text-align:right;"><input type="button" class="btn btn-primary" style="height:30px;padding:5px 20px;font-size:14px !important;" onclick="javascript:cleanInsureDIYPopup('insurediy-popup-box');
				return false;" value="<?php echo JText::_("JOK"); ?>" /></div>
	</div>
</div>


