<?php
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', '.insurediy-form-content select');

$quotation = $this->quotation;

$file_identity_card = explode("|", $quotation['file_identity_card']);
$fname_id_card = (isset($file_identity_card[1])) ? $file_identity_card[1] : "";

$file_proof_address = explode("|", $quotation['file_proof_address']);
$fname_address = (isset($file_proof_address[1])) ? $file_proof_address[1] : "";

$accept = "image/gif, image/jpeg, image/png, application/pdf, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/msword";
?>

<script type="text/javascript">
	window.addEvent('domready', function () {
		var myAccordion = new Fx.Accordion($$('.togglers'), $$('.step2-checkbox'), {display: -1, alwaysHide: true});
	});

	function clickFileType(div_id) {
		$(div_id).click();
	}

	function copyPasteText(div_id, text, this_id) {
		var jThis = jQuery(this_id);
		if (text.length > 0) {
			var filename = text.replace(/^.*[\\\/]/, ''); // remove the path since we can't get the actual path
			var extensions = ["gif", "jpeg", "jpg", "png", "pdf", "doc", "docx"];
			var extension = filename.substr((filename.lastIndexOf('.') + 1));
			var isValid = (extensions.indexOf(extension) !== -1);
			if (isValid) {
				$(div_id).setProperty('value', filename);
			} else {
				jThis.val("");
				$("insurediy-popup-box-3").setStyle('display', 'block');
			}
		} else {
			$(div_id).setProperty('value', "");
		}
	}

	function save6thStep() {
		var complete_1 = false;
		var complete_2 = false;
		var meet_type = 0;

		var file_identity_card = '<?php echo (($quotation['file_identity_card']) ? true : false) ?>';
		var file_proof_address = '<?php echo (($quotation['file_proof_address']) ? true : false) ?>';

		if (file_identity_card == '1' || file_proof_address == '1') {
			complete_1 = true;
		}

		$$('#step-2-book-appointment input[type="radio"]:checked').each(function (elem) {
			meet_type = elem.value; // 1 or 2
			complete_2 = true;
		});

		/* check the time is between 9am to 8pm */
		if (meet_type == 2) { // book appointment
			var timeslot1 = $('timeslot1').getProperty('value'); // hour
			var timeslot2 = $('timeslot2').getProperty('value'); // min
			var timeslot3 = $('timeslot3').getProperty('value'); // AM or PM

			var x = new Date("01/01/2013 9:00 AM");
			var y = new Date("01/01/2013 9:00 PM");
			x.setHours('9', '00');
			y.setHours('21', '00')
			var checkTime = new Date("01/01/2013 " + timeslot1 + ":" + timeslot2 + " " + timeslot3);

			if (x <= checkTime && checkTime <= y) {
				// in between of x and y
				complete_2 = true;
			} else {
				// outside of x and y time range
				complete_2 = false;
				$("insurediy-popup-box-2").setStyle('display', 'block');
//				alert('Your appointment time must between 9am to 9pm. ');
				return false;
			}
		}

		if (complete_2) {
			$('task').setProperty('value', 'form.step7save');
			document.step2FinalForm.submit();
		} else {
			$('insurediy-popup-box').setStyle('display', 'block');
		}

	}

	function fileupload() {
		$('task').setProperty('value', 'form.saveFileUpload');
		document.step2FinalForm.submit();
	}

	function filedelete(type) {
		$('task').setProperty('value', 'form.deleteDocuments');
		$('file_type').setProperty('value', type);
		document.step2FinalForm.submit();
	}
</script>

<div class="insurediy-form">
	<div class="header-top-wrapper">
		<?php echo InsureDIYHospitalChildHelper::renderHeader('icon-insurediy-final-steps', JText::_('COM_INSUREDIYHOSPITALCHILD_PAGE_HEADING_THANK_YOU_PAGE'), 4); ?>
	</div>
	<div class="insurediy-form-content">
		<?php echo MyHelper::renderDefaultMessage(); ?>
		<div class="edit<?php echo $this->pageclass_sfx; ?>">
			<form action="<?php echo JRoute::_('index.php?option=com_insurediyhospitalchild'); ?>" method="post" name="step2FinalForm" id="step2FinalForm" class="form-validate form-vertical" enctype="multipart/form-data">
				<div class="whitebg radius-top border-all marginbottom20">
					<div class="radius-top darkbluebg">
						<div class="padding1520 color99ecff size15" style="font-weight:bold;">
							<?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_UNIQUE_ORDER_NUMBER"); ?> : <?php echo $quotation['unique_order_no']; ?>
						</div>
						<div class="header2bg radius-top">
							<div class="ico-identity-card">
								<?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_PAGE_SUB_HEADING_FINAL_PAGE_STEP_1"); ?>
							</div>
						</div>
					</div>
					<div class="padding20">
						<div class="size14 marginbottom20"><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_EXPLANATION_PHOTO_UPLOAD"); ?></div>
						<div class="padding20 lightbluebg marginbottom20 size14" style="border:1px solid #c0dce7;">
							<div style="font-size: 14px;padding-bottom: 10px;">
								<?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_FILES_ACCEPTED"); ?>
							</div>
							<table>
								<tr>
									<td width="220px">
										<strong><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_FIELD_ID_PASSPORT_LABEL"); ?></strong>
									</td>
									<td>
										<input type="text" id="inputr_1" readonly="readonly" onclick="javascript:clickFileType('file_1');
												return false;" style="margin:0;width:330px;background-color:#f0f0f0;" value="<?php echo $fname_id_card ?>" />&nbsp;
										<input type="button" class="btn btn-primary validate" onclick="javascript:clickFileType('file_1');
												return false;" style="font-size:14px !important;height:40px;padding:10px 30px;font-family:sourcesansprobold;color:#416106" value="<?php echo JText::_('JBROWSE') ?>" />
										<input type="file" name="jform[file_identity_card]" accept="<?php echo $accept; ?>" id="file_1" onchange="javascript:copyPasteText('inputr_1', this.value, '#file_1');" style="display:none;" />
										<?php if ($quotation['file_identity_card']) : ?>
											<input type="button" class="btn btn-primary validate" onclick="javascript:filedelete('file_identity_card');" style="font-size:14px !important;height:40px;padding:10px 30px;font-family:sourcesansprobold;color:#416106" value="<?php echo JText::_('JDELETE') ?>" />
										<?php endif; ?>
									</td>
								</tr>
								<?php if ($quotation['file_identity_card']) : ?>
									<tr>
										<td>&nbsp;</td>
										<td><span style="font-size:12px;"><?php echo $fname_id_card ?> <?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_UPLOADED"); ?></span></td>
									</tr>
								<?php endif; ?>
								<tr><td width="220px">&nbsp;</td><td></td></tr>
								<tr>
									<td>
										<strong><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_FIELD_PROOF_ADDRESS_LABEL"); ?></strong> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_FIELD_PROOF_ADDRESS_DESC"); ?>">
									</td>
									<td>
										<input type="text" id="inputr_3" readonly="readonly" onclick="javascript:clickFileType('file_3');
												return false;" style="margin:0;width:330px;background-color:#f0f0f0;" value="<?php echo $fname_address ?>" />&nbsp;
										<input type="button" class="btn btn-primary validate" name="address_proof" onclick="javascript:clickFileType('file_3');
												return false;" style="font-size:14px !important;height:40px;padding:10px 30px;font-family:sourcesansprobold;color:#416106;" value="<?php echo JText::_('JBROWSE') ?>" >
										<input type="file" name="jform[file_proof_address]" accept="<?php echo $accept; ?>" id="file_3" onchange="javascript:copyPasteText('inputr_3', this.value, '#file_3');" style="display:none;" />
										<?php if ($quotation['file_proof_address']) : ?>
											<input type="button" class="btn btn-primary validate" onclick="javascript:filedelete('file_proof_address');" style="font-size:14px !important;height:40px;padding:10px 30px;font-family:sourcesansprobold;color:#416106" value="<?php echo JText::_('JDELETE') ?>" />
										<?php endif; ?>
									</td>
								</tr>
								<?php if ($quotation['file_proof_address']) : ?>
									<tr>
										<td>&nbsp;</td>
										<td><span style="font-size:12px;"><?php echo $fname_address ?> <?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_UPLOADED"); ?></span></td>
									</tr>
								<?php endif; ?>
							</table>
						</div>
						<div class="btn-toolbar" style="float:right">
							<div class="btn-group">
								<button type="button" class="btn btn-primary validate" onclick="javascript:fileupload();" style="font-size:14px !important;height:40px;padding:10px 30px"><?php echo JText::_('JUPLOAD') ?></button>
							</div>
						</div>
						<div class="clear"></div>
					</div>
				</div>
				<div id="step-2-book-appointment" class="whitebg radius-top border-all marginbottom20">
					<div class="header2bg">
						<div class="ico-location-gps">
							<?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_PAGE_SUB_HEADING_FINAL_PAGE_STEP_2"); ?>
						</div>
					</div>
					<div class="padding20 size14">
						<div class="marginbottom20"><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_PHYSICAL_SIGNATURES_TEXT"); ?></div>
						<div class="lightbluebg padding10" style="border:1px solid #c0dce7;">
							<input type="radio" value="1" name="jform[meet_type]" class="togglers" id="meeting_type_1" />&nbsp;&nbsp;&nbsp;<label for="meeting_type_1"><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_MEETING_TYPE_1_LABEL"); ?></label>
						</div>
						<div id="visit-14-locations" class="step2-checkbox" style="border-left:1px solid #c0dce7;border-right:1px solid #c0dce7;">
							<div class="padding2030">
								<table class="size13">
									<tr>
										<td width="110px"><strong><?php echo $this->form->getLabel("pre_location_id"); ?></strong></td>
										<td>
											<?php echo $this->form->getInput("pre_location_id"); ?>
										</td>
									</tr>
									<tr>
										<td></td>
										<td>
											<div style="margin-top:10px;font-style:italic;">
												<?php echo JText::sprintf("COM_INSUREDIYHOSPITALCHILD_EXPLANATION_FORM_READY", $quotation['sign_start'], $quotation['sign_end']); ?>
											</div>
										</td>
									</tr>
								</table>
								<div class="btn-toolbar" style="float:right">
									<div class="btn-group">
										<button type="button" onclick="javascript:save6thStep();
												return false;" class="btn btn-primary validate" style="font-size:14px !important;height:40px;padding:10px 30px"><?php echo JText::_('JSUBMIT') ?></button>
									</div>
								</div>
								<div class="clear"></div>
							</div>

						</div>

						<div class="lightbluebg padding10 relative" style="border:1px solid #c0dce7;">
							<a href="#" onclick="return false;" class="btn btn-primary or-btn" style="width: 32px; left: 50%; top: -15px; position: absolute;"><?php echo JText::_("BTN_OR"); ?></a>
							<input type="radio" value="2" name="jform[meet_type]" class="togglers" id="meeting_type_2" />&nbsp;&nbsp;&nbsp;<label for="meeting_type_2"><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_MEETING_TYPE_2_LABEL"); ?></label>
						</div>
						<div id="book-appointment" class="step2-checkbox" >

							<div class="padding2030" style="border:1px solid #c0dce7;">
								<div class="marginbottom20">
									<div style="float:left;width:50%;">
										<div style="float:left;margin:7px 0;"><?php echo $this->form->getLabel('meet_date'); ?></div>
										<div style="float:left;margin-left:20px;"><?php echo $this->form->getInput('meet_date'); ?></div>
										<div class="clear"></div>
									</div>
									<div style="float:left;width:50%;">
										<div style="float:left;margin:7px 0;"><label><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_TIME"); ?></label></div>
										<div style="float:left;margin-left:20px;">
											<select name="jform[time1]" id="timeslot1" class="time_selection">
												<option value="1">1</option>
												<option value="2">2</option>
												<option value="3">3</option>
												<option value="4">4</option>
												<option value="5">5</option>
												<option value="6">6</option>
												<option value="7">7</option>
												<option value="8">8</option>
												<option value="9">9</option>
												<option value="10">10</option>
												<option value="11">11</option>
												<option value="12">12</option>
											</select>
										</div>
										<div style="float:left;margin-left:10px;" >
											<select name="jform[time2]" id="timeslot2" class="time_selection">
												<option value="00">00</option>
												<option value="15">15</option>
												<option value="30">30</option>
												<option value="45">45</option>
											</select>
										</div>
										<div style="float:left;margin-left:10px;" >
											<select name="jform[time3]" id="timeslot3" class="time_selection">
												<option value="AM">AM</option>
												<option value="PM" selected="selected">PM</option>
											</select>
										</div>
										<div class="clear"></div>
									</div>
									<div class="clear"></div>
								</div>

								<div class="marginbottom20">
									<div class="marginbottom10"><label><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_ADDRESS"); ?></label></div>
									<div><textarea name="jform[meet_address]" style="color: #0c85b2;"><?php echo $quotation['complete_address'] ?></textarea></div>
								</div>
								<div class="marginbottom20">
									<div class="marginbottom10"><label><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_CONTACT_NUMBER"); ?></label></div>
									<div><input type="text" name="jform[country_code]" style="width:40px;text-align:center;" value="<?php echo $quotation['contact_country_code'] ?>" />&nbsp;&nbsp;&nbsp;<input type="text" name="jform[phone_number]" value="<?php echo $quotation['contact_contact_no'] ?>" /></div>
								</div>

								<div class="size13 marginbottom20"><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_EXPLANATION_BRING_ID"); ?></div>
								<div class="btn-toolbar" style="float:right">
									<div class="btn-group">
										<button type="button" onclick="javascript:save6thStep();
												return false;" class="btn btn-primary validate" style="font-size:14px !important;height:40px;padding:10px 30px"><?php echo JText::_('JSUBMIT') ?></button>
									</div>
								</div>
								<div class="clear"></div>
							</div>
						</div>
					</div>
				</div>
				<input type="hidden" name="task" id="task" value="form.step7save" />
				<input type="hidden" name="file_type" id="file_type" value="" />

				<?php echo JHtml::_('form.token'); ?>
				<div style="float:left;margin:15px 0;">
					{modulepos insurediy-secured-ssl}
				</div>
				<div class="clear"></div>
			</form>
		</div>
	</div>
</div>

<?php /* Popup Box */ ?>
<div class="insurediy-popup" id="insurediy-popup-box" style="display:none;">
	<div class="header">
		<div class="ico-warning">
			<div class="text-header2"><?php echo JText::_("POP_IMPORTANT_NOTICE"); ?></div>
		</div>
		<a class="close" href="javascript:cleanInsureDIYPopup('insurediy-popup-box');">&nbsp;</a>
		<div class="clear"></div>
	</div>
	<div class="padding">
		<div class="marginbottom20"><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_APPOITMENT_STEPS_POP_UP_TEXT"); ?></div>
		<div style="text-align:right;"><input type="button" class="btn btn-primary" style="height:30px;padding:5px 20px;font-size:14px !important;" onclick="javascript:cleanInsureDIYPopup('insurediy-popup-box');
				return false;" value="<?php echo JText::_("JOK") ?>" /></div>
	</div>
</div>
<div class="logo-slider"><?php echo MyHelper::load_module(102); ?></div>

<?php /* Popup Box  2 */ ?>
<div class="insurediy-popup" id="insurediy-popup-box-2" style="display:none;">
	<div class="header">
		<div class="ico-warning">
			<div class="text-header2"><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_POP_UP_HEADER_WARNING"); ?></div>
		</div>
		<a class="close" href="javascript:cleanInsureDIYPopup('insurediy-popup-box-2');">&nbsp;</a>
		<div class="clear"></div>
	</div>
	<div class="padding">
		<div class="marginbottom20"><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_APPOITMENT_TIME_POP_UP_TEXT"); ?></div>
		<div style="text-align:right;"><input type="button" class="btn btn-primary" style="height:30px;padding:5px 20px;font-size:14px !important;" onclick="javascript:cleanInsureDIYPopup('insurediy-popup-box-2');
				return false;" value="<?php echo JText::_("JOK") ?>" /></div>
	</div>
</div>


<?php /* Popup Box  3 */ ?>
<div class="insurediy-popup" id="insurediy-popup-box-3" style="display:none;">
	<div class="header">
		<div class="ico-warning">
			<div class="text-header2"><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_POP_UP_HEADER_WARNING"); ?></div>
		</div>
		<a class="close" href="javascript:cleanInsureDIYPopup('insurediy-popup-box-3');">&nbsp;</a>
		<div class="clear"></div>
	</div>
	<div class="padding">
		<div class="marginbottom20"><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_APPOITMENT_INVALID_FILE_TYPE_POP_UP_TEXT"); ?></div>
		<div style="text-align:right;"><input type="button" class="btn btn-primary" style="height:30px;padding:5px 20px;font-size:14px !important;" onclick="javascript:cleanInsureDIYPopup('insurediy-popup-box-3');
				return false;" value="<?php echo JText::_("JOK") ?>" /></div>
	</div>
</div>

<!-- Google Code for InsureDIY Hong Kong Conversion Pixel Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 958440587;
var google_conversion_language = "en";
var google_conversion_format = "3";
var google_conversion_color = "ffffff";
var google_conversion_label = "onKfCKP7rm8Qi8mCyQM";
var google_conversion_value = "<?php echo $this->quotation['selected_plan']->premium; ?>";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/958440587/?value=<?php echo $this->quotation['selected_plan']->premium; ?>&amp;currency_code=HKD&amp;label=onKfCKP7rm8Qi8mCyQM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>