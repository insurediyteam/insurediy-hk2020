<?php
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', '.insurediy-form-content select');

//JHtml::_('behavior.modal', 'a.modal_jform_contenthistory');
// Create shortcut to parameters.
$params = $this->state->get('params');
?>

<div class="insurediy-form">
	<div class="header-top-wrapper">
		<div class="header-top">
			<div class="h1-heading"><i class="icon-mychild"></i><?php echo JText::_("ERR_PAGE_HEADING"); ?></div>
		</div>
	</div>
	<div style="padding:30px;margin-top: 100px;">
		<?php echo MyHelper::renderDefaultMessage(); ?>
		<div class="edit<?php echo $this->pageclass_sfx; ?>">
			<div class="form-quote-box" style="width: 670px">
				<?php if (count($this->error)): ?>
					<?php foreach ($this->error as $error): ?>
						<div class="center" style="color:#2f2f2f;font-size:14px;margin-top: 15px;">
							<?php echo $error; ?>
						</div>
					<?php endforeach; ?>
				<?php else: ?>
					<div class="center" style="color:#2f2f2f;font-size:14px;margin-top: 15px;">
						<?php echo JText::_("ERR_UNKNOWN"); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>

	</div>
</div>
<div class="logo-slider"><?php echo MyHelper::load_module(102); ?></div>
