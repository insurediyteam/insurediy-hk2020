<?php
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', '.insurediy-form-content select');

// Create shortcut to parameters.
$params = $this->state->get('params');

$return = "";
?>

<script type="text/javascript">
	Joomla.submitbutton = function (task)
	{
		if (document.formvalidator.isValid(document.getElementById('adminForm')))
		{
			Joomla.submitform(task);
		}
	}
	window.addEvent("domready", function () {

	});

</script>

<div class="insurediy-form">
	<div class="header-top-wrapper">
		<?php echo InsureDIYHospitalChildHelper::renderHeader('icon-mychild', JText::_('COM_INSUREDIYHOSPITALCHILD_PAGE_HEADING_ABOUT_YOUR_TRIP'), 2); ?>
	</div>
	<div class="edit<?php echo $this->pageclass_sfx; ?> insurediy-form-content" style="margin-top: 60px;">
		<?php echo MyHelper::renderDefaultMessage(); ?>
		<div style="font-weight:bold;font-size:16px;">
			<?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_LOG_IN_TO_PROCEED"); ?>
		</div>
		<fieldset class="insurediy-life-log-in">
			<form action="<?php echo JRoute::_('index.php', true); ?>" method="post" id="login-form" class="form-inline form-validate">
				<h2><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_LOG_IN_LOGIN"); ?></h2>
				<div class="userdata">
					<div style="height: 130px;">
						<div id="form-login-username" class="control-group">
							<div class="controls">
								<div class="input-prepend">
									<input id="modlgn-username" type="text" name="username" class="input-small required validate-username" tabindex="0" size="18" placeholder="<?php echo JText::_('JGLOBAL_EMAIL') ?>" />
								</div>
							</div>
						</div>
						<div id="form-login-password" class="control-group">
							<div class="controls">
								<div class="input-prepend">
									<input id="modlgn-passwd" type="password" name="password" class="input-small required" tabindex="0" size="18" placeholder="<?php echo JText::_('JGLOBAL_PASSWORD') ?>" />
								</div>
							</div>
						</div>
						<a href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>" style="margin: 0 5px;" target="_blank" ><?php echo JText::_('FORGOT_PASSWORD') ?></a>
					</div>
					<div style="margin-top:29px;">
						<div id="form-login-submit" class="control-group">
							<div class="controls">
								<button type="submit" tabindex="0" name="Submit" class="btn btn-primary" style="padding:5px 20px;font-size:14px !important;height:30px;"><?php echo JText::_('JSUBMIT') ?></button>
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<input type="hidden" name="option" value="com_insurediyhospitalchild" />
					<input type="hidden" name="task" value="user.login" />
					<input type="hidden" name="return" value="<?php echo $return; ?>" />
					<?php echo JHtml::_('form.token'); ?>
				</div>
			</form>
		</fieldset>
		<div style="width:5%;float: left">
			<div class="mid-line"></div><div style="text-align: center;"><a href="#" onclick="return false;" class="btn btn-primary or-btn"><?php echo JText::_("BTN_OR"); ?></a></div><div class="mid-line"></div>
		</div>
		<fieldset class="insurediy-life-register">
			<form action="<?php echo JRoute::_('index.php', true); ?>" method="post" id="login-form" class="form-inline form-validate">
				<h2><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_LOG_IN_REGISTER"); ?></h2>
				<div class="userdata">
					<div style="height: 130px;">
						<div id="form-login-username" class="control-group">
							<div class="controls">
								<div class="input-prepend">
									<input id="email" type="text" name="email" class="input-small required validate-email" tabindex="0" size="18" placeholder="<?php echo JText::_('JGLOBAL_EMAIL') ?>" />
								</div>
							</div>
						</div>
						<div id="form-login-password" class="control-group">
							<div class="controls">
								<div class="input-prepend">
									<input id="password" type="password" name="password" class="input-small required validate-password" tabindex="0" size="18" placeholder="<?php echo JText::_('JGLOBAL_PASSWORD') ?>" />
								</div>
							</div>
						</div>
						<div id="form-login-password" class="control-group">
							<div class="controls">
								<div class="input-prepend">
									<input id="password2" type="password" name="password2" class="input-small required validate-password" tabindex="0" size="18" placeholder="Confirm <?php echo JText::_('JGLOBAL_PASSWORD') ?>" />
								</div>
							</div>
						</div>
					</div>
					<div style="margin-top:29px;">
						<div id="form-login-submit" class="control-group">
							<div class="controls">
								<button type="submit" tabindex="0" name="Submit" class="btn btn-primary" style="padding:5px 20px;font-size:14px !important;height:30px;"><?php echo JText::_('JSUBMIT') ?></button>
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<?php $usersConfig = JComponentHelper::getParams('com_users'); ?>

					<input type="hidden" name="option" value="com_insurediyhospitalchild" />
					<input type="hidden" name="task" value="user.register" />
					<input type="hidden" name="return" value="<?php echo $return; ?>" />
					<?php echo JHtml::_('form.token'); ?>
				</div>
			</form>
		</fieldset>
		<div class="clear"></div>
		<div style="float:left;margin:15px 0 0">
				{modulepos insurediy-secured-ssl}
			</div>
		</form>
	</div>
	<div class="clear"><br></div>
	<div style="text-align:center;width:100%;background-color:#ffffff;padding-top:15px;">
			<span style="font-size:15px;font-weight:bold;"><?php echo JText::_('INSUREDIY_AWARDS') ?></span>
		</div>
	<div id="awards">
		<img src="<?php echo JText::_('CORPHUB_LOGO') ?>">
		<img src="<?php echo JText::_('WPH_LOGO') ?>">
	</div>
</div>
<div class="logo-slider"><?php echo MyHelper::load_module(102); ?></div>

<?php /* Popup Box */ ?>
<div class="insurediy-popup" id="insurediy-popup-box" style="display:none;">
	<div class="header">
		<div class="text-header"><?php echo JText::_("POP_IMPORTANT_NOTICE"); ?></div>
		<a class="close" href="javascript:cleanInsureDIYPopup('insurediy-popup-box');">&nbsp;</a>
		<div class="clear"></div>
	</div>
	<div class="padding">
		<div><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_MEDICAL_ISSUE_MSG"); ?></div>
		<div style="text-align:right;"><input type="button" class="btn btn-primary" style="height:30px;padding:5px 20px;font-size:14px !important;" onclick="javascript:cleanInsureDIYPopup('insurediy-popup-box');
				return false;" value="Ok" /></div>
	</div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function () {

	});
</script>
