<?php
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', '.insurediy-life-cover-options select');
JHtml::_('bootstrap.tooltip');
$session = JFactory::getSession();
$step1CriteriaPass = $this->step1CriteriaPass;
$ask4override = $this->ask4override;
$session->set("hospitalchild.step1.ask4override", NULL);
$quotation = $this->quotation;
$answers = $quotation['answers'];
?>

<script type="text/javascript">
	function checkYesOnGenderForm() {
		var yes_answer = 0;
		$$('input[type="radio"].gender_has_criteria:checked').each(function (elem) {
			if (elem.value == 1) {
				yes_answer = 1;
			}
		});
		var step1criteria = <?php echo ($step1CriteriaPass) ? 1 : 0; ?>;
		if (yes_answer == 1 || step1criteria == 0) {
			$('insurediy-popup-box').setStyle('display', 'block');
		} else {
			document.questionnairesForm.submit();
		}
	}

	function continueWithError() {
		document.questionnairesForm.submit();
	}
	function toHomeWithError() {
		document.questionnairesForm.continueWithError.value = "0";
		document.questionnairesForm.submit();
	}
</script>

<div class="insurediy-form">
	<div class="header-top-wrapper">
		<?php echo InsureDIYHospitalChildHelper::renderHeader('icon-mychild', JText::_('COM_INSUREDIYHOSPITALCHILD_PAGE_HEADING_YOUR_HOSPITAL_COVER'), 1); ?>
	</div>

	<div style="padding:30px;margin-top: 60px;">
		<?php echo MyHelper::renderDefaultMessage(); ?>

		<div class="edit<?php echo $this->pageclass_sfx; ?>">
			<form action="<?php echo JRoute::_('index.php?option=com_insurediyhospitalchild'); ?>" method="post" name="questionnairesForm" id="questionnairesForm" class="form-validate form-vertical">
				<div class="header2bg">
					<div class="ico-question-mark">
						<?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_QUESTIONS_SUB_HEADER_MSG"); ?>
					</div>
				</div>
				<div class="insurediy-life-detail-questions-forms">
					<table>
						<?php
						foreach ($this->questionnaires as $key => $question) :
							$ans = isset($answers[$question->id]) ? $answers[$question->id]->answer : FALSE;
							?>
							<tr>
								<td class="column1"><?php echo $key + 1 ?>.</td>
								<td class="column2">
									<div>
										<strong><?php echo $question->questionnaire ?></strong>
										<?php if (!empty($question->help_quote)) : ?>
											<span class="help-quote hasTooltip" title="<?php echo $question->help_quote ?>">&nbsp;</span>
										<?php endif; ?>
									</div>
									<span class="description"><?php echo $question->description ?></span>
									<input type="hidden" name="jform[question_id][<?php echo $key ?>]" value="<?php echo $question->id ?>" />
								</td>
								<td class="column3">
									<input type="radio" value="1" name="jform[answer][<?php echo $key ?>]" class="styled <?php echo ($question->is_criteria) ? 'gender_has_criteria' : '' ?>" <?php echo ($ans) ? 'checked="checked"' : ""; ?> /><div style="float:left;margin-right:10px;"><?php echo JText::_('JYES') ?></div>
									<input type="radio" value="0" name="jform[answer][<?php echo $key ?>]" class="styled" <?php echo ($ans) ? "" : 'checked="checked"' ?>  /><div style="float:left;"><?php echo JText::_('JNO') ?></div>
									<div class="clear"></div>
								</td>
							</tr>
						<?php endforeach; ?>
					</table>
				</div>
				<input type="hidden" id="continueWithError" name="continueWithError" value="1" />
				<input type="hidden" name="task" value="form.step2save" />
				<input type="hidden" name="quotation_id" value="<?php echo $quotation['id']; ?>" />

				<?php echo JHtml::_('form.token'); ?>
				<div style="float:left;margin:15px 0;">
					{modulepos insurediy-secured-ssl}
				</div>
				<div class="btn-toolbar" style="float:right">
					<div>
						<button style="margin-right:10px;" type="button" onclick="javascript:document.questionnairesForm.task.value = 'form.back';
								document.questionnairesForm.submit();" class="btn btn-primary validate"><?php echo JText::_('BTN_BACK') ?>
						</button>
						<button type="button" onclick="javascript:checkYesOnGenderForm();
								return false;" class="btn btn-primary validate"><?php echo JText::_('JGET_QUOTES') ?></button>
					</div>
				</div>
				<div class="clear"></div>
			</form>
		</div>

	</div>
</div>
<div class="logo-slider"><?php echo MyHelper::load_module(102); ?></div>

<?php /* Popup Box */ ?>
<div class="insurediy-popup" id="insurediy-popup-box" style="display:none;">
	<div class="header">
		<div class="text-header"><?php echo JText::_("POP_IMPORTANT_NOTICE"); ?></div>
		<a class="close" href="javascript:cleanInsureDIYPopup('insurediy-popup-box');">&nbsp;</a>
		<div class="clear"></div>
	</div>
	<div class="padding">
		<div><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_MEDICAL_ISSUE_MSG_1"); ?><br>
			<br>
			<?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_MEDICAL_ISSUE_MSG_2"); ?>
		</div>
		<div style="text-align:right;">
			<input type="button" class="btn btn-primary" style="height:30px;padding:5px 20px;font-size:14px !important;" onclick="javascript:continueWithError();
					return false;" value="<?php echo JText::_("JYES"); ?>" />
				   <?php if (FALSE): ?>
				<input type="button" class="btn btn-primary" style="height:30px;padding:5px 20px;font-size:14px !important;" onclick="javascript:toHomeWithError();
							return false;" value="<?php echo JText::_("JNO"); ?>" />
				   <?php endif; ?>
			<input type="button" class="btn btn-primary" style="height:30px;padding:5px 20px;font-size:14px !important;" onclick="javascript:cleanInsureDIYPopup('insurediy-popup-box');" value="<?php echo JText::_("JNO"); ?>" />
		</div>
	</div>
</div>

<?php if (!$ask4override) : ?>
	<?php /* Popup Box - check After login */ ?>
	<div class="insurediy-popup" id="insurediy-popup-box2" style="">
		<div class="header">
			<div class="text-header"><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_ATTENTION"); ?></div>
			<a class="close" href="javascript:cleanInsureDIYPopup('insurediy-popup-box2');">&nbsp;</a>
			<div class="clear"></div>
		</div>
		<div class="padding">
			<div style="margin-bottom:20px;"><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_ATTENTION_MSG"); ?></div>
			<form action="index.php?option=com_insurediyhospitalchild" method="post" name="step2RefreshInfoForm">
				<div style="text-align:right;">
					<input type="submit" class="btn btn-primary" style="height:30px;padding:5px 20px;font-size:14px !important;" value="<?php echo JText::_("JYES"); ?>" />
					<input type="button" class="btn btn-primary" style="height:30px;padding:5px 20px;font-size:14px !important;" onclick="javascript:cleanInsureDIYPopup('insurediy-popup-box2');
								return false;" value="<?php echo JText::_("JNO"); ?>" />
				</div>
				<input type="hidden" name="return" value="<?php echo base64_encode(JURI::getInstance()->toString()); ?>" />
				<input type="hidden" name="task" value="form.step2RefreshInfo" />
				<?php echo JHtml::_('form.token'); ?>
			</form>
		</div>
	</div>
<?php endif; ?>
