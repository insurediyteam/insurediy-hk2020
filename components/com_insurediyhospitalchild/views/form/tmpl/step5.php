<?php
	defined('_JEXEC') or die;
	
	JHtml::_('behavior.keepalive');
	JHtml::_('behavior.formvalidation');
	JHtml::_('formbehavior.chosen', '.insurediy-form select');
	JHtml::_('MyBehavior.jsInsurediy');
	JHtml::_('script', 'system/jquery.validate.js', false, true);
	
	
	$currency = InsureDIYHospitalChildHelper::getCurrency();
	
	$form = & $this->form;
	$quotation = $this->quotation;
	$fldGroup = "contact-details";
	$selected_branch = $this->quotation["bank_branch_no"];
?>

<script type="text/javascript">
	function toggleEdit(div_id, isEdit) {
		//do edit
		if (isEdit === true) {
			$(div_id).toggleClass("readonly", true);
			} else if (isEdit === false) {
			$(div_id).toggleClass("readonly", false);
			} else {
			$(div_id).toggleClass("readonly");
		}
		if (jQuery("#" + div_id).hasClass("readonly")) {
			$$("#" + div_id + " input:not('.ignore-edit')").set("readonly", "readonly");
			//			$$("#" + div_id + " select:not('.ignore-edit')").set("disabled", "disabled");
			jQuery("#" + div_id + " select:not('.ignore-edit')").prop('disabled', true).trigger("liszt:updated");
			jQuery("#" + div_id + " select:not('.ignore-edit')").prop('disabled', false);
			} else {
			$$("#" + div_id + " input:not('.ignore-edit')").set("readonly", "");
			//			$$("#" + div_id + " select:not('.ignore-edit')").set("disabled", "");
			jQuery("#" + div_id + " select:not('.ignore-edit')").prop('disabled', false).trigger("liszt:updated");
		}
		//		$$("#" + div_id + " .chzn-container").toggleClass("chzn-disabled");
	}
	
	function toggleThis(div_id, btn_id) {
		new Fx.Slide(div_id, {resetHeight: true}).toggle();
		//		jQuery("#" + div_id).toggle(100);
		$(btn_id).toggleClass("active");
	}
	
	function nameChanged(showMsg) {
		var fname = jQuery("#jform_contact_firstname");
		var lname = jQuery("#jform_contact_lastname");
		var accName = jQuery("#jform_bank_acct_holder_name");
		accName.val(fname.val() + " " + lname.val());
		if (showMsg) {
			$('insurediy-popup-box').setStyle('display', 'block');
		}
	}
	
	function checkData(div_id) {
		//		var jDiv = jQuery("#" + div_id);
		var firstTime = <?php echo json_encode(($this->current_user->is_firsttime) ? 1 : 0) ?>;
		if (firstTime == "1") {
			return false;
		}
		//		jDiv.find("input").each(function() {
		//			var jThis = jQuery(this);
		//			if (jThis.val().length > 0) {
		//				hasData = true;
		//			}
		//		});
		return true;
	}
	
	function selectOnChange() {
		var jThis = jQuery(this);
		var jControl = jThis.closest("div.controls");
		if (jThis.val()) {
			jControl.find(".error-container label.error").remove();
			} else {
			jControl.find("label.error").each(function () {
				var jThis = jQuery(this);
				var jContainer = jThis.closest("div.control-group");
				var jControl = jContainer.find("div.controls");
				jControl.find(".error-container").append(jThis.clone());
				jThis.remove();
			});
		}
	}
	
	window.addEvent('domready', function () {
		jQuery("#adminForm").validate({
			ignore: [] // <-- option so that hidden elements are validated
		});
		var jForm = jQuery("form#adminForm");
		var jSelects = jForm.find("select");
		var jInputs = jForm.find("input");
		jForm.submit(function () {
			jForm.find("label.error").each(function () {
				var jThis = jQuery(this);
				var jContainer = jThis.closest("div.control-group");
				var jControl = jContainer.find("div.controls");
				jControl.find(".error-container").append(jThis.clone());
				jThis.remove();
			});
			var errors = jForm.find("label.error");
			if (errors.length > 0) {
				var label = jForm.find("label.error:first");
				jQuery('html, body').animate({
					'scrollTop': label.offset().top - 400
				}, 300);
			}
		});
		jSelects.each(function () {
			var jThis = jQuery(this);
			jThis.on("change", selectOnChange);
		});
		jInputs.each(function () {
			var jThis = jQuery(this);
			jThis.on("change", selectOnChange);
		});
		
		var fname = jQuery("#jform_contact_firstname");
		var lname = jQuery("#jform_contact_lastname");
		var hasData = checkData('contact-details-fields');
		
		nameChanged(false);
		fname.on("change", function () {
			nameChanged(hasData);
		});
		lname.on("change", function () {
			nameChanged(hasData);
		});
		if (hasData) {
			toggleEdit('contact-details-fields', true);
			} else {
			jQuery("#edit-btn").hide();
		}
		var idType = jQuery("#jform_contact_identity_type");
		var doe = jQuery("#contact_identity_doe");
		var expirtyDateCal = jQuery("#jform_contact_expiry_date");
		expirtyDateCal.prop("required", '');
		if (idType.val() === "passport") {
			doe.fadeIn();
		}
		idType.on('change', function () {
			var typeValue = idType.val();
			if (typeValue === "passport") {
				doe.fadeIn();
				expirtyDateCal.prop("required", 'required');
				} else {
				doe.fadeOut();
				expirtyDateCal.prop("required", '');
			}
			jQuery("#jform_bank_id_type option[value=" + typeValue + "]").attr("selected", "selected").trigger("liszt:updated");
		});
		jQuery("#jform_contact_identity_no").keyup(function (e) {
			jQuery("#jform_bank_id_no").val(jQuery("#jform_contact_identity_no").val());
		});
		var jBanks = jQuery("#jform_bank_bank_no");
		<?php if ($selected_branch): ?>
		var jSelectedBank = jBanks.find("option:selected");
		if (jSelectedBank.val()) {
			var temp = <?php echo $selected_branch; ?>;
			jsInsurediy.loadBranches(jSelectedBank.val(), "jform_bank_branch_no", {}, temp);
		}
		<?php endif; ?>
		jBanks.on("change", function () {
			var jSelectedBank = jBanks.find("option:selected");
			jsInsurediy.loadBranches(jSelectedBank.val(), "jform_bank_branch_no");
		});
	});
</script>

<div class="insurediy-form bb-form">
	<div class="header-top-wrapper">
		<?php echo InsureDIYHospitalChildHelper::renderHeader('icon-contact-details', JText::_('COM_INSUREDIYHOSPITALCHILD_PAGE_HEADING_CONTACT_DETAILS'), 3); ?>
	</div>
	<div class="insurediy-form-content">
		<?php echo MyHelper::renderDefaultMessage(); ?>
		<form action="<?php echo JRoute::_('index.php?option=com_insurediyhospitalchild&view=form'); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-vertical">
			<fieldset class="insurediy-form-fields">
				<div id="contact-details-fields" class="contact-details-fields">
					<div class="row" style="float: left;width: 39.5%;margin-left: 0;">
						<div class="span12 control-group" style="margin-left: 0;">
							<div class="control-label"><?php echo $this->form->getLabel('contact_firstname'); ?></div>
							<div class="controls">
								<?php echo $this->form->getInput('contact_firstname'); ?>
								<div class="error-container"></div>
							</div>
						</div>
						<div class="span12 control-group" style="margin-left: 0;">
							<div class="control-label"><?php echo $this->form->getLabel('contact_lastname'); ?></div>
							<div class="controls">
								<?php echo $this->form->getInput('contact_lastname'); ?>
								<div class="error-container"></div>
							</div>
						</div>
						<div class="span12 control-group" style="margin-left: 0;">
							<div class="control-label"><?php echo $this->form->getLabel('contact_contact_no'); ?></div>
							<div class="controls">
								<?php echo $this->form->getInput('contact_contact_no'); ?>
								<div class="error-container"></div>
							</div>
						</div>
						<div class="span12" style="margin-left: 0;">
							<div class="control-label"><?php echo $this->form->getLabel('contact_identity_no'); ?></div>
							<div style="width: 30%;margin-right: 10px;float: left;">
								<div class="controls"><?php echo $this->form->getInput('contact_identity_type'); ?></div>
							</div>
							<div style="width: 64%;float: left;">
								<div class="control-group">
									<div class="controls">
										<?php echo $this->form->getInput('contact_identity_no'); ?>
										<div class="error-container"></div>
									</div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
						<div id="contact_identity_doe" class="span12" style="margin-left: 0;display: none;">
							<div class="control-label"><?php echo $this->form->getLabel('contact_expiry_date'); ?></div>
							<div class="controls"><?php echo $this->form->getInput('contact_expiry_date'); ?></div>
						</div>
						
						<div class="clear"></div>
					</div>
					<div style="width: .7%;margin-left: .3%;height: 300px;float: left;border-left: 1px solid #ccc;box-sizing: border-box;"></div>
					<div style="float: left;width: 59%;">
						<div>
							<div class="span4 control-group">
								<div class="control-label"><?php echo $this->form->getLabel('contact_room_no'); ?></div>
								<div class="controls">
									<?php echo $this->form->getInput('contact_room_no'); ?>
									<div class="error-container"></div>
								</div>
							</div>
							<div class="span4 control-group">
								<div class="control-label"><?php echo $this->form->getLabel('contact_floor_no'); ?></div>
								<div class="controls">
									<?php echo $this->form->getInput('contact_floor_no'); ?>
									<div class="error-container"></div>
								</div>
							</div>
							<div class="span4 control-group">
								<div class="control-label"><?php echo $this->form->getLabel('contact_block_no'); ?></div>
								<div class="controls">
									<?php echo $this->form->getInput('contact_block_no'); ?>
									<div class="error-container"></div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
						<div>
							<div class="span6 control-group">
								<div class="control-label"><?php echo $this->form->getLabel('contact_building_name'); ?></div>
								<div class="controls">
									<?php echo $this->form->getInput('contact_building_name'); ?>
									<div class="error-container"></div>
								</div>
							</div>
							<div class="span6 control-group">
								<div class="control-label"><?php echo $this->form->getLabel('contact_street_name'); ?></div>
								<div class="controls">
									<?php echo $this->form->getInput('contact_street_name'); ?>
									<div class="error-container"></div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
						<div>
							<div class="span6 control-group">
								<div class="control-label"><?php echo $this->form->getLabel('contact_district_name'); ?></div>
								<div class="controls">
									<?php echo $this->form->getInput('contact_district_name'); ?>
									<div class="error-container"></div>
								</div>
							</div>
							<div class="span6 control-group">
								<div class="control-label"><?php echo $this->form->getLabel('contact_postalcode'); ?></div>
								<div class="controls">
									<?php echo $this->form->getInput('contact_postalcode'); ?>
									<div class="error-container"></div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
						<div>
							<div class="span6 control-group">
								<div class="control-label"><?php echo $this->form->getLabel('contact_country'); ?></div>
								<div class="controls">
									<?php echo $this->form->getInput('contact_country'); ?>
									<div class="error-container"></div>
								</div>
							</div>
							<div class="span6 control-group">
								<div class="controls" style="padding-top: 30px;text-align: right;">
									<a id="edit-btn" href="javascript:toggleEdit('contact-details-fields')" class="btn btn-primary" style="padding: 5px 20px;"><?php echo JText::_("JACTION_EDIT"); ?></a>
								</div>
							</div>
							<div class="clear"></div>
						</div>
					</div>
					<div class="clear"></div>
				</div>
				
				<?php if ($this->current_user->is_firsttime == 1) : ?>
				<input type="hidden" name="jform[is_firsttime]" value="<?php echo $this->current_user->is_firsttime ?>">
				<input type="hidden" name="jform[gender]" value="<?php echo $this->quotation['gender'] ?>">
				<input type="hidden" name="jform[marital_status]" value="<?php echo $this->quotation['marital_status'] ?>">
				<input type="hidden" name="jform[dob]" value="<?php echo $this->quotation['dob'] ?>">
				<input type="hidden" name="jform[monthly_income]" value="<?php echo $this->quotation['monthly_income'] ?>">
				<input type="hidden" name="jform[occupation]" value="<?php echo $this->quotation['occupation'] ?>">
				<?php endif; ?>
				
				<div class="insurediy-contact-detail-employer-subheader">
					<div class="insurediy-contact-detail-subtext"><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_FIELD_EMPLOYER_DETAILS_TITLE"); ?> (if any)</div>
					<a href="javascript:toggleThis('toggleEmployerName','ENToggleBtn')"><div id="ENToggleBtn" class="insurediy-contact-detail-btn-collapse">&nbsp;</div></a>
					<div class="clear"></div>
				</div>
				
				<div id="toggleEmployerName">
					<div>
						<div class="control-group-contact-detail-employer-name control-group">
							<div class="control-label"><?php echo $this->form->getLabel('employer_name'); ?></div>
							<div class="controls">
								<?php echo $this->form->getInput('employer_name'); ?>
								<div class="error-container"></div>
							</div>
						</div>
						<div class="control-group-contact-detail-employer-address control-group">
							<div class="control-label"><?php echo $this->form->getLabel('employer_address'); ?></div>
							<div class="controls">
								<?php echo $this->form->getInput('employer_address'); ?>
								<div class="error-container"></div>
							</div>
						</div>
						<div class="clear"></div>
					</div>
					
					<div>
						<div class="control-group-contact-detail-employer-job-nature control-group">
							<div class="control-label"><?php echo $this->form->getLabel('employer_job_nature'); ?></div>
							<div class="controls">
								<?php echo $this->form->getInput('employer_job_nature'); ?>
								<div class="error-container"></div>
							</div>
						</div>
						<div class="clear"></div>
						
					</div>
				</div>
				
				<div class="insurediy-contact-detail-bank-subheader">
					<div class="insurediy-contact-detail-subtext"><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_FIELD_BANK_DETAILS_TITLE"); ?></div>
					<a href="javascript:toggleThis('toggleBankDetails','BDToggleBtn')"><div id="BDToggleBtn" class="insurediy-contact-detail-btn-collapse">&nbsp;</div></a>
					<div class="clear"></div>
				</div>
				<div id="toggleBankDetails">
					<div style="margin-bottom:20px;font-size:14px;"><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_TWO_MONTHS_PREMIUM_COLLECTION_NOTICE_MSG"); ?></div>
					<div>
						<div class="control-group-contact-detail-bank-bank-no control-group">
							<div class="control-label"><?php echo $this->form->getLabel('bank_bank_no'); ?></div>
							<div class="controls">
								<?php echo $this->form->getInput('bank_bank_no'); ?>
								<div class="error-container"></div>
							</div>
						</div>
						<div class="control-group-contact-detail-bank-branch-no control-group">
							<div class="control-label"><?php echo $this->form->getLabel('bank_branch_no'); ?></div>
							<div class="controls">
								<?php echo $this->form->getInput('bank_branch_no'); ?>
								<div class="error-container"></div>
							</div>
						</div>
						<div class="control-group-contact-detail-bank-acct-no control-group">
							<div class="control-label"><?php echo $this->form->getLabel('bank_acct_no'); ?></div>
							<div class="controls">
								<?php echo $this->form->getInput('bank_acct_no'); ?>
								<div class="error-container"></div>
							</div>
						</div>
						<div class="clear"></div>
					</div>
					
					<div>
						<div class="control-group-contact-detail-bank-acct-holder-name control-group">
							<div class="control-label"><?php echo $this->form->getLabel('bank_acct_holder_name'); ?></div>
							<?php
								$bhname = $form->getValue("bank_acct_holder_name");
								if (strlen($bhname) < 1) {
									$fname = $form->getValue("contact_firstname");
									$lname = $form->getValue("contact_lastname");
									$form->setValue('bank_acct_holder_name', $fldGroup, $fname . " " . $lname);
								}
							?>
							<div class="controls">
								<?php echo $this->form->getInput('bank_acct_holder_name'); ?>
								<div class="error-container"></div>
							</div>
						</div>
						<div class="control-group-contact-detail-bank-id-type control-group">
							<div class="control-label"><?php echo $this->form->getLabel('bank_id_type'); ?></div>
							<div class="controls">
								<?php echo $this->form->getInput('bank_id_type'); ?>
								<div class="error-container"></div>
							</div>
						</div>
						<div class="control-group-contact-detail-bank-id-no control-group">
							<div class="control-label"><?php echo $this->form->getLabel('bank_id_no'); ?></div>
							<div class="controls">
								<?php echo $this->form->getInput('bank_id_no'); ?>
								<div class="error-container"></div>
							</div>
						</div>
						<div class="clear"></div>
					</div>
				</div>
				
				<!-- Social Stuffs Start -->
				<div class="insurediy-contact-detail-promotion-code" style="margin-bottom: 10px;">
					<div class="title-text"><?php echo JText::_("SOCIAL_PROMO_CODE_TITLE"); ?></div>
					<div class="title-text2">
						<span style="font-size:14px;line-height: 30px;"><strong><?php echo JText::_("SOCIAL_PROMO_CODE_LABEL"); ?></strong></span>&nbsp;&nbsp;&nbsp;
					</div>
					<div style="float:right;width: 262px;">
						<input name="jform[promo_code]" id="jform_promo_code" type="text" value="<?php echo $quotation['promo_code']; ?>" /> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("SOCIAL_PROMO_CODE_DESC"); ?>">
					</div>
					<div class="clear"></div>
				</div>
				<div class="grey-hrule"></div>
				<div class="container5 clearfix" style="padding: 20px 10px; margin-top:10px;">
					<div class="clearfix">
						<div class="cpan5"><img src="images/DIY_Rewards.png" style="margin: 10px auto;display: block;" alt="Rewards" /></div>
						<div class="cpan55">
							<?php if (!$this->user->referred_by): ?>
							<div class="clearfix">
								<div class="cpan2">
									<div class="blue-title-text"><?php echo JText::_("SOCIAL_REFERRAL_TITLE"); ?> <img class="hasTooltip" alt="help-quote" style="margin-top: -5px;" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("SOCIAL_REFERRAL_DESC"); ?>"/></div>
									<span><?php echo JText::_("SOCIAL_REFERRAL_MSG_HEADER"); ?> <?php echo JText::_("SOCIAL_REFERRAL_MSG"); ?></span>
								</div>
								<div style="float:right;width: 262px;">
									<?php echo $form->getInput("referred_by"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("SOCIAL_REFERRAL_DESC"); ?>" />
								</div>
							</div>
							<div class="spacer-1"></div>
							<div class="grey-hrule"></div>
							<div class="spacer-1"></div>
							<?php endif; ?>
							
							<div class="clearfix">
								<div class="cpan2">
									<span class="blue-title-text" style="font-size: 19px;font-family:'sourcesanspro-semibold' "><?php echo JText::_("SOCIAL_USE_YOUR_SOCIAL_NETWORK"); ?><br/>
									<?php echo JText::_("SOCIAL_SHARE_TITLE"); ?></span>
									<div class="spacer-1"></div>
									<span style="font-size: 13px;"><?php echo $this->menu_params->get('diy_rewards_box1'); ?></span>
								</div>
								<div class="cpan2">
									<div style="float:right;" class="social-fb-msg">
										<?php echo str_replace("XXYY", JFactory::getUser()->referral_id, $this->menu_params->get('diy_rewards_box2')); ?>
									</div>
									<div class="clearfix">
										<div style="float:right;">
											<div class="social-btn-wrapper">
												<script type="text/javascript">var sharerwb = "<?php echo InsureDIYHelper::getWeiboSharer() ?>";</script>
												<a href="javascript: void(0)" target="_parent" onclick="window.open(sharerwb, 'sharer', 'width=626,height=436');"><img src="images/layout-icons/weibo.png"></a>
											</div>
										</div>
										<div style="float:right;margin-right: 20px;padding-top:6px;">
											<div class="social-btn-wrapper">
												<script type="text/javascript">var sharer = "<?php echo InsureDIYHelper::getFbSharer() ?>";</script>
												<a href="javascript: void(0)" target="_parent" onclick="window.open(sharer, 'sharer', 'width=626,height=436');"><img src="images/layout-icons/facebook.png"></a>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- Social Stuffs End-->
				<input type="hidden" name="task" value="form.step5save" />
				<input type="hidden" name="quotation_id" value="<?php echo $quotation['id']; ?>" />
				<?php echo JHtml::_('form.token'); ?>
				<div style="float:left;margin:15px 0;">
					<div>{modulepos insurediy-secured-ssl}</div>
				</div>
				<div class="btn-toolbar" style="float:right">
					<div>
						<button style="margin-right:10px;" type="button" onclick="javascript:document.adminForm.task.value = 'form.back';
						document.adminForm.submit();" class="btn btn-primary validate"><?php echo JText::_('BTN_BACK') ?></button>
						<button type="submit" class="btn btn-primary validate"><?php echo JText::_('JCONTINUE') ?></button>
					</div>
				</div>
				<div class="clear"></div>
			</fieldset>
		</form>
	</div>
</div>
<div class="logo-slider"><?php echo MyHelper::load_module(102); ?></div>

<?php /* Popup Box */ ?>
<div class="insurediy-popup" id="insurediy-popup-box" style="display:none;">
	<div class="header">
		<div class="text-header"><?php echo JText::_("POP_IMPORTANT_NOTICE"); ?></div>
		<a class="close" href="javascript:cleanInsureDIYPopup('insurediy-popup-box');">&nbsp;</a>
		<div class="clear"></div>
	</div>
	<div class="padding">
		<div><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_NAME_CHANGE_NOTICE_MSG"); ?></div>
		<div style="text-align:right;"><input type="button" class="btn btn-primary" style="height:30px;padding:5px 20px;font-size:14px !important;" onclick="javascript:cleanInsureDIYPopup('insurediy-popup-box');
		return false;" value="<?php echo JText::_("JOK"); ?>" /></div>
	</div>
</div>

<script>
	dataLayer.push({
		'event': 'checkoutOption',
		'ecommerce': {
			'checkout': {
				'actionField': {'step': 2}
			}
		}
	});
</script>


