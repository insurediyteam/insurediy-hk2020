<?php

defined('_JEXEC') or die;

class InsureDIYHospitalChildViewForm extends JViewLegacy {

	protected $form;
	protected $item;
	protected $state;

	public function display($tpl = null) {
		$session = JFactory::getSession();
		$model = $this->getModel();
		$app = JFactory::getApplication();
		$params = JComponentHelper::getParams('com_insurediyhospitalchild');
		$user = JFactory::getUser();
		$form = $this->get("Form");
		$quotation = InsureDIYHospitalChildHelper::getQuotation();
		$currentUser = MyHelper::getCurrentUser();

		$menu_params = $app->getMenu()->getActive()->params;
		$this->menu_params = $menu_params;
		
		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseWarning(500, implode("\n", $errors));
			return false;
		}

		$layout = InsureDIYHospitalChildHelper::getLayout();

		switch ($layout) {
			case 'step2':
				$this->step1CriteriaPass = $session->get('hospitalchild.step1CriteriaPass', TRUE);
				$this->ask4override = $session->get('hospitalchild.step1.ask4override', TRUE);
				$this->questionnaires = $this->get('Questionnaires');
				break;
			case 'step3':
				$this->policy_plans = $this->get('PolicyPlans');
				break;
			case 'step4':
				break;
			case 'step5':
				$referred_by = $session->get(SESSION_KEY_REFID, FALSE);
				if (!$currentUser->referred_by && $referred_by) {
					$form->setValue('referred_by', "", $referred_by);
				}
				break;
			case 'step6':
				$quotation['complete_address'] = InsureDIYHospitalChildHelper::getCompleteAddress($quotation);
				$this->paymentData = $model->getPDPayment();
				if (!$this->paymentData) {
					$app->enqueueMessage(JText::_("ERR_PAYMENT_CONFIG"));
				}
				break;
			case 'step7':
				$time = strtotime($quotation['created_date']);
				$year = (int) date("Y", $time);
				$month = (int) date("m", $time);
				$day = (int) date("d", $time);

				$x_days = (int) $params->get('x_days');
				$y_days = (int) $params->get('y_days');

				$quotation['sign_start'] = date("d M Y", mktime(0, 0, 0, $month, ($day + $x_days), $year));
				$quotation['sign_end'] = date("d M Y", mktime(0, 0, 0, $month, ($day + $x_days + $y_days), $year));
				$quotation['complete_address'] = InsureDIYHospitalChildHelper::getCompleteAddress($quotation);

				// data for email
				$session->set("hospitalchild.maildata.sign_start." . $quotation['id'], $quotation['sign_start']);
				$session->set("hospitalchild.maildata.sign_end." . $quotation['id'], $quotation['sign_end']);
				break;
			default:
				break;
		}

		$this->quotation = $quotation;
		$this->setLayout($layout);
		$this->state = $this->get('State');
		$this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));
		$this->currency = $params->get("currency", "HK$");
		$this->params = $params;
		$this->user = $user;
		$this->current_user = $currentUser;
		$this->form = $form;
		$this->flag_banner_path = 'images/flag_banners';
		$this->_prepareDocument();
		
		switch ($layout) {
			case 'thankyou':
			InsureDIYHospitalChildHelper::clearSessionData(); // woot~ all done. clear the session
			break;
		}
		
		parent::display($tpl);
	}

	/**
	 * Prepares the document
	 */
	protected function _prepareDocument() {
		$app = JFactory::getApplication();
		$menus = $app->getMenu();
		$title = null;

		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();

		if (empty($this->item->id)) {
			$head = JText::_('COM_INSUREDIYHOSPITALCHILD_FORM_PAGE_HEADING');
		} else {
			$head = JText::_('COM_INSUREDIYHOSPITALCHILD_FORM_EDIT_PAGE_HEADING');
		}

		if ($menu) {
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		} else {
			$this->params->def('page_heading', $head);
		}

		$title = $this->params->def('page_title', $head);
		if ($app->getCfg('sitename_pagetitles', 0) == 1) {
			$title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
		} elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
			$title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
		}
		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description')) {
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords')) {
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots')) {
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}

		//facebook stuffs
		$this->document->setMetaData("og:title", InsureDIYHospitalChildHelper::getFbTitle());
		$this->document->setMetaData("og:description", InsureDIYHospitalChildHelper::getFbDesc());
		$this->document->setMetaData("og:image", InsureDIYHospitalChildHelper::getFbImage());
		$this->document->setMetaData("og:app_id", InsureDIYHospitalChildHelper::getFbAppId());
		$this->document->setMetaData("og:site_name", InsureDIYHospitalChildHelper::getFbSiteName());
	}

}
