<?php

defined('JPATH_PLATFORM') or die;

JFormHelper::loadFieldClass("calendar");

class JFormFieldBookCalendar extends JFormFieldCalendar {

	protected $type = 'BookCalendar';
	protected $maxlength;
	protected $format;
	protected $filter;

	protected function getInput() {
		$params = JComponentHelper::getParams('com_insurediyhospitalchild');
		$x_days = (int) $params->get('x_days');
		$x_days = MyHelper::getNextWeekday($x_days);
		$y_days = (int) $params->get('y_days');

		// Translate placeholder text
		$hint = $this->translateHint ? JText::_($this->hint) : $this->hint;

		// Initialize some field attributes.
		$format = $this->format;

		// Build the attributes array.
		$attributes = array();

		empty($this->size) ? null : $attributes['size'] = $this->size;
		empty($this->maxlength) ? null : $attributes['maxlength'] = $this->maxlength;
		empty($this->class) ? null : $attributes['class'] = $this->class;
		!$this->readonly ? null : $attributes['readonly'] = '';
		!$this->disabled ? null : $attributes['disabled'] = '';
		empty($this->onchange) ? null : $attributes['onchange'] = $this->onchange;
		empty($hint) ? null : $attributes['placeholder'] = $hint;
		$this->autocomplete ? null : $attributes['autocomplete'] = 'off';
		!$this->autofocus ? null : $attributes['autofocus'] = '';

		if ($this->required) {
			$attributes['required'] = '';
			$attributes['aria-required'] = 'true';
		}

		if (is_array($attributes)) {
			$attribs = JArrayHelper::toString($attributes);
		}

		// Handle the special case for "now".
		if (strtoupper($this->value) == 'NOW') {
			$this->value = strftime($format);
		}

		// Get some system objects.
		$config = JFactory::getConfig();
		$user = JFactory::getUser();

		// If a known filter is given use it.
		switch (strtoupper($this->filter)) {
			case 'SERVER_UTC':
				// Convert a date to UTC based on the server timezone.
				if ((int) $this->value) {
					// Get a date object based on the correct timezone.
					$date = JFactory::getDate($this->value, 'UTC');
					$date->setTimezone(new DateTimeZone($config->get('offset')));

					// Transform the date string.
					$this->value = $date->format('d-m-Y', true, false);
				}

				break;

			case 'USER_UTC':
				// Convert a date to UTC based on the user timezone.
				if ((int) $this->value) {
					// Get a date object based on the correct timezone.
					$date = JFactory::getDate($this->value, 'UTC');

					$date->setTimezone(new DateTimeZone($user->getParam('timezone', $config->get('offset'))));

					// Transform the date string.
					$this->value = $date->format('d-m-Y', true, false);
				}

				break;
		}
		JHtml::_('bootstrap.tooltip');
		// Including fallback code for HTML5 non supported browsers.
		JHtml::_('jquery.framework');
		JHtml::_('script', 'system/html5fallback.js', false, true);
		JHtml::_('script', 'system/jquery.ui.core.js', false, true);
		JHtml::_('script', 'system/jquery.ui.datepicker.js', false, true);
		JHtml::_('stylesheet', 'system/jquery-ui.css', false, true);

		$document = JFactory::getDocument();
		$document->addScriptDeclaration(
				'jQuery(function() {
				
				jQuery( "#' . $this->id . '" ).datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "media/system/images/ico-calendar.png",
					minDate: ' . $x_days . ',
					maxDate: ' . ($x_days + $y_days) . ',
					dateFormat: "dd-mm-yy",
					defaultDate: ' . $x_days . ',
					beforeShowDay: jQuery.datepicker.noWeekends
				});
				jQuery( "#' . $this->id . '" ).datepicker("setDate", ' . $x_days . ');
			});'
		);


		return '<div class="input-append"><input type="text" title="' . (0 !== (int) $this->value ? static::_('date', $this->value, null, null) : '')
				. '" name="' . $this->name . '" id="' . $this->id . '" value="' . htmlspecialchars($this->value, ENT_COMPAT, 'UTF-8') . '" ' . $attribs . ' /></div>';
	}

}
