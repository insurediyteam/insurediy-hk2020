<?php

defined('_JEXEC') or die;

JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . '/tables');

class InsureDIYHospitalChildModelForm extends JModelForm {

	protected $_context = 'com_insurediyhospitalchild.quotation';
	protected $_tb_plan = "#__insure_hospitalchild_plans";
	protected $_tb_company = "#__insure_companies";
	protected $_tb_quotation = "#__insure_hospitalchild_quotations";
	protected $_tb_quotation_plan = "#__insure_hospitalchild_quotations_to_plans";
	protected $_tb_quotation_traveller = "#__insure_hospitalchild_quotations_to_travellers";
	protected $_tb_questionnaire = "#__insure_hospitalchild_questionnaires";
	protected $_tb_quotation_questionnaire = "#__insure_hospitalchild_quotation_questionnaires";
	protected $_tb_quotation_ex_insurance = "#__insure_hospitalchild_quotation_existing_insurances";

	protected function populateState() {
		$app = JFactory::getApplication();
		$params = $app->getParams();
		$this->setState('params', $params);
	}

	public function getForm($data = array(), $loadData = TRUE) {
		// Get the form.
		$form = $this->loadForm('com_insurediyhospitalchild.form', 'form', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}

		return $form;
	}

	protected function loadFormData() {
		$data = $this->getData();
		$this->preprocessData('com_insurediyhospitalchild.form', $data);

		return $data;
	}

	protected function getData() {
		$user = JFactory::getUser();
		$session = JFactory::getSession();
		$data = new stdClass();

		$inputQid = InsureDIYHospitalChildHelper::getInputQid();
		$quotation = array();

		if ($user->get('guest')) {
			if ($inputQid) {
				$this->setError(JText::_("ERR_NO_ACCESS_TO_VIEW"));
				return FALSE;
			}
		} else {
			if ($inputQid) {
				$quotation = InsureDIYHospitalChildHelper::getQuotation($inputQid);
				if (empty($quotation) || $quotation['user_id'] != $user->id) {
					$this->setError(JText::_("ERR_NO_ACCESS_TO_VIEW"));
					return FALSE;
				}
				$session->set("hospitalchild.quotation_id", $inputQid);
				$session->set("hospitalchild.data", $quotation);
			} else {
				$quotation = $session->get("hospitalchild.data", array());
			}
			$sessQid = InsureDIYHospitalChildHelper::getCurrentQid();
			if (!$sessQid) { // New quote
				$currentUser = MyHelper::getCurrentUser();
				$quotation['dob'] = $currentUser->dob;
				$quotation['gender'] = $currentUser->gender;
				$quotation['monthly_income'] = $currentUser->monthly_salary;
				$quotation['occupation'] = $currentUser->occupation;
				$quotation['country_residence'] = $currentUser->country;
				$quotation['nationality'] = $currentUser->nationality;
				$quotation['marital_status'] = $currentUser->marital_status;
			}
		}
		if ($quotation) {
			foreach ($quotation as $k => $v) {
				$data->$k = $v;
			}
		}
		return $data;
	}

	public function getPolicyPlans() {
		$db = JFactory::getDBO();
		$data = InsureDIYHospitalChildHelper::getQuotation();
		$query = $db->getQuery(TRUE);

		$query->select("p.*, c.logo");
		$query->from($this->_tb_plan . " AS p");
		$query->innerJoin($this->_tb_company . " AS c ON c.insurer_code = p.insurer_code");

		// filters
		$room_type = $data['room_type'];
		$gender = $data['gender'];
		$cover_amt = $data['cover_amt'];
		$cover_country = $data['cover_country'];
		$residency = $data['country_residence'];
		$nationality = $data['nationality'];

		$query->where("p.room_type = " . $db->Quote($room_type));
		$query->where("p.gender = " . $db->Quote($gender));
		$query->where("p.cover_amt = " . $db->Quote($cover_amt));
		$query->where("p.cover_country = " . $db->Quote($cover_country));
		$query->where("p.age = " . $db->Quote($data['age']));
		$query->where("p.residency = " . $db->Quote($residency));
		$query->where("p.nationality = " . $db->Quote($nationality));
		$query->where("p.state = 1");

		$db->setQuery($query, 0, 6);
		$rows = $db->loadObjectList();
		return $rows;
	}

	public function getMyPlans() {
		$db = JFactory::getDBO();
		$quotation_id = InsureDIYHospitalChildHelper::getQid();
		$query = $db->getQuery(TRUE);
		$query->select("*");
		$query->from($this->_tb_quotation_plan);
		$query->where("quotation_id = " . $quotation_id);
		$db->setQuery($query);
		$rows = $db->loadObject();
		return $rows;
	}

	public function getMyInfo($id = FALSE) {
		$db = JFactory::getDBO();
		$quotation_id = ($id) ? $id : InsureDIYHospitalChildHelper::getQid();
		$query = $db->getQuery(TRUE);
		$query->select("*");
		$query->from($this->_tb_quotation);
		$query->where("id = " . $quotation_id);
		$db->setQuery($query);
		$row = $db->loadObject();
		return $row;
	}

	public function getPDPayment() {
		$app = JFactory::getApplication();
		$params = $app->getParams();
		$quotation_id = InsureDIYHospitalChildHelper::getQid();
		$config_com_insureDIY	= JComponentHelper::getParams('com_insurediy');
		$data = new stdClass();
		if ($this->hasPayment($quotation_id)) {
			$data->hasPayment = TRUE;
			$quotation = InsureDIYHospitalChildHelper::getQuotation($quotation_id);
			if (!$quotation) {
				return FALSE;
			}
			$data->action = $params->get('pdUrl', "https://test.paydollar.com/b2cDemo/eng/payment/payForm.jsp");
			$data->mpsMode = $params->get('mpsMode', 'NIL');
			$data->currCode = $params->get('currCode', '344');
			$data->lang = $params->get('lang', 'E');
			$data->merchantId = $params->get('merchantId', FALSE);
			$data->orderRef = $quotation['unique_order_no'];
			$data->amount = $quotation["selected_plan"]->premium * InsureDIYHospitalChildHelper::getPremiumMonth();
			$data->cancelUrl = JURI::base() . 'index.php?option=com_insurediyhospitalchild&view=form&layout=canceled';
			$data->failUrl = JURI::base() . 'index.php?option=com_insurediyhospitalchild&view=form&layout=error';
			$data->successUrl = JURI::base() . 'index.php?option=com_insurediyhospitalchild&view=form&quotation_id=' . $quotation_id;
			$data->payType = "N";
			$data->payMethod = "ALL";
			$data->secureHash = $config_com_insureDIY->get('secureHash', '');
			$data->remark = "hospitalchild"; // Using remark to check for different type of insurances. Do not use this for other purpose.
			if (!$data->merchantId) {
				return FALSE;
			}
		} else {
			$data->hasPayment = FALSE;
		}
		return $data;
	}

//	public function getQuotation($quotation_id, $user_id = FALSE) {
//		if (!$quotation_id) {
//			return FALSE;
//		}
//		$db = JFactory::getDBO();
//		$query = $db->getQuery(TRUE);
//
//		$query->select("q.*, qp.premium, qp.insurer_code");
//		$query->from($this->_tb_quotation . " AS q");
//		$query->leftJoin($this->_tb_quotation_plan . " AS qp ON q.id = qp.quotation_id");
//		$query->where("q.id = " . $quotation_id);
//		if ($user_id) {
//			$query->where("q.user_id = " . (int) $user_id);
//		}
//		$db->setQuery($query);
//		$quotation = $db->loadObject();
//		return $quotation;
//	}

	public function updateQuotation($quotation_id, $data) {
		$db = JFactory::getDbo();
		$dataobj = is_object($data) ? $data : MyHelper::array2jObject($data);
		$dataobj->id = $quotation_id;
		$result = $db->updateObject($this->_tb_quotation, $dataobj, "id");
		if ($result) {
			InsureDIYHospitalChildHelper::overrideSessionWithQid($quotation_id);
		}
		return $result;
	}

	private function hasPayment($quotation_id) { // quotation_id(int), return boolean
		$has = TRUE;
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from($this->_tb_quotation)
				->where("id = " . (int) $quotation_id);
		$db->setQuery($query);
		$quotation = $db->loadObject();
		if ($quotation->payment_status != "N" && $quotation->payment_status != "P") {
			$has = FALSE;
		}
		if ($quotation->quote_stage < 5) {
			$has = FALSE;
		}
		if ($quotation->unique_order_no == "") {
			$has = FALSE;
		}
		return $has;
	}

	public function getMySumInsuredTotal() {
		return $this->checkSumInsuredQuotationTotal();
	}

	public function getContactDetails() {
		$user = JFactory::getUser();
		$data = array();

		if ($user->id > 0) {
			$db = JFactory::getDbo();
			$query = $db->getQuery(TRUE);
			$query->select("u.name AS contact_firstname, "
					. "u.lastname AS contact_lastname, "
					. "u.room_no AS contact_room_no,"
					. "u.floor_no AS contact_floor_no,"
					. "u.block_no AS contact_block_no,"
					. "u.building_name AS contact_building_name,"
					. "u.street_no AS contact_street_name,"
					. "u.country_code AS contact_country_code,"
					. "u.contact_no AS contact_contact_no,"
					. "u.district AS contact_district_name,"
					. "u.postal_code AS contact_postalcode,"
					. "u.country AS contact_country");
			$query->from("#__users u");
			$query->where("id=" . $db->quote($user->id));
			$db->setQuery($query);
			$results = $db->loadAssoc();

			if (!is_null($results)) {
				$session = JFactory::getSession();
				$this->my_detail = $session->get('details');
				$this->my_detail['contact_country_code'] = isset($results["contact_country_code"]) ? $results["contact_country_code"] : "";
				$session->set('details', $this->my_detail);
			}

			return ($results) ? $results : $data;
		}
		return $data;
	}

	public function getQuestionnaires() {
		$db = JFactory::getDBO();
		$quotation_id = InsureDIYHospitalChildHelper::getQid();
		$gender = $db->setQuery($db->getQuery(TRUE)->select("gender")->from($this->_tb_quotation)->where("id = " . $db->quote($quotation_id)))->loadResult();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from($this->_tb_questionnaire)
				->where("gender = " . $db->quote($gender))->where("state = 1")
				->order("ordering ASC");
		return $db->setQuery($query)->loadObjectList();
	}

	public function step1save($data) {
		$db = JFactory::getDBO();
		$session = JFactory::getSession();
		$user = JFactory::getUser();

		if ($data['cover_country'] == "WW" || $data['cover_country'] == "WU") {
			$data['room_type'] = 3;
			$data['cover_amt'] = 2;
		}

		$data['user_id'] = 0;
		if (!$user->get('guest')) {
			$data['user_id'] = $user->id;
			$data['email'] = $user->email;
			$data['unique_order_no'] = InsureDIYHelper::getUniqueOrderNo($user->id);
			if (isset($data['id']) && $data['id'] == 0 && !$user->is_firsttime) { // If new quotation and user is already logged in, get the contact information from user and save it in quotation
				$data = array_merge($data, InsureDIYHospitalChildHelper::getCurrentUserContacts());
			}
		}

		$data['dob'] = date("Y-m-d", strtotime($data['dob']));
		$data['quote_stage'] = 2; // step 2 now

		if (isset($data['id']) && $data['id'] == 0) {
			$data['created_date'] = JFactory::getDate()->toSql();
			$dataobj = MyHelper::array2jObject($data);
			$db->insertObject($this->_tb_quotation, $dataobj, "id");
		} else {
			$dataobj = MyHelper::array2jObject($data);
			$db->updateObject($this->_tb_quotation, $dataobj, "id");
		}
		$quotation = InsureDIYHospitalChildHelper::getQuotation($dataobj->id);
		$session->set("hospitalchild.data", $quotation);
		return $dataobj->id;
	}

	public function step2save($data) {
		$db = JFactory::getDBO();
		$quotation_id = InsureDIYHospitalChildHelper::getQid();
		$session = JFactory::getSession();
		if (count($data['question_id'])) {
			$this->deleteAllAnswers($quotation_id);
			$answers = $data['answer'];
			foreach ($data['question_id'] as $key => $v) {
				$answer = $answers[$key] ? $answers[$key] : 0;
				$data = array("question_id" => $v, "quotation_id" => $quotation_id, "answer" => $answer);
				$dataobj = MyHelper::array2jObject($data);
				$db->insertObject($this->_tb_quotation_questionnaire, $dataobj);
			}
		}
		$this->updateStage($quotation_id, 3);
		$quotation = InsureDIYHospitalChildHelper::getQuotation($quotation_id);
		$session->set("hospitalchild.data", $quotation);
		return TRUE;
	}

	public function step3save($quotation_id, $plan_id) {
		$db = JFactory::getDbo();
		$session = JFactory::getSession();
		$plan = $this->getPlan($plan_id);
		$plan->quotation_id = $quotation_id;
		$query = $db->getQuery(TRUE)
				->select("count(*)")
				->from($this->_tb_quotation_plan)
				->where("quotation_id = " . $quotation_id);
		$count = $db->setQuery($query)->loadResult();
		if ($count) {
			$check = $db->updateObject($this->_tb_quotation_plan, $plan, "quotation_id");
		} else {
			$check = $db->insertObject($this->_tb_quotation_plan, $plan);
		}
		if ($check) {
			$this->updateStage($quotation_id, "4");
			$quotation = InsureDIYHospitalChildHelper::getQuotation($quotation_id);
			$session->set("hospitalchild.data", $quotation);
		}
		return $check && $this->updateStage($quotation_id, "4");
	}

	public function step4save($post) {
		$user = JFactory::getUser();
		$db = JFactory::getDBO();
		$session = JFactory::getSession();
		$quotation_id = InsureDIYHospitalChildHelper::getQid();
		$insurerNameArr = $post['insurer_name'];
		$policyTypeArr = $post['policy_type'];
		$sumInsuredArr = $post['sum_insured'];
		$issuanceDateArr = $post['issuance_date'];
		unset($post['insurer_name']);
		unset($post['policy_type']);
		unset($post['sum_insured']);
		unset($post['issuance_date']);
		//$post['unique_order_no'] = InsureDIYHelper::getUniqueOrderNo($user->id);
		$objData = MyHelper::array2object($post);
		$objData->id = $quotation_id;
		$db->updateObject($this->_tb_quotation, $objData, "id");

		/* Clear Ex plans before saving */
		$this->deleteAllExInsurances($quotation_id);

		/* existing insurance - save multiple */
		foreach ($insurerNameArr as $key => $val) {
			$policy_type = $policyTypeArr[$key];
			$sum_insured = $sumInsuredArr[$key];
			$issuance_date = $issuanceDateArr[$key];
			if (strlen($val) > 0 || strlen($policy_type) > 0 || strlen($sum_insured) > 0 || strlen($issuance_date) > 0) {
				$query = " INSERT INTO " . $this->_tb_quotation_ex_insurance . " (`quotation_id`,`insurer_name`,`policy_type`,`sum_insured`,`issuance_date`) VALUES('$quotation_id','$val','$policy_type','$sum_insured','$issuance_date');";
				$db->setQuery($query);
				$db->query();
			}
		}

		$this->updateStage($quotation_id, 5);
		$quotation = InsureDIYHospitalChildHelper::getQuotation($quotation_id);
		$session->set("hospitalchild.data", $quotation);
		return TRUE;
	}

	public function step5save($post) { // this is parent's detail. so no ex insurance.
		$db = JFactory::getDBO();
		$session = JFactory::getSession();
		$user = JFactory::getUser();
		$quotation_id = InsureDIYHospitalChildHelper::getQid();
		$is_firsttime = isset($post["is_firsttime"]) ? $post["is_firsttime"] : FALSE;

		/* unset is_firsttime field, not needed for Quotation */
		unset($post["is_firsttime"]);
		//$post['unique_order_no'] = InsureDIYHelper::getUniqueOrderNo($user->id);

		$objData = MyHelper::array2object($post);
		$objData->id = $quotation_id;
		$db->updateObject($this->_tb_quotation, $objData, "id");

		/* if this customer is first time , insert contact details into profile data */
		if ($is_firsttime) {
			$fieldTmp = array();
			$fieldTmp[] = " name = " . $db->Quote($post['contact_firstname'], false);
			$fieldTmp[] = " lastname = " . $db->Quote($post['contact_lastname'], false);
			$fieldTmp[] = " country_code = " . $db->Quote($post['contact_country_code'], false);
			$fieldTmp[] = " contact_no = " . $db->Quote($post['contact_contact_no'], false);
			$fieldTmp[] = " room_no = " . $db->Quote($post['contact_room_no'], false);
			$fieldTmp[] = " floor_no = " . $db->Quote($post['contact_floor_no'], false);
			$fieldTmp[] = " block_no = " . $db->Quote($post['contact_block_no'], false);
			$fieldTmp[] = " building_name = " . $db->Quote($post['contact_building_name'], false);
			$fieldTmp[] = " street_no = " . $db->Quote($post['contact_street_name'], false);
			$fieldTmp[] = " district = " . $db->Quote($post['contact_district_name'], false);
			$fieldTmp[] = " postal_code = " . $db->Quote($post['contact_postal_code'], false);
			$fieldTmp[] = " country = " . $db->Quote($post['contact_country'], false);
			$fieldTmp[] = " is_firsttime = " . $db->Quote(0, false);
			$fieldTmp[] = " gender = " . $db->Quote($post['gender'], false);
			$fieldTmp[] = " marital_status = " . $db->Quote($post['marital_status'], false);
			$fieldTmp[] = " dob = " . $db->Quote($post['dob'], false);
			$fieldTmp[] = " monthly_salary = " . $db->Quote($post['monthly_income'], false);
			$fieldTmp[] = " occupation = " . $db->Quote($post['occupation'], false);

			$query = " UPDATE #__users SET " . implode(",", $fieldTmp) . " WHERE id = '" . $user->id . "' ";
			$db->setQuery($query);
			$db->query();
		}
		$this->updateStage($quotation_id, 6);
		$quotation = InsureDIYHospitalChildHelper::getQuotation($quotation_id);
		$session->set("hospitalchild.data", $quotation);
		return TRUE;
	}

	public function step7save($post) {
		$db = JFactory::getDbo();
		$user = JFactory::getUser();
		$session = JFactory::getSession();
		$quotation_id = InsureDIYHospitalChildHelper::getQid();
		$session->set("hospitalchild.maildata.contact_no." . $quotation_id, $post['country_code'] . " " . $post['phone_number']);

		if ($post['meet_type'] == 1) { // predetermine location
			$post['meet_date'] = '0000-00-00';
			$post['meet_time'] = '';
			$post['meet_contact_no'] = '';
			$post['meet_address'] = '';

			// data for email
			$session->set("hospitalchild.maildata.address." . $quotation_id, $post['pre_location_id']);
			$session->set("hospitalchild.maildata.meet_time." . $quotation_id, "9am to 6pm");
			$start_date = $session->get("hospitalchild.maildata.sign_start." . $quotation_id);
			$end_date = $session->get("hospitalchild.maildata.sign_end." . $quotation_id);
			$session->set("hospitalchild.maildata.meet_date." . $quotation_id, $start_date . " to " . $end_date);
			unset($post['time1']);
			unset($post['time2']);
			unset($post['time3']);
			unset($post['country_code']);
			unset($post['phone_number']);
		} else { // 2, set appointment
			$post['meet_time'] = $post['time1'] . ':' . $post['time2'] . ' ' . $post['time3'];
			$post['meet_contact_no'] = $post['country_code'] . ' ' . $post['phone_number'];
			$post['pre_location_id'] = '';
			$post['meet_date'] = date("Y-m-d", strtotime($post['meet_date']));

			// data for email
			$session->set("hospitalchild.maildata.meet_time." . $quotation_id, $post['meet_time']);
			$session->set("hospitalchild.maildata.meet_date." . $quotation_id, $post['meet_date']);
			$session->set("hospitalchild.maildata.address." . $quotation_id, $post['meet_address']);

			unset($post['time1']);
			unset($post['time2']);
			unset($post['time3']);
			unset($post['country_code']);
			unset($post['phone_number']);
		}
		$post['quote_stage'] = 8;
		$post['quote_status'] = 1; // Quote Status is Complete
		//$post['unique_order_no'] = InsureDIYHelper::getUniqueOrderNo($user->id);
		$objData = MyHelper::array2object($post);
		return $db->updateObject($this->_tb_quotation, $objData, "id");
	}

	public function savePhotoIdentityFileUpload($file, $path, $db_field) {
		JLoader::import('joomla.filesystem.folder');
		JLoader::import('joomla.filesystem.file');

		$session = JFactory::getSession();
		$db = JFactory::getDBO();
		$quotation_id = InsureDIYHospitalChildHelper::getQid();
		if (!$quotation_id) {
			return false;
		}

		$name = $file['name'];
		$type = $file['type'];
		$tmp_name = $file['tmp_name'];
		$error = $file['error'];
		$size = $file['size'];

		$uidStr = '' . JFactory::getUser()->id;
		if (strlen($uidStr) % 2) {
			$uidStr = '0' . $uidStr; //prepend 0 to odd num
		}
		$deepPath = "";
		//create sub-folder for every 2 digits
		foreach (str_split($uidStr, 2) as $dir) {
			$deepPath .= DS . $dir;
			if (!JFolder::exists(JPATH_BASE . DS . $path . DS . $deepPath)) {
				JFolder::create(JPATH_BASE . DS . $path . DS . $deepPath, 0777);
			}
		}
		if ($size > 0) {
			$filename = md5(time()) . rand(100, 999) . '-' . $name;
			if (move_uploaded_file($tmp_name, JPATH_BASE . DS . $path . DS . $deepPath . DS . $filename)) {
				$nameToStore = $deepPath . DS . $filename . "|" . $name;

				$query = " SELECT " . $db_field . " FROM " . $this->_tb_quotation . " WHERE id = '$quotation_id' ";
				$db->setQuery($query);

				if ($prev_entry = $db->loadResult()) {
					$prev_entry = explode("|", $prev_entry);
					$prev_filename = (isset($prev_entry[0])) ? $prev_entry[0] : FALSE;
					if ($prev_filename) {
						unlink(JPATH_BASE . '/' . $path . '/' . $prev_filename);
					}
				}
				$query = " UPDATE " . $this->_tb_quotation . " SET " . $db_field . " = " . $db->quote($nameToStore) . " WHERE id = '$quotation_id' ";
				$db->setQuery($query);
				$db->query();
			}
		}
		$quotation = InsureDIYHospitalChildHelper::getQuotation($quotation_id);
		$session->set("hospitalchild.data", $quotation);
		return true;
	}

	public function saveContactDetails($qid) {
		$contact = $this->getContactDetails();
		$db = JFactory::getDbo();
		$contact['id'] = $qid;
		return $db->updateObject($this->_tb_quotation, MyHelper::array2jObject($contact), "id");
	}

	public function deleteAllAnswers($quotation_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->delete($this->_tb_quotation_questionnaire)
				->where("quotation_id = " . $quotation_id);
		return $db->setQuery($query)->execute();
	}

	public function deleteAllExInsurances($quotation_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->delete($this->_tb_quotation_ex_insurance)
				->where("quotation_id = " . $quotation_id);
		return $db->setQuery($query)->execute();
	}

	public function getPlan($plan_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from($this->_tb_plan)
				->where("plan_id = " . $plan_id);
		$db->setQuery($query);
		return $db->loadObject();
	}

	public function updateStage($quotation_id, $stage) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->update($this->_tb_quotation)
				->set("quote_stage = " . $stage)
				->where("id = " . $quotation_id);
		$db->setQuery($query);
		$check = $db->execute();
		return $check;
	}

	public function getStep($quotation_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("quote_stage")
				->from($this->_tb_quotation)
				->where("id = " . $quotation_id);
		$db->setQuery($query);
		$check = $db->loadResult();
		return $check;
	}

	public function saveConfirmationDetail($data) {
		$db = JFactory::getDBO();
		$quotation_id = InsureDIYHospitalChildHelper::getQid();
		$query = $db->getQuery(TRUE);
		$query->update($this->_tb_quotation);
		$conditions = array();
		$conditions[] = "quote_stage = 6";
		$conditions[] = "payment_status = " . $db->quote("P");
		$conditions[] = "declare_have_you_replaced = " . $db->quote($data['declare_have_you_replaced']);
		$conditions[] = "declare_do_you_intend_to_replace = " . $db->quote($data['declare_do_you_intend_to_replace']);
		$query->set($conditions, ",");
		$query->where("id = " . $db->quote($quotation_id));
		$json = array();
		if ($db->setQuery($query)->execute()) {
			$json['success'] = "1";
		} else {
			$json['error'] = "1";
		}
		return $json;
	}

	public function savePDFPaths($quotation_id, $array = array(), $client = TRUE) {
		if (empty($array)) {
			return;
		}
		$toJson = new stdClass();
		foreach ($array as $k => $v) {
			if (is_array($v)) {
				$toJson->$k = implode(" | ", $v);
			} else {
				$toJson->$k = $v;
			}
		}
		$db = JFactory::getDBO();
		if ($client) {
			$query = " UPDATE " . $this->_tb_quotation . " SET `file_json_pdfpath` = " . $db->Quote(json_encode($toJson), false) . " WHERE id = '$quotation_id' ";
		} else {
			$query = " UPDATE " . $this->_tb_quotation . " SET `file_json_pdfpath2` = " . $db->Quote(json_encode($toJson), false) . " WHERE id = '$quotation_id' ";
		}
		$db->setQuery($query);
		$db->query();
	}

	// Mail Stuffs - start
	public function sendEmails($pdfarrs, $client = TRUE, $id = FALSE) {
		$mailer = JFactory::getMailer();
		$params = JComponentHelper::getParams('com_insurediyhospitalchild');
		$myInfo = $this->getMyInfo($id);
		$legit = $this->checkBlacklist($myInfo);
		$maildata = $this->getMailData($myInfo);
		$emails = $this->getMails($params, $legit);
		$emailbodies = $this->getEmailBodies($params, $maildata);

		$mode = TRUE;
		foreach ($pdfarrs as $pdfarr) {
			$attachments = $this->prepareAttachments($pdfarr, $myInfo->id);
			if ($client) {
				$mailer->ClearAttachments();
				$mailer->ClearAllRecipients();
				$mailer->sendMail($emails['from'], $emails['fromName'], $emails['recipient'], $emails['subject'], $emailbodies['ce_customer'], $mode, NULL, $emails['bcc'], $attachments);
			} else {
				$mailer->ClearAttachments();
				$mailer->ClearAllRecipients();
				//$mailer->sendMail($emails['from'], $emails['fromName'], $emails['service_recipient'], $emails['subject'], $emailbodies['ce_service'], $mode, NULL, $emails['bcc'], $attachments);
				$mailer->sendMail($emails['from'], $emails['fromName'], $emails['service_recipient'], $emails['subject'], $emailbodies['ce_service'], $mode, NULL, array(), $attachments);
			}
		}
	}

	public function prepareAttachments($pdfarr, $id) {
		$temp = array();
		foreach ($pdfarr as $pdf) {
			$temp[] = MyHelper::getDeepPath(QUOTATION_PDF_SAVE_PATH, $id) . $pdf;
		}
		return $temp;
	}

	public function getEmailBodies($params, $data) {
		$array = array();
		$array['cover_letter'] = InsureDIYHelper::replaceVariables($params->get("clformat"), $data);
		$array['ce_customer'] = InsureDIYHelper::replaceVariables($params->get("cecformat"), $data);
		$array['ce_service'] = InsureDIYHelper::replaceVariables($params->get("cesformat"), $data);
		return $array;
	}

	public function getMails($params, $legit) {
		$confg = JFactory::getConfig();
		$user = JFactory::getUser();
		$array = array();
		if ($legit) {
			$array['recipient'] = $user->email;
			$sales_email = $params->get('sales_email');
			$array['bcc'] = array("0" => "zan@ifoundries.com", "1" => $sales_email, "2" => "rylaicf77@gmail.com");
			$array['subject'] = 'InsureDIY - Application Form';
		} else {
			$array['recipient'] = $params->get('admin_email');
			$sales_email = $params->get('sales_email');
			$array['bcc'] = array("0" => "zan@ifoundries.com", "1" => $sales_email, "2" => "rylaicf77@gmail.com");
			$array['subject'] = 'InsureDIY - Application Form[Blacklist]';
		}
		$array['service_recipient'] = $params->get('service_email', "zan@ifoundries.com");
		$array['from'] = $confg->get("mailfrom");
		$array['fromName'] = $confg->get("fromname");
		return $array;
	}

	public function getMailData($myInfo) {
		$session = JFactory::getSession();
		$data = array();
		$data['first_name'] = $myInfo->contact_firstname;
		$data['last_name'] = $myInfo->contact_lastname;
		$data['order_no'] = $myInfo->unique_order_no;
		$data['address'] = $session->get("maildata.address." . $myInfo->id, FALSE);
		$data['contact_number'] = $session->get("maildata.contact_no." . $myInfo->id, FALSE);
		return $data;
	}

	public function checkBlacklist($data) {
		$name = $data->contact_firstname . " " . $data->contact_lastname;
//		$nationality = $data->contact_country;
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$query->select("count(*)");
		$query->from("#__insure_blacklist");
		$query->where("UPPER(name) = UPPER(" . $db->quote($name) . ")");
//		$query->where("UPPER(name) = UPPER(" . $db->quote($name) . ") AND nationality = " . $db->quote($nationality));
		$db->setQuery($query);
		$result = $db->loadResult();
		if ($result > 0) {
			return FALSE;
		}
		return TRUE;
	}

	// Mail Stuffs - end

	public function getPdfData($quotation_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_life_quotations")
				->where("id=" . $quotation_id);
		$db->setQuery($query);
		$result = $db->loadObject();

		$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_life_quotation_existing_insurances")
				->where("quotation_id=" . $quotation_id);
		$db->setQuery($query);
		$existing_insurances = $db->loadObjectList();

		$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_life_quotation_properties")
				->where("quotation_id=" . $quotation_id)
				->order("id");
		$db->setQuery($query);
		$properties = $db->loadObjectList();

		$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_life_quotation_cars")
				->where("quotation_id=" . $quotation_id);
		$db->setQuery($query, 0, 3);
		$cars = $db->loadObjectList();

		// Prepare data before return
		$result->hasLargeSum = $this->checkSumInsuredQuotationTotal() >= 5000000;
		$result->contact_firstname = strtoupper($result->contact_firstname);
		$result->contact_lastname = strtoupper($result->contact_lastname);
		$result->country_residence = InsureDIYHelper::getCountryOfResidence($result->country_residence);
		$result->nationality = InsureDIYHelper::getNationality($result->nationality);

		$result->monthly_income_ckbox = $result->monthly_income;
		$result->monthly_income = JText::_($result->monthly_income);

		switch ($result->contact_identity_type) {
			case 'chid' :
				$result->contact_identity_no2 = $result->contact_identity_no;
				unset($result->contact_identity_no);

				$tmp = explode('-', $result->contact_expiry_date);
				$result->contact_expiry_date_y = $tmp[0];
				$result->contact_expiry_date_m = $tmp[1];
				$result->contact_expiry_date_d = $tmp[2];
				break;
			case 'passport':
				$result->contact_identity_no3 = $result->contact_identity_no;
				unset($result->contact_identity_no);

				$tmp = explode('-', $result->contact_expiry_date);
				$result->contact_expiry_date_y2 = $tmp[0];
				$result->contact_expiry_date_m2 = $tmp[1];
				$result->contact_expiry_date_d2 = $tmp[2];

				$result->bank_acct_holder_type = "p";
				break;
			case "hkid":
				$result->bank_acct_holder_type = "i";
			default:
				break;
		}
		$result->bank_acct_holder_doc_no = $result->contact_identity_no;

		$dob = explode("-", $result->dob);
		$result->dob_y = $dob[0];
		$result->dob_m = $dob[1];
		$result->dob_d = $dob[2];
		$result->age = MyHelper::getAge($result->dob);
		$result->contact_telephone = (($result->contact_country_code) ? '(' . $result->contact_country_code . ') ' : '') . $result->contact_contact_no;

		$result->job_nature = JText::_($result->job_nature);
		$result->curreny = "hkd";
		$result->mode = "monthly";
		$result->source = "savings";
		$result->purpose = "protection";
		$result->cover_amt = "HK$" . $result->cover_amt;

		$result->contact_lang = "english";

		$result->height_unit = "cm";
		$result->weight_unit = "kg";
		$result->has_used_drugs = "1";

		$result->direct_promotion = "1";
		$result->full_name = $result->contact_firstname . " " . $result->contact_lastname;
		$result->relationship = "applicant";

		foreach ($existing_insurances as $k => $existing_insurance) {
			$key = $k + 1;
			$insurer_key = "ex_insurer_name_" . $key;
			$insured_key = "ex_insured_name_" . $key;
			$sum_key = "ex_sum_insured_" . $key;
			$date_key = "ex_issuance_date_" . $key;

			$result->$insurer_key = $existing_insurance->insurer_name;
			$result->$insured_key = $result->full_name;
			$result->$sum_key = $existing_insurance->sum_insured;
			$result->$date_key = $existing_insurance->issuance_date;
		}

		$year = (int) date("Y", strtotime($result->created_date));
		$result->lastyear = $year - 1;
		$result->last2year = $year - 2;
		$result->last3year = $year - 3;
		foreach ($properties as $k => $property) {
			$key = $k + 1;
			$property_purchase_key = "property_purchase_" . $key;
			$property_price_key = "property_price_" . $key;
			$property_mortgage_key = "property_mortgage_" . $key;
			$property_value_key = "property_value_" . $key;
			$result->$property_purchase_key = $property->property_purchase;
			$result->$property_price_key = $property->property_price;
			$result->$property_mortgage_key = $property->property_mortgage;
			$result->$property_value_key = $property->property_value;
		}

		$result->no_of_cars = count($cars);

		foreach ($cars as $k => $car) {
			$key = $k + 1;
			$model_key = "car_model_" . $key;
			$result->$model_key = $car->car_model;
		}

		$result->contact_country = ($result->contact_country == "HK") ? $result->contact_country : "OTH";

		$result->fna = "0"; // SUN only
		$result->owner_type = "0";
		return $result;
	}

	public function createPoints($quotation_id) {
		$user = JFactory::getUser();
		$quotation = InsureDIYHospitalChildHelper::getQuotation($quotation_id);
		$plan = $quotation['selected_plan'];
		$total_pur_points = $plan->pur_points;
		$total_ref_points = $plan->ref_points;
		if (InsureDIYHelper::checkReferral($quotation['referred_by'])) {
			InsureDIYHelper::updateReferredBy($quotation['referred_by']);
			InsureDIYHelper::createReferral($quotation['referred_by'], "hospitalchild." . $quotation_id, $quotation['unique_order_no'], $total_ref_points, sprintf(JText::_("REFERRAL_POINT_RECORD_DESCRIPTION"), $user->email));
		}
		$desc = sprintf(JText::_("PURCHASE_POINT_RECORD_DESCRIPTION"), $quotation['unique_order_no']);
		$percent = InsureDIYHelper::getPromoCode($quotation['promo_code']);
		if ($percent) {
			$total_pur_points = $total_pur_points * (100 + $percent) / 100;
			InsureDIYHelper::usePromoCode($quotation['promo_code']);
			$desc .= ". With promo code " . $quotation['promo_code'];
		}
		InsureDIYHelper::createPoint($user->id, $total_pur_points, "hospitalchild." . $quotation_id, $quotation['unique_order_no'], "", $desc, 1);
		return TRUE;
	}

	public function createPolicyRecord($quotation_id) {
		$db = JFactory::getDbo();
		$quotation = InsureDIYHospitalChildHelper::getQuotation($quotation_id);
		$obj = new stdClass();
		$obj->user_id = $quotation['user_id'];
		$obj->type = "3"; // hospital is 3.
		$obj->insurer = $quotation['selected_plan']->insurer_code;
		$obj->currency = 344; // HK$
		$obj->status = 2; // Set as pending. 1 => active, 0 => inactive, 2 => pending
		$obj->room_type = $quotation['room_type'];
		$obj->cover_type = $quotation['cover_amt'];
		$obj->quotation_id = $quotation['id'];
		$db->insertObject("#__insure_policies", $obj);
	}

	public function back($quotation_id) {
		$db = JFactory::getDBO();
		$user = JFactory::getUser();
		$current_stage = InsureDIYHospitalChildHelper::getCurrentStage($quotation_id);
		$backables = array(2 => 1, 3 => 2, 4 => 3, 5 => 4, 6 => 5);
		$backstage = (in_array($current_stage, array_keys($backables))) ? $backables[$current_stage] : FALSE;
		if ($backstage && !$user->get('guest')) {
			$query = $db->getQuery(TRUE)
					->update($this->_tb_quotation)
					->set("quote_stage = " . $backstage)
					->where("id = " . $quotation_id)
					->where("quote_stage > 1")
					->where("user_id = " . $user->id);
			$result = $db->setQuery($query)->execute();
		}
		return $backstage;
	}

}
