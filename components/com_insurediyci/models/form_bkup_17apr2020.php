<?php

defined('_JEXEC') or die;

JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . '/tables');

class InsureDIYCIModelForm extends JModelForm {

	protected $_context = 'com_insurediyci.quotation';
	protected $_tb_questionnaire = "#__insure_ci_questionnaire";
	protected $_tb_plan = "#__insure_ci_plans";
	protected $_tb_company = "#__insure_companies";
	protected $_tb_quotation = "#__insure_ci_quotations";
	protected $_tb_quotation_plan = "#__insure_ci_quotation_plan_xref";
	protected $_tb_quotation_ex_insurance = "#__insure_ci_quotation_existing_insurances";
	protected $_tb_quotation_questionnaire = "#__insure_ci_quotation_questionnaire";
	protected $_tb_quotation_property = "#__insure_ci_quotation_properties";
	protected $_tb_quotation_cars = "#__insure_ci_quotation_cars";
	protected $_tb_policy_number = "#__insure_ci_policy_numbers";

	public function getTable($type = 'Quotation', $prefix = 'InsureDIYCITable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}

	public function getForm($data = array(), $loadData = true) {
		$form = $this->loadForm('com_insurediyci.form', 'form', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}
		return $form;
	}

	protected function loadFormData() {
		$data = $this->getData();
		$this->preprocessData('com_insurediyci.form', $data);
		return $data;
	}

	protected function getData() {
		$user = JFactory::getUser();
		$session = JFactory::getSession();
		$data = new stdClass();
		$inputQid = InsureDIYCIHelper::getInputQid();
		$quotation = array();

		if ($user->get('guest')) {
			if ($inputQid) {
				$this->setError(JText::_("ERR_NO_ACCESS_TO_VIEW"));
				return FALSE;
			}
		} else {
			if ($inputQid) {
				$quotation = InsureDIYCIHelper::getQuotation($inputQid);
				if (empty($quotation) || $quotation['user_id'] != $user->id) {
					$this->setError(JText::_("ERR_NO_ACCESS_TO_VIEW"));
					return FALSE;
				}
				$session->set("ci.quotation_id", $inputQid);
				$session->set("ci.data", $quotation);
			} else {
				$quotation = $session->get("ci.data", array());
			}
			$sessQid = InsureDIYCIHelper::getCurrentQid();
			if (!$sessQid) { // New quote
				$currentUser = MyHelper::getCurrentUser();
				$quotation['dob'] = $currentUser->dob;
				$quotation['gender'] = $currentUser->gender;
				$quotation['monthly_income'] = $currentUser->monthly_salary;
				$quotation['occupation'] = $currentUser->occupation;
				$quotation['country_residence'] = $currentUser->country;
				$quotation['nationality'] = $currentUser->nationality;
				$quotation['marital_status'] = $currentUser->marital_status;
			}
		}

		foreach ($quotation as $k => $v) {
			$data->$k = $v;
		}

		return $data;
	}

	public function getCurrentQuotation() {
		if (!isset($this->_quotation)) {
			$this->_quotation = InsureDIYCIHelper::getQuotation();
		}
		return $this->_quotation;
	}

	// Methods to save each steps
	public function step1save($data) {
		$db = JFactory::getDBO();
		$user = JFactory::getUser();
		$session = JFactory::getSession();

		unset($data['agree_company']);
		unset($data['read_terms']);
		unset($data['read_policy']);
		unset($data['read_personal']);
		unset($data['agree_product']);

		$session->set("ci.cover_length", $data['cover_length']);
		if (!$user->get('guest')) { // logged in
			$data['user_id'] = $user->id;
			$data['email'] = $user->email;
			$data['unique_order_no'] = InsureDIYHelper::getUniqueOrderNo($user->id);
			if (isset($data['id']) && $data['id'] == 0 && !$user->is_firsttime) { // If new quotation and user is already logged in, get the contact information from user and save it in quotation
				$data = array_merge($data, $this->getCurrentUserContacts());
			}
		}

		$data['dob'] = date("Y-m-d", strtotime($data['dob']));
		$data['quote_stage'] = 2; // step 2 now
		$quotation_id = 0;
		if (isset($data['id']) && $data['id'] == 0) {
			$data['created_date'] = JFactory::getDate()->toSql();
			$dataobj = MyHelper::array2jObject($data);
			$db->insertObject($this->_tb_quotation, $dataobj, "id");
			$quotation_id = $dataobj->id;
		} else {
			$dataobj = MyHelper::array2jObject($data);
			$db->updateObject($this->_tb_quotation, $dataobj, "id");
			$quotation_id = $dataobj->id;
		}

		$quotation = InsureDIYCiHelper::getQuotation($quotation_id);
		$session->set("ci.data", $quotation);
		$session->set("ci.protection_needs_analysis", $quotation['protection_needs_analysis']);
		return $dataobj->id;
	}

	public function step2save($data) {
		$db = JFactory::getDBO();
		$quotation_id = InsureDIYCIHelper::getQid();
		$session = JFactory::getSession();
		$user = JFactory::getUser();
		
		$my_saved_data = $data;
		// Contact Details
		$data = array();
		$data['id'] = $quotation_id;
		$data['user_id'] = $user->id;
		$data['email'] = $user->email;
		//$data['unique_order_no'] = InsureDIYHelper::getUniqueOrderNo($user->id);
		if (!$user->is_firsttime) { // If new quotation and user is already logged in, get the contact information from user and save it in quotation
			$data = array_merge($data, $this->getCurrentUserContacts());
		}
		$dataobj = MyHelper::array2jObject($data);
		$db->updateObject($this->_tb_quotation, $dataobj, "id");
		
		// Questionnaire
		$data = $my_saved_data;
		if (count($data['question_id'])) {
			$this->deleteAllAnswers($quotation_id);
			$answers = $data['answer'];
			foreach ($data['question_id'] as $key => $v) {
				$answer = $answers[$key] ? $answers[$key] : 0;
				$data = array("question_id" => $v, "quotation_id" => $quotation_id, "answer" => $answer);
				$dataobj = MyHelper::array2jObject($data);
				$db->insertObject($this->_tb_quotation_questionnaire, $dataobj);
			}
		}
		$this->updateStage($quotation_id, 3);
		$quotation = InsureDIYCiHelper::getQuotation($quotation_id);
		$session->set("ci.data", $quotation);

		return TRUE;
	}

	public function step3save($data) {
		$session = JFactory::getSession();
		$quotation_id = InsureDIYCIHelper::getQid();
		if (isset($data['plans']) && count($data['plans'])) {
			$this->deleteAllPlans($quotation_id); // clear the current data first
			foreach ($data['plans'] as $plan) {
				$this->addplan($plan, $quotation_id);
			}
			$this->updateStage($quotation_id, 4);
			$quotation = InsureDIYCiHelper::getQuotation($quotation_id);
			$session->set("ci.data", $quotation);
			return TRUE;
		}
		return FALSE;
	}

	public function step4save($post) {
		$db = JFactory::getDBO();
		$session = JFactory::getSession();
		$user = JFactory::getUser();
		$quotation_id = InsureDIYCIHelper::getQid();
		$is_firsttime = isset($post["is_firsttime"]) ? $post["is_firsttime"] : FALSE;
		$insurerNameArr = $post['insurer_name'];
		$policyTypeArr = $post['policy_type'];
		$sumInsuredArr = $post['sum_insured'];
		$issuanceDateArr = $post['issuance_date'];
		unset($post['insurer_name']);
		unset($post['policy_type']);
		unset($post['sum_insured']);
		unset($post['issuance_date']);
		$post["contact_expiry_date"] = date("Y-m-d", strtotime($post['contact_expiry_date']));

		/* unset is_firsttime field, not needed for Quotation */
		unset($post["is_firsttime"]);
		//$post['unique_order_no'] = InsureDIYHelper::getUniqueOrderNo($user->id);
		$objData = MyHelper::array2object($post);
		$objData->id = $quotation_id;
		$db->updateObject($this->_tb_quotation, $objData, "id");

		/* if this customer is first time , insert contact details into profile data */
		if ($is_firsttime) {
			$fieldTmp = array();
			$fieldTmp[] = " name = " . $db->Quote($post['contact_firstname'], false);
			$fieldTmp[] = " lastname = " . $db->Quote($post['contact_lastname'], false);
			$fieldTmp[] = " country_code = " . $db->Quote($post['contact_country_code'], false);
			$fieldTmp[] = " contact_no = " . $db->Quote($post['contact_contact_no'], false);
			$fieldTmp[] = " room_no = " . $db->Quote($post['contact_room_no'], false);
			$fieldTmp[] = " floor_no = " . $db->Quote($post['contact_floor_no'], false);
			$fieldTmp[] = " block_no = " . $db->Quote($post['contact_block_no'], false);
			$fieldTmp[] = " building_name = " . $db->Quote($post['contact_building_name'], false);
			$fieldTmp[] = " street_no = " . $db->Quote($post['contact_street_name'], false);
			$fieldTmp[] = " district = " . $db->Quote($post['contact_district_name'], false);
			$fieldTmp[] = " postal_code = " . $db->Quote($post['contact_postal_code'], false);
			$fieldTmp[] = " country = " . $db->Quote($post['contact_country'], false);
			$fieldTmp[] = " is_firsttime = " . $db->Quote(0, false);
			$fieldTmp[] = " gender = " . $db->Quote($post['gender'], false);
			$fieldTmp[] = " marital_status = " . $db->Quote($post['marital_status'], false);
			$fieldTmp[] = " dob = " . $db->Quote($post['dob'], false);
			$fieldTmp[] = " monthly_salary = " . $db->Quote($post['monthly_income'], false);
			$fieldTmp[] = " occupation = " . $db->Quote($post['occupation'], false);

			$query = " UPDATE #__users SET " . implode(",", $fieldTmp) . " WHERE id = '" . $user->id . "' ";
			$db->setQuery($query);
			$db->query();
		}

		/* Clear Ex plans before saving */
		$db->setQuery(" DELETE FROM " . $this->_tb_quotation_ex_insurance . " WHERE quotation_id = '$quotation_id' ");
		$db->query();

		/* existing insurance - save multiple */
		foreach ($insurerNameArr as $key => $val) {
			$policy_type = $policyTypeArr[$key];
			$sum_insured = $sumInsuredArr[$key];
			$issuance_date = date("Y-m-d", strtotime($issuanceDateArr[$key]));

			if (strlen($val) > 0 || strlen($policy_type) > 0 || strlen($sum_insured) > 0 || strlen($issuance_date) > 0) {
				$query = " INSERT INTO " . $this->_tb_quotation_ex_insurance . " (`quotation_id`,`insurer_name`,`policy_type`,`sum_insured`,`issuance_date`) VALUES('$quotation_id','$val','$policy_type','$sum_insured','$issuance_date');";
				$db->setQuery($query);
				$db->query();
			}
		}

		$db->setQuery(" UPDATE " . $this->_tb_quotation . " SET quote_stage = 5 WHERE id = '$quotation_id' ");
		$db->query();

		$quotation = InsureDIYCiHelper::getQuotation($quotation_id);
		$session->set("ci.data", $quotation);
		return true;
	}

	public function extraInfoSave($post) {
		$db = JFactory::getDBO();
		$session = JFactory::getSession();

		$quotation_id = InsureDIYCIHelper::getQid();

		$propertyPurchaseArr = $post['property_purchase'];
		$propertyPriceArr = $post['property_price'];
		$propertyMortgageArr = $post['property_mortgage'];
		$propertyValueArr = $post['property_value'];

		$carModelArr = $post['car_model'];

		unset($post['property_purchase']);
		unset($post['property_price']);
		unset($post['property_mortgage']);
		unset($post['property_value']);
		unset($post['car_model']);
		$arrTmp = array();

		foreach ($post as $key => $val) {
			$arrTmp[] = '`' . $key . '` = ' . $db->Quote(addslashes($val), false);
		}

		$query = " UPDATE " . $this->_tb_quotation . " SET " . implode(" , ", $arrTmp) . " WHERE id = '$quotation_id' ; ";
		$db->setQuery($query);
		$db->query();

		/* Cars */
		$this->deleteAllCars($quotation_id); // need to clear first
		if ($post['other_own_car']) {
			foreach ($carModelArr as $key => $val) :
				$car_model = $carModelArr[$key];
				$query = " INSERT INTO " . $this->_tb_quotation_cars . " (`quotation_id`,`car_model`) VALUES('$quotation_id','$car_model');";
				$db->setQuery($query);
				$db->query();
			endforeach;
		}
		/* Property */
		$this->deleteAllProperties($quotation_id); // need to clear first
		if ($post['other_own_property']) {
			foreach ($propertyPurchaseArr as $key => $val) :
				$property_purchase = $propertyPurchaseArr[$key];
				$property_price = $propertyPriceArr[$key];
				$property_mortgage = $propertyMortgageArr[$key];
				$property_value = $propertyValueArr[$key];

				$query = " INSERT INTO " . $this->_tb_quotation_property . " (`quotation_id`,`property_purchase`,`property_price`,`property_mortgage`,`property_value`) "
						. " VALUES('$quotation_id','$property_purchase','$property_price','$property_mortgage','$property_value'); ";
				$db->setQuery($query);
				$db->query();
			endforeach;
		}
		$quotation = InsureDIYCIHelper::getQuotation($quotation_id);
		$session->set("ci.data", $quotation);
		return true;
	}

	public function step6save($post) {
		$session = JFactory::getSession();
		$db = JFactory::getDBO();

		$quotation_id = InsureDIYCIHelper::getQid();
		$session->set("ci.maildata.contact_no." . $quotation_id, $post['country_code'] . " " . $post['phone_number']);

		if ($post['meet_type'] == 1) { // predetermine location
			$post['meet_date'] = '0000-00-00';
			$post['meet_time'] = '';
			$post['meet_contact_no'] = '';
			$post['meet_address'] = '';

			// data for email
			$session->set("ci.maildata.address." . $quotation_id, $post['pre_location_id']);
			$session->set("ci.maildata.meet_time." . $quotation_id, "9am to 6pm");
			$start_date = $session->get("ci.maildata.sign_start." . $quotation_id);
			$end_date = $session->get("ci.maildata.sign_end." . $quotation_id);
			$session->set("ci.maildata.meet_date." . $quotation_id, $start_date . " to " . $end_date);
			unset($post['time1']);
			unset($post['time2']);
			unset($post['time3']);
			unset($post['country_code']);
			unset($post['phone_number']);
		} else { // 2, set appointment
			$post['meet_time'] = $post['time1'] . ':' . $post['time2'] . ' ' . $post['time3'];
			$post['meet_contact_no'] = $post['country_code'] . ' ' . $post['phone_number'];
			$post['pre_location_id'] = '';
			$post['meet_date'] = date("Y-m-d", strtotime($post['meet_date']));

			// data for email
			$session->set("ci.maildata.meet_time." . $quotation_id, $post['meet_time']);
			$session->set("ci.maildata.meet_date." . $quotation_id, $post['meet_date']);
			$session->set("ci.maildata.address." . $quotation_id, $post['meet_address']);

			unset($post['time1']);
			unset($post['time2']);
			unset($post['time3']);
			unset($post['country_code']);
			unset($post['phone_number']);
		}

		$post['quote_status'] = 1; // Quote Status is Complete
		$arrTmp = array();
		foreach ($post as $key => $val) :
			$arrTmp[] = '`' . $key . '` = ' . $db->Quote(addslashes($val), false);
		endforeach;
		$query = " UPDATE " . $this->_tb_quotation . " SET " . implode(" , ", $arrTmp) . " WHERE id = '$quotation_id' ; ";
		$db->setQuery($query);
		return $db->query();
	}

	// Some miscellenous methods
	public function addplan($planstr, $quotation_id) {
		$db = JFactory::getDBO();

		// TO DO: Only need to post plan_index_code. 
		$tmp = explode('|', $planstr);
		$plan_index_code = $tmp[1];
		$plan = $this->getPlan($plan_index_code);
		$obj = new stdClass();
		$obj->quotation_id = $quotation_id;
		$obj->plan_name = $plan->plan_name;
		$obj->plan_index_code = $plan->plan_index_code;
		$obj->sum_insured = $plan->sum_insured;
		$obj->price = $plan->price;
		$obj->ref_points = $plan->ref_points;
		$obj->pur_points = $plan->pur_points;

		if (strtolower(substr($obj->plan_index_code, 0, 3)) == "aia") { // if AIA get policy number.
			$year_code = substr($obj->plan_index_code, 3, 5);
			$obj->policy_number = $this->getAPolicyNo($year_code);
		}
		$db->insertObject($this->_tb_quotation_plan, $obj, "id");
	}

	public function getPlan($plan_index_code) {
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from($this->_tb_plan)
				->where("plan_index_code = " . $db->quote($plan_index_code));
		return $db->setQuery($query)->loadObject();
	}

	public function deleteAllPlans($quotation_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->delete($this->_tb_quotation_plan)
				->where("quotation_id = " . $quotation_id);
		return $db->setQuery($query)->execute();
	}

	public function deleteAllAnswers($quotation_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->delete($this->_tb_quotation_questionnaire)
				->where("quotation_id = " . $quotation_id);
		return $db->setQuery($query)->execute();
	}

	public function deleteAllCars($quotation_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->delete($this->_tb_quotation_cars)
				->where("quotation_id = " . $quotation_id);
		return $db->setQuery($query)->execute();
	}

	public function deleteAllProperties($quotation_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->delete($this->_tb_quotation_property)
				->where("quotation_id = " . $quotation_id);
		return $db->setQuery($query)->execute();
	}

	public function updateStage($quotation_id, $stage) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->update($this->_tb_quotation)
				->set("quote_stage = " . $stage)
				->where("id = " . $quotation_id);
		return $db->setQuery($query)->execute();
	}

	public function getCurrentUser() {
		$db = JFactory::getDBO();
		$user = JFactory::getUser();
		if ($user->guest) {
			return;
		}
		$query = " SELECT * FROM #__users WHERE id = " . $db->Quote($user->id, false) . " LIMIT 1 ";
		return $db->setQuery($query)->loadObject();
	}

	public function getCurrentUserContacts() {
		$fields = array(
			"contact_firstname" => "name", "contact_lastname" => "lastname", "contact_country_code" => "country_code",
			"contact_contact_no" => "contact_no", "contact_room_no" => "room_no", "contact_floor_no" => "floor_no",
			"contact_block_no" => "block_no", "contact_building_name" => "building_name", "contact_street_name" => "street_no",
			"contact_district_name" => "district", "contact_postalcode" => "postal_code", "contact_country" => "country",
//			"dep_young_children" => "dep_young_children", "dep_kids" => "dep_kids", "dep_teenagers" => "dep_teenagers", "dep_young_adult" => "dep_young_adult",
//			"dep_elderly" => "dep_elderly"
				//"gender" => "gender", "marital_status" => "marital_status", "dob" => "dob", "monthly_income" => "monthly_salary", "occupation" => "occupation"
		);
		$userData = $this->getCurrentUser();
		$tmp = array();
		foreach ($fields as $key => $field) {
			$tmp[$key] = $userData->$field;
		}
		$tmp['other_dependant_no'] = (int) $userData->dep_young_children + (int) $userData->dep_kids + (int) $userData->dep_teenagers + (int) $userData->dep_young_adults + (int) $userData->dep_elderly;
		unset($tmp['dep_young_children']);
		return $tmp;
	}

	public function getQuestionnaires() {
		$db = JFactory::getDBO();
		$quotation_id = InsureDIYCIHelper::getQid();
		$gender = $db->setQuery($db->getQuery(TRUE)->select("gender")->from($this->_tb_quotation)->where("id = " . $db->quote($quotation_id)))->loadResult();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from($this->_tb_questionnaire)
				->where("gender = " . $db->quote($gender))->where("state = 1")
				->order("ordering ASC");
		return $db->setQuery($query)->loadObjectList();
	}

	public function getPolicyPlans() {
		$db = JFactory::getDBO();
		$data = $this->getCurrentQuotation();
		$where = array(
			' p.gender = ' . $db->Quote($data['gender']),
			' p.age = ' . $db->Quote($data['age']),
			' p.sum_insured = ' . $db->Quote($data['cover_amt']),
			' p.is_smoking = ' . $db->Quote($data['has_used_tobacco']),
			' p.`state` = 1 ', ' p.year_code = ' . $db->Quote($data['cover_length']),
			//'(p.nationality =' . $db->Quote($data['nationality']) . ' OR p.nationality = ' . $db->Quote("XX") . ')',
			'(p.residency =' . $db->Quote($data['country_residence']) . ' OR p.residency = ' . $db->Quote("XX") . ')'
		);
		$query = $db->getQuery(TRUE)
				->select("p.*, c.logo")
				->from($this->_tb_plan . " AS p")
				->leftJoin($this->_tb_company . " AS c ON c.insurer_code = p.insurer_code")
				->where($where)
				//->group("insurer_code")
				->order("p.price ASC");
		return $db->setQuery($query, 0, 6)->loadObjectList();
	}

	public function getFuturePolicyPlans() {
		$db = JFactory::getDBO();
		$data = $this->getCurrentQuotation();
		$yearcode = $data['cover_length'];
		$length_cover = $this->getLengthCover($yearcode);
		$ages = $this->getAgesForGraph($length_cover);
		$agestr = (count($ages)) ? implode(",", $ages) : "0";
		$nationality = isset($data['nationality']) ? $data['nationality'] : "XX";
		$residency = isset($data['country_residence']) ? $data['country_residence'] : "XX";
		$nationalities = $db->quote($nationality) . ", 'XX'";
		$residencies = $db->quote($residency) . ", 'XX'";

		$conditions = array('p.gender = ' . $db->Quote($data['gender']), 'p.sum_insured = ' . $db->Quote($data['cover_amt']), 'p.is_smoking = ' . $db->Quote($data['has_used_tobacco']),
			'p.state = 1 ', 'p.year_code = ' . $db->quote($yearcode), 'p.age in (' . $agestr . ')', 'p.nationality in (' . $nationalities . ')', 'p.residency in (' . $residencies . ')');
		$query = $db->getQuery(TRUE)
				->select('p.age, p.price, c.company_name, p.insurer_code, c.color_code, p.plan_index_code')
				->from($this->_tb_plan . " AS p")
				->innerJoin($this->_tb_company . " AS c ON c.insurer_code = p.insurer_code")
				->where($conditions)
				->order("p.age ASC");
		return $db->setQuery($query)->loadObjectList();
	}

	public function getAgesForGraph($length_cover) {
		$session = JFactory::getSession();
		$data = $session->get("ci.data");
		$current_age = isset($data['age']) ? $data['age'] : 18;

		if (60 - $current_age > $length_cover) {
			$ages = range($current_age, 60, $length_cover);
		} else {
			$ages = array();
		}
		return $ages;
	}

	public function getLengthCover($yearcode = "") {
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
				->select("length_cover")
				->from("#__insure_ci_years")
				->where("year_code=" . $db->quote($yearcode));
		return $db->setQuery($query)->loadResult();
	}

	public function getMyPlans() {
		$db = JFactory::getDBO();
		$quotation_id = InsureDIYCIHelper::getQid();

		$query = $db->getQuery(TRUE)
				->select("p.insurer_code, p.price, p.plan_index_code, p.plan_name, c.company_name")
				->from($this->_tb_quotation_plan . " AS px")
				->leftJoin($this->_tb_plan . " AS p ON p.plan_index_code = px.plan_index_code AND p.sum_insured = px.sum_insured")
				->leftJoin($this->_tb_company . " AS c ON c.insurer_code = p.insurer_code")
				->where("px.quotation_id = '$quotation_id' ");
//
//		$query = " SELECT p.insurer_code, p.price, p.plan_index_code, p.plan_name FROM " . $this->_tb_quotation_plan . " AS px "
//				. " LEFT JOIN " . $this->_tb_plan . " AS p ON p.plan_index_code = px.plan_index_code AND p.sum_insured = px.sum_insured"
//				. " WHERE px.quotation_id = '$quotation_id' ";

		$db->setQuery($query);
		$rows = $db->loadObjectList();

		return $rows;
	}

	/**
	 * Method to get My Info on Stage 6
	 *
	 * @param   integer	The id of the object to get.
	 *
	 * @return  mixed  Object on success, false on failure.
	 */
	public function getMyInfo() {
		$quotation_id = InsureDIYCIHelper::getQid();
		return $this->getQuotation($quotation_id);
	}

	public function getQuotation($quotation_id, $user_id = FALSE) {
		if (!$quotation_id) {
			return FALSE;
		}
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE);

		$query->select("*");
		$query->from($this->_tb_quotation);
		$query->where("id = " . $quotation_id);
		if ($user_id) {
			$query->where("user_id = " . (int) $user_id);
		}
		$db->setQuery($query);
		return $db->loadObject();
	}

	public function updateQuotation($quotation_id, $data) {
		$db = JFactory::getDbo();
		$dataobj = is_object($data) ? $data : MyHelper::array2jObject($data);
		$dataobj->id = $quotation_id;
		$session = JFactory::getSession();
		$sessData = $session->get("ci.data", FALSE);
		if ($sessData) {
			foreach ($data as $key => $value) {
				$sessData[$key] = $value;
			}
		} else {
			$sessData = $this->getQuotation($quotation_id);
		}
		$session->set("ci.data", $sessData);
		return $db->updateObject($this->_tb_quotation, $dataobj, "id");
	}

	public function getPDPayment() {
		$app = JFactory::getApplication();
		$params = $app->getParams();
		$quotation_id = InsureDIYCIHelper::getQid();
		$config_com_insureDIY	= JComponentHelper::getParams('com_insurediy');
		$data = new stdClass();
		if ($this->hasPayment($quotation_id)) {
			$data->hasPayment = TRUE;

			$quotation = $this->getTable();
			if (!$quotation->load($quotation_id)) {
				return FALSE;
			}
			$month = $params->get('premium_month', 1);
			$data->action = $params->get('pdUrl', "https://test.paydollar.com/b2cDemo/eng/payment/payForm.jsp");
			$data->mpsMode = $params->get('mpsMode', 'NIL');
			$data->currCode = $params->get('currCode', '344');
			$data->lang = $params->get('lang', 'E');
			$data->merchantId = $params->get('merchantId', FALSE);
			$data->orderRef = $quotation->unique_order_no;
			$data->amount = $this->getAmount($quotation_id, $month);
			$data->cancelUrl = JURI::base() . 'index.php?option=com_insurediyci&view=form&layout=canceled&Itemid=157';
			$data->failUrl = JURI::base() . 'index.php?option=com_insurediyci&view=form&layout=error&Itemid=157';
			$data->successUrl = JURI::base() . 'index.php?option=com_insurediyci&view=form&Itemid=157&quotation_id=' . $quotation_id;
			$data->payType = "N";
			$data->payMethod = "ALL";
			$data->secureHash = $config_com_insureDIY->get('secureHash', '');
			$data->remark = "ci"; // Using remark to check for different type of insurances. Do not use this for other purpose.

			if (!$data->merchantId) {
				return FALSE;
			}
		} else {
			$data->hasPayment = FALSE;
		}
		return $data;
	}

	private function hasPayment($quotation_id) {
		$quotation = $this->getQuotation($quotation_id);
		return ($quotation->payment_status == "P" || $quotation->payment_status == "N" || $quotation->payment_status == "X") && $quotation->quote_stage == "6" && $quotation->unique_order_no != "";
	}

	private function getAmount($quotation_id, $month) {
		$query = " SELECT price FROM " . $this->_tb_quotation_plan . " WHERE quotation_id = '$quotation_id' ";
		$db = JFactory::getDbo();
		$db->setQuery($query);
		$rows = $db->loadObjectList();

		$amount = 0.00;
		foreach ($rows as $r):
			$amount += (float) $r->price;
		endforeach;

		return $amount * (float) $month;
	}

	public function getAPolicyNo($year_code) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
						->select("policy_number")
						->from($this->_tb_policy_number)
						->where("state = 1")->where("plan_name = " . $db->quote($year_code));
		$db->setQuery($query);
		$policyno = $db->loadResult();
		if ($policyno) {
			$query = $db->getQuery(TRUE)
					->update($this->_tb_policy_number)
					->set("state = 0")
					->where("policy_number = " . $db->quote($policyno));
			$db->setQuery($query);
			$result = $db->execute();
		}
		if ($result && $policyno) {
			return $policyno;
		} else {
			return "";
		}
	}

	public function saveConfirmationDetail($data) {
		$db = JFactory::getDBO();
		$quotation_id = InsureDIYCIHelper::getQid();
		$conditions = array();
		$conditions[] = "quote_stage = 6";
		$conditions[] = "payment_status = " . $db->quote("P");
		$conditions[] = "declare_have_you_replaced = " . $db->quote($data['declare_have_you_replaced']);
		$conditions[] = "declare_do_you_intend_to_replace = " . $db->quote($data['declare_do_you_intend_to_replace']);
		$query = $db->getQuery(TRUE)
				->update($this->_tb_quotation)
				->set($conditions, ",")
				->where("id = " . $db->quote($quotation_id));
		return $db->setQuery($query)->execute();
	}

	/**
	 * Method to upload file for Quotation only including on Step 6 (Final Step) and Step 5a : with Sum Insured more than 5 
	 *
	 * @param   integer	The id of the object data.
	 *
	 * @return  boolean True or False.
	 */
	public function savePhotoIdentityFileUpload($file, $path, $db_field) {
		JLoader::import('joomla.filesystem.folder');
		JLoader::import('joomla.filesystem.file');
		$db = JFactory::getDBO();

		$quotation_id = InsureDIYCIHelper::getQid();

		if (!$quotation_id) {
			return false;
		}

		$name = $file['name'];
		$type = $file['type'];
		$tmp_name = $file['tmp_name'];
		$error = $file['error'];
		$size = $file['size'];

		$uidStr = '' . JFactory::getUser()->id;
		if (strlen($uidStr) % 2) {
			$uidStr = '0' . $uidStr; //prepend 0 to odd num
		}
		$deepPath = "";
		//create sub-folder for every 2 digits
		foreach (str_split($uidStr, 2) as $dir) {
			$deepPath .= DS . $dir;
			if (!JFolder::exists(JPATH_BASE . DS . $path . DS . $deepPath)) {
				JFolder::create(JPATH_BASE . DS . $path . DS . $deepPath, 0777);
			}
		}

		if ($size > 0) {
			$filename = md5(time()) . rand(100, 999) . '-' . $name;
			if (move_uploaded_file($tmp_name, JPATH_BASE . DS . $path . DS . $deepPath . DS . $filename)) {
				$nameToStore = $deepPath . DS . $filename . "|" . $name;

				$query = " SELECT " . $db_field . " FROM " . $this->_tb_quotation . " WHERE id = '$quotation_id' ";
				$db->setQuery($query);

				if ($prev_entry = $db->loadResult()) {
					$prev_entry = explode("|", $prev_entry);
					$prev_filename = (isset($prev_entry[0])) ? $prev_entry[0] : FALSE;
					if ($prev_filename) {
						unlink(JPATH_BASE . '/' . $path . '/' . $prev_filename);
					}
				}

				$query = " UPDATE " . $this->_tb_quotation . " SET " . $db_field . " = " . $db->quote($nameToStore) . " WHERE id = '$quotation_id' ";
				$db->setQuery($query);
				$db->query();
			}
		}

		return true;
	}

	public function getPlans($quotation_id) {
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from($this->_tb_quotation_plan)
				->where("quotation_id = " . $db->quote($quotation_id));
		return $db->setQuery($query)->loadObjectList();
	}

	public function checkSumInsuredQuotationTotal() {
		$db = JFactory::getDBO();
		$quotation_id = InsureDIYCIHelper::getQid();
		/* Selected Plans */
		$query = $db->getQuery(TRUE)
				->select("sum_insured")
				->from($this->_tb_quotation_plan)
				->where("quotation_id = " . $db->quote($quotation_id));
		$plans = $db->setQuery($query)->loadObjectList();
		$total_sum_insured = 0;
		foreach ($plans as $r) {
			$total_sum_insured += (int) $r->sum_insured;
		}
		/* Ex Insurances */
		$query->clear();
		$query->select("sum_insured")
				->from($this->_tb_quotation_ex_insurance)
				->where("quotation_id = " . $db->quote($quotation_id));
		$ex_plans = $db->setQuery($query)->loadObjectList();
		foreach ($ex_plans as $r) {
			$total_sum_insured += intval(str_replace(",", "", $r->sum_insured));
		}
		return $total_sum_insured;
	}

	public function savePDFPaths($quotation_id, $array = array(), $client = TRUE) {
		if (empty($array)) {
			return;
		}
		$toJson = new stdClass();
		foreach ($array as $k => $v) {
			if (is_array($v)) {
				$toJson->$k = implode(" | ", $v);
			} else {
				$toJson->$k = $v;
			}
		}
		$db = JFactory::getDBO();
		if ($client) {
			$query = " UPDATE " . $this->_tb_quotation . " SET `file_json_pdfpath` = " . $db->Quote(json_encode($toJson), false) . " WHERE id = '$quotation_id' ";
		} else {
			$query = " UPDATE " . $this->_tb_quotation . " SET `file_json_pdfpath2` = " . $db->Quote(json_encode($toJson), false) . " WHERE id = '$quotation_id' ";
		}
		$db->setQuery($query);
		$db->query();
	}

	// Mail Stuffs - start
	public function sendEmails($pdfarrs, $client = TRUE) {
		$mailer = JFactory::getMailer();
		$params = JComponentHelper::getParams('com_insurediyci');
		$myInfo = $this->getMyInfo();
		$data = $this->getMailData($myInfo);
		$legit = $this->checkBlacklist($myInfo);

		$emails = $this->getMails($params, $legit);
		$emailbodies = $this->getEmailBodies($params, $data);
		$mode = TRUE;
		foreach ($pdfarrs as $pdfarr) {
			$attachments = $this->prepareAttachments($pdfarr, $myInfo->id);
			if ($client) {
				$mailer->ClearAttachments();
				$mailer->ClearAllRecipients();
				$mailer->sendMail($emails['from'], $emails['fromName'], $emails['recipient'], $emails['subject'], $emailbodies['ce_customer'], $mode, NULL, $emails['bcc'], $attachments);
			} else {
				$mailer->ClearAttachments();
				$mailer->ClearAllRecipients();
				//$mailer->sendMail($emails['from'], $emails['fromName'], $emails['service_recipient'], $emails['subject'], $emailbodies['ce_service'], $mode, NULL, $emails['bcc'], $attachments);
				$mailer->sendMail($emails['from'], $emails['fromName'], $emails['service_recipient'], $emails['subject'], $emailbodies['ce_service'], $mode, NULL, array(), $attachments);
			}
		}
	}

	public function prepareAttachments($pdfarr, $id) {
		$temp = array();
		foreach ($pdfarr as $pdf) {
			$temp[] = MyHelper::getDeepPath(CI_PDF_SAVE_PATH, $id) . $pdf;
		}
		return $temp;
	}

	public function getEmailBodies($params, $data) {
		$array = array();
		$array['cover_letter'] = InsureDIYHelper::replaceVariables($params->get("clformat"), $data);
		$array['ce_customer'] = InsureDIYHelper::replaceVariables($params->get("cecformat"), $data);
		$array['ce_service'] = InsureDIYHelper::replaceVariables($params->get("cesformat"), $data);
		return $array;
	}

	public function getMails($params, $legit) {
		$confg = JFactory::getConfig();
		$user = JFactory::getUser();
		$array = array();
		if ($legit) {
			$array['recipient'] = $user->email;
			$sales_email = $params->get('sales_email');
			$array['bcc'] = array("0" => "zan@ifoundries.com", "1" => $sales_email, "2" => "rylaicf77@gmail.com");
			$array['subject'] = 'InsureDIY - Application Form';
		} else {
			$array['recipient'] = $params->get('admin_email');
			$sales_email = $params->get('sales_email');
			$array['bcc'] = array("0" => "zan@ifoundries.com", "1" => $sales_email, "2" => "rylaicf77@gmail.com");
			$array['subject'] = 'InsureDIY - Application Form[Blacklist]';
		}
		$array['service_recipient'] = $params->get('service_email', "zan@ifoundries.com");
		$array['from'] = $confg->get("mailfrom");
		$array['fromName'] = $confg->get("fromname");
		return $array;
	}

	public function getMailData($myInfo) {
		$session = JFactory::getSession();
		$form_list = array();
		$form_list[] = "Application form";
		$form_list[] = "Direct debit form";

		$data = array();
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$query->select("plan_index_code");
		$query->from($this->_tb_quotation_plan);
		$query->where("quotation_id = " . $myInfo->id);
		$db->setQuery($query);
		$plans = $db->loadAssocList();
		if ($this->checkSumInsuredQuotationTotal() >= InsureDIYCIHelper::getReachAmount()) {
			$form_list[] = "Largeamount questionnaire";
		}
		foreach ($plans as $plan) {
			if (strpos(strtolower($plan['plan_index_code']), "sun") !== FALSE) {
				$form_list[] = "Financial needs analysis form";
				break;
			}
		}

		$attach_form_list = "<ol><li>" . implode("</li><li>", $form_list) . "</li></ol>";

		$data['first_name'] = $myInfo->contact_firstname;
		$data['last_name'] = $myInfo->contact_lastname;
		$data['order_no'] = $myInfo->unique_order_no;
		$data['address'] = $session->get("ci.maildata.address." . $myInfo->id, FALSE);
		$data['date_of_appointment'] = $session->get("ci.maildata.meet_date." . $myInfo->id, FALSE);
		$data['time_of_appointment'] = $session->get("ci.maildata.meet_time." . $myInfo->id, FALSE);
		$data['contact_number'] = $session->get("ci.maildata.contact_no." . $myInfo->id, FALSE);
		$data['attach_form_list'] = $attach_form_list;

		return $data;
	}

	public function checkBlacklist($data) {
		$name = $data->contact_firstname . " " . $data->contact_lastname;
		$nationality = $data->nationality;
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$query->select("count(*)");
		$query->from("#__insure_blacklist");
		$query->where("UPPER(name) = UPPER(" . $db->quote($name) . ") AND nationality = " . $db->quote($nationality));
		$db->setQuery($query);
		$result = $db->loadResult();
		if ($result > 0) {
			return FALSE;
		}
		return TRUE;
	}

	// Mail Stuffs - end

	public function getPdfData($quotation_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from($this->_tb_quotation)
				->where("id=" . $quotation_id);
		$db->setQuery($query);
		$result = $db->loadObject();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from($this->_tb_quotation_ex_insurance)
				->where("quotation_id=" . $quotation_id);
		$db->setQuery($query);
		$existing_insurances = $db->loadObjectList();

		$query = $db->getQuery(TRUE)
				->select("*")
				->from($this->_tb_quotation_property)
				->where("quotation_id=" . $quotation_id)
				->order("id");
		$db->setQuery($query);
		$properties = $db->loadObjectList();

		$query = $db->getQuery(TRUE)
				->select("*")
				->from($this->_tb_quotation_cars)
				->where("quotation_id=" . $quotation_id);
		$db->setQuery($query, 0, 3);

		$cars = $db->loadObjectList();
		// Prepare data before return
		$result->hasLargeSum = $this->checkSumInsuredQuotationTotal() >= 5000000;
		$result->contact_firstname = strtoupper($result->contact_firstname);
		$result->contact_lastname = strtoupper($result->contact_lastname);
		$result->country_residence = InsureDIYHelper::getCountryOfResidence($result->country_residence);
		$result->nationality = InsureDIYHelper::getNationality($result->nationality);

		$result->monthly_income_ckbox = $result->monthly_income;
		$result->monthly_income = JText::_($result->monthly_income);

		switch ($result->contact_identity_type) {
			case 'chid' :
				$result->contact_identity_no2 = $result->contact_identity_no;
				unset($result->contact_identity_no);

				$tmp = explode('-', $result->contact_expiry_date);
				$result->contact_expiry_date_y = $tmp[0];
				$result->contact_expiry_date_m = $tmp[1];
				$result->contact_expiry_date_d = $tmp[2];
				break;
			case 'passport':
				$result->contact_identity_no3 = $result->contact_identity_no;
				unset($result->contact_identity_no);

				$tmp = explode('-', $result->contact_expiry_date);
				$result->contact_expiry_date_y2 = $tmp[0];
				$result->contact_expiry_date_m2 = $tmp[1];
				$result->contact_expiry_date_d2 = $tmp[2];

				$result->bank_acct_holder_type = "p";
				break;
			case "hkid":
				$result->bank_acct_holder_type = "i";
			default:
				break;
		}
		$result->bank_acct_holder_doc_no = $result->contact_identity_no;

		$dob = explode("-", $result->dob);
		$result->dob_y = $dob[0];
		$result->dob_m = $dob[1];
		$result->dob_d = $dob[2];
		$result->age = MyHelper::getAge($result->dob);
		$result->contact_telephone = (($result->contact_country_code) ? '(' . $result->contact_country_code . ') ' : '') . $result->contact_contact_no;

		$result->job_nature = JText::_($result->job_nature);
		$result->curreny = "hkd";
		$result->mode = "monthly";
		$result->source = "savings";
		$result->purpose = "protection";
		$result->cover_amt = "HK$" . $result->cover_amt;

		$result->contact_lang = "english";

		$result->height_unit = "cm";
		$result->weight_unit = "kg";
		$result->has_used_drugs = "1";

		$result->direct_promotion = "1";
		$result->full_name = $result->contact_firstname . " " . $result->contact_lastname;
		$result->relationship = "applicant";

		foreach ($existing_insurances as $k => $existing_insurance) {
			$key = $k + 1;
			$insurer_key = "ex_insurer_name_" . $key;
			$insured_key = "ex_insured_name_" . $key;
			$sum_key = "ex_sum_insured_" . $key;
			$date_key = "ex_issuance_date_" . $key;

			$result->$insurer_key = $existing_insurance->insurer_name;
			$result->$insured_key = $result->full_name;
			$result->$sum_key = $existing_insurance->sum_insured;
			$result->$date_key = $existing_insurance->issuance_date;
		}

		$year = (int) date("Y", strtotime($result->created_date));
		$result->lastyear = $year - 1;
		$result->last2year = $year - 2;
		$result->last3year = $year - 3;
		foreach ($properties as $k => $property) {
			$key = $k + 1;
			$property_purchase_key = "property_purchase_" . $key;
			$property_price_key = "property_price_" . $key;
			$property_mortgage_key = "property_mortgage_" . $key;
			$property_value_key = "property_value_" . $key;
			$result->$property_purchase_key = $property->property_purchase;
			$result->$property_price_key = $property->property_price;
			$result->$property_mortgage_key = $property->property_mortgage;
			$result->$property_value_key = $property->property_value;
		}

		$result->no_of_cars = count($cars);

		foreach ($cars as $k => $car) {
			$key = $k + 1;
			$model_key = "car_model_" . $key;
			$result->$model_key = $car->car_model;
		}

		$result->contact_country = ($result->contact_country == "HK") ? $result->contact_country : "OTH";

		$result->fna = "0"; // SUN only
		$result->owner_type = "0";
		return $result;
	}

	public function createPoints($quotation_id) {
		$user = JFactory::getUser();
		$quotation = InsureDIYCIHelper::getQuotation($quotation_id);
		$plans = $quotation['selected_plans'];
		$total_pur_points = 0;
		$total_ref_points = 0;
		foreach ($plans as $plan) {
			$total_pur_points += $plan->pur_points;
			$total_ref_points += $plan->ref_points;
		}
		if (InsureDIYHelper::checkReferral($quotation['referred_by'])) {
			InsureDIYHelper::updateReferredBy($quotation['referred_by']);
			InsureDIYHelper::createReferral($quotation['referred_by'], "ci." . $quotation_id, $quotation['unique_order_no'], $total_ref_points, sprintf(JText::_("REFERRAL_POINT_RECORD_DESCRIPTION"), $user->email));
		}
		$desc = sprintf(JText::_("PURCHASE_POINT_RECORD_DESCRIPTION"), $quotation['unique_order_no']);
		$percent = InsureDIYHelper::getPromoCode($quotation['promo_code']);
		if ($percent) {
			$total_pur_points = $total_pur_points * (100 + $percent) / 100;
			InsureDIYHelper::usePromoCode($quotation['promo_code']);
			$desc .= ". With promo code " . $quotation['promo_code'];
		}
		InsureDIYHelper::createPoint($user->id, $total_pur_points, "ci." . $quotation_id, $quotation['unique_order_no'], "", $desc, 1);
		return TRUE;
	}

	public function createPolicyRecord($quotation_id) {
		$db = JFactory::getDbo();
		$quotation = InsureDIYCIHelper::getQuotation($quotation_id);
		foreach ($quotation['selected_plans'] as $v) {
			$obj = new stdClass();
			$obj->user_id = $quotation['user_id'];
			$obj->type = 2; // ci is 2.
			$obj->insurer = $v->insurer_code;
			$obj->currency = 344; // HK$
			$obj->cover_amt = $v->sum_insured;
			$obj->status = 2; // Set as pending. 1 => active, 0 => inactive, 2 => pending
			$obj->quotation_id = $quotation->id;
			$db->insertObject("#__insure_policies", $obj);
		}
	}

	public function back($quotation_id) {
		$db = JFactory::getDBO();
		$user = JFactory::getUser();
		$current_stage = InsureDIYCIHelper::getCurrentStage($quotation_id);
		$backables = array(2 => 1, 3 => 2, 4 => 3, 5 => 4);
		$backstage = (in_array($current_stage, array_keys($backables))) ? $backables[$current_stage] : FALSE;
		if ($current_stage == 6) {
			$backstage = ($this->checkSumInsuredQuotationTotal() >= InsureDIYCIHelper::getReachAmount()) ? 5 : 4;
		}
		if ($backstage && !$user->get('guest')) {
			$query = $db->getQuery(TRUE)
					->update($this->_tb_quotation)
					->set("quote_stage = " . $backstage)
					->where("id = " . $quotation_id)
					->where("quote_stage > 1")
					->where("user_id = " . $user->id);
			$result = $db->setQuery($query)->execute();
		}

		return $backstage;
	}

}
