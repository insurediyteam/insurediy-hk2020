<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_insurediyci
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

//JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . '/tables');

/**
 *  Component Model for a Weblink record
 *
 * @package     Joomla.Site
 * @subpackage  com_insurediyci
 * @since       1.5
 */
class InsureDIYCIModelPDF_SUN extends JModelForm {

	/**
	 * Model context string.
	 *
	 * @access	protected
	 * @var		string
	 */
	protected $_context = 'com_insurediyci.pdf_sun';

	public function getReturnPage() {
		// No Return Page
		return;
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since   1.6
	 */
	protected function populateState() {
		// No State
		return;
	}

	/**
	 * Method to get an object.
	 *
	 * @param   integer	The id of the object to get.
	 *
	 * @return  mixed  Object on success, false on failure.
	 */
	public function getItem($id = null) {
		// nothing to do
	}

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	public function getTable($type = '', $prefix = 'InsureDIYCITable', $config = array()) {
		// nothing to do
		return;
	}

	/**
	 * Method to get the profile form.
	 *
	 * The base form is loaded from XML and then an event is fired
	 * for users plugins to extend the form with extra fields.
	 *
	 * @param   array  $data		An optional array of data for the form to interogate.
	 * @param   boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return  JForm	A JForm object on success, false on failure
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true) {
		// No Form
		return;
	}

	/**
	 * Generate PDF File
	 *
	 * @param   N/A
	 *
	 * @return  Integer Total Sum Insured
	 */
	public function generate() {

		$session = JFactory::getSession();
		$db = JFactory::getDBO();

		$quotation_id = $session->get('quotation_id', JRequest::getVar('quotation_id'));

		$query = " SELECT * FROM #__insure_ci_quotations WHERE id = '$quotation_id' ";
		$db->setQuery($query);
		$data = $db->loadObject();

		$query = " SELECT * FROM #__insure_ci_quotation_properties WHERE quotation_id = '$quotation_id' LIMIT 3 ";
		$db->setQuery($query);
		$properties = $db->loadObjectList();

		$query = " SELECT * FROM #__insure_ci_quotation_cars WHERE quotation_id = '$quotation_id' LIMIT 3 ";
		$db->setQuery($query);
		$cars = $db->loadObjectList();

		$application = array();
		$application = array('Sun-DDA.pdf', 'Sun-Large_Amount_Questionarie.pdf', 'SunLife-Application-1.pdf', 'SunLife-Application-2.pdf', 'SunLife-Application-3.pdf', 'Sun-Customer_Declaration.pdf');

		$user = JFactory::getUser();

		// initiate FPDI
		$pdf = new FPDI();

		$path = 'images/insurediyform/form/sun/';


		/* Sun-DDA - 2 pages */
		$pageCount = $pdf->setSourceFile($path . $application[0]);

		for ($n = 1; $n <= $pageCount; $n++) {

			// add a page
			$pdf->AddPage();

			// import page 1
			$tplIdx = $pdf->importPage($n, '/TrimBox');

			// use the imported page and place it at point 10,10 with a width of 100 mm
			$pdf->useTemplate($tplIdx, 0, 0);

			// now write some text above the imported page
			$pdf->SetFont('Helvetica');
			$pdf->SetFontSize(12); // normal is 12pt
			$pdf->SetTextColor(0, 0, 0);

			switch ($n) :
				case 1:

					$pdf->SetXY(10, 135);
					$pdf->Write(0, $data->bank_acct_holder_name);

					$pdf->SetXY(10, 147);
					$pdf->Write(0, $data->bank_id_no);

					switch ($data->bank_id_type) :
						case 'hkid' : $pdf->Image("images/ico-checked-black.png", 25, 152);
							break;
						case 'passport' : $pdf->Image("images/ico-checked-black.png", 58, 152);
							break;
					endswitch;

					$pdf->SetFontSize(10); // normal is 12pt

					$pdf->SetXY(115, 125);
					$pdf->Write(0, chunk_split($data->bank_bank_no, 1, '  '));

					$pdf->SetXY(135, 125);
					$pdf->Write(0, chunk_split($data->bank_branch_no, 1, '  '));

					$pdf->SetXY(150, 125);
					$pdf->Write(0, chunk_split($data->bank_acct_no, 1, '  '));

					break;
				case 2:

					$pdf->Image("images/ico-checked-black.png", 10, 145);
					break; // I am insured
					break;

			endswitch;
		}

		/* Sun-Large Amount Questionarie.pdf */
		$pageCount = $pdf->setSourceFile($path . $application[1]);

		for ($n = 1; $n <= $pageCount; $n++) {

			// add a page
			$pdf->AddPage();

			// import page 1
			$tplIdx = $pdf->importPage($n, '/TrimBox');

			// use the imported page and place it at point 10,10 with a width of 100 mm
			$pdf->useTemplate($tplIdx, 0, 0);

			// now write some text above the imported page
			$pdf->SetFont('Helvetica');
			$pdf->SetFontSize(8); // normal is 12pt
			$pdf->SetTextColor(0, 0, 0);

			switch ($n) :
				case 1:
					$pdf->SetXY(89, 37);
					$pdf->Write(0, $data->contact_firstname . ' ' . $data->contact_lastname);

					$pdf->SetXY(100, 155);
					$pdf->Write(0, $data->other_ownership_percent);

					$pdf->SetXY(100, 163);
					$pdf->Write(0, $data->other_ownership_employees_no);

					$pdf->SetXY(100, 168);
					$pdf->Write(0, $data->other_ownership_commencement_biz);

					/* Property Value */
					foreach ($properties as $key => $value) :

						switch ($key) {

							case 0:

								$pdf->SetXY(84, 227);
								$pdf->Write(0, $value->property_price);

								$pdf->SetXY(84, 231);
								$pdf->Write(0, $value->property_mortgage);

								$pdf->SetXY(84, 235);
								$pdf->Write(0, $value->property_value);

								break;

							case 1:

								$pdf->SetXY(132, 227);
								$pdf->Write(0, $value->property_price);

								$pdf->SetXY(132, 231);
								$pdf->Write(0, $value->property_mortgage);

								$pdf->SetXY(132, 235);
								$pdf->Write(0, $value->property_value);

								break;

							case 2:

								$pdf->SetXY(180, 227);
								$pdf->Write(0, $value->property_price);

								$pdf->SetXY(180, 231);
								$pdf->Write(0, $value->property_mortgage);

								$pdf->SetXY(180, 235);
								$pdf->Write(0, $value->property_value);

								break;
						}

					endforeach;


					/* Cars */
					foreach ($cars as $key => $value) :
						switch ($key) {

							case 0:

								$pdf->SetXY(84, 246);
								$pdf->Write(0, $value->car_model);

								break;

							case 1:

								$pdf->SetXY(132, 246);
								$pdf->Write(0, $value->car_model);

								break;

							case 2:

								$pdf->SetXY(180, 246);
								$pdf->Write(0, $value->car_model);

								break;
						}
					endforeach;


					break;
			endswitch;
		}


		/* SunLife-Application-1.pdf - 6 pages */
		$pageCount = $pdf->setSourceFile($path . $application[2]);

		for ($n = 1; $n <= $pageCount; $n++) {

			// add a page
			$pdf->AddPage();

			// import page 1
			$tplIdx = $pdf->importPage($n, '/TrimBox');

			// use the imported page and place it at point 10,10 with a width of 100 mm
			$pdf->useTemplate($tplIdx, 0, 0);

			// now write some text above the imported page
			$pdf->SetFont('Helvetica');
			$pdf->SetFontSize(10); // normal is 12pt
			$pdf->SetTextColor(0, 0, 0);

			switch ($n) :
				case 1:

					$pdf->SetXY(50, 87);
					$pdf->Write(0, $data->contact_firstname . ' ' . $data->contact_lastname);

					switch ($data->gender) : // SEX
						case 'M' : $pdf->Image("images/ico-checked-black.png", 52, 114);
							break;
						case 'F' : $pdf->Image("images/ico-checked-black.png", 92, 114);
							break;
					endswitch;

					switch ($data->has_used_tobacco) :
						case 0 : $pdf->Image("images/ico-checked-black.png", 52, 120);
							break;
						case 1 : $pdf->Image("images/ico-checked-black.png", 92, 120);
							break;
					endswitch;

					$pdf->SetXY(50, 128);
					$pdf->Write(0, $data->contact_identity_no);

					$pdf->SetXY(50, 135);
					$pdf->Write(0, MyHelper::getNationality($data->nationality)); // nationality

					$dob = explode("-", $data->dob);

					$pdf->SetXY(50, 141);
					$pdf->Write(0, chunk_split(sprintf('%02d', $dob[2]), 1, '  ')); // dd

					$pdf->SetXY(60, 141);
					$pdf->Write(0, chunk_split(sprintf('%02d', $dob[1]), 1, '  ')); // mm

					$pdf->SetXY(70, 141);
					$pdf->Write(0, chunk_split($dob[0], 1, '  ')); // year

					$age = (int) date("Y") - (int) $dob[0];

					$pdf->SetXY(110, 141);
					$pdf->Write(0, $age);

					switch ($data->marital_status) :
						case 'S' : $pdf->Image("images/ico-checked-black.png", 52, 145);
							break;
						case 'M' : $pdf->Image("images/ico-checked-black.png", 76, 145);
							break;
						default: break;
					endswitch;

					$pdf->SetXY(55, 155);
					$pdf->Write(0, $data->contact_room_no);

					$pdf->SetXY(65, 155);
					$pdf->Write(0, $data->contact_floor_no);

					$pdf->SetXY(77, 155);
					$pdf->Write(0, $data->contact_block_no);

					$pdf->SetXY(90, 155);
					$pdf->Write(0, $data->contact_building_name);

					$pdf->SetXY(50, 163);
					$pdf->Write(0, $data->contact_street_name);

					$pdf->SetXY(125, 163);
					$pdf->Write(0, $data->contact_district_name);

					$pdf->Image("images/ico-checked-black.png", 153, 157); // HK

					$pdf->SetXY(78, 192);
					$pdf->Write(0, (($data->contact_country_code) ? '(' . $data->contact_country_code . ') ' : '') . $data->contact_contact_no);

					$pdf->SetXY(108, 192);
					$pdf->Write(0, $data->email);

					$pdf->SetXY(50, 215);
					$pdf->Write(0, $data->occupation);

					$pdf->SetXY(50, 257);
					$pdf->Write(0, $data->job_nature);

					break;
				case 2:

					$pdf->Image("images/ico-checked-black.png", 39, 18); // HK$

					break;

				case 3:

					$pdf->SetXY(108, 150);
					$pdf->Write(0, $data->height_amt);

					$pdf->SetXY(108, 158);
					$pdf->Write(0, $data->weight_amt);

					switch ($data->has_weight_change) :
						case 1 : $pdf->Image("images/ico-checked-black.png", 108, 170);
							break;
						case 0 : $pdf->Image("images/ico-checked-black.png", 118, 170);
							break;
						default: break;
					endswitch;

					switch ($data->has_physicians) :
						case 1 : $pdf->Image("images/ico-checked-black.png", 108, 265);
							break;
						case 0 : $pdf->Image("images/ico-checked-black.png", 118, 265);
							break;
						default: break;
					endswitch;

					break;

				case 4:
					switch ($data->has_used_tobacco) :
						case 1 : $pdf->Image("images/ico-checked-black.png", 108, 23);
							break;
						case 0 : $pdf->Image("images/ico-checked-black.png", 118, 23);
							break;
						default: break;
					endswitch;
					break;

				case 5 :

					break;

				case 6 :

					break;

			endswitch;
		}


		/* SunLife-Application-2.pdf - 1 pages */
		$pageCount = $pdf->setSourceFile($path . $application[3]);

		for ($n = 1; $n <= $pageCount; $n++) {

			// add a page
			$pdf->AddPage();

			// import page 1
			$tplIdx = $pdf->importPage($n, '/TrimBox');

			// use the imported page and place it at point 10,10 with a width of 100 mm
			$pdf->useTemplate($tplIdx, 0, 0);

			// now write some text above the imported page
			$pdf->SetFont('Helvetica');
			$pdf->SetFontSize(10); // normal is 12pt
			$pdf->SetTextColor(0, 0, 0);

			switch ($n) :
				case 1:

					break;
			endswitch;
		}


		/* SunLife-Application-3.pdf - 2 pages */
		$pageCount = $pdf->setSourceFile($path . $application[4]);

		for ($n = 1; $n <= $pageCount; $n++) {

			// add a page
			$pdf->AddPage();

			// import page 1
			$tplIdx = $pdf->importPage($n, '/TrimBox');

			// use the imported page and place it at point 10,10 with a width of 100 mm
			$pdf->useTemplate($tplIdx, 0, 0);

			// now write some text above the imported page
			$pdf->SetFont('Helvetica');
			$pdf->SetFontSize(10); // normal is 12pt
			$pdf->SetTextColor(0, 0, 0);

			switch ($n) :
				case 1:

					break;
			endswitch;
		}

		/* Sun-Customer_Declaration */
		$pageCount = $pdf->setSourceFile($path . $application[5]);
		for ($n = 1; $n <= $pageCount; $n++) {

			// add a page
			$pdf->AddPage();
			// import page 1
			$tplIdx = $pdf->importPage($n, '/TrimBox');
			// use the imported page and place it at point 10,10 with a width of 100 mm
			$pdf->useTemplate($tplIdx, 0, 0);

			// now write some text above the imported page
			$pdf->SetFont('Helvetica');
			$pdf->SetFontSize(10); // normal is 12pt
			$pdf->SetTextColor(0, 0, 0);

			switch ($n) :
				case 1:
					$pdf->Image("images/ico-checked-black.png", 135, 118.5);

					$pdf->SetXY(56, 186);
					$pdf->Write(0, $data->contact_firstname . ' ' . $data->contact_lastname);
					break;
			endswitch;
		}

		if (@!mkdir("images/insurediyform/quotation/" . $data->id, 0777, true)) {

		}

		$path = "images/insurediyform/quotation/" . $data->id . "/SUN_Application_" . JHtml::_('date', time(), 'dMYHis') . ".pdf";

		$pdf->Output($path, 'F');

		return $path;
	}

}
