<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_banners
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('JPATH_BASE') or die;

/**
 * Clicks Field class for the Joomla Framework.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_banners
 * @since       1.6
 */
JLoader::import('incs.form.fields.customlabel', JPATH_ROOT);

class JFormFieldInsureDIYCILength2 extends JFormField {

	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since   1.6
	 */
	protected $type = 'InsureDIYCILength2';

	/**
	 * Method to get the field input markup.
	 *
	 * @return  string	The field input markup.
	 * @since   1.6
	 */
	protected function getInput() {
		//$onchange	= ' onchange="document.id(\''.$this->id.'_unlimited\').checked=document.id(\''.$this->id.'\').value==\'\';"';
		//$onclick	= ' onclick="if (document.id(\''.$this->id.'_unlimited\').checked) document.id(\''.$this->id.'\').value=\'\';"';
//		$db = JFactory::getDBO();
		$session = JFactory::getSession();
		$value = empty($this->value) ? '' : $this->value;

		$checked = ' checked="checked" ';
		$class = ' class="checked" ';

		$cover_length =$session->get('cover_length');

		$html = '';

		$options = array();
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$query->select("*");
		$query->from("#__insure_ci_years");
		$db->setQuery($query);
		$results = $db->loadAssocList();
		if ($results) {
			foreach ($results as $k => $result) {
				$text = $result['year_name'];
				$value = $result['year_code'];
				$options[] = JHtml::_(
								'select.option', $value, $text, 'value', 'text', false
				);
			}
		}
		/* Length of Cover */
//		$options[] = JHtml::_('select.option', 1, '1 Year', 'value', 'text');
//		$options[] = JHtml::_('select.option', 5, '5 Year(s)', 'value', 'text');
//		$options[] = JHtml::_('select.option', 10, '10 Year(s)', 'value', 'text');
//		$options[] = JHtml::_('select.option', 15, '15 Year(s)', 'value', 'text');
//		$options[] = JHtml::_('select.option', 20, '20 Year(s)', 'value', 'text');
//		$options[] = JHtml::_('select.option', 60, 'To age 60 to 75', 'value', 'text');

		$html .= '<div id="insurediyci-cvrlgth1">';
		$html .= JHTML::_('select.genericlist', $options, 'jform[cover_length]', 'onchange="javascript:void(0);" ', 'value', 'text', (isset($cover_length) ? $cover_length : ''));
		$html .= '</div>';

//		$options = array();
//		for ($i = 18; $i < 70; $i++) :
//			$options[] = JHtml::_('select.option', $i, $i . ' years old', 'value', 'text');
//		endfor;
//
//		$html .= '<div id="insurediyci-cvrlgth2">';
//		$html .= JHTML::_('select.genericlist', $options, 'jform[cover_until_length]', 'onchange="javascript:void(0);" ', 'value', 'text', (isset($data['cover_until_length']) ? $data['cover_until_length'] : ''));
//		$html .= '</div>';

//		if ($this->default == 1) {
//			$script = "
//			window.addEvent('domready',function(){
//				$('insurediyci-cvrlgth2').setStyle('display','none');
//
//				$('insurediy-click-length-1').addEvent('click', function() {
//					$('insurediyci-cvrlgth1').setStyle('display','');
//					$('insurediyci-cvrlgth2').setStyle('display','none');
//				});
//
//				$('insurediy-click-length-2').addEvent('click', function() {
//					$('insurediyci-cvrlgth1').setStyle('display','none');
//					$('insurediyci-cvrlgth2').setStyle('display','');
//				})
//			});";
//		} else { // 2
//			$script = "
//			window.addEvent('domready',function(){
//				$('insurediyci-cvrlgth1').setStyle('display','none');
//
//				$('insurediy-click-length-1').addEvent('click', function() {
//					$('insurediyci-cvrlgth1').setStyle('display','');
//					$('insurediyci-cvrlgth2').setStyle('display','none');
//				});
//
//				$('insurediy-click-length-2').addEvent('click', function() {
//					$('insurediyci-cvrlgth1').setStyle('display','none');
//					$('insurediyci-cvrlgth2').setStyle('display','');
//				})
//			});";
//		}

//		JFactory::getDocument()->addScriptDeclaration($script);

		return '<div class="insurediy-custom-radio">'
//				. '<div class="insurediy-custom-radio-alcohol-length-of-cover"><input type="radio" id="insurediy-length-1" value="1" name="' . $this->name . '" ' . (($this->default == 1) ? $checked : '') . ' /><label for="insurediy-length-1" id="insurediy-click-length-1" ' . (($this->default == 1) ? $class : '') . '>&nbsp;</label></div>'
//				. '<div class="insurediy-custom-radio-alcohol-cover-until-age"><input type="radio" id="insurediy-length-2" value="2" name="' . $this->name . '" ' . (($this->default == 2) ? $checked : '') . ' /><label for="insurediy-length-2" id="insurediy-click-length-2" ' . (($this->default == 2) ? $class : '') . '>&nbsp;</label></div>'
				. $html
				. '</div>';
	}

}
