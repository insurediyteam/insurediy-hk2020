<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_banners
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('JPATH_BASE') or die;

/**
 * Clicks Field class for the Joomla Framework.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_banners
 * @since       1.6
 */
JLoader::import('incs.form.fields.customlabel', JPATH_ROOT);

class JFormFieldInsureDIYCITobacco extends JformFieldCustomLabel {

	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since   1.6
	 */
	protected $type = 'InsureDIYCITobacco';

	/**
	 * Method to get the field label markup.
	 *
	 * @return  string  The field label markup.
	 *
	 * @since   11.1
	 */
//	protected function getLabel() {
//		$label = '';
//		$imghelpquote = '';
//
//		if ($this->hidden) {
//			return $label;
//		}
//
//		// Get the label text from the XML element, defaulting to the element name.
//		$text = $this->element['label'] ? (string) $this->element['label'] : (string) $this->element['name'];
//		$text = $this->translateLabel ? JText::_($text) : $text;
//
//		// Build the class for the label.
//		$class = !empty($this->description) ? 'hasTooltip' : '';
//		$class = $this->required == true ? $class . ' required' : $class;
//		$class = !empty($this->labelClass) ? $class . ' ' . $this->labelClass : $class;
//
//		// Add the opening label tag and main attributes attributes.
//		$label .= '<label id="' . $this->id . '-lbl" for="' . $this->id . '" class="' . $class . '"';
//		$imghelpquote .= ' ';
//
//		// If a description is specified, use it to build a tooltip.
//		if (!empty($this->description)) {
//
//			JHtml::_('bootstrap.tooltip');
//			$imghelpquote .= '&nbsp;<img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="' . JHtml::tooltipText(trim($text, ':'), JText::_($this->description), 0) . '" />';
//		}
//
//		// Add the label text and closing tag.
//		if ($this->required) {
//			$label .= '>' . $text . '<span class="star">&#160;*</span>' . $imghelpquote . '</label>';
//		} else {
//			$label .= '>' . $text . $imghelpquote . '</label>';
//		}
//
//		return $label;
//	}

	/**
	 * Method to get the field input markup.
	 *
	 * @return  string	The field input markup.
	 * @since   1.6
	 */
	protected function getInput() {
		//$onchange	= ' onchange="document.id(\''.$this->id.'_unlimited\').checked=document.id(\''.$this->id.'\').value==\'\';"';
		//$onclick	= ' onclick="if (document.id(\''.$this->id.'_unlimited\').checked) document.id(\''.$this->id.'\').value=\'\';"';
		$inputClass = (isset($this->element['class'])) ? $this->element['class'] : "";
		$value = empty($this->value) ? 0 : $this->value;
		$checked = ' checked="checked" ';
		$class = ' class="checked" ';
		if (!$value) {
			$value = $this->default;
		}

//		$session = JFactory::getSession();
//		$data = $session->get('details');
//		if (isset($data['has_used_tobacco'])) {
//			$this->default = $data['has_used_tobacco'];
//		}


		return '<div class="insurediy-custom-radio">'
				. '<div class="insurediy-custom-radio-smoke-yes"><input class="' . $inputClass . '" type="radio" id="insurediy-smoke-1" value="1" name="' . $this->name . '" ' . (($value) ? $checked : '') . ' /><label for="insurediy-smoke-1" ' . (($value) ? $class : '') . '>&nbsp;</label></div>'
				. '<div class="insurediy-custom-radio-smoke-no"><input class="' . $inputClass . '" type="radio" id="insurediy-smoke-2" value="0" name="' . $this->name . '" ' . (($value) ? '' : $checked) . ' /><label for = "insurediy-smoke-2" ' . (($value) ? '' : $class) . '>&nbsp;</label></div>'
				. '<div style = "clear:both"></div>'
				. '</div>';
	}

}
