<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_banners
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('JPATH_BASE') or die;

//JFormHelper::loadFieldClass('list');

//require_once __DIR__ . '/../../helpers/banners.php';

/**
 * Bannerclient Field class for the Joomla Framework.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_banners
 * @since       1.6
 */

JLoader::import('incs.form.fields.customlabel', JPATH_ROOT);

class JFormFieldInsureDIYCIDrugs extends JformFieldCustomLabel{

	/**
	 * Method to get the field label markup.
	 *
	 * @return  string  The field label markup.
	 *
	 * @since   11.1
	 */
//	protected function getLabel() {
//		$label = '';
//		$imghelpquote = '';
//
//		if ($this->hidden) {
//			return $label;
//		}
//
//		// Get the label text from the XML element, defaulting to the element name.
//		$text = $this->element['label'] ? (string) $this->element['label'] : (string) $this->element['name'];
//		$text = $this->translateLabel ? JText::_($text) : $text;
//
//		// Build the class for the label.
//		$class = !empty($this->description) ? 'hasTooltip' : '';
//		$class = $this->required == true ? $class . ' required' : $class;
//		$class = !empty($this->labelClass) ? $class . ' ' . $this->labelClass : $class;
//
//		// Add the opening label tag and main attributes attributes.
//		$label .= '<label id="' . $this->id . '-lbl" for="' . $this->id . '" class="' . $class . '"';
//		$imghelpquote .= ' ';
//
//		// If a description is specified, use it to build a tooltip.
//		if (!empty($this->description)) {
//
//			JHtml::_('bootstrap.tooltip');
//			$imghelpquote .= '&nbsp;<img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="' . JHtml::tooltipText(trim($text, ':'), JText::_($this->description), 0) . '" />';
//		}
//
//		// Add the label text and closing tag.
//		if ($this->required) {
//			$label .= '>' . $text . '<span class="star">&#160;*</span>' . $imghelpquote . '</label>';
//		} else {
//			$label .= '>' . $text . $imghelpquote . '</label>';
//		}
//
//		return $label;
//	}

	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since   1.6
	 */
	protected $type = 'InsureDIYCIDrugs';

	/**
	 * Method to get the field options.
	 *
	 * @return  array  The field option objects.
	 *
	 * @since   1.6
	 */
	public function getOptions() {
		$options = BannersHelper::getClientOptions();

		return array_merge(parent::getOptions(), $options);
	}

	protected function getInput() {
		//$onchange	= ' onchange="document.id(\''.$this->id.'_unlimited\').checked=document.id(\''.$this->id.'\').value==\'\';"';
		//$onclick	= ' onclick="if (document.id(\''.$this->id.'_unlimited\').checked) document.id(\''.$this->id.'\').value=\'\';"';
		$inputClass = (isset($this->element['class'])) ? $this->element['class'] : "";
		$value = empty($this->value) ? '' : $this->value;
		$checked = ' checked="checked" ';
		$class = ' class="checked" ';

		$session = JFactory::getSession();
		$data = $session->get('details');
		if (isset($data['has_used_drugs'])) {
			$this->default = $data['has_used_drugs'];
		}

		return '<div class="insurediy-custom-radio">'
				. '<div class="insurediy-custom-radio-drugs-yes"><input class="' . $inputClass . '" type="radio" id="insurediy-drugs-1" value="1" name="' . $this->name . '" ' . (($this->default == 1) ? $checked : '') . ' /><label for="insurediy-drugs-1" ' . (($this->default == 1) ? $class : '') . '>&nbsp;</label></div>'
				. '<div class="insurediy-custom-radio-drugs-no"><input class="' . $inputClass . '" type="radio" id="insurediy-drugs-2" value="0" name="' . $this->name . '" ' . (($this->default == 0) ? $checked : '') . ' /><label for="insurediy-drugs-2" ' . (($this->default == 0) ? $class : '') . '>&nbsp;</label></div>'
				. '<div style="clear:both"></div>'
				. '</div>';
	}

}
