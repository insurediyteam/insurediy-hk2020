<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_insurediyci
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * HTML Article View class for the component
 *
 * @package     Joomla.Site
 * @subpackage  com_insurediyci
 * @since       1.5
 */
class InsureDIYCIViewForm extends JViewLegacy {

	protected $form;
	protected $item;
	protected $state;

	public function display($tpl = null) {
		$session = JFactory::getSession();
		$model = $this->getModel();
		$app = JFactory::getApplication();
		$params = JComponentHelper::getParams('com_insurediyci');
		$user = JFactory::getUser();
		$currentUser = MyHelper::getCurrentUser();
		$form = $this->get("Form");
		$quotation = InsureDIYCIHelper::getQuotation();

		$menu_params = $app->getMenu()->getActive()->params;
		$this->menu_params = $menu_params;

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseWarning(500, implode("\n", $errors));
			return false;
		}
		$layout = InsureDIYCIHelper::getLayout();
		//Re Generate Unique Order No
		$db = JFactory::getDBO();
		$dataobj = new stdClass();
		$dataobj->id = $quotation['id'];
		$dataobj->unique_order_no = InsureDIYHelper::getUniqueOrderNo($user->id);
		switch ($layout) {
			case 'step1':
				$this->askTna = is_null($session->get('ci.protection_needs_analysis', NULL));
			case 'step2':
				$this->questionnaires = $this->get('Questionnaires');
				$this->step1CriteriaPass = $session->get('ci.step1CriteriaPass');
				$this->ask4override = $session->get("ci.step1.ask4override", TRUE);
				break;
			case 'step3':
				$this->policy_plans = $this->get('PolicyPlans');
				$this->future_policy_plans = $this->get('FuturePolicyPlans');

				$companies = array();
				foreach ($this->policy_plans as $plan) {
					$companies[] = $plan->insurer_code;
				}

				$cdata = array();
				$ages = array();
				$cname = array();
				$colorCode = array();
				$length_cover = $model->getLengthCover($quotation['cover_length']);
				$age = "";
				foreach ($this->future_policy_plans as $future_policy_plan) {
					$age .= $future_policy_plan->age;
					if (in_array($future_policy_plan->insurer_code, $companies)) {
						$cdata[$future_policy_plan->insurer_code][$future_policy_plan->age] = $future_policy_plan->price;
						for ($i = 1; $i < $length_cover; $i++) {
							$cdata[$future_policy_plan->insurer_code][$future_policy_plan->age + $i] = $future_policy_plan->price;
						}
					}
					if (!in_array($future_policy_plan->age, $ages)) {
						$ages[] = $future_policy_plan->age;
						for ($i = 1; $i < $length_cover; $i++) {
							$ages[] = $future_policy_plan->age + $i;
						}
					}
					if (!in_array($future_policy_plan->company_name, $cname)) {
						$cname[] = $future_policy_plan->company_name;
						$colorCode[] = ($future_policy_plan->color_code) ? $future_policy_plan->color_code : '000000';
					}
				}
				$chartData = array();
				$data1 = array_merge(array(0 => "Age"), $cname);
				$chartData[] = $data1; // first row
				foreach ($ages as $age) {
					$temp = array();
					$temp[] = (int) $age;
					foreach ($cdata as $d) {
						$temp[] = (isset($d[$age])) ? (int) $d[$age] : null;
					}
					$chartData[] = $temp;
				}
				if ($length_cover > 1) {
					$ages = $model->getAgesForGraph($length_cover);
				} else {
					$ages = $model->getAgesForGraph(5);
				}
				$year_code = $quotation['cover_length'];
				$showGraph = TRUE;
				if ($year_code == "60T75" || count($cdata) == 0) {
					$showGraph = FALSE;
				}
				$this->showGraph = $showGraph;
				$this->maxAge = (count($ages) > 0) ? max($ages) : 0;
				$this->ages = $ages;
				$this->chartData = $chartData;
				$this->colorData = $colorCode;

				break;

			case 'step4':
				$this->has_physicians = $quotation['has_physicians'];

				// FB and referral stuffs
				$referred_by = $session->get(SESSION_KEY_REFID, FALSE);
				if (!$currentUser->referred_by && $referred_by) {
					$form->setValue('referred_by', "", $referred_by);
				}
				break;

			case 'extrainfo':
				$this->my_sum_insured_total = $model->checkSumInsuredQuotationTotal($quotation['id']);

				break;
			case 'step5':
				$quotation['complete_address'] = InsureDIYCIHelper::getCompleteAddress($quotation);
				$db->updateObject('#__insure_ci_quotations', $dataobj, "id");
				$this->paymentData = $this->get('PDPayment');
				if (!$this->paymentData) {
					$app->enqueueMessage(JText::_("ERR_PAYMENT_CONFIG"));
				}
				break;
			case 'step6' :
				$quotation['complete_address'] = InsureDIYCIHelper::getCompleteAddress($quotation);
				//$db->updateObject('#__insure_ci_quotations', $dataobj, "id");
				$time = strtotime($quotation['created_date']);
				$year = (int) date("Y", $time);
				$month = (int) date("m", $time);
				$day = (int) date("d", $time);

				$x_days = (int) $params->get('x_days');
				$y_days = (int) $params->get('y_days');

				$quotation['sign_start'] = date("d M Y", mktime(0, 0, 0, $month, ($day + $x_days), $year));
				$quotation['sign_end'] = date("d M Y", mktime(0, 0, 0, $month, ($day + $x_days + $y_days), $year));

				// data for email
				$session->set("ci.maildata.sign_start." . $quotation['id'], $quotation['sign_start']);
				$session->set("ci.maildata.sign_end." . $quotation['id'], $quotation['sign_end']);

				break;
		}

		// Total Premiums for Tracking
		if(isset($quotation['selected_plans'])) {
			$total_premium = 0.0;
			foreach($quotation['selected_plans'] as $k => $v) {
				$total_premium += (float) $v->price;
			}
			$quotation['total_premium_price'] = $total_premium;
		}
		
		$this->setLayout($layout);
		$this->state = $this->get('State');
		$this->form = $form;
		$this->currency = $params->get("currency");
		$this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));
		$this->quotation = $quotation;
		$this->params = $params;
		$this->user = $user;
		$this->current_user = $currentUser;
		$this->flag_banner_path = 'images/flag_banners';
		$this->_prepareDocument();
		
		switch ($layout) {
			case 'thankyou':
			InsureDIYCIHelper::clearSessionData();
			break;
		}
		
		parent::display($tpl);
	}

	/**
	 * Prepares the document
	 */
	protected function _prepareDocument() {
		$app = JFactory::getApplication();
		$menus = $app->getMenu();
		$title = null;

		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();

		if (empty($this->item->id)) {
			$head = JText::_('COM_INSUREDIYCI_FORM_PAGE_HEADING');
		} else {
			$head = JText::_('COM_INSUREDIYCI_FORM_EDIT_PAGE_HEADING');
		}

		if ($menu) {
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		} else {
			$this->params->def('page_heading', $head);
		}

		$title = $this->params->def('page_title', $head);
		if ($app->getCfg('sitename_pagetitles', 0) == 1) {
			$title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
		} elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
			$title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
		}
		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description')) {
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords')) {
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots')) {
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}
		//facebook stuffs
		$this->document->setMetaData("og:title", InsureDIYCIHelper::getFbTitle());
		$this->document->setMetaData("og:description", InsureDIYCIHelper::getFbDesc());
		$this->document->setMetaData("og:image", InsureDIYCIHelper::getFbImage());
		$this->document->setMetaData("og:app_id", InsureDIYCIHelper::getFbAppId());
		$this->document->setMetaData("og:site_name", InsureDIYCIHelper::getFbSiteName());
	}

}
