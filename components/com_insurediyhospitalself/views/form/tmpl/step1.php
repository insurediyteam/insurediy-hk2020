<?php
	defined('_JEXEC') or die;
	
	JHtml::_('behavior.keepalive');
	JHtml::_('behavior.formvalidation');
	JHtml::_('formbehavior.chosen', '.insurediy-form-content select');
	JHtml::_('script', 'system/jquery.validate.js', false, true);
	
	$params = $this->state->get('params');
	$itemid = JRequest::getVar("itemid", FALSE);
	$form = $this->form;
?>

<script type="text/javascript">
	jQuery(document).ready(function () {
		jQuery("#adminForm").validate({
			ignore: [], // <-- option so that hidden elements are validated
		});
		
	});
</script>
<!-- Google Tag Manager Data Layer-->
<script>
	/**
		* Call this function when a user clicks on a product link. This function uses the
		event
		* callback datalayer variable to handle navigation after the ecommerce data has
		been sent
		* to Google Analytics.
		* @param {Object} productObj An object representing a
		product.
		*/
		function pushProductClick(productObj) {
			dataLayer.push({
				'event': 'productClick',
				'ecommerce': {
					'click': {
						'actionField': {'list': productObj.list}, // Optional list property.
						'products': [{
							'name': productObj.name,
							// Name or ID is required.
							'id': productObj.id
						}]
					}
				},
				'eventCallback': function() {
				}
			});
		}
	</script>
	<!-- End of Google Tag Manager Data Layer-->
	
	<div class="insurediy-form">
		<div class="header-top-wrapper">
			<?php echo InsureDIYHospitalSelfHelper::renderHeader('icon-myself', JText::_('COM_INSUREDIYHOSPITALSELF_PAGE_HEADING_YOUR_HOSPITAL_COVER'), 1); ?>
		</div>
		<div class="edit<?php echo $this->pageclass_sfx; ?> insurediy-form-content">
			<?php echo MyHelper::renderDefaultMessage(); ?>
			<form action="<?php echo JRoute::_('index.php?option=com_insurediyhospitalself&view=form'); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-vertical" enctype="multipart/form-data">
				<fieldset class="insurediy-form-fields">
					<!-- Row 1 -->
					<div class="control-row clearfix" style="margin-bottom:0;">
						<div class="cpan2">
							<div class="control-group" style="margin-bottom:0;">
								<div class="control-label">
									<?php echo $this->form->getLabel('cover_country'); ?>
									<div id="error-container"></div>
								</div>
								<div class="controls"><?php echo $this->form->getInput('cover_country'); ?>
									<div class="error-container"></div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
						<div class="cpan2 clearfix">
							<div class="cpan2 clearfix">
								<div class="control-group" style="margin-bottom:0;">
									<div class="control-label"><?php echo $this->form->getLabel('room_type'); ?></div>
									<div class="controls"><?php echo $this->form->getInput('room_type'); ?>
										<div class="error-container"></div>
									</div>
								</div>
							</div>
							<div class="cpan2 clearfix">
								<div class="control-group" style="margin-bottom:0;">
									<div class="control-label"><?php echo $this->form->getLabel('cover_amt'); ?></div>
									<div class="controls"><?php echo $this->form->getInput('cover_amt'); ?>
										<div class="error-container"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<!-- Row 1 end -->
					<div class="grey-hrule" style="margin: 10px 0"></div>
					<!-- Row 2 -->
					<div class="control-row clearfix">
						<div class="cpan2 clearfix">
							<div class="cpan2 clearfix">
								<div class="control-group">
									<div class="control-label"><?php echo $this->form->getLabel('gender'); ?></div>
									<div class="controls"><?php echo $this->form->getInput('gender'); ?>
										<div class="error-container"></div>
									</div>
								</div>
							</div>
							<div class="cpan2 clearfix">
								<div class="control-group">
									<div class="control-label"><?php echo $this->form->getLabel('dob'); ?></div>
									<div class="controls"><?php echo $this->form->getInput('dob'); ?>
										<div class="error-container"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="cpan2 clearfix">
							<div class="cpan2 clearfix">
								<div class="control-group">
									<div class="control-label"><?php echo $this->form->getLabel('weight_amt'); ?></div>
									<div class="controls"><?php echo $this->form->getInput('weight_amt'); ?>
										<div class="error-container"></div>
									</div>
								</div>
							</div>
							<div class="cpan2 clearfix">
								<div class="control-group">
									<div class="control-label"><?php echo $this->form->getLabel('height_amt'); ?></div>
									<div class="controls"><?php echo $this->form->getInput('height_amt'); ?>
										<div class="error-container"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<!-- Row 2 end -->
					
					<!-- Row 3 -->
					<div class="control-row clearfix">
						<div class="cpan2 clearfix">
							<div class="control-group">
								<div class="control-label"><?php echo $this->form->getLabel('has_weight_change'); ?></div>
								<div class="controls"><?php echo $this->form->getInput('has_weight_change'); ?>
									<div class="error-container"></div>
								</div>
							</div>
						</div>
						<div class="cpan2 clearfix">
							<div class="control-group">
								<div class="control-label"><?php echo $this->form->getLabel('has_used_tobacco'); ?></div>
								<div class="controls"><?php echo $this->form->getInput('has_used_tobacco'); ?>
									<div class="error-container"></div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<!-- Row 3 end -->
					
					<!-- Row 4 -->
					<div class="control-row clearfix">
						<div class="cpan2 clearfix">
							<div class="control-group">
								<div class="control-label"><?php echo $this->form->getLabel('has_used_alcohol'); ?></div>
								<div class="controls"><?php echo $this->form->getInput('has_used_alcohol'); ?>
									<div class="error-container"></div>
								</div>
							</div>
						</div>
						<div class="cpan2 clearfix">
							<div class="control-group">
								<div class="control-label"><?php echo $this->form->getLabel('has_used_drugs'); ?></div>
								<div class="controls"><?php echo $this->form->getInput('has_used_drugs'); ?>
									<div class="error-container"></div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<!-- Row 4 end -->
					
					<!-- Row 5 -->
					<div class="control-row clearfix">
						<div class="cpan2 clearfix">
							<div class="control-group">
								<div class="control-label"><?php echo $this->form->getLabel('has_physicians'); ?></div>
								<div class="controls"><?php echo $this->form->getInput('has_physicians'); ?>
									<div class="error-container"></div>
								</div>
							</div>
						</div>
						<div class="cpan2 clearfix">
							<div class="control-group">
								<div class="control-label"><?php echo $this->form->getLabel('marital_status'); ?></div>
								<div class="controls"><?php echo $this->form->getInput('marital_status'); ?>
									<div class="error-container"></div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<!-- Row 5 end -->
					
					<!-- Row 6 -->
					<div class="control-row clearfix">
						<div class="cpan2 clearfix">
							<div class="control-group">
								<div class="control-label"><?php echo $this->form->getLabel('occupation'); ?></div>
								<div class="controls"><?php echo $this->form->getInput('occupation'); ?>
									<div class="error-container"></div>
								</div>
							</div>
						</div>
						<div class="cpan2 clearfix">
							<div class="control-group">
								<div class="control-label"><?php echo $this->form->getLabel('job_nature'); ?></div>
								<div class="controls"><?php echo $this->form->getInput('job_nature'); ?>
									<div class="error-container"></div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<!-- Row 6 end -->
					
					<!-- Row 7 -->
					<div class="control-row clearfix">
						<div class="cpan2 clearfix">
							<div class="control-group">
								<div class="control-label"><?php echo $this->form->getLabel('monthly_income'); ?></div>
								<div class="controls"><?php echo $this->form->getInput('monthly_income'); ?>
									<div class="error-container"></div>
								</div>
							</div>
						</div>
						<div class="cpan2 clearfix">
							<div class="control-group">
								<div class="control-label"><?php echo $this->form->getLabel('country_residence'); ?></div>
								<div class="controls"><?php echo $this->form->getInput('country_residence'); ?>
									<div class="error-container"></div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<!-- Row 7 end -->
					
					<!-- Row 8 -->
					<div class="control-row clearfix">
						<div class="cpan2 clearfix">
							<div class="control-group">
								<div class="control-label"><?php echo $this->form->getLabel('nationality'); ?></div>
								<div class="controls"><?php echo $this->form->getInput('nationality'); ?>
									<div class="error-container"></div>
								</div>
							</div>
						</div>
						<div class="cpan2 clearfix">
							<div class="control-group">
								<div class="control-label"><?php echo $this->form->getLabel('has_reside_overseas'); ?></div>
								<div class="controls"><?php echo $this->form->getInput('has_reside_overseas'); ?>
									<div class="error-container"></div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<!-- Row 8 end -->
					
					<!-- Row 9 -->
					<div class="control-row clearfix">
						<div class="cpan2 clearfix">
							<div class="control-group">
								<div class="control-label"><?php echo $this->form->getLabel('has_claimed_insurance'); ?></div>
								<div class="controls"><?php echo $this->form->getInput('has_claimed_insurance'); ?>
									<div class="error-container"></div>
								</div>
							</div>
						</div>
						<div class="cpan2 clearfix">
							
						</div>
						<div class="clear"></div>
					</div>
					<!-- Row 9 end -->
					
					<div class="clear"></div>
				</fieldset>
				<fieldset class="insurediy-agreements">
					<?php echo MyHelper::load_module_pos("agreements"); ?>
					<?php if (FALSE): ?>
					<?php foreach ($this->form->getFieldset('agree_fields') as $field): ?>
					<div>
						<div class="control-group">
							<div class="control-label" style="float:left;width:4.5%;"><div style="position:relative;"><?php echo $field->input; ?></div></div>
							<div class="controls" style="float:left;width:95%"><?php echo $field->label; ?>
								<div class="error-container"></div>
							</div>
							<div style="clear:both"></div>
						</div>
					</div>
					<?php endforeach; ?>
					<?php endif; ?>
				</fieldset>
				<input type="hidden" name="task" value="form.step1save" />
				<?php echo $this->form->getInput('protection_needs_analysis'); ?>
				<?php echo $this->form->getInput('id'); ?>
				<?php if ($itemid): ?>
				<input type="hidden" name="Itemid" value="$itemid" />
				<?php endif; ?>
				<?php echo JHtml::_('form.token'); ?>
				
				<div style="float:left;margin:15px 0;">
					{modulepos insurediy-secured-ssl}
				</div>
				<div class="btn-toolbar" style="float:right">
					<div class="btn-group">
						<button type="submit" class="btn btn-primary validate" onclick="pushProductClick({name:'Self Medical',id:'selfMed01', list:'mainpage'})"><?php echo JText::_('JCONTINUE') ?></button>
					</div>
				</div>
				<div class="clear"></div>
			</form>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="logo-slider"><?php echo MyHelper::load_module(102); ?></div>
	
	<?php /* Popup Box */ ?>
	<div class="insurediy-popup" id="insurediy-popup-box" style="display:none;">
		<div class="header">
			<div class="text-header"><?php echo JText::_("POP_IMPORTANT_NOTICE"); ?></div>
			<a class="close" href="javascript:cleanInsureDIYPopup('insurediy-popup-box');">&nbsp;</a>
			<div class="clear"></div>
		</div>
		<div class="padding">
			<div><?php echo JText::_("COM_INSUREDIYHOSPITALSELF_MEDICAL_ISSUE_MSG"); ?></div>
			<div style="text-align:right;margin-top: 20px;">
				<input type="button" class="btn btn-primary" style="height:30px;padding:5px 20px;font-size:14px !important;" onclick="javascript:cleanInsureDIYPopup('insurediy-popup-box');
				return false;" value="<?php echo JText::_("JOK"); ?>" />
			</div>
		</div>
	</div>
	
	<?php /* Popup Box */ ?>
	<div class="insurediy-popup" id="insurediy-popup-box-2" <?php echo $this->askTna ? '' : 'style="display:none;"'; ?> >
		<div class="header">
			<div class="text-header"><?php echo JText::_("POP_IMPORTANT_NOTICE"); ?></div>
			<a class="close" href="javascript:tnaOnClickNo();">&nbsp;</a>
			<div class="clear"></div>
		</div>
		<div class="padding">
			<div><?php echo JText::_("COM_INSUREDIYHOSPITALSELF_NEW_QUOTE_POP_UP_MSG"); ?></div>
			<form action="<?php echo JRoute::_('index.php'); ?>" method="post" name="adminForm2" id="adminForm2" class="form-validate form-vertical" enctype="multipart/form-data">
				<div style="text-align:right;">
					<input type="button" class="btn btn-primary pop-up-button" onclick="tnaOnClickNo()" value="<?php echo JText::_("JNO"); ?>" />
					<input type="submit" class="btn btn-primary pop-up-button" value="<?php echo JText::_("JYES"); ?>" />
				</div>
				<?php echo JHtml::_('form.token'); ?>
				<input type='hidden' name='option' value='com_insurediyhospitalself' />
				<input type='hidden' name='task' value='form.step0save' />
			</form>
			
		</div>
	</div>
	
	<script type="text/javascript">
		var jFamily, jGroup, groupType, endDateWrapper, endDateInput, tripType;
		
		jQuery(document).ready(function () {
			var jForm = jQuery("form#adminForm");
			var jSelects = jForm.find("select");
			var jInputs = jForm.find("input");
			jQuery('html, body').scrollTop(0);
			
			jSelects.each(function () {
				var jThis = jQuery(this);
				jThis.on("change", selectOnChange);
			});
			jInputs.each(function () {
				var jThis = jQuery(this);
				jThis.on("change", selectOnChange);
			});
			
			//		jFamily = jQuery("#travellers_for_family");
			//		jGroup = jQuery("#travellers_for_group");
			//		groupType = jQuery("input[name='jform[group_type]']");
			//		endDateWrapper = jQuery("#end-date-wrapper");
			//		endDateInput = endDateWrapper.find("#jform_end_date");
			//		tripType = jQuery("input[name='jform[trip_type]']");
			
			jForm.submit(function () {
				if ($jq("#insurediy-popup-box-2").css("display") !== "none") {
					return false;
				}
				jForm.find("label.error").each(function () {
					var jThis = jQuery(this);
					var jContainer = jThis.closest("div.control-group");
					var jControl = jContainer.find("div.controls");
					jControl.find(".error-container").append(jThis.clone());
					jThis.remove();
				});
				var errors = jForm.find("label.error");
				if (errors.length > 0) {
					var label = jForm.find("label.error:first");
					jQuery('html, body').animate({
						'scrollTop': label.offset().top - 400
					}, 300);
				}
			});
			//
			//		groupType.each(function() {
			//			var jThis = jQuery(this);
			//			jThis.on("change", function() {
			//				groupOnChange(jThis);
			//			});
			//		});
			//		var selectedGroupType = jQuery("input[name='jform[group_type]']:checked");
			//		groupOnChange(selectedGroupType);
			//
			//		tripType.each(function() {
			//			var jThis = jQuery(this);
			//			jThis.on("change", function() {
			//				tripOnChange(jThis);
			//			});
			//		});
			//		var selectedTripType = jQuery("input[name='jform[trip_type]']:checked");
			//		tripOnChange(selectedTripType);
			var country = jQuery("#jform_cover_country");
			var room = jQuery("#jform_room_type");
			var cover = jQuery("#jform_cover_amt");
			
			country.on("change", function () {
				var selected = country.find("option:selected").val();
				if (selected == "HK") {
					room.find("option").each(function () {
						var jThis = jQuery(this);
						jThis.prop('disabled', false);
					});
					cover.find("option").each(function () {
						var jThis = jQuery(this);
						jThis.prop('disabled', false);
					});
					room.trigger("liszt:updated");
					cover.trigger("liszt:updated");
				}
				if (selected == "WW") {
					room.find("option[value=1]").prop('disabled', true);
					room.find("option[value=2]").prop('disabled', true);
					cover.find("option[value=1]").prop('disabled', true);
					
					room.val("3");
					cover.val("2");
					
					room.trigger("liszt:updated");
					cover.trigger("liszt:updated");
				}
				if (selected == "WU") {
					room.find("option[value=1]").prop('disabled', true);
					room.find("option[value=2]").prop('disabled', true);
					cover.find("option[value=1]").prop('disabled', true);
					
					room.val("3");
					cover.val("2");
					
					room.trigger("liszt:updated");
					cover.trigger("liszt:updated");
				}
			});
		});
		
		function groupOnChange(element) {
			var id = element.prop("id");
			if (id === "insurediy-with-group") {
				jFamily.fadeOut(function () {
					jGroup.fadeIn(500);
				});
				} else if (id === "insurediy-with-family") {
				jGroup.fadeOut(function () {
					if (jFamily.is(":hidden")) {
						jFamily.fadeIn(500);
					}
				});
				} else {
				jGroup.fadeOut(500);
				jFamily.fadeOut(500);
			}
			return true;
		}
		
		function tripOnChange(element) {
			var id = element.prop("id");
			if (id == "insurediy-single-trip") {
				endDateWrapper.fadeIn(500);
				endDateInput.attr("required", "required");
				endDateInput.attr("aria-required", "true");
				} else {
				endDateWrapper.fadeOut(500);
				endDateInput.removeAttr("required");
				endDateInput.removeAttr("aria-required");
			}
			return true;
		}
		
		function selectOnChange() {
			var jThis = jQuery(this);
			var jControl = jThis.closest("div.controls");
			if (jThis.val()) {
				jControl.find(".error-container label.error").remove();
				} else {
				jControl.find("label.error").each(function () {
					var jThis = jQuery(this);
					var jContainer = jThis.closest("div.control-group");
					var jControl = jContainer.find("div.controls");
					jControl.find(".error-container").append(jThis.clone());
					jThis.remove();
				});
			}
		}
		function tnaOnClickNo() {
			$jq("#jform_protection_needs_analysis").val("0");
			cleanInsureDIYPopup('insurediy-popup-box-2');
			return false;
		}
	</script>
