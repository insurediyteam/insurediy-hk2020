<?php
	
	defined('_JEXEC') or die;
	
	JHtml::_('behavior.keepalive');
	JHtml::_('behavior.formvalidation');
	JHtml::_('formbehavior.chosen', '.insurediy-form-content select');
	
	//JHtml::_('behavior.modal', 'a.modal_jform_contenthistory');
	// Create shortcut to parameters.
	$params = $this->state->get('params');
	
	$quotation = $this->quotation;
	
?>

<script type="text/javascript">
	window.addEvent('domready', function() {
		var myAccordion = new Fx.Accordion($$('.togglers'), $$('.step2-checkbox'), {display: -1, alwaysHide: true});
	});
	
	function clickFileType(div_id) {
		$(div_id).click();
	}
	
	function copyPasteText(div_id, text) {
		$(div_id).setProperty('value', text);
	}
</script>

<div class="insurediy-form">
	<div class="header-top-wrapper">
		<?php echo InsureDIYHospitalSelfHelper::renderHeader('icon-travel', JText::_('COM_INSUREDIYHOSPITALSELF_PAGE_HEADING_FINAL_STEPS'), 4); ?>
	</div>
	<div style="padding:30px;margin-top: 100px;">
		<?php echo MyHelper::renderDefaultMessage(); ?>
		<div class="edit<?php echo $this->pageclass_sfx; ?>">
			<div class="form-quote-box" style="width: 670px">
				<span style="color:#2f2f2f;font-size:37px;line-height: 40px;">
					<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_PAGE_SUB_HEADING_THANK_YOU_FOR_QUOTATION"); ?>
				</span>
				<div class="center" style="color:#2f2f2f;font-size:14px;margin-top: 15px;">
					<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_EXPLANATION_OUR_STAFFS_WILL_CONTACT"); ?>
				</div>
			</div>
		</div>
		
	</div>
</div>
<div class="logo-slider"><?php echo MyHelper::load_module(102); ?></div>

<script>
	// Send transaction data with a pageview if available
	// when the page loads. Otherwise, use an event when the transaction
	// data becomes available.
	dataLayer.push({
		'ecommerce': {
			'purchase': {
				'actionField': {
					'id': '<?php echo $this->quotation['unique_order_no']; ?>',
					// Transaction ID. Required for purchases and refunds.
					'revenue': '<?php echo $this->quotation['selected_plan']->premium; ?>'
				},
				'products': [{ // List of productFieldObjects.
					'name': '<?php echo $this->quotation['selected_plan']->plan_name; ?>', 
					// Name or ID is required.
					'id': '<?php echo $this->quotation['selected_plan']->plan_id; ?>',
					'price': '<?php echo $this->quotation['selected_plan']->premium; ?>',
					'brand': '<?php echo $this->quotation['selected_plan']->insurer_code; ?>',
					'category': 'Self Med',
					'variant': '<?php echo $this->quotation['selected_plan']->trip_type; ?>',
					'quantity': 1 // Optional fields may be omitted or set to empty string.
				}]
			}
		}
	});
</script>

<script>
window.uetq = window.uetq || [];
window.uetq.push({ 'gv': <?php echo $this->quotation['selected_plan']->premium; ?>, 'gc': 'HKD' }); 
</script>

