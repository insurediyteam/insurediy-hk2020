<?php
	defined('_JEXEC') or die;
	
	JHtml::_('behavior.keepalive');
	JHtml::_('behavior.formvalidation');
	JHtml::_('formbehavior.chosen', '.insurediy-plan-options select');
	JHtml::_('bootstrap.tooltip');
	
	$params = $this->state->get('params');
	$quotation = $this->quotation;
	$total_plans = count($this->policy_plans);
	$width = ($total_plans) ? 670 / $total_plans : 670;

	// remarketing email
	$user = $this->user;
	$products = [];
	$pol = [];
	foreach ($this->policy_plans as $key => $value) {
		$pol["premium"] = $value->premium;
		$pol["code"] = $value->insurer_code;
		array_push($products, $pol);
	}
	$mailQueue = (object)array(
		"email" => $user->email,
		"products" => $products,
		"type" => "hospital-self-before",
		"parent" => "hospital"
	);
	RemarketingHelpersRemarketing::newRemarketing($mailQueue);
	//end of remarketing email
	
?>
<script type="text/javascript">
	var checkboxHeight = "33";
	var radioHeight = "18";
	var selectWidth = "190";
	function chooseYourPlans() {
		var has_chosen = 0;
		$$('input[type="checkbox"]:checked').each(function (elem) {
			has_chosen = 1;
		});
		
		if (has_chosen == 0) {
			$('insurediy-popup-box').setStyle('display', 'block');
			} else {
			document.choosePlanForm.submit();
		}
	}
</script>
<script>
	//Measure a view of product details. This example assumes the detail view occurs on pageload,
	//and also tracks a standard pageview of the details page.
	dataLayer.push({
		'ecommerce': {
			'impressions': [<?php $position = 1; foreach ($this->policy_plans as $key => $policy) :?>
			{
				'name': '<?php echo $policy->plan_name; ?>', //Name or ID is required.
				'id': '<?php echo $policy->plan_index_code; ?>',
				'price': '<?php echo $policy->price; ?>',
				'brand': '<?php echo $policy->insurer_code; ?>',
				'category': 'Self Med',
				'list': 'Self Medical Quotation',
				'variant': '<?php echo $policy->year_code; ?>',
				'position': <?php echo $position; ?>
			},
			<?php $position++; endforeach; ?>]
		}
	});
	//Measure adding a product to a shopping cart by using an 'add' actionFieldObject
	//and a list of productFieldObjects.
	function addToCart(productObj) {
		dataLayer.push({
			'event': 'productClick',
			'ecommerce': {
				'click': {
					'actionField': {'list': 'Self Medical Quotation'}, // Optional list property.
					'products': [{
						'name': productObj.plan_name,
						'id': productObj.plan_index_code,
						'price': productObj.premium,
						'brand': productObj.insurer_code,
						'category': 'Self Med',
						'variant': productObj.trip_type
					}]
				}
			}
		});
		dataLayer.push({
			'event': 'addToCart',
			'ecommerce': {
				'currencyCode': 'SGD',
				'add': { // 'add' actionFieldObject measures.
					'products': [{ // adding a product to a shopping cart.
						'name': productObj.plan_name,
						'id': productObj.plan_index_code,
						'price': productObj.premium,
						'brand': productObj.insurer_code,
						'category': 'Self Med',
						'variant': productObj.trip_type,
						'quantity': 1
					}]
				}
			}
		});
	}	
</script>
<div class="insurediy-form">
	<div class="header-top-wrapper">
		<?php echo InsureDIYHospitalSelfHelper::renderHeader('icon-myself', JText::_('COM_INSUREDIYHOSPITALSELF_PAGE_HEADING_YOUR_HOSPITAL_COVER'), 2); ?>
	</div>
	<div class="insurediy-plan-options">
		<form action="index.php?option=com_insurediyhospitalself" method="post" name="planOptionsForm">
			<div>
				<div class="title" style="float: left;color:#0ea8ca;margin-left: 28px;line-height: 39px"><?php echo JText::_("COM_INSUREDIYHOSPITALSELF_HOSPITAL_COVER_OPTIONS"); ?></div>
				<div style="float: right">
					<div style="float:left;font-weight:bold;font-size:14px;color:#999;line-height: 39px"><?php echo JText::_("COM_INSUREDIYHOSPITALSELF_TYPE_OF_COVER"); ?></div>
					<div style="float:left;margin-left:10px;width:156px;">
						<?php echo $this->form->getInput("cover_country"); ?>
					</div>
					<div  style="float:left;margin-left:10px;width:156px;">
						<?php echo $this->form->getInput("room_type"); ?>
					</div>
					<div style="float:left;margin:0 10px;width:156px;">
						<?php echo $this->form->getInput("cover_amt"); ?>
					</div>
					<div style="float:left;margin-left:10px;">
						<input type="hidden" name="task" value="form.updatePlanOptions">
						<?php echo JHtml::_('form.token'); ?>
						<button type="submit" class="btn btn-primary validate" style="height:34px;padding:5px 20px;font-size:14px !important;"><?php echo JText::_("JGET_QUOTES"); ?></button>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		</form>
	</div>
	<div class="insurediy-form-content" style="margin-top: 0px;">
		<?php echo MyHelper::renderDefaultMessage(); ?>
		<div class="edit<?php echo $this->pageclass_sfx; ?>">
			<form action="<?php echo JRoute::_('index.php?option=com_insurediyhospitalself'); ?>" method="post" name="choosePlanForm" id="choosePlanForm" class="form-validate form-vertical">
				<?php if (!empty($this->policy_plans)) : ?>
				<table class="insurediy-table-plans">
					<tr>
						<td class="td-first">
							<div class="ico-quotation-mark" style="padding-left: 30px;"><?php echo JText::_("COM_INSUREDIYHOSPITALSELF_YOUR_QUOTATIONS"); ?></div>
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner" style="position:relative;width: <?php echo $width ?>px;">
							<?php if ($policy->logo) : ?>
							<div class="center">
								<img src="<?php echo $policy->logo ?>" />
							</div>
							<?php endif; ?>
							<?php if (isset($policy->flag_banner) && strlen($policy->flag_banner) > 0): ?>
							<div style="position:absolute;top:0;"><img src="<?php echo $this->flag_banner_path . '/' . $policy->flag_banner ?>" /></div>
							<?php endif; ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td class="td-first premuim-text">
							<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_FIELD_PREMIUM_TITLE"); ?> (<?php echo $this->currency; ?>) <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_FIELD_PREMIUM_DESC"); ?>"><br>
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner premuim-text">
							<?php echo MyHelper::numberFormat($policy->premium); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<?php /*<tr>
						<td class="td-first">
							<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_PLAN_DIY_POINTS_TITLE"); ?> <?php echo InsureDIYHelper::generateQuestionMark("COM_INSUREDIYHOSPITALSELF_PLAN_DIY_POINTS_DESC"); ?>
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo MyHelper::numberFormat($policy->pur_points); ?>
						</td>
						<?php endforeach; ?>
					</tr>*/ ?>
					<tr>
						<td class="td-first" >
							<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_FIELD_COVER_TYPE_TITLE"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_FIELD_COVER_TYPE_TOOLTIPS"); ?>">
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->cover_type); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td class="td-first">
							<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_FIELD_MAX_RENEWAL_AGE_TITLE"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_FIELD_MAX_RENEWAL_AGE_TOOLTIPS"); ?>">
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->max_renewal_age); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td class="td-first" >
							<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_FIELD_ROOM_N_BOARD_LABEL"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_FIELD_ROOM_N_BOARD_TOOLTIPS"); ?>">
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->room_n_board); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td class="td-first" >
							<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_FIELD_SURGICAL_LABEL"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_FIELD_SURGICAL_TOOLTIPS"); ?>">
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->surgical); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td class="td-first" >
							<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_FIELD_ANAESTHETIST_LABEL"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_FIELD_ANAESTHETIST_TOOLTIPS"); ?>">
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->anaesthetist); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td class="td-first" >
							<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_FIELD_OPERATING_THEATRE_LABEL"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_FIELD_OPERATING_THEATRE_TOOLTIPS"); ?>">
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->operating_theatre); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td class="td-first" >
							<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_FIELD_EMERGENCY_OP_LABEL"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_FIELD_EMERGENCY_OP_TOOLTIPS"); ?>">
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->emergency_op); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td class="td-first" >
							<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_FIELD_CANCER_N_KIDNEY_LABEL"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_FIELD_CANCER_N_KIDNEY_TOOLTIPS"); ?>">
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->cancer_n_kidney); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					
					
					<tr>
						<td class="td-first">
							<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_FIELD_MEDICAL_EXAMINATION_TITLE"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_FIELD_MEDICAL_EXAMINATION_TOOLTIPS"); ?>">
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->medical_examination); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td class="td-first">
							<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_FIELD_WAITING_PERIOD_TITLE"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_FIELD_WAITING_PERIOD_TOOLTIPS"); ?>">
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->waiting_period); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td class="td-first">
							<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_FIELD_OTHER_BENEFIT_TITLE"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_FIELD_OTHER_BENEFIT_TOOLTIPS"); ?>">
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo InsureDIYHelper::printPlanAttr($policy->other_benefit); ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td class="td-first">
							<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_FIELD_DOWNLOAD_BROCHURE_TITLE"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_FIELD_DOWNLOAD_BROCHURE_DESC"); ?>">
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<?php echo ($policy->brochure) ? '<a href="' . $policy->brochure . '" target="_blank"><img src="templates/protostar/images/icon/ico-downloadfile.png" /></a>' : 'N/A' ?>
						</td>
						<?php endforeach; ?>
					</tr>
					<tr>
						<td class="td-first">
							<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_FIELD_PURCHASE_TITLE"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_FIELD_PURCHASE_TOOLTIPS"); ?>">
						</td>
						<?php foreach ($this->policy_plans as $key => $policy) : ?>
						<td class="td-inner">
							<button class="btn btn-primary btn-rounded" type="submit" name="plan_id" onclick="addToCart({plan_name:'<?php echo $policy->plan_name; ?>', plan_index_code:'<?php echo $policy->plan_index_code; ?>', premium:'<?php echo $policy->premium; ?>', insurer_code:'<?php echo $policy->insurer_code; ?>', trip_type:'<?php echo $policy->cover_country; ?>'})" value="<?php echo $policy->plan_id; ?>"><?php echo JText::_("COM_INSUREDIYHOSPITALSELF_BUTTONS_APPLY_NOW"); ?></button>
						</td>
						<?php endforeach; ?>
					</tr>
				</table>
				<?php else : ?>
				<div><?php echo JText::_("COM_INSUREDIYHOSPITALSELF_NO_PLANS_MSG"); ?></div>
				<?php endif; ?>
				<input type="hidden" name="task" value="form.step3save" />
				<input type="hidden" name="quotation_id" value="<?php echo $quotation['id']; ?>" />
				<?php echo JHtml::_('form.token'); ?>
				
				<div style="float:left;margin:15px 0;">
					<div>{modulepos insurediy-download-adobe}</div>
					<div>{modulepos insurediy-secured-ssl}</div>
				</div>
				<div class="btn-toolbar" style="float:right">
					<div>
						<button style="margin-right:10px;" type="button" onclick="javascript:document.choosePlanForm.task.value = 'form.back';
						document.choosePlanForm.submit();" class="btn btn-primary validate"><?php echo JText::_('BTN_BACK') ?></button>
					</div>
				</div>
				<div class="clear"></div>
			</form>
		</div>
	</div>
</div>

<?php /* Popup Box */ ?>
<div class="insurediy-popup" id="insurediy-popup-box" style="display:none;">
	<div class="header">
		<div class="text-header"><?php echo JText::_("POP_IMPORTANT_NOTICE"); ?></div>
		<a class="close" href="javascript:cleanInsureDIYPopup('insurediy-popup-box');">&nbsp;</a>
		<div class="clear"></div>
	</div>
	<div class="padding">
		<div><?php echo JText::_("COM_INSUREDIYHOSPITALSELF_AT_LEAST_1_PLAN_MSG"); ?></div>
		<div style="text-align:right;"><input type="button" class="btn btn-primary" style="height:30px;padding:5px 20px;font-size:14px !important;" onclick="javascript:cleanInsureDIYPopup('insurediy-popup-box');
		return false;" value="Ok" /></div>
	</div>
</div>

<?php if (FALSE): ?>
<tr>
	<td class="td-first"><?php echo JText::_("COM_INSUREDIYHOSPITALSELF_PLANS"); ?></td>
	<?php foreach ($this->policy_plans as $key => $policy) : ?>
	<td class="td-inner">
		<div class="center" style="font-family:montserrat-regular;font-size:14px;"><?php echo $policy->plan_name; ?></div>
	</td>
	<?php endforeach; ?>
</tr>
<?php endif; ?>

<?php if (FALSE): ?>
<tr>
	<td class="td-first" >
		<?php echo JText::_("COM_INSUREDIYHOSPITALSELF_FIELD_PRODUCT_BROCHURE_TITLE"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_(""); ?>">
	</td>
	<?php foreach ($this->policy_plans as $key => $policy) : ?>
	<td class="column-n center" >
		<?php echo ($policy->brochure) ? '<a href="' . $policy->brochure . '" target="_blank"><img src="templates/protostar/images/icon/ico-downloadfile.png" /></a>' : 'N/A' ?>
	</td>
	<?php endforeach; ?>
</tr>
<?php endif; ?>					