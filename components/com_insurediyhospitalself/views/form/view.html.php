<?php

defined('_JEXEC') or die;

class InsureDIYHospitalSelfViewForm extends JViewLegacy {

	protected $form;
	protected $item;
	protected $state;

	public function display($tpl = null) {
		$session = JFactory::getSession();
		$model = $this->getModel();
		$app = JFactory::getApplication();
		$params = JComponentHelper::getParams('com_insurediyhospitalself');
		$user = JFactory::getUser();
		$form = $this->get("Form");
		$quotation = InsureDIYHospitalSelfHelper::getQuotation();
		$currentUser = MyHelper::getCurrentUser();

		$menu_params = $app->getMenu()->getActive()->params;
		$this->menu_params = $menu_params;
		
		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseWarning(500, implode("\n", $errors));
			return false;
		}

		$layout = InsureDIYHospitalSelfHelper::getLayout();
		//Re Generate Unique Order No
		$db = JFactory::getDBO();
		$dataobj = new stdClass();
		$dataobj->id = $quotation['id'];
		$dataobj->unique_order_no = InsureDIYHelper::getUniqueOrderNo($user->id);
		switch ($layout) {
			case 'step1':
				$this->askTna = is_null($session->get('hospitalself.protection_needs_analysis', NULL));
			case 'step2':
				$this->questionnaires = $this->get('Questionnaires');
				$this->step1CriteriaPass = $session->get('hospitalself.step1CriteriaPass', TRUE);
				$this->ask4override = $session->get('hospitalself.step1.ask4override', TRUE);
				break;
			case 'step3':
				$this->policy_plans = $this->get('PolicyPlans');
				break;
			case 'step4':
				$referred_by = $session->get(SESSION_KEY_REFID, FALSE);
				if (!$currentUser->referred_by && $referred_by) {
					$form->setValue('referred_by', "", $referred_by);
				}
				break;
			case 'step5':
				$quotation['complete_address'] = InsureDIYHospitalSelfHelper::getCompleteAddress($quotation);
				$db->updateObject('#__insure_hospitalself_quotations', $dataobj, "id");
				$this->paymentData = $model->getPDPayment();
				if (!$this->paymentData) {
					$app->enqueueMessage(JText::_("ERR_PAYMENT_CONFIG"));
				}
				break;
			case 'step6':
				//$db->updateObject('#__insure_hospitalself_quotations', $dataobj, "id");
				$time = strtotime($quotation['created_date']);
				$year = (int) date("Y", $time);
				$month = (int) date("m", $time);
				$day = (int) date("d", $time);

				$x_days = (int) $params->get('x_days');
				$y_days = (int) $params->get('y_days');

				$quotation['sign_start'] = date("d M Y", mktime(0, 0, 0, $month, ($day + $x_days), $year));
				$quotation['sign_end'] = date("d M Y", mktime(0, 0, 0, $month, ($day + $x_days + $y_days), $year));
				$quotation['complete_address'] = InsureDIYHospitalSelfHelper::getCompleteAddress($quotation);

				// data for email
				$session->set("hospitalself.maildata.sign_start." . $quotation['id'], $quotation['sign_start']);
				$session->set("hospitalself.maildata.sign_end." . $quotation['id'], $quotation['sign_end']);
				break;
			default:
				break;
		}

		$this->quotation = $quotation;
		$this->setLayout($layout);
		$this->state = $this->get('State');
		$this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));
		$this->currency = $params->get("currency", "HK$");
		$this->params = $params;
		$this->user = $user;
		$this->current_user = $currentUser;
		$this->form = $form;
		$this->flag_banner_path = 'images/flag_banners';
		$this->_prepareDocument();
		
		switch ($layout) {
			case 'thankyou':
			InsureDIYHospitalSelfHelper::clearSessionData(); // woot~ all done. clear the session
			break;
		}
		
		parent::display($tpl);
	}

	/**
	 * Prepares the document
	 */
	protected function _prepareDocument() {
		$app = JFactory::getApplication();
		$menus = $app->getMenu();
		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();

		$head = JText::_('COM_INSUREDIYHOSPITALSELF_FORM_PAGE_HEADING');
		if ($menu) {
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		} else {
			$this->params->def('page_heading', $head);
		}

		$title = $this->params->def('page_title', $head);
		if ($app->getCfg('sitename_pagetitles', 0) == 1) {
			$title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
		} elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
			$title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
		}
		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description')) {
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords')) {
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots')) {
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}

//		//facebook stuffs
//		$this->document->setMetaData("og:title", InsureDIYHospitalSelfHelper::getFbTitle());
//		$this->document->setMetaData("og:description", InsureDIYHospitalSelfHelper::getFbDesc());
//		$this->document->setMetaData("og:image", InsureDIYHospitalSelfHelper::getFbImage());
//		$this->document->setMetaData("og:app_id", InsureDIYHospitalSelfHelper::getFbAppId());
//		$this->document->setMetaData("og:site_name", InsureDIYHospitalSelfHelper::getFbSiteName());
	}

}
