<?php

defined('_JEXEC') or die;

class InsureDIYHospitalSelfControllerForm extends JControllerForm {

	private $base_layout_url = "index.php?option=com_insurediyhospitalself&view=form&layout=";
	private $form_url = "index.php?option=com_insurediyhospitalself&Itemid=170";

	public function getModel($name = 'form', $prefix = '', $config = array('ignore_request' => true)) {
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}

	public function step0save() {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$app = JFactory::getApplication();
		$session = JFactory::getSession();
		$session->set('hospitalself.protection_needs_analysis', 1);
		$app->redirect(JRoute::_("index.php?option=com_calculator&view=hospital&return=hospital&Itemid=187"));
	}

	public function step1save() {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$session = JFactory::getSession();
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$post = $app->input->get('jform', '', 'array');
		$step1CriteriaPass = $this->step1CriteriaCheck($post);

		//prepare data for contact detail page
		$phonecode = InsureDIYHelper::getCountryPhoneCode($post['country_residence']);
		$post['contact_country'] = $post['country_residence'];
		$post['contact_country_code'] = $phonecode;
		$quotation_id = $model->step1save($post);
		if ($quotation_id) {
			$session->set('hospitalself.step1CriteriaPass', $step1CriteriaPass);
			$session->set('hospitalself.quotation_id', $quotation_id);
		}
		
		$layout = InsureDIYHospitalSelfHelper::getLayout();
		
		if($layout == 'login') {
			$app->redirect(JRoute::_($this->form_url.'&step=sign_in'));
		} else {
			$app->redirect(JRoute::_($this->form_url.'&step=2'));
		}
		
	}

	public function step1CriteriaCheck($data) {
		return !($data['has_weight_change'] || $data['has_used_tobacco'] || $data['has_used_alcohol'] || $data['has_used_drugs'] || $data['has_reside_overseas'] || $data['has_claimed_insurance']);
	}

	public function checkOverrides() {
		$session = JFactory::getSession();
		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		$quotation_id = InsureDIYHospitalSelfHelper::getCurrentQid();
		if ($quotation_id && !$user->get("guest")) {
			$data = $session->get('hospitalself.data');
			$userData = MyHelper::getCurrentUser();
			$same = TRUE;
			if (isset($data['gender']) && isset($userData->gender) && strlen($userData->gender) > 0 && $data['gender'] != $userData->gender) {
				$same = FALSE;
			}
			if ($same && isset($data['dob']) && isset($userData->dob) && strtotime($userData->dob) > 0 && strtotime($data['dob']) != strtotime($userData->dob)) {
				$same = FALSE;
			}
			if ($same && isset($data['marital_status']) && isset($userData->marital_status) && strlen($userData->marital_status) > 0 && $data['marital_status'] != $userData->marital_status) {
				$same = FALSE;
			}
			if ($same && isset($data['monthly_salary']) && isset($userData->monthly_income) && strlen($userData->monthly_income) > 0 && $data['monthly_salary'] != $userData->monthly_income) {
				$same = FALSE;
			}
			if ($same && isset($data['country_residence']) && isset($userData->country) && strlen($userData->country) > 0 && $data['country_residence'] != $userData->country) {
				$same = FALSE;
			}
			if ($same && isset($data['occupation']) && isset($userData->occupation) && strlen($userData->occupation) > 0 && $data['occupation'] != $userData->occupation) {
				$same = FALSE;
			}

			$session->set("hospitalself.step1.ask4override", $same);
		}
		$app->redirect(JRoute::_($this->form_url.'&step=2'));
	}

	public function step2RefreshInfo() {
		$app = JFactory::getApplication();
		$session = JFactory::getSession();
		$quotation_id = InsureDIYHospitalSelfHelper::getQid();

		$model = $this->getModel();
		$user = $model->getCurrentUser();
		$data = array();
		if ($user->gender) {
			$data['gender'] = $user->gender;
		}
		if ($user->monthly_salary) {
			$data['monthly_income'] = $user->monthly_salary;
		}
		if ($user->occupation) {
			$data['occupation'] = $user->occupation;
		}
		if ($user->dob != '0000-00-00') {
			$data['dob'] = $user->dob;
		}
		if ($user->country) {
			$data['country_residence'] = $user->country;
		}
		if ($user->marital_status) {
			$data['marital_status'] = $user->marital_status;
		}
		if ($model->updateQuotation($quotation_id, $data)) {
			$session->set("hospitalself.step1.ask4override", NULL);
		}
		$app->redirect(JRoute::_($this->form_url.'&step=2'));
	}

	public function step2save() {
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$post = $app->input->get('jform', '', 'array');
		if ($model->step2save($post)) {
			$continueWithError = $app->input->get('continueWithError', TRUE);
			if (!$continueWithError) {
				InsureDIYHospitalSelfHelper::clearSessionData();
				$app->redirect("index.php");
			}
		}
		$app->redirect(JRoute::_($this->form_url.'&step=3'));
	}

	public function step3save() {
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$check = TRUE;
		$plan_id = $app->input->post->get('plan_id', FALSE, 'int');
		$quotation_id = InsureDIYHospitalSelfHelper::getQid();
		if ($quotation_id && $plan_id) {
			$check = $model->step3save($quotation_id, $plan_id);
		} else {
			$check = FALSE;
		}
		if ($check) {
			$app->redirect(JRoute::_($this->form_url.'&step=4'));
		} else {
			$app->redirect(JRoute::_($this->form_url.'&step=3'), JText::_("DB_SAVE_FAIL"));
		}
	}

	public function step4save() {
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$post = $app->input->get('jform', '', 'array');
		$referred_by = $post['referred_by'];
		if (!InsureDIYHelper::checkReferral($referred_by)) { // referral is valid
			unset($post['referred_by']);
		}
		if (!$model->step4save($post)) {
			$app->redirect(JRoute::_($this->form_url.'&step=4'), JText::_("DB_SAVE_FAIL"));
		}
		$app->redirect(JRoute::_($this->form_url.'&step=5'));
	}

	public function step5save() {
		$app = JFactory:: getApplication();
		$model = $this->getModel();
		$quotation_id = InsureDIYHospitalSelfHelper::getQid();

		$post = $app->input->get('jform', '', 'array');
		$post['id'] = $quotation_id;

		if (!$model->step5save($post)) {
			$app->redirect(JRoute::_($this->form_url.'&step=5'), JText::_("DB_SAVE_FAIL"));
		}
		$app->redirect(JRoute::_($this->form_url.'&step=6'));
	}

	public function step6save() {
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$quotation_id = InsureDIYHospitalSelfHelper::getQid();

		$post = $app->input->get('jform', '', 'array');
		$post['id'] = $quotation_id;
		$this->saveFileUpload(FALSE);
		$check = $model->step6save($post);
		if ($check) {
			$model->createPolicyRecord($quotation_id);
			$model->createPoints($quotation_id);
			// Send email n generate pdf
//			$pdfarr = array();
//			$pdfarr[] = array();
//			$model->sendEmails($pdfarr);
//			$model->sendEmails($pdfarr, FALSE);
		} else {
			$app->redirect($this->form_url, JText::_("DB_SAVE_FAIL"));
		}
		//InsureDIYHospitalSelfHelper::clearSessionData(); // woot~ all done. clear the session
		$app->redirect($this->getRedirectUrl('thankyou'));
	}

	public function saveBeforePayment() {
		$app = JFactory::getApplication();
		$post = $app->input->post->getArray();
		$model = $this->getModel();
		echo json_encode($model->saveConfirmationDetail($post));
		exit;
	}

	public function saveFileUpload($redirect = TRUE) {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$app = JFactory::getApplication();
		$files = $app->input->files->get('jform', null);
		$check = $files['file_identity_card'] ['error'] >
				0 && $files['file_birth_cert']['error'] > 0 && $files['file_proof_address']['error'] > 0;
		if ($check) {
			$msg = 'No File Uploaded';
		} else {
			$model = $this->getModel();
			$path = 'media/com_insurediyhospitalself/documents';
			$row1 = $model->savePhotoIdentityFileUpload($files['file_identity_card'], $path, 'file_identity_card');
			$row3 = $model->savePhotoIdentityFileUpload($files['file_proof_address'], $path, 'file_proof_address');
		}
		if ($redirect) {
			$app->redirect(JRoute::_($this->form_url.'&step=7'), $msg);
		}
	}

	public function deleteDocuments() {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$app = JFactory::getApplication();
		$db = JFactory::getDBO();
		$quotation_id = InsureDIYHospitalSelfHelper::getQid();
		$file_type = JRequest::getVar('file_type');
		$query = " SELECT " . $file_type . " FROM #__insure_hospitalself_quotations WHERE id = '$quotation_id' LIMIT 1 ";
		$file_entry = $db->setQuery($query)->loadResult();
		$file_entry = explode("|", $file_entry);
		$filename = (isset($file_entry[0])) ? $file_entry[0] : FALSE;
		if ($filename) {
			$query = " UPDATE #__insure_hospitalself_quotations SET " . $file_type . " = '' WHERE id = '$quotation_id' ";
			$db->setQuery($query);
			$db->query();
			unlink(JPATH_BASE . '/media/com_insurediyhospitalself/documents/' . $filename);
			$oriname = (isset($file_entry[1])) ? $file_entry[1] : $filename;
			$msg = $oriname . ' has been deleted ';
		} else {
			$msg = 'Error deleting file.';
		}
		InsureDIYHospitalSelfHelper::overrideSessionWithQid($quotation_id);
		$app->redirect(JRoute::_($this->form_url.'&step=7'), $msg);
	}

	public function getRedirectUrl($layout) {
		$url = $this->base_layout_url . $layout;
		return $url;
	}

	public function updatePlanOptions() {
		JSession::checkToken() or jexit(JText::_("JINVALID_TOKEN"));
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$quotation_id = InsureDIYHospitalSelfHelper::getQid();
		$post = $app->input->get('jform', '', 'array');
		if (!$model->updateQuotation($quotation_id, $post)) {
			$app->redirect(JRoute::_($this->form_url.'&step=3'), JText::_("DB_SAVE_FAIL"));
		}
		$app->redirect(JRoute::_($this->form_url.'&step=3'));
	}

	public function back() {
		JSession::checkToken() or jexit(JText::_("JINVALID_TOKEN"));
		$app = JFactory::getApplication();
		$quotation_id = $app->input->post->get("quotation_id", FALSE, "integer");
		$backstage = 0;
		if ($quotation_id) {
			$model = $this->getModel();
			$backstage = $model->back($quotation_id);
		}
		$app->redirect(JRoute::_($this->form_url.'&step='.$backstage));
	}

}
