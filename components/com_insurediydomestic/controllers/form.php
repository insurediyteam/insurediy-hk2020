<?php

defined('_JEXEC') or die;

class InsureDIYDomesticControllerForm extends JControllerForm {

	private $base_layout_url = "index.php?option=com_insurediydomestic&view=form&layout=thankyou";
	private $form_url = "index.php?option=com_insurediydomestic&Itemid=161";

	public function getModel($name = 'form', $prefix = '', $config = array('ignore_request' => true)) {
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}

	public function step1save() {
		$session = JFactory::getSession();
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$post = $app->input->get('jform', '', 'array');
		$quotation_id = $model->step1save($post);
		if ($quotation_id) {
			$session->set('domestic.quotation_id', $quotation_id);
		}
		
		$layout = InsureDIYDomesticHelper::getLayout();
		
		if($layout == 'login') {
			$app->redirect(JRoute::_($this->form_url.'&step=sign_in'));
		} else {
			$app->redirect(JRoute::_($this->form_url.'&step=2'));
		}
	}

	public function step2save() {
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$check = TRUE;
		$plan_id = $app->input->post->get('plan_id', FALSE, 'int');
		$quotation_id = InsureDIYDomesticHelper::getQid();

		if ($quotation_id && $plan_id) {
			$check = $model->step2save($quotation_id, $plan_id) && $model->updateStage($quotation_id, 3);
		} else {
			$app->redirect($this->getRedirectUrl(), JText::_("ERR_NO_PLAN_SELECTED"));
		}
		$app->redirect(JRoute::_($this->form_url.'&step=3'));
	}

	public function step3save() {
		$app = JFactory::getApplication();
		$session = JFactory::getSession();
		$quotation = $session->get("domestic.data");
		$model = $this->getModel();
		$post = $app->input->get('jform', '', 'array');
		$quotation_id = InsureDIYDomesticHelper::getQid();
		$user = MyHelper::getCurrentUser();
		$referred_by = $post['referred_by'];
		if (!InsureDIYHelper::checkReferral($referred_by)) { // referral is valid
			unset($post['referred_by']);
		}

		if ($model->step3save($post)) {
			$model->deleteAllHelpers($quotation_id);
			for ($i = 0; $i < $quotation['no_of_helper']; $i++) {
				$post = $app->input->get('jform' . ($i + 1), '', 'array');
				$model->saveHelper($post);
			}
			$model->updateStage($quotation_id, 4);
			InsureDIYDomesticHelper::overrideSessionWithQid($quotation_id);
			$app->redirect(JRoute::_($this->form_url.'&step=4'));
		}
		$app->redirect(JRoute::_($this->form_url.'&step=3'), JText::_("DB_SAVE_FAIL"));
	}

	public function step5save() {
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$quotation_id = InsureDIYDomesticHelper::getQid();

		$post = $app->input->get('jform', '', 'array');
		$post['id'] = $quotation_id;

		$check = $model->step5save($post);
		if ($check) {
			$model->updateStage($quotation_id, '6');
			InsureDIYDomesticHelper::overrideSessionWithQid($quotation_id);
			$app->redirect(JRoute::_($this->form_url.'&step=6'));
		} else {
			$app->redirect(JRoute::_($this->form_url.'&step=5'), JText::_("DB_SAVE_FAIL"));
		}
	}

	public function step6save() {
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$quotation_id = InsureDIYDomesticHelper::getQid();

		$post = $app->input->get('jform', '', 'array');
		$post['id'] = $quotation_id;

		$check = $model->step6save($post);
		if ($check) {
			$this->createPoints($quotation_id);

			$model->updateStage($quotation_id, '7');
			$pdfarr = array();
			$pdfarr[] = array();
			$model->sendEmails($pdfarr);
			$model->sendEmails($pdfarr, FALSE);
			$model->createPolicyRecord($quotation_id);
		}
		$app->redirect(JRoute::_($this->form_url.'&step=7'));
	}

	public function saveBeforePayment() {
		$app = JFactory::getApplication();
		$post = $app->input->post->getArray();
		$model = $this->getModel();
		echo json_encode($model->saveConfirmationDetail($post));
		exit;
	}

	/*public function saveFileUpload() {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$app = JFactory::getApplication();
		$files = $app->input->files->get('jform', null);
		if ($files['file_identity_card']['error'] > 0 && $files['file_birth_cert']['error'] > 0 && $files['file_proof_address']['error'] > 0) {
			$msg = 'No File Uploaded';
		} else {
			$model = $this->getModel();
			$path = 'media/com_insurediydomestic/documents';
			$row1 = $model->savePhotoIdentityFileUpload($files['file_identity_card'], $path, 'file_identity_card');
			$row3 = $model->savePhotoIdentityFileUpload($files['file_proof_address'], $path, 'file_proof_address');
		}
		$app->redirect($this->getRedirectUrl(), $msg);
	}

	public function deleteDocuments() {
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$app = JFactory::getApplication();
		$db = JFactory::getDBO();
		$quotation_id = InsureDIYDomesticHelper::getQid();
		$file_type = JRequest::getVar('file_type');
		$query = " SELECT " . $file_type . " FROM #__insure_domestic_quotations WHERE id = '$quotation_id' LIMIT 1 ";
		$file_entry = $db->setQuery($query)->loadResult();
		$file_entry = explode("|", $file_entry);
		$filename = (isset($file_entry[0])) ? $file_entry[0] : FALSE;
		if ($filename) {
			$query = " UPDATE #__insure_domestic_quotations SET " . $file_type . " = '' WHERE id = '$quotation_id' ";
			$db->setQuery($query);
			$db->query();
			unlink(JPATH_BASE . '/media/com_insurediydomestic/documents/' . $filename);
			$msg = $filename . ' has been deleted ';
		} else {
			$msg = 'Error deleting file.';
		}
		InsureDIYDomesticHelper::overrideSessionWithQid($quotation_id);
		$app->redirect($this->getRedirectUrl(), $msg);
	}*/

	/*public function startNewQuotation() {
		JSession::checkToken() or jexit(JText::_("JINVALID_TOKEN"));
		$app = JFactory::getApplication();
		InsureDIYDomesticHelper::clearSessionData();
		$app->redirect($this->base_layout_url . "&Itemid=170");
	}*/

	public function getRedirectUrl() {
		$qid = InsureDIYDomesticHelper::getInputQid();
		$url = $this->base_layout_url;
		if ($qid) {
			$url.="quotation_id=" . $qid;
		}
		return JRoute::_($url);
//		return JRoute::_($url, FALSE);
	}

	/*public function updatePlanOptions() {
		JSession::checkToken() or jexit(JText::_("JINVALID_TOKEN"));
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$quotation_id = InsureDIYDomesticHelper::getQid();
		$post = $app->input->get('jform', '', 'array');
		if ($model->updateQuotation($quotation_id, $post)) {
			$app->redirect($this->base_layout_url . "&Itemid=170");
		} else {
			$app->redirect($this->base_layout_url . "&Itemid=170", JText::_("DB_SAVE_FAIL"));
		}
	}*/

	public function back() {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$app = JFactory::getApplication();
		$quotation_id = $app->input->post->get("quotation_id", FALSE, "integer");
		$backstage = 0;
		if ($quotation_id) {
			$model = $this->getModel();
			$backstage = $model->back($quotation_id);
		}
		$app->redirect(JRoute::_($this->form_url.'&step='.$backstage));
	}

}
