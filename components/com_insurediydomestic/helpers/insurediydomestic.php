<?php

defined('_JEXEC') or die;

class InsureDIYDomesticHelper {

	public static function renderHeader($icon, $header, $active) {
		$classes = array();
		switch ($active) {
			case 1:
				$classes = array('-active', '', '', '-inactive', '', '-inactive', '', '');
				break;
			case 2:
				$classes = array('-active', '-active', '-active', '', '', '-inactive', '', '');
				break;
			case 3:
				$classes = array('-active', '-active', '-active', '-active', '-active', '', '', '');
				break;
			case 4:
				$classes = array('-active', '-active', '-active', '-active', '-active', '-active', '-active', '-active');
				break;
			default:
				break;
		}

		$buffer = '<div class="header-top">';
		$buffer.= '<div class="h1-heading">';
		$buffer.= '<i class="' . $icon . '" ></i>';
		$buffer.= $header;
		$buffer.='</div>';
		$buffer.= '<div class="insurediy-breadcrumbs">';
		$buffer.= '<div class="mybreadcrumb back"></div>';
		$buffer.= '<div class="mybreadcrumb middle' . $classes[0] . '">';
		$buffer.= JText::_("BREADCRUMB_YOUR_DETAILS") . '</div>';
		$buffer.='<div class="mybreadcrumb between' . $classes[1] . '">';
		$buffer.= '</div><div class = "mybreadcrumb middle' . $classes[2] . '">';
		$buffer.= JText::_("BREADCRUMB_SELECT_PRODUCT");
		$buffer.='</div>';
		$buffer.='<div class="mybreadcrumb between' . $classes[3] . '">';
		$buffer.='</div>';
		$buffer.='<div class="mybreadcrumb middle' . $classes[4] . '">';
		$buffer.= JText::_("BREADCRUMB_APPLY") . '</div>';
		$buffer.='<div class="mybreadcrumb between' . $classes[5] . '">';
		$buffer.='</div>';
		$buffer.='<div class="mybreadcrumb middle' . $classes[6] . '">';
		$buffer.= JText::_("BREADCRUMB_PAYMENT") . '</div>';
		$buffer.='<div class="mybreadcrumb front' . $classes[7] . '"></div>';
		$buffer.='<div class="clear"></div>';
		$buffer.= '</div>';
		$buffer.= '<div class="clear"></div></div>';
		return $buffer;
	}

	public static function getCompleteAddress($data) {
		$complete_address = '';
		$complete_address .= ($data['contact_room_no'] ? 'Flat ' . $data['contact_room_no'] : '');
		$complete_address .= ($data['contact_floor_no'] ? ', ' . $data['contact_floor_no'] . '/F' : '');
		$complete_address .= ($data['contact_block_no'] ? ', Blk ' . $data['contact_block_no'] : '');
		$complete_address .= ($data['contact_building_name'] ? ', ' . $data['contact_building_name'] : '');
		$complete_address .= ($data['contact_street_name'] ? ', ' . $data['contact_street_name'] : '');
		$complete_address .= ($data['contact_district_name'] ? ', ' . $data['contact_district_name'] : '');
		$complete_address .= ($data['contact_country'] ? ', ' . $data['contact_country'] . ' ' : '');
		return $complete_address;
	}

	// PARAMS
	public static function getAppSummeryMod($default = "") {
		$params = JComponentHelper::getParams("com_insurediydomestic");
		return $params->get("app_summery", $default);
	}

	public static function getDeclarationsMod($default = "") {
		$params = JComponentHelper::getParams("com_insurediydomestic");
		return $params->get("declarations", $default);
	}

	public static function getCurrency($default = "HK$") {
		$params = JComponentHelper::getParams("com_insurediydomestic");
		return $params->get("currency", $default);
	}

	public static function getPremiumMonth($default = 3) {
		$params = JComponentHelper::getParams("com_insurediydomestic");
		return $params->get("premium_month", $default);
	}

	public static function getFbTitle($default = "") {
		$params = JComponentHelper::getParams("com_insurediydomestic");
		return $params->get("fbtitle", $default);
	}

	public static function getFbDesc($default = "") {
		$params = JComponentHelper::getParams("com_insurediydomestic");
		return $params->get("fbdesc", $default);
	}

	public static function getFbImage($default = "") {
		$params = JComponentHelper::getParams("com_insurediydomestic");
		return $params->get("fbimage", $default);
	}

	public static function getFbSiteName($default = FALSE) {
		$default = ($default) ? $default : JFactory::getConfig()->get("sitename", "");
		$params = JComponentHelper::getParams("com_insurediydomestic");
		return $params->get("fbsitename", $default);
	}

	public static function getFbAppId($default = "") {
		$params = JComponentHelper::getParams("com_insurediydomestic");
		return $params->get("fbappid", $default);
	}

	public static function getRefExp($default = "") {
		$params = JComponentHelper::getParams("com_insurediydomestic");
		return $params->get("refExp", $default);
	}

	public static function getRefMsg($default = "") {
		$params = JComponentHelper::getParams("com_insurediydomestic");
		return str_replace("XXYY", JFactory::getUser()->referral_id, $params->get("refMsg", $default));
	}

	// QUOTATION STUFFS
	public static function isOldQuote() {
		return self::getQid();
	}

	public static function getCurrentQid() {
		return JFactory::getSession()->get("domestic.quotation_id", 0);
	}

	public static function getInputQid() {
		return JFactory::getApplication()->input->get("quotation_id", 0, "integer");
	}

	public static function getQid() {
		return self::getInputQid() ? self::getInputQid() : self::getCurrentQid();
	}

	public static function canViewLayout($layout) {
		$user = JFactory::getUser();
		$id = self::getQid();
		$requestedStage = self::getStageFromLayout($layout);
		$currentStage = self::getCurrentStage($id);
		$return = new stdClass();
		if ($user->get("guest") && $requestedStage == 99 && self::getCurrentQid()) {
			$return->can = TRUE;
			$return->msg = "";
			return $return;
		}
		if ($requestedStage > 1 && $requestedStage != 99 && $user->get("guest")) {
			$return->can = FALSE;
			$return->msg = JText::_("ERR_LOGIN_TO_VIEW");
			return $return;
		}
		if ($currentStage > 2 && !self::checkOwnership($id)) {
			$return->can = FALSE;
			$return->msg = JText::_("ERR_NO_ACCESS_TO_VIEW");
			return $return;
		}

		$return->can = $requestedStage <= $currentStage;
		$return->msg = JText::_("ERR_SKIP_STEPS");
		return $return;
	}

	public static function getCurrentLayout($stage) {
		$validstages = array(2, 3, 4, 5);
		if (in_array($stage, $validstages)) {
			return self::getLayoutFromStage($stage);
		}
		return FALSE;
	}

	public static function getLayout() {
		$user = JFactory::getUser();
		$app = JFactory::getApplication();

		$inputLayout = $app->input->get("layout", FALSE);
		$staticLayouts = array("error", "thankyou", "canceled", "syserror");
		if ($inputLayout && in_array($inputLayout, $staticLayouts)) {
			return $inputLayout;
		}

		$qid = self::getQid();
		if (!$qid) {
			return "step1";
		}
		$stage = self::getCurrentStage($qid);
		if ($stage > 1 && $user->id < 1) {
			return "login";
		}
		return self::getLayoutFromStage($stage);
	}

	public static function checkOwnership($id) {
		$user = JFactory::getUser();
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("count(*)")
				->from("#__insure_domestic_quotations")
				->where("user_id = " . $db->quote($user->id))
				->where("id = " . $db->quote($id));
		$db->setQuery($query);
		return $db->loadResult();
	}

	public static function getCurrentStage($id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("quote_stage")
				->from("#__insure_domestic_quotations")
				->where("id = " . $db->quote($id));
		$db->setQuery($query);
		return $db->loadResult();
	}

	public static function getLayoutFromStage($step) {
		$stageToLayouts = array(1 => "step1", 2 => "step2", 3 => "step3", 4 => "step4", 5 => "thankyou");
		return isset($stageToLayouts[$step]) ? $stageToLayouts[$step] : "step1";
	}

	public static function getStageFromLayout($layout) {
		$layoutToStages = array("step1" => 1, "login" => 99, "step2" => 2, "step3" => 3, "step4" => 4, "thankyou" => 5);
		return isset($layoutToStages[$layout]) ? $layoutToStages[$layout] : 0;
	}

	public static function clearSessionData() {
		$session = JFactory::getSession();
		$session->set("domestic.data", NULL);
		$session->set("domestic.quotation_id", NULL);
	}

	public static function overrideSessionWithQid($qid) {
		$session = JFactory::getSession();
		$session->set("domestic.quotation_id", $qid);
		$session->set("domestic.data", self::getQuotation($qid));
	}

	public static function getQuotation($qid = FALSE, $checkUser = TRUE) {
		if (!$qid) {
			$qid = self::getQid();
		}
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_domestic_quotations")
				->where("id = " . $db->quote($qid));
		if($checkUser){
			$query->where("user_id = " . $db->quote(JFactory::getUser()->id));
		}
		$quotation = $db->setQuery($query)->loadAssoc();
		if ($quotation) {
			$planQuery = $db->getQuery(TRUE)
					->select("qtp.*, c.company_name")
					->from("#__insure_domestic_quotations_to_plans AS qtp, #__insure_companies AS c")
					->where("qtp.quotation_id = " . $db->quote($quotation['id']))
					->where("qtp.insurer_code = c.insurer_code");

			$quotation['selected_plan'] = $db->setQuery($planQuery)->loadObject();

			$helperQuery = $db->getQuery(TRUE)
					->select("*")
					->from("#__insure_domestic_quotations_to_helpers")
					->where("quotation_id = " . $db->quote($quotation['id']));
			$helpers = $db->setQuery($helperQuery)->loadObjectList();
			$quotation['helpers'] = ($helpers) ? $helpers : array();
		}
		return $quotation;
	}

	public static function getCurrentUserContacts() {
		$fields = array(
			"contact_firstname" => "name", "contact_lastname" => "lastname", "contact_country_code" => "country_code",
			"contact_contact_no" => "contact_no", "contact_room_no" => "room_no", "contact_floor_no" => "floor_no",
			"contact_block_no" => "block_no", "contact_building_name" => "building_name", "contact_street_name" => "street_no",
			"contact_district_name" => "district", "contact_postalcode" => "postal_code", "contact_country" => "country"
		);
		$userData = MyHelper::getCurrentUser();
		$tmp = array();
		foreach ($fields as $key => $field) {
			$tmp[$key] = $userData->$field;
		}
		return $tmp;
	}

}
