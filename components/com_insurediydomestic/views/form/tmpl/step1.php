<?php
	defined('_JEXEC') or die;
	
	JHtml::_('behavior.keepalive');
	JHtml::_('behavior.formvalidation');
	JHtml::_('formbehavior.chosen', '.insurediy-form-content select');
	JHtml::_('script', 'system/jquery.validate.js', false, true);
	
	$params = $this->state->get('params');
	$itemid = JRequest::getVar("itemid", FALSE);
	$form = $this->form;
?>

<script type="text/javascript">
	jQuery(document).ready(function() {
		jQuery("#adminForm").validate({
			ignore: [], // <-- option so that hidden elements are validated
		});
	});
</script>

<!-- Google Tag Manager Data Layer-->
<script>
	/**
		* Call this function when a user clicks on a product link. This function uses the
		event
		* callback datalayer variable to handle navigation after the ecommerce data has
		been sent
		* to Google Analytics.
		* @param {Object} productObj An object representing a
		product.
		*/
		function pushProductClick(productObj) {
			dataLayer.push({
				'event': 'productClick',
				'ecommerce': {
					'click': {
						'actionField': {'list': productObj.list}, // Optional list property.
						'products': [{
							'name': productObj.name,
							// Name or ID is required.
							'id': productObj.id
						}]
					}
				},
				'eventCallback': function() {
				}
			});
		}
	</script>
	<!-- End of Google Tag Manager Data Layer-->
	
	<div class="insurediy-form">
		<div class="header-top-wrapper">
			<?php echo InsureDIYDomesticHelper::renderHeader('icon-domestic', JText::_('COM_INSUREDIYDOMESTIC_PAGE_HEADING_ABOUT_YOUR_DOMESTIC_HELPER'), 1); ?>
		</div>
		<div class="edit<?php echo $this->pageclass_sfx; ?> insurediy-form-content">
			<?php echo MyHelper::renderDefaultMessage(); ?>
			<form action="<?php echo JRoute::_('index.php?option=com_insurediydomestic&view=form'); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-vertical" enctype="multipart/form-data">
				<fieldset class="insurediy-form-fields">
					<!-- Row 1 -->
					<div class="control-row clearfix">
						<div class="cpan2">
							<div class="control-group">
								<div class="control-label">
									<?php echo $this->form->getLabel('duration'); ?>
									<div id="error-container"></div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
						<div class="cpan3">
							<div class="control-group">
								<div class="controls"><?php echo $this->form->getInput('duration'); ?>
									<div class="error-container"></div>
								</div>
							</div>
							<div class="clear"></div>
						</div>
						<div class="clear"></div>
					</div>
					<!-- Row 1 end -->
					
					<!-- Row 2 -->
					<div class="control-row clearfix">
						<div class="cpan2 clearfix">
							<div class="control-group">
								<div class="control-label"><?php echo $this->form->getLabel('no_of_helper'); ?></div>
							</div>
						</div>
						<div class="cpan3 clearfix">
							<div class="cpan2 control-group">
								<div class="controls"><?php echo $this->form->getInput('no_of_helper'); ?>
									<div class="error-container"></div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<!-- Row 2 end -->
					
					<!-- Row 3 -->
					<div class="control-row clearfix">
						<div class="cpan2 clearfix">
							<div class="control-group">
								<div class="control-label"><?php echo $this->form->getLabel('is_oversea'); ?></div>
								
							</div>
						</div>
						<div class="cpan3 clearfix">
							<div class="cpan2 control-group">
								<div class="controls"><?php echo $this->form->getInput('is_oversea'); ?>
									<div class="error-container"></div>
								</div>
							</div>
						</div>
						<div class="clear"></div>
					</div>
					<!-- Row 3 end -->
					
					<div class="clear"></div>
				</fieldset>
				<fieldset class="insurediy-agreements">
					<?php echo MyHelper::load_module_pos("agreements"); ?>
					<?php if (FALSE): ?>
					<?php foreach ($this->form->getFieldset('agree_fields') as $field): ?>
					<div>
						<div class="control-group">
							<div class="control-label" style="float:left;width:4.5%;"><div style="position:relative;"><?php echo $field->input; ?></div></div>
							<div class="controls" style="float:left;width:95%"><?php echo $field->label; ?>
								<div class="error-container"></div>
							</div>
							<div style="clear:both"></div>
						</div>
					</div>
					<?php endforeach; ?>
					<?php endif; ?>
				</fieldset>
				<input type="hidden" name="task" value="form.step1save" />
				<?php echo $this->form->getInput('id'); ?>
				<?php if ($itemid): ?>
				<input type="hidden" name="Itemid" value="$itemid" />
				<?php endif; ?>
				<?php echo JHtml::_('form.token'); ?>
				
				<div style="float:left;margin:15px 0;">
					{modulepos insurediy-secured-ssl}
				</div>
				<div class="btn-toolbar" style="float:right">
					<div class="btn-group">
						<button type="submit" class="btn btn-primary validate" onclick="pushProductClick({name:'DHI', id:'dhi01', list:'mainpage'})"><?php echo JText::_('JCONTINUE') ?></button>
					</div>
				</div>
				<div class="clear"></div>
			</form>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="logo-slider"><?php echo MyHelper::load_module(102); ?></div>
	
	<?php /* Popup Box */ ?>
	<div class="insurediy-popup" id="insurediy-popup-box" style="display:none;">
		<div class="header">
			<div class="text-header"><?php echo JText::_("POP_IMPORTANT_NOTICE"); ?></div>
			<a class="close" href="javascript:cleanInsureDIYPopup('insurediy-popup-box');">&nbsp;</a>
			<div class="clear"></div>
		</div>
		<div class="padding">
			<div><?php echo JText::_("COM_INSUREDIYDOMESTIC_MEDICAL_ISSUE_MSG"); ?></div>
			<div style="text-align:right;margin-top: 20px;">
				<input type="button" class="btn btn-primary" style="height:30px;padding:5px 20px;font-size:14px !important;" onclick="javascript:cleanInsureDIYPopup('insurediy-popup-box');
				return false;" value="<?php echo JText::_("JOK"); ?>" />
			</div>
		</div>
	</div>
	
	<script type="text/javascript">
		var jFamily, jGroup, groupType, endDateWrapper, endDateInput, tripType;
		
		jQuery(document).ready(function() {
			var jForm = jQuery("form#adminForm");
			var jSelects = jForm.find("select");
			var jInputs = jForm.find("input");
			
			jSelects.each(function() {
				var jThis = jQuery(this);
				jThis.on("change", selectOnChange);
			});
			jInputs.each(function() {
				var jThis = jQuery(this);
				jThis.on("change", selectOnChange);
			});
			
		});
		
		function selectOnChange() {
			var jThis = jQuery(this);
			var jControl = jThis.closest("div.controls");
			if (jThis.val()) {
				jControl.find(".error-container label.error").remove();
				} else {
				jControl.find("label.error").each(function() {
					var jThis = jQuery(this);
					var jContainer = jThis.closest("div.control-group");
					var jControl = jContainer.find("div.controls");
					jControl.find(".error-container").append(jThis.clone());
					jThis.remove();
				});
			}
		}
	</script>
