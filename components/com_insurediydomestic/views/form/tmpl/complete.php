<?php
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', '.insurediy-form-content select');

//JHtml::_('behavior.modal', 'a.modal_jform_contenthistory');
// Create shortcut to parameters.
$params = $this->state->get('params');
?>

<div class="insurediy-form">
	<div class="header-top-wrapper">
		<?php echo InsureDIYDomesticHelper::renderHeader('icon-domestic', JText::_('COM_INSUREDIYDOMESTIC_PAGE_HEADING_PAYMENT_ERROR'), 4); ?>
	</div>
	<div style="padding:30px;margin-top: 100px;">
		<?php echo MyHelper::renderDefaultMessage(); ?>
		<div class="edit<?php echo $this->pageclass_sfx; ?>">
			<div class="form-quote-box" style="width: 670px">
				<span style="color:#2f2f2f;font-size:37px;line-height: 40px;">
					<?php echo JText::_("COM_INSUREDIYDOMESTIC_PAGE_SUB_HEADING_PAYMENT_ERROR"); ?>
				</span>
				<div class="center" style="color:#2f2f2f;font-size:14px;margin-top: 15px;">
					<?php echo JText::_("COM_INSUREDIYDOMESTIC_PAGE_SUB_HEADING_PAYMENT_ERROR_EXPLANATION"); ?>
				</div>
			</div>
		</div>

	</div>
</div>
<div class="logo-slider"><?php echo MyHelper::load_module(102); ?></div>
