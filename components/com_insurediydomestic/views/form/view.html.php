<?php

defined('_JEXEC') or die;

class InsureDIYDomesticViewForm extends JViewLegacy {

	protected $form;
	protected $item;
	protected $state;

	public function display($tpl = null) {
		$app = JFactory::getApplication();
		$session = JFactory::getSession();
		$model = $this->getModel();
		$params = JComponentHelper::getParams('com_insurediydomestic');
		$user = JFactory::getUser();
		$form = $this->get("Form");
		$quotation = InsureDIYDomesticHelper::getQuotation();
		$currentUser = MyHelper::getCurrentUser();

		$menu_params = $app->getMenu()->getActive()->params;
		$this->menu_params = $menu_params;
		
		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseWarning(500, implode("\n", $errors));
			return false;
		}

		$layout = InsureDIYDomesticHelper::getLayout();
		//Re Generate Unique Order No
		$db = JFactory::getDBO();
		$dataobj = new stdClass();
		$dataobj->id = $quotation['id'];
		$dataobj->unique_order_no = InsureDIYHelper::getUniqueOrderNo($user->id);
		switch ($layout) {
			case 'step2':
				$this->policy_plans = $this->get('PolicyPlans');
				break;
			case 'step3':
				$referred_by = $session->get(SESSION_KEY_REFID, FALSE);
				if (!$currentUser->referred_by && $referred_by) {
					$form->setValue('referred_by', "", $referred_by);
				}
				break;
			case 'step4':
				$db->updateObject('#__insure_domestic_quotations', $dataobj, "id");
				$this->paymentData = $model->getPDPayment();
				$quotation['complete_address'] = InsureDIYDomesticHelper::getCompleteAddress($quotation);
				break;
			case 'step5':
				//$db->updateObject('#__insure_domestic_quotations', $dataobj, "id");
				break;
			case 'step6':
				$year = (int) date("Y", strtotime($quotation['created_date']));
				$month = (int) date("m", strtotime($quotation['created_date']));
				$day = (int) date("d", strtotime($quotation['created_date']));

				$x_days = (int) $params->get('x_days');
				$y_days = (int) $params->get('y_days');

				$quotation['sign_start'] = date("d M Y", mktime(0, 0, 0, $month, ($day + $x_days), $year));
				$quotation['sign_end'] = date("d M Y", mktime(0, 0, 0, $month, ($day + $x_days + $y_days), $year));
				$quotation['complete_address'] = InsureDIYDomesticHelper::getCompleteAddress($quotation);

				// data for email
				$session->set("domestic.maildata.sign_start." . $quotation['id'], $quotation['sign_start']);
				$session->set("domestic.maildata.sign_end." . $quotation['id'], $quotation['sign_end']);
				break;
			case 'syserror':
				$this->error = $app->input->get("error");
			default:
				break;
		}

		$this->quotation = $quotation;
		$this->setLayout($layout);
		$this->form = $form;
		$this->state = $this->get('State');
		$this->currency = $params->get("currency", "HK$");
		$this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));
		$this->params = $params;
		$this->user = $user;
		$this->current_user = $currentUser;
		$this->flag_banner_path = 'images/flag_banners';
		$this->_prepareDocument();
		
		switch ($layout) {
			case 'thankyou':
				InsureDIYDomesticHelper::clearSessionData();
			break;	
		}
		parent::display($tpl);
	}

	/**
	 * Prepares the document
	 */
	protected function _prepareDocument() {
		$app = JFactory::getApplication();
		$menus = $app->getMenu();
		$title = null;

		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();
		$head = JText::_('COM_INSUREDIYDOMESTIC_FORM_PAGE_HEADING');

		if ($menu) {
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		} else {
			$this->params->def('page_heading', $head);
		}

		$title = $this->params->def('page_title', $head);
		if ($app->getCfg('sitename_pagetitles', 0) == 1) {
			$title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
		} elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
			$title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
		}
		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description')) {
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords')) {
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots')) {
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}

		//facebook stuffs
		$this->document->setMetaData("og:title", InsureDIYDomesticHelper::getFbTitle());
		$this->document->setMetaData("og:description", InsureDIYDomesticHelper::getFbDesc());
		$this->document->setMetaData("og:image", InsureDIYDomesticHelper::getFbImage());
		$this->document->setMetaData("og:app_id", InsureDIYDomesticHelper::getFbAppId());
		$this->document->setMetaData("og:site_name", InsureDIYDomesticHelper::getFbSiteName());
	}

}
