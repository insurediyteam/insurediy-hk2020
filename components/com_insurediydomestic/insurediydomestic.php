<?php

defined('_JEXEC') or die;
JLoader::import('incs.form.field', JPATH_ROOT);

require_once JPATH_COMPONENT . '/helpers/insurediydomestic.php';

$controller = JControllerLegacy::getInstance('InsureDIYDomestic');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
