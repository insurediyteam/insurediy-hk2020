<?php

defined('_JEXEC') or die;

class InsureDIYDomesticController extends JControllerLegacy {

	protected $default_view = 'form';

	public function display() {
		$this->input->set('view', $this->default_view);
		$layout = $this->input->get('layout', "step1");
		$staticLayouts = array("error", "thankyou", "canceled", "syserror");
		if (in_array($layout, $staticLayouts)) {
			return parent::display();
		}

		$session = JFactory::getSession();
		$user = JFactory::getUser();
		$sessionQid = InsureDIYDomesticHelper::getCurrentQid();
		$inputQid = InsureDIYDomesticHelper::getInputQid();

		if ($user->get("guest")) { // Guest
			if ($sessionQid) {
				$layout = "login"; //
				$this->input->set('layout', $layout);
				return parent::display();
			}
		}

		if ($inputQid) {
			$quotation = InsureDIYDomesticHelper::getQuotation($inputQid);
			if (!$quotation) {
				$this->error[] = JText::_("ERR_QUOTATION_NOT_FOUND");
				$layout = "syserror";
				$this->input->set('layout', $layout);
				return parent::display();
			}

			if ($quotation && $quotation['user_id'] != $user->id) {
				$this->error[] = JText::_("ERR_NO_ACCESS_TO_VIEW");
				$layout = "syserror";
				$this->input->set('layout', $layout);
				return parent::display();
			}

			$session->set("domestic.data", $quotation); // override current session
		}

		$sessionData = $session->get("domestic.data", FALSE); // Current Quotation Data
		if (!$sessionData || !$sessionQid) {
			$layout = "step1";
			$this->input->set('layout', $layout);
			return parent::display();
		}

		// Override both view and layout so that user cannot put in funny stuffs.
		$this->input->set('layout', InsureDIYDomesticHelper::getCurrentLayout($sessionData['quote_stage']));
		return parent::display();
	}

}
