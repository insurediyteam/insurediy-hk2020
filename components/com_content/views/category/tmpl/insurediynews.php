<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers');

JHtml::_('behavior.caption');
$cat_params = $this->category->get('params');
$header_icon_class = $cat_params->get("header_icon_class", FALSE);
?>
<div class="category-list<?php echo $this->pageclass_sfx; ?>">
	<?php if ($cat_params->get('show_title', FALSE)): ?>
		<div class = "page-header ">
			<h1>
				<?php if ($header_icon_class): ?>
					<i style="margin-right: 20px;" class = "<?php echo $header_icon_class; ?>"></i>
				<?php endif; ?>
				<?php echo $cat_params->get('page_title'); ?>
			</h1>
		</div>
	<?php endif; ?>
	<?php
	echo $this->loadTemplate("articles");
//echo JLayoutHelper::render('joomla.content.category_insurediynews', $this);
	?>
</div>
