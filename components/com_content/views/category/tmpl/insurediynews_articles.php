<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_content
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

JHtml::_('behavior.framework');

// Create some shortcuts.
$params = &$this->item->params;
$n = count($this->items);
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));

// check for at least one editable article
$isEditable = false;
if (!empty($this->items)) {
	foreach ($this->items as $article) {
		if ($article->params->get('access-edit')) {
			$isEditable = true;
			break;
		}
	}
}
$description = $this->category->get('description', "");
?>
<div style="padding: 10px 25px;">
	<?php if ($this->params->get('show_description', 0)): ?>
		<div class="category_desc"><?php echo $description; ?></div>
		<div class="spacer-2"></div>
	<?php endif; ?>
	<?php if (empty($this->items)) : ?>
		<?php if ($this->params->get('show_no_articles', 1)) : ?>
			<p><?php echo JText::_('COM_CONTENT_NO_ARTICLES'); ?></p>
		<?php endif; ?>
	<?php else : ?>
		<form action="<?php echo htmlspecialchars(JUri::getInstance()->toString()); ?>" method="post" name="adminForm" id="adminForm" class="form-inline">
			<?php if ($this->params->get('show_headings') || $this->params->get('filter_field') != 'hide' || $this->params->get('show_pagination_limit')) : ?>
				<fieldset class="filters btn-toolbar clearfix">
					<?php if ($this->params->get('filter_field') != 'hide') : ?>
						<div class="btn-group">
							<label class="filter-search-lbl element-invisible" for="filter-search">
								<?php echo JText::_('COM_CONTENT_' . $this->params->get('filter_field') . '_FILTER_LABEL') . '&#160;'; ?>
							</label>
							<input type="text" name="filter-search" id="filter-search" value="<?php echo $this->escape($this->state->get('list.filter')); ?>" class="inputbox" onchange="document.adminForm.submit();" title="<?php echo JText::_('COM_CONTENT_FILTER_SEARCH_DESC'); ?>" placeholder="<?php echo JText::_('COM_CONTENT_' . $this->params->get('filter_field') . '_FILTER_LABEL'); ?>" />
						</div>
					<?php endif; ?>
					<?php if ($this->params->get('show_pagination_limit')) : ?>
						<div class="btn-group pull-right">
							<label for="limit" class="element-invisible">
								<?php echo JText::_('JGLOBAL_DISPLAY_NUM'); ?>
							</label>
							<?php echo $this->pagination->getLimitBox(); ?>
						</div>
					<?php endif; ?>

					<input type="hidden" name="filter_order" value="" />
					<input type="hidden" name="filter_order_Dir" value="" />
					<input type="hidden" name="limitstart" value="" />
					<input type="hidden" name="task" value="" />
				</fieldset>
			<?php endif; ?>
		</form>

		<?php foreach ($this->items as $i => $article) : ?>
			<div class="insurediy-news-archive">
				<div class="created_date">
					<?php if ($this->params->get('list_show_date')) : ?>
						<?php echo JHtml::_('date', $article->displayDate, $this->escape($this->params->get('date_format', JText::_('DATE_FORMAT_LC3')))); ?>
					<?php endif; ?>
				</div>
				<div class="info">
					<div class="title">
						<a href="<?php echo JRoute::_(ContentHelperRoute::getArticleRoute($article->slug, $article->catid)); ?>">
							<?php echo $this->escape($article->title); ?>
						</a>
					</div>
					<div class="introtext"><?php echo $article->introtext; ?></div>
					<div>
						<div class="controls">
							<a href="<?php echo JRoute::_(ContentHelperRoute::getArticleRoute($article->slug, $article->catid)); ?>"><button style="padding:5px 20px;font-size:14px !important;height:30px;" class="btn btn-primary" name="Submit" tabindex="0" type="submit">Read More</button></a>
						</div>
					</div>
				</div>
				<div class="clear"></div>
			</div>
		<?php endforeach; ?>
	<?php endif; ?>

	<?php // Code to add a link to submit an article.  ?>
	<?php if ($this->category->getParams()->get('access-create')) : ?>
		<?php echo JHtml::_('icon.create', $this->category, $this->category->params); ?>
	<?php endif; ?>

	<?php // Add pagination links ?>
	<?php if (!empty($this->items)) : ?>
		<?php if (($this->params->def('show_pagination', 2) == 1 || ($this->params->get('show_pagination') == 2)) && ($this->pagination->pagesTotal > 1)) : ?>
			<div class="pagination">

				<?php if ($this->params->def('show_pagination_results', 1)) : ?>
					<p class="counter pull-right">
						<?php echo $this->pagination->getPagesCounter(); ?>
					</p>
				<?php endif; ?>

				<?php echo $this->pagination->getPagesLinks(); ?>
			</div>
		<?php endif; ?>

	<?php endif; ?>
</div>
