<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_insurediylife
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
JLoader::import('incs.form.field', JPATH_ROOT);

require_once JPATH_COMPONENT . '/helpers/insurediylife.php';

JLoader::register('LifeQuotation', JPATH_COMPONENT . '/class/quotation.php');
new LifeQuotation();

$controller = JControllerLegacy::getInstance('InsureDIYLife');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
