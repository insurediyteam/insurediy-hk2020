<?php
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', '.insurediy-form-content select');
?>

<div class="insurediylife-form">
	<div class="header-top-wrapper">
		<?php echo InsureDIYLifeHelper::renderHeader('icon-insurediy-final-steps', JText::_('COM_INSUREDIYLIFE_PAGE_HEADING_PAYMENT_ERROR_PAGE'), 4); ?>
	</div>
	<div style="padding:30px;margin-top: 100px;">
		<?php echo MyHelper::renderDefaultMessage(); ?>
		<div class="edit<?php echo $this->pageclass_sfx; ?>">
			<div class="form-quote-box">
				<span style="color:#2f2f2f;font-size:37px;line-height: 40px;">
					<?php echo JText::_("COM_INSUREDIYLIFE_PAGE_SUB_HEADING_PAYMENT_ERROR"); ?>
				</span>
				<div class="center" style="color:#2f2f2f;font-size:14px;margin-top: 15px;">
					<?php echo JText::_("COM_INSUREDIYLIFE_PAGE_SUB_HEADING_PAYMENT_ERROR_EXPLANATION"); ?>
				</div>
			</div>
		</div>

	</div>
</div>
<div class="logo-slider"><?php echo MyHelper::load_module(102); ?></div>
