<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_insurediylife
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', '.insurediylife-form select');

$form = & $this->form;
$fldGroup = "contact-details";
$cars = $this->quotation['cars'];
$props = $this->quotation['props'];

$accept = "image/gif, image/jpeg, image/png, application/pdf, application/vnd.openxmlformats-officedocument.wordprocessingml.document, application/msword";
?>

<script type="text/javascript">
	var carRow = <?php echo count($cars) + 1; ?>, propertyRow = <?php echo count($props) + 1; ?>, jCarClone, jPropertyClone;

	function clickFileType(div_id) {
		$(div_id).click();
	}

	function copyPasteText(div_id, text, this_id) {
		var jThis = jQuery(this_id);
		if (text.length > 0) {
			var filename = text.replace(/^.*[\\\/]/, ''); // remove the path since we can't get the actual path
			var extensions = ["gif", "jpeg", "jpg", "png", "pdf", "doc", "docx"];
			var extension = filename.substr((filename.lastIndexOf('.') + 1));
			var isValid = (extensions.indexOf(extension) !== -1);
			if (isValid) {
				$(div_id).setProperty('value', filename);
			} else {
				jThis.val("");
				$("insurediy-popup-box-3").setStyle('display', 'block');
			}
		} else {
			$(div_id).setProperty('value', "");
		}
	}

	function duplicateProperty(parent_id) {
		propertyRow = propertyRow + 1;
		var jClone = jPropertyClone.clone();
		var newId = jPropertyClone.prop("id") + "-" + propertyRow;
		jClone.prop("id", newId);
		//jClone.addClass("epi-contact-detail-wrapper");
		jClone.find("#jform_policy_type_chzn").remove();
		jClone.find("input").each(function () {
			var jThis = jQuery(this);
			jThis.prop("id", jThis.prop("id") + "-" + propertyRow);
			jThis.val("");
		});
		jClone.find(".property-fields-row1 .property-fields-col2").prepend("<button type=\"button\" class=\"close\" aria-hidden=\"true\" onclick=\"javascript:removeExInsurace('#" + newId + "');\">×</button>");
		jQuery("#" + parent_id).append(jClone);
		var jSelect = jQuery("#" + newId + " select");
		var newSelectId = jSelect.prop("id") + "-" + propertyRow;
		jSelect.prop("id", newSelectId);
		jQuery("#" + newSelectId).removeClass("chzn-done").addClass("chsn");
		jQuery("#" + newSelectId).chosen();

		onNumberOnlyAndFormat(".numberMoneyFormat");

	}

	function duplicateCar(parent_id) {
		carRow = carRow + 1;
		var jClone = jCarClone.clone();
		var newId = jCarClone.prop("id") + "-" + carRow;
		jClone.prop("id", newId);
		jClone.find("#jform_policy_type_chzn").remove();
		jClone.find("input").each(function () {
			var jThis = jQuery(this);
			jThis.prop("id", jThis.prop("id") + "-" + carRow);
			jThis.val("");
		});
		jClone.prepend("<button type=\"button\" class=\"close\" aria-hidden=\"true\" onclick=\"javascript:removeExInsurace('#" + newId + "');\">×</button>");
		jQuery("#" + parent_id).append(jClone);
		var jSelect = jQuery("#" + newId + " select");
		var newSelectId = jSelect.prop("id") + "-" + carRow;
		jSelect.prop("id", newSelectId);
		jQuery("#" + newSelectId).removeClass("chzn-done").addClass("chsn");
		jQuery("#" + newSelectId).chosen();

		onNumberOnlyAndFormat(".numberMoneyFormat");

	}

	function removeExInsurace(id) {
		jQuery(id).remove();
	}

	function checkData(div_id) {
		var jDiv = jQuery("#" + div_id);
		var hasData = false;

		jDiv.find("input").each(function () {
			var jThis = jQuery(this);
			if (jThis.val().length > 0) {
				hasData = true;
			}
		});
		return hasData;
	}

	function onNumberOnlyAndFormat(input_id) {
		var jInput = jQuery(input_id);

		jInput.keydown(function (event) {
			var jThis = jQuery(this);
			var val = jThis.val();
			var hasDot = val.indexOf(".");
			// Allow only backspace and delete
			if (event.keyCode == 9 || event.keyCode == 46 || event.keyCode == 8 || (event.keyCode == 190 && hasDot == -1)) {
				// let it happen, don't do anything
			}
			else {
				// Ensure that it is a number and stop the keypress
				if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105)) {
					// 0-9
				} else {
					event.preventDefault();
				}
			}
		});

		jInput.on("keyup change", function () {
			var jThis = jQuery(this);
			var val = jThis.val();
			if (val.length > 0) {
				jThis.val(numberFormat(val));
			}
		});
	}

	function numberFormat(number) {
		number = parseFloat(number.replace(/,/g, ''));
		return number.formatMoney(0, '.', ',');
	}

	Number.prototype.formatMoney = function (c, d, t) {
		var n = this;
		c = isNaN(c = Math.abs(c)) ? 2 : c;
		d = d === undefined ? "." : d;
		t = t === undefined ? "," : t;
		var s = n < 0 ? "-" : "";
		var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "";
		var j = (j = i.length) > 3 ? j % 3 : 0;
		return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	}

	jQuery(document).ready(function () {
<?php if (count($cars)): ?>
			jQuery('#car-fields').css('display', 'block');
<?php endif; ?>
<?php if (count($props)): ?>
			jQuery('#property-fields').css('display', 'block');
<?php endif; ?>
<?php if (isset($this->quotation['other_ownership_of_company']) && $this->quotation['other_ownership_of_company']): ?>
			jQuery('#load-ownership-of-company').css('display', 'block');
<?php endif; ?>

		jQuery("select").change(function () {
			switch (this.id) {
				case 'jform_other_own_property':
					if (this.value == 1) {
						jQuery('#property-fields').css('display', 'block');
					} else {
						jQuery('#property-fields').css('display', 'none')
					}
					break;
				case 'jform_other_own_car':
					if (this.value == 1) {
						jQuery('#car-fields').css('display', 'block');
					} else {
						jQuery('#car-fields').css('display', 'none')
					}
					break;
				case 'jform_other_ownership_of_company':
					if (this.value == 1) {
						jQuery('#load-ownership-of-company').css('display', 'block');
					} else {
						jQuery('#load-ownership-of-company').css('display', 'none');
					}
					break;
			}
		});

		onNumberOnlyAndFormat(".numberMoneyFormat");
		jPropertyClone = jQuery("#property-fields-sub-container");
		jCarClone = jQuery("#extra-form-another-cars");

	});


</script>



<div class="insurediylife-form">

	<div class="header-top-wrapper">
		<?php echo InsureDIYLifeHelper::renderHeader('icon-term-insurance', JText::_('COM_INSUREDIYLIFE_PAGE_HEADING_YOUR_LIFE_COVER'), 3); ?>
	</div>
	<div style="padding:20px;margin-top: 58px;">
		<?php echo MyHelper::renderDefaultMessage(); ?>
		<div style="font-size:14px;margin-bottom:15px;"><?php echo Jtext::_("COM_INSUREDIYLIFE_EXTRAINFO_INTRO_TEXT"); ?></div>
		<form action="<?php echo JRoute::_('index.php?option=com_insurediylife&view=form'); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-vertical" enctype="multipart/form-data">
			<fieldset class="insurediy-life-data">
				<div id="contact-details-fields" class="contact-details-fields">
					<div class="box-sized-input">
						<div style="float:left;width:30%;">
							<div class="control-label"><?php echo $this->form->getLabel('other_dependant_no'); ?></div>
							<div class="controls"><?php echo $this->form->getInput('other_dependant_no'); ?></div>
						</div>
						<div class="clear"></div>
						<div class="" style="font-size:14px;font-weight:bold;margin:20px 0;"><?php echo Jtext::_("COM_INSUREDIYLIFE_TOTAL_ANNUAL_INCOME_LAST_3_YEARS"); ?></div>
						<div style="float: left;width:30%;">
							<div class="control-label"><?php echo $this->form->getLabel('other_total_income_last_year'); ?></div>
							<div class="controls"><?php echo $this->form->getInput('other_total_income_last_year'); ?></div>
						</div>
						<div style="float: left;width:30%;margin-left:1%;">
							<div class="control-label"><?php echo $this->form->getLabel('other_total_income_year_before'); ?></div>
							<div class="controls"><?php echo $this->form->getInput('other_total_income_year_before'); ?></div>
						</div>
						<div style="float: left;width:30%;margin-left:1%;">
							<div class="control-label"><?php echo $this->form->getLabel('other_total_income_two_years'); ?></div>
							<div class="controls"><?php echo $this->form->getInput('other_total_income_two_years'); ?></div>
						</div>
						<div class="clear"></div>
						<div style="float:left;width:30%;">
							<div class="control-label"><?php echo $this->form->getLabel('other_when_join_company'); ?></div>
							<div class="controls"><?php echo $this->form->getInput('other_when_join_company'); ?></div>
						</div>
						<div class="clear"></div>

						<hr />

						<div style="float:left;width:100%;">
							<div class="control-label" style="float:left;width:25%;margin-top:5px;"><?php echo $this->form->getLabel('other_own_property'); ?></div>
							<div class="controls" style="float:left;width:30%;"><?php echo $this->form->getInput('other_own_property'); ?></div>
							<div class="clear"></div>
						</div>
						<div class="clear"></div>

						<div id="property-fields" style="display:none;margin-top:20px;">

							<div id="property-fields-container">
								<?php if (empty($props)): ?>
									<div id="property-fields-sub-container">
										<div class="property-fields-row1">
											<div class="property-fields-col1">
												<div class="control-label"><?php echo $form->getLabel("property_purchase"); ?></div>
												<div class="controls"><?php echo $form->getInput("property_purchase"); ?></div>
											</div>
											<div class="property-fields-col2">
												<div class="control-label"><?php echo $form->getLabel("property_price"); ?></div>
												<div class="controls" ><?php echo $form->getInput("property_price"); ?></div>
											</div>
											<div class="clear"></div>
										</div>
										<div class="property-fields-row2">
											<div class="property-fields-col1">
												<div class="control-label"><?php echo $form->getLabel("property_mortgage"); ?></div>
												<div class="controls" style="position:relative;">
													<?php /* <div class="hkd-word">&nbsp;</div> */ ?>
													<?php echo $form->getInput("property_mortgage"); ?>
												</div>
											</div>
											<div class="property-fields-col2">
												<div class="control-label"><?php echo $form->getLabel("property_value"); ?></div>
												<div class="controls"><?php echo $form->getInput("property_value"); ?></div>
											</div>
											<div class="clear"></div>
										</div>
										<div class="clear"></div>
										<hr style="border-top:1px solid #ccc;" />
									</div>
								<?php else: ?>
									<?php
									foreach ($props as $k => $prop):
										$form->setValue("property_purchase", "", $prop->property_purchase);
										$form->setValue("property_price", "", $prop->property_price);
										$form->setValue("property_mortgage", "", $prop->property_mortgage);
										$form->setValue("property_value", "", $prop->property_value);
										if ($k) {
											$row = $k + 1;
											$form->setFieldAttribute("property_purchase", "id", "property_purchase_" . $row);
											$form->setFieldAttribute("property_price", "id", "property_price_" . $row);
											$form->setFieldAttribute("property_mortgage", "id", "property_mortgage_" . $row);
											$form->setFieldAttribute("property_value", "id", "property_value_" . $row);
										}
										?>
										<div id="property-fields-sub-container<?php echo ($k) ? "_" . $k : ""; ?>">

											<div class="property-fields-row1">
												<div class="property-fields-col1">
													<div class="control-label"><?php echo $form->getLabel("property_purchase"); ?></div>
													<div class="controls"><?php echo $form->getInput("property_purchase"); ?></div>
												</div>
												<div class="property-fields-col2">
													<?php if ($k): ?>
														<button type="button" class="close" aria-hidden="true" onclick="javascript:removeExInsurace('#property-fields-sub-container<?php echo ($k) ? "_" . $k : ""; ?>');">×</button>
													<?php endif; ?>
													<div class="control-label"><?php echo $form->getLabel("property_price"); ?></div>
													<div class="controls" ><?php echo $form->getInput("property_price"); ?></div>
												</div>
												<div class="clear"></div>
											</div>
											<div class="property-fields-row2">
												<div class="property-fields-col1">
													<div class="control-label"><?php echo $form->getLabel("property_mortgage"); ?></div>
													<div class="controls" style="position:relative;">
														<?php /* <div class="hkd-word">&nbsp;</div> */ ?>
														<?php echo $form->getInput("property_mortgage"); ?>
													</div>
												</div>
												<div class="property-fields-col2">
													<div class="control-label"><?php echo $form->getLabel("property_value"); ?></div>
													<div class="controls"><?php echo $form->getInput("property_value"); ?></div>
												</div>
												<div class="clear"></div>
											</div>
											<div class="clear"></div>
											<hr style="border-top:1px solid #ccc;" />
										</div>
									<?php endforeach; ?>

								<?php endif; ?>
							</div>

							<div class="insurediy-contact-detail-add-another-policy">
								<div class="title-text"><a href="javascript:duplicateProperty('property-fields-container')"><?php echo Jtext::_("COM_INSUREDIYLIFE_ADD_ANOTHER_PROPERTY"); ?></a></div>
							</div>
						</div>

						<hr />
						<div style="float:left;width:100%;">
							<div class="control-label" style="float:left;width:25%;margin-top:5px;"><?php echo $this->form->getLabel('other_own_car'); ?></div>
							<div class="controls" style="float:left;width:30%;"><?php echo $this->form->getInput('other_own_car'); ?></div>
							<div class="clear"></div>
						</div>
						<div class="clear"></div>
						<div id="car-fields" style="display:none;margin-top:20px;">
							<div id="extra-form-another-cars-container">
								<?php if (empty($cars)): ?>
									<div id="extra-form-another-cars">
										<div class="control-label" style="float:left;width:25%;margin-top:5px;"><?php echo $form->getLabel("car_model"); ?></div>
										<div class="controls" style="float:left;width:60%;"><?php echo $form->getInput("car_model"); ?></div>
										<div class="clear"></div>
									</div>
								<?php else: ?>
									<?php
									foreach ($cars as $k => $car):
										$form->setValue("car_model", "", $car->car_model);
										if ($k) {
											$row = $k + 1;
											$form->setFieldAttribute("car_model", "id", "car_model_" . $row);
										}
										?>
										<div id="extra-form-another-cars<?php echo ($k) ? "_" . $k : ""; ?>">
											<?php if ($k): ?>
												<button type="button" class="close" aria-hidden="true" onclick="javascript:removeExInsurace('#extra-form-another-cars<?php echo ($k) ? "_" . $k : ""; ?>');">×</button>
											<?php endif; ?>
											<div class="control-label" style="float:left;width:25%;margin-top:5px;"><?php echo $form->getLabel("car_model"); ?></div>
											<div class="controls" style="float:left;width:60%;"><?php echo $form->getInput("car_model"); ?></div>
											<div class="clear"></div>
										</div>
									<?php endforeach; ?>

								<?php endif; ?>

							</div>
							<div class="insurediy-contact-detail-add-another-policy">
								<div class="title-text"><a href="javascript:duplicateCar('extra-form-another-cars-container')"><?php echo Jtext::_("COM_INSUREDIYLIFE_ADD_ANOTHER_CAR"); ?></a></div>
							</div>
						</div>

						<hr />

						<div class="" style="font-size:14px;font-weight:bold;margin:10px 0 15px;"><?php echo Jtext::_("COM_INSUREDIYLIFE_TOTAL_OTHER_ASSETS"); ?></div>

						<div style="float: left;width:30%;">
							<div class="control-label"><?php echo $this->form->getLabel('other_assets_fixed_deposits'); ?></div>
							<div class="controls"><?php echo $this->form->getInput('other_assets_fixed_deposits'); ?></div>
						</div>
						<div style="float: left;width:30%;margin-left:1%;">
							<div class="control-label"><?php echo $this->form->getLabel('other_assets_shares'); ?></div>
							<div class="controls"><?php echo $this->form->getInput('other_assets_shares'); ?></div>
						</div>
						<div style="float: left;width:30%;margin-left:1%;">
							<div class="control-label"><?php echo $this->form->getLabel('other_assets_others'); ?></div>
							<div class="controls"><?php echo $this->form->getInput('other_assets_others'); ?></div>
						</div>
						<div class="clear"></div>

						<div style="float: left;width:30%;">
							<div class="control-label"><?php echo $this->form->getLabel('other_assets_other_loans'); ?></div>
							<div class="controls"><?php echo $this->form->getInput('other_assets_other_loans'); ?></div>
						</div>
						<div class="clear"></div>
						<hr />
						<div style="float:left;width:100%;">
							<div class="control-label" style="float:left;width:50%;margin-top:5px;"><?php echo $this->form->getLabel('other_ownership_of_company'); ?></div>
							<div class="controls" style="float:left;width:30%;"><?php echo $this->form->getInput('other_ownership_of_company'); ?></div>
							<div class="clear"></div>
						</div>
						<div class="clear"></div>
					</div>

					<div id="load-ownership-of-company" style="display:none;margin-top:20px;">
						<div class="box-sized-input">
							<div style="float: left;width:30%;">
								<div class="control-label"><?php echo $this->form->getLabel('other_ownership_percent'); ?></div>
								<div class="controls"><?php echo $this->form->getInput('other_ownership_percent'); ?></div>
							</div>
							<div style="float: left;width:30%;margin-left:1%;">
								<div class="control-label"><?php echo $this->form->getLabel('other_ownership_commencement_biz'); ?></div>
								<div class="controls"><?php echo $this->form->getInput('other_ownership_commencement_biz'); ?></div>
							</div>
							<div style="float: left;width:30%;margin-left:1%;">
								<div class="control-label"><?php echo $this->form->getLabel('other_ownership_employees_no'); ?></div>
								<div class="controls"><?php echo $this->form->getInput('other_ownership_employees_no'); ?></div>
							</div>
							<div class="clear"></div>
						</div>

						<?php if ($this->my_sum_insured_total > 7000000) : ?>
							<hr />

							<div style="margin-bottom:10px;">
								<div class="control-label" style="float:left;width:48%;height: 40px; line-height: 40px;margin-bottom: 0px;"><?php echo $this->form->getLabel('upload_financial_company'); ?></div>
								<div class="controls" style="float:left;width:52%;">
									<label for="inputr_1" style="display: inline-block;width: 45px;">Year 1</label>
									<input type="text" id="inputr_1" readonly="readonly" onclick="javascript:clickFileType('file_1');
											return false;" class="input-file" value="<?php //echo $fname_id_card     ?>" /><input type="button" class="btn btn-primary validate" onclick="javascript:clickFileType('file_1');
													return false;" style="font-size:14px !important;height:40px;padding:10px 30px;font-family:sourcesansprobold;color:#416106" value="<?php echo JText::_('JBROWSE') ?>" />
									<input type="file" name="jform[file_company_financial1]" id="file_1" accept="<?php echo $accept; ?>" onchange="javascript:copyPasteText('inputr_1', this.value,
													'#file_1');" style="display:none;" />
								</div>
								<div class="clear"></div>
							</div>
							<div style="margin-bottom:10px;">
								<div class="control-label" style="float:left;width:48%;">&nbsp;</div>
								<div class="controls" style="float:left;width:52%;">
									<label for="inputr_2" style="display: inline-block;width: 45px;">Year 2</label>
									<input type="text" id="inputr_2" readonly="readonly" onclick="javascript:clickFileType('file_2');
											return false;" class="input-file" value="<?php //echo $fname_id_card     ?>" /><input type="button" class="btn btn-primary validate" onclick="javascript:clickFileType('file_2');
													return false;" style="font-size:14px !important;height:40px;padding:10px 30px;font-family:sourcesansprobold;color:#416106" value="<?php echo JText::_('JBROWSE') ?>" />
									<input type="file" name="jform[file_company_financial2]" id="file_2" accept="<?php echo $accept; ?>" onchange="javascript:copyPasteText('inputr_2', this.value,
													'#file_2');" style="display:none;" />
								</div>
								<div class="clear"></div>
							</div>
							<div style="margin-bottom:10px;">
								<div class="control-label" style="float:left;width:48%;">&nbsp;</div>
								<div class="controls" style="float:left;width:52%;">
									<label for="inputr_3" style="display: inline-block;width: 45px;">Year 3</label>
									<input type="text" id="inputr_3" readonly="readonly" onclick="javascript:clickFileType('file_3');
											return false;" class="input-file" value="<?php //echo $fname_id_card     ?>" /><input type="button" class="btn btn-primary validate" onclick="javascript:clickFileType('file_3');
													return false;" style="font-size:14px !important;height:40px;padding:10px 30px;font-family:sourcesansprobold;color:#416106" value="<?php echo JText::_('JBROWSE') ?>" />
									<input type="file" name="jform[file_company_financial3]" id="file_3" accept="<?php echo $accept; ?>" onchange="javascript:copyPasteText('inputr_3', this.value,
													'#file_3');" style="display:none;" />
								</div>
								<div class="clear"></div>
							</div>
							<hr />

							<div style="margin-bottom:10px;">
								<div class="control-label" style="float:left;width:48%;"><?php echo $this->form->getLabel('upload_income_tax'); ?></div>
								<div class="controls" style="float:left;width:52%;">
									<label for="inputr_1" style="display: inline-block;width: 45px;"></label>
									<input type="text" id="inputr_4" readonly="readonly" onclick="javascript:clickFileType('file_4');
											return false;" class="input-file" value="<?php //echo $fname_id_card     ?>" /><input type="button" class="btn btn-primary validate" onclick="javascript:clickFileType('file_4');
													return false;" style="font-size:14px !important;height:40px;padding:10px 30px;font-family:sourcesansprobold;color:#416106" value="<?php echo JText::_('JBROWSE') ?>" />
									<input type="file" name="jform[file_income_tax]" id="file_4" accept="<?php echo $accept; ?>" onchange="javascript:copyPasteText('inputr_4', this.value,
													'#file_4');" style="display:none;" />

								</div>
								<div class="clear"></div>
							</div>
						<?php endif; ?>
					</div>
				</div>
				<input type="hidden" name="task" value="form.extrainfosave" />
				<input type="hidden" name="quotation_id" value="<?php echo InsureDIYLifeHelper::getQid(); ?>" />
				<?php echo JHtml::_('form.token'); ?>
				<div style="float:left;margin:15px 0;">
					<div>{modulepos insurediy-secured-ssl}</div>
				</div>
				<div class="btn-toolbar" style="float:right">
					<div class="">
						<button style="margin-right:10px;" type="button" onclick="javascript:document.adminForm.task.value = 'form.back';
								document.adminForm.submit();" class="btn btn-primary validate"><?php echo JText::_('BTN_BACK') ?></button>
						<button type="submit" class="btn btn-primary validate"><?php echo JText::_('BTN_CONTINUE') ?></button>
					</div>
				</div>
				<div class="clear"></div>
			</fieldset>
		</form>
	</div>
</div>
<div class="logo-slider"><?php echo MyHelper::load_module(102); ?></div>

<?php /* Popup Box  3 */ ?>
<div class="insurediy-popup" id="insurediy-popup-box-3" style="display:none;">
	<div class="header">
		<div class="ico-warning">
			<div class="text-header2"><?php echo JText::_("COM_INSUREDIYLIFE_POP_UP_HEADER_WARNING"); ?></div>
		</div>
		<a class="close" href="javascript:cleanInsureDIYPopup('insurediy-popup-box-3');">&nbsp;</a>
		<div class="clear"></div>
	</div>
	<div class="padding">
		<div class="marginbottom20"><?php echo JText::_("COM_INSUREDIYLIFE_ERROR_INVALID_FILE_TYPE"); ?></div>
		<div style="text-align:right;"><input type="button" class="btn btn-primary" style="height:30px;padding:5px 20px;font-size:14px !important;" onclick="javascript:cleanInsureDIYPopup('insurediy-popup-box-3');
				return
				false;" value="<?php echo JText::_("JOK") ?>" /></div>
	</div>
</div>