<?php
	defined('_JEXEC') or die;
	
	JHtml::_('behavior.keepalive');
	JHtml::_('behavior.formvalidation');
	JHtml::_('formbehavior.chosen', '.insurediy-form-content select');
	
	$quotation = $this->quotation;

?>

<script type="text/javascript">
	window.addEvent('domready', function() {
		var myAccordion = new Fx.Accordion($$('.togglers'), $$('.step2-checkbox'), {display: -1, alwaysHide: true});
	});
	
	function clickFileType(div_id) {
		$(div_id).click();
	}
	
	function copyPasteText(div_id, text) {
		$(div_id).setProperty('value', text);
	}
</script>

<div class="insurediylife-form">
	<div class="header-top-wrapper">
		<?php echo InsureDIYLifeHelper::renderHeader('icon-insurediy-final-steps', JText::_('COM_INSUREDIYLIFE_PAGE_HEADING_THANK_YOU_PAGE'), 4); ?>
	</div>
	<div style="padding:30px;margin-top: 100px;">
		<?php echo MyHelper::renderDefaultMessage(); ?>
		<div class="clearfix edit<?php echo $this->pageclass_sfx; ?>">
			<div class="form-quote-box">
				<span style="color:#2f2f2f;font-size:37px;line-height: 40px;">
					<?php echo JText::_("COM_INSUREDIYLIFE_PAGE_SUB_HEADING_THANK_YOU_FOR_QUOTATION"); ?>
				</span>
				<div class="center" style="color:#2f2f2f;font-size:14px;margin-top: 15px;">
					<?php echo JText::_("COM_INSUREDIYLIFE_EXPLANATION_OUR_STAFFS_WILL_CONTACT"); ?>
				</div>
			</div>
		</div>
		
	</div>
</div>
<div class="logo-slider"><?php echo MyHelper::load_module(102); ?></div>

<script>
	// Send transaction data with a pageview if available
	// when the page loads. Otherwise, use an event when the transaction
	// data becomes available.
	<?php
		$total = 0;
		foreach ($quotation['selected_plans'] as $plan) {
			$total += $plan->price;
		}
	?>
	dataLayer.push({
		'ecommerce': {
			'purchase': {
				'actionField': {
					'id': '<?php echo $quotation['unique_order_no']; ?>',
					// Transaction ID. Required for purchases and refunds.
					'revenue': '<?php echo number_format($total, 2); ?>'
				},
				'products': [
				<?php foreach ($quotation['selected_plans'] as $plan) : ?>
				{ // List of productFieldObjects.
					'name': '<?php echo $plan->plan_name; ?>', // Name or ID is required.
					'id': '<?php echo $plan->plan_id; ?>',
					'price': '<?php echo $plan->price; ?>',
					'brand': '<?php echo $plan->insurer_code; ?>',
					'category': 'Life',
					'variant': '',
					'quantity': 1 // Optional fields may be omitted or set to empty string.
				},
				<?php endforeach; ?>
				]
			}
		}
	});
</script>

<script>
window.uetq = window.uetq || [];
window.uetq.push({ 'gv': <?php echo $this->quotation['selected_plan']->premium; ?>, 'gc': 'HKD' }); 
</script>