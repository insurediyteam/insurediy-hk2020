<?php
	defined('_JEXEC') or die;
	
	JHtml::_('behavior.keepalive');
	JHtml::_('behavior.formvalidation');
	JHtml::_('formbehavior.chosen', '.insurediy-life-cover-options select');
	JHtml::_('bootstrap.tooltip');
	
	$quotation = $this->quotation;
	
	$chartData = $this->chartData;
	$maxAge = $this->maxAge;
	$ages = $this->ages;
	$showGraph = $this->showGraph;
	$showGraph = FALSE;
	
	$total_plans = count($this->policy_plans);
	$width = ($total_plans) ? 670 / $total_plans : 670;

	// remarketing email
	$user = $this->user;
	$products = [];
	$pol = [];
	foreach ($this->policy_plans as $key => $value) {
		$pol["premium"] = $value->price;
		$pol["code"] = $value->insurer_code;
		array_push($products, $pol);
	}
	$mailQueue = (object)array(
		"email" => $user->email,
		"products" => $products,
		"type" => "life-before",
		"parent" => "life"
	);
	RemarketingHelpersRemarketing::newRemarketing($mailQueue);
	//end of remarketing email
	
?>

<script type="text/javascript">
	
	var checkboxHeight = "33";
	var radioHeight = "18";
	var selectWidth = "190";
	
	function chooseYourPlans() {
		var has_chosen = 0;
		
		$$('input[type="checkbox"]:checked').each(function (elem) {
			has_chosen = 1;
		});
		
		if (has_chosen == 0) {
			$('insurediy-popup-box').setStyle('display', 'block');
			} else {
			document.choosePlanForm.submit();
		}
	}
	
</script>
<?php if ($showGraph): ?>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<script type="text/javascript">
	google.load("visualization", "1", {packages: ["corechart"]});
	google.setOnLoadCallback(drawChart);
	function drawChart() {
		var chartData = <?php echo json_encode($chartData); ?>;
		var data = google.visualization.arrayToDataTable(chartData);
		//		var vardata = google.visualization.arrayToDataTable([
		//			['Age', 'Sales', 'Expenses'],
		//			['2004', 1000, 400],
		//			['2005', 1170, 460],
		//			['2006', 660, 1120],
		//			['2007', 1030, 540]
		//		]);
		
		var options = {'title': 'Monthly Premium',
			'width': 695,
			'height': 400,
			'lineWidth': 2,
			'backgroundColor': '#f4fcff',
			'isStacked': true,
			'colors': <?php echo json_encode($this->colorData) ?>,
			'legend': {'position': 'right', 'alignment': "start", "maxLines": 4},
			'hAxis': {'title': 'Age', 'ticks': [<?php echo implode(",", $ages); ?>], 'maxValue': <?php echo $maxAge; ?>, "titleTextStyle": {"bold": true, "itallic": false}},
			//			'vAxis': {'format': 'HK$ #,###'}
		};
		
		var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
		chart.draw(data, options);
	}
</script>
<?php endif; ?>
<script type="text/javascript">
	//Measure a view of product details. This example assumes the detail view occurs on pageload,
	//and also tracks a standard pageview of the details page.
	dataLayer.push({
		'ecommerce': {
			'impressions': [<?php $position = 1; foreach ($this->policy_plans as $key =>$policy) :?>
			{
				'name': '<?php echo $policy->plan_name; ?>', //Name or ID is required.
				'id': '<?php echo $policy->plan_index_code; ?>',
				'price': '<?php echo $policy->price; ?>',
				'brand': '<?php echo $policy->insurer_code; ?>',
				'category': 'Life',
				'list': 'Life Quotation',
				'variant': '<?php echo $policy->year_code; ?>',
				'position': <?php echo $position; ?>
			},
			<?php $position++; endforeach; ?>]
		}
	});
	
	//Measure adding a product to a shopping cart by using an 'add' actionFieldObject
	//and a list of productFieldObjects.
	function addToCart() {
		var count = <?php echo count($this->policy_plans); ?>;
		var productObj = [];
		for (i = 0; i < count; i++) { 
			var input = document.getElementsByName("jform[plans]["+i+"]")[0];
			if (input.checked) {
				var inputObj = input.value.split("|");
				var insureCode = inputObj[1].slice(0,3);
				productObj.push({
					'name': inputObj[0],
					'id': inputObj[1],
					'price': inputObj[3],
					'brand': insureCode,
					'category': 'Life',
					'variant': inputObj[2],
					'quantity': 1
				});
			}
		}
		
		dataLayer.push({
			'event': 'productClick',
			'ecommerce': {
				'click': {
					'actionField': {'list': 'Life Quotation'}, // Optional list property.
					'products': productObj
				}
			}
		});
	}
	</script>
	<div class="insurediylife-form">
		<div class="header-top-wrapper">
			<?php echo InsureDIYLifeHelper::renderHeader('icon-term-insurance', JText::_('COM_INSUREDIYLIFE_PAGE_HEADING_YOUR_LIFE_COVER'), 2); ?>
			<div class="insurediy-life-cover-options">
				<form action="index.php?option=com_insurediylife" method="post" name="adminForm" id="adminForm">
					<div>
						<div class="title" style="float: left;color:#0ea8ca;margin-left: 28px;line-height: 39px"><?php echo JText::_("COM_INSUREDIYLIFE_LIFE_COVER_OPTIONS"); ?></div>
						<div style="float: right">
							<div style="float:left;font-weight:bold;font-size:14px;color:#999;line-height: 39px"><?php echo JText::_("COM_INSUREDIYLIFE_AMOUNT_OF_COVER"); ?></div>
							<div style="float:left;margin-left:10px;width:156px;margin-right: 10px;"><?php echo $this->form->getInput('cover_amt'); ?></div>
							<div style="float:left;margin:0 10px;width:156px;"><?php echo $this->form->getInput('cover_length'); ?></div>
							<div style="float:left;margin-left:10px;">
								<input type="hidden" name="return" value="<?php echo base64_encode(JURI::getInstance()->toString()); ?>" />
								<input type="hidden" name="task" value="form.updateLifeCoverOptions" />
								<?php echo JHtml::_('form.token'); ?>
								<button type="submit" class="btn btn-primary validate" style="height:34px;padding:5px 20px;font-size:14px !important;"><?php echo JText::_('JGET_QUOTES') ?></button>
							</div>
						</div>
						<div class="clear"></div>
					</div>
				</form>
			</div>
		</div>
		<div id="insurediy-form-body" style="padding:30px 20px;margin-top: 106px">
			<?php echo MyHelper::renderDefaultMessage(); ?>
			<div class="edit<?php echo $this->pageclass_sfx; ?>">
				<form action="<?php echo JRoute::_('index.php?option=com_insurediylife'); ?>" method="post" name="choosePlanForm" id="choosePlanForm" class="form-validate form-vertical">
					
					<?php if (!empty($this->policy_plans)) : ?>
					<table class="insurediylife-table-quotations insurediy-table-plans">
						<tr>
							<td class="td-first">
								<div class="ico-quotation-mark" style="padding-left: 30px;"><?php echo JText::_("COM_INSUREDIYLIFE_YOUR_QUOTATIONS"); ?></div>
							</td>
							<?php foreach ($this->policy_plans as $key => $policy) : ?>
							<td class="td-inner" style="position: relative; width: <?php echo $width; ?>px;">
								<?php if ($policy->logo) : ?>
								<div class="logo-container" style="width: <?php echo $width; ?>px;">
									<img src="<?php echo $policy->logo ?>" />
								</div>
								<?php endif; ?>
								<?php if (isset($policy->flag_banner) && strlen($policy->flag_banner) > 0): ?>
								<div style="position:absolute;top:0;"><img src="<?php echo $this->flag_banner_path . '/' . $policy->flag_banner ?>" /></div>
								<?php endif; ?>
							</td>
							<?php endforeach; ?>
						</tr>
						<tr class="price-row">
							<td class="td-first premuim-text" style="min-width: 247px;"><?php echo JText::_("COM_INSUREDIYLIFE_FIELD_PREMIUM_TITLE"); ?> (<?php echo $this->currency; ?>) <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYLIFE_FIELD_PREMIUM_DESC"); ?>"><br>
								<?php if (FALSE): ?>
								<a href="javascript:showInsureDIYPopup('insurediy-chart-popup-box');">+ <?php echo JText::_("COM_INSUREDIYLIFE_SHOW_FUTURE_PREMIUMS"); ?></a>
								<?php endif; ?>
							</td>
							<?php foreach ($this->policy_plans as $key => $policy) : ?>
							<td class="td-inner premuim-text">
								<?php echo MyHelper::numberFormat($policy->price); ?>
							</td>
							<?php endforeach; ?>
						</tr>
						<?php /*<tr>
							<td class="td-first">
								<?php echo JText::_("COM_INSUREDIYLIFE_PLAN_DIY_POINTS_TITLE"); ?> <?php echo InsureDIYHelper::generateQuestionMark("COM_INSUREDIYLIFE_PLAN_DIY_POINTS_DESC"); ?>
							</td>
							<?php foreach ($this->policy_plans as $key => $policy) : ?>
							<td class="td-inner">
								<?php echo MyHelper::numberFormat($policy->pur_points); ?>
							</td>
							<?php endforeach; ?>
						</tr>*/ ?>
						<tr>
							<td class="td-first">
								<?php echo JText::_("COM_INSUREDIYLIFE_FIELD_GUARANTEED_TITLE"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYLIFE_FIELD_GUARANTEED_DESC"); ?>">
							</td>
							<?php foreach ($this->policy_plans as $key => $policy) : ?>
							<td class="td-inner">
								<?php echo InsureDIYHelper::printPlanAttr($policy->renewable) ?>
							</td>
							<?php endforeach; ?>
						</tr>
						<tr>
							<td class="td-first">
								<?php echo JText::_("COM_INSUREDIYLIFE_FIELD_TERMINAL_ILLNESS_TITLE"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYLIFE_FIELD_TERMINAL_ILLNESS_DESC"); ?>">
							</td>
							<?php foreach ($this->policy_plans as $key => $policy) : ?>
							<td class="td-inner">
								<?php echo InsureDIYHelper::printPlanAttr($policy->terminal_illness_benefit) ?>
							</td>
							<?php endforeach; ?>
						</tr>
						<tr>
							<td class="td-first">
								<?php echo JText::_("COM_INSUREDIYLIFE_FIELD_OTHER_BENEFITS_TITLE"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYLIFE_FIELD_OTHER_BENEFITS_DESC"); ?>">
							</td>
							<?php foreach ($this->policy_plans as $key => $policy) : ?>
							<td class="td-inner">
								<?php echo InsureDIYHelper::printPlanAttr($policy->benefits) ?>
							</td>
							<?php endforeach; ?>
						</tr>
						<tr>
							<td class="td-first">
								<?php echo JText::_("COM_INSUREDIYLIFE_FIELD_DOWNLOAD_BROCHURE_TITLE"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYLIFE_FIELD_DOWNLOAD_BROCHURE_DESC"); ?>">
							</td>
							<?php foreach ($this->policy_plans as $key => $policy) : ?>
							<td class="td-inner">
								<?php echo ($policy->download_file) ? '<a href="' . $policy->download_file . '" target="_blank"><img src="templates/protostar/images/icon/ico-downloadfile.png" /></a>' : 'N/A' ?>
							</td>
							<?php endforeach; ?>
						</tr>
						<tr>
							<td class="td-first"><?php echo JText::_("COM_INSUREDIYLIFE_FIELD_SELECT_TO_APPLY_TITLE"); ?> <img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="" data-original-title="<?php echo JText::_("COM_INSUREDIYLIFE_FIELD_SELECT_TO_APPLY_DESC"); ?>"></td>
							<?php
								foreach ($this->policy_plans as $key => $policy) :
								$selected = "";
								if (!empty($this->quotation['selected_plan_pics']) && in_array($policy->plan_index_code.'|'.$policy->price, $this->quotation['selected_plan_pics'])) {
									$selected = 'checked="checked"';
								}
							?>
							<td class="td-inner">
								<div style="position:relative;">
									<input <?php echo $selected; ?> type="checkbox" class="styled" name="jform[plans][<?php echo $key ?>]" value="<?php echo $policy->plan_name . "|" . $policy->plan_index_code . '|' . $policy->sum_insured . '|' . $policy->price ?>" />
								</div>
							</td>
							<?php endforeach; ?>
						</tr>
					</table>
					<?php else : ?>
					<div><?php echo JText::_("COM_INSUREDIYLIFE_NO_PLANS_MSG"); ?></div>
					<?php endif; ?>
					<input type="hidden" name="task" value="form.step3save" />
					<input type="hidden" name="quotation_id" value="<?php echo $quotation['id']; ?>" />
					<?php echo JHtml::_('form.token'); ?>
					
					<div style="float:left;margin:15px 0;">
						<div>{modulepos insurediy-download-adobe}</div>
						<div>{modulepos insurediy-secured-ssl}</div>
					</div>
					<?php if (!empty($this->policy_plans)) : ?>
					<div class="btn-toolbar" style="float:right">
						<div>
							<button style="margin-right:10px;" type="button" onclick="javascript:document.choosePlanForm.task.value = 'form.back';
							document.choosePlanForm.submit();" class="btn btn-primary validate"><?php echo JText::_('BTN_BACK') ?></button>
							<button type="button" onclick="javascript:chooseYourPlans();addToCart();return false;" class="btn btn-primary validate"><?php echo JText::_('BTN_CONTINUE') ?></button>
						</div>
					</div>
					<?php else: ?>
					<div class="btn-toolbar" style="float:right">
						<button style="margin-right:10px;" type="button" onclick="javascript:document.choosePlanForm.task.value = 'form.back';
						document.choosePlanForm.submit();" class="btn btn-primary validate"><?php echo JText::_('BTN_BACK') ?></button>
					</div>
					<?php endif; ?>
					<div class="clear"></div>
				</form>
			</div>
			
		</div>
	</div>
	
	<?php /* Popup Box */ ?>
	<div class="insurediy-popup" id="insurediy-popup-box" style="display:none;">
		<div class="header">
			<div class="text-header"><?php echo JText::_("POP_IMPORTANT_NOTICE"); ?></div>
			<a class="close" href="javascript:cleanInsureDIYPopup('insurediy-popup-box');">&nbsp;</a>
			<div class="clear"></div>
		</div>
		<div class="padding">
			<div><?php echo JText::_("COM_INSUREDIYLIFE_AT_LEAST_1_PLAN_MSG"); ?></div>
			<div style="text-align:right;"><input type="button" class="btn btn-primary" style="height:30px;padding:5px 20px;font-size:14px !important;" onclick="javascript:cleanInsureDIYPopup('insurediy-popup-box');
			return false;" value="Ok" /></div>
		</div>
	</div>
	
	<?php /* Chart Popup Box */ ?>
	<div class="insurediy-popup-big" id="insurediy-chart-popup-box" style="display:none;width: 695px;">
		<div class="header">
			<div class="text-header"><i class="icon-bars"></i>&nbsp;&nbsp;<?php echo JText::_("COM_INSUREDIYLIFE_POP_UP_HEADER_FUTURE_PREMIUMS"); ?></div>
			<a class="close" href="javascript:cleanInsureDIYPopup('insurediy-chart-popup-box');">&nbsp;</a>
			<div class="clear"></div>
		</div>
		<div id="chart_div" style="width: 100%; height: 400px;">
			<?php if (!$showGraph): ?>
			<div style="padding: 25px;">
				<?php echo JText::_("COM_INSUREDIYLIFE_NO_FUTURE_PREMIUMS_MSG"); ?>
			</div>
			<?php endif; ?>
		</div>
	</div>
	<?php if (FALSE): ?>
	<tr>
		<td class="column1 center" style="<?php echo $column_width ?>;padding:10px 5px;"><?php echo JText::_("COM_INSUREDIYLIFE_PLANS"); ?></td>
		<?php foreach ($this->policy_plans as $key => $policy) : ?>
		<td class="column-n" style="<?php echo $column_width ?>;padding:10px 0;word-break: break-all;"><div class="center" style="font-family:montserrat-regular;font-size:14px;"><?php echo $policy->plan_name; //$policy->plan_index_code                                                                                                          ?></div></td>
		<?php endforeach; ?>
	</tr>
	<?php endif; ?>
	
	<script type="text/javascript">
		/*$jq(document).ready(function () {
			var x = $jq("#insurediy-form-body").offset().top;
			var y = $jq("#choosePlanForm .price-row").offset().top;
			$jq(window).scroll(function () {
			var appendTop = (window.pageYOffset > (y - x));
			$jq("#choosePlanForm .insurediy-table-plans").find(".logo-container").each(
			function () {
			var jThis = $jq(this);
			jThis.toggleClass("follow", appendTop);
			}
			);
			});
		});*/
	</script>
	
