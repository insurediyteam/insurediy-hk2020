<?php
	defined('_JEXEC') or die;
	JHtml::_('jquery.framework');
	JHtml::_('script', 'system/jquery.validate.js', false, true);
	JHtml::_('behavior.keepalive');
	JHtml::_('behavior.formvalidation');
	JHtml::_('formbehavior.chosen', '.insurediy-form-content select');
	
	$itemid = JRequest::getVar("itemid", FALSE);
?>

<script type="text/javascript">
	Joomla.submitbutton = function (task)
	{
		if (document.formvalidator.isValid(document.getElementById('adminForm')))
		{
			Joomla.submitform(task);
			return false;
		}
	}
	
	function checkYesOnForm() {
		var criteria = 1;
		$$('input[type="radio"].has_criteria:checked').each(function (elem) {
			if (elem.value == 1) {
				criteria = 0;
			}
		});
		$$('select.has_criteria').each(function (elem) {
			if (elem.value == 1) {
				criteria = 0;
			}
		});
		if (criteria === 0) {
			$('insurediy-popup-box').setStyle('display', 'block');
			return false;
		}
		return true;
	}
	
	<!-- Google Tag Manager Data Layer-->
	/**
		* Call this function when a user clicks on a product link. This function uses the
		event
		* callback datalayer variable to handle navigation after the ecommerce data has
		been sent
		* to Google Analytics.
		* @param {Object} productObj An object representing a
		product.
		*/
		function pushProductClick(productObj) {
			dataLayer.push({
				'event': 'productClick',
				'ecommerce': {
					'click': {
						'actionField': {'list': productObj.list}, // Optional list property.
						'products': [{
							'name': productObj.name,
							// Name or ID is required.
							'id': productObj.id
						}]
					}
				},
				'eventCallback': function() {
				}
			});
		}
	</script>
	<!-- End of Google Tag Manager Data Layer-->
	
</script>
<div class="insurediylife-form">
	<div class="header-top-wrapper">
		<?php echo InsureDIYLifeHelper::renderHeader('icon-term-insurance', JText::_('COM_INSUREDIYLIFE_PAGE_HEADING_YOUR_TERM_COVER'), 1); ?>
	</div>
	
	<div class="edit<?php echo $this->pageclass_sfx; ?> insurediy-form-content" style="margin-top: 60px;">
		<?php
			echo MyHelper::renderDefaultMessage();
		?>
		<form action="<?php echo JRoute::_('index.php?option=com_insurediylife&view=form'); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-vertical" enctype="multipart/form-data">
			<fieldset class="insurediy-life-data">
				<div>
					<div class="control-group2">
						<div class="control-label"><?php echo $this->form->getLabel('cover_amt'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('cover_amt'); ?></div>
					</div>
					<div class="control-group2">
						<div class="control-label"><?php echo $this->form->getLabel('cover_length'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('cover_length'); ?></div>
					</div>
					<div class="clear"></div>
				</div>
				<div class="grey-hrule" style="margin: 10px 0"></div>
				<div class="margin-bottom20">
					<div class="control-group4">
						<div class="control-label"><?php echo $this->form->getLabel('gender'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('gender'); ?></div>
					</div>
					<div class="control-group4">
						<div class="control-label"><?php echo $this->form->getLabel('dob'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('dob'); ?></div>
					</div>
					<div class="control-group4">
						<div class="control-label"><?php echo $this->form->getLabel('weight_amt'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('weight_amt'); ?></div>
					</div>
					<div class="control-group4">
						<div class="control-label"><?php echo $this->form->getLabel('height_amt'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('height_amt'); ?></div>
					</div>
					<div class="clear"></div>
				</div>
				
				<div class="margin-bottom20">
					<div class="control-group2">
						<div class="control-label"><?php echo $this->form->getLabel('has_weight_change'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('has_weight_change'); ?></div>
					</div>
					<div class="control-group2">
						<div class="control-label"><?php echo $this->form->getLabel('has_used_tobacco'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('has_used_tobacco'); ?></div>
					</div>
					<div class="clear"></div>
				</div>
				
				<div class="margin-bottom20">
					<div class="control-group2">
						<div class="control-label"><?php echo $this->form->getLabel('has_used_alcohol'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('has_used_alcohol'); ?></div>
					</div>
					<div class="control-group2">
						<div class="control-label"><?php echo $this->form->getLabel('has_used_drugs'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('has_used_drugs'); ?></div>
					</div>
					<div class="clear"></div>
				</div>
				
				<div class="margin-bottom20">
					<div class="control-group2">
						<div class="control-label"><?php echo $this->form->getLabel('has_physicians'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('has_physicians'); ?></div>
					</div>
					<div class="control-group2">
						<div class="control-label"><?php echo $this->form->getLabel('marital_status'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('marital_status'); ?></div>
					</div>
					<div class="clear"></div>
				</div>
				
				<div class="margin-bottom20">
					<div class="control-group2">
						<div class="control-label"><?php echo $this->form->getLabel('occupation'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('occupation'); ?></div>
					</div>
					<div class="control-group2">
						<div class="control-label"><?php echo $this->form->getLabel('job_nature'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('job_nature'); ?></div>
					</div>
					<div class="clear"></div>
				</div>
				
				<div class="margin-bottom20">
					<div class="control-group2 margin-bottom20">
						<div class="control-label"><?php echo $this->form->getLabel('monthly_income'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('monthly_income'); ?></div>
					</div>
					<div class="control-group2">
						<div class="control-label"><?php echo $this->form->getLabel('country_residence'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('country_residence'); ?></div>
					</div>
					<div class="clear"></div>
				</div>
				
				<div class="margin-bottom20">
					<div class="control-group2">
						<div class="control-label"><?php echo $this->form->getLabel('nationality'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('nationality'); ?></div>
					</div>
					<div class="control-group2">
						<div class="control-label"><?php echo $this->form->getLabel('has_reside_overseas'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('has_reside_overseas'); ?></div>
					</div>
					<div class="clear"></div>
				</div>
				
				<div class="margin-bottom20">
					<div class="control-group2">
						<div class="control-label"><?php echo $this->form->getLabel('has_claimed_insurance'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('has_claimed_insurance'); ?></div>
					</div>
					<div class="control-group2">
						<div class="control-label"><?php echo $this->form->getLabel('has_reinstate_insurance'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('has_reinstate_insurance'); ?></div>
					</div>
					<div class="clear"></div>
				</div>
			</fieldset>
			<fieldset class="insurediy-agreements">
				<?php echo MyHelper::load_module_pos("agreements"); ?>
				<?php if (FALSE): ?>
				<?php foreach ($this->form->getFieldset("agree_fields") as $field): ?>
				<div>
					<div class="control-group">
						<div class="control-label" style="float:left;width:4.5%;"><div style="position:relative;"><?php echo $field->input; ?></div></div>
						<div class="controls" style="float:left;width:95%"><?php echo $field->label; ?></div>
						<div style="clear:both"></div>
					</div>
				</div>
				<?php endforeach; ?>
				<?php endif; ?>
			</fieldset>
			<input type="hidden" name="task" value="form.step1save" />
			<?php echo $this->form->getInput('protection_needs_analysis'); ?>
			<?php echo $this->form->getInput('id'); ?>
			<?php if ($itemid): ?>
			<input type="hidden" name="Itemid" value="$itemid" />
			<?php endif; ?>
			<?php echo JHtml::_('form.token'); ?>
			
			<div style="float:left;margin:15px 0;">
				{modulepos insurediy-secured-ssl}
			</div>
			<div class="btn-toolbar" style="float:right">
				<div class="btn-group">
					<button type="submit" class="btn btn-primary validate" onclick="pushProductClick({name:'Life', id:'lifel01',list:'mainpage'})"><?php echo JText::_('JCONTINUE') ?></button>
				</div>
			</div>
			<div class="clear"></div>
		</form>
	</div>
</div>
<div class="logo-slider"><?php echo MyHelper::load_module(102); ?></div>

<?php /* Popup Box */ ?>
<div class="insurediy-popup" id="insurediy-popup-box" style="display:none;">
	<div class="header">
		<div class="text-header"><?php echo JText::_("POP_IMPORTANT_NOTICE"); ?></div>
		<a class="close" href="javascript:cleanInsureDIYPopup('insurediy-popup-box');">&nbsp;</a>
		<div class="clear"></div>
	</div>
	<div class="padding">
		<div><?php echo JText::_("COM_INSUREDIYLIFE_MEDICAL_ISSUE_MSG"); ?></div>
		<div style="text-align:right;"><input type="button" class="btn btn-primary" style="height:30px;padding:5px 20px;font-size:14px !important;" onclick="javascript:cleanInsureDIYPopup('insurediy-popup-box');
		return false;" value="<?php echo JText::_("JOK"); ?>" /></div>
	</div>
</div>

<?php /* Popup Box */ ?>
<div class="insurediy-popup" id="insurediy-popup-box-2" <?php echo $this->askTna ? '' : 'style="display:none;"'; ?> >
	<div class="header">
		<div class="text-header"><?php echo JText::_("POP_IMPORTANT_NOTICE"); ?></div>
		<a class="close" href="javascript:tnaOnClickNo();">&nbsp;</a>
		<div class="clear"></div>
	</div>
	<div class="padding">
		<div><?php echo JText::_("COM_INSUREDIYLIFE_NEW_QUOTE_POP_UP_MSG"); ?></div>
		<form action="<?php echo JRoute::_('index.php'); ?>" method="post" name="adminForm2" id="adminForm2" class="form-validate form-vertical" enctype="multipart/form-data">
			<div style="text-align:right;">
				<input type="button" class="btn btn-primary pop-up-button" onclick="tnaOnClickNo()" value="<?php echo JText::_("JNO"); ?>" />
				<input type="submit" class="btn btn-primary pop-up-button" value="<?php echo JText::_("JYES"); ?>" />
			</div>
			<?php echo JHtml::_('form.token'); ?>
			<input type='hidden' name='protection_needs_analysis' value='1' />
			<input type='hidden' name='option' value='com_insurediylife' />
			<input type='hidden' name='task' value='form.step0save' />
		</form>
		
	</div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function () {
		jQuery("#adminForm").validate({
			ignore: [], // <-- option so that hidden elements are validated
		});
		console.log();
		var jForm = jQuery("form#adminForm");
		var jSelects = jForm.find("select");
		var jInputs = jForm.find("input");
		jQuery('html, body').scrollTop(0);
		jSelects.each(function () {
			var jThis = jQuery(this);
			jThis.on("change", selectOnChange);
		});
		jInputs.each(function () {
			var jThis = jQuery(this);
			jThis.on("change", selectOnChange);
		});
		var jForm = jQuery("form#adminForm");
		jForm.submit(function () {
			if ($jq("#insurediy-popup-box-2").css("display") !== "none") {
				return false;
			}
			var errors = jForm.find("label.error");
			if (errors.length > 0) {
				var label = jForm.find("label.error:first");
				jQuery('html, body').animate({
					'scrollTop': label.offset().top - 350
				}, 300);
			}
			
			var jAgreeContainer = jQuery("fieldset.insurediy-agreements");
			jAgreeContainer.find("label.error").each(function () {
				var jThis = jQuery(this);
				var jContainer = jThis.closest("div.control-group");
				var jControl = jContainer.find("div.controls");
				jControl.append(jThis.clone());
				jThis.remove();
			});
		});
	});
	function selectOnChange() {
		var jThis = jQuery(this);
		if (jThis.val()) {
			jThis.siblings("label.error").remove();
		}
	}
	function tnaOnClickNo() {
		$jq("#jform_protection_needs_analysis").val("0");
		cleanInsureDIYPopup('insurediy-popup-box-2');
		return false;
	}
</script>
