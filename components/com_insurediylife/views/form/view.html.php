<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_insurediylife
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * HTML Article View class for the component
 *
 * @package     Joomla.Site
 * @subpackage  com_insurediylife
 * @since       1.5
 */
class InsureDIYLifeViewForm extends JViewLegacy {

	protected $form;
	protected $item;
	protected $state;

	public function display($tpl = null) {
		$session = JFactory::getSession();
		$model = $this->getModel();
		$app = JFactory::getApplication();
		$params = JComponentHelper::getParams('com_insurediylife');
		$user = JFactory::getUser();
		//$quotation = LifeQuotation::getFields();
		$quotation_id = InsureDIYLifeHelper::getQid();
		$quotation = InsureDIYLifeHelper::getQuotation($quotation_id);

		$form = $this->get("Form");
		$currentUser = MyHelper::getCurrentUser();

//		$userPath = MyHelper::getDeepPath(LIFE_PDF_SAVE_PATH, $quotation['user_id']);
		// testing
//		$forms = InsureDIYHelper::getCompanyForms("AIA");
//		$pdfData = InsureDIYLifeHelper::getPdfData("1");
//		$gOrigin = (isset($forms['life_generic']) && strlen($forms['life_generic'])) ? $forms['life_generic'] : "";
//
//
//		if (JFile::exists($gOrigin)) {
//			$filename = InsureDIYHelper::generateFileName($forms['life_generic']);
//			$dest = $userPath . DS . $filename;
//
//			$result = InsureDIYHelper::generatePdfForm($gOrigin, $dest, $pdfData);
//			MyHelper::debug($result);
//		}
//		exit;
		// Check for errors.
		
		$menu_params = $app->getMenu()->getActive()->params;
		$this->menu_params = $menu_params;
		
		if (count($errors = $this->get('Errors'))) {
			JError::raiseWarning(500, implode("\n", $errors));
			return false;
		}

		$layout = InsureDIYLifeHelper::getLayout();
		//Re Generate Unique Order No
		$db = JFactory::getDBO();
		$dataobj = new stdClass();
		$dataobj->id = $quotation['id'];
		$dataobj->unique_order_no = InsureDIYHelper::getUniqueOrderNo($user->id);
		
		switch ($layout) {
			case 'step1':
				$this->askTna = is_null($session->get('life.protection_needs_analysis', NULL));
			case 'step2':
				$this->questionnaires = $this->get('Questionnaires');
				$this->step1CriteriaPass = $session->get('life.step1CriteriaPass');
				$this->ask4override = $session->get("life.step1.ask4override", TRUE);
				break;
			case 'step3':
				$this->policy_plans = $this->get('PolicyPlans');
				$this->future_policy_plans = $this->get('FuturePolicyPlans');

				$companies = array();
				foreach ($this->policy_plans as $plan) {
					$companies[] = $plan->insurer_code;
				}

				$cdata = array();
				$ages = array();
				$cname = array();
				$colorCode = array();
				$length_cover = $model->getLengthCover($quotation['cover_length']);
				$age = "";
				foreach ($this->future_policy_plans as $future_policy_plan) {
					$age .= $future_policy_plan->age;
					if (in_array($future_policy_plan->insurer_code, $companies)) {
						$cdata[$future_policy_plan->insurer_code][$future_policy_plan->age] = $future_policy_plan->price;
						for ($i = 1; $i < $length_cover; $i++) {
							$cdata[$future_policy_plan->insurer_code][$future_policy_plan->age + $i] = $future_policy_plan->price;
						}
					}
					if (!in_array($future_policy_plan->age, $ages)) {
						$ages[] = $future_policy_plan->age;
						for ($i = 1; $i < $length_cover; $i++) {
							$ages[] = $future_policy_plan->age + $i;
						}
					}
					if (!in_array($future_policy_plan->company_name, $cname)) {
						$cname[] = $future_policy_plan->company_name;
						$colorCode[] = ($future_policy_plan->color_code) ? $future_policy_plan->color_code : '000000';
					}
				}
				$chartData = array();
				$data1 = array_merge(array(0 => "Age"), $cname);
				$chartData[] = $data1; // first row
				foreach ($ages as $age) {
					$temp = array();
					$temp[] = (int) $age;
					foreach ($cdata as $d) {
						$temp[] = (isset($d[$age])) ? (int) $d[$age] : null;
					}
					$chartData[] = $temp;
				}
				if ($length_cover > 1) {
					$ages = $model->getAgesForGraph($length_cover, $quotation['age']);
				} else {
					$ages = $model->getAgesForGraph(5, $quotation['age']);
				}
				$year_code = $quotation['cover_length'];
				$showGraph = TRUE;
//				$form->setValue("cover_length", NULL, $year_code);
				if ($year_code == "60T75" || count($cdata) == 0) {
					$showGraph = FALSE;
				}
				$this->showGraph = $showGraph;
				$this->maxAge = (count($ages) > 0) ? max($ages) : 0;
				$this->ages = $ages;
				$this->chartData = $chartData;
				$this->colorData = $colorCode;
				break;
			case 'step4':
				$this->has_physicians = $quotation['has_physicians'];

				$referred_by = $session->get(SESSION_KEY_REFID, FALSE);
				if (!$currentUser->referred_by && $referred_by) {
					$form->setValue('referred_by', "", $referred_by);
				}
				break;

			case 'extrainfo':
				$this->my_sum_insured_total = $model->checkSumInsuredQuotationTotal($quotation['id']);
				break;

			case 'step5':
				//$quotation['unique_order_no'] = $dataobj->unique_order_no;
				$quotation['complete_address'] = InsureDIYLifeHelper::getCompleteAddress($quotation);
				$db->updateObject('#__insure_life_quotations', $dataobj, "id");
				$this->paymentData = $this->get('PDPayment');
				if (!$this->paymentData) {
					$app->enqueueMessage(JText::_("ERR_PAYMENT_CONFIG"));
				}
				break;
			case 'step6' :
				//$db->updateObject('#__insure_life_quotations', $dataobj, "id");
				$quotation['complete_address'] = InsureDIYLifeHelper::getCompleteAddress($quotation);
				$time = strtotime($quotation['created_date']);
				$year = (int) date("Y", $time);
				$month = (int) date("m", $time);
				$day = (int) date("d", $time);

				$x_days = (int) $params->get('x_days');
				$y_days = (int) $params->get('y_days');

				$quotation['sign_start'] = date("d M Y", mktime(0, 0, 0, $month, ($day + $x_days), $year));
				$quotation['sign_end'] = date("d M Y", mktime(0, 0, 0, $month, ($day + $x_days + $y_days), $year));

				// data for email
				$session->set("life.maildata.sign_start." . $quotation['id'], $quotation['sign_start']);
				$session->set("life.maildata.sign_end." . $quotation['id'], $quotation['sign_end']);
			break;
		}

		// Total Premiums for Tracking
		if(isset($quotation['selected_plans'])) {
			$total_premium = 0.0;
			foreach($quotation['selected_plans'] as $k => $v) {
				$total_premium += (float) $v->price;
			}
			$quotation['total_premium_price'] = $total_premium;
		}
		
		$this->quotation = $quotation;
		$this->setLayout($layout);
		$this->state = $this->get('State');
		$this->pageclass_sfx = htmlspecialchars($params->get('pageclass_sfx'));
		$this->currency = $params->get("currency", "HK$");
		$this->params = $params;
		$this->user = $user;
		$this->current_user = $currentUser;
		$this->form = $form;
		$this->flag_banner_path = 'images/flag_banners';
		$this->_prepareDocument();
		
		// only to clear session purpose.
		switch ($layout) {
			case 'thankyou':
			InsureDIYLifeHelper::clearSessionData(); // woot~ all done. clear the session
			break;
		}
		
		parent::display($tpl);
	}

	/**
	 * Prepares the document
	 */
	protected function _prepareDocument() {
		$app = JFactory::getApplication();
		$menus = $app->getMenu();

		// Because the application sets a default page title,
		// we need to get it from the menu item itself
		$menu = $menus->getActive();

		$head = JText::_('COM_INSUREDIYLIFE_FORM_PAGE_HEADING');
		if ($menu) {
			$this->params->def('page_heading', $this->params->get('page_title', $menu->title));
		} else {
			$this->params->def('page_heading', $head);
		}

		$title = $this->params->def('page_title', $head);
		if ($app->getCfg('sitename_pagetitles', 0) == 1) {
			$title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
		} elseif ($app->getCfg('sitename_pagetitles', 0) == 2) {
			$title = JText::sprintf('JPAGETITLE', $title, $app->getCfg('sitename'));
		}
		$this->document->setTitle($title);

		if ($this->params->get('menu-meta_description')) {
			$this->document->setDescription($this->params->get('menu-meta_description'));
		}

		if ($this->params->get('menu-meta_keywords')) {
			$this->document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}

		if ($this->params->get('robots')) {
			$this->document->setMetadata('robots', $this->params->get('robots'));
		}

//		//facebook stuffs
//		$this->document->setMetaData("og:title", InsureDIYLifeHelper::getFbTitle());
//		$this->document->setMetaData("og:description", InsureDIYLifeHelper::getFbDesc());
//		$this->document->setMetaData("og:image", InsureDIYLifeHelper::getFbImage());
//		$this->document->setMetaData("og:app_id", InsureDIYLifeHelper::getFbAppId());
//		$this->document->setMetaData("og:site_name", InsureDIYLifeHelper::getFbSiteName());
	}

}
