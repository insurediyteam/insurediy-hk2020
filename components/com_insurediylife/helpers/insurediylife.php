<?php

defined('_JEXEC') or die;

class InsureDIYLifeHelper {

	public static function renderHeader($icon, $header, $active) {
		$classes = array();
		switch ($active) {
			case 1:
				$classes = array('-active', '', '', '-inactive', '', '-inactive', '', '');
				break;
			case 2:
				$classes = array('-active', '-active', '-active', '', '', '-inactive', '', '');
				break;
			case 3:
				$classes = array('-active', '-active', '-active', '-active', '-active', '', '', '');
				break;
			case 4:
				$classes = array('-active', '-active', '-active', '-active', '-active', '-active', '-active', '-active');
				break;
			default:
				break;
		}

		$buffer = '<div class="header-top">';
		$buffer.= '<div class="h1-heading">';
		$buffer.= '<i class="' . $icon . '" ></i>';
		$buffer.= $header;
		$buffer.='</div>';
		$buffer.= '<div class="insurediy-breadcrumbs">';
		$buffer.= '<div class="mybreadcrumb back"></div>';
		$buffer.= '<div class="mybreadcrumb middle' . $classes[0] . '">';
		$buffer.= JText::_("COM_INSUREDIYLIFE_BREADCRUMB_YOUR_DETAILS") . '</div>';
		$buffer.='<div class="mybreadcrumb between' . $classes[1] . '">';
		$buffer.= '</div><div class = "mybreadcrumb middle' . $classes[2] . '">';
		$buffer.= JText::_("COM_INSUREDIYLIFE_BREADCRUMB_SELECT_PRODUCT");
		$buffer.='</div>';
		$buffer.='<div class="mybreadcrumb between' . $classes[3] . '">';
		$buffer.='</div>';
		$buffer.='<div class="mybreadcrumb middle' . $classes[4] . '">';
		$buffer.= JText::_("COM_INSUREDIYLIFE_BREADCRUMB_APPLY") . '</div>';
		$buffer.='<div class="mybreadcrumb between' . $classes[5] . '">';
		$buffer.='</div>';
		$buffer.='<div class="mybreadcrumb middle' . $classes[6] . '">';
		$buffer.= JText::_("COM_INSUREDIYLIFE_BREADCRUMB_PAYMENT") . '</div>';
		$buffer.='<div class="mybreadcrumb front' . $classes[7] . '"></div>';
		$buffer.='<div class="clear"></div>';
		$buffer.= '</div>';
		$buffer.= '<div class="clear"></div></div>';
		return $buffer;
	}

	public static function getMyCurrentQuotationID() {

		$session = JFactory::getSession();
		$quotation_id = JRequest::getVar('quotation_id');

		if ($quotation_id) {
			$session->set('quotation_id', $quotation_id);
		} else {
			$quotation_id = $session->get('quotation_id');
		}

		return $quotation_id;
	}

	public static function generatePDF($xmlpath, $data, $pdfpath, $pdfname, $path) {
		$raw = MySimpleXMLHelper::loadFile($xmlpath);
		$xml = MySimpleXMLHelper::bind($raw, $data);
		$namesurfix = (isset($data->namesurfix)) ? $data->namesurfix : "_" . JHtml::_('date', time(), 'dMYHis') . ".pdf";
		return MySimpleFPDIHelper::generate($path . $pdfpath, $xml, $data->id, $pdfname . $namesurfix);
	}

	public static function generateAIA($xmlpath, $data) {
		$arr = array();
		$temp = array();
		$path = "images/insurediyform/form/aia/";
		$temp[] = InsureDIYLifeHelper::generatePDF($xmlpath . "aia.app.xml", $data, "AIA-ApplicationV2.pdf", "AIA-Application", $path);
		$temp[] = InsureDIYLifeHelper::generatePDF($xmlpath . "aia.dd.xml", $data, "AIA-Direct_Debit.pdf", "AIA-Direct_Debit", $path);
		if ($data->hasLargeSum) {
			$temp[] = InsureDIYLifeHelper::generatePDF($xmlpath . "aia.ls.xml", $data, "AIA-Large_Amount_Questionnaire.pdf", "AIA-Large_Amount_Questionnaire", $path);
		}
		foreach ($temp as $v) {
			if ($v) {
				$arr[] = $v;
			} else {
				return FALSE;
			}
		}
		return $arr;
	}

	public static function generateSUN($xmlpath, $data) {
		$arr = array();
		$temp = array();
		$path = "images/insurediyform/form/sun/";

		$temp[] = $this->generatePDF($xmlpath . "sun.app1.xml", $data, "SunLife-Application-1.pdf", "SunLife-Application-1", $path);
		$temp[] = $this->generatePDF($xmlpath . "sun.app2.xml", $data, "SunLife-Application-2.pdf", "SunLife-Application-2", $path);
		$temp[] = $this->generatePDF($xmlpath . "sun.app3.xml", $data, "SunLife-Application-3.pdf", "SunLife-Application-3", $path);
		$temp[] = $this->generatePDF($xmlpath . "sun.dda.xml", $data, "Sun-DDA.pdf", "Sun-Direct_Debit", $path);
		$temp[] = $this->generatePDF($xmlpath . "sun.cd.xml", $data, "Sun-Customer_Declaration.pdf", "Sun-Customer_Declaration", $path);
		if ($data->hasLargeSum) {
			$temp[] = $this->generatePDF($xmlpath . "sun.ls.xml", $data, "Sun-Large_Amount_Questionarie.pdf", "Sun-Large_Amount_Questionnaire", $path);
		}

		foreach ($temp as $v) {
			if ($v) {
				$arr[] = $v;
			} else {
				return FALSE;
			}
		}
		return $arr;
	}

	public static function getCompleteAddress($data) {
		$complete_address = '';
		$complete_address .= ($data['contact_room_no'] ? 'Flat ' . $data['contact_room_no'] : '');
		$complete_address .= ($data['contact_floor_no'] ? ', ' . $data['contact_floor_no'] . '/F' : '');
		$complete_address .= ($data['contact_block_no'] ? ', Blk ' . $data['contact_block_no'] : '');
		$complete_address .= ($data['contact_building_name'] ? ', ' . $data['contact_building_name'] : '');
		$complete_address .= ($data['contact_street_name'] ? ', ' . $data['contact_street_name'] : '');
		$complete_address .= ($data['contact_district_name'] ? ', ' . $data['contact_district_name'] : '');
		$complete_address .= ($data['contact_country'] ? ', ' . $data['contact_country'] . ' ' : '');
		return $complete_address;
	}

	// PARAMS
	public static function getAppSummeryMod($default = "") {
		$params = JComponentHelper::getParams("com_insurediylife");
		return $params->get("app_summery", $default);
	}

	public static function getDeclarationsMod($default = "") {
		$params = JComponentHelper::getParams("com_insurediylife");
		return $params->get("declarations", $default);
	}

	public static function getReachAmount($default = 5000000) {
		$params = JComponentHelper::getParams("com_insurediylife");
		return $params->get("reach_amount", $default);
	}

	public static function getCurrency($default = "HK$") {
		$params = JComponentHelper::getParams("com_insurediylife");
		return $params->get("currency", $default);
	}

	public static function getFbTitle($default = "") {
		$params = JComponentHelper::getParams("com_insurediylife");
		return $params->get("fbtitle", $default);
	}

	public static function getFbDesc($default = "") {
		$params = JComponentHelper::getParams("com_insurediylife");
		return $params->get("fbdesc", $default);
	}

	public static function getFbImage($default = "") {
		$params = JComponentHelper::getParams("com_insurediylife");
		return $params->get("fbimage", $default);
	}

	public static function getFbSiteName($default = FALSE) {
		$default = ($default) ? $default : JFactory::getConfig()->get("sitename", "");
		$params = JComponentHelper::getParams("com_insurediylife");
		return $params->get("fbsitename", $default);
	}

	public static function getFbAppId($default = "") {
		$params = JComponentHelper::getParams("com_insurediylife");
		return $params->get("fbappid", $default);
	}

	public static function getRefExp($default = "") {
		$params = JComponentHelper::getParams("com_insurediylife");
		return $params->get("refExp", $default);
	}

	public static function getRefMsg($default = "") {
		$params = JComponentHelper::getParams("com_insurediylife");
		return str_replace("XXYY", JFactory::getUser()->referral_id, $params->get("refMsg", $default));
	}

	// QUOTATION STUFFS
	public static function isOldQuote() {
		return self::getQid();
	}

	public static function getCurrentQid() {
		return JFactory::getSession()->get("life.quotation_id", 0);
	}

	public static function getInputQid() {
		return JFactory::getApplication()->input->get("quotation_id", 0, "integer");
	}

	public static function getQid() {
		return self::getInputQid() ? self::getInputQid() : self::getCurrentQid();
	}

	public static function canViewLayout($layout) {
		$user = JFactory::getUser();
		$stage = self::getStageFromLayout($layout);
		$current = self::getCurrentStage(self::getQid());
		if ($stage > 1 && $user->id < 1) {
			return FALSE;
		}
		if ($current > 6 && $stage <= 6) {
			return FALSE;
		}
		return $stage <= $current;
	}

	public static function getLayout() {
		$user = JFactory::getUser();
		$app = JFactory::getApplication();

		$inputLayout = $app->input->get("layout", FALSE);
		$staticLayouts = array("error", "thankyou", "canceled", "syserror");

		if ($inputLayout && in_array($inputLayout, $staticLayouts)) {
			return $inputLayout;
		}

		$qid = self::getQid();
		if (!$qid) {
			return "step1";
		}
		$stage = self::getCurrentStage($qid);
		if ($stage > 1 && $user->id < 1) {
			return "login";
		}
		return self::getLayoutFromStage($stage);
	}

	public static function checkOwnership($id) {
		$user = JFactory::getUser();
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("count(*)")
				->from("#__insure_life_quotations")
				->where("user_id = " . $db->quote($user->id))
				->where("id = " . $db->quote($id));
		$db->setQuery($query);
		return $db->loadResult();
	}

	public static function getCurrentStage($id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("quote_stage")
				->from("#__insure_life_quotations")
				->where("id = " . $db->quote($id));
		$db->setQuery($query);
		return $db->loadResult();
	}

	public static function getLayoutFromStage($step) {
		$stageToLayouts = array(1 => "step1", 2 => "step2", 3 => "step3", 4 => "step4", 5 => "extrainfo", 6 => "step5", 7 => "step6", 8 => "thankyou");
		return isset($stageToLayouts[$step]) ? $stageToLayouts[$step] : "step1";
	}

	public static function getStageFromLayout($layout) {
		$layoutToStages = array("step1" => 1, "login" => 2, "step2" => 2, "step3" => 3, "step4" => 4, "extrainfo" => 5, "step5" => 6, "step6" => 7, "thankyou" => 8);
		return isset($layoutToStages[$layout]) ? $layoutToStages[$layout] : 0;
	}

	public static function clearSessionData() {
		$session = JFactory::getSession();
		$session->set("life.data", NULL);
		$session->set("life.quotation_id", NULL);
		$session->set("life.protection_needs_analysis", NULL);
		$session->set('life.step1CriteriaPass', NULL);
	}

	public static function getQuotation($qid = FALSE) {
		if (!$qid) {
			$qid = self::getQid();
		}
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_life_quotations")
				->where("id = " . $db->quote($qid));
		$quotation = $db->setQuery($query)->loadAssoc();
		if ($quotation) {
			$quotation['age'] = MyHelper::getAge($quotation['dob']);
			$quotation['is_smoking'] = $quotation['has_used_tobacco'];
			$quotation['residency'] = $quotation['country_residence'];

			$planQuery = $db->getQuery(TRUE)->select("px.*, c.company_name, p.insurer_code")
					->from("#__insure_life_quotation_plan_xref px")
					->leftJoin("#__insure_life_plans AS p ON p.plan_index_code = px.plan_index_code AND p.sum_insured = px.sum_insured AND p.price = px.price")
					->leftJoin("#__insure_companies AS c ON c.insurer_code = p.insurer_code")
					->where("px.quotation_id = " . $db->quote($quotation['id']));
			$quotation['selected_plans'] = $db->setQuery($planQuery)->loadObjectList();
			if (count($quotation['selected_plans'])) {
				foreach ($quotation['selected_plans'] as $sp) {
					$quotation['selected_plan_pics'][] = $sp->plan_index_code.'|'.$sp->price;
				}
			}

			$exQuery = $db->getQuery(TRUE)->select("*")->from("#__insure_life_quotation_existing_insurances")->where("quotation_id = " . $db->quote($quotation['id']));
			$exInsurances = $db->setQuery($exQuery)->loadObjectList();
			$quotation['exInsurances'] = ($exInsurances) ? $exInsurances : array();

			$carQuery = $db->getQuery(TRUE)->select("*")->from("#__insure_life_quotation_cars")->where("quotation_id = " . $db->quote($quotation['id']));
			$cars = $db->setQuery($carQuery)->loadObjectList();
			$quotation['cars'] = ($cars) ? $cars : array();

			$propQuery = $db->getQuery(TRUE)->select("*")->from("#__insure_life_quotation_properties")->where("quotation_id = " . $db->quote($quotation['id']));
			$props = $db->setQuery($propQuery)->loadObjectList();
			$quotation['props'] = ($props) ? $props : array();

			$answerQuery = $db->getQuery(TRUE)->select("*")->from("#__insure_life_quotation_questionnaire")->where("quotation_id = " . $db->quote($quotation['id']));
			$answers = $db->setQuery($answerQuery)->loadObjectList("question_id");
			$quotation['answers'] = ($answers) ? $answers : array();
		}
		return $quotation;
	}

	public static function getQuotationData($qid) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_life_quotations")
				->where("id = " . $db->quote($qid));
		$quotation = $db->setQuery($query)->loadAssoc();
		if ($quotation) {
			$quotation['age'] = MyHelper::getAge($quotation['dob']);
			$quotation['is_smoking'] = $quotation['has_used_tobacco'];
			$quotation['residency'] = $quotation['country_residence'];

			$planQuery = $db->getQuery(TRUE)->select("px.*, c.company_name, p.insurer_code")
					->from("#__insure_life_quotation_plan_xref px")
					->leftJoin("#__insure_life_plans AS p ON p.plan_index_code = px.plan_index_code AND p.sum_insured = px.sum_insured")
					->leftJoin("#__insure_companies AS c ON c.insurer_code = p.insurer_code")
					->where("px.quotation_id = " . $db->quote($quotation['id']));
			$quotation['selected_plans'] = $db->setQuery($planQuery)->loadObjectList();
			if (count($quotation['selected_plans'])) {
				foreach ($quotation['selected_plans'] as $sp) {
					$quotation['selected_plan_pics'][] = $sp->plan_index_code;
				}
			}

			$exQuery = $db->getQuery(TRUE)->select("*")->from("#__insure_life_quotation_existing_insurances")->where("quotation_id = " . $db->quote($quotation['id']));
			$exInsurances = $db->setQuery($exQuery)->loadObjectList();
			$quotation['exInsurances'] = ($exInsurances) ? $exInsurances : array();

			$carQuery = $db->getQuery(TRUE)->select("*")->from("#__insure_life_quotation_cars")->where("quotation_id = " . $db->quote($quotation['id']));
			$cars = $db->setQuery($carQuery)->loadObjectList();
			$quotation['cars'] = ($cars) ? $cars : array();

			$propQuery = $db->getQuery(TRUE)->select("*")->from("#__insure_life_quotation_properties")->where("quotation_id = " . $db->quote($quotation['id']));
			$props = $db->setQuery($propQuery)->loadObjectList();
			$quotation['props'] = ($props) ? $props : array();

			$answerQuery = $db->getQuery(TRUE)->select("*")->from("#__insure_life_quotation_questionnaire")->where("quotation_id = " . $db->quote($quotation['id']));
			$answers = $db->setQuery($answerQuery)->loadObjectList();
			$quotation['answers'] = ($answers) ? $answers : array();
		}
		return $quotation;
	}

	public static function getPdfData($qid = FALSE) {
		if (!$qid) {
			$qid = self::getQid();
			if (!$qid) {
				return array();
			}
		}
		$quotation = self::getQuotationData($qid);

		foreach ($quotation['selected_plans'] as $k => $v) {
			foreach ($v as $k2 => $v2) {
				$key = "plan_" . ($k + 1) . "_" . $k2;
				$quotation[$key] = $v2;
			}
		}
		unset($quotation['selected_plans']);
		unset($quotation['selected_plan_pics']);

		foreach ($quotation['exInsurances'] as $k => $v) {
			foreach ($v as $k2 => $v2) {
				$key = "ex_insurance_" . ($k + 1) . "_" . $k2;
				$quotation[$key] = $v2;
			}
		}
		unset($quotation['exInsurances']);

		foreach ($quotation['cars'] as $k => $v) {
			foreach ($v as $k2 => $v2) {
				$key = "car_" . ($k + 1) . "_" . $k2;
				$quotation[$key] = $v2;
			}
		}
		unset($quotation['cars']);

		foreach ($quotation['props'] as $k => $v) {
			foreach ($v as $k2 => $v2) {
				$key = "property_" . ($k + 1) . "_" . $k2;
				$quotation[$key] = $v2;
			}
		}
		unset($quotation['props']);

		foreach ($quotation['answers'] as $k => $v) {
			foreach ($v as $k2 => $v2) {
				$key = "question_" . ($k + 1) . "_" . $k2;
				$quotation[$key] = $v2;
			}
		}
		unset($quotation['answers']);

		$dob = explode("-", $quotation['dob']);

		$quotation['contact_fullname'] = strtoupper($quotation['contact_firstname'] . " " . $quotation['contact_lastname']);
		$quotation['broker_code'] = "89192";
		$quotation['broker_name'] = "InsureDIY Limited";
		$quotation['tech_rep_name'] = "Choo Oi San";
		$quotation['tech_rep_rel'] = "3975 2896";
		$quotation['currency'] = "HKD";
		$quotation['age'] = MyHelper::getAge($quotation['dob']);
		$quotation['dob_month'] = $dob[2];
		$quotation['dob_date'] = $dob[1];
		$quotation['dob_year'] = $dob[0];

		return $quotation;
	}

}
