<?php

defined('_JEXEC') or die;

class InsureDIYLifeControllerForm extends JControllerForm {

	private $base_layout_url = "index.php?option=com_insurediylife&view=form&layout=";
	private $form_url = "index.php?option=com_insurediylife&Itemid=156";

	public function getModel($name = 'form', $prefix = '', $config = array('ignore_request' => true)) {
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}

	protected function getRedirectToItemAppend($recordId = null, $urlVar = null) {
		$append = parent::getRedirectToItemAppend($recordId, $urlVar);
		$itemId = $this->input->getInt('Itemid');
		$return = $this->getReturnPage();

		if ($itemId) {
			$append .= '&Itemid=' . $itemId;
		}

		if ($return) {
			$append .= '&return=' . base64_encode($return);
		}

		return $append;
	}

	protected function getReturnPage() {
		$return = $this->input->get('return', null, 'base64');
		if (empty($return) || !JUri::isInternal(base64_decode($return))) {
			return JUri::base();
		} else {
			return base64_decode($return);
		}
	}

	public function step0save() {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$app = JFactory::getApplication();
		$session = JFactory::getSession();
		$session->set('life.protection_needs_analysis', 1);
		$app->redirect(JRoute::_("index.php?option=com_calculator&view=life&return=life&Itemid=186"));
	}

	public function step1save() {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$session = JFactory::getSession();
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$post = $app->input->get('jform', '', 'array');
		$step1CriteriaPass = $this->step1CriteriaCheck($post);

		//prepare data for contact detail page
		$phonecode = InsureDIYHelper::getCountryPhoneCode($post['country_residence']);
		$post['contact_country'] = $post['country_residence'];
		$post['contact_country_code'] = $phonecode;
		$quotation_id = $model->step1save($post);
		if ($quotation_id) {
			$session->set('life.step1CriteriaPass', $step1CriteriaPass);
			$session->set('life.quotation_id', $quotation_id);
		}
		
		$layout = InsureDIYLifeHelper::getLayout();
		
		if($layout == 'login') {
			$app->redirect(JRoute::_($this->form_url.'&step=sign_in'));
		} else {
			$app->redirect(JRoute::_($this->form_url.'&step=2'));
		}
		
	}

	public function step1CriteriaCheck($data) {
		return !($data['has_weight_change'] || $data['has_used_tobacco'] || $data['has_used_alcohol'] || $data['has_used_drugs'] || $data['has_reside_overseas'] || $data['has_claimed_insurance'] || $data['has_reinstate_insurance']);
	}

	public function step2save() {
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$post = $app->input->get('jform', '', 'array');
		if (!$model->step2save($post)) {
			$app->redirect($this->form_url, JText::_("DB_SAVE_FAIL"));
		}

		$continueWithError = $app->input->get('continueWithError', TRUE);
		if (!$continueWithError) {
			InsureDIYLifeHelper::clearSessionData();
			$app->redirect("index.php");
		}
		$app->redirect(JRoute::_($this->form_url.'&step=3'));
	}

	public function step3save() {
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$post = $app->input->get('jform', '', 'array');
		if (!$model->step3save($post)) {
			$app->redirect($this->form_url, JText::_("DB_SAVE_FAIL"));
		}
		$app->redirect(JRoute::_($this->form_url.'&step=4'));
	}

	public function step4save() {
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$post = $app->input->get('jform', '', 'array');
		$quotation_id = InsureDIYLifeHelper::getQid();
		$referred_by = $post['referred_by'];

		if (!InsureDIYHelper::checkReferral($referred_by)) { // referral is valid
			unset($post['referred_by']);
		}

		if (!$model->step4save($post)) {
			$app->redirect(JRoute::_($this->form_url.'&step=4'), JText::_("DB_SAVE_FAIL"));
		}
		
		// check whether the sum-insured is more than 5M
		$step = 0;
		if ($model->checkSumInsuredQuotationTotal($quotation_id) >= InsureDIYLifeHelper::getReachAmount()) {
			// go to 5th Step : Extra Info Page for more than 5M sum insured
			$step = 5;
			$model->updateStage($quotation_id, $step);
		} else {
			// go to 6th Step : Confirmation Page
			$step = 6;
			$model->updateStage($quotation_id, $step);
		}
		$app->redirect(JRoute::_($this->form_url.'&step='.$step));
	}

	public function extraInfoSave() {
		$app = JFactory::getApplication();
		$post = $app->input->get('jform', '', 'array');
		$files = $app->input->files->get('jform', null);
		$quotation_id = InsureDIYLifeHelper::getQid();

		// save quotation must perform here.
		$model = $this->getModel();
		if (!$model->extraInfoSave($post)) {
			$app->redirect(JRoute::_($this->form_url.'&step=5'), JText::_("DB_SAVE_FAIL"));
		}

		$path = 'media/com_insurediylife/documents';
		$row1 = $model->savePhotoIdentityFileUpload($files['file_company_financial1'], $path, 'file_company_financial1');
		$row2 = $model->savePhotoIdentityFileUpload($files['file_company_financial2'], $path, 'file_company_financial2');
		$row3 = $model->savePhotoIdentityFileUpload($files['file_company_financial3'], $path, 'file_company_financial3');
		$row4 = $model->savePhotoIdentityFileUpload($files['file_income_tax'], $path, 'file_income_tax');
		$model->updateStage($quotation_id, 6);
		$app->redirect(JRoute::_($this->form_url.'&step=6'));
	}

	public function saveBeforePayment() {
		$app = JFactory::getApplication();
		$post = $app->input->post->getArray();
		$model = $this->getModel();
		echo json_encode($model->saveConfirmationDetail($post));
		exit;
	}

	public function saveConfirmationDetail() {
		$app = JFactory::getApplication();
		$post = $app->input->post->getArray();
		$model = $this->getModel();
		if (!$model->saveConfirmationDetail($post)) {
			$app->redirect($this->form_url, JText::_("DB_SAVE_FAIL"));
		}
		$app->redirect(JRoute::_($this->form_url.'&step=6'));
	}

	public function step6save() {
		$app = JFactory::getApplication();
		$quotation_id = InsureDIYLifeHelper::getQid();
		$model = $this->getModel();

		$post = $app->input->get('jform', '', 'array');
		$post['id'] = $quotation_id;

		$this->saveFileUpload(FALSE);
		if ($quotation_id && $model->step6save($post)) { // Check for save and quotation ID
			$model->createPolicyRecord($quotation_id); // TODO: create policy records
			$model->createPoints($quotation_id); // create points for referrer and purchaser

			$quotation = InsureDIYLifeHelper::getQuotation($quotation_id);
			$hasLargeSum = $model->checkSumInsuredQuotationTotal($quotation_id) >= 5000000;

			$plans = $quotation['selected_plans'];

			JLoader::import('joomla.filesystem.folder');
			JLoader::import('joomla.filesystem.file');
			$userPath = MyHelper::getDeepPath(LIFE_PDF_SAVE_PATH, $quotation['user_id']);

			$pdfarrs = array();
			$pdfarrs2 = array();
			$pdfData = InsureDIYLifeHelper::getPdfData($quotation_id);
			foreach ($plans as $plan) {
				$forms = InsureDIYHelper::getCompanyForms($plan->insurer_code);
				if (!$forms['life_generate_pdf']) {
					continue;
				}
				$gOrigin = (isset($forms['life_generic']) && strlen($forms['life_generic'])) ? $forms['life_generic'] : "";
				if (JFile::exists($gOrigin)) {
					$filename = InsureDIYHelper::generateFileName($forms['life_generic']);
					$dest = $userPath . DS . $filename;

					$result = InsureDIYHelper::generatePdfForm($gOrigin, $dest, $pdfData);
					$pdfarrs[$plan->plan_index_code] = $result;
					$pdfarrs2[$plan->plan_index_code] = $result;
				}

				if (!$hasLargeSum) {
					continue;
				}
				$lsOrigin = (isset($forms['life_large_sum']) && strlen($forms['life_large_sum'])) ? $forms['life_large_sum'] : "";
				if (JFile::exists($lsOrigin)) {
					$filename = InsureDIYHelper::generateFileName($forms['life_large_sum']);
					$dest = $userPath . DS . $filename;

					$result = InsureDIYHelper::generatePdfForm($lsOrigin, $dest, $pdfData);
					$pdfarrs[$plan->plan_index_code] = $result;
					$pdfarrs2[$plan->plan_index_code] = $result;
				}

				$exOrigin = (isset($forms['life_extra_info']) && strlen($forms['life_extra_info'])) ? $forms['life_extra_info'] : "";
				if (JFile::exists($exOrigin)) {
					$filename = InsureDIYHelper::generateFileName($forms['life_extra_info']);
					$dest = $userPath . DS . $filename;

					$result = InsureDIYHelper::generatePdfForm($exOrigin, $dest, $pdfData);
					$pdfarrs[$plan->plan_index_code] = $result;
					$pdfarrs2[$plan->plan_index_code] = $result;
				}
			}

			$model->savePDFPaths($quotation_id, $pdfarrs);
			$model->savePDFPaths($quotation_id, $pdfarrs2, FALSE);
			$model->sendEmails($pdfarrs);
			$model->sendEmails($pdfarrs2, FALSE);
		} else {
//			$app->redirect($this->getRedirectUrl('thankyou'), JText::_("ERR_PDF_GENERATION_FAILED"));
		}
		InsureDIYLifeHelper::clearSessionData(); // woot~ all done. clear the session
		$app->redirect(JRoute::_($this->getRedirectUrl('thankyou')));
	}

	public function updateLifeCoverOptions() {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$app = JFactory::getApplication();
		$model = $this->getModel();
		$post = $app->input->get('jform', '', 'array');
		$quotation_id = InsureDIYLifeHelper::getQid();
		$data = array("cover_length" => $post['cover_length'], "cover_amt" => $post['cover_amt']);
		$model->updateQuotation($quotation_id, $data);
		$app->redirect(JRoute::_($this->form_url.'&step=3'));
	}

	public function saveFileUpload($redirect = TRUE) {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$app = JFactory::getApplication();
		$files = $app->input->files->get('jform', null);
		if ($files['file_identity_card']['error'] > 0 && $files['file_birth_cert']['error'] > 0 && $files['file_proof_address']['error'] > 0) {
			$msg = 'No File Uploaded';
		} else {
			$model = $this->getModel();
			$path = 'media/com_insurediylife/documents';
			$row1 = $model->savePhotoIdentityFileUpload($files['file_identity_card'], $path, 'file_identity_card');
			$row3 = $model->savePhotoIdentityFileUpload($files['file_proof_address'], $path, 'file_proof_address');
		}
		if ($redirect) {
			$app->redirect(JRoute::_($this->form_url.'&step=7'), $msg);
		}
	}

	public function deleteDocuments() {
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$app = JFactory::getApplication();
		$db = JFactory::getDBO();
		$quotation_id = InsureDIYLifeHelper::getQid();
		$file_type = JRequest::getVar('file_type');
		$query = " SELECT " . $file_type . " FROM #__insure_life_quotations WHERE id = '$quotation_id' LIMIT 1 ";
		$file_entry = $db->setQuery($query)->loadResult();
		$file_entry = explode("|", $file_entry);
		$filename = (isset($file_entry[0])) ? $file_entry[0] : FALSE;
		if ($filename) {
			$query = " UPDATE #__insure_life_quotations SET " . $file_type . " = '' WHERE id = '$quotation_id' ";
			$db->setQuery($query);
			$db->query();
			unlink(JPATH_BASE . '/media/com_insurediylife/documents/' . $filename);
			$oriname = (isset($file_entry[1])) ? $file_entry[1] : $filename;
			$msg = $oriname . ' has been deleted ';
		} else {
			$msg = 'Error deleting file.';
		}
		$app->redirect(JRoute::_($this->form_url.'&step=7'), $msg);
	}

	public function step2RefreshInfo() {
		$app = JFactory::getApplication();
		$session = JFactory::getSession();
		$quotation_id = InsureDIYLifeHelper::getQid();
		$model = $this->getModel();
		$user = $model->getCurrentUser();
		$data = array();
		$data['gender'] = $user->gender;
		$data['monthly_income'] = $user->monthly_salary;
		$data['occupation'] = $user->occupation;
		if ($user->dob != '0000-00-00') {
			$data['dob'] = $user->dob;
		}
		$data['country_residence'] = $user->country;
		$data['marital_status'] = $user->marital_status;
		if ($model->updateQuotation($quotation_id, $data)) {
			$session->set("life.step1.ask4override", NULL);
		}
		$app->redirect(JRoute::_($this->form_url.'&step=2'));
	}

	public function checkOverrides() {
		$session = JFactory::getSession();
		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		$quotation_id = InsureDIYLifeHelper::getQid();
		if ($quotation_id && $user->id > 0) {
			$data = $session->get('life.data');
			$model = $this->getModel();
			$userData = $model->getCurrentUser();
			$same = TRUE;
			if (isset($data['gender']) && isset($userData->gender) && strlen($userData->gender) > 0 && $data['gender'] != $userData->gender) {
				$same = FALSE;
			}
			if ($same && isset($data['dob']) && isset($userData->dob) && strtotime($userData->dob) > 0 && strtotime($data['dob']) != strtotime($userData->dob)) {
				$same = FALSE;
			}
			if ($same && isset($data['marital_status']) && isset($userData->marital_status) && strlen($userData->marital_status) > 0 && $data['marital_status'] != $userData->marital_status) {
				$same = FALSE;
			}
			if ($same && isset($data['monthly_salary']) && isset($userData->monthly_income) && strlen($userData->monthly_income) > 0 && $data['monthly_salary'] != $userData->monthly_income) {
				$same = FALSE;
			}
			if ($same && isset($data['country_residence']) && isset($userData->country) && strlen($userData->country) > 0 && $data['country_residence'] != $userData->country) {
				$same = FALSE;
			}
			if ($same && isset($data['occupation']) && isset($userData->occupation) && strlen($userData->occupation) > 0 && $data['occupation'] != $userData->occupation) {
				$same = FALSE;
			}
			$session->set("life.step1.ask4override", $same);
		}
		$app->redirect(JRoute::_($this->form_url.'&step=2'));
	}

	public function getRedirectUrl($layout) {
		$url = $this->base_layout_url . $layout;
		return $url;
	}

	public function back() {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$app = JFactory::getApplication();
		$quotation_id = $app->input->post->get("quotation_id", FALSE, "integer");
		$backstage = 0;
		if ($quotation_id) {
			$model = $this->getModel();
			$backstage = $model->back($quotation_id);
		}
		$app->redirect(JRoute::_($this->form_url.'&step='.$backstage));
	}

}
