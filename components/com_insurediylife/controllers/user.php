<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_insurediylife
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * @package     Joomla.Site
 * @subpackage  com_insurediylife
 * @since       1.5
 */
class InsureDIYLifeControllerUser extends JControllerForm {

	/**
	 * @since   1.6
	 */
	protected $view_item = 'form';

	/**
	 * @since   1.6
	 */
	protected $view_list = 'categories';

	/**
	 * The URL edit variable.
	 *
	 * @var    string
	 * @since  3.2
	 */
	protected $urlVar = 'a.id';

	/**
	 * Method to add a new record.
	 *
	 * @return  boolean  True if the article can be added, false if not.
	 * @since   1.6
	 */
	public function add() {

		if (!parent::add()) {
			// Redirect to the return page.
			$this->setRedirect($this->getReturnPage());
		}
	}

	/**
	 * Method override to check if you can add a new record.
	 *
	 * @param   array  $data	An array of input data.
	 * @return  boolean
	 * @since   1.6
	 */
	protected function allowAdd($data = array()) {
		$user = JFactory::getUser();
		$categoryId = JArrayHelper::getValue($data, 'catid', $this->input->getInt('id'), 'int');
		$allow = null;

		if ($categoryId) {
			// If the category has been passed in the URL check it.
			$allow = $user->authorise('core.create', $this->option . '.category.' . $categoryId);
		}

		if ($allow === null) {
			// In the absense of better information, revert to the component permissions.
			return parent::allowAdd($data);
		} else {
			return $allow;
			}
	}

	/**
	 * Method to check if you can add a new record.
	 *
	 * @param   array  $data	An array of input data.
	 * @param   string	$key	The name of the key for the primary key.
	 *
	 * @return  boolean
	 * @since   1.6
	 */
	protected function allowEdit($data = array(), $key = 'id') {
		$recordId = (int) isset($data[$key]) ? $data[$key] : 0;
		$categoryId = 0;

		if ($recordId) {
			$categoryId = (int) $this->getModel()->getItem($recordId)->catid;
		}

		if ($categoryId) {
			// The category has been set. Check the category permissions.
			return JFactory::getUser()->authorise('core.edit', $this->option . '.category.' . $categoryId);
		} else {
			// Since there is no asset tracking, revert to the component permissions.
			return parent::allowEdit($data, $key);
		}
	}

	/**
	 * Method to cancel an edit.
	 *
	 * @param   string	$key	The name of the primary key of the URL variable.
	 *
	 * @return  Boolean	True if access level checks pass, false otherwise.
	 * @since   1.6
	 */
	public function cancel($key = 'w_id') {
		parent::cancel($key);

		// Redirect to the return page.
		$this->setRedirect($this->getReturnPage());
	}

	/**
	 * Method to edit an existing record.
	 *
	 * @param   string	$key	The name of the primary key of the URL variable.
	 * @param   string	$urlVar	The name of the URL variable if different from the primary key (sometimes required to avoid router collisions).
	 *
	 * @return  Boolean	True if access level check and checkout passes, false otherwise.
	 * @since   1.6
	 */
	public function edit($key = null, $urlVar = 'w_id') {
		$result = parent::edit($key, $urlVar);

		return $result;
	}

	/**
	 * Method to get a model object, loading it if required.
	 *
	 * @param   string	$name	The model name. Optional.
	 * @param   string	$prefix	The class prefix. Optional.
	 * @param   array  $config	Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 * @since   1.5
	 */
	public function getModel($name = 'form', $prefix = '', $config = array('ignore_request' => true)) {
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	/**
	 * Gets the URL arguments to append to an item redirect.
	 *
	 * @param   integer  $recordId	The primary key id for the item.
	 * @param   string	$urlVar		The name of the URL variable for the id.
	 *
	 * @return  string	The arguments to append to the redirect URL.
	 * @since   1.6
	 */
	protected function getRedirectToItemAppend($recordId = null, $urlVar = null) {
		$append = parent::getRedirectToItemAppend($recordId, $urlVar);
		$itemId = $this->input->getInt('Itemid');
		$return = $this->getReturnPage();

		if ($itemId) {
			$append .= '&Itemid=' . $itemId;
		}

		if ($return) {
			$append .= '&return=' . base64_encode($return);
		}

		return $append;
	}

	/**
	 * Get the return URL.
	 *
	 * If a "return" variable has been passed in the request
	 *
	 * @return  string	The return URL.
	 * @since   1.6
	 */
	protected function getReturnPage() {
		$return = $this->input->get('return', null, 'base64');

		if (empty($return) || !JUri::isInternal(base64_decode($return))) {
			return JUri::base();
		} else {
			return base64_decode($return);
		}
	}

	/**
	 * Function that allows child controller access to model data after the data has been saved.
	 *
	 * @param   JModelLegacy  $model      The data model object.
	 * @param   array         $validData  The validated data.
	 *
	 * @return  void
	 * @since   1.6
	 */
	protected function postSaveHook(JModelLegacy $model, $validData = array()) {
		return;
	}

	/**
	 * Method to save a record.
	 *
	 * @param   string	$key	The name of the primary key of the URL variable.
	 * @param   string	$urlVar	The name of the URL variable if different from the primary key (sometimes required to avoid router collisions).
	 *
	 * @return  Boolean	True if successful, false otherwise.
	 * @since   1.6
	 */
	public function save($key = null, $urlVar = 'w_id') {
		$result = parent::save($key, $urlVar);

		// If ok, redirect to the return page.
		if ($result) {
			$this->setRedirect($this->getReturnPage());
		}

		return $result;
	}

	public function login() {
		JSession::checkToken('post') or jexit(JText::_('JInvalid_Token'));
		$user = JFactory::getUser();
		$app = JFactory::getApplication();
		if ($user->id > 0) {
			$app->redirect(JRoute::_("index.php?option=com_insurediylife&view=form&layout=login", FALSE), "You are already logged in.");
		}
		$data = array();
		$data['return'] = "index.php?option=com_insurediylife&step=sign_in";
		$data['username'] = $this->input->getVar('username', '', 'method', 'username');
		$data['password'] = $this->input->getString('password', '', 'post', 2);
		$data['secretkey'] = JRequest::getString('secretkey', '');
		$app->setUserState('users.login.form.return', $data['return']);

		// Get the log in options.
		$options = array();
		$options['remember'] = $this->input->getBool('remember', false);
		$options['return'] = $data['return'];

		// Get the log in credentials.
		$credentials = array();
		$credentials['username'] = $data['username'];
		$credentials['password'] = $data['password'];
		$credentials['secretkey'] = $data['secretkey'];

		// Perform the log in.
		if (true === $app->login($credentials, $options)) {
			$app->setUserState('users.login.form.data', array());
			$result = $this->updateQuotation();
			if ($result) {
				$app->redirect(JRoute::_("index.php?option=com_insurediylife&task=form.checkOverrides", false));
			} else {
				$app->redirect(JRoute::_($app->getUserState('users.login.form.return'), false));
			}
		} else {
			// Login failed !
			$data['remember'] = (int) $options['remember'];
			$app->setUserState('users.login.form.data', $data);
			$app->redirect(JRoute::_($app->getUserState('users.login.form.return'), false));
		}
	}

	private function updateQuotation() {
		$user = JFactory::getUser();
		$app = JFactory::getApplication();
		if (!$user->id > 0) {
			return FALSE;
		}

		$quotation_id = InsureDIYLifeHelper::getCurrentQid();
		if (!$quotation_id) {
			$app->redirect(JRoute::_("index.php?option=com_insurediylife&view=form", false));
		}

		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$query->update("#__insure_life_quotations");
		$query->set("quote_stage = 2");
		$query->set("user_id=" . $db->quote($user->id));
		$query->set("email=" . $db->quote($user->email));
		$query->set("unique_order_no=" . InsureDIYHelper::getUniqueOrderNo($user->id));
		$query->where("id=" . $db->quote($quotation_id));
		$db->setQuery($query);
		$result = $db->execute();
		if ($result) {
			return TRUE;
		}
		$app->redirect(JRoute::_("index.php?option=com_insurediylife&view=form", false), "Database Error. Please contact to admin.");
	}

	public function register() {
		JSession::checkToken('post') or jexit(JText::_('JInvalid_Token'));
		$user = JFactory::getUser();
		$app = JFactory::getApplication();
		if ($user->id > 0) {
			$app->redirect(JRoute::_("index.php?option=com_insurediylife&view=form&layout=login", FALSE), "You are already logged in.");
		}
		$returnUrl = "index.php?option=com_insurediylife&step=sign_in";
		$successUrl = "index.php?option=com_insurediylife&step=2";

		$data['email'] = $this->input->getVar('email', '', 'method', 'email');
		$data['password'] = $this->input->getString('password', '', 'post', 2);
		$data['password2'] = $this->input->getString('password2', '', 'post', 2);

		$uparams = JComponentHelper::getParams("com_users");
		$min_length = $uparams->get("minimum_length");
		if (strlen($data['password']) < $min_length) {
			$app->redirect(JRoute::_($returnUrl, FALSE), JText::sprintf("ERR_PASSWORD_TOO_SHORT", $min_length));
		}

		if ($data['password'] != $data['password2']) {
			$app->redirect(JRoute::_($returnUrl, FALSE), JText::_("ERR_PASSWORDS_NOT_THE_SAME"));
		}

		$result = $this->createUser($data);
		if (!$result) {
			$app->redirect(JRoute::_($returnUrl, FALSE));
		}
		$credentials = array("username" => $data['email'], "password" => $data['password']);
		$app->login($credentials);
		$result2 = $this->updateQuotation();
		if ($result2) {
			$conf = JFactory::getConfig();
			$from = $conf->get("mailfrom");
			$fromName = $conf->get("fromname");
			
			// 1. iFoundries changed : Welcome
			$emailSubject = JText::sprintf(
				'Welcome to InsureDIY! – Your Account Details'
			);
			$data['user_email'] = $data['email'];
			$emailBody = InsureDIYHelper::replaceVariables($uparams->get("ugformat"), $data);
			JFactory::getMailer()->sendMail($from, $fromName, $data['email'], $emailSubject, $emailBody, TRUE);
			
			// 2. iFoundries changed : Send Password
			$emailSubject = JText::sprintf(
				'InsureDIY – Your Account Password'
			);
			
			$data['user_email'] = $data['email'];
			$emailBody = InsureDIYHelper::replaceVariables($uparams->get("ugformat_password"), $data);
			JFactory::getMailer()->sendMail($from, $fromName, $data['email'], $emailSubject, $emailBody, TRUE);
			
			
			$saveResult = $this->saveUserData();
			
			$app->redirect(JRoute::_($successUrl, false), "Your account has been successfully created.");
		} else {
			$app->redirect(JRoute::_($app->getUserState('users.login.form.return'), false));
		}
	}

	private function saveUserData() {
		$session = JFactory::getSession();
		$userData = $session->get('step1.post.data', FALSE);
		if (is_array($userData) && count($userData) > 0) {
			$user = JFactory::getUser();
			$db = JFactory::getDbo();
			$query = $db->getQuery(TRUE);
			$query->update("#__users");
			if (isset($userData['gender'])) {
				$query->set("gender='" . $userData['gender'] . "'");
			}
			if (isset($userData['occupation'])) {
				$query->set("occupation='" . $userData['occupation'] . "'");
			}
			if (isset($userData['monthly_income'])) {
				$query->set("monthly_salary='" . $userData['monthly_income'] . "'");
			}
			if (isset($userData['country_residence'])) {
				$query->set("country='" . $userData['country_residence'] . "'");
			}
			if (isset($userData['marital_status'])) {
				$query->set("marital_status='" . $userData['marital_status'] . "'");
			}
			$query->where("id=" . $db->quote($user->id));
			$db->setQuery($query);
			$result = $db->execute();
			$session->set('step1.post.data', NULL);
			return $result;
		}
		return FALSE;
	}

	private function createUser($param) {
		$app = JFactory::getApplication();
		$name = $param['email'];
		$salt = JUserHelper::genRandomPassword(32);
		$crypt = JUserHelper::getCryptedPassword($param['password'], $salt);
		$password = $crypt . ':' . $salt;

		$user = new JUser();
		$user->groups = array(2); //add to registered group
		$user->email = $param["email"];
		$user->username = $name;
		$user->name = $name;
		$user->password = $password;
		$user->is_firsttime = 1; /* for register inside quotation only , set is_firsttime = 1 */

		if (!$user->save()) {
			echo "ya";
			$app->enqueueMessage($user->getError());
			return FALSE;
		}

		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$query->update("#__users");
		$query->set("name=''");
		$query->where("id=" . $db->quote($user->id));
		$db->setQuery($query);
		$db->execute();

		return $user;
	}

}
