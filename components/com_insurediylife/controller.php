<?php

defined('_JEXEC') or die;

class InsureDIYLifeController extends JControllerLegacy {

	protected $default_view = 'form';

	public function display() {
		$this->input->set('view', $this->default_view);
		return parent::display(false);
	}

}
