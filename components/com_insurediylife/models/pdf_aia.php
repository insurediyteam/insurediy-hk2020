<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_insurediylife
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

//JTable::addIncludePath(JPATH_COMPONENT_ADMINISTRATOR . '/tables');

/**
 *  Component Model for a Weblink record
 *
 * @package     Joomla.Site
 * @subpackage  com_insurediylife
 * @since       1.5
 */
class InsureDIYLifeModelPDF_AIA extends JModelForm {

	/**
	 * Model context string.
	 *
	 * @access	protected
	 * @var		string
	 */
	protected $_context = 'com_insurediylife.pdf_aia';

	public function getReturnPage() {
		// No Return Page
		return;
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since   1.6
	 */
	protected function populateState() {
		// No State 
		return;
	}

	/**
	 * Method to get an object.
	 *
	 * @param   integer	The id of the object to get.
	 *
	 * @return  mixed  Object on success, false on failure.
	 */
	public function getItem($id = null) {
		// nothing to do
	}

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param	type	The table type to instantiate
	 * @param	string	A prefix for the table class name. Optional.
	 * @param	array	Configuration array for model. Optional.
	 * @return	JTable	A database object
	 * @since	1.6
	 */
	public function getTable($type = '', $prefix = 'InsureDIYLifeTable', $config = array()) {
		// nothing to do
		return;
	}

	/**
	 * Method to get the profile form.
	 *
	 * The base form is loaded from XML and then an event is fired
	 * for users plugins to extend the form with extra fields.
	 *
	 * @param   array  $data		An optional array of data for the form to interogate.
	 * @param   boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return  JForm	A JForm object on success, false on failure
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true) {
		// No Form
		return;
	}

	/**
	 * Generate PDF File
	 *
	 * @param   N/A
	 *
	 * @return  Integer Total Sum Insured
	 */
	public function generate($quotation_id = FALSE) {

		$session = JFactory::getSession();
		$db = JFactory::getDBO();
		if (!$quotation_id || !is_int($quotation_id)) {
			$quotation_id = $session->get('quotation_id', JRequest::getVar('quotation_id'));
		}

		$query = " SELECT * FROM #__insure_life_quotations WHERE id = '$quotation_id' ";
		$db->setQuery($query);
		$data = $db->loadObject();

		$query = " SELECT * FROM #__insure_life_quotation_properties WHERE quotation_id = '$quotation_id' LIMIT 3 ";
		$db->setQuery($query);
		$properties = $db->loadObjectList();

		$query = " SELECT * FROM #__insure_life_quotation_cars WHERE quotation_id = '$quotation_id' LIMIT 3 ";
		$db->setQuery($query);
		$cars = $db->loadObjectList();


		$application = array();
		$application = array('AIA-ApplicationV2.pdf', 'AIA-Direct_Debit.pdf', 'AIA-Large_Amount_Questionnaire.pdf');

		$user = JFactory::getUser();

		// initiate FPDI
		$pdf = new FPDI();

		$path = 'images/insurediyform/form/aia/';

		/* Prepare Application Form */
		$pageCount = $pdf->setSourceFile($path . $application[0]);

		for ($n = 1; $n <= $pageCount; $n++) {

			// add a page
			$pdf->AddPage();

			// import page 1
			$tplIdx = $pdf->importPage($n, '/TrimBox');

			// use the imported page and place it at point 10,10 with a width of 100 mm
			$pdf->useTemplate($tplIdx, 0, 0);

			// now write some text above the imported page
			$pdf->SetFont('Helvetica');
			$pdf->SetFontSize(12); // normal is 12pt
			$pdf->SetTextColor(0, 0, 0);

			switch ($n) :
				case 1:
					$pdf->SetXY(73, 157);
					$pdf->Write(0, $data->contact_firstname);

					$pdf->SetXY(73, 164);
					$pdf->Write(0, $data->contact_lastname);

					if (strtolower($data->gender) == 'm') :
						$pdf->Image("images/ico-checked-black.png", 80, 187);
					endif;

					if (strtolower($data->gender) == 'f') :
						$pdf->Image("images/ico-checked-black.png", 105, 187);
					endif;

					switch ($data->contact_identity_type) :
						case 'hkid' :
							$pdf->Image("images/ico-checked-black.png", 73, 193);

							$pdf->SetXY(77, 206);
							$pdf->Write(0, $data->contact_identity_no);

							break;
						case 'chid' :
							$pdf->Image("images/ico-checked-black.png", 72, 220);

							$pdf->SetXY(105, 220);
							$pdf->Write(0, $data->contact_identity_no);

							if ($data->contact_expiry_date != '0000-00-00') {

								$tmp = explode('-', $data->contact_expiry_date);

								$year = $tmp[0];
								$month = $tmp[1];
								$day = $tmp[2];

								$pdf->SetXY(103, 230);
								$pdf->Write(0, $day);

								$pdf->SetXY(113, 230);
								$pdf->Write(0, $month);

								$pdf->SetXY(123, 230);
								$pdf->Write(0, $year);
							}
							break;
						case 'passport':

							$pdf->Image("images/ico-checked-black.png", 72, 239);

							$pdf->SetXY(105, 241);
							$pdf->Write(0, $data->contact_identity_no);

							if ($data->contact_expiry_date != '0000-00-00') {

								$tmp = explode('-', $data->contact_expiry_date);

								$year = $tmp[0];
								$month = $tmp[1];
								$day = $tmp[2];

								$pdf->SetXY(103, 250);
								$pdf->Write(0, $day);

								$pdf->SetXY(113, 250);
								$pdf->Write(0, $month);

								$pdf->SetXY(123, 250);
								$pdf->Write(0, $year);
							}

							break;
					endswitch;

					// nationality
					$pdf->SetXY(73, 266);
					$pdf->Write(0, InsureDIYHelper::getNationality($data->nationality));

					break;
				case 2:
					$dob = explode("-", $data->dob);

					$pdf->SetFontSize(8); // normal is 12pt

					$pdf->SetXY(75, 20);
					$pdf->Write(0, $dob[1]); // mm

					$pdf->SetXY(88, 20);
					$pdf->Write(0, $dob[2]); // dd

					$pdf->SetXY(101, 20);
					$pdf->Write(0, $dob[0]); // year

					$age = (int) date("Y") - (int) $dob[0];

					$pdf->SetXY(116, 25);
					$pdf->Write(0, $age);

					switch ($data->marital_status) :
						case 'M' : $pdf->Image("images/ico-checked-black.png", 82, 31);
							break;
						case 'S' : $pdf->Image("images/ico-checked-black.png", 107, 31);
							break;
						default: break;
					endswitch;

					$pdf->SetXY(85, 65);
					$pdf->Write(0, $data->contact_room_no);

					$pdf->SetXY(105, 65);
					$pdf->Write(0, $data->contact_floor_no);

					$pdf->SetXY(125, 65);
					$pdf->Write(0, $data->contact_block_no);

					$pdf->SetXY(75, 80);
					$pdf->Write(0, $data->contact_building_name);

					$pdf->SetXY(75, 95);
					$pdf->Write(0, $data->contact_street_name);

					$pdf->SetXY(100, 105);
					$pdf->Write(0, $data->contact_district_name);

					$pdf->SetXY(136, 113);
					$pdf->Write(0, $user->email);

					$pdf->SetXY(142, 78);
					$pdf->Write(0, (($data->contact_country_code) ? '(' . $data->contact_country_code . ') ' : '') . $data->contact_contact_no);

					$pdf->SetXY(75, 242);
					$pdf->Write(0, $data->occupation);

					$pdf->SetXY(75, 260);
					$pdf->Write(0, $data->job_nature);

					break;
				case 3:
					switch ($data->monthly_income) {
						case "JGLOBAL_MONTHLY_INCOME_2000_3000":
						case "JGLOBAL_MONTHLY_INCOME_3001_4000":
							$pdf->Image("images/ico-checked-black.png", 16.5, 37.6);
							break;
						case "JGLOBAL_MONTHLY_INCOME_4001_5000":
						case "JGLOBAL_MONTHLY_INCOME_5001_6000":
							$pdf->Image("images/ico-checked-black.png", 16.5, 42.6);
							break;
						default:
							break;
					}
					$pdf->Image("images/ico-checked-black.png", 83, 60); // currency

					break;
				case 4:
					break;
				case 5:
					break;
				case 6:

					$pdf->SetFontSize(8); // normal is 12pt

					$pdf->SetXY(15, 33);
					$pdf->Write(0, $data->height_amt);

					$pdf->Image("images/ico-checked-black.png", 54, 33); // cm

					$pdf->SetXY(65, 33);
					$pdf->Write(0, $data->weight_amt);

					$pdf->Image("images/ico-checked-black.png", 101, 33); //kg

					$pdf->SetXY(60, 53);
					$pdf->Write(0, $data->physician_name);

					$pdf->SetXY(60, 61);
					$pdf->Write(0, $data->physician_address);

					$pdf->SetXY(60, 80);
					$pdf->Write(0, $data->physician_consult_reason);

					$pdf->SetXY(60, 87);
					$pdf->Write(0, $data->physician_consult_result);

					// weight change excess
					$pdf->SetXY(37, 103);
					$pdf->Write(0, $data->contact_firstname . ' ' . $data->contact_lastname);

					$pdf->Image("images/ico-checked-black.png", 173, 103);


					break;
				case 7:
					break;
				case 8:

					// need to do : if yes or no, currently is only fix to no..

					switch ($data->declare_have_you_replaced) :
						case 1 : $pdf->Image("images/ico-checked-black.png", 29, 51); // do you intend to replace
							break;
						case 0 : $pdf->Image("images/ico-checked-black.png", 29, 59); // have you replaced
							break;
					endswitch;


					switch ($data->declare_do_you_intend_to_replace) :
						case 1 : $pdf->Image("images/ico-checked-black.png", 29, 96); // do you intend to replace
							break;
						case 0 : $pdf->Image("images/ico-checked-black.png", 29, 104); // do you intend to replace
							break;
					endswitch;

					break;
				case 9:
					break;
				case 10:
					break;
				case 11:
					break;
				case 12:
					break;
				case 13:
					break;
			endswitch;
		}



		/* Direct Debit Form */
		$pageCount = $pdf->setSourceFile($path . $application[1]);

		for ($n = 1; $n <= $pageCount; $n++) {

			// add a page
			$pdf->AddPage();

			// import page 1
			$tplIdx = $pdf->importPage($n, '/TrimBox');

			// use the imported page and place it at point 10,10 with a width of 100 mm
			$pdf->useTemplate($tplIdx, 0, 0, 210);

			// now write some text above the imported page
			$pdf->SetFont('Helvetica');
			$pdf->SetFontSize(10); // normal is 12pt
			$pdf->SetTextColor(0, 0, 0);

			switch ($n) :
				case 1:
					$pdf->SetXY(55, 107);
					$pdf->Write(0, $data->bank_acct_holder_name);

					$pdf->SetXY(170, 107);
					$pdf->Write(0, $data->bank_id_no);

					$pdf->SetFontSize(13); // normal is 12pt

					$pdf->SetXY(109, 190);
					$pdf->Write(0, chunk_split($data->bank_branch_no, 1, '  '));

					$pdf->SetXY(129, 190);
					$pdf->Write(0, chunk_split($data->bank_branch_no, 1, '  '));

					$pdf->SetXY(150, 190);
					$pdf->Write(0, chunk_split($data->bank_acct_no, 1, '  '));

					$pdf->SetXY(15, 205);
					$pdf->Write(0, chunk_split($data->contact_firstname, 1, '  ') . '  /  ' . chunk_split($data->contact_lastname, 1, '  '));

					$pdf->SetXY(21, 249);
					$pdf->Write(0, chunk_split($data->bank_id_no, 1, '  '));

					switch ($data->bank_id_type) :
						case 'hkid' : $pdf->Image("images/ico-checked-black.png", 142, 245);
							break;
						case 'passport' : $pdf->Image("images/ico-checked-black.png", 152, 245);
							break;
					endswitch;

					break;
			endswitch;
		}




		/* Prepare Questionnaire Form */
		$pageCount = $pdf->setSourceFile($path . $application[2]);

		for ($n = 1; $n <= $pageCount; $n++) {

			// add a page
			$pdf->AddPage();

			// import page 1
			$tplIdx = $pdf->importPage($n, '/TrimBox');

			// use the imported page and place it at point 10,10 with a width of 100 mm
			$pdf->useTemplate($tplIdx, 0, 0, 210);

			// now write some text above the imported page
			$pdf->SetFont('Helvetica');
			$pdf->SetFontSize(10); // normal is 12pt
			$pdf->SetTextColor(0, 0, 0);

			switch ($n) :
				case 1:

					$pdf->SetXY(55, 57);
					$pdf->Write(0, $data->contact_firstname . ' ' . $data->contact_lastname);

					$pdf->SetXY(120, 57);
					$pdf->Write(0, $data->contact_identity_no);

					$pdf->SetXY(15, 77);
					$pdf->Write(0, $data->contact_country_code);

					$year = (int) date("Y", strtotime($data->created_date));
					$lastyear = $year - 1;
					$last2year = $year - 2;
					$last3year = $year - 3;

					$pdf->SetXY(78, 190);
					$pdf->Write(0, $lastyear);

					$pdf->SetXY(128, 190);
					$pdf->Write(0, $last2year);

					$pdf->SetXY(178, 190);
					$pdf->Write(0, $last3year);

					$pdf->SetXY(60, 195);
					$pdf->Write(0, $data->other_total_income_last_year);

					$pdf->SetXY(111, 195);
					$pdf->Write(0, $data->other_total_income_year_before);

					$pdf->SetXY(162, 195);
					$pdf->Write(0, $data->other_total_income_two_years);

					/* Property Value */
					foreach ($properties as $key => $value) :

						switch ($key) {

							case 0:

								$pdf->SetXY(55, 263);
								$pdf->Write(0, $value->property_purchase);

								$pdf->SetXY(55, 269);
								$pdf->Write(0, $value->property_value);

								break;

							case 1:

								$pdf->SetXY(130, 263);
								$pdf->Write(0, $value->property_purchase);

								$pdf->SetXY(130, 269);
								$pdf->Write(0, $value->property_value);

								break;
						}

					endforeach;

					break;

				case 2:

					/* Cars */
					$pdf->SetXY(60, 25);
					$pdf->Write(0, count($cars));

					foreach ($cars as $key => $value) :
						switch ($key) {

							case 0:

								$pdf->SetXY(60, 33);
								$pdf->Write(0, $value->car_model);

								break;

							case 1:

								$pdf->SetXY(111, 33);
								$pdf->Write(0, $value->car_model);

								break;

							case 2:

								$pdf->SetXY(162, 33);
								$pdf->Write(0, $value->car_model);

								break;
						}
					endforeach;

					$pdf->SetXY(100, 105);
					$pdf->Write(0, $data->other_ownership_percent);

					$pdf->SetXY(100, 113);
					$pdf->Write(0, $data->other_ownership_commencement_biz);

					$pdf->SetXY(100, 121);
					$pdf->Write(0, $data->other_ownership_employees_no);

					break;

				case 3 :

					break;
			endswitch;
		}
		if (@!mkdir("images/insurediyform/quotation/" . $data->id, 0777, true)) {
			
		}

		$path = "images/insurediyform/quotation/" . $data->id . "/AIA_Application_" . JHtml::_('date', time(), 'dMYHis') . ".pdf";
		$pdf->Output($path, 'F');

		return $path;
	}

}
