<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_banners
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('JPATH_BASE') or die;

/**
 * Impressions Field class for the Joomla Framework.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_banners
 * @since       1.6
 */
JLoader::import('incs.form.fields.customlabel', JPATH_ROOT);

class JFormFieldInsureDIYLifeGender extends JformFieldCustomLabel {

	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since   1.6
	 */
	protected $type = 'InsureDIYLifeGender';

	/**
	 * Method to get the field input markup.
	 *
	 * @return  string	The field input markup.
	 * @since   1.6
	 */
	protected function getInput() {

		$value = empty($this->value) ? '' : $this->value;
		$checked = ' checked="checked" ';
		$class = ' class="checked" ';

		$session = JFactory::getSession();
		if (strlen($this->value) > 0) {
			$this->default = $this->value;
		}

		return '<div class="insurediy-custom-radio">'
				. '<div class="insurediy-custom-radio-gender-male"><input type="radio" id="insurediy-gender-male" ' . (($this->default == 'M') ? $checked : '') . ' value="M" name="' . $this->name . '" /><label for="insurediy-gender-male" ' . (($this->default == 'M') ? $class : '') . '>Male</label></div>'
				. '<div class="insurediy-custom-radio-gender-female"><input type="radio" id="insurediy-gender-female" ' . (($this->default == 'F') ? $checked : '') . ' value="F" name="' . $this->name . '" /><label for="insurediy-gender-female" ' . (($this->default == 'F') ? $class : '') . '>Female</label></div>'
				. '<div style="clear:both"></div>'
				. '</div>';
	}

}
