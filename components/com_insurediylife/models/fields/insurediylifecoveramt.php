<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_banners
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('JPATH_BASE') or die;

/**
 * Clicks Field class for the Joomla Framework.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_banners
 * @since       1.6
 */
JLoader::import('incs.form.fields.customlabel', JPATH_ROOT);

class JFormFieldInsureDIYLifeCoverAmt extends JformFieldCustomLabel {

	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since   1.6
	 */
	protected $type = 'InsureDIYLifeCoverAmt';

	/**
	 * Method to get the field label markup.
	 *
	 * @return  string  The field label markup.
	 *
	 * @since   11.1
	 */
	protected function getLabel() {
		$label = '';
		$imghelpquote = '';

		if ($this->hidden) {
			return $label;
		}

		// Get the label text from the XML element, defaulting to the element name.
		$text = $this->element['label'] ? (string) $this->element['label'] : (string) $this->element['name'];
		$text = $this->translateLabel ? JText::_($text) : $text;

		// Build the class for the label.
		$class = !empty($this->description) ? 'hasTooltip' : '';
		$class = $this->required == true ? $class . ' required' : $class;
		$class = !empty($this->labelClass) ? $class . ' ' . $this->labelClass : $class;

		// Add the opening label tag and main attributes attributes.
		$label .= '<label id="' . $this->id . '-lbl" for="' . $this->id . '" class="' . $class . '"';
		$imghelpquote .= ' ';

		// If a description is specified, use it to build a tooltip.
		if (!empty($this->description)) {

			JHtml::_('bootstrap.tooltip');
			$imghelpquote .= '&nbsp;<img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="' . JHtml::tooltipText(trim($text, ':'), JText::_($this->description), 0) . '" />';
		}

		// Add the label text and closing tag.
		if ($this->required) {
			$label .= '>' . $text . '<span class="star">&#160;*</span>' . $imghelpquote . '</label>';
		} else {
			$label .= '>' . $text . $imghelpquote . '</label>';
		}

		return $label;
	}

	/**
	 * Method to get the field input markup.
	 *
	 * @return  string	The field input markup.
	 * @since   1.6
	 */
	protected function getInput() {
		$session = JFactory::getSession();
		$db = JFactory::getDBO();

		$quotation_id = $session->get('quotation_id', JRequest::getVar('quotation_id'));

		$query = " SELECT label_text AS value, sum_insured AS text FROM #__insure_life_sum_insured ";
		$db->setQuery($query);
		$rows = $db->loadObjectList();

		$query = " SELECT cover_amt FROM #__insure_life_quotations WHERE id = '$quotation_id'; ";
		$db->setQuery($query);
		$cover_amt = $db->loadResult();

		$current = (isset($cover_amt) ? $cover_amt : '');

		foreach ($rows as $r) :
			$options[] = JHtml::_('select.option', $r->text, $r->value, 'value', 'text');
		endforeach;

		$html = JHTML::_('select.genericlist', $options, $this->name, 'onchange="javascript:void(0);" ', 'value', 'text', $current);

		return $html;
	}

}
