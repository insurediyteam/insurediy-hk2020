<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_calculator
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Calculator helper.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_calculator
 * @since       1.6
 */
class CalculatorHelper extends JHelperContent
{
	/**
	 * Configure the Linkbar.
	 *
	 * @param   string	$vName  The name of the active view.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	public static function addSubmenu($vName = 'calculator')
	{
		JHtmlSidebar::addEntry(
			JText::_('COM_CALCULATOR_SUBMENU_CALCULATOR'),
			'index.php?option=com_calculator&view=calculator',
			$vName == 'calculator'
		);

		JHtmlSidebar::addEntry(
			JText::_('COM_CALCULATOR_SUBMENU_CATEGORIES'),
			'index.php?option=com_categories&extension=com_calculator',
			$vName == 'categories'
		);
	}

}
