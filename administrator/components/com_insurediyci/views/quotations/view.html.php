<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_CedeleDepots
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * View class for a list of CedeleDepots.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_CedeleDepots
 * @since       1.5
 */
class InsureDIYCIViewQuotations extends JViewLegacy {

	protected $items;
	protected $pagination;
	protected $state;

	/**
	 * Display the view
	 *
	 * @return  void
	 */
	public function display($tpl = null) {
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');

		InsureDIYCIHelper::addSubmenu('quotations');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar() {
		require_once JPATH_COMPONENT . '/helpers/insurediyci.php';

		$state = $this->get('State');
		$canDo = InsureDIYCIHelper::getActions($state->get('filter.category_id'));
		$user = JFactory::getUser();
		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');

		JToolbarHelper::title(JText::_('COM_INSUREDIYCI_MANAGER_QUOTATIONS'), 'insurediyci.png');

		if ($canDo->get('core.create')) {
			JToolbarHelper::addNew('quotation.add');
		}

		if ($canDo->get('core.edit')) {
			JToolbarHelper::editList('quotation.edit');
		}


		if ($canDo->get('core.edit.state')) {

			JToolbarHelper::publish('quotation.publish', 'JTOOLBAR_PUBLISH', true);
			JToolbarHelper::unpublish('quotation.unpublish', 'JTOOLBAR_UNPUBLISH', true);

			//JToolbarHelper::archiveList('quotation.archive');
			//JToolbarHelper::checkin('quotation.checkin');
		}


		if ($state->get('filter.state') == -2 && $canDo->get('core.delete')) {
			JToolbarHelper::deleteList('', 'quotation.delete', 'JTOOLBAR_EMPTY_TRASH');
		} elseif ($canDo->get('core.edit.state')) {
			JToolbarHelper::trash('quotation.trash');
		}


		// Add a batch button
		/* if ($canDo->get('core.edit'))
		  {
		  JHtml::_('bootstrap.modal', 'collapseModal');
		  $title = JText::_('JTOOLBAR_BATCH');
		  $dhtml = "<button data-toggle=\"modal\" data-target=\"#collapseModal\" class=\"btn btn-small\">
		  <i class=\"icon-checkbox-partial\" title=\"$title\"></i>
		  $title</button>";
		  $bar->appendButton('Custom', $dhtml, 'batch');
		  }
		 */
		if ($canDo->get('core.admin')) {
			JToolbarHelper::preferences('com_insurediyci');
		}

		//JToolbarHelper::help('JHELP_COMPONENTS_insurediyciS_LINKS');

		JHtmlSidebar::setAction('index.php?option=com_insurediyci&view=quotations');
		JHtmlSidebar::setAction('index.php?option=com_insurediyci&view=genders');
		JHtmlSidebar::setAction('index.php?option=com_insurediyci&view=plans');

//		JHtmlSidebar::addFilter(
//				JText::_('JOPTION_SELECT_insurediyci_TYPE'), 'filter_insurediyci_type', JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), 'value', 'text', $this->state->get('filter.insurediyci_type'), true)
//		);


		/* JHtmlSidebar::addFilter(
		  JText::_('JOPTION_SELECT_PUBLISHED'),
		  'filter_state',
		  JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), 'value', 'text', $this->state->get('filter.state'), true)
		  );

		  JHtmlSidebar::addFilter(
		  JText::_('JOPTION_SELECT_CATEGORY'),
		  'filter_category_id',
		  JHtml::_('select.options', JHtml::_('category.options', 'com_insurediycis'), 'value', 'text', $this->state->get('filter.category_id'))
		  );

		  JHtmlSidebar::addFilter(
		  JText::_('JOPTION_SELECT_ACCESS'),
		  'filter_access',
		  JHtml::_('select.options', JHtml::_('access.assetgroups'), 'value', 'text', $this->state->get('filter.access'))
		  );

		  JHtmlSidebar::addFilter(
		  JText::_('JOPTION_SELECT_LANGUAGE'),
		  'filter_language',
		  JHtml::_('select.options', JHtml::_('contentlanguage.existing', true, true), 'value', 'text', $this->state->get('filter.language'))
		  );

		  JHtmlSidebar::addFilter(
		  '-' . JText::_('JSELECT') . ' ' . JText::_('JTAG') . '-',
		  'filter_tag',
		  JHtml::_('select.options', JHtml::_('tag.options', true, true), 'value', 'text', $this->state->get('filter.tag'))
		  );
		 */
	}

	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 *
	 * @since   3.0
	 */
	protected function getSortFields() {
		return array(
//			't.insurediyci_date' => JText::_('COM_INSUREDIYCI_CEDELEDEPOT_DATE'),
//			'a.ordering' => JText::_('JGRID_HEADING_ORDERING'),
		);
	}

}
