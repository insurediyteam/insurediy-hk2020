<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_weblinks
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

global $reqOption, $reqView, $reqLayout, $helper;

$reqOption = 'com_insurediyci';
$reqView = JRequest::getCmd('view', 'quotations');
$reqLayout = JRequest::getCmd('layout', 'default');

if (!JFactory::getUser()->authorise('core.manage', 'com_insurediyci'))
{
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

JLoader::register('InsureDIYCIHelper', __DIR__ . '/helpers/insurediyci.php');

$controller	= JControllerLegacy::getInstance('InsureDIYCI');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
