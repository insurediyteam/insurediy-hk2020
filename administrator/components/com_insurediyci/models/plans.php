<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Methods supporting a list of weblink records.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 * @since       1.6
 */
class InsureDIYCIModelPlans extends JModelList {

	/**
	 * Constructor.
	 *
	 * @param   array  An optional associative array of configuration settings.
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array()) {
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'plan_index_code', 'p.plan_index_code',
				'plan_name', 'p.plan_name',
				'sum_insured', 'p.sum_insured',
				'price', 'p.price'
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since   1.6
	 */
	protected function populateState($ordering = null, $direction = null) {
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$accessId = $this->getUserStateFromRequest($this->context . '.filter.access', 'filter_access');
		$this->setState('filter.access', $accessId);

		$stateId = $this->getUserStateFromRequest($this->context . '.filter.state', 'filter_state');
		$this->setState('filter.state', $stateId);

		$insurer_codeId = $this->getUserStateFromRequest($this->context . '.filter.insurer_code', 'filter_insurer_code');
		$this->setState('filter.insurer_code', $insurer_codeId);

		$year_codeId = $this->getUserStateFromRequest($this->context . '.filter.year_code', 'filter_year_code');
		$this->setState('filter.year_code', $year_codeId);

		$is_smokingId = $this->getUserStateFromRequest($this->context . '.filter.is_smoking', 'filter_is_smoking');
		$this->setState('filter.is_smoking', $is_smokingId);

		$ageId = $this->getUserStateFromRequest($this->context . '.filter.age', 'filter_age');
		$this->setState('filter.age', $ageId);

		$genderId = $this->getUserStateFromRequest($this->context . '.filter.gender', 'filter_gender');
		$this->setState('filter.gender', $genderId);

		$sum_insuredId = $this->getUserStateFromRequest($this->context . '.filter.sum_insured', 'filter_sum_insured');
		$this->setState('filter.sum_insured', $sum_insuredId);

		$renewableId = $this->getUserStateFromRequest($this->context . '.filter.renewable', 'filter_renewable');
		$this->setState('filter.renewable', $renewableId);

//		$terminal_illness_benefitId = $this->getUserStateFromRequest($this->context . '.filter.terminal_illness_benefit', 'filter_terminal_illness_benefit');
//		$this->setState('filter.terminal_illness_benefit', $terminal_illness_benefitId);
		// Load the parameters.
		$params = JComponentHelper::getParams('com_insurediyci');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('p.plan_name', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id    A prefix for the store id.
	 * @return  string  A store id.
	 * @since   1.6
	 */
	protected function getStoreId($id = '') {
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.access');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return  JDatabaseQuery
	 * @since   1.6
	 */
	protected function getListQuery() {
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$user = JFactory::getUser();

		/* $this->getState(
		  ' * '
		  ) */
		// Select the required fields from the table.
		$query->select('*');
		$query->from($db->quoteName('#__insure_ci_plans') . ' AS p');

		// Filter by access level.
		/* if ($access = $this->getState('filter.access'))
		  {
		  $query->where('a.access = ' . (int) $access);
		  } */


		// Filter by access level.
		/* if ($access = $this->getState('filter.access'))
		  {
		  $query->where('a.access = ' . (int) $access);
		  }

		  // Implement View Level Access
		  if (!$user->authorise('core.admin'))
		  {
		  $groups = implode(',', $user->getAuthorisedViewLevels());
		  $query->where('a.access IN (' . $groups . ')');
		  }
		 */

		// Filter by published state
		$published = $this->getState('filter.state');
		if (is_numeric($published)) {
			$query->where('p.state = ' . (int) $published);
		} elseif ($published === '') {
			$query->where('(p.state IN (0, 1))');
		}

		// Filter by Insurer Code state
		$insurer_code = $this->getState('filter.insurer_code');
		if ($insurer_code) {
			$query->where('p.insurer_code = ' . $db->Quote($insurer_code, false));
		}

		// Filter by Year Code state
		$year_code = $this->getState('filter.year_code');
		if ($year_code) {
			$query->where('p.year_code = ' . $db->Quote($year_code, false));
		}

		// Filter by Is Smoking state
		$is_smoking = $this->getState('filter.is_smoking');
		if (is_numeric($is_smoking)) {
			$query->where('p.is_smoking = ' . (int) $is_smoking);
		} elseif ($is_smoking === '') {
			$query->where('(p.is_smoking IN (0, 1))');
		}

		// Filter by Gender state
		$gender = $this->getState('filter.gender');
		if ($gender) {
			$query->where('p.gender = ' . $db->Quote($gender, false));
		}

		// Filter by Age state
		$age = $this->getState('filter.age');
		if (is_numeric($age)) {
			$query->where('p.age = ' . (int) $age);
		} elseif ($age === '') {

			$query->where('p.age > 0');
		}

		// Filter by Sum Insured state
		$sum_insured = $this->getState('filter.sum_insured');
		if (is_numeric($sum_insured)) {

			$query->where('p.sum_insured = ' . (int) $sum_insured);
		} elseif ($sum_insured === '') {

			$query->where('(p.sum_insured > 0)');
		}

		// Filter by Renewable state
		$renewable = $this->getState('filter.renewable');
		if (is_numeric($renewable)) {
			$query->where('p.renewable = ' . (int) $renewable);
		} elseif ($renewable === '') {
			$query->where('(p.renewable IN (0, 1))');
		}

//		// Filter by Has Linked Index state
//		$terminal_illness_benefit = $this->getState('filter.terminal_illness_benefit');
//		if (is_numeric($terminal_illness_benefit))
//		{
//			$query->where('p.terminal_illness_benefit = ' . (int) $terminal_illness_benefit);
//		}
//		elseif ($terminal_illness_benefit === '')
//		{
//			$query->where('(p.terminal_illness_benefit IN (0, 1))');
//		}

		/*
		  // Filter by category.
		  $categoryId = $this->getState('filter.category_id');
		  if (is_numeric($categoryId))
		  {
		  $query->where('a.catid = ' . (int) $categoryId);
		  }
		 */

		// Filter by search in title
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			$search = $db->quote('%' . $db->escape($search, true) . '%');
			$query->where(' (p.plan_name LIKE ' . $search . ') OR (p.plan_index_code LIKE ' . $search . ')');
		}

		// Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		$query->order($db->escape($orderCol . ' ' . $orderDirn));


		//echo nl2br(str_replace('#__','jos_',$query));

		return $query;
	}

}
