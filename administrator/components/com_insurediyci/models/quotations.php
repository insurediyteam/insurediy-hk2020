<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Methods supporting a list of weblink records.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 * @since       1.6
 */
class InsureDIYCIModelQuotations extends JModelList {

	/**
	 * Constructor.
	 *
	 * @param   array  An optional associative array of configuration settings.
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array()) {
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'contact_firstname', 'q.contact_firstname',
				'contact_lastname', 'q.contact_lastname',
				'cover_amt', 'q.cover_amt',
				'length_amt', 'q.length_amt',
				'email', 'q.email',
				'created_date', 'q.created_date'
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since   1.6
	 */
	protected function populateState($ordering = null, $direction = null) {
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$accessId = $this->getUserStateFromRequest($this->context . '.filter.access', 'filter_access', null, 'int');
		$this->setState('filter.access', $accessId);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_insurediyci');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('q.created_date', 'desc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id    A prefix for the store id.
	 * @return  string  A store id.
	 * @since   1.6
	 */
	protected function getStoreId($id = '') {
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.access');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return  JDatabaseQuery
	 * @since   1.6
	 */
	protected function getListQuery() {
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$user = JFactory::getUser();

		/* $this->getState(
		  ' * '
		  ) */
		// Select the required fields from the table.
		$query->select('*');
		$query->from($db->quoteName('#__insure_ci_quotations') . ' AS q');

		// Filter by access level.
		/* if ($access = $this->getState('filter.access'))
		  {
		  $query->where('a.access = ' . (int) $access);
		  } */


		// Filter by access level.
		/* if ($access = $this->getState('filter.access'))
		  {
		  $query->where('a.access = ' . (int) $access);
		  }

		  // Implement View Level Access
		  if (!$user->authorise('core.admin'))
		  {
		  $groups = implode(',', $user->getAuthorisedViewLevels());
		  $query->where('a.access IN (' . $groups . ')');
		  }
		 */

		// Filter by published state
		/* $published = $this->getState('filter.state');
		  if (is_numeric($published))
		  {
		  $query->where('a.state = ' . (int) $published);
		  }
		  elseif ($published === '')
		  {
		  $query->where('(a.state IN (0, 1))');
		  }

		  // Filter by category.
		  $categoryId = $this->getState('filter.category_id');
		  if (is_numeric($categoryId))
		  {
		  $query->where('a.catid = ' . (int) $categoryId);
		  }
		 */

		// Filter by search in title
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			$search = $db->quote('%' . $db->escape($search, true) . '%');
			$query->where(' (q.contact_firstname LIKE ' . $search . ') OR (q.contact_lastname LIKE ' . $search . ') OR (q.email LIKE ' . $search . ')');
		}

		// Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering', FALSE);
		$orderDirn = $this->state->get('list.direction', FALSE);
		if ($orderCol && $orderDirn) {
			$query->order($db->escape($orderCol . ' ' . $orderDirn));
		}


		//echo nl2br(str_replace('#__','jos_',$query));

		return $query;
	}

}
