<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Transactions model.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 * @since       1.5
 */
class InsureDIYCIModelQuotation extends JModelAdmin {

	/**
	 * @var		string	The prefix to use with controller messages.
	 * @since   1.6
	 */
	protected $text_prefix = 'COM_INSUREDIYCI';

	/**
	 * Method to test whether a record can be deleted.
	 *
	 * @param   object	A record object.
	 * @return  boolean  True if allowed to delete the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canDelete($record) {
		if (!empty($record->id)) {
			if ($record->state != -2) {
				return;
			}
			$user = JFactory::getUser();

			if ($record->catid) {
				return $user->authorise('core.delete', 'com_insurediyci.category.' . (int) $record->catid);
			} else {
				return parent::canDelete($record);
			}
		}
	}

	/**
	 * Method to test whether a record can have its state changed.
	 *
	 * @param   object	A record object.
	 * @return  boolean  True if allowed to change the state of the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canEditState($record) {
		$user = JFactory::getUser();

		if (!empty($record->catid)) {
			return $user->authorise('core.edit.state', 'com_insurediyci.category.' . (int) $record->catid);
		} else {
			return parent::canEditState($record);
		}
	}

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   type	The table type to instantiate
	 * @param   string	A prefix for the table class name. Optional.
	 * @param   array  Configuration array for model. Optional.
	 * @return  JTable	A database object
	 * @since   1.6
	 */
	public function getTable($type = 'Quotation', $prefix = 'InsureDIYCITable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array  $data		An optional array of data for the form to interogate.
	 * @param   boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return  JForm	A JForm object on success, false on failure
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true) {
		$app = JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm('com_insurediyci.quotation', 'quotation', array('control' => 'jform', 'load_data' => $loadData));

		if (empty($form)) {
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 * @since   1.6
	 */
	protected function loadFormData() {
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_insurediyci.edit.quotation.data', array());

		if (empty($data)) {
			$data = $this->getItem();

			// Prime some default values.
			if ($this->getState('quotation.id') == 0) {
				$app = JFactory::getApplication();
				//$data->set('catid', $app->input->get('catid', $app->getUserState('com_contact.contacts.filter.category_id'), 'int'));
			}
		}
//		MyHelper::debug($data);
		$this->preprocessData('com_insurediyci.quotation', $data);

		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param   integer	The id of the primary key.
	 *
	 * @return  mixed  Object on success, false on failure.
	 * @since   1.6
	 */
	public function getItem($pk = null) {
		if ($item = parent::getItem($pk)) {

			if (!empty($item->id)) {

			}

			$item->file_application = $this->getFileApplicationPaths($item->file_json_pdfpath, $item->id);
			$item->file_application2 = $this->getFileApplicationPaths($item->file_json_pdfpath2, $item->id);


			$existingInsurances = $this->getExistingInsurances($item->id);

			$values = array();
			foreach ($existingInsurances as $existingInsurance) {
				foreach ($existingInsurance as $k => $v) {
					if ($k == "issuance_date") {
						$values[$k][] = date("d-m-Y", strtotime($v));
					} else {
						$values[$k][] = $v;
					}
				}
			}

			$transaction = $this->getTransaction($item->unique_order_no);
			if (is_object($transaction)) {
				$item->PayRef = $transaction->PayRef;
				$item->TxTime = $transaction->TxTime;
				$item->Amt = $transaction->Amt;
			}


			$item->existing_insurances = $values;
			$item->selected_plans = $this->getSelectedPlans($item->id);
			$item->carModels = $this->getCarModels($item->id);


			$ownProperties = $this->getOwnProperties($item->id);

			$item->ownProperties = $ownProperties;
		}

		return $item;
	}

	public function getTransaction($unique_order_no) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_transactions")
				->where("unique_order_no = " . $db->quote($unique_order_no));
		$db->setQuery($query);

		return $db->loadObject();
	}

	public function getFileApplicationPaths($json, $id) {
		$tmp = array();
		$decoded = json_decode($json);
		$array = (is_array($decoded)) ? $decoded : MyHelper::object2array($decoded);
		foreach ($array as $path) {
			$vals = explode('|', $path);
			foreach ($vals as $val) {
				$tmp[$val] = MyHelper::getDeepPath(CI_PDF_SAVE_PATH, $id, TRUE) . trim($val);
			}
		}
		return $tmp;
	}

	public function getSelectedPlans($pk) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$query->select("plan_name, plan_index_code, sum_insured, price");
		$query->from("#__insure_ci_quotation_plan_xref");
		$query->where("quotation_id=" . $db->quote($pk));
		$db->setQuery($query);
		$result = $db->loadAssocList();
		if (!$result) {
//			$app->enqueueMessage(JText::_("COM_INSUREDIYCI_ERROR_FTL_EXISTING_INSURANCES"), "Warning");
		}
		return $result;
	}

	public function getExistingInsurances($pk) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$query->select("id, insurer_name, policy_type, sum_insured, issuance_date");
		$query->from("#__insure_ci_quotation_existing_insurances");
		$query->where("quotation_id=" . $db->quote($pk));
		$db->setQuery($query);
		$result = $db->loadAssocList();
		if (!$result) {
//			$app->enqueueMessage(JText::_("COM_INSUREDIYCI_ERROR_FTL_EXISTING_INSURANCES"), "Warning");
		}
		return $result;
	}

	public function getOwnProperties($pk) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$query->select("id, property_purchase, property_price, property_mortgage, property_value");
		$query->from("#__insure_ci_quotation_properties");
		$query->where("quotation_id=" . $db->quote($pk));
		$db->setQuery($query);
		$result = $db->loadAssocList();
		if (!$result) {
//			$app->enqueueMessage(JText::_("COM_INSUREDIYCI_ERROR_FTL_EXISTING_INSURANCES"), "Warning");
		}
		return $result;
	}

	public function getCarModels($pk) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$query->select("id, car_model");
		$query->from("#__insure_ci_quotation_cars");
		$query->where("quotation_id=" . $db->quote($pk));
		$db->setQuery($query);
		$result = $db->loadAssocList();
		if (!$result) {
//			$app->enqueueMessage(JText::_("COM_INSUREDIYCI_ERROR_FTL_EXISTING_INSURANCES"), "Warning");
		}
		return $result;
	}

//	public function getPropertyValues($pk) {
//		$app = JFactory::getApplication();
//		$db = JFactory::getDbo();
//		$query = $db->getQuery(TRUE);
//		$query->select("insurer_name, policy_type, sum_insured, issuance_date");
//		$query->from("#__insure_ci_quotation_existing_insurances");
//		$query->where("quotation_id=" . $db->quote($pk));
//		$db->setQuery($query);
//		$result = $db->loadAssocList();
//		if (!$result) {
//			$app->enqueueMessage(JText::_("COM_INSUREDIYCI_ERROR_FTL_EXISTING_INSURANCES"), "Warning");
//		}
//		return $result;
//	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @since   1.6
	 */
	protected function prepareTable($table) {
		$date = JFactory::getDate();
		$user = JFactory::getUser();

		/* $table->title		= htmlspecialchars_decode($table->title, ENT_QUOTES);
		  //$table->alias		= JApplication::stringURLSafe($table->alias);

		  if (empty($table->alias))
		  {
		  $table->alias = JApplication::stringURLSafe($table->title);
		  }

		  if (empty($table->id))
		  {
		  // Set the values
		  // Set the values
		  //$table->modified	= $date->toSql();
		  //$table->modified_by	= $user->get('id');

		  // Increment the content version number.
		  //$table->version++;
		  }
		 */
	}

	/**
	 * A protected method to get a set of ordering conditions.
	 *
	 * @param   object	A record object.
	 * @return  array  An array of conditions to add to add to ordering queries.
	 * @since   1.6
	 */
	protected function getReorderConditions($table) {
		$condition = array();

		return $condition;
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array  $data		An optional array of data for the form to interogate.
	 * @param   boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return  JForm	A JForm object on success, false on failure
	 * @since   1.6
	 */
	public function getGenderQuestion() {
		$db = JFactory::getDBO();

		$query = " SELECT * FROM #__insure_ci_quotation_questionnaire AS qx "
				. " LEFT JOIN #__insure_ci_questionnaire AS q ON q.id = qx.question_id "
				. " WHERE qx.quotation_id = " . $db->Quote($this->getState('quotation.id'), false);
		$db->setQuery($query);
		$rows = $db->loadObjectList();

		return $rows;
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array  $data		An optional array of data for the form to interogate.
	 * @param   boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return  JForm	A JForm object on success, false on failure
	 * @since   1.6
	 */
	public function getListApplicationFormHTML() { // useless
		$db = JFactory::getDBO();

		$query = " SELECT file_json_pdfpath FROM #__insure_ci_quotations WHERE id = " . $db->Quote($this->getState('quotation.id'), false);
		$db->setQuery($query);
		$json = $db->loadResult();

		$html = '';

		$array = MyHelper::object2array(json_decode($json));


		foreach ($array as $a):

			$tmp = explode('/', $a);
			$index = count($tmp) - 1;
			$html .= '<a href="' . JURI::root() . $a . '">' . $tmp[$index] . '</a><br />';

		endforeach;

		return $html;
	}

	/**
	 * Method to save the form data.
	 *
	 * @param   array  $data  The form data.
	 *
	 * @return  boolean  True on success.
	 *
	 * @since	3.1
	 */
	public function save($data) {
		$app = JFactory::getApplication();

		$jinput = JFactory::getApplication()->input->getArray($_POST);
		$data['id'] = $jinput['jform']['id'];
		
		$data['dob'] = date("Y-m-d", strtotime($jinput['jform']['dob']));
		
		$return = parent::save($data);

		// ifoundries modified
		/* if($return) {

		  $store_code = $data['code'];

		  foreach($jinput['usergroup'] as $member_type_id) :

		  $arrtmp = array();

		  for($i=1;$i<=10;$i++) :

		  $arrtmp[] = " cat".$i."_rebate_point = '". $jinput['member_'.$member_type_id.'_cat'.$i.'_rebate_point']."' ";

		  endfor;

		  $query = " UPDATE #__user_transaction_rebate_points SET ".implode(',', $arrtmp) ." WHERE store_code = '$store_code' AND user_group_id = '$member_type_id' ";
		  $this->_db->setQuery( $query );
		  $this->_db->query();

		  endforeach;
		  }
		 */
		// iFoundries : to do 
		// save the Rebate Points Here

		return $return;
	}

	/**
	 * Method to change the title & alias.
	 *
	 * @param   integer  $category_id  The id of the parent.
	 * @param   string   $alias        The alias.
	 * @param   string   $name         The title.
	 *
	 * @return  array  Contains the modified title and alias.
	 *
	 * @since   3.1
	 */
	protected function generateNewTitle($category_id, $alias, $name) {
		// Alter the title & alias
		$table = $this->getTable();

		while ($table->load(array('alias' => $alias))) {
			if ($name == $table->title) {
				$name = JString::increment($name);
			}
			$alias = JString::increment($alias, 'dash');
		}

		return array($name, $alias);
	}

	public function getPdfData($quotation_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_ci_quotations")
				->where("id=" . $quotation_id);
		$db->setQuery($query);
		$result = $db->loadObject();

		$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_ci_quotation_existing_insurances")
				->where("quotation_id=" . $quotation_id);
		$db->setQuery($query);
		$existing_insurances = $db->loadObjectList();

		$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_ci_quotation_properties")
				->where("quotation_id=" . $quotation_id)
				->order("id");
		$db->setQuery($query);
		$properties = $db->loadObjectList();

		$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_ci_quotation_cars")
				->where("quotation_id=" . $quotation_id);
		$db->setQuery($query, 0, 3);
		$cars = $db->loadObjectList();

		// Prepare data before return
		$result->hasLargeSum = $this->checkSumInsuredQuotationTotal($quotation_id) >= 5000000;
		$result->contact_firstname = strtoupper($result->contact_firstname);
		$result->contact_lastname = strtoupper($result->contact_lastname);
		$result->country_residence = InsureDIYHelper::getCountryOfResidence($result->country_residence);
		$result->nationality = InsureDIYHelper::getNationality($result->nationality);

		$result->monthly_income_ckbox = $result->monthly_income;
		$result->monthly_income = JText::_($result->monthly_income);

		switch ($result->contact_identity_type) {
			case 'chid' :
				$result->contact_identity_no2 = $result->contact_identity_no;
				unset($result->contact_identity_no);

				$tmp = explode('-', $result->contact_expiry_date);
				$result->contact_expiry_date_y = $tmp[0];
				$result->contact_expiry_date_m = $tmp[1];
				$result->contact_expiry_date_d = $tmp[2];
				break;
			case 'passport':
				$result->contact_identity_no3 = $result->contact_identity_no;
				unset($result->contact_identity_no);

				$tmp = explode('-', $result->contact_expiry_date);
				$result->contact_expiry_date_y2 = $tmp[0];
				$result->contact_expiry_date_m2 = $tmp[1];
				$result->contact_expiry_date_d2 = $tmp[2];

				$result->bank_acct_holder_type = "p";
				break;
			case "hkid":
				$result->bank_acct_holder_type = "i";
			default:
				break;
		}

		$dob = explode("-", $result->dob);
		$result->dob_y = $dob[0];
		$result->dob_m = $dob[1];
		$result->dob_d = $dob[2];
		$result->age = (int) date("Y") - (int) $dob[0];
		$result->contact_telephone = (($result->contact_country_code) ? '(' . $result->contact_country_code . ') ' : '') . $result->contact_contact_no;

		$result->job_nature = JText::_($result->job_nature);
		$result->curreny = "hkd";
		$result->mode = "monthly";
		$result->source = "savings";
		$result->purpose = "protection";
		$result->cover_amt = "HK$" . $result->cover_amt;

		$result->contact_lang = "english";

		$result->height_unit = "cm";
		$result->weight_unit = "kg";
		$result->has_used_drugs = "1";

		$result->direct_promotion = "1";
		$result->full_name = $result->contact_firstname . " " . $result->contact_lastname;
		$result->relationship = "applicant";

		foreach ($existing_insurances as $k => $existing_insurance) {
			$key = $k + 1;
			$insurer_key = "ex_insurer_name_" . $key;
			$insured_key = "ex_insured_name_" . $key;
			$sum_key = "ex_sum_insured_" . $key;
			$date_key = "ex_issuance_date_" . $key;

			$result->$insurer_key = $existing_insurance->insurer_name;
			$result->$insured_key = $result->full_name;
			$result->$sum_key = $existing_insurance->sum_insured;
			$result->$date_key = $existing_insurance->issuance_date;
		}

		$year = (int) date("Y", strtotime($result->created_date));
		$result->lastyear = $year - 1;
		$result->last2year = $year - 2;
		$result->last3year = $year - 3;
		foreach ($properties as $k => $property) {
			$key = $k + 1;
			$property_purchase_key = "property_purchase_" . $key;
			$property_price_key = "property_price_" . $key;
			$property_mortgage_key = "property_mortgage_" . $key;
			$property_value_key = "property_value_" . $key;
			$result->$property_purchase_key = $property->property_purchase;
			$result->$property_price_key = $property->property_price;
			$result->$property_mortgage_key = $property->property_mortgage;
			$result->$property_value_key = $property->property_value;
		}

		$result->no_of_cars = count($cars);

		foreach ($cars as $k => $car) {
			$key = $k + 1;
			$model_key = "car_model_" . $key;
			$result->$model_key = $car->car_model;
		}

		$result->contact_country = ($result->contact_country == "HK") ? $result->contact_country : "OTH";

		$result->fna = "0"; // SUN only
		$result->owner_type = "0";
		return $result;
	}

	public function checkSumInsuredQuotationTotal($quotation_id) {
		$db = JFactory::getDbo();
		/* 1. Sum Insured from Plans - see Step 3 page */
		$query = " SELECT sum_insured FROM #__insure_ci_quotation_plan_xref WHERE quotation_id = '$quotation_id' ";
		$db->setQuery($query);
		$rows = $db->loadObjectList();

		$total_sum_insured = 0;
		foreach ($rows as $r):
			$total_sum_insured += (int) $r->sum_insured;
		endforeach;

		/* 2. Sum Insured from Existing Plans  - see Step 4 page */
		$query = " SELECT sum_insured FROM #__insure_ci_quotation_existing_insurances WHERE quotation_id = '$quotation_id' ";
		$db->setQuery($query);
		$rows = $db->loadObjectList();

		foreach ($rows as $r):
			$total_sum_insured += intval(str_replace(",", "", $r->sum_insured));
		endforeach;

		return $total_sum_insured;
	}

	public function getPlans($quotation_id) {
		$db = JFactory::getDBO();
		$query = " SELECT * FROM #__insure_ci_quotation_plan_xref WHERE quotation_id = '$quotation_id' ";
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		return $rows;
	}

	public function savePDFPaths($quotation_id, $array = array(), $client = TRUE) {
		if (empty($array)) {
			return;
		}

		$toJson = new stdClass();

		foreach ($array as $k => $v) {
			if (is_array($v)) {
				$toJson->$k = implode(" | ", $v);
			} else {
				$toJson->$k = $v;
			}
		}

		$db = JFactory::getDBO();
		if ($client) {
			$query = " UPDATE #__insure_ci_quotations SET `file_json_pdfpath` = " . $db->Quote(json_encode($toJson), false) . " WHERE id = '$quotation_id' ";
		} else {
			$query = " UPDATE #__insure_ci_quotations SET `file_json_pdfpath2` = " . $db->Quote(json_encode($toJson), false) . " WHERE id = '$quotation_id' ";
		}
		$db->setQuery($query);
		$db->query();
	}

}
