<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

class InsureDIYCIModelCSVImport extends JModelAdmin {

	protected $text_prefix = 'COM_INSUREDIYCI';

	protected function canDelete($record) {
		if (!empty($record->id)) {
			if ($record->state != -2) {
				return;
			}
			$user = JFactory::getUser();

			if ($record->catid) {
				return $user->authorise('core.delete', 'com_insurediyci.category.' . (int) $record->catid);
			} else {
				return parent::canDelete($record);
			}
		}
	}

	protected function canEditState($record) {
		$user = JFactory::getUser();

		if (!empty($record->catid)) {
			return $user->authorise('core.edit.state', 'com_insurediyci.category.' . (int) $record->catid);
		} else {
			return parent::canEditState($record);
		}
	}

	public function getTable($type = 'Plan', $prefix = 'InsureDIYCITable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}

	public function getForm($data = array(), $loadData = true) {
		$form = $this->loadForm('com_insurediyci.csvimport', 'csvimport', array('control' => 'jform', 'load_data' => $loadData));

		if (empty($form)) {
			return false;
		}

		return $form;
	}

	protected function loadFormData() {
		$data = JFactory::getApplication()->getUserState('com_insurediyci.edit.plan.data', array());

		if (empty($data)) {
			$data = $this->getItem();
			if ($this->getState('plan.id') == 0) {
				$app = JFactory::getApplication();
			}
		}

		$this->preprocessData('com_insurediyci.plan', $data);

		return $data;
	}

	public function getItem($pk = null) {
		if ($item = parent::getItem($pk)) {

//$item->articletext = trim($item->fulltext) != '' ? $item->introtext . "<hr id=\"system-readmore\" />" . $item->fulltext : $item->introtext;

			if (!empty($item->id)) {
				/* $item->tags = new JHelperTags;
				  $item->tags->getTagIds($item->id, 'com_insurediyci.cedeledepot');
				  $item->metadata['tags'] = $item->tags;
				 */
			}
		}

		return $item;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @since   1.6
	 */
	protected function prepareTable($table) {
		$date = JFactory::getDate();
		$user = JFactory::getUser();

		/* $table->title		= htmlspecialchars_decode($table->title, ENT_QUOTES);
		  //$table->alias		= JApplication::stringURLSafe($table->alias);

		  if (empty($table->alias))
		  {
		  $table->alias = JApplication::stringURLSafe($table->title);
		  }

		  if (empty($table->id))
		  {
		  // Set the values
		  // Set the values
		  //$table->modified	= $date->toSql();
		  //$table->modified_by	= $user->get('id');

		  // Increment the content version number.
		  //$table->version++;
		  }
		 */
	}

	/**
	 * A protected method to get a set of ordering conditions.
	 *
	 * @param   object	A record object.
	 * @return  array  An array of conditions to add to add to ordering queries.
	 * @since   1.6
	 */
	protected function getReorderConditions($table) {
		$condition = array();

		return $condition;
	}

	/**
	 * Method to import plan data
	 *
	 * @param   array  $data  The form data.
	 *
	 * @return  boolean  True on success.
	 *
	 * @since	3.1
	 */
	public function import_plans() {
		ini_set('memory_limit', '-1'); //**************IMPORTANT //Lame
		$app = JFactory::getApplication();
		$db = JFactory::getDBO();

		$files = $app->input->files->get('jform');

		$file = $files['csvimport'];

		$filename = JFile::makeSafe($file['name']);

		$ext = JFile::getExt($filename);
//		MyHelper::debug($ext);exit;
		if ($ext != 'csv') {
			return false;
		}

		$dest = JPATH_ROOT . '/idy_tmp/' . $filename;
		move_uploaded_file($file['tmp_name'], $dest);
//		JFile::upload($file['tmp_name'], $dest);

		/*
		  Current Field Accepted now:
		  [0] = Insurer
		  [1] = Plan
		  [2] = Non-Smk/Smk
		  [3] = Gender
		  [4] = Age
		  [5] = SA (Sum Insured)
		  [6] = LOOKUP_INDEX
		  [7] = [PLAN][SMK][GENDER][AGE][SA]
		  [8] = PREMIUM

		 */

		if (($handle = fopen($dest, "r")) !== FALSE) {
			$fields = array(
				"plan_name",
				"insurer_code",
				"year_code",
				"is_smoking",
				"gender",
				"age",
				"sum_insured",
				"price",
				"guarantee_period",
				"renewable",
				"no_of_ci_covered",
				"early_stage_cover",
				"benefits",
				"flag_banner",
				"download_file",
				"nationality",
				"residency",
				"state",
				"ref_points",
				"pur_points",
				"plan_index_code"
			);
			$header = fgetcsv($handle, 1000, ",");
			if (count($header) < 18) {
				return FALSE;
			}

			$query = " TRUNCATE #__insure_ci_plans ;";
			$db->setQuery($query);
			$db->query();

			$queryData = array();
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				set_time_limit(0);
				foreach($data as $key => $d) {$data[$key] = addslashes($data[$key]);}
				$insurer_code = isset($data[1]) ? $data[1] : "";
				$year_code = isset($data[2]) ? $data[2] : "";
				$data[3] = ($data[3] == "S") ? 1 : 0;
				$gender = isset($data[4]) ? $data[4] : "";
				$age = isset($data[5]) ? $data[5] : "0";
				$sum_insured = isset($data[6]) ? $data[6] : "0";
				$nationality = (isset($data[15]) && strlen($data[15]) == 2 ) ? $data[15] : "XX";
				$residency = (isset($data[16]) && strlen($data[16]) == 2 ) ? $data[16] : "XX";
				$smokingTxt = ($data[3] == 0) ? "NS" : "SS";

				$plan_index_code = $insurer_code . $year_code . $smokingTxt . $gender . $age . $nationality . $residency . (floor($sum_insured / 1000000));
				$data[] = $plan_index_code;
				$queryData[] = "('" . implode("', '", $data) . "')";
			}

			$count = count($queryData);
			$perrun = 1000;
			if ($count > $perrun) {
				$times = floor($count / $perrun) + 1;
				for ($i = 0; $i < $times; $i++) {
					$left = $count - ($perrun * $i);
					$length = ($left > $perrun) ? $perrun : $left;
					$tempData = array_slice($queryData, $i * $perrun, $length);
					$query = "INSERT INTO #__insure_ci_plans (" . implode(",", $fields) . ") VALUES " . implode(",", $tempData) . ";";
					$db->setQuery($query);
					$db->query();
				}
			} else {
				$query = "INSERT INTO #__insure_ci_plans (" . implode(",", $fields) . ") VALUES " . implode(",", $queryData) . ";";
				$db->setQuery($query);
				$db->query();
			}
			fclose($handle);
		}

		unlink($dest);

// iFoundries : to do
// save the Rebate Points Here

		return true;
	}

	/**
	 * Method to change the title & alias.
	 *
	 * @param   integer  $category_id  The id of the parent.
	 * @param   string   $alias        The alias.
	 * @param   string   $name         The title.
	 *
	 * @return  array  Contains the modified title and alias.
	 *
	 * @since   3.1
	 */
	protected function generateNewTitle($category_id, $alias, $name) {
// Alter the title & alias
		$table = $this->getTable();

		while ($table->load(array('alias' => $alias))) {
			if ($name == $table->title) {
				$name = JString::increment($name);
			}
			$alias = JString::increment($alias, 'dash');
		}

		return array($name, $alias);
	}

}
