<?php

/**
 * @package     Joomla.Platform
 * @subpackage  Form
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */
defined('JPATH_PLATFORM') or die;

/**
 * Form Field class for the Joomla Platform.
 * Supports a generic list of options.
 *
 * @package     Joomla.Platform
 * @subpackage  Form
 * @since       11.1
 */
class JFormFieldInsureDIYCIList extends JFormField {

	/**
	 * The form field type.
	 *
	 * @var    string
	 * @since  11.1
	 */
	protected $type = 'InsureDIYCIList';

	/**
	 * Method to get the field label markup.
	 *
	 * @return  string  The field label markup.
	 *
	 * @since   11.1
	 */
	protected function getLabel() {
		$label = '';
		$imghelpquote = '';

		if ($this->hidden) {
			return $label;
		}

		// Get the label text from the XML element, defaulting to the element name.
		$text = $this->element['label'] ? (string) $this->element['label'] : (string) $this->element['name'];
		$text = $this->translateLabel ? JText::_($text) : $text;

		// Build the class for the label.
		$class = !empty($this->description) ? 'hasTooltip' : '';
		$class = $this->required == true ? $class . ' required' : $class;
		$class = !empty($this->labelClass) ? $class . ' ' . $this->labelClass : $class;

		// Add the opening label tag and main attributes attributes.
		$label .= '<label id="' . $this->id . '-lbl" for="' . $this->id . '" class="' . $class . '"';
		$imghelpquote .= ' ';

		// If a description is specified, use it to build a tooltip.
		if (!empty($this->description)) {

			JHtml::_('bootstrap.tooltip');
			//$imghelpquote .= '&nbsp;<img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="' . JHtml::tooltipText(trim($text, ':'), JText::_($this->description), 0) . '" />';
		}

		// Add the label text and closing tag.
		if ($this->required) {
			$label .= '>' . $text . '<span class="star">&#160;*</span>' . $imghelpquote . '</label>';
		} else {
			$label .= '>' . $text . $imghelpquote . '</label>';
		}

		return $label;
	}

	/**
	 * Method to get the field input markup for a generic list.
	 * Use the multiple attribute to enable multiselect.
	 *
	 * @return  string  The field input markup.
	 *
	 * @since   11.1
	 */
	protected function getInput() {
		$html = array();
		$attr = '';

		// Initialize some field attributes.
		$attr .=!empty($this->class) ? ' class="' . $this->class . '"' : '';
		$attr .=!empty($this->size) ? ' size="' . $this->size . '"' : '';
		$attr .= $this->multiple ? ' multiple' : '';
		$attr .= $this->required ? ' required aria-required="true"' : '';
		$attr .= $this->autofocus ? ' autofocus' : '';

		// To avoid user's confusion, readonly="true" should imply disabled="true".
		if ((string) $this->readonly == '1' || (string) $this->readonly == 'true' || (string) $this->disabled == '1' || (string) $this->disabled == 'true') {
			$attr .= ' disabled="disabled"';
		}

		// Initialize JavaScript field attributes.
		$attr .= $this->onchange ? ' onchange="' . $this->onchange . '"' : '';

		// Get the field options.
		$options = (array) $this->getOptions();
		$multipleList = (isset($this->element['multipleList'])) ? TRUE : FALSE;
		if ($multipleList) {
			$this->name = $this->name . "[]";
		}
		// Create a read-only list (no name) with a hidden input to store the value.
		if ((string) $this->readonly == '1' || (string) $this->readonly == 'true') {
			$html[] = JHtml::_('select.genericlist', $options, '', trim($attr), 'value', 'text', $this->value, $this->id);
			$html[] = '<input type="hidden" name="' . $this->name . '" value="' . $this->value . '"/>';
		} else {
			// Create a regular list.
			$html[] = JHtml::_('select.genericlist', $options, $this->name, trim($attr), 'value', 'text', $this->value, $this->id);
		}

		return implode($html);
	}

	/**
	 * Method to get the field options.
	 *
	 * @return  array  The field option objects.
	 *
	 * @since   11.1
	 */
	protected function getOptions() {
		$options = array();

		foreach ($this->element->children() as $option) {
			// Only add <option /> elements.
			if ($option->getName() != 'option') {
				continue;
			}

			// Filter requirements
			if ($requires = explode(',', (string) $option['requires'])) {
				// Requires multilanguage
				if (in_array('multilanguage', $requires) && !JLanguageMultilang::isEnabled()) {
					continue;
				}

				// Requires associations
				if (in_array('associations', $requires) && !JLanguageAssociations::isEnabled()) {
					continue;
				}
			}

			$value = (string) $option['value'];

			$disabled = (string) $option['disabled'];
			$disabled = ($disabled == 'true' || $disabled == 'disabled' || $disabled == '1');

			$disabled = $disabled || ($this->readonly && $value != $this->value);

			// Create a new option object based on the <option /> element.
			$tmp = JHtml::_(
							'select.option', $value, JText::alt(trim((string) $option), preg_replace('/[^a-zA-Z0-9_\-]/', '_', $this->fieldname)), 'value', 'text', $disabled
			);

			// Set some option attributes.
			$tmp->class = (string) $option['class'];

			// Set some JavaScript option attributes.
			$tmp->onclick = (string) $option['onclick'];

			// Add the option object to the result set.
			$options[] = $tmp;
		}

		$num_range_max = ($this->element["numRangeMax"]) ? (int) $this->element["numRangeMax"] : FALSE;
		$num_range_min = ($this->element["numRangeMin"]) ? (int) $this->element["numRangeMin"] : FALSE;
		$tmplist = array();
		$unit = ($this->element['unit']) ? $this->element['unit'] : "";
		if ($num_range_max && $num_range_min) {
			for ($i = $num_range_min; $i <= $num_range_max; $i++) {
				$tmplist[] = JHtml::_(
								'select.option', $i, $i . " " . $unit, 'value', 'text', false
				);
			}
		}

		$data = (isset($this->element["data"])) ? $this->element["data"] : FALSE;
		if ($data) {
			switch ($data) {
				case "pre_location":
					$db = JFactory::getDbo();
					$query = $db->getQuery(TRUE);
					$query->select("*");
					$query->from("#__insure_meeting_locations");
					$db->setQuery($query);
					$results = $db->loadAssocList();
					if ($results) {
						foreach ($results as $k => $result) {
							unset($result["id"]);
							$address = implode(" ", $result);
							$tmplist[] = JHtml::_(
											'select.option', $address, $address . " " . $unit, 'value', 'text', false
							);
						}
					}
					break;
				default:
					break;
			}
		}

		if (count($tmplist) > 0) {
			$options = array_merge($options, $tmplist);
		}
		reset($options);

		return $options;
	}

}
