<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_notifications
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Notification Component Notification Model
 *
 * @package     Joomla.Administrator
 * @subpackage  com_notifications
 * @since       1.6
 */
class NotificationsControllerNotification extends JControllerForm {

	/**
	 * Method (override) to check if you can save a new or existing record.
	 *
	 * Adjusts for the primary key name and hands off to the parent class.
	 *
	 * @param   array  An array of input data.
	 * @param   string	The name of the key for the primary key.
	 *
	 * @return  boolean
	 */
	protected function allowSave($data, $key = 'message_id') {
		return parent::allowSave($data, $key);
	}

	/**
	 * Reply to an existing message.
	 *
	 * This is a simple redirect to the compose form.
	 */
	public function reply() {
		if ($replyId = $this->input->getInt('reply_id')) {
			$this->setRedirect('index.php?option=com_notifications&view=notification&layout=edit&reply_id=' . $replyId);
		} else {
			$this->setMessage(JText::_('COM_NOTIFICATIONS_INVALID_REPLY_ID'));
			$this->setRedirect('index.php?option=com_notifications&view=notifications');
		}
	}

	protected function postSaveHook(JModelLegacy $model, $validData = array()) {
		if (isset($validData['message_id']) && $validData['message_id']) {
			$this->setMessage(JText::_('JLIB_APPLICATION_SAVE_SUCCESS_2'));
			$this->setRedirect('index.php?option=com_notifications&view=notifications');
		}
	}

}
