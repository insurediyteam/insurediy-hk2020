<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_notifications
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * HTML View class for the Messages component
 *
 * @package     Joomla.Administrator
 * @subpackage  com_notifications
 * @since       1.6
 */
class NotificationsViewNotification extends JViewLegacy {

	protected $form;
	protected $item;
	protected $state;

	public function display($tpl = null) {
		$this->form = $this->get('Form');
		$this->item = $this->get('Item');
		$this->state = $this->get('State');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
		
		if ($this->getLayout() == 'edit') {
			$this->message_id = JFactory::getApplication()->input->get("message_id", FALSE);
		}

		parent::display($tpl);
		$this->addToolbar();
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar() {
		if ($this->getLayout() == 'edit') {
			JToolbarHelper::title(JText::_('COM_NOTIFICATIONS_WRITE_PRIVATE_MESSAGE'), 'envelope-opened new-privatemessage');
			JToolbarHelper::save('notification.save', 'COM_NOTIFICATIONS_TOOLBAR_SEND');
			JToolbarHelper::cancel('notification.cancel');
			JToolbarHelper::help('JHELP_COMPONENTS_MESSAGING_WRITE');
		} else {
			JToolbarHelper::title(JText::_('COM_NOTIFICATIONS_VIEW_PRIVATE_MESSAGE'), 'envelope inbox');
//			$sender = JUser::getInstance($this->item->user_id_from);
//			if ($sender->authorise('core.admin') || $sender->authorise('core.manage', 'com_notifications') && $sender->authorise('core.login.admin'))
//			{
//				JToolbarHelper::custom('notification.reply', 'redo', null, 'COM_NOTIFICATIONS_TOOLBAR_REPLY', false);
//			}
			JToolbarHelper::cancel('notification.cancel');
			JToolbarHelper::help('JHELP_COMPONENTS_MESSAGING_READ');
		}
	}

}
