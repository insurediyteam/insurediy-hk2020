<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_notifications
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
JHtml::_('behavior.modal');

/**
 * View class for a list of notifications.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_notifications
 * @since       1.6
 */
class NotificationsViewNotifications extends JViewLegacy {

	protected $items;
	protected $pagination;
	protected $state;

	/**
	 * Display the view
	 */
	public function display($tpl = null) {
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
		$this->state = $this->get('State');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar() {
		$state = $this->get('State');
		$canDo = NotificationsHelper::getActions();

		JToolbarHelper::title(JText::_('COM_NOTIFICATIONS_MANAGER_MESSAGES'), 'envelope inbox');

		if ($canDo->get('core.create')) {
			JToolbarHelper::addNew('notification.add');
		}

		if ($canDo->get('core.edit.state')) {
			JToolbarHelper::divider();
			JToolbarHelper::publish('notifications.publish', 'COM_NOTIFICATIONS_TOOLBAR_MARK_AS_READ');
			JToolbarHelper::unpublish('notifications.unpublish', 'COM_NOTIFICATIONS_TOOLBAR_MARK_AS_UNREAD');
		}

		if ($state->get('filter.state') == -2 && $canDo->get('core.delete')) {
			JToolbarHelper::divider();
			JToolbarHelper::deleteList('', 'notifications.delete', 'JTOOLBAR_EMPTY_TRASH');
		} elseif ($canDo->get('core.edit.state')) {
			JToolbarHelper::divider();
			JToolbarHelper::trash('notifications.trash');
		}

		//JToolbarHelper::addNew('module.add');
		JToolbarHelper::divider();
//		$bar = JToolBar::getInstance('toolbar');

		// Instantiate a new JLayoutFile instance and render the layout
//		JHtml::_('behavior.modal', 'a.notificationsSettings');
//		$layout = new JLayoutFile('toolbar.mysettings');
//
//		$bar->appendButton('Custom', $layout->render(array()), 'upload');

		if ($canDo->get('core.admin')) {
			JToolbarHelper::preferences('com_notifications');
		}

		JToolbarHelper::divider();
		JToolbarHelper::help('JHELP_COMPONENTS_MESSAGING_INBOX');
	}

}
