<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_notifications
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * @package     Joomla.Administrator
 * @subpackage  com_notifications
 * @since       1.6
 */
class NotificationsHelper
{
	/**
	 * Configure the Linkbar.
	 *
	 * @param   string	The name of the active view.
	 *
	 * @return  void
	 * @since   1.6
	 */
	public static function addSubmenu($vName)
	{
		JHtmlSidebar::addEntry(
			JText::_('COM_NOTIFICATIONS_ADD'),
			'index.php?option=com_notifications&view=notification&layout=edit',
			$vName == 'notification'
		);

		JHtmlSidebar::addEntry(
			JText::_('COM_NOTIFICATIONS_READ'),
			'index.php?option=com_notifications',
			$vName == 'notifications'
		);
	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @return  JObject
	 */
	public static function getActions()
	{
		$user	= JFactory::getUser();
		$result	= new JObject;

		$actions = JAccess::getActions('com_notifications');

		foreach ($actions as $action)
		{
			$result->set($action->name,	$user->authorise($action->name, 'com_notifications'));
		}

		return $result;
	}

	/**
	 * Get a list of filter options for the state of a module.
	 *
	 * @return  array  An array of JHtmlOption elements.
	 */
	public static function getStateOptions()
	{
		// Build the filter options.
		$options	= array();
		$options[]	= JHtml::_('select.option',	'1',	JText::_('COM_NOTIFICATIONS_OPTION_READ'));
		$options[]	= JHtml::_('select.option',	'0',	JText::_('COM_NOTIFICATIONS_OPTION_UNREAD'));
		$options[]	= JHtml::_('select.option',	'-2',	JText::_('JTRASHED'));
		return $options;
	}
}
