<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Transactions helper.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 * @since       1.6
 */
class InsureDIYLifeHelper {

	/**
	 * Configure the Linkbar.
	 *
	 * @param   string	The name of the active view.
	 * @since   1.6
	 */
	public static function addSubmenu($vName = 'quotations') {
		JHtmlSidebar::addEntry(
				JText::_('COM_INSUREDIYLIFE_SUBMENU_QUOTATIONS'), 'index.php?option=com_insurediylife&view=quotations', $vName == 'quotations'
		);

		JHtmlSidebar::addEntry(
				JText::_('COM_INSUREDIYLIFE_SUBMENU_GENDER_QUESTIONNAIRE'), 'index.php?option=com_insurediylife&view=genders', $vName == 'genders'
		);

		JHtmlSidebar::addEntry(
				JText::_('COM_INSUREDIYLIFE_SUBMENU_PLANS'), 'index.php?option=com_insurediylife&view=plans', $vName == 'plans'
		);

		JHtmlSidebar::addEntry(
				JText::_('COM_INSUREDIYLIFE_SUBMENU_CSV_EXPORT'), 'index.php?option=com_insurediylife&view=csvexport', $vName == 'csvexport'
		);

		JHtmlSidebar::addEntry(
				JText::_('COM_INSUREDIYLIFE_SUBMENU_CSV_IMPORT'), 'index.php?option=com_insurediylife&view=csvimport', $vName == 'csvimport'
		);

		JHtmlSidebar::addEntry(
				JText::_('COM_INSUREDIYLIFE_SUBMENU_POLICYNOS'), 'index.php?option=com_insurediylife&view=policynos', $vName == 'policynos'
		);
		JHtmlSidebar::addEntry(
				JText::_('COM_INSUREDIYLIFE_SUBMENU_PNIMPORT'), 'index.php?option=com_insurediylife&view=pnimport', $vName == 'pnimport'
		);
	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @param   integer  The category ID.
	 * @return  JObject
	 * @since   1.6
	 */
	public static function getActions($categoryId = 0) {
		$user = JFactory::getUser();
		$result = new JObject;

		if (empty($categoryId)) {
			$assetName = 'com_insurediy';
			$level = 'component';
		} else {
			$assetName = 'com_insurediy.category.' . (int) $categoryId;
			$level = 'category';
		}

		$actions = JAccess::getActions('com_insurediylife', $level);

		foreach ($actions as $action) {
			$result->set($action->name, $user->authorise($action->name, $assetName));
		}

		return $result;
	}

	public static function getYesNoOptions() {
		$options = array();

		array_unshift($options, JHtml::_('select.option', '0', JText::_('JNO')));
		array_unshift($options, JHtml::_('select.option', '1', JText::_('JYES')));


		return $options;
	}

	public static function getGenderOptions() {
		$options = array();

		array_unshift($options, JHtml::_('select.option', 'F', JText::_('COM_INSUREDIYLIFE_GENDER_FEMALE')));
		array_unshift($options, JHtml::_('select.option', 'M', JText::_('COM_INSUREDIYLIFE_GENDER_MALE')));

		return $options;
	}

	public static function getInsuranceCompanyOptions() {
		$db = JFactory::getDBO();

		$options = array();

		$query = " SELECT * FROM #__insure_companies ";
		$db->setQuery($query);
		$rows = $db->loadObjectList();

		foreach ($rows as $r) :
			$options[] = JHtml::_('select.option', $r->insurer_code, $r->company_name);
		endforeach;

		return $options;
	}

	public static function getPolicyNumberStateOptions() {
		$options = array();
		$options[] = JHtml::_('select.option', 0, "COM_INSUREDIYLIFE_POLICY_NUMBER_STATE_USED");
		$options[] = JHtml::_('select.option', 1, "COM_INSUREDIYLIFE_POLICY_NUMBER_STATE_AVAILABLE");

		return $options;
	}

	public static function getYearPlanOptions() {
		$db = JFactory::getDBO();

		$options = array();

		$query = " SELECT * FROM #__insure_life_years ";
		$db->setQuery($query);
		$rows = $db->loadObjectList();

		foreach ($rows as $r) :
			$options[] = JHtml::_('select.option', $r->year_code, $r->year_name);
		endforeach;

		return $options;
	}

	public static function getAgeOptions() {
		$options = array();

		for ($i = 18; $i <= 55; $i++) :
			$options[] = JHtml::_('select.option', $i, $i);
		endfor;

		return $options;
	}

	public static function getSumInsuredOptions() {

		$arrays = array('1000000', '1800000', '2000000', '3000000', '4000000', '5000000', '6000000',
			'7000000', '9500000', '12000000', '14500000', '17000000', '19500000', '22000000',
			'24500000', '27000000');

		$options = array();

		foreach ($arrays as $a) :

			$text_number = (string) number_format($a);
			$options[] = JHtml::_('select.option', $a, $text_number);

		endforeach;

		return $options;
	}

}
