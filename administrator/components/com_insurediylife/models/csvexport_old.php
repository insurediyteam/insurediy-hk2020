<?php
	
	/**
		* @package     Joomla.Administrator
		* @subpackage  com_insurediylife
		*
		* @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
		* @license     GNU General Public License version 2 or later; see LICENSE.txt
	*/
	defined('_JEXEC') or die;
	
	/**
		* CSV Export model.
		*
		* @package     Joomla.Administrator
		* @subpackage  com_insurediylife
		* @since       1.5
	*/
	class InsureDIYLifeModelCSVExport extends JModelAdmin {
		
		/**
			* @var		string	The prefix to use with controller messages.
			* @since   1.6
		*/
		protected $text_prefix = 'COM_INSUREDIYLIFE';
		
		/**
			* Method to test whether a record can be deleted.
			*
			* @param   object	A record object.
			* @return  boolean  True if allowed to delete the record. Defaults to the permission set in the component.
			* @since   1.6
		*/
		protected function canDelete($record) {
			if (!empty($record->id)) {
				if ($record->state != -2) {
					return;
				}
				$user = JFactory::getUser();
				
				if ($record->catid) {
					return $user->authorise('core.delete', 'com_insurediylife.category.' . (int) $record->catid);
					} else {
					return parent::canDelete($record);
				}
			}
		}
		
		/**
			* Method to test whether a record can have its state changed.
			*
			* @param   object	A record object.
			* @return  boolean  True if allowed to change the state of the record. Defaults to the permission set in the component.
			* @since   1.6
		*/
		protected function canEditState($record) {
			$user = JFactory::getUser();
			
			if (!empty($record->catid)) {
				return $user->authorise('core.edit.state', 'com_insurediylife.category.' . (int) $record->catid);
				} else {
				return parent::canEditState($record);
			}
		}
		
		/**
			* Returns a reference to the a Table object, always creating it.
			*
			* @param   type	The table type to instantiate
			* @param   string	A prefix for the table class name. Optional.
			* @param   array  Configuration array for model. Optional.
			* @return  JTable	A database object
			* @since   1.6
		*/
		public function getTable($type = 'Plan', $prefix = 'InsureDIYLifeTable', $config = array()) {
			return JTable::getInstance($type, $prefix, $config);
		}
		
		/**
			* Method to get the record form.
			*
			* @param   array  $data		An optional array of data for the form to interogate.
			* @param   boolean	$loadData	True if the form is to load its own data (default case), false if not.
			* @return  JForm	A JForm object on success, false on failure
			* @since   1.6
		*/
		public function getForm($data = array(), $loadData = true) {
			$app = JFactory::getApplication();
			
			// Get the form.
			$form = $this->loadForm('com_insurediylife.csvexport', 'csvexport', array('control' => 'jform', 'load_data' => $loadData));
			
			if (empty($form)) {
				return false;
			}
			
			return $form;
		}
		
		/**
			* Method to get the data that should be injected in the form.
			*
			* @return  mixed  The data for the form.
			* @since   1.6
		*/
		protected function loadFormData() {
			// Check the session for previously entered form data.
			$data = JFactory::getApplication()->getUserState('com_insurediylife.edit.plan.data', array());
			
			if (empty($data)) {
				$data = $this->getItem();
				
				// Prime some default values.
				if ($this->getState('plan.id') == 0) {
					$app = JFactory::getApplication();
					//$data->set('catid', $app->input->get('catid', $app->getUserState('com_contact.contacts.filter.category_id'), 'int'));
				}
			}
			
			$this->preprocessData('com_insurediylife.plan', $data);
			
			return $data;
		}
		
		/**
			* Method to get a single record.
			*
			* @param   integer	The id of the primary key.
			*
			* @return  mixed  Object on success, false on failure.
			* @since   1.6
		*/
		public function getItem($pk = null) {
			
			return parent::getItem($pk);
		}
		
		/**
			* Prepare and sanitise the table prior to saving.
			*
			* @since   1.6
		*/
		protected function prepareTable($table) {
			
		}
		
		/**
			* A protected method to get a set of ordering conditions.
			*
			* @param   object	A record object.
			* @return  array  An array of conditions to add to add to ordering queries.
			* @since   1.6
		*/
		protected function getReorderConditions($table) {
			$condition = array();
			
			return $condition;
		}
		
		/**
			* Method to save the form data.
			*
			* @param   array  $data  The form data.
			*
			* @return  boolean  True on success.
			*
			* @since	3.1
		*/
		public function save($data) {
			$app = JFactory::getApplication();
			
			$jinput = JFactory::getApplication()->input->getArray($_POST);
			$data['id'] = $jinput['jform']['id'];
			
			$return = parent::save($data);
			return $return;
		}
		
		public function export_csv() {
			
			//DB limit override
			ini_set('memory_limit', '-1'); //**************IMPORTANT
			
			$app = JFactory::getApplication();
			$db = JFactory::getDBO();
			
			$jform = $app->input->post->get('jform', '', 'filter');
			
			$datatype = $jform['export_type'];
			$items = array();
			$headers = FALSE;
			set_time_limit(0);
			switch ($datatype) {
				case 'plans':
				$fields = array(
				'plan_name',
				'insurer_code',
				'year_code',
				'is_smoking',
				'gender',
				'age',
				'sum_insured',
				'price',
				'renewable',
				'terminal_illness_benefit',
				'benefits',
				'flag_banner',
				'download_file',
				'nationality',
				'residency',
				'state AS published',
				'ref_points',
				'pur_points'
				);
				$query = " SELECT " . implode(',', $fields) . " FROM #__insure_life_plans";
				$db->setQuery($query);
				$items = $db->loadAssocList();
				break;
				
				case 'all_quotations':
				case 'completed_quotations':
				case 'pending_quotations':
				$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_life_quotations");
				if ($datatype == "completed_quotations") {
					$query->where("quote_status = 1");
				}
				if ($datatype == "pending_quotations") {
					$query->where("quote_status != 1");
				}
				
				if (strlen($jform['date_start']) > 0 && strlen($jform['date_end']) > 0) {
					$sdate = new JDate($jform['date_start']);
					$edate = new JDate($jform['date_end']);
					$query->where("DATE(" . $db->quote($sdate->toSql()) . ') <= DATE(created_date)');
					$query->where('DATE(created_date) <= DATE(' . $db->quote($edate->toSql()) . ")");
					} elseif (strlen($jform['date_start']) > 0) {
					$sdate = new JDate($jform['date_start']);
					$query->where('DATE(created_date) >= DATE(' . $db->quote($sdate->toSql()) . ")");
					} elseif (strlen($jform['date_end']) > 0) {
					$edate = new JDate($jform['date_end']);
					$query->where('DATE(created_date) <= DATE(' . $db->quote($edate->toSql()) . ")");
				}
				
				$db->setQuery($query);
				$quotations = $db->loadAssocList();
				
				if (empty($quotations)) {
					break;
				}
				$headers = array_keys(reset($quotations));
				
				$query->clear();
				$query->select("qp.quotation_id, qp.plan_index_code, qp.plan_name, qp.price")
				->from("#__insure_life_quotation_plan_xref as qp");
				
				$plans = $db->loadObjectList();
				$groupedPlans = array();
				foreach ($plans as $plan) {
					$key = $plan->quotation_id;
					unset($plan->quotation_id);
					$groupedPlans[$key][] = $plan;
				}
				$maxNoOfPlans = 0;
				foreach ($groupedPlans as $groupedPlan) {
					if (count($groupedPlan) > $maxNoOfPlans) {
						$maxNoOfPlans = count($groupedPlan);
					}
				}
				$fieldNames = array("insurer_code","plan_name", "premium");
				for ($i = 1; $i <= $maxNoOfPlans; $i++) {
					foreach ($fieldNames as $fieldName) {
						$headers[] = $fieldName . "_" . $i;
					}
				}
				
				// Collect Details of Existing Insurance Applications and Policies
				$query->clear();
				$query->select("ei.quotation_id, ei.insurer_name, ei.policy_type, ei.sum_insured, ei.issuance_date")
				->from("#__insure_life_quotation_existing_insurances AS ei");
				
				$existing_insurances = $db->loadObjectList();
				$exist_ins = array();
				
				foreach($existing_insurances as $q) {
					$key = $q->quotation_id;
					unset($q->quotation_id);
					$exist_ins[$key][] = $q;
				}
				$maxNoOfExistingInsurances = 0;
				foreach ($exist_ins as $q) {
					if (count($q) > $maxNoOfExistingInsurances) {
						$maxNoOfExistingInsurances = count($q);
					}
				}
				
				$fieldNames2 = array("exist_ins_insurer_name", "exist_ins_policy_type","exist_ins_sum_insured","exist_ins_issuance_date");
				for ($i = 1; $i <= $maxNoOfExistingInsurances; $i++) {
					foreach ($fieldNames2 as $fieldName) {
						$headers[] = $fieldName . "_" . $i;
					}
				}
				
				// Collect Questionnaire per Quotation ID
				$query->clear();
				$query->select("qq.quotation_id, q.questionnaire, qq.answer")
				->from(" #__insure_life_quotation_questionnaire AS qq ")
				->join("left"," #__insure_life_questionnaire AS q ON q.id = qq.question_id ");
				
				$questionnaires = $db->loadObjectList();
				$questions = array();
				
				foreach($questionnaires as $q) {
					$key = $q->quotation_id;
					unset($q->quotation_id);
					$questions[$key][] = $q;
				}
				$maxNoOfQuestions = 0;
				foreach ($questions as $q) {
					if (count($q) > $maxNoOfQuestions) {
						$maxNoOfQuestions = count($q);
					}
				}
				
				$fieldNames3 = array("questionnaire", "answer");
				for ($i = 1; $i <= $maxNoOfQuestions; $i++) {
					foreach ($fieldNames3 as $fieldName) {
						$headers[] = $fieldName . "_" . $i;
					}
				}
				
				
				foreach ($quotations as $quotation) {
				
					// populate plans per quotation
					if (isset($groupedPlans[$quotation["id"]])) {
						$total_exist = 1;
						foreach ($groupedPlans[$quotation["id"]] as $pk => $plans) {
							foreach ($plans as $k => $v) {
								$key = $k . "_" . ($pk + 1);
								if($k == 'plan_index_code') {$v = substr($v,0,3);}
								$quotation[$key] = $v;
							}
							$total_exist++;
						}
						
						for($i = $total_exist; $i <= $maxNoOfPlans; $i++) {
							foreach($fieldNames as  $a) {
								$key = $a . "_" . ($i);
								$quotation[$key] = '';
							}
						}
						
					} else {
						for ($i = 1; $i <= $maxNoOfPlans; $i++) {
							foreach($fieldNames as  $a) {
								$key = $a . "_" . ($i);
								$quotation[$key] = '';
							}
						}
					}
					
					// populate Existing Insurances per quotation
					if (isset($exist_ins[$quotation["id"]])) {
						$total_exist = 1;
						foreach ($exist_ins[$quotation["id"]] as $pk => $q) {
							foreach ($q as $k => $v) {
								$key = $k . "_" . ($pk + 1);
								$quotation[$key] = $v;
							}
							$total_exist++;
						}
						
						for($i = $total_exist; $i <= $maxNoOfExistingInsurances; $i++) {
							foreach($fieldNames2 as  $a) {
								$key = $a . "_" . ($i);
								$quotation[$key] = '';
							}
						}
						
					} 
					else 
					{
						for ($i = 1; $i <= $maxNoOfExistingInsurances; $i++) {
							foreach($fieldNames2 as  $a) {
								$key = $a . "_" . ($i);
								$quotation[$key] = '';
							}
						}
					}
					
					// populate Question per quotation
					if (isset($questions[$quotation["id"]])) {
						foreach ($questions[$quotation["id"]] as $pk => $q) {
							foreach ($q as $k => $v) {
								$key = $k . "_" . ($pk + 1);
								$quotation[$key] = $v;
								
							}
						}
					}
				
					$items[] = $quotation;
				}
				break;
				case 'policies':
				$fields = array(
				'p.insurer',
				'p.policy_number',
				'c.code AS currency',
				'p.cover_amt',
				'p.start_date',
				'p.end_date',
				'p.status'
				);
				
				$query = $db->getQuery(TRUE)
				->select($fields)
				->from("#__insure_policies AS p ")
				->join("left", "#__currencies AS c ON c.id = p.currency ")
				->where("p.type = 1 ");
				
				$db->setQuery($query);
				$items = $db->loadAssocList();
				
				foreach($items as $i => &$item) {
					$item['status'] = InsureDIYHelper::getPoliciesStatus($item['status']);
				}
				
				break;
			}
			
			if (count($items) == 0) {
				echo 'No Items';
				} else {
				
				ob_start();
				
				$df = fopen("php://output", 'w');
				
				$headers = ($headers) ? $headers : array_keys(reset($items));
				
				fputcsv($df, $headers);
				
				foreach ($items as $row) {
					
					switch ($datatype) {
						case 'plans':
						$row['is_smoking'] = (($row['is_smoking'] == 1) ? 'S' : 'NS');
						
						break;
						case 'all_quotations':
						case 'completed_quotations':
						case 'pending_quotations':
						
						$row['job_nature'] = JText::_($row['job_nature']);
						$row['monthly_income'] = JText::_($row['monthly_income']);
						$row['job_nature'] = JText::_($row['job_nature']);
						$row['created_date'] = JHtml::_('date', $row['created_date'], 'd-m-Y H:i');
						$row['contact_identity_no'] = str_replace(chr(239).chr(188).chr(136),'(', $row['contact_identity_no']);
						$row['contact_identity_no'] = str_replace(chr(239).chr(188).chr(137),')', $row['contact_identity_no']);
						break;
						default:
						break;
					}
					fputcsv($df, $row,',','"');
				}
				fclose($df);
			}
			
			
			// disable caching
			$filename = 'life_' . $datatype . '_' . JHtml::date(NULL, 'EXPORT_DATE_FORMAT') . '.csv';
			$now = gmdate("D, d M Y H:i:s");
			header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
			header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
			header("Last-Modified: {$now} GMT");
			
			// force download
			header("Content-Type: application/force-download");
			header("Content-Type: application/octet-stream");
			header("Content-Type: application/download");
			
			// disposition / encoding on response body
			header("Content-Disposition: attachment;filename={$filename}");
			header("Content-Transfer-Encoding: binary");
			
			return true;
		}
		
		/**
			* Method to change the title & alias.
			*
			* @param   integer  $category_id  The id of the parent.
			* @param   string   $alias        The alias.
			* @param   string   $name         The title.
			*
			* @return  array  Contains the modified title and alias.
			*
			* @since   3.1
		*/
		protected function generateNewTitle($category_id, $alias, $name) {
			// Alter the title & alias
			$table = $this->getTable();
			
			while ($table->load(array('alias' => $alias))) {
				if ($name == $table->title) {
					$name = JString::increment($name);
				}
				$alias = JString::increment($alias, 'dash');
			}
			
			return array($name, $alias);
		}
		
	}
