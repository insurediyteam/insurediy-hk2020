<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
JLoader::import("components.com_insurediylife.helpers.insurediylife", JPATH_ROOT);

/**
 * Transactions model.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 * @since       1.5
 */
class InsureDIYLifeModelQuotation extends JModelAdmin {

	/**
	 * @var		string	The prefix to use with controller messages.
	 * @since   1.6
	 */
	protected $text_prefix = 'COM_INSUREDIYLIFE';

	/**
	 * Method to test whether a record can be deleted.
	 *
	 * @param   object	A record object.
	 * @return  boolean  True if allowed to delete the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canDelete($record) {
		if (!empty($record->id)) {
			if ($record->state != -2) {
				return;
			}
			$user = JFactory::getUser();

			if ($record->catid) {
				return $user->authorise('core.delete', 'com_insurediylife.category.' . (int) $record->catid);
			} else {
				return parent::canDelete($record);
			}
		}
	}

	/**
	 * Method to test whether a record can have its state changed.
	 *
	 * @param   object	A record object.
	 * @return  boolean  True if allowed to change the state of the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canEditState($record) {
		$user = JFactory::getUser();

		if (!empty($record->catid)) {
			return $user->authorise('core.edit.state', 'com_insurediylife.category.' . (int) $record->catid);
		} else {
			return parent::canEditState($record);
		}
	}

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   type	The table type to instantiate
	 * @param   string	A prefix for the table class name. Optional.
	 * @param   array  Configuration array for model. Optional.
	 * @return  JTable	A database object
	 * @since   1.6
	 */
	public function getTable($type = 'Quotation', $prefix = 'InsureDIYLifeTable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array  $data		An optional array of data for the form to interogate.
	 * @param   boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return  JForm	A JForm object on success, false on failure
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true) {
		$app = JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm('com_insurediylife.quotation', 'quotation', array('control' => 'jform', 'load_data' => $loadData));

		if (empty($form)) {
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 * @since   1.6
	 */
	protected function loadFormData() {
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_insurediylife.edit.quotation.data', array());

		if (empty($data)) {
			$data = $this->getItem();
//			MyHelper::debug($data);exit;
			// Prime some default values.
			if ($this->getState('quotation.id') == 0) {
				$app = JFactory::getApplication();
				//$data->set('catid', $app->input->get('catid', $app->getUserState('com_contact.contacts.filter.category_id'), 'int'));
			}
		}
//		MyHelper::debug($data);
		$this->preprocessData('com_insurediylife.quotation', $data);

		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param   integer	The id of the primary key.
	 *
	 * @return  mixed  Object on success, false on failure.
	 * @since   1.6
	 */
	public function getItem($pk = null) {
		if ($item = parent::getItem($pk)) {

			if (!empty($item->id)) {

			}

//			$item->file_application = $this->getFileApplicationPaths($item->file_json_pdfpath, $item->user_id);
//			$item->file_application2 = $this->getFileApplicationPaths($item->file_json_pdfpath2, $item->user_id);
			$item->file_application = $this->getFileApplicationPaths($item->file_json_pdfpath, $item->user_id);
			$item->file_application2 = $this->getFileApplicationPaths($item->file_json_pdfpath2, $item->user_id);


			$existingInsurances = $this->getExistingInsurances($item->id);

			$values = array();
			foreach ($existingInsurances as $existingInsurance) {
				foreach ($existingInsurance as $k => $v) {
					if ($k == "issuance_date") {
						$values[$k][] = date("d-m-Y", strtotime($v));
					} else {
						$values[$k][] = $v;
					}
				}
			}

			$transaction = $this->getTransaction($item->unique_order_no);
			if (is_object($transaction)) {
				$item->PayRef = $transaction->PayRef;
				$item->TxTime = $transaction->TxTime;
				$item->Amt = $transaction->Amt;
			}


			$item->existing_insurances = $values;
			$item->selected_plans = $this->getSelectedPlans($item->id);
			$item->carModels = $this->getCarModels($item->id);


			$ownProperties = $this->getOwnProperties($item->id);

			$item->ownProperties = $ownProperties;
		}

		return $item;
	}

	public function getTransaction($unique_order_no) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_transactions")
				->where("unique_order_no = " . $db->quote($unique_order_no));
		$db->setQuery($query);

		return $db->loadObject();
	}

	public function getFileApplicationPaths($json, $id) {
		$tmp = array();
		$decoded = json_decode($json);
		$array = (is_array($decoded)) ? $decoded : MyHelper::object2array($decoded);
		foreach ($array as $path) {
			$vals = explode('|', $path);
			foreach ($vals as $val) {
//				$tmp[$val] = MyHelper::getDeepPath(LIFE_PDF_SAVE_PATH, $id, TRUE) . trim($val);
				$tmp[$val] =$val;
			}
		}
		return $tmp;
	}

	public function getSelectedPlans($pk) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$query->select("plan_name, plan_index_code, sum_insured, price");
		$query->from("#__insure_life_quotation_plan_xref");
		$query->where("quotation_id=" . $db->quote($pk));
		$db->setQuery($query);
		$result = $db->loadAssocList();
		if (!$result) {
//			$app->enqueueMessage(JText::_("COM_INSUREDIYLIFE_ERROR_FTL_EXISTING_INSURANCES"), "Warning");
		}
		return $result;
	}

	public function getExistingInsurances($pk) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$query->select("id, insurer_name, policy_type, sum_insured, issuance_date");
		$query->from("#__insure_life_quotation_existing_insurances");
		$query->where("quotation_id=" . $db->quote($pk));
		$db->setQuery($query);
		$result = $db->loadAssocList();
		if (!$result) {
//			$app->enqueueMessage(JText::_("COM_INSUREDIYLIFE_ERROR_FTL_EXISTING_INSURANCES"), "Warning");
		}
		return $result;
	}

	public function getOwnProperties($pk) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$query->select("id, property_purchase, property_price, property_mortgage, property_value");
		$query->from("#__insure_life_quotation_properties");
		$query->where("quotation_id=" . $db->quote($pk));
		$db->setQuery($query);
		$result = $db->loadAssocList();
		if (!$result) {
//			$app->enqueueMessage(JText::_("COM_INSUREDIYLIFE_ERROR_FTL_EXISTING_INSURANCES"), "Warning");
		}
		return $result;
	}

	public function getCarModels($pk) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$query->select("id, car_model");
		$query->from("#__insure_life_quotation_cars");
		$query->where("quotation_id=" . $db->quote($pk));
		$db->setQuery($query);
		$result = $db->loadAssocList();
		if (!$result) {
//			$app->enqueueMessage(JText::_("COM_INSUREDIYLIFE_ERROR_FTL_EXISTING_INSURANCES"), "Warning");
		}
		return $result;
	}

//	public function getPropertyValues($pk) {
//		$app = JFactory::getApplication();
//		$db = JFactory::getDbo();
//		$query = $db->getQuery(TRUE);
//		$query->select("insurer_name, policy_type, sum_insured, issuance_date");
//		$query->from("#__insure_life_quotation_existing_insurances");
//		$query->where("quotation_id=" . $db->quote($pk));
//		$db->setQuery($query);
//		$result = $db->loadAssocList();
//		if (!$result) {
//			$app->enqueueMessage(JText::_("COM_INSUREDIYLIFE_ERROR_FTL_EXISTING_INSURANCES"), "Warning");
//		}
//		return $result;
//	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @since   1.6
	 */
	protected function prepareTable($table) {
		$date = JFactory::getDate();
		$user = JFactory::getUser();

		/* $table->title		= htmlspecialchars_decode($table->title, ENT_QUOTES);
		  //$table->alias		= JApplication::stringURLSafe($table->alias);

		  if (empty($table->alias))
		  {
		  $table->alias = JApplication::stringURLSafe($table->title);
		  }

		  if (empty($table->id))
		  {
		  // Set the values
		  // Set the values
		  //$table->modified	= $date->toSql();
		  //$table->modified_by	= $user->get('id');

		  // Increment the content version number.
		  //$table->version++;
		  }
		 */
	}

	/**
	 * A protected method to get a set of ordering conditions.
	 *
	 * @param   object	A record object.
	 * @return  array  An array of conditions to add to add to ordering queries.
	 * @since   1.6
	 */
	protected function getReorderConditions($table) {
		$condition = array();

		return $condition;
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array  $data		An optional array of data for the form to interogate.
	 * @param   boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return  JForm	A JForm object on success, false on failure
	 * @since   1.6
	 */
	public function getGenderQuestion() {
		$db = JFactory::getDBO();

		$query = " SELECT * FROM #__insure_life_quotation_questionnaire AS qx "
				. " LEFT JOIN #__insure_life_questionnaire AS q ON q.id = qx.question_id "
				. " WHERE qx.quotation_id = " . $db->Quote($this->getState('quotation.id'), false);
		$db->setQuery($query);
		$rows = $db->loadObjectList();

		return $rows;
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array  $data		An optional array of data for the form to interogate.
	 * @param   boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return  JForm	A JForm object on success, false on failure
	 * @since   1.6
	 */
	public function getListApplicationFormHTML() { // useless
		$db = JFactory::getDBO();

		$query = " SELECT file_json_pdfpath FROM #__insure_life_quotations WHERE id = " . $db->Quote($this->getState('quotation.id'), false);
		$db->setQuery($query);
		$json = $db->loadResult();

		$html = '';

		$array = MyHelper::object2array(json_decode($json));


		foreach ($array as $a):

			$tmp = explode('/', $a);
			$index = count($tmp) - 1;
			$html .= '<a href="' . JURI::root() . $a . '">' . $tmp[$index] . '</a><br />';

		endforeach;

		return $html;
	}

	/**
	 * Method to save the form data.
	 *
	 * @param   array  $data  The form data.
	 *
	 * @return  boolean  True on success.
	 *
	 * @since	3.1
	 */
	public function save($data) {
		$app = JFactory::getApplication();

		$jinput = JFactory::getApplication()->input->getArray($_POST);
		$data['id'] = $jinput['jform']['id'];
		$return = parent::save($data);

		// ifoundries modified
		/* if($return) {

		  $store_code = $data['code'];

		  foreach($jinput['usergroup'] as $member_type_id) :

		  $arrtmp = array();

		  for($i=1;$i<=10;$i++) :

		  $arrtmp[] = " cat".$i."_rebate_point = '". $jinput['member_'.$member_type_id.'_cat'.$i.'_rebate_point']."' ";

		  endfor;

		  $query = " UPDATE #__user_transaction_rebate_points SET ".implode(',', $arrtmp) ." WHERE store_code = '$store_code' AND user_group_id = '$member_type_id' ";
		  $this->_db->setQuery( $query );
		  $this->_db->query();

		  endforeach;
		  }
		 */
		// iFoundries : to do 
		// save the Rebate Points Here

		return $return;
	}

	/**
	 * Method to change the title & alias.
	 *
	 * @param   integer  $category_id  The id of the parent.
	 * @param   string   $alias        The alias.
	 * @param   string   $name         The title.
	 *
	 * @return  array  Contains the modified title and alias.
	 *
	 * @since   3.1
	 */
	protected function generateNewTitle($category_id, $alias, $name) {
		// Alter the title & alias
		$table = $this->getTable();

		while ($table->load(array('alias' => $alias))) {
			if ($name == $table->title) {
				$name = JString::increment($name);
			}
			$alias = JString::increment($alias, 'dash');
		}

		return array($name, $alias);
	}

	public function getPdfData($quotation_id) {
		return InsureDIYLifeHelper::getPdfData($quotation_id);
	}

	public function checkSumInsuredQuotationTotal($quotation_id) {
		$db = JFactory::getDbo();
		/* 1. Sum Insured from Plans - see Step 3 page */
		$query = " SELECT sum_insured FROM #__insure_life_quotation_plan_xref WHERE quotation_id = '$quotation_id' ";
		$db->setQuery($query);
		$rows = $db->loadObjectList();

		$total_sum_insured = 0;
		foreach ($rows as $r):
			$total_sum_insured += (int) $r->sum_insured;
		endforeach;

		/* 2. Sum Insured from Existing Plans  - see Step 4 page */
		$query = " SELECT sum_insured FROM #__insure_life_quotation_existing_insurances WHERE quotation_id = '$quotation_id' ";
		$db->setQuery($query);
		$rows = $db->loadObjectList();

		foreach ($rows as $r):
			$total_sum_insured += intval(str_replace(",", "", $r->sum_insured));
		endforeach;

		return $total_sum_insured;
	}

	public function getPlans($quotation_id) {
		$db = JFactory::getDBO();
		$planQuery = $db->getQuery(TRUE)->select("px.*, c.company_name, p.insurer_code")
				->from("#__insure_life_quotation_plan_xref px")
				->leftJoin("#__insure_life_plans AS p ON p.plan_index_code = px.plan_index_code AND p.sum_insured = px.sum_insured")
				->leftJoin("#__insure_companies AS c ON c.insurer_code = p.insurer_code")
				->where("px.quotation_id = " . $db->quote($quotation_id));
		$rows = $db->setQuery($planQuery)->loadObjectList();
		return $rows;
	}

	public function savePDFPaths($quotation_id, $array = array(), $client = TRUE) {
		if (empty($array)) {
			return;
		}
		$db = JFactory::getDBO();

		$toJson = new stdClass();

		foreach ($array as $k => $v) {
			if (is_array($v)) {
				$toJson->$k = $db->escape(implode(" | ", $v));
			} else {
				$toJson->$k = $db->escape($v);
			}
		}

		if ($client) {
			$query = " UPDATE #__insure_life_quotations SET `file_json_pdfpath` = " . $db->Quote(json_encode($toJson), false) . " WHERE id = '$quotation_id' ";
		} else {
			$query = " UPDATE #__insure_life_quotations SET `file_json_pdfpath2` = " . $db->Quote(json_encode($toJson), false) . " WHERE id = '$quotation_id' ";
		}
		$db->setQuery($query);
		$db->query();
	}

}
