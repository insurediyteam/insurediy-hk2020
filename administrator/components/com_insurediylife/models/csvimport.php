<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Transactions model.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 * @since       1.5
 */
class InsureDIYLifeModelCSVImport extends JModelAdmin {

	/**
	 * @var		string	The prefix to use with controller messages.
	 * @since   1.6
	 */
	protected $text_prefix = 'COM_INSUREDIYLIFE';

	/**
	 * Method to test whether a record can be deleted.
	 *
	 * @param   object	A record object.
	 * @return  boolean  True if allowed to delete the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canDelete($record) {
		if (!empty($record->id)) {
			if ($record->state != -2) {
				return;
			}
			$user = JFactory::getUser();

			if ($record->catid) {
				return $user->authorise('core.delete', 'com_insurediylife.category.' . (int) $record->catid);
			} else {
				return parent::canDelete($record);
			}
		}
	}

	/**
	 * Method to test whether a record can have its state changed.
	 *
	 * @param   object	A record object.
	 * @return  boolean  True if allowed to change the state of the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canEditState($record) {
		$user = JFactory::getUser();

		if (!empty($record->catid)) {
			return $user->authorise('core.edit.state', 'com_insurediylife.category.' . (int) $record->catid);
		} else {
			return parent::canEditState($record);
		}
	}

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   type	The table type to instantiate
	 * @param   string	A prefix for the table class name. Optional.
	 * @param   array  Configuration array for model. Optional.
	 * @return  JTable	A database object
	 * @since   1.6
	 */
	public function getTable($type = 'Plan', $prefix = 'InsureDIYLifeTable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array  $data		An optional array of data for the form to interogate.
	 * @param   boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return  JForm	A JForm object on success, false on failure
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true) {
		$app = JFactory::getApplication();

// Get the form.
		$form = $this->loadForm('com_insurediylife.csvimport', 'csvimport', array('control' => 'jform', 'load_data' => $loadData));

		if (empty($form)) {
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 * @since   1.6
	 */
	protected function loadFormData() {
// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_insurediylife.edit.plan.data', array());

		if (empty($data)) {
			$data = $this->getItem();

// Prime some default values.
			if ($this->getState('plan.id') == 0) {
				$app = JFactory::getApplication();
//$data->set('catid', $app->input->get('catid', $app->getUserState('com_contact.contacts.filter.category_id'), 'int'));
			}
		}

		$this->preprocessData('com_insurediylife.plan', $data);

		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param   integer	The id of the primary key.
	 *
	 * @return  mixed  Object on success, false on failure.
	 * @since   1.6
	 */
	public function getItem($pk = null) {
		if ($item = parent::getItem($pk)) {

//$item->articletext = trim($item->fulltext) != '' ? $item->introtext . "<hr id=\"system-readmore\" />" . $item->fulltext : $item->introtext;

			if (!empty($item->id)) {
				/* $item->tags = new JHelperTags;
				  $item->tags->getTagIds($item->id, 'com_insurediylife.cedeledepot');
				  $item->metadata['tags'] = $item->tags;
				 */
			}
		}

		return $item;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @since   1.6
	 */
	protected function prepareTable($table) {
		$date = JFactory::getDate();
		$user = JFactory::getUser();

		/* $table->title		= htmlspecialchars_decode($table->title, ENT_QUOTES);
		  //$table->alias		= JApplication::stringURLSafe($table->alias);

		  if (empty($table->alias))
		  {
		  $table->alias = JApplication::stringURLSafe($table->title);
		  }

		  if (empty($table->id))
		  {
		  // Set the values
		  // Set the values
		  //$table->modified	= $date->toSql();
		  //$table->modified_by	= $user->get('id');

		  // Increment the content version number.
		  //$table->version++;
		  }
		 */
	}

	/**
	 * A protected method to get a set of ordering conditions.
	 *
	 * @param   object	A record object.
	 * @return  array  An array of conditions to add to add to ordering queries.
	 * @since   1.6
	 */
	protected function getReorderConditions($table) {
		$condition = array();

		return $condition;
	}

	/**
	 * Method to import plan data
	 *
	 * @param   array  $data  The form data.
	 *
	 * @return  boolean  True on success.
	 *
	 * @since	3.1
	 */
	public function import_plans() {
		ini_set('memory_limit', '-1'); //**************IMPORTANT
		$app = JFactory::getApplication();
		$db = JFactory::getDBO();

		$files = $app->input->files->get('jform');

		$file = $files['csvimport'];

		$filename = JFile::makeSafe($file['name']);

		$ext = JFile::getExt($filename);

		if ($ext != 'csv') {
			return false;
		}

		$dest = JPATH_ROOT . '/tmp/' . $filename;
//		move_uploaded_file($file['tmp_name'], $dest);
		JFile::upload($file['tmp_name'], $dest);

		$fields = array(
			"plan_name",
			"plan_index_code",
			"insurer_code",
			"year_code",
			"is_smoking",
			"gender",
			"age",
			"sum_insured",
			"renewable",
			"terminal_illness_benefit",
			"benefits",
			"flag_banner",
			"download_file",
			"price",
			"nationality",
			"residency",
			"ref_points",
			"pur_points",
			"state",
		);

		if (($handle = fopen($dest, "r")) !== FALSE) {

			$header = fgetcsv($handle, 1000, ",");
			if (count($header) < 16) {
				return FALSE;
			}

			/* clear the data */
			$query = " TRUNCATE #__insure_life_plans ;";
			$db->setQuery($query);
			$db->query();

			$queryData = array();
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {

				set_time_limit(0);
				$plan_name = isset($data[0]) ? $data[0] : "";
				$insurer_code = isset($data[1]) ? $data[1] : "";
				$year_code = isset($data[2]) ? $data[2] : "";
				$smoking = ($data[3] == 'S') ? "1" : "0";
				$gender = isset($data[4]) ? $data[4] : "";
				$age = isset($data[5]) ? $data[5] : "0";
				$sum_insured = isset($data[6]) ? $data[6] : "0";
				$price = isset($data[7]) ? $data[7] : "";
				$renewable = isset($data[8]) ? $data[8] : "0";
				$terminal_illness_benefit = isset($data[9]) ? $data[9] : "0";
				$benefits = isset($data[10]) ? $data[10] : "";
				$flag_banner = isset($data[11]) ? $data[11] : "";
				$download_file = isset($data[12]) ? $data[12] : "";
				$nationality = (isset($data[13]) && strlen($data[13]) == 2 ) ? $data[13] : "XX";
				$residency = (isset($data[14]) && strlen($data[14]) == 2 ) ? $data[14] : "XX";
				$state = isset($data[15]) ? $data[15] : "1";
				$ref_points = isset($data[16]) ? $data[16] : "0";
				$pur_points = isset($data[17]) ? $data[17] : "0";

				$plan_index_code = $insurer_code . $year_code . (($smoking == "0") ? "NS" : "SS") . $gender . $age . $nationality . $residency . (floor($sum_insured / 1000000));

				$queryData[] = "("
						. $db->quote($plan_name) . ", "
						. $db->quote($plan_index_code) . ", "
						. $db->quote($insurer_code) . ", "
						. $db->quote($year_code) . ", "
						. $db->quote($smoking) . ", "
						. $db->quote($gender) . ", "
						. $db->quote($age) . ", "
						. $db->quote($sum_insured) . ", "
						. $db->quote($renewable) . ", "
						. $db->quote($terminal_illness_benefit) . ", "
						. $db->quote($benefits) . ", "
						. $db->quote($flag_banner) . ", "
						. $db->quote($download_file) . ", "
						. $db->quote($price) . ", "
						. $db->quote($nationality) . ", "
						. $db->quote($residency) . ", "
						. $ref_points . ","
						. $pur_points . ","
						. $state . ")";
			}

			$count = count($queryData);
			$perrun = 1000;
			if ($count > $perrun) {
				$times = floor($count / $perrun) + 1;
				for ($i = 0; $i < $times; $i++) {
					$left = $count - ($perrun * $i);
					$length = ($left > $perrun) ? $perrun : $left;
					$tempData = array_slice($queryData, $i * $perrun, $length);
					$query = "INSERT INTO #__insure_life_plans (" . implode(",", $fields) . ") VALUES " . implode(",", $tempData) . ";";
					$db->setQuery($query);
					$db->query();
				}
			} else {
				$query = "INSERT INTO #__insure_life_plans (" . implode(",", $fields) . ") VALUES " . implode(",", $queryData) . ";";
				$db->setQuery($query);
				$db->query();
			}
			fclose($handle);
		}

//		unlink($dest);

		return true;
	}

	/**
	 * Method to change the title & alias.
	 *
	 * @param   integer  $category_id  The id of the parent.
	 * @param   string   $alias        The alias.
	 * @param   string   $name         The title.
	 *
	 * @return  array  Contains the modified title and alias.
	 *
	 * @since   3.1
	 */
	protected function generateNewTitle($category_id, $alias, $name) {
// Alter the title & alias
		$table = $this->getTable();

		while ($table->load(array('alias' => $alias))) {
			if ($name == $table->title) {
				$name = JString::increment($name);
			}
			$alias = JString::increment($alias, 'dash');
		}

		return array($name, $alias);
	}

}
