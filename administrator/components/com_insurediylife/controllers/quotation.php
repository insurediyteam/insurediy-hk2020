<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_insurediylife
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Weblink controller class.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_insurediylife
 * @since       1.6
 */
class InsureDIYLifeControllerQuotation extends JControllerForm {

	/**
	 * Method override to check if you can add a new record.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  boolean
	 *
	 * @since   1.6
	 */
	protected function allowAdd($data = array()) {
		$user = JFactory::getUser();
		//$categoryId = JArrayHelper::getValue($data, 'catid', $this->input->getInt('filter_category_id'), 'int');
		$allow = null;

		/* if ($categoryId)
		  {
		  // If the category has been passed in the URL check it.
		  $allow = $user->authorise('core.create', $this->option . '.category.' . $categoryId);
		  }
		 */

		if ($allow === null) {
			// In the absense of better information, revert to the component permissions.
			return parent::allowAdd($data);
		} else {
			return $allow;
		}
	}

	/**
	 * Method to check if you can add a new record.
	 *
	 * @param   array   $data  An array of input data.
	 * @param   string  $key   The name of the key for the primary key.
	 *
	 * @return  boolean
	 * @since   1.6
	 */
	protected function allowEdit($data = array(), $key = 'id') {
		$recordId = (int) isset($data[$key]) ? $data[$key] : 0;
		$categoryId = 0;
		if ($recordId) {
			$categoryId = (int) $this->getModel()->getItem($recordId)->catid;
		}

		if ($categoryId) {
			// The category has been set. Check the category permissions.
			return JFactory::getUser()->authorise('core.edit', $this->option . '.category.' . $categoryId);
		} else {
			// Since there is no asset tracking, revert to the component permissions.
			return parent::allowEdit($data, $key);
		}
	}

	/**
	 * Method to run batch operations.
	 *
	 * @param   object  $model  The model.
	 *
	 * @return  boolean   True if successful, false otherwise and internal error is set.
	 *
	 * @since   1.7
	 */
	public function batch($model = null) {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Set the model
		$model = $this->getModel('InsuredDIYLife', '', array());

		// Preset the redirect
		$this->setRedirect(JRoute::_('index.php?option=com_insurediylife&view=quotations' . $this->getRedirectToListAppend(), false));

		return parent::batch($model);
	}

	/**
	 * Function that allows child controller access to model data after the data has been saved.
	 *
	 * @param   JModelLegacy  $model      The data model object.
	 * @param   array         $validData  The validated data.
	 *
	 * @return	void
	 * @since	1.6
	 */
	protected function postSaveHook(JModelLegacy $model, $validData = array()) {
		if ($task == 'save') {
			$this->setRedirect(JRoute::_('index.php?option=com_insurediylife&view=quotations', false));
		}
	}

	public function saveEI() {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$data = JFactory::getApplication()->input->getArray($_POST);
		$data['jform']['id'] = $data['ei_id'];

		$data['jform']['issuance_date'] = date("Y-m-d", strtotime($data['jform']['issuance_date']));
		$model = $this->getModel("existinginsurance");
		$result = $model->save($data['jform']);

		echo json_encode($data);
		exit;
	}

	public function addEI() {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$data = JFactory::getApplication()->input->getArray($_POST);
		$model = $this->getModel("existinginsurance");
		$data['jform']['quotation_id'] = $data['quotation_id'];
		$data['jform']['issuance_date'] = date("Y-m-d", strtotime($data['jform']['issuance_date']));
		$result = $model->save($data['jform']);
		echo json_encode($result);
		exit;
//		if ($result) {
//			MyUri::redirect("index.php?option=com_insurediylife&view=quotation&layout=edit&id=" . $data['quotation_id'] . "&activetab=existing_insurances", JText::_("COM_INSUREDIYLIFE_SUCCESSFULLY_ADD_EI_MSG"));
//		} else {
//			MyUri::redirect("index.php?option=com_insurediylife&view=quotation&layout=edit&id=" . $data['quotation_id'] . "&activetab=existing_insurances", JText::_("COM_INSUREDIYLIFE_FAIL_TO_ADD_EI_MSG"));
//		}
	}

	public function deleteEI() {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$jInput = JFactory::getApplication()->input->getArray($_POST);
		$model = $this->getModel("existinginsurance");
		$data['quotation_id'] = $jInput['quotation_id'];
		$data['ei_id'] = $jInput['element_id'];
		$result = $model->delete($data);
		if ($result) {
			MyUri::redirect("index.php?option=com_insurediylife&view=quotation&layout=edit&id=" . $data['quotation_id'] . "&activetab=existing_insurances", JText::_("COM_INSUREDIYLIFE_SUCCESSFULLY_DELETE_EI_MSG"));
		} else {
			MyUri::redirect("index.php?option=com_insurediylife&view=quotation&layout=edit&id=" . $data['quotation_id'] . "&activetab=existing_insurances", JText::_("COM_INSUREDIYLIFE_FAIL_TO_DELETE_EI_MSG"));
		}
	}

//	public function generatepdf() {
//		$app = JFactory::getApplication();
//		$id = $app->input->get("id", FALSE);
//		$model = $this->getModel();
//		if ($id && MyHelper::is_admin()) {
//			$data = $model->getPdfData($id);
//			$plans = $model->getPlans($id);
//			$pdfarrs = array();
//			$pdfarrs2 = array();
//			$xmlpath = JPATH_COMPONENT . "/models/forms/";
//			foreach ($plans as $plan) {
//				// add plan data to the data object
//				$data->plan_name = $plan->plan_name;
//				$data->plan_par = "nonpar";
//				$data->price = "HK$" . $plan->price;
//				$data->namesurfix = "_" . JHtml::_('date', time(), 'dMYHis') . ".pdf";
//				$data->policy_number = $plan->policy_number;
//
//				//broker info
//				$data->broker_code = "89192";
//				$data->broker_name = "InsureDIY Limited";
//				$data->broker_rep = "Choo Oi San";
//				$data->broker_ph = "3975 2896";
//
//				switch (strtolower(substr($plan->plan_index_code, 0, 3))) {
//					case 'aia':
//						$result = InsureDIYHelper::generateAIA($xmlpath, $data);
//						$pdfarrs[$plan->plan_index_code] = $result;
//						$data->namesurfix = "_" . JHtml::_('date', time(), 'dMYHis') . "2.pdf";
//
//						$result = InsureDIYHelper::generateAIA($xmlpath, $data, FALSE);
//						$pdfarrs2[$plan->plan_index_code] = $result;
//						break;
//					case 'sun':
//						$result = InsureDIYHelper::generateSUN($xmlpath, $data);
//						$pdfarrs[$plan->plan_index_code] = $result;
//						$data->namesurfix = "_" . JHtml::_('date', time(), 'dMYHis') . "2.pdf";
//						$result = InsureDIYHelper::generateSUN($xmlpath, $data, FALSE);
//						$pdfarrs2[$plan->plan_index_code] = $result;
//						break;
//					default:
//						break;
//				}
//			}
//
//
//			$model->savePDFPaths($id, $pdfarrs);
//			$model->savePDFPaths($id, $pdfarrs2, FALSE);
//		}
//		MyUri::redirect("index.php?option=com_insurediylife&view=quotation&layout=edit&id=" . $id . "&activetab=files");
//	}

	public function generatepdf() {
		$app = JFactory::getApplication();
		$id = $app->input->get("id", FALSE);
		$model = $this->getModel();
		if ($id && MyHelper::is_admin()) {
			$plans = $model->getPlans($id);
			$pdfData = $model->getPdfData($id);

			$pdfarrs = array();
			$pdfarrs2 = array();

			$hasLargeSum = $model->checkSumInsuredQuotationTotal($id) >= 5000000;

			JLoader::import('joomla.filesystem.folder');
			JLoader::import('joomla.filesystem.file');
			$userPath = MyHelper::getDeepPath(LIFE_PDF_SAVE_PATH, $pdfData['user_id']);

			foreach ($plans as $plan) {
				$forms = InsureDIYHelper::getCompanyForms($plan->insurer_code);
				if (!$forms['life_generate_pdf']) {
					continue;
				}
				$gOrigin = (isset($forms['life_generic']) && strlen($forms['life_generic'])) ? JPATH_SITE . DS . $forms['life_generic'] : "";

				if (JFile::exists($gOrigin)) {
					$filename = InsureDIYHelper::generateFileName($forms['life_generic']);
					$dest = JPATH_SITE . DS . $userPath . $filename;
					$result = InsureDIYHelper::generatePdfForm($gOrigin, $dest, $pdfData);
					$pdfarrs[$plan->plan_index_code] = $userPath . $filename;;
					$pdfarrs2[$plan->plan_index_code] = $userPath . $filename;;
				}

				if (!$hasLargeSum) {
					continue;
				}
				$lsOrigin = (isset($forms['life_large_sum']) && strlen($forms['life_large_sum'])) ? $forms['life_large_sum'] : "";
				if (JFile::exists($lsOrigin)) {
					$filename = InsureDIYHelper::generateFileName($forms['life_large_sum']);
					$dest = JPATH_SITE . DS . $userPath . DS . $filename;

					$result = InsureDIYHelper::generatePdfForm($lsOrigin, $dest, $pdfData);
					$pdfarrs[$plan->plan_index_code] = $userPath . $filename;;
					$pdfarrs2[$plan->plan_index_code] = $userPath . $filename;;
				}

				$exOrigin = (isset($forms['life_extra_info']) && strlen($forms['life_extra_info'])) ? $forms['life_extra_info'] : "";
				if (JFile::exists($exOrigin)) {
					$filename = InsureDIYHelper::generateFileName($forms['life_extra_info']);
					$dest = JPATH_SITE . DS . $userPath . DS . $filename;

					$result = InsureDIYHelper::generatePdfForm($exOrigin, $dest, $pdfData);
					$pdfarrs[$plan->plan_index_code] = $userPath . $filename;;
					$pdfarrs2[$plan->plan_index_code] = $userPath . $filename;;
				}
			}
			$model->savePDFPaths($id, $pdfarrs);
			$model->savePDFPaths($id, $pdfarrs2, FALSE);
		}
		MyUri::redirect("index.php?option=com_insurediylife&view=quotation&layout=edit&id=" . $id . "&activetab=files");
	}

}
