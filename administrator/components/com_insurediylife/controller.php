<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_insurediylife
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Cedele Depot Controller
 *
 * @package     Joomla.Administrator
 * @subpackage  com_insurediylife
 * @since       1.5
 */
class InsureDIYLifeController extends JControllerLegacy {

	protected $default_view = 'quotations';

	/**
	 * Method to display a view.
	 *
	 * @param   boolean			$cachable	If true, the view output will be cached
	 * @param   array  $urlparams	An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return  JController		This object to support chaining.
	 * @since   1.5
	 */
	public function display($cachable = false, $urlparams = false) {
		global $reqView, $reqLayout;
		switch ($reqView) {
			case 'quotation':
				$xtraModel = 'existinginsurance';
				break;
		}
	
		if (($view = $this->getView($reqView, 'html'))) {
			$document = JFactory::getDocument();
			$model = $this->getModel((!empty($model)) ? $model : $reqView);

			if (isset($xtraModel) && $xtraModel) {
				if (is_array($xtraModel)) {
					foreach ($xtraModel as $x) {
						$view->setModel($this->getModel($x), FALSE);
					}
				} else {
					$view->setModel($this->getModel($xtraModel), FALSE);
				}
			}

			// Push the model into the view (as default)
			$view->setModel($model, TRUE);
			$view->setLayout($reqLayout);
			$view->assignRef('document', $document);
		}

		parent::display();

		return $this;
	}

}
