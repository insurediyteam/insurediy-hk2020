<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_InsureDIYLife
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * View class for a list of InsureDIYLife.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_InsureDIYLife
 * @since       1.5
 */
class InsureDIYLifeViewPlans extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;

	/**
	 * Display the view
	 *
	 * @return  void
	 */
	public function display($tpl = null)
	{
		$this->state		= $this->get('State');
		$this->items		= $this->get('Items');
		$this->pagination	= $this->get('Pagination');

		InsureDIYLifeHelper::addSubmenu('plans');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar()
	{
		require_once JPATH_COMPONENT.'/helpers/insurediylife.php';

		$state	= $this->get('State');
		$canDo	= InsureDIYLifeHelper::getActions($state->get('filter.category_id'));
		$user	= JFactory::getUser();
		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');

		JToolbarHelper::title(JText::_('COM_INSUREDIYLIFE_MANAGER_PLANS'), 'insurediylife.png');
		
		/*if ($canDo->get('core.create'))
		{
			JToolbarHelper::addNew('plan.add');
		}
		*/

		if ($canDo->get('core.edit'))
		{
			JToolbarHelper::editList('plan.edit');
		}
		
		
		if ($canDo->get('core.edit.state')) {

			JToolbarHelper::publish('plans.publish', 'JTOOLBAR_PUBLISH', true);
			JToolbarHelper::unpublish('plans.unpublish', 'JTOOLBAR_UNPUBLISH', true);

			//JToolbarHelper::archiveList('plan.archive');
			//JToolbarHelper::checkin('plan.checkin');
		}
		
		/*if ($state->get('filter.state') == -2 && $canDo->get('core.delete'))
		{
			JToolbarHelper::deleteList('', 'plan.delete', 'JTOOLBAR_EMPTY_TRASH');
		} elseif ($canDo->get('core.edit.state'))
		{
			JToolbarHelper::trash('plans.trash');
		}
		*/
		
	
		// Add a batch button
		/*if ($canDo->get('core.edit'))
		{
			JHtml::_('bootstrap.modal', 'collapseModal');
			$title = JText::_('JTOOLBAR_BATCH');
			$dhtml = "<button data-toggle=\"modal\" data-target=\"#collapseModal\" class=\"btn btn-small\">
						<i class=\"icon-checkbox-partial\" title=\"$title\"></i>
						$title</button>";
			$bar->appendButton('Custom', $dhtml, 'batch');
		}
		*/
		
		if ($canDo->get('core.admin'))
		{
			JToolbarHelper::preferences('com_insurediylife');
		}

		JHtmlSidebar::setAction('index.php?option=com_insurediylife&view=quotations');
		JHtmlSidebar::setAction('index.php?option=com_insurediylife&view=genders');
		JHtmlSidebar::setAction('index.php?option=com_insurediylife&view=plans');
		JHtmlSidebar::setAction('index.php?option=com_insurediylife&view=csvexport');
		JHtmlSidebar::setAction('index.php?option=com_insurediylife&view=csvimport');
		
		JHtmlSidebar::addFilter(
			JText::_('JOPTION_SELECT_PUBLISHED'),
			'filter_state',
			JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), 'value', 'text', $this->state->get('filter.state'), true)
		);
		
		JHtmlSidebar::addFilter(
			JText::_('JOPTION_SELECT_INSURER_CODE'),
			'filter_insurer_code',
			JHtml::_('select.options', InsureDIYLifeHelper::getInsuranceCompanyOptions(), 'value', 'text', $this->state->get('filter.insurer_code'), true)
		);
		
		
		JHtmlSidebar::addFilter(
			JText::_('JOPTION_SELECT_YEAR_CODE'),
			'filter_year_code',
			JHtml::_('select.options', InsureDIYLifeHelper::getYearPlanOptions(), 'value', 'text', $this->state->get('filter.year_code'), true)
		);

		JHtmlSidebar::addFilter(
			JText::_('JOPTION_SELECT_IS_SMOKING'),
			'filter_is_smoking',
			JHtml::_('select.options', InsureDIYLifeHelper::getYesNoOptions(), 'value', 'text', $this->state->get('filter.is_smoking'), true)
		);
		
		JHtmlSidebar::addFilter(
			JText::_('JOPTION_SELECT_GENDER'),
			'filter_gender',
			JHtml::_('select.options', InsureDIYLifeHelper::getGenderOptions(), 'value', 'text', $this->state->get('filter.gender'), true)
		);
		
		JHtmlSidebar::addFilter(
			JText::_('JOPTION_SELECT_AGE'),
			'filter_age',
			JHtml::_('select.options', InsureDIYLifeHelper::getAgeOptions(), 'value', 'text', $this->state->get('filter.age'), true)
		);
		
		JHtmlSidebar::addFilter(
			JText::_('JOPTION_SELECT_SUM_INSURED'),
			'filter_sum_insured',
			JHtml::_('select.options', InsureDIYLifeHelper::getSumInsuredOptions(), 'value', 'text', $this->state->get('filter.sum_insured'), true)
		);
		
		JHtmlSidebar::addFilter(
			JText::_('JOPTION_SELECT_RENEWABLE'),
			'filter_renewable',
			JHtml::_('select.options', InsureDIYLifeHelper::getYesNoOptions(), 'value', 'text', $this->state->get('filter.renewable'), true)
		);
		
		JHtmlSidebar::addFilter(
			JText::_('JOPTION_SELECT_HAS_TERMINAL_ILLNESS_BENEFIT'),
			'filter_terminal_illness_benefit',
			JHtml::_('select.options', InsureDIYLifeHelper::getYesNoOptions(), 'value', 'text', $this->state->get('filter.terminal_illness_benefit'), true)
		);
		
	}

	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 *
	 * @since   3.0
	 */
	protected function getSortFields()
	{
		return array(
			'p.plan_index_code' => JText::_('JGRID_HEADING_ORDERING'),
			'p.plan_name' => JText::_('COM_INSUREDIYLIFE_HEADER_PLAN_NAME'),
			'p.sum_insured' => JText::_('COM_INSUREDIYLIFE_HEADER_SUM_INSURED'),
			'p.price' => JText::_('COM_INSUREDIYLIFE_HEADER_PRICE')
			
		);
	}
}
