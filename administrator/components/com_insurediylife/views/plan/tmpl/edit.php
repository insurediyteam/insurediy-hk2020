<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_cedeledepots
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'plan.cancel' || document.formvalidator.isValid(document.id('insurediylife-plans-form')))
		{
<?php //echo $this->form->getField('description')->save();     ?>
			Joomla.submitform(task, document.getElementById('insurediylife-plans-form'));
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_insurediylife'); ?>" method="post" name="adminForm" id="insurediylife-plans-form" class="form-validate">
	<div class="row-fluid">
		<div class="span10 form-horizontal">
			<fieldset>
				<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'info_details')); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'info_details', JText::_('COM_INSUREDIYLIFE_PLAN_INFORMATION')); ?>
				<?php /* <div style="margin:10px 0;">Please make sure : <b>Insurance Company</b>, <b>Year Plan</b>, <b>Smoking</b>, <b>Gender</b>, <b>Age</b>, and <b>Sum Insured</b> are not exist in database, or else the form cannot be saved.</div> */ ?>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('plan_name'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('plan_name'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('plan_index_code'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('plan_index_code'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('insurer_code'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('insurer_code'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('year_code'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('year_code'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('is_smoking'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('is_smoking'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('gender'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('gender'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('age'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('age'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('sum_insured'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('sum_insured'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('renewable'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('renewable'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('terminal_illness_benefit'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('terminal_illness_benefit'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('benefits'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('benefits'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('flag_banner'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('flag_banner'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('download_file'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('download_file'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('price'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('price'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('nationality'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('nationality'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('residency'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('residency'); ?></div>
				</div>

				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('id'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('id'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('ref_points'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('ref_points'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('pur_points'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('pur_points'); ?></div>
				</div>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<input type="hidden" name="task" value="" />
				<?php echo JHtml::_('form.token'); ?>

				<?php echo JHtml::_('bootstrap.endTabSet'); ?>
			</fieldset>
		</div>
		<!-- End Weblinks -->
		<!-- Begin Sidebar -->
		<?php //echo JLayoutHelper::render('joomla.edit.details', $this);  ?>
		<!-- End Sidebar -->
	</div>
</form>
