<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_insurediylife
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * View to edit a weblink.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_insurediylife
 * @since       1.5
 */
class InsureDIYLifeViewPNImport extends JViewLegacy {

	protected $state;
	protected $item;
	protected $form;

	/**
	 * Display the view
	 */
	public function display($tpl = null) {
		$this->state = $this->get('State');
		$this->form = $this->get('Form');

		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar() {
		JToolBar::getInstance('toolbar');

		JToolbarHelper::title(JText::_('COM_INSUREDIYLIFE_HEADER_IMPORT_POLICY_NUMBERS'), 'insurediylife.png');

		// If not checked out, can save the item.
		if (empty($this->item->id)) {
			JToolbarHelper::cancel('policynos.cancel');
		} else {
			JToolbarHelper::cancel('policynos.cancel', 'JTOOLBAR_CLOSE');
		}
	}

}
