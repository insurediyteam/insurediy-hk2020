<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_insurediylife
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * View to edit a weblink.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_insurediylife
 * @since       1.5
 */
class InsureDIYLifeViewCSVExport extends JViewLegacy
{
	protected $state;

	protected $item;

	protected $form;

	/**
	 * Display the view
	 */
	public function display($tpl = null)
	{
		$this->state	= $this->get('State');
		$this->item		= $this->get('Item');
		$this->form		= $this->get('Form');
		
		$db = JFactory::getDBO();
		
		/*$query = " SELECT r.*, ur.title AS member_type FROM #__user_transaction_rebate_points AS r "
						.	" LEFT JOIN #__usergroups AS ur ON ur.id = r.user_group_id "
						.	"	WHERE store_code = '".$this->item->code."' ";		
						
			$db->setQuery( $query );
			$this->rebate_points = $db->loadObjectList();
		*/
		
		// Check for errors.
		if (empty($this->item))
		{
			JError::raiseError(500, "No Cedele Depot Item");
			return false;
		}

		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar()
	{
		JFactory::getApplication()->input->set('hidemainmenu', true);

		$user		= JFactory::getUser();
		$userId		= $user->get('id');
		$isNew		= ($this->item->id == 0);

		JToolbarHelper::title(JText::_('COM_INSUREDIYLIFE_MANAGER_PLAN') , 'insurediylife.png');

		// Since it's an existing record, check the edit permission, or fall back to edit own if the owner.
		//if ($canDo->get('core.edit') || ($canDo->get('core.edit.own') && $this->item->created_by == $userId))
		//{
			//JToolbarHelper::apply('plan.apply');
			//JToolbarHelper::save('plan.save');

			// We can save this record, but check the create permission to see if we can return to make a new one.
			/*if ($canDo->get('core.create'))
			{
				JToolbarHelper::save2new('contact.save2new');
			}
			*/
		//}
		// If not checked out, can save the item.
		if (empty($this->item->id))
		{
			JToolbarHelper::cancel('plan.cancel');
		}
		else
		{
			JToolbarHelper::cancel('plan.cancel', 'JTOOLBAR_CLOSE');
		}
		
		/*
		JToolbarHelper::divider();
		JToolbarHelper::help('JHELP_COMPONENTS_CedeleDepotS_LINKS_EDIT');
		*/
	}
}
