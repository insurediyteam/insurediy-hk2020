<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_cedeledepots
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('MyBehavior.jsInsurediy');


$item = $this->item;
$existing_insurances = $item->existing_insurances;
$ownProperties = $item->ownProperties;
$carModels = $item->carModels;
$form = & $this->form;
$ei_form = & $this->ei_form;
$activetab = JFactory::getApplication()->input->get("activetab", "info_details");
$selected_plans = $item->selected_plans;
?>
<script type="text/javascript">
	Joomla.submitbutton = function (task)
	{
		if (task == 'quotation.cancel' || document.formvalidator.isValid(document.id('quotation-form')))
		{
<?php //echo $this->form->getField('description')->save();                                                                                                                                                                                                           ?>
			Joomla.submitform(task, document.getElementById('quotation-form'));
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_insurediylife'); ?>" method="post" name="adminForm" id="quotation-form" class="form-validate">
	<div class="row-fluid">
		<div class="span11 form-horizontal">
			<fieldset>
				<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => $activetab)); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'info_details', JText::_('COM_INSUREDIYLIFE_QUOTATION_BASIC_INFORMATION')); ?>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('unique_order_no'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('unique_order_no'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('cover_amt'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('cover_amt'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('cover_length'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('cover_length'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('gender'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('gender'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('dob'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('dob'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('weight_amt'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('weight_amt'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('height_amt'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('height_amt'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('has_weight_change'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('has_weight_change'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('has_used_tobacco'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('has_used_tobacco'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('has_used_alcohol'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('has_used_alcohol'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('has_used_drugs'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('has_used_drugs'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('has_physicians'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('has_physicians'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('marital_status'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('marital_status'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('occupation'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('occupation'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('job_nature'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('job_nature'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('monthly_income'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('monthly_income'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('country_residence'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('country_residence'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('nationality'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('nationality'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('has_reside_overseas'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('has_reside_overseas'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('has_claimed_insurance'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('has_claimed_insurance'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('has_reinstate_insurance'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('has_reinstate_insurance'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('declare_have_you_replaced'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('declare_have_you_replaced'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('declare_do_you_intend_to_replace'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('declare_do_you_intend_to_replace'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('email'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('email'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('promo_code'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('promo_code'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('referred_by'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('referred_by'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('id'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('id'); ?></div>
				</div>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'gender_question', JText::_('COM_INSUREDIYLIFE_QUOTATION_GENDER_QUESTIONNAIRE')); ?>

				<?php if ($this->gender_question) : ?>
					<?php foreach ($this->gender_question as $r) : ?>
						<div class="control-group">
							<div class="control-label" style="word-wrap:break-word;width:400px;padding:0;margin-right:20px;"><?php echo $r->questionnaire ?></div>
							<div class="controls" style=""><b><?php echo ($r->answer) ? JText::_('JYES') : JText::_('JNO') ?></b></div>
						</div>
					<?php endforeach; ?>
				<?php endif; ?>

				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'contact', JText::_('COM_INSUREDIYLIFE_QUOTATION_CONTACT')); ?>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('contact_firstname'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('contact_firstname'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('contact_lastname'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('contact_lastname'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('contact_contact_no'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('contact_contact_no'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('contact_identity_no'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('contact_identity_no'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('contact_room_no'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('contact_room_no'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('contact_floor_no'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('contact_floor_no'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('contact_block_no'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('contact_block_no'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('contact_building_name'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('contact_building_name'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('contact_street_name'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('contact_street_name'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('contact_district_name'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('contact_district_name'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('contact_postalcode'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('contact_postalcode'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('contact_country'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('contact_country'); ?></div>
				</div>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'employer', JText::_('COM_INSUREDIYLIFE_QUOTATION_EMPLOYER')); ?>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('employer_name'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('employer_name'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('employer_address'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('employer_address'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('employer_job_nature'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('employer_job_nature'); ?></div>
				</div>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'physician', JText::_('COM_INSUREDIYLIFE_QUOTATION_PHYSICIANS')); ?>

				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('physician_name'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('physician_name'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('physician_address'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('physician_address'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('physician_consult_result'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('physician_consult_result'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('physician_consult_reason'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('physician_consult_reason'); ?></div>
				</div>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'bank', JText::_('COM_INSUREDIYLIFE_QUOTATION_BANK')); ?>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('bank_bank_no'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('bank_bank_no'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('bank_branch_no'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('bank_branch_no'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('bank_acct_no'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('bank_acct_no'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('bank_acct_holder_name'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('bank_acct_holder_name'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('bank_id_type'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('bank_id_type'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('bank_id_no'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('bank_id_no'); ?></div>
				</div>

				<?php echo JHtml::_('bootstrap.endTab'); ?>
				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'appointment', JText::_('COM_INSUREDIYLIFE_QUOTATION_APPOINTMENT_DETAIL')); ?>

				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('pre_location_id'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('pre_location_id'); ?></div>
				</div>

				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('meet_date'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('meet_date'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('meet_time'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('meet_time'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('meet_address'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('meet_address'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('meet_contact_no'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('meet_contact_no'); ?></div>
				</div>

				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'files', JText::_('COM_INSUREDIYLIFE_QUOTATION_FILES')); ?>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('file_identity_card'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('file_identity_card'); ?></div>
				</div>

				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('file_proof_address'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('file_proof_address'); ?></div>
				</div>

				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('file_company_financial1'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('file_company_financial1'); ?></div>
				</div>

				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('file_company_financial2'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('file_company_financial2'); ?></div>
				</div>

				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('file_company_financial3'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('file_company_financial3'); ?></div>
				</div>

				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('file_income_tax'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('file_income_tax'); ?></div>
				</div>

				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('file_application'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('file_application'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('file_application2'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('file_application2'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"></div>
					<div class="controls"><a class="btn btn-primary" href="index.php?option=com_insurediylife&task=quotation.generatepdf&id=<?php echo $item->id; ?>"><?php echo JText::_("BTN_GENERATE_PDF"); ?></a></div>
				</div>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'transaction_logs', JText::_('COM_INSUREDIYLIFE_QUOTATION_TRANSACTION_LOGS')); ?>

				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('payment_status'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('payment_status'); ?></div>
				</div>

				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('Amt'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('Amt'); ?></div>
				</div>

				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('PayRef'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('PayRef'); ?></div>
				</div>

				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('TxTime'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('TxTime'); ?></div>
				</div>


				<?php echo JHtml::_('bootstrap.endTab'); ?>


				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'extra_info', JText::_('COM_INSUREDIYLIFE_QUOTATION_EXTRA_INFO')); ?>

				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('other_dependant_no'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('other_dependant_no'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('other_total_income_last_year'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('other_total_income_last_year'); ?></div>
				</div>

				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('other_total_income_year_before'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('other_total_income_year_before'); ?></div>
				</div>

				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('other_total_income_two_years'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('other_total_income_two_years'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('other_when_join_company'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('other_when_join_company'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('other_own_property'); ?></div>
					<div class="controls">
						<div class="pull-left" style="margin-right: 20px;">
							<?php echo $this->form->getInput('other_own_property'); ?>
						</div>
						<div class="pull-left">
							<a href="#propertiesModal" role="button" style="width:65px" class="btn btn-small btn-primary" data-toggle="modal"><?php echo JText::_("BTN_VIEW"); ?></a>
						</div>
						<div class="clear"></div>
					</div>
				</div>

				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('other_own_car'); ?></div>
					<div class="controls">
						<div class="pull-left" style="margin-right: 20px;">
							<?php echo $this->form->getInput('other_own_car'); ?>
						</div>
						<div class="pull-left">
							<a href="#carModelModal" role="button" style="width:65px" class="btn btn-small btn-primary" data-toggle="modal"><?php echo JText::_("BTN_VIEW"); ?></a>
						</div>
						<div class="clear"></div>
					</div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('other_assets_fixed_deposits'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('other_assets_fixed_deposits'); ?></div>
				</div><div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('other_assets_shares'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('other_assets_shares'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('other_assets_others'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('other_assets_others'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('other_assets_other_loans'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('other_assets_other_loans'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('other_ownership_of_company'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('other_ownership_of_company'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('other_ownership_percent'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('other_ownership_percent'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('other_ownership_commencement_biz'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('other_ownership_commencement_biz'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('other_ownership_employees_no'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('other_ownership_employees_no'); ?></div>
				</div>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'existing_insurances', JText::_('COM_INSUREDIYLIFE_QUOTATION_EXISTING_INSURANCES')); ?>

				<?php
				$ei_fields = $form->getFieldset("existing_insurances");
				if ($existing_insurances) {
					foreach ($existing_insurances['insurer_name'] as $k => $v):
						$fid = "existing_insurances" . ($k + 1);
						?>
						<div id="<?php echo $fid; ?>">
							<div class="control-header">
								<h4>Exisiting Insurances <?php echo $k + 1; ?></h4>
							</div>
							<?php
							foreach ($ei_fields as $field):
								$fn = $field->fieldname;
								?>
								<div class="control-group">
									<div class="control-label">
										<?php
										$form->setValue($fn, "", $existing_insurances[$fn][$k]);
										$form->setFieldAttribute($fn, 'id', $fn . "_" . $k, "");
										echo $form->getLabel($fn);
										?>
									</div>
									<div class="controls"><?php echo $form->getInput($fn); ?></div>
								</div>
							<?php endforeach; ?>
							<?php if (FALSE): ?>
								<a href="#deleteModel" role="button" style="width:65px;" class="btn btn-small btn-danger" data-toggle="modal" onclick="javascript:document.deleteForm.ei_id.value = '<?php echo $existing_insurances["id"][$k]; ?>';
													document.deleteForm.task.value = 'quotation.deleteEI'"><?php echo JText::_("BTN_DELETE"); ?></a>
							   <?php endif; ?>
							<input type="hidden" name="ei_id" value="<?php echo $existing_insurances["id"][$k]; ?>" />
							<?php echo JHtml::_('form.token'); ?>

							<a href="#deleteModel" role="button" style="width:65px;" class="btn btn-small btn-danger" data-toggle="modal" onclick="javascript:jsInsurediy.callDeleteForm('deleteForm', '<?php echo $existing_insurances["id"][$k]; ?>', 'quotation.deleteEI');"><?php echo JText::_("BTN_DELETE"); ?></a>
							<a href="#" role="button" style="width: 65px;" class="btn btn-small btn-success" onclick="javascript:jsInsurediy.saveWithAjax('<?php echo $fid; ?>', 'quotation.saveEI', 'com_insurediylife');
											return false;"><?php echo JText::_("BTN_SAVE"); ?></a>
						</div>
						<hr>
						<?php
					endforeach;
				}
				?>
				<div id="eiAddForm" name="eiAddForm">
					<?php foreach ($ei_form->getFieldset("existing_insurances") as $field): ?>
						<div class="control-group">
							<div class="control-label"><?php echo $field->label; ?></div>
							<div class="controls"><?php echo $field->input; ?></div>
						</div>
					<?php endforeach; ?>
					<input type="hidden" name="quotation_id" value="<?php echo $item->id; ?>" />
					<?php echo JHtml::_('form.token'); ?>
					<a href="#" role="button" style="width:65px" class="btn btn-small btn-success" onclick="javascript:jsInsurediy.addWithAjax('eiAddForm', 'quotation.addEI', 'com_insurediylife');
							return false;"><?php echo JText::_("BTN_ADD"); ?></a>
				</div>
				<?php if (FALSE): ?>
					<a href="#addModal" role="button" style="width:65px" class="btn btn-small btn-success" data-toggle="modal"><?php echo JText::_("BTN_ADD"); ?></a>
				<?php endif; ?>

				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'plans', JText::_('COM_INSUREDIYLIFE_QUOTATION_PLANS_SELECTED')); ?>
				<div class="span8">
					<?php if (count($selected_plans) > 0): ?>
						<table class="table table-hover">
							<thead>
								<tr>
									<th>#</th>
									<th><?php echo JText::_("PLAN_TB_PLAN_NAME"); ?></th>
									<th><?php echo JText::_("PLAN_TB_PLAN_INDEX_CODE"); ?></th>
									<th><?php echo JText::_("PLAN_TB_SUM_INSURED"); ?></th>
									<th><?php echo JText::_("PLAN_TB_PRICE"); ?></th>
								</tr>
							</thead>
							<?php foreach ($selected_plans as $k => $selected_plan): ?>
								<tr>
									<td>
										<?php echo $k + 1 ?>
									</td>
									<td>
										<?php echo $selected_plan['plan_name']; ?>
									</td>
									<td>
										<?php echo $selected_plan['plan_index_code']; ?>
									</td>
									<td>
										<?php echo $selected_plan['sum_insured']; ?>
									</td>
									<td>
										<?php echo $selected_plan['price']; ?>
									</td>
								</tr>
							<?php endforeach; ?>
						</table>
					<?php else: ?>
						<?php echo "No plans selected"; ?>
					<?php endif; ?>

				</div>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<input type="hidden" name="task" value="" />
				<?php echo JHtml::_('form.token'); ?>
				<?php echo JHtml::_('bootstrap.endTabSet'); ?>
			</fieldset>
		</div>
		<!-- End Weblinks -->
		<!-- Begin Sidebar -->
		<?php //echo JLayoutHelper::render('joomla.edit.details', $this);                  ?>
		<!-- End Sidebar -->
	</div>
</form>
<div id="propertiesModal" class="modal hide fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3><?php echo JText::_("COM_INSUREDIYLIFE_PROPERTIES_HEADER"); ?></h3>
	</div>
	<div class="modal-body">
		<div id="extra_info" class="form-horizontal">
			<?php if (FALSE): ?>
				<?php
				$op_fields = $form->getFieldset("properties");
				if ($ownProperties) {
					foreach ($ownProperties['id'] as $k => $v):
						?>
						<div class="control-header">
							<h4>Properties <?php echo $k + 1; ?></h4>
						</div>
						<?php
						foreach ($op_fields as $field):
							$fn = $field->fieldname;
							?>
							<div class="control-group">
								<div class="control-label">
									<?php
									$form->setValue($fn, "", $ownProperties[$fn][$k]);
									$form->setFieldAttribute($fn, 'id', $fn . "_" . $k, "");
									echo $form->getLabel($fn);
									?>
								</div>
								<div class="controls"><?php echo $form->getInput($fn); ?></div>
							</div>
						<?php endforeach; ?>
						<?php if (FALSE): ?>
							<a href="#deleteModel" role="button" style="width:65px" class="btn btn-small btn-danger" data-toggle="modal" onclick="javascript:document.deleteForm.ei_id.value = '<?php echo $ownProperties["id"][$k]; ?>';
													document.deleteForm.task.value = 'quotation.deleteOP'"><?php echo JText::_("BTN_DELETE"); ?></a>
						   <?php endif; ?>
						<a href="#deleteModel" role="button" style="width:65px" class="btn btn-small btn-danger" data-toggle="modal" onclick="javascript:jsInsurediy.callDeleteForm('deleteForm', '<?php echo $ownProperties["id"][$k]; ?>', 'quotation.deleteOP');"><?php echo JText::_("BTN_DELETE"); ?></a>
						<hr>
						<?php
					endforeach;
				}
				?>
			<?php endif; ?>
			<?php if (count($ownProperties) > 0): ?>
				<table class="table table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Property Purchase</th>
							<th>Property Price</th>
							<th>Property Mortage</th>
							<th>Property Value</th>
						</tr>
					</thead>
					<?php foreach ($ownProperties as $k => $v): ?>
						<tr>
							<td><?php echo $k + 1; ?></td>
							<td><?php echo $v['property_purchase']; ?></td>
							<td><?php echo $v['property_price']; ?></td>
							<td><?php echo $v['property_mortgage']; ?></td>
							<td><?php echo $v['property_value']; ?></td>
						</tr>
					<?php endforeach; ?>
				</table>
			<?php else: ?>
				No properties provided.
			<?php endif; ?>


		</div>
	</div>
	<div class="modal-footer center">
		<a href="#" data-dismiss="modal" class="btn btn-small"><?php echo JText::_("BTN_CLOSE"); ?></a>
	</div>
</div>

<div id="carModelModal" class="modal hide fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3><?php echo JText::_("COM_INSUREDIYLIFE_CAR_MODEL_HEADER"); ?></h3>
	</div>
	<div class="modal-body">
		<div id="extra_info" class="form-horizontal">
			<?php if (count($carModels) > 0): ?>
				<table class="table table-hover">
					<thead>
						<tr>
							<th>#</th>
							<th>Car Model</th>
						</tr>
					</thead>
					<?php foreach ($carModels as $k => $v): ?>
						<tr>
							<td><?php echo $k + 1; ?></td>
							<td><?php echo $v['car_model']; ?></td>
						</tr>
					<?php endforeach; ?>
				</table>
			<?php else: ?>
				No Car Model provided.
			<?php endif; ?>
		</div>
	</div>
	<div class="modal-footer center">
		<a href="#" data-dismiss="modal" class="btn btn-small"><?php echo JText::_("BTN_CLOSE"); ?></a>
	</div>
</div>

<div id="addModal" class="modal hide fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3><?php echo JText::_("COM_INSUREDIYLIFE_EXISTING_INSURANCES_ADD_HEADER"); ?></h3>
	</div>
	<form style="margin: 0;" action="<?php echo JRoute::_('index.php?option=com_insurediylife'); ?>" method="post" name="addForm" id="addForm">
		<div class="modal-body">
			<?php foreach ($ei_form->getFieldset("existing_insurances") as $field): ?>
				<div class="control-group">
					<div class="control-label"><?php echo $field->label; ?></div>
					<div class="controls"><?php echo $field->input; ?></div>
				</div>
			<?php endforeach; ?>
		</div>
		<div class="modal-footer center">
			<input type="hidden" name="task" value="quotation.addEI" />
			<input type="hidden" name="quotation_id" value="<?php echo $item->id; ?>" />
			<?php echo JHtml::_('form.token'); ?>
			<a href="#" data-dismiss="modal" class="btn btn-small"><?php echo JText::_("BTN_CANCEL"); ?></a>
			<button type="submit" class="btn btn-small btn-success"><?php echo JText::_("BTN_ADD"); ?></button>
		</div>
	</form>
</div>
<div id="deleteModel" class="modal hide fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3><?php echo JText::_("COM_INSUREDIYLIFE_WARNING_HEADER"); ?></h3>
	</div>
	<div class="modal-body">
		<?php echo JText::_("COM_INSUREDIYLIFE_EXISTING_INSURANCES_DELETE_WARNING"); ?>
	</div>
	<div class="modal-footer center">
		<form style="margin: 0;" action="<?php echo JRoute::_('index.php?option=com_insurediylife'); ?>" method="post" name="deleteForm" id="deleteForm">
			<input type="hidden" id="task" name="task" value="" />
			<input type="hidden" id="element_id" name="element_id" value="" />
			<input type="hidden" name="quotation_id" value="<?php echo $item->id; ?>" />
			<?php echo JHtml::_('form.token'); ?>
			<a href="#" data-dismiss="modal" class="btn btn-small"><?php echo JText::_("BTN_CANCEL"); ?></a>
			<button type="submit" class="btn btn-small btn-danger"><?php echo JText::_("BTN_DELETE"); ?></button>
		</form>
	</div>
</div>
