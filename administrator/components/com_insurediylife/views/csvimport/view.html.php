<?php

defined('_JEXEC') or die;

class InsureDIYLifeViewCSVImport extends JViewLegacy {

	protected $state;
	protected $item;
	protected $form;

	public function display($tpl = null) {
		$this->state = $this->get('State');
		$this->form = $this->get('Form');

		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar() {
		JFactory::getApplication()->input->set('hidemainmenu', true);

		JToolbarHelper::title(JText::_('COM_INSUREDIYLIFE_CSV_IMPORT'), 'insurediylife.png');

		// If not checked out, can save the item.
		if (empty($this->item->id)) {
			JToolbarHelper::cancel('plan.cancel');
		} else {
			JToolbarHelper::cancel('plan.cancel', 'JTOOLBAR_CLOSE');
		}
	}

}
