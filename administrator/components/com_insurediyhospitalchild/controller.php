<?php

defined('_JEXEC') or die;

class InsureDIYHospitalChildController extends JControllerLegacy {

	protected $default_view = 'quotations';

	public function display($cachable = false, $urlparams = false) {
		global $reqView, $reqLayout;
		switch ($reqView) {
			case 'quotation':
				$xtraModel = 'existinginsurance';
				break;
		}

		if (($view = $this->getView($reqView, 'html'))) {
			$document = JFactory::getDocument();
			$model = $this->getModel((!empty($model)) ? $model : $reqView);

			if (isset($xtraModel) && $xtraModel) {
				if (is_array($xtraModel)) {
					foreach ($xtraModel as $x) {
						$view->setModel($this->getModel($x), FALSE);
					}
				} else {
					$view->setModel($this->getModel($xtraModel), FALSE);
				}
			}

			// Push the model into the view (as default)
			$view->setModel($model, TRUE);
			$view->setLayout($reqLayout);
			$view->assignRef('document', $document);
		}

		parent::display();

		return $this;
	}

}
