<?php
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'plan.cancel' || document.formvalidator.isValid(document.id('insurediyhospitalchild-plans-form')))
		{
			Joomla.submitform(task, document.getElementById('insurediyhospitalchild-plans-form'));
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_insurediyhospitalchild'); ?>" method="post" name="adminForm" id="insurediyhospitalchild-plans-form" class="form-validate">
	<div class="row-fluid">
		<div class="span10 form-horizontal">
			<fieldset>
				<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'info_details')); ?>
				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'info_details', JText::_('COM_INSUREDIYHOSPITALCHILD_PLAN_INFORMATION')); ?>
				<?php /* <div style="margin:10px 0;">Please make sure : <b>Insurance Company</b>, <b>Year Plan</b>, <b>Smoking</b>, <b>Gender</b>, <b>Age</b>, and <b>Sum Insured</b> are not exist in database, or else the form cannot be saved.</div> */ ?>
				<?php foreach ($this->form->getFieldset("basic") as $field): ?>
					<div class="control-group">
						<div class="control-label"><?php echo $field->label; ?></div>
						<div class="controls"><?php echo $field->input; ?></div>
					</div>
				<?php endforeach; ?>

				<?php echo JHtml::_('bootstrap.endTab'); ?>
				<input type="hidden" name="task" value="" />
				<?php echo JHtml::_('form.token'); ?>

				<?php echo JHtml::_('bootstrap.endTabSet'); ?>
			</fieldset>
		</div>
		<!-- End Weblinks -->
		<!-- Begin Sidebar -->
		<?php //echo JLayoutHelper::render('joomla.edit.details', $this);   ?>
		<!-- End Sidebar -->
	</div>
</form>
