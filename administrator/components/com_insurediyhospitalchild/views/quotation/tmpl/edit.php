<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_cedeledepots
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('MyBehavior.jsInsurediy');


$item = $this->item;
$existing_insurances = $item->existing_insurances;
$form = & $this->form;
$ei_form = & $this->ei_form;
$activetab = JFactory::getApplication()->input->get("activetab", "info_details");
$selected_plan = $item->selected_plan;
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'quotation.cancel' || document.formvalidator.isValid(document.id('quotation-form')))
		{
<?php //echo $this->form->getField('description')->save();                                                                                                                                                                                                                              ?>
			Joomla.submitform(task, document.getElementById('quotation-form'));
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_insurediyhospitalchild'); ?>" method="post" name="adminForm" id="quotation-form" class="form-validate">
	<div class="row-fluid">
		<div class="span11 form-horizontal">
			<fieldset>
				<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => $activetab)); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'info_details', JText::_('COM_INSUREDIYHOSPITALCHILD_QUOTATION_BASIC_INFORMATION')); ?>
				<?php foreach ($this->form->getFieldset("quote-details") as $field): ?>
					<div class="control-group">
						<div class="control-label"><?php echo $field->label; ?></div>
						<div class="controls"><?php echo $field->input; ?></div>
					</div>
				<?php endforeach; ?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'gender_question', JText::_('COM_INSUREDIYHOSPITALCHILD_QUOTATION_GENDER_QUESTIONNAIRE')); ?>

				<?php if ($this->questionnaires) : ?>
					<?php foreach ($this->questionnaires as $r) : ?>
						<div class="control-group">
							<div class="control-label" style="word-wrap:break-word;width:400px;padding:0;margin-right:20px;"><?php echo $r->questionnaire ?></div>
							<div class="controls" style=""><b><?php echo ($r->answer) ? JText::_('JYES') : JText::_('JNO') ?></b></div>
						</div>
					<?php endforeach; ?>
				<?php endif; ?>

				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'contact', JText::_('COM_INSUREDIYHOSPITALCHILD_QUOTATION_CONTACT')); ?>
				<fieldset>
					<legend>Parent's Detail</legend>
					<?php foreach ($this->form->getFieldset("contact-details") as $field): ?>
						<div class="control-group">
							<div class="control-label"><?php echo $field->label; ?></div>
							<div class="controls"><?php echo $field->input; ?></div>
						</div>
					<?php endforeach; ?>
				</fieldset>
				<fieldset>
					<legend>Child's Detail</legend>
					<?php foreach ($this->form->getFieldset("child-details") as $field): ?>
						<div class="control-group">
							<div class="control-label"><?php echo $field->label; ?></div>
							<div class="controls"><?php echo $field->input; ?></div>
						</div>
					<?php endforeach; ?>
				</fieldset>

				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'employer', JText::_('COM_INSUREDIYHOSPITALCHILD_QUOTATION_EMPLOYER')); ?>
				<?php foreach ($this->form->getFieldset("employer-details") as $field): ?>
					<div class="control-group">
						<div class="control-label"><?php echo $field->label; ?></div>
						<div class="controls"><?php echo $field->input; ?></div>
					</div>
				<?php endforeach; ?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'physician', JText::_('COM_INSUREDIYHOSPITALCHILD_QUOTATION_PHYSICIANS')); ?>
				<?php foreach ($this->form->getFieldset("physician-details") as $field): ?>
					<div class="control-group">
						<div class="control-label"><?php echo $field->label; ?></div>
						<div class="controls"><?php echo $field->input; ?></div>
					</div>
				<?php endforeach; ?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'bank', JText::_('COM_INSUREDIYHOSPITALCHILD_QUOTATION_BANK')); ?>
				<?php foreach ($this->form->getFieldset("bank-details") as $field): ?>
					<div class="control-group">
						<div class="control-label"><?php echo $field->label; ?></div>
						<div class="controls"><?php echo $field->input; ?></div>
					</div>
				<?php endforeach; ?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>
				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'appointment', JText::_('COM_INSUREDIYHOSPITALCHILD_QUOTATION_APPOINTMENT_DETAIL')); ?>
				<?php foreach ($this->form->getFieldset("appointment-details") as $field): ?>
					<div class="control-group">
						<div class="control-label"><?php echo $field->label; ?></div>
						<div class="controls"><?php echo $field->input; ?></div>
					</div>
				<?php endforeach; ?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'files', JText::_('COM_INSUREDIYHOSPITALCHILD_QUOTATION_FILES')); ?>
				<?php foreach ($this->form->getFieldset("file-details") as $field): ?>
					<div class="control-group">
						<div class="control-label"><?php echo $field->label; ?></div>
						<div class="controls"><?php echo $field->input; ?></div>
					</div>
				<?php endforeach; ?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'transaction_logs', JText::_('COM_INSUREDIYHOSPITALCHILD_QUOTATION_TRANSACTION_LOGS')); ?>
				<?php foreach ($this->form->getFieldset("transaction-details") as $field): ?>
					<div class="control-group">
						<div class="control-label"><?php echo $field->label; ?></div>
						<div class="controls"><?php echo $field->input; ?></div>
					</div>
				<?php endforeach; ?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'existing_insurances', JText::_('COM_INSUREDIYHOSPITALCHILD_QUOTATION_EXISTING_INSURANCES')); ?>
				<?php
				$ei_fields = $form->getFieldset("existing_insurances");
				if ($existing_insurances) {
					foreach ($existing_insurances['insurer_name'] as $k => $v):
						$fid = "existing_insurances" . ($k + 1);
						?>
						<div id="<?php echo $fid; ?>">
							<div class="control-header">
								<h4>Exisiting Insurances <?php echo $k + 1; ?></h4>
							</div>
							<?php
							foreach ($ei_fields as $field):
								$fn = $field->fieldname;
								?>
								<div class="control-group">
									<div class="control-label">
										<?php
										$form->setValue($fn, "", $existing_insurances[$fn][$k]);
										$form->setFieldAttribute($fn, 'id', $fn . "_" . $k, "");
										echo $form->getLabel($fn);
										?>
									</div>
									<div class="controls"><?php echo $form->getInput($fn); ?></div>
								</div>
							<?php endforeach; ?>
							<?php if (FALSE): ?>
								<a href="#deleteModel" role="button" style="width:65px;" class="btn btn-small btn-danger" data-toggle="modal" onclick="javascript:document.deleteForm.ei_id.value = '<?php echo $existing_insurances["id"][$k]; ?>';
													document.deleteForm.task.value = 'quotation.deleteEI'"><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_BUTTON_DELETE"); ?></a>
							   <?php endif; ?>
							<input type="hidden" name="ei_id" value="<?php echo $existing_insurances["id"][$k]; ?>" />
							<?php echo JHtml::_('form.token'); ?>

							<a href="#deleteModel" role="button" style="width:65px;" class="btn btn-small btn-danger" data-toggle="modal" onclick="javascript:jsInsurediy.callDeleteForm('deleteForm', '<?php echo $existing_insurances["id"][$k]; ?>', 'quotation.deleteEI');"><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_BUTTON_DELETE"); ?></a>
							<a href="#" role="button" style="width: 65px;" class="btn btn-small btn-success" onclick="javascript:jsInsurediy.saveWithAjax('<?php echo $fid; ?>', 'quotation.saveEI', 'com_insurediyhospitalchild');
											return false;"><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_BUTTON_SAVE"); ?></a>
						</div>
						<hr>
						<?php
					endforeach;
				}
				?>
				<div id="eiAddForm" name="eiAddForm">
					<?php foreach ($ei_form->getFieldset("existing_insurances") as $field): ?>
						<div class="control-group">
							<div class="control-label"><?php echo $field->label; ?></div>
							<div class="controls"><?php echo $field->input; ?></div>
						</div>
					<?php endforeach; ?>
					<input type="hidden" name="quotation_id" value="<?php echo $item->id; ?>" />
					<?php echo JHtml::_('form.token'); ?>
					<a href="#" role="button" style="width:65px" class="btn btn-small btn-success" onclick="javascript:jsInsurediy.addWithAjax('eiAddForm', 'quotation.addEI', 'com_insurediyhospitalchild');
							return false;"><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_BUTTON_ADD"); ?></a>
				</div>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'plan', JText::_('COM_INSUREDIYHOSPITALCHILD_QUOTATION_PLAN_SELECTED')); ?>
				<?php foreach ($this->form->getFieldset("plan-details") as $field): ?>
					<div class="control-group">
						<div class="control-label"><?php echo $field->label; ?></div>
						<div class="controls"><?php echo $field->input; ?></div>
					</div>
				<?php endforeach; ?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<input type="hidden" name="task" value="" />
				<?php echo JHtml::_('form.token'); ?>
				<?php echo JHtml::_('bootstrap.endTabSet'); ?>
			</fieldset>
		</div>
	</div>
</form>

<div id="deleteModel" class="modal hide fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_WARNING_HEADER"); ?></h3>
	</div>
	<div class="modal-body">
		<?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_EXISTING_INSURANCES_DELETE_WARNING"); ?>
	</div>
	<div class="modal-footer center">
		<form style="margin: 0;" action="<?php echo JRoute::_('index.php?option=com_insurediyhospitalchild'); ?>" method="post" name="deleteForm" id="deleteForm">
			<input type="hidden" id="task" name="task" value="" />
			<input type="hidden" id="element_id" name="element_id" value="" />
			<input type="hidden" name="quotation_id" value="<?php echo $item->id; ?>" />
			<?php echo JHtml::_('form.token'); ?>
			<a href="#" data-dismiss="modal" class="btn btn-small"><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_BUTTON_CANCEL"); ?></a>
			<button type="submit" class="btn btn-small btn-danger"><?php echo JText::_("COM_INSUREDIYHOSPITALCHILD_BUTTON_DELETE"); ?></button>
		</form>
	</div>
</div>
