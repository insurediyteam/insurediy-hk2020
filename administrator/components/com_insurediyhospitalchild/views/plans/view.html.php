<?php

defined('_JEXEC') or die;

class InsureDIYHospitalChildViewPlans extends JViewLegacy {

	protected $items;
	protected $pagination;
	protected $state;

	/**
	 * Display the view
	 *
	 * @return  void
	 */
	public function display($tpl = null) {
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');

		InsureDIYHospitalChildHelper::addSubmenu('plans');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar() {
		require_once JPATH_COMPONENT . '/helpers/insurediyhospitalchild.php';

		$state = $this->get('State');
		$canDo = InsureDIYHospitalChildHelper::getActions($state->get('filter.category_id'));
		$user = JFactory::getUser();
		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');

		JToolbarHelper::title(JText::_('COM_INSUREDIYHOSPITALCHILD_MANAGER_PLANS'), 'insurediyhospitalchild.png');

		if ($canDo->get('core.edit')) {
			JToolbarHelper::editList('plan.edit');
		}


		if ($canDo->get('core.edit.state')) {
			JToolbarHelper::publish('plans.publish', 'JTOOLBAR_PUBLISH', true);
			JToolbarHelper::unpublish('plans.unpublish', 'JTOOLBAR_UNPUBLISH', true);
		}

		if ($canDo->get('core.admin')) {
			JToolbarHelper::preferences('com_insurediyhospitalchild');
		}

		JHtmlSidebar::setAction('index.php?option=com_insurediyhospitalchild&view=quotations');
		JHtmlSidebar::setAction('index.php?option=com_insurediyhospitalchild&view=genders');
		JHtmlSidebar::setAction('index.php?option=com_insurediyhospitalchild&view=plans');
		JHtmlSidebar::setAction('index.php?option=com_insurediyhospitalchild&view=csvexport');
		JHtmlSidebar::setAction('index.php?option=com_insurediyhospitalchild&view=csvimport');

		JHtmlSidebar::addFilter(
				JText::_('JOPTION_SELECT_PUBLISHED'), 'filter_state', JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), 'value', 'text', $this->state->get('filter.state'), true)
		);

		JHtmlSidebar::addFilter(
				JText::_('JOPTION_SELECT_INSURER_CODE'), 'filter_insurer_code', JHtml::_('select.options', InsureDIYHospitalChildHelper::getInsuranceCompanyOptions(), 'value', 'text', $this->state->get('filter.insurer_code'), true)
		);
	}

	protected function getSortFields() {
		return array(
			'p.plan_index_code' => JText::_('JGRID_HEADING_ORDERING'),
			'p.plan_name' => JText::_('COM_INSUREDIYHOSPITALCHILD_HEADER_PLAN_NAME'),
			'p.sum_insured' => JText::_('COM_INSUREDIYHOSPITALCHILD_HEADER_SUM_INSURED'),
			'p.price' => JText::_('COM_INSUREDIYHOSPITALCHILD_HEADER_PRICE')
		);
	}

}
