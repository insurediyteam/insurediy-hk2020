<?php

defined('_JEXEC') or die;

class InsureDIYHospitalChildModelCSVImport extends JModelAdmin {

	protected $text_prefix = 'COM_INSUREDIYHOSPITALCHILD';

	public function getForm($data = array(), $loadData = true) {
		$form = $this->loadForm('com_insurediyhospitalchild.csvimport', 'csvimport', array('control' => 'jform', 'load_data' => $loadData));

		if (empty($form)) {
			return false;
		}

		return $form;
	}

	public function getTable($type = 'Plan', $prefix = 'InsureDIYHospitalChildTable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}

	public function import_plans() {
		$app = JFactory::getApplication();
		$db = JFactory::getDBO();
//		$cols = array("plan_name", "insurer_code", "cover_amt", "room_type", "premium", "cover_country", "gender", "max_renewal_age", "flag_banner", "maternity_cover", "medical_examination", "waiting_period", "other_benefit", "brochure", "state");
		$cols = array(
			"plan_name",
			"insurer_code",
			"gender",
			"room_type",
			"cover_amt",
			"cover_country",
			"age",
			"premium",
			"cover_type",
			"max_renewal_age",
			"room_n_board",
			"surgical",
			"anaesthetist",
			"operating_theatre",
			"emergency_op",
			"cancer_n_kidney",
			"medical_examination",
			"waiting_period",
			"other_benefit",
			"flag_banner",
			"brochure",
			"nationality",
			"residency",
			"state",
			"ref_points",
			"pur_points"
		);

		$files = $app->input->files->get('jform');
		$file = $files['csvimport'];
		$filename = JFile::makeSafe($file['name']);

		$ext = JFile::getExt($filename);

		if ($ext != 'csv') {
			return false;
		}

		$dest = JPATH_ROOT . '/idy_tmp/' . $filename;
		move_uploaded_file($file['tmp_name'], $dest);
//		JFile::upload($file['tmp_name'], $dest);



		if (($handle = fopen($dest, "r")) !== FALSE) {
			$header = fgetcsv($handle, 1000, ",");
			if (count($header) < 18) {
				return false;
			}
			$query = " TRUNCATE #__insure_hospitalchild_plans ;";
			$db->setQuery($query);
			$db->query();

			$queryData = array();
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				set_time_limit(0);
				// [insurer_code][room_type][cover_amt][age][gender][cover_country][nationality][residency]
				$insurer_code = strlen($data[1]) > 0 ? $data[1] : "XXX";
				$room_type = strlen($data[3]) > 0 ? $data[3] : "0";
				$cover_amt = strlen($data[4]) > 0 ? $data[4] : "0";
				$age = strlen($data[6]) > 0 ? $data[6] : "0";
				$gender = strlen($data[2]) > 0 ? $data[2] : "X";
				$cover_country = strlen($data[5]) > 0 ? $data[5] : "XX";
				$nationality = strlen($data[21]) > 0 ? $data[21] : "XX";
				$residency = strlen($data[22]) > 0 ? $data[22] : "XX";

				$plan_index_code = $insurer_code . $room_type . $cover_amt . $age . $gender . $cover_country . $nationality . $residency;
//				$plan_index_code = $data[1] . $data[3] . $data[4] . $data[6] . $data[2] . $data[5] . $data[21] . $data[22];
				$queryData[] = "('" . $plan_index_code . "', '" . implode("','", $data) . "')";
			}
			$count = count($queryData);
			$perrun = 1000;
			if ($count > $perrun) {
				$times = floor($count / $perrun) + 1;
				for ($i = 0; $i < $times; $i++) {
					$left = $count - ($perrun * $i);
					$length = ($left > $perrun) ? $perrun : $left;
					$tempData = array_slice($queryData, $i * $perrun, $length);
					$query = "INSERT INTO #__insure_hospitalchild_plans (plan_index_code, " . implode(",", $cols) . ") VALUES " . implode(",", $tempData) . ";";
					$db->setQuery($query);
					$db->query();
				}
			} else {
				$query = "INSERT INTO #__insure_hospitalchild_plans (plan_index_code, " . implode(",", $cols) . ") VALUES " . implode(",", $queryData) . ";";
				$db->setQuery($query);
				$db->query();
			}
			fclose($handle);
		}
		unlink($dest);
		return true;
	}

}
