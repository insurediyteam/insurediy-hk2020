<?php

defined('JPATH_PLATFORM') or die;

JFormHelper::loadFieldClass('insurediyhospitalchildlist');

class JFormFieldInsureDIYHospitalChildSQL extends JFormFieldInsureDIYHospitalChildList {

	public $type = 'InsureDIYHospitalChildSQL';
	protected $keyField;
	protected $valueField;
	protected $translate = false;
	protected $query;

	public function __get($name) {
		switch ($name) {
			case 'keyField':
			case 'valueField':
			case 'translate':
			case 'query':
				return $this->$name;
		}

		return parent::__get($name);
	}

	public function __set($name, $value) {
		switch ($name) {
			case 'keyField':
			case 'valueField':
			case 'translate':
			case 'query':
				$this->$name = (string) $value;
				break;

			default:
				parent::__set($name, $value);
		}
	}

	public function setup(SimpleXMLElement $element, $value, $group = null) {
		$return = parent::setup($element, $value, $group);

		if ($return) {
			$this->keyField = $this->element['key_field'] ? (string) $this->element['key_field'] : 'value';
			$this->valueField = $this->element['value_field'] ? (string) $this->element['value_field'] : (string) $this->element['name'];
			$this->translate = $this->element['translate'] ? (string) $this->element['translate'] : false;
			$this->query = (string) $this->element['query'];
		}

		return $return;
	}

	protected function getOptions() {
		$options = array();

		// Initialize some field attributes.
		$key = $this->keyField;
		$value = $this->valueField;

		// Get the database object.
		$db = JFactory::getDbo();

		// Set the query and get the result list.
		$db->setQuery($this->query);
		$items = $db->loadObjectlist();

		// Build the field options.
		if (!empty($items)) {
			foreach ($items as $item) {
				if ($this->translate == true) {
					$options[] = JHtml::_('select.option', $item->$key, JText::_($item->$value));
				} else {
					$options[] = JHtml::_('select.option', $item->$key, $item->$value);
				}
			}
		}

		// Merge any additional options in the XML definition.
		$options = array_merge(parent::getOptions(), $options);

		return $options;
	}

}
