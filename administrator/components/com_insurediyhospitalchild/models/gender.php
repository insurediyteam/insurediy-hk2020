<?php

defined('_JEXEC') or die;

class InsureDIYHospitalChildModelGender extends JModelAdmin {

	protected $text_prefix = 'COM_INSUREDIYHOSPITALCHILD';

	protected function canDelete($record) {
		if (!empty($record->id)) {
			if ($record->state != -2) {
				return;
			}
			$user = JFactory::getUser();

			if ($record->catid) {
				return $user->authorise('core.delete', 'com_insurediyhospitalchild.category.' . (int) $record->catid);
			} else {
				return parent::canDelete($record);
			}
		}
	}

	protected function canEditState($record) {
		$user = JFactory::getUser();

		if (!empty($record->catid)) {
			return $user->authorise('core.edit.state', 'com_insurediyhospitalchild.category.' . (int) $record->catid);
		} else {
			return parent::canEditState($record);
		}
	}

	public function getTable($type = 'Gender', $prefix = 'InsureDIYHospitalChildTable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}

	public function getForm($data = array(), $loadData = true) {
		$app = JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm('com_insurediyhospitalchild.gender', 'gender', array('control' => 'jform', 'load_data' => $loadData));

		if (empty($form)) {
			return false;
		}

		// Determine correct permissions to check.
		/* if ($this->getState('cedeledepot.id'))
		  {
		  // Existing record. Can only edit in selected categories.
		  $form->setFieldAttribute('catid', 'action', 'core.edit');
		  }
		  else
		  {
		  // New record. Can only create in selected categories.
		  $form->setFieldAttribute('catid', 'action', 'core.create');
		  }
		 */

		// Modify the form based on access controls.
		/* if (!$this->canEditState((object) $data))
		  {
		  // Disable fields for display.
		  $form->setFieldAttribute('ordering', 'disabled', 'true');
		  $form->setFieldAttribute('state', 'disabled', 'true');
		  $form->setFieldAttribute('publish_up', 'disabled', 'true');
		  $form->setFieldAttribute('publish_down', 'disabled', 'true');

		  // Disable fields while saving.
		  // The controller has already verified this is a record you can edit.
		  $form->setFieldAttribute('ordering', 'filter', 'unset');
		  $form->setFieldAttribute('state', 'filter', 'unset');
		  $form->setFieldAttribute('publish_up', 'filter', 'unset');
		  $form->setFieldAttribute('publish_down', 'filter', 'unset');
		  }
		 */

		return $form;
	}

	protected function loadFormData() {
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_insurediyhospitalchild.edit.gender.data', array());

		if (empty($data)) {
			$data = $this->getItem();

			// Prime some default values.
			if ($this->getState('gender.id') == 0) {
				$app = JFactory::getApplication();
				//$data->set('catid', $app->input->get('catid', $app->getUserState('com_contact.contacts.filter.category_id'), 'int'));
			}
		}

		$this->preprocessData('com_insurediyhospitalchild.gender', $data);

		return $data;
	}

	public function getItem($pk = null) {
		if ($item = parent::getItem($pk)) {

			//$item->articletext = trim($item->fulltext) != '' ? $item->introtext . "<hr id=\"system-readmore\" />" . $item->fulltext : $item->introtext;

			if (!empty($item->id)) {
				/* $item->tags = new JHelperTags;
				  $item->tags->getTagIds($item->id, 'com_insurediyhospitalchild.cedeledepot');
				  $item->metadata['tags'] = $item->tags;
				 */
			}
		}

		return $item;
	}

	protected function prepareTable($table) {
		$date = JFactory::getDate();
		$user = JFactory::getUser();

		/* $table->title		= htmlspecialchars_decode($table->title, ENT_QUOTES);
		  //$table->alias		= JApplication::stringURLSafe($table->alias);

		  if (empty($table->alias))
		  {
		  $table->alias = JApplication::stringURLSafe($table->title);
		  }

		  if (empty($table->id))
		  {
		  // Set the values
		  // Set the values
		  //$table->modified	= $date->toSql();
		  //$table->modified_by	= $user->get('id');

		  // Increment the content version number.
		  //$table->version++;
		  }
		 */
	}

	protected function getReorderConditions($table) {
		$condition = array();

		return $condition;
	}

	public function save($data) {
		$app = JFactory::getApplication();

		$jinput = JFactory::getApplication()->input->getArray($_POST);
		$data['id'] = $jinput['jform']['id'];

		$return = parent::save($data);

		// ifoundries modified
		/* if($return) {

		  $store_code = $data['code'];

		  foreach($jinput['usergroup'] as $member_type_id) :

		  $arrtmp = array();

		  for($i=1;$i<=10;$i++) :

		  $arrtmp[] = " cat".$i."_rebate_point = '". $jinput['member_'.$member_type_id.'_cat'.$i.'_rebate_point']."' ";

		  endfor;

		  $query = " UPDATE #__user_transaction_rebate_points SET ".implode(',', $arrtmp) ." WHERE store_code = '$store_code' AND user_group_id = '$member_type_id' ";
		  $this->_db->setQuery( $query );
		  $this->_db->query();

		  endforeach;
		  }
		 */
		// iFoundries : to do 
		// save the Rebate Points Here

		return $return;
	}

	/**
	 * Method to change the title & alias.
	 *
	 * @param   integer  $category_id  The id of the parent.
	 * @param   string   $alias        The alias.
	 * @param   string   $name         The title.
	 *
	 * @return  array  Contains the modified title and alias.
	 *
	 * @since   3.1
	 */
	protected function generateNewTitle($category_id, $alias, $name) {
		// Alter the title & alias
		$table = $this->getTable();

		while ($table->load(array('alias' => $alias))) {
			if ($name == $table->title) {
				$name = JString::increment($name);
			}
			$alias = JString::increment($alias, 'dash');
		}

		return array($name, $alias);
	}

}
