<?php

defined('_JEXEC') or die;

class InsureDIYHospitalChildModelPlans extends JModelList {

	public function __construct($config = array()) {
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'plan_index_code', 'p.plan_index_code',
				'plan_name', 'p.plan_name',
				'cover_amt', 'p.cover_amt',
				'premium', 'p.premium'
			);
		}

		parent::__construct($config);
	}

	protected function populateState($ordering = null, $direction = null) {
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$accessId = $this->getUserStateFromRequest($this->context . '.filter.access', 'filter_access');
		$this->setState('filter.access', $accessId);

		$stateId = $this->getUserStateFromRequest($this->context . '.filter.state', 'filter_state');
		$this->setState('filter.state', $stateId);

		$insurer_codeId = $this->getUserStateFromRequest($this->context . '.filter.insurer_code', 'filter_insurer_code');
		$this->setState('filter.insurer_code', $insurer_codeId);

		$premium = $this->getUserStateFromRequest($this->context . '.filter.premium', 'premium');
		$this->setState('filter.premium', $premium);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_insurediyhospitalchild');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('p.plan_name', 'asc');
	}

	protected function getStoreId($id = '') {
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.access');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	protected function getListQuery() {
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);

		$query->select('*');
		$query->from($db->quoteName('#__insure_hospitalchild_plans') . ' AS p');

		// Filter by published state
		$published = $this->getState('filter.state');
		if (is_numeric($published)) {
			$query->where('p.state = ' . (int) $published);
		} elseif ($published === '') {
			$query->where('(p.state IN (0, 1))');
		}

		// Filter by Insurer Code state
		$insurer_code = $this->getState('filter.insurer_code');
		if ($insurer_code) {
			$query->where('p.insurer_code = ' . $db->Quote($insurer_code, false));
		}

		// Filter by Year Code state
		$premium = $this->getState('filter.premium');
		if ($premium) {
			$query->where('p.premium = ' . $db->Quote($premium, false));
		}

		// Filter by search in title
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			$search = $db->quote('%' . $db->escape($search, true) . '%');
			$query->where(' (p.plan_name LIKE ' . $search . ') OR (p.plan_index_code LIKE ' . $search . ')');
		}

		// Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		$query->order($db->escape($orderCol . ' ' . $orderDirn));
		return $query;
	}

}
