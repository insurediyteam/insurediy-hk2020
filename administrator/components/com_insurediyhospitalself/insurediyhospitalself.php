<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_weblinks
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

global $reqOption, $reqView, $reqLayout, $helper;

$reqOption = 'com_insurediyhospitalself';
$reqView = JRequest::getCmd('view', 'quotations');
$reqLayout = JRequest::getCmd('layout', 'default');

if (!JFactory::getUser()->authorise('core.manage', 'com_insurediyhospitalself'))
{
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

JLoader::register('InsureDIYHospitalSelfHelper', __DIR__ . '/helpers/insurediyhospitalself.php');

$controller	= JControllerLegacy::getInstance('InsureDIYHospitalSelf');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
