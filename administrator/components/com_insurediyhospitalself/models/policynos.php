<?php

defined('_JEXEC') or die;

class InsureDIYHospitalSelfModelPolicyNos extends JModelList {

	public function __construct($config = array()) {
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'plan_name', 'p.plan_name',
				'policy_number', 'p.policy_number',
				'state', 'p.state'
			);
		}

		parent::__construct($config);
	}

	protected function populateState($ordering = null, $direction = null) {
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$accessId = $this->getUserStateFromRequest($this->context . '.filter.access', 'filter_access');
		$this->setState('filter.access', $accessId);

		$stateId = $this->getUserStateFromRequest($this->context . '.filter.state', 'filter_state');
		$this->setState('filter.state', $stateId);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_insurediyhospitalself');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('p.plan_name', 'asc');
	}

	protected function getStoreId($id = '') {
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.access');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	protected function getListQuery() {
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$query->select('*');
		$query->from($db->quoteName('#__insure_hospitalself_policy_numbers') . ' AS p');

		// Filter by published state
		$published = $this->getState('filter.state');
		if (is_numeric($published)) {
			$query->where('p.state = ' . (int) $published);
		} elseif ($published === '') {
			$query->where('(p.state IN (0, 1))');
		}

		// Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		$query->order($db->escape($orderCol . ' ' . $orderDirn));

		return $query;
	}

	public function delete($cid) {
		$cids = (array) $cid;
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		if (count($cids) > 0) {
			$query->delete("#__insure_hospitalself_policy_numbers");
			$query->where("id IN (" . implode(",", $cids) . ")");
			$db->setQuery($query);
			$result = $db->execute();
			if ($result) {
				return TRUE;
			}
			$this->setError(JText::_('COM_INSUREDIYHOSPITALSELF_DELETING_ITEMS_FAIL'));
			return FALSE;
		}
		$this->setError(JText::_('COM_INSUREDIYHOSPITALSELF_NO_ITEM_SELECTED'));
		return FALSE;
	}

}
