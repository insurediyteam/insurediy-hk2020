<?php

defined('JPATH_PLATFORM') or die;

class JFormFieldFileLink extends JFormField {

	protected $type = 'FileLink';

	protected function getInput() {
		$isList = (isset($this->element['islist'])) ? (int) $this->element['islist'] : 0;
		$basepath = (isset($this->element['basepath'])) ? (string) $this->element['basepath'] : 'media/com_insurediyhospitalself/documents/';
		if ($this->value) :
			if ($isList) {
				foreach ($this->value as $key => $val) {
					$html[] = '<a target="_blank" href="' . MyUri::getUrls("site") . $val . '">' . $key . '</a><br>';
				}
			} else {
				$val = explode('|', $this->value);
				$path = JURI::root() . $basepath;
				$html[] = '<a target="_blank" href="' . $path . $val[0] . '">' . $val[1] . '</a>';
			}
		else :
			$html[] = 'N/A';
		endif;
		return implode($html);
	}

}
