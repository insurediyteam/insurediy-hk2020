<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_insurediyhospitalself
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * View to edit a weblink.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_insurediyhospitalself
 * @since       1.5
 */
class InsureDIYHospitalSelfViewQuotation extends JViewLegacy {

	protected $state;
	protected $item;
	protected $form;

	/**
	 * Display the view
	 */
	public function display($tpl = null) {
		$this->state = $this->get('State');
		$this->item = $this->get('Item');
		$this->form = $this->get('Form');
		$this->questionnaires = $this->get('Questionnaires');
		$this->applications_form = $this->get('ListApplicationFormHTML');
		$model = $this->getModel("Existinginsurance");

		$this->ei_form = $model->getForm();


		// Check for errors.
		if (empty($this->item)) {
			JError::raiseError(500, "No Cedele Depot Item");
			return false;
		}

		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar() {
		JFactory::getApplication()->input->set('hidemainmenu', true);

		JToolbarHelper::title(JText::_('COM_INSUREDIYHOSPITALSELF_MANAGER_QUOTATION'), 'insurediyhospitalself.png');
		JToolbarHelper::apply('quotation.apply');
		JToolbarHelper::save('quotation.save');

		if (empty($this->item->id)) {
			JToolbarHelper::cancel('quotation.cancel');
		} else {
			JToolbarHelper::cancel('quotation.cancel', 'JTOOLBAR_CLOSE');
		}
	}

}
