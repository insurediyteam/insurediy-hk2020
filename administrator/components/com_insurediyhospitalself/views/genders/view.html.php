<?php

defined('_JEXEC') or die;

class InsureDIYHospitalSelfViewGenders extends JViewLegacy {

	protected $items;
	protected $pagination;
	protected $state;

	public function display($tpl = null) {
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');

		InsureDIYHospitalSelfHelper::addSubmenu('genders');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar() {

		$state = $this->get('State');
		$canDo = InsureDIYHospitalSelfHelper::getActions($state->get('filter.category_id'));
		$user = JFactory::getUser();
		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');

		JToolbarHelper::title(JText::_('COM_INSUREDIYHOSPITALSELF_MANAGER_GENDER'), 'insurediyhospitalself.png');

		if ($canDo->get('core.create')) {
			JToolbarHelper::addNew('gender.add');
		}

		if ($canDo->get('core.edit')) {
			JToolbarHelper::editList('gender.edit');
		}


		if ($canDo->get('core.edit.state')) {

			JToolbarHelper::publish('genders.publish', 'JTOOLBAR_PUBLISH', true);
			JToolbarHelper::unpublish('genders.unpublish', 'JTOOLBAR_UNPUBLISH', true);

			//JToolbarHelper::archiveList('gender.archive');
			//JToolbarHelper::checkin('gender.checkin');
		}

		if ($state->get('filter.state') == -2 && $canDo->get('core.delete')) {
			JToolbarHelper::deleteList('', 'genders.delete', 'JTOOLBAR_EMPTY_TRASH');
		} elseif ($canDo->get('core.edit.state')) {

			JToolbarHelper::trash('genders.trash');
		}

		if ($canDo->get('core.admin')) {
			JToolbarHelper::preferences('com_insurediyhospitalself');
		}

		//JToolbarHelper::help('JHELP_COMPONENTS_CedeleDepotS_LINKS');

		JHtmlSidebar::setAction('index.php?option=com_insurediyhospitalself&view=quotations');
		JHtmlSidebar::setAction('index.php?option=com_insurediyhospitalself&view=genders');
		JHtmlSidebar::setAction('index.php?option=com_insurediyhospitalself&view=plans');

		JHtmlSidebar::addFilter(
				JText::_('JOPTION_SELECT_INSUREDIYLIFE_GENDER'), 'filter_gender', JHtml::_('select.options', InsureDIYHospitalSelfHelper::getGenderOptions(), 'value', 'text', $this->state->get('filter.gender'), true)
		);


		JHtmlSidebar::addFilter(
				JText::_('JOPTION_SELECT_PUBLISHED'), 'filter_state', JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), 'value', 'text', $this->state->get('filter.state'), true)
		);
	}

	protected function getSortFields() {
		return array(
			'g.ordering' => JText::_('JGRID_HEADING_ORDERING'),
			'g.gender' => JText::_('COM_INSUREDIYHOSPITALSELF_HEADER_GENDER'),
			'g.questionnaire' => JText::_('COM_INSUREDIYHOSPITALSELF_HEADER_QUESTIONNAIRE')
		);
	}

}
