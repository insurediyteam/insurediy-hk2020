<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_InsureDIYHospitalSelf
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * View class for a list of InsureDIYHospitalSelf.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_InsureDIYHospitalSelf
 * @since       1.5
 */
class InsureDIYHospitalSelfViewPolicyNos extends JViewLegacy {

	protected $items;
	protected $pagination;
	protected $state;

	/**
	 * Display the view
	 *
	 * @return  void
	 */
	public function display($tpl = null) {
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');

		InsureDIYHospitalSelfHelper::addSubmenu('policynos');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar() {
		require_once JPATH_COMPONENT . '/helpers/insurediyhospitalself.php';

		$state = $this->get('State');
		$canDo = InsureDIYHospitalSelfHelper::getActions($state->get('filter.category_id'));
		$user = JFactory::getUser();
		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');

		JToolbarHelper::title(JText::_('COM_INSUREDIYHOSPITALSELF_MANAGER_PLANS'), 'insurediyhospitalself.png');
		JToolbarHelper::deleteList('', 'policynos.delete', 'COM_INSUREDIYHOSPITALSELF_BUTTON_DELETE');

		JHtmlSidebar::setAction('index.php?option=com_insurediyhospitalself&view=quotations');
		JHtmlSidebar::setAction('index.php?option=com_insurediyhospitalself&view=genders');
		JHtmlSidebar::setAction('index.php?option=com_insurediyhospitalself&view=plans');
		JHtmlSidebar::setAction('index.php?option=com_insurediyhospitalself&view=csvexport');
		JHtmlSidebar::setAction('index.php?option=com_insurediyhospitalself&view=csvimport');
		JHtmlSidebar::setAction('index.php?option=com_insurediyhospitalself&view=policynos');

		JHtmlSidebar::addFilter(
				JText::_('JOPTION_SELECT_PUBLISHED'), 'filter_state', JHtml::_('select.options', InsureDIYHospitalSelfHelper::getPolicyNumberStateOptions(), 'value', 'text', $this->state->get('filter.state'), true)
		);
	}

	protected function getSortFields() {
		return array(
			'p.plan_name' => JText::_('COM_INSUREDIYHOSPITALSELF_HEADER_PLAN_NAME'),
			'p.state' => JText::_('JSTATUS'),
			'p.policy_number' => JText::_('COM_INSUREDIYHOSPITALSELF_HEADER_POLICY_NUMBERS'),
		);
	}

}
