<?php
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
?>

<div class="row-fluid">
	<div class="span10 form-horizontal">
		<fieldset>
			<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'import')); ?>

			<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'import', JText::_('COM_INSUREDIYHOSPITALSELF_CSV_IMPORT_DETAILS')); ?>
			<form action="<?php echo JRoute::_('index.php?option=com_insurediyhospitalself&task=csvimport.plans'); ?>" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data" class="form-validate">
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('csvimport'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('csvimport'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"></div>
					<div class="controls"><input type="submit" value="Import It!" /></div>
				</div>
				<input type="hidden" name="task" value="csvimport.plans" />
				<?php echo JHtml::_('form.token'); ?>
			</form>
			<?php echo JHtml::_('bootstrap.endTab'); ?>
			<?php echo JHtml::_('bootstrap.endTabSet'); ?>
		</fieldset>
	</div>
	<!-- End Weblinks -->
	<!-- Begin Sidebar -->
	<?php //echo JLayoutHelper::render('joomla.edit.details', $this);   ?>
	<!-- End Sidebar -->
</div>		

