<?php
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
?>

<div class="row-fluid">
	<div class="span10 form-horizontal">
		<fieldset>
			<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'export')); ?>

			<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'export', JText::_('COM_INSUREDIYHOSPITALSELF_CSV_EXPORT_DETAILS')); ?>
			<form action="<?php echo JRoute::_('index.php?option=com_insurediyhospitalself&task=csvexport.general'); ?>" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data" class="form-validate">
				<fieldset>
					<legend><?php echo JText::_("LEGEND_CHOOSE_TYPE"); ?></legend>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('export_type'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('export_type'); ?></div>
					</div>
				</fieldset>

				<fieldset>
					<legend><?php echo JText::_("LEGEND_QUOATION_FILTER"); ?></legend>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('date_start'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('date_start'); ?></div>
					</div>
					<div class="control-group">
						<div class="control-label"><?php echo $this->form->getLabel('date_end'); ?></div>
						<div class="controls"><?php echo $this->form->getInput('date_end'); ?></div>
					</div>
				</fieldset>

				<div class="control-group">
					<div class="control-label"></div>
					<div class="controls"><input class="btn btn-primary" type="submit" value="Export" /></div>
				</div>
				
				<input type="hidden" name="task" value="csvexport.general" />
				<?php echo JHtml::_('form.token'); ?>
			</form>
			<?php echo JHtml::_('bootstrap.endTab'); ?>
			<?php echo JHtml::_('bootstrap.endTabSet'); ?>
		</fieldset>
	</div>
	<!-- End Weblinks -->
	<!-- Begin Sidebar -->
	<?php //echo JLayoutHelper::render('joomla.edit.details', $this);   ?>
	<!-- End Sidebar -->
</div>		

