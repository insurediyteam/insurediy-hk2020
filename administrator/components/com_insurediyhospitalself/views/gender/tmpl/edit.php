<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_cedeledepots
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
//JHtml::_('formbehavior.chosen', 'select');
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'gender.cancel' || document.formvalidator.isValid(document.id('insurediyhospitalself-gender-form')))
		{
<?php //echo $this->form->getField('description')->save();       ?>
			Joomla.submitform(task, document.getElementById('insurediyhospitalself-gender-form'));
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_insurediyhospitalself'); ?>" method="post" name="adminForm" id="insurediyhospitalself-gender-form" class="form-validate">
	<div class="row-fluid">
		<div class="span10 form-horizontal">
			<fieldset>
				<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'info_details')); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'info_details', JText::_('COM_INSUREDIYHOSPITALSELF_GENDER_INFORMATION')); ?>

				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('gender'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('gender'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('questionnaire'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('questionnaire'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('is_criteria'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('is_criteria'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('description'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('description'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('help_quote'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('help_quote'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('id'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('id'); ?></div>
				</div>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<input type="hidden" name="task" value="" />
				<?php echo JHtml::_('form.token'); ?>

				<?php echo JHtml::_('bootstrap.endTabSet'); ?>
			</fieldset>
		</div>
		<!-- End Weblinks -->
		<!-- Begin Sidebar -->
		<?php //echo JLayoutHelper::render('joomla.edit.details', $this);  ?>
		<!-- End Sidebar -->
	</div>
</form>
