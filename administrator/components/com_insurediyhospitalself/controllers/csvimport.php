<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_insurediyhospitalself
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Weblink controller class.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_insurediyhospitalself
 * @since       1.6
 */
class InsureDIYHospitalSelfControllerCSVImport extends JControllerForm {

	/**
	 * Method override to check if you can add a new record.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  boolean
	 *
	 * @since   1.6
	 */
	protected function allowAdd($data = array()) {
		$user = JFactory::getUser();
//$categoryId = JArrayHelper::getValue($data, 'catid', $this->input->getInt('filter_category_id'), 'int');
		$allow = null;

		/* if ($categoryId)
		  {
		  // If the category has been passed in the URL check it.
		  $allow = $user->authorise('core.create', $this->option . '.category.' . $categoryId);
		  }
		 */

		if ($allow === null) {
// In the absense of better information, revert to the component permissions.
			return parent::allowAdd($data);
		} else {
			return $allow;
		}
	}

	/**
	 * Method to check if you can add a new record.
	 *
	 * @param   array   $data  An array of input data.
	 * @param   string  $key   The name of the key for the primary key.
	 *
	 * @return  boolean
	 * @since   1.6
	 */
	protected function allowEdit($data = array(), $key = 'id') {
		$recordId = (int) isset($data[$key]) ? $data[$key] : 0;
		$categoryId = 0;
		if ($recordId) {
			$categoryId = (int) $this->getModel()->getItem($recordId)->catid;
		}

		if ($categoryId) {
// The category has been set. Check the category permissions.
			return JFactory::getUser()->authorise('core.edit', $this->option . '.category.' . $categoryId);
		} else {
// Since there is no asset tracking, revert to the component permissions.
			return parent::allowEdit($data, $key);
		}
	}

	/**
	 * Method to run batch operations.
	 *
	 * @param   object  $model  The model.
	 *
	 * @return  boolean   True if successful, false otherwise and internal error is set.
	 *
	 * @since   1.7
	 */
	public function batch($model = null) {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

// Set the model
		$model = $this->getModel('CSVImport', '', array());

// Preset the redirect
		$this->setRedirect(JRoute::_('index.php?option=com_insurediyhospitalself&view=csvimport' . $this->getRedirectToListAppend(), false));

		return parent::batch($model);
	}

	/**
	 * Function that allows child controller access to model data after the data has been saved.
	 *
	 * @param   JModelLegacy  $model      The data model object.
	 * @param   array         $validData  The validated data.
	 *
	 * @return	void
	 * @since	1.6
	 */
	protected function postSaveHook(JModelLegacy $model, $validData = array()) {
		if ($task == 'save') {
			$this->setRedirect(JRoute::_('index.php?option=com_insurediyhospitalself&view=csvimport', false));
		}
	}

	public function plans() {

		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

// Set the model
		$model = $this->getModel('CSVImport');

		if ($model->import_plans()) {
			$msg = ' Importing Plans Success. ';
		} else {
			$msg = ' Importing Plans Failed! Please try again...';
		}

		$this->setRedirect(JRoute::_('index.php?option=com_insurediyhospitalself&view=csvimport', false), $msg);
	}

	public function generateData() {
		ini_set('memory_limit', '-1'); //**************IMPORTANT

		$csvHeaders = array("plan_name", "insurer_code", "year_code", "is_smoking", "gender", "age", "sum_insured", "price", "renewable", "terminal_illness_benefit", "benefits", "flag_banner", "download_file", "nationality", "residency", "published");
		$insurers = array("AIA", "AXA", "SUN", "ZUR");
		$years = array("01YRT", "05YRT", "10YRT", "15YRT", "20YRT", "60T75");
		$genders = array("M", "F");
		$nationalities = array("CN", "HK", "MO", "XX");
		$residencies = array("CN", "HK", "MO", "XX");
		$ages = range(18, 55, 1);
		$sum_insured_s = array("1000000", "2000000", "3000000", "4000000", "5000000", "6000000", "8000000", "10000000");
		$count = 1;
		$csvMaster = array();
		$csv_lines = array();
		foreach ($insurers as $i_key => $insurer) {
			foreach ($genders as $gender) {
				foreach ($years as $year) {
					foreach ($nationalities as $nationality) {
						foreach ($residencies as $residency) {
							foreach ($ages as $agekey => $age) {
								set_time_limit(0);
								foreach ($sum_insured_s as $si_key => $sum_insured) {
									$csv = array();
									foreach ($csvHeaders as $k => $v) {
										switch ($v) {
											case "plan_name":
												$csv[] = $insurer . " Plan " . $count;
												break;
											case "insurer_code":
												$csv[] = $insurer;
												break;
											case "year_code":
												$csv[] = $year;
												break;
											case "is_smoking":
												$csv[] = "NS";
												break;
											case "gender":
												$csv[] = $gender;
												break;
											case "age":
												$csv[] = $age;
												break;
											case "sum_insured":
												$csv[] = $sum_insured;
												break;
											case "price":
												$csv[] = $agekey + 1 + $si_key + $i_key;
												break;
											case "renewable":
												$csv[] = 1;
												break;
											case "terminal_illness_benefit":
												$csv[] = 1;
												break;
											case "nationality":
												$csv[] = $nationality;
												break;
											case "residency":
												$csv[] = $residency;
												break;
											case "published":
												$csv[] = 1;
												break;
											default:
												$csv[] = "";
												break;
										}
									}
									$count ++;
									$csv_lines[] = implode(',', MyHelper::escapeCSV($csv, TRUE));
								}
							}
						}
					}
				}
			}
			$count = 1;
		}
		$csvMaster = implode("\n", $csv_lines);
		$csvHeader = implode(',', MyHelper::escapeCSV($csvHeaders, TRUE));

		$actualCSVarr = array(
			$csvHeader,
			$csvMaster
		);

		$actualCSVData = implode("\n", $actualCSVarr);
		MyHelper::fileDownload("sample.csv", $actualCSVData);
	}

	public function generateData2() { // hospital
		ini_set('memory_limit', '-1'); //**************IMPORTANT

		$csvHeaders = array("plan_name", "insurer_code", "cover_amt", "room_type", "premium", "cover_country", "gender", "max_renewal_age", "flag_banner", "maternity_cover", "medical_examination", "waiting_period", "other_benefit", "brochure", "state");
		$insurers = array("AIA", "AXA", "SUN", "ZUR");
		$genders = array("M", "F");
		$cover_country = array("HK");
		$room_types = array("1", "2", "3", "4");
		$cover_amts = array("1", "2", "3", "4");
		$count = 1;
		$csvMaster = array();
		$csv_lines = array();
		MyHelper::debug($csvHeaders);
		exit;
		foreach ($insurers as $i_key => $insurer) {
			foreach ($genders as $gender) {
				foreach ($room_types as $room_type) {
					foreach ($cover_amts as $cover_amt) {
						set_time_limit(0);
						$csv = array();
						foreach ($csvHeaders as $k => $v) {
							switch ($v) {
								case "plan_name":
									$csv[] = $insurer . " Plan " . $count;
									break;
								case "insurer_code":
									$csv[] = $insurer;
									break;
								case "cover_amt":
									$csv[] = $cover_amt;
									break;
								case "room_type":
									$csv[] = $room_type;
									break;
								case "gender":
									$csv[] = $gender;
									break;
								case "premium":
									$csv[] = 100 + ($cover_amt * 20);
									break;
								case "cover_country":
									$csv[] = "HK";
									break;
								case "max_renewal_age":
									$csv[] = "53";
									break;
								case "maternity_cover":
									$csv[] = 1;
									break;
								case "medical_examination":
									$csv[] = 1;
									break;
								case "waiting_period":
									$csv[] = "1 month";
									break;
								default:
									$csv[] = "";
									break;
							}
						}
						$count ++;
						$csv_lines[] = implode(',', MyHelper::escapeCSV($csv, TRUE));
					}
				}
			}
			$count = 1;
		}
		$csvMaster = implode("\n", $csv_lines);
		$csvHeader = implode(',', MyHelper::escapeCSV($csvHeaders, TRUE));

		$actualCSVarr = array(
			$csvHeader,
			$csvMaster
		);

		$actualCSVData = implode("\n", $actualCSVarr);
		MyHelper::fileDownload("sample.csv", $actualCSVData);
	}

}
