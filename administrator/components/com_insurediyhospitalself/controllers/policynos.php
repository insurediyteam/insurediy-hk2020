<?php

defined('_JEXEC') or die;

class InsureDIYHospitalSelfControllerPolicyNos extends JControllerAdmin {

	protected $text_prefix = "COM_INSUREDIYHOSPITALSELF";

	public function getModel($name = 'Policynos', $prefix = 'InsureDIYHospitalSelfModel', $config = array('ignore_request' => true)) {
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}

	public function saveOrderAjax() {
		// Get the input
		$input = JFactory::getApplication()->input;
		$pks = $input->post->get('cid', array(), 'array');
		$order = $input->post->get('order', array(), 'array');

		// Sanitize the input
		JArrayHelper::toInteger($pks);
		JArrayHelper::toInteger($order);

		// Get the model
		$model = $this->getModel();

		// Save the ordering
		$return = $model->saveorder($pks, $order);

		if ($return) {
			echo "1";
		}

		// Close the application
		JFactory::getApplication()->close();
	}

	public function deletePolicyNos() {
		
	}

	protected function postDeleteHook(JModelLegacy $model, $ids = null) {

	}

	public function cancel() {
		MyUri::redirect("index.php?option=com_insurediyhospitalself&view=policynos");
	}

}
