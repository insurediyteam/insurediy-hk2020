<?php

/**
 * @package     Joomla.Administrator
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Weblink controller class.
 *
 * @package     Joomla.Administrator
 * @since       1.6
 */
class ProductControllerItem extends JControllerForm {

	/**
	 * Method override to check if you can add a new record.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  boolean
	 *
	 * @since   1.6
	 */
	protected function allowAdd($data = array()) {
		$user = JFactory::getUser();
		//$categoryId = JArrayHelper::getValue($data, 'catid', $this->input->getInt('filter_category_id'), 'int');
		$allow = null;

		/* if ($categoryId)
		  {
		  // If the category has been passed in the URL check it.
		  $allow = $user->authorise('core.create', $this->option . '.category.' . $categoryId);
		  }
		 */

		if ($allow === null) {
			// In the absense of better information, revert to the component permissions.
			return parent::allowAdd($data);
		} else {
			return $allow;
		}
	}

	/**
	 * Method to check if you can add a new record.
	 *
	 * @param   array   $data  An array of input data.
	 * @param   string  $key   The name of the key for the primary key.
	 *
	 * @return  boolean
	 * @since   1.6
	 */
	protected function allowEdit($data = array(), $key = 'id') {
		$recordId = (int) isset($data[$key]) ? $data[$key] : 0;
		$categoryId = 0;
		if ($recordId) {
			$categoryId = (int) $this->getModel()->getItem($recordId)->catid;
		}

		if ($categoryId) {
			// The category has been set. Check the category permissions.
			return JFactory::getUser()->authorise('core.edit', $this->option . '.category.' . $categoryId);
		} else {
			// Since there is no asset tracking, revert to the component permissions.
			return parent::allowEdit($data, $key);
		}
	}

	/**
	 * Method to run batch operations.
	 *
	 * @param   object  $model  The model.
	 *
	 * @return  boolean   True if successful, false otherwise and internal error is set.
	 *
	 * @since   1.7
	 */
	public function batch($model = null) {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Set the model
		$model = $this->getModel('Product', '', array());

		// Preset the redirect
		$this->setRedirect(JRoute::_('index.php?option=com_Product&view=quotations' . $this->getRedirectToListAppend(), false));

		return parent::batch($model);
	}

	/**
	 * Function that allows child controller access to model data after the data has been saved.
	 *
	 * @param   JModelLegacy  $model      The data model object.
	 * @param   array         $validData  The validated data.
	 *
	 * @return	void
	 * @since	1.6
	 */
	protected function postSaveHook(JModelLegacy $model, $validData = array()) {
		$task = $this->getTask();
		$app = JFactory::getApplication();
		if ($task == 'apply') {
			$id = ($validData['id']) ? $validData['id'] : $app->getUserState("com_product.itemform.id", 0);
			$this->setRedirect(JRoute::_('index.php?option=com_product&view=item&layout=edit&id=' . $id, false));
		}
		if ($task == 'save') {
			$this->setRedirect(JRoute::_('index.php?option=com_product&view=list', false));
		}
	}

	public function cancel() {
		$app = JFactory::getApplication();
		$app->setUserState("com_product.itemform.iserror", NULL);
		$app->setUserState("com_product.itemform.data", NULL);
		$app->setUserState("com_product.itemform.id", NULL);

		$this->setRedirect(JRoute::_('index.php?option=com_product&view=list', false));
	}

}
