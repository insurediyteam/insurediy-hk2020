<?php
/**
 * @package     Joomla.Administrator
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
$form = $this->form;
$item = $this->item;
$fldGroupBasic = "basic";
$id = JRequest::getVar("id", 0);
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task) {
		if (task == 'item.cancel' || document.formvalidator.isValid(document.id('adminForm'))) {
			if (task == "item.preview") {
				window.open('<?php echo JURI::root(); ?>index.php?option=com_item&view=item&id=<?php echo $id; ?>');
								return;
							}
							Joomla.submitform(task, document.getElementById('adminForm'));
						}
					}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_product'); ?>" method="post" name="adminForm" id="adminForm" class="form-validate" enctype="multipart/form-data">
	<div class="row-fluid">
		<div class="span10 form-horizontal">
			<fieldset>
				<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'basic')); ?>
				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'basic', JText::_('COM_PRODUCT_ITEM_TAB_BASIC')); ?>
				<?php foreach ($form->getFieldset($fldGroupBasic) as $fields): ?>
					<div class="control-group">
						<div class="control-label"><?php echo $fields->label; ?></div>
						<div class="controls"><?php echo $fields->input; ?></div>
					</div>
				<?php endforeach; ?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<input type="hidden" name="task" value="" />
				<?php echo JHtml::_('form.token'); ?>
				<?php echo JHtml::_('bootstrap.endTabSet'); ?>
			</fieldset>
		</div>
		<!-- Begin Sidebar -->
		<?php //echo JLayoutHelper::render('joomla.edit.details', $this);     ?>
		<!-- End Sidebar -->
	</div>
</form>
