<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_product
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * View class for a list of list.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_product
 * @since       1.6
 */
class ProductViewList extends JViewLegacy {

	protected $items;
	protected $pagination;
	protected $state;

	/**
	 * Display the view
	 *
	 * @param   string  $tpl  The name of the template file to parse; automatically searches through the template paths.
	 *
	 * @return  void
	 */
	public function display($tpl = null) {
		if ($this->getLayout() !== 'modal') {
			ProductHelper::addSubmenu('list');
		}
		$model = $this->getModel("list");
		$items = $model->getItems();
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
		$this->state = $this->get('State');
		$this->filterForm = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');
		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
		// We don't need toolbar in the modal window.
		if ($this->getLayout() !== 'modal') {
			$this->addToolbar();
			$this->sidebar = JHtmlSidebar::render();
		}

		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function addToolbar() {
		$canDo = JHelperContent::getActions($this->state->get('filter.category_id'), 0, 'com_product');
		$user = JFactory::getUser();

		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');

		JToolbarHelper::title(JText::_('COM_PRODUCT_LIST_TITLE'), 'product.png');


		if ($canDo->get('core.create') || (count($user->getAuthorisedCategories('com_product', 'core.create'))) > 0) {
			JToolbarHelper::addNew('item.add');
		}

		if (($canDo->get('core.edit')) || ($canDo->get('core.edit.own'))) {
			JToolbarHelper::editList('item.edit');
		}

		if ($canDo->get('core.edit.state')) {
			JToolbarHelper::publish('list.publish', 'JTOOLBAR_PUBLISH', true);
			JToolbarHelper::unpublish('list.unpublish', 'JTOOLBAR_UNPUBLISH', true);
		}

		if ($this->state->get('filter.status') == 2 && $canDo->get('core.edit')) {
			JToolbarHelper::deleteList('', 'list.delete', 'JTOOLBAR_EMPTY_TRASH');
			JToolBarHelper::custom('list.restore', 'publish.png', 'publish_f2.png', 'BTN_RESTORE', true);
		} else {
			JToolbarHelper::trash('list.trash');
		}


		if ($canDo->get('core.admin')) {
			JToolbarHelper::preferences('com_product');
		}

		$filterOptions = array();
		$filterOptions[] = JHtml::_('select.option', '1', 'JPUBLISHED');
		$filterOptions[] = JHtml::_('select.option', '0', 'JUNPUBLISHED');
		$filterOptions[] = JHtml::_('select.option', '2', 'JTRASHED');


		JHtmlSidebar::addFilter(
				JText::_('JOPTION_SELECT_PUBLISHED'), 'filter_status', JHtml::_('select.options', $filterOptions, 'value', 'text', $this->state->get('filter.status'), true)
		);
	}

	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 *
	 * @since   3.0
	 */
	protected function getSortFields() {
		return array(
			'p.ordering' => JText::_('JGRID_HEADING_ORDERING'),
			'p.id' => JText::_('COM_PRODUCT_TB_HEADER_ID'),
			'p.name' => JText::_('COM_PRODUCT_TB_HEADER_NAME'),
			'p.points' => JText::_('COM_PRODUCT_TB_HEADER_POINTS'),
			'p.publish' => JText::_('COM_PRODUCT_TB_HEADER_PUBLISH'),
			'p.created' => JText::_('COM_PRODUCT_TB_HEADER_CREATED'),
			'p.created_by' => JText::_('COM_PRODUCT_TB_HEADER_CREATED_BY'),
			'p.modified' => JText::_('COM_PRODUCT_TB_HEADER_MODIFIED'),
			'p.modified_by' => JText::_('COM_PRODUCT_TB_HEADER_MODIFIED_BY')
		);
	}

}
