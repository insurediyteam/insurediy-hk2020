<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_product
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Methods supporting a list of article records.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_product
 *
 * @since       1.6
 */
class ProductModelList extends JModelList {

	/**
	 * Constructor.
	 *
	 * @param   array  $config  An optional associative array of configuration settings.
	 *
	 * @since   1.6
	 * @see     JController
	 */
	public function __construct($config = array()) {
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'id', 'a.id',
				'name', 'a.name',
				'points', 'a.points',
				'bp_email', 'a.bp_email',
				'state', 'a.state',
				'created', 'a.created',
				'created_by', 'a.created_by',
				'modified', 'a.modified',
				'modified_by', 'a.modified_by',
				'ordering', 'a.ordering',
			);
		}
		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @param   string  $ordering   An optional ordering field.
	 * @param   string  $direction  An optional direction (asc|desc).
	 *
	 * @return  void
	 *
	 * @since   1.6
	 */
	protected function populateState($ordering = null, $direction = null) {
		$app = JFactory::getApplication();

		// Adjust the context to support modal layouts.
		if ($layout = $app->input->get('layout')) {
			$this->context .= '.' . $layout;
		}

		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$published = $this->getUserStateFromRequest($this->context . '.filter.published', 'filter_published', '');
		$this->setState('filter.published', $published);

		// Load the filter state.
		$status = $this->getUserStateFromRequest($this->context . '.filter.status', 'filter_status');
		if ($status == "1" || $status == "0" || $status == "2") {
			$this->setState('filter.status', $status);
		}

		// List state information.
		parent::populateState('a.name', 'asc');
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return  JDatabaseQuery
	 *
	 * @since   1.6
	 */
	protected function getListQuery() {
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(TRUE);
		$user = JFactory::getUser();
		$app = JFactory::getApplication();

		// Select the required fields from the table.
		$query->select(
				$this->getState(
						'list.select', 'a.id, a.name, a.points, a.bp_email, a.state,a.publish, a.created, a.created_by, a.modified, a.modified_by, a.ordering'
				)
		);
		$query->from('#__products AS a');

		// Filter by published state
		$published = $this->getState('filter.published');

		if (is_numeric($published)) {
			$query->where('a.publish = ' . (int) $published);
		} elseif ($published === '') {
			$query->where('(a.publish = 0 OR a.publish = 1)');
		}

		// Filter by search in title.
		$search = $this->getState('filter.search');

		if (!empty($search)) {
			if (stripos($search, 'id:') === 0) {
				$query->where('a.id = ' . (int) substr($search, 3));
			} elseif (stripos($search, 'author:') === 0) {
				$search = $db->quote('%' . $db->escape(substr($search, 7), true) . '%');
				$query->where('(a.name LIKE ' . $search . ')');
			} else {
				$search = $db->quote('%' . $db->escape($search, true) . '%');
				$query->where('(a.bp_email LIKE ' . $search . ')');
			}
		}
		// Filter by state
		$status = $this->getState('filter.status');
		if ($status == "1" || $status == "0") {
			$query->where('a.publish=' . $db->quote($status));
		}
		if ($status == "2") {
			$query->where('a.trash=' . $db->quote(1)); // 1 = trashed
		} else {
			$query->where('a.trash=' . $db->quote(0)); // 0 = active
		}
		// Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering', 'a.name');
		$orderDirn = $this->state->get('list.direction', 'asc');

//		if ($orderCol == 'a.ordering' || $orderCol == 'name') {
//			$orderCol = 'a.name ' . $orderDirn . ', a.ordering';
//		}

		$query->order($db->escape($orderCol . ' ' . $orderDirn));
		return $query;
	}

	/**
	 * Method to get a list of articles.
	 * Overridden to add a check for access levels.
	 *
	 * @return  mixed  An array of data items on success, false on failure.
	 *
	 * @since   1.6.1
	 */
	public function getItems() {
		$items = parent::getItems();
		return $items;
	}

	public function saveorder($pks = null, $order = null) {
		$table = JTable::getInstance("Item", "ProductTable");

		if (empty($pks)) {
			return JError::raiseWarning(500, JText::_($this->text_prefix . '_ERROR_NO_ITEMS_SELECTED'));
		}

		// Update ordering values
		foreach ($pks as $i => $pk) {
			$table->load((int) $pk);
			// Access checks.
			if ($table->ordering != $order[$i]) {
				$table->ordering = $order[$i];
				if (!$table->store()) {
					$this->setError($table->getError());
					return false;
				}
			}
		}

		// Clear the component's cache
		$this->cleanCache();

		return true;
	}

	function trash() {
		$app = JFactory::getApplication();
		$cid = JRequest::getVar('cid');
		JArrayHelper::toInteger($cid);
		foreach ($cid as $id) {
			$table = JTable::getInstance('Item', 'ProductTable');
			$table->load($id);
			$table->trash = 1;
			$table->publish = 0;
			$table->store();
		}
		$app->redirect('index.php?option=com_product&view=list', JText::_('COM_PRODUCT_MSG_ITEMS_MOVED_TO_TRASH'));
	}

	function restore() {
		$app = JFactory::getApplication();
		$cid = JRequest::getVar('cid');
		JArrayHelper::toInteger($cid);
		foreach ($cid as $id) {
			$table = JTable::getInstance('Item', 'ProductTable');
			$table->load($id);
			$table->trash = 0;
			$table->store();
		}
		$app->redirect('index.php?option=com_product&view=list', JText::_('COM_PRODUCT_MSG_ITEMS_RESTORED'));
	}

	function delete() {
		$app = JFactory::getApplication();
		$cid = JRequest::getVar('cid');
		JArrayHelper::toInteger($cid);
		foreach ($cid as $id) {
			//TO-DO: Delete image
			$table = JTable::getInstance('Item', 'ProductTable');
			if ($table->load($id)) {
				$table->delete($id);
			}
		}
		$app->redirect('index.php?option=com_product&view=list', JText::_('COM_PRODUCT_MSG_ITEMS_DELETED'));
	}

	function publish() {
		$app = JFactory::getApplication();
		$cid = JRequest::getVar('cid');
		foreach ($cid as $id) {
			$row = JTable::getInstance('Item', 'ProductTable');
			$row->load($id);
			$row->publish($id, 1);
		}
		$app->redirect('index.php?option=com_product&view=list');
	}

	function unpublish() {
		$app = JFactory::getApplication();
		$cid = JRequest::getVar('cid');
		foreach ($cid as $id) {
			$row = JTable::getInstance('Item', 'ProductTable');
			$row->load($id);
			$row->publish($id, 0);
		}
		$app->redirect('index.php?option=com_product&view=list');
	}

}
