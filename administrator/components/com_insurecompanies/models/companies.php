<?php

defined('_JEXEC') or die;

class InsureCompaniesModelCompanies extends JModelList {

	public function __construct($config = array()) {
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'company_name', 'c.company_name',
				'insurer_code', 'c.insurer_code'
			);
		}

		parent::__construct($config);
	}

	protected function populateState($ordering = null, $direction = null) {
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$state = $this->getUserStateFromRequest($this->context . '.filter.state', 'filter_state', 1);
		$this->setState('filter.state', $state);

		$accessId = $this->getUserStateFromRequest($this->context . '.filter.access', 'filter_access', null, 'int');
		$this->setState('filter.access', $accessId);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_insurecompanies');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('c.company_name', 'asc');
	}

	protected function getStoreId($id = '') {
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.access');

		return parent::getStoreId($id);
	}

	protected function getListQuery() {
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(TRUE);

		// Select the required fields from the table.
		$query->select('*');
		$query->from($db->quoteName('#__insure_companies') . ' AS c');

		// Filter by search in title
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			$search = $db->quote('%' . $db->escape($search, true) . '%');
			$query->where(' (c.company_name LIKE ' . $search . ') ');
		}

		$state = $this->getState("filter.state", 1);
		if ($state) {
			$query->where('state = ' . $db->quote($state));
		} else {
			$query->where('state = 1');
		}

		// Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering', "c.company_name");
		$orderDirn = $this->state->get('list.direction', "");
		$query->order($db->escape($orderCol . ' ' . $orderDirn));
		return $query;
	}

}
