<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_CedeleDepots
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * View class for a list of CedeleDepots.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_CedeleDepots
 * @since       1.5
 */
class InsureCompaniesViewCompanies extends JViewLegacy {

	protected $items;
	protected $pagination;
	protected $state;

	/**
	 * Display the view
	 *
	 * @return  void
	 */
	public function display($tpl = null) {
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');

		InsureCompaniesHelper::addSubmenu('companies');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		foreach ($this->items as $item) {
			$item->thumb = 'components/com_insurecompanies/assets/icon-image.png';
			if (strcasecmp(substr($item->logo, 0, 4), 'http') != 0 && !empty($item->logo)) {
				$item->logo = JURI::root(true) . '/' . $item->logo;
			}
			$item->preview = '<img src="' . $item->logo . '" alt="' . $this->escape($item->company_name) . '" width="100" />';
		}

		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar() {
		require_once JPATH_COMPONENT . '/helpers/insurecompanies.php';

		$state = $this->get('State');
		$canDo = InsureCompaniesHelper::getActions($state->get('filter.category_id'));
		$user = JFactory::getUser();
		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');

		JToolbarHelper::title(JText::_('COM_INSURECOMPANIES_MANAGER_COMPANIES'), 'insurecompanies.png');

		if ($canDo->get('core.create')) {
			JToolbarHelper::addNew('company.add');
		}

		if ($canDo->get('core.edit')) {
			JToolbarHelper::editList('company.edit');
		}


		if ($canDo->get('core.edit.state')) {

			//JToolbarHelper::publish('company.publish', 'JTOOLBAR_PUBLISH', true);
			//JToolbarHelper::unpublish('company.unpublish', 'JTOOLBAR_UNPUBLISH', true);
			//JToolbarHelper::archiveList('quotation.archive');
			//JToolbarHelper::checkin('quotation.checkin');
		}

		JToolbarHelper::deleteList('', 'company.delete', 'JACTION_DELETE');

//		if ($state->get('filter.state') == -2 && $canDo->get('core.delete')) {
//			JToolbarHelper::deleteList('', 'company.delete', 'JACTION_DELETE');
//		} elseif ($canDo->get('core.edit.state')) {
//			JToolbarHelper::trash('company.trash');
//		}


		// Add a batch button
		/* if ($canDo->get('core.edit'))
		  {
		  JHtml::_('bootstrap.modal', 'collapseModal');
		  $title = JText::_('JTOOLBAR_BATCH');
		  $dhtml = "<button data-toggle=\"modal\" data-target=\"#collapseModal\" class=\"btn btn-small\">
		  <i class=\"icon-checkbox-partial\" title=\"$title\"></i>
		  $title</button>";
		  $bar->appendButton('Custom', $dhtml, 'batch');
		  }
		 */
		if ($canDo->get('core.admin')) {
			JToolbarHelper::preferences('com_insurecompanies');
		}

		//JToolbarHelper::help('JHELP_COMPONENTS_insurecompaniesS_LINKS');

		JHtmlSidebar::setAction('index.php?option=com_insurecompanies&view=companies');

		/* JHtmlSidebar::addFilter(
		  JText::_('JOPTION_SELECT_insurecompanies_TYPE'),
		  'filter_insurecompanies_type',
		  JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), 'value', 'text', $this->state->get('filter.insurecompanies_type'), true)
		  );
		 */
//		$stateOptions = array();
//		$stateOptions[] = array("value" => 1, "text" => JText::_("JPUBLISHED"));
//		$stateOptions[] = array("value" => -2, "text" => JText::_("JTRASHED"));
//		JHtmlSidebar::addFilter(
//				JText::_('JOPTION_SELECT_PUBLISHED'), 'filter_state', JHtml::_('select.options', $stateOptions, 'value', 'text', $this->state->get('filter.state', 1), true)
//		);
		/*
		  JHtmlSidebar::addFilter(
		  JText::_('JOPTION_SELECT_CATEGORY'),
		  'filter_category_id',
		  JHtml::_('select.options', JHtml::_('category.options', 'com_insurecompaniess'), 'value', 'text', $this->state->get('filter.category_id'))
		  );

		  JHtmlSidebar::addFilter(
		  JText::_('JOPTION_SELECT_ACCESS'),
		  'filter_access',
		  JHtml::_('select.options', JHtml::_('access.assetgroups'), 'value', 'text', $this->state->get('filter.access'))
		  );

		  JHtmlSidebar::addFilter(
		  JText::_('JOPTION_SELECT_LANGUAGE'),
		  'filter_language',
		  JHtml::_('select.options', JHtml::_('contentlanguage.existing', true, true), 'value', 'text', $this->state->get('filter.language'))
		  );

		  JHtmlSidebar::addFilter(
		  '-' . JText::_('JSELECT') . ' ' . JText::_('JTAG') . '-',
		  'filter_tag',
		  JHtml::_('select.options', JHtml::_('tag.options', true, true), 'value', 'text', $this->state->get('filter.tag'))
		  );
		 */
	}

	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 *
	 * @since   3.0
	 */
	protected function getSortFields() {
		return array(
				//'t.insurecompanies_date' => JText::_('COM_INSUREDIYLIFE_CEDELEDEPOT_DATE')
				/* 'a.ordering' => JText::_('JGRID_HEADING_ORDERING'),
				 */
		);
	}

}
