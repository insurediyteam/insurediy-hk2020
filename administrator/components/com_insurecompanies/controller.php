<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_insurecompanies
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * Cedele Depot Controller
 *
 * @package     Joomla.Administrator
 * @subpackage  com_insurecompanies
 * @since       1.5
 */
class InsureCompaniesController extends JControllerLegacy
{
	protected $default_view = 'companies';
	/**
	 * Method to display a view.
	 *
	 * @param   boolean			$cachable	If true, the view output will be cached
	 * @param   array  $urlparams	An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
	 *
	 * @return  JController		This object to support chaining.
	 * @since   1.5
	 */
	public function display($cachable = false, $urlparams = false)
	{

		$view   = $this->input->get('view', 'companies');
		$layout = $this->input->get('layout', 'default');
		$id     = $this->input->getInt('id');

		// Check for edit form.
		/*if ($view == 'cedeledepot' && $layout == 'edit' && !$this->checkEditId('com_insurecompanies.edit.cedeledepot', $id))
		{
			// Somehow the person just went to the form - we don't allow that.
			$this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_UNHELD_ID', $id));
			$this->setMessage($this->getError(), 'error');
			$this->setRedirect(JRoute::_('index.php?option=com_insurecompanies&view=cedeledepots', false));

			return false;
		}*/
			
		parent::display();

		return $this;
		
	}
}
