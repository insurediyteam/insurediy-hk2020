<?php
/**
* @package RSSeo!
* @copyright (C) 2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/copyleft/gpl.html
*/
defined('_JEXEC') or die('Restricted access');

class rsseoModelKeywords extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param	array	An optional associative array of configuration settings.
	 * @see		JController
	 * @since	1.6
	 */
	public function __construct($config = array()) {
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'id', 'keyword', 'importance',
				'position', 'date'
			);
		}

		parent::__construct($config);
	}
	
	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return	void
	 * @since	1.6
	 */
	protected function populateState($ordering = null, $direction = null) {
		$this->setState('filter.search', $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search'));
		$this->setState('filter.importance', $this->getUserStateFromRequest($this->context.'.filter.importance', 'filter_importance'));
		
		// List state information.
		parent::populateState('importance', 'desc');
	}
	
	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return	JDatabaseQuery
	 * @since	1.6
	 */
	protected function getListQuery() {
		$db 	= JFactory::getDBO();
		$query 	= $db->getQuery(true);
		
		// Select fields
		$query->select('*');
		
		// Select from table
		$query->from($db->qn('#__rsseo_keywords'));
		
		// Filter by importance
		if ($importance = $this->getState('filter.importance')) {
			$query->where($db->qn('importance').' = '.$db->q($importance));
		}
		
		// Filter by search in title
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			$search = $db->q('%'.$db->escape($search, true).'%');
			$query->where($db->qn('keyword').' LIKE '.$search.' ');
		}
		
		// Add the list ordering clause
		$listOrdering = $this->getState('list.ordering', 'importance');
		$listDirn = $db->escape($this->getState('list.direction', 'desc'));
		$query->order($db->qn($listOrdering).' '.$listDirn);
		return $query;
	}
	
	/**
	 * Method to get the items list.
	 *
	 * @return	mixed	An array of data items on success, false on failure.
	 * @since	1.6.1
	 */
	public function getItems() {
		$items	= parent::getItems();
		
		foreach ($items as $i => $item) {
			if (!empty($item->lastposition)) {
				if ($item->position > $item->lastposition)
					$items[$i]->badge = 'important';
				else if ($item->position < $item->lastposition) 
					$items[$i]->badge = 'success';
				else if ($item->position == $item->lastposition)
					$items[$i]->badge = '';
			} else $items[$i]->badge = '';
		}
		
		return $items;
	}
	
	/**
	 *	Method to set the side bar
	 */
	public function getSidebar() {
		if (rsseoHelper::isJ3()) {
			JHtmlSidebar::addFilter(
				JText::_('COM_RSSEO_KEYWORDS_IMPORTANCE_SELECT'),
				'filter_importance',
				JHtml::_('select.options', $this->getFilterOptions(), 'value', 'text', $this->getState('filter.importance'), true)
			);
			
			return JHtmlSidebar::render();
		}
	}
	
	/**
	 *	Method to set the filter bar
	 */
	public function getFilterBar() {
		$config	 = rsseoHelper::getConfig();
		$options = array();
		$options['search'] = array(
			'label' => JText::_('JSEARCH_FILTER'),
			'value' => $this->getState('filter.search')
		);
		$options['listDirn']  = $this->getState('list.direction', 'importance');
		$options['listOrder'] = $this->getState('list.ordering', 'desc');
		$options['limitBox']  = $this->getPagination()->getLimitBox();
		$options['sortFields'] = array(
			JHtml::_('select.option', 'id', JText::_('COM_RSSEO_GLOBAL_SORT_ID')),
			JHtml::_('select.option', 'keyword', JText::_('COM_RSSEO_KEYWORDS_KEYWORD')),
			JHtml::_('select.option', 'importance', JText::_('COM_RSSEO_KEYWORDS_IMPORTANCE')),
			JHtml::_('select.option', 'position', JText::_('COM_RSSEO_KEYWORDS_POSITION')),
			JHtml::_('select.option', 'date', JText::_('COM_RSSEO_KEYWORDS_DATE'))
		);
		
		if (!rsseoHelper::isJ3()) {
			$options['rightItems'] = array(
				array('input' => JHtml::_('icon.filter', 'filter_importance', $this->getFilterOptions(), JText::_('COM_RSSEO_KEYWORDS_IMPORTANCE_SELECT'), $this->getState('filter.importance')))
			);
		}
		
		$bar = new RSFilterBar($options);
		return $bar;
	}
	
	protected function getFilterOptions() {
		return array(
			JHtml::_('select.option', 'low', JText::_('COM_RSSEO_KEYWORD_IMPORTANCE_LOW')),
			JHtml::_('select.option', 'relevant', JText::_('COM_RSSEO_KEYWORD_IMPORTANCE_RELEVANT')),
			JHtml::_('select.option', 'important', JText::_('COM_RSSEO_KEYWORD_IMPORTANCE_IMPORTANT')),
			JHtml::_('select.option', 'critical', JText::_('COM_RSSEO_KEYWORD_IMPORTANCE_CRITICAL'))
		);
	}
}