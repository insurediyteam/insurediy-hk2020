<?php
/**
* @package RSSeo!
* @copyright (C) 2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/copyleft/gpl.html
*/
defined('_JEXEC') or die('Restricted access');

class rsseoModelCompetitors extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param	array	An optional associative array of configuration settings.
	 * @see		JController
	 * @since	1.6
	 */
	public function __construct($config = array()) {
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'id', 'name', 'pagerank', 'age',
				'googleb', 'googlep', 'alexa',
				'bingb', 'bingp', 'technorati',
				'dmoz', 'date', 'googler'
			);
		}

		parent::__construct($config);
	}
	
	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return	void
	 * @since	1.6
	 */
	protected function populateState($ordering = null, $direction = null) {
		$this->setState('filter.search', $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search'));
		$this->setState('filter.parent', $this->getUserStateFromRequest($this->context.'.filter.parent', 'filter_parent', 0));
		
		// List state information.
		parent::populateState('id', 'asc');
	}
	
	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return	JDatabaseQuery
	 * @since	1.6
	 */
	protected function getListQuery() {
		$db 	= JFactory::getDBO();
		$query 	= $db->getQuery(true);
		
		// Select fields
		$query->select('*');
		
		// Select from table
		$query->from($db->qn('#__rsseo_competitors'));
		
		// Get parents only
		$parent = $this->getState('filter.parent');
		$query->where($db->qn('parent_id').' = '.(int) $parent);
		
		// Filter by search in title
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			$search = $db->q('%'.$db->escape($search, true).'%');
			$query->where('('.$db->qn('name').' LIKE '.$search.' OR '.$db->qn('tags').' LIKE '.$search.')');
		}
		
		// Add the list ordering clause
		$listOrdering = $this->getState('list.ordering', 'id');
		$listDirn = $db->escape($this->getState('list.direction', 'asc'));
		$query->order($db->escape($listOrdering).' '.$listDirn);
		return $query;
	}
	
	/**
	 * Method to get the items list.
	 *
	 * @return	mixed	An array of data items on success, false on failure.
	 * @since	1.6.1
	 */
	public function getItems() {
		$db		= JFactory::getDBO();
		$query	= $db->getQuery(true);
		$items	= parent::getItems();
		$config = rsseoHelper::getConfig();
		
		foreach ($items as $i => $item) {
			if (!$this->getState('filter.parent')) {			
				// Get history
				$query->clear();
				$query->select('*')
					->from($db->qn('#__rsseo_competitors'))
					->where($db->qn('parent_id').' = '.(int) $item->id)
					->order($db->qn('date').' DESC');
				$db->setQuery($query,0,2);
				$history = $db->loadObjectList();
				
				if(isset($history[1])) {
					$compare = $history[1]; 
				} else $compare = $history[0];
				
				if (empty($compare)) {
					$compare = $item;
				}
				
				// Google page rank
				if ($config->enable_pr) {
					if ($compare->pagerank < $item->pagerank) 
						$items[$i]->pagerankbadge = 'success';
					else if ($compare->pagerank > $item->pagerank)
						$items[$i]->pagerankbadge = 'important';
					else if ($compare->pagerank == $item->pagerank) 
						$items[$i]->pagerankbadge = '';
				} else $items[$i]->pagerankbadge = '';
				
				// Google pages
				if ($config->enable_googlep) {
					if ($compare->googlep < $item->googlep) 
						$items[$i]->googlepbadge = 'success';
					else if ($compare->googlep > $item->googlep)
						$items[$i]->googlepbadge = 'important';
					else if ($compare->googlep == $item->googlep) 
						$items[$i]->googlepbadge = '';
				} else $items[$i]->googlepbadge = '';
				
				// Google backlinks
				if ($config->enable_googleb) {
					if ($compare->googleb < $item->googleb) 
						$items[$i]->googlebbadge = 'success';
					else if ($compare->googleb > $item->googleb)
						$items[$i]->googlebbadge = 'important';
					else if ($compare->googleb == $item->googleb) 
						$items[$i]->googlebbadge = '';
				} else $items[$i]->googlebbadge = '';
				
				// Google similar pages
				if ($config->enable_googler) {
					if ($compare->googler < $item->googler) 
						$items[$i]->googlerbadge = 'success';
					else if ($compare->googler > $item->googler)
						$items[$i]->googlerbadge = 'important';
					else if ($compare->googler == $item->googler) 
						$items[$i]->googlerbadge = '';
				} else $items[$i]->googlerbadge = '';
				
				// Bing pages
				if ($config->enable_bingp) {
					if ($compare->bingp < $item->bingp) 
						$items[$i]->bingpbadge = 'success';
					else if ($compare->bingp > $item->bingp)
						$items[$i]->bingpbadge = 'important';
					else if ($compare->bingp == $item->bingp) 
						$items[$i]->bingpbadge = '';
				} else $items[$i]->bingpbadge = '';
				
				// Bing backlinks
				if ($config->enable_bingb) {
					if ($compare->bingb < $item->bingb) 
						$items[$i]->bingbbadge = 'success';
					else if ($compare->bingb > $item->bingb)
						$items[$i]->bingbbadge = 'important';
					else if ($compare->bingb == $item->bingb) 
						$items[$i]->bingbbadge = '';
				} else $items[$i]->bingbbadge = '';
					
				// Alexa page rank
				if ($config->enable_alexa) {
					if ($compare->alexa < $item->alexa) 
						$items[$i]->alexabadge = 'important';
					else if ($compare->alexa > $item->alexa)
						$items[$i]->alexabadge = 'success';
					else if ($compare->alexa == $item->alexa) 
						$items[$i]->alexabadge = '';
				} else $items[$i]->alexabadge = '';
				
				// Technorati rank
				if ($config->enable_tehnorati) {
					if ($compare->technorati < $item->technorati) 
						$items[$i]->technoratibadge = 'success';
					else if ($compare->technorati > $item->technorati)
						$items[$i]->technoratibadge = 'important';
					else if ($compare->technorati == $item->technorati) 
						$items[$i]->technoratibadge = '';
				} else $items[$i]->technoratibadge = '';
				
				//Dmoz
				if ($config->enable_dmoz) {
					if ($item->dmoz == -1)
						$items[$i]->dmozbadge = '';
					else if ($item->dmoz == 0)
						$items[$i]->dmozbadge = 'important';
					else if ($item->dmoz == 1)
						$items[$i]->dmozbadge = 'success';
				} else $items[$i]->dmozbadge = '';
			
			} else {
				$items[$i]->pagerankbadge = '';
				$items[$i]->googlepbadge = '';
				$items[$i]->googlebbadge = '';
				$items[$i]->googlerbadge = '';
				$items[$i]->bingpbadge = '';
				$items[$i]->bingbbadge = '';
				$items[$i]->alexabadge = '';
				$items[$i]->technoratibadge = '';
				$items[$i]->dmozbadge = '';
			}
			
			// Convert number
			$items[$i]->googlep = $items[$i]->googlep == -1 ? '-' : number_format($items[$i]->googlep, 0, '', '.');
			$items[$i]->googleb = $items[$i]->googleb == -1 ? '-' : number_format($items[$i]->googleb, 0, '', '.');
			$items[$i]->googler = $items[$i]->googler == -1 ? '-' : number_format($items[$i]->googler, 0, '', '.');
			$items[$i]->bingp = $items[$i]->bingp == -1 ? '-' : number_format($items[$i]->bingp, 0, '', '.');
			$items[$i]->bingb = $items[$i]->bingb == -1 ? '-' : number_format($items[$i]->bingb, 0, '', '.');
			$items[$i]->alexa = $items[$i]->alexa == -1 ? '-' : number_format($items[$i]->alexa, 0, '', '.');
			$items[$i]->technorati = $items[$i]->technorati == -1 ? '-' : number_format($items[$i]->technorati, 0, '', '.');
		}
		
		return $items;
	}
	
	/**
	 * Method to get competitor name.
	 *
	 * @return	string	The name of the competitor.
	 */
	public function getCompetitor() {
		$db = JFactory::getDBO();
		$query = $db->getQuery(true);
		
		$query->select($db->qn('name'))
			->from($db->qn('#__rsseo_competitors'))
			->where($db->qn('id').' = '.(int) $this->getState('filter.parent'));
		$db->setQuery($query);
		return $db->loadResult();
	}
	
	/**
	 *	Method to set the side bar
	 */
	public function getSidebar() {
		if (rsseoHelper::isJ3()) {
			return JHtmlSidebar::render();
		}
	}
	
	/**
	 *	Method to set the filter bar
	 */
	public function getFilterBar() {
		$config	 = rsseoHelper::getConfig();
		$options = array();
		$options['search'] = array(
			'label' => JText::_('JSEARCH_FILTER'),
			'value' => $this->getState('filter.search')
		);
		$options['listDirn']  = $this->getState('list.direction', 'id');
		$options['listOrder'] = $this->getState('list.ordering', 'asc');
		$options['limitBox']  = $this->getPagination()->getLimitBox();
		$options['sortFields'] = array(
			JHtml::_('select.option', 'id', JText::_('COM_RSSEO_GLOBAL_SORT_ID')),
			JHtml::_('select.option', 'name', JText::_('COM_RSSEO_COMPETITORS_COMPETITOR'))
		);
		
		if ($config->enable_age) $options['sortFields'][] = JHtml::_('select.option', 'age', JText::_('COM_RSSEO_COMPETITORS_AGE'));
		if ($config->enable_pr) $options['sortFields'][] = JHtml::_('select.option', 'pagerank', JText::_('COM_RSSEO_COMPETITORS_PAGE_RANK'));
		if ($config->enable_googlep) $options['sortFields'][] = JHtml::_('select.option', 'googlep', JText::_('COM_RSSEO_COMPETITORS_GOOGLE_PAGES'));
		if ($config->enable_googleb) $options['sortFields'][] = JHtml::_('select.option', 'googleb', JText::_('COM_RSSEO_COMPETITORS_GOOGLE_BACKLINKS'));
		if ($config->enable_googler) $options['sortFields'][] = JHtml::_('select.option', 'googler', JText::_('COM_RSSEO_COMPETITORS_GOOGLE_RELATED'));
		if ($config->enable_bingp) $options['sortFields'][] = JHtml::_('select.option', 'bingp', JText::_('COM_RSSEO_COMPETITORS_BING_PAGES'));
		if ($config->enable_bingb) $options['sortFields'][] = JHtml::_('select.option', 'bingb', JText::_('COM_RSSEO_COMPETITORS_BING_BACKLINKS'));
		if ($config->enable_alexa) $options['sortFields'][] = JHtml::_('select.option', 'alexa', JText::_('COM_RSSEO_COMPETITORS_ALEXA_RANK'));
		if ($config->enable_tehnorati) $options['sortFields'][] = JHtml::_('select.option', 'technorati', JText::_('COM_RSSEO_COMPETITORS_TECHNORATI_RANK'));
		if ($config->enable_dmoz) $options['sortFields'][] = JHtml::_('select.option', 'dmoz', JText::_('COM_RSSEO_COMPETITORS_DMOZ_RANK'));
		$options['sortFields'][] = JHtml::_('select.option', 'date', JText::_('COM_RSSEO_COMPETITORS_DATE'));
		
		$bar = new RSFilterBar($options);
		return $bar;
	}
}