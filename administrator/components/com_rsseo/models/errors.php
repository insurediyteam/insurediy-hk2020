<?php
/**
* @package RSSeo!
* @copyright (C) 2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/copyleft/gpl.html
*/
defined('_JEXEC') or die('Restricted access');

class rsseoModelErrors extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param	array	An optional associative array of configuration settings.
	 * @see		JController
	 * @since	1.6
	 */
	public function __construct($config = array()) {
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'id', 'name', 'error',
				'published'
			);
		}

		parent::__construct($config);
	}
	
	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return	void
	 * @since	1.6
	 */
	protected function populateState($ordering = null, $direction = null) {
		$this->setState('filter.search', $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search'));
		$this->setState('filter.published', $this->getUserStateFromRequest($this->context.'.filter.published', 'filter_published', ''));
		
		// List state information.
		parent::populateState('id', 'asc');
	}
	
	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return	JDatabaseQuery
	 * @since	1.6
	 */
	protected function getListQuery() {
		$db 	= JFactory::getDBO();
		$query 	= $db->getQuery(true);
		
		// Select fields
		$query->select('*');
		
		// Select from table
		$query->from($db->qn('#__rsseo_errors'));
		
		// Filter by published state
		$published = $this->getState('filter.published');
		if (is_numeric($published)) {
			$query->where($db->qn('published').' = ' . (int) $published);
		}
		elseif ($published === '') {
			$query->where('('.$db->qn('published').' = 0 OR '.$db->qn('published').' = 1)');
		}
		
		// Filter by search in title
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			$search = $db->q('%'.$db->escape($search, true).'%');
			$query->where($db->qn('name').' LIKE '.$search);
		}
		
		// Add the list ordering clause
		$listOrdering = $this->getState('list.ordering', 'id');
		$listDirn = $db->escape($this->getState('list.direction', 'asc'));
		$query->order($db->qn($listOrdering).' '.$listDirn);
		return $query;
	}
	
	/**
	 *	Method to set the side bar
	 */
	public function getSidebar() {
		if (rsseoHelper::isJ3()) {
			JHtmlSidebar::addFilter(
				JText::_('JOPTION_SELECT_PUBLISHED'),
				'filter_published',
				JHtml::_('select.options', JHtml::_('jgrid.publishedOptions', array('published' => true, 'unpublished' => true, 'archived' => false, 'trash' => false, 'all' => false)), 'value', 'text', $this->getState('filter.published'), true)
			);
			
			return JHtmlSidebar::render();
		}
	}
	
	/**
	 *	Method to set the filter bar
	 */
	public function getFilterBar() {
		$config	 = rsseoHelper::getConfig();
		$options = array();
		$options['search'] = array(
			'label' => JText::_('JSEARCH_FILTER'),
			'value' => $this->getState('filter.search')
		);
		$options['listDirn']  = $this->getState('list.direction', 'id');
		$options['listOrder'] = $this->getState('list.ordering', 'asc');
		$options['limitBox']  = $this->getPagination()->getLimitBox();
		$options['sortFields'] = array(
			JHtml::_('select.option', 'id', JText::_('COM_RSSEO_GLOBAL_SORT_ID')),
			JHtml::_('select.option', 'name', JText::_('COM_RSSEO_ERRORS_NAME')),
			JHtml::_('select.option', 'error', JText::_('COM_RSSEO_ERRORS_ERROR_CODE')),
			JHtml::_('select.option', 'published', JText::_('COM_RSSEO_PAGES_STATUS'))
		);
		
		if (!rsseoHelper::isJ3()) {
			$options['rightItems'] = array(
				array('input' => JHtml::_('icon.filter', 'filter_published', JHtml::_('jgrid.publishedOptions', array('published' => true, 'unpublished' => true, 'archived' => false, 'trash' => false, 'all' => false)), JText::_('JOPTION_SELECT_PUBLISHED'), $this->getState('filter.published')))
			);
		}
		
		$bar = new RSFilterBar($options);
		return $bar;
	}
}