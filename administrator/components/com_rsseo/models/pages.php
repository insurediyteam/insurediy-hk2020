<?php
/**
* @package RSSeo!
* @copyright (C) 2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/copyleft/gpl.html
*/
defined('_JEXEC') or die('Restricted access');

class rsseoModelPages extends JModelList
{	
	/**
	 * Constructor.
	 *
	 * @param	array	An optional associative array of configuration settings.
	 * @see		JController
	 * @since	1.6
	 */
	public function __construct($config = array()) {
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'id', 'url', 'title',
				'level', 'grade', 'crawled',
				'date', 'hits'
			);
		}

		parent::__construct($config);
	}
	
	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return	void
	 * @since	1.6
	 */
	protected function populateState($ordering = null, $direction = null) {
		$this->setState('filter.search', $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search'));
		$this->setState('filter.published', $this->getUserStateFromRequest($this->context.'.filter.published', 'filter_published', ''));
		$this->setState('filter.level', $this->getUserStateFromRequest($this->context.'.filter.level', 'filter_level', ''));
		$this->setState('filter.insitemap', $this->getUserStateFromRequest($this->context.'.filter.insitemap', 'filter_insitemap', ''));
		$this->setState('filter.modified', $this->getUserStateFromRequest($this->context.'.filter.modified', 'filter_modified', ''));
		$this->setState('filter.md5title', JFactory::getApplication()->input->get('md5title'));
		$this->setState('filter.md5descr', JFactory::getApplication()->input->get('md5descr'));
		
		// List state information.
		parent::populateState('level', 'asc');
	}
	
	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return	JDatabaseQuery
	 * @since	1.6
	 */
	protected function getListQuery() {
		$db 	= JFactory::getDBO();
		$query 	= $db->getQuery(true);
		
		// Select fields
		$query->select('*');
		
		// Select from table
		$query->from($db->qn('#__rsseo_pages'));
		
		// Filter by level.
		if ($level = $this->getState('filter.level')) {
			$query->where($db->qn('level').' = ' . (int) $level);
		}
		
		// Filter by sitemap.
		$insitemap = $this->getState('filter.insitemap');
		if (is_numeric($insitemap)) {
			$query->where($db->qn('insitemap').' = ' . (int) $insitemap);
		}
		
		// Filter by modified page.
		$modified = $this->getState('filter.modified');
		if (is_numeric($modified)) {
			$query->where($db->qn('modified').' = ' . (int) $modified);
		}
		
		// Filter by page title.
		if ($md5title = $this->getState('filter.md5title')) {
			$query->where('MD5('.$db->qn('title').') = ' . $db->quote($md5title));
		}
		
		// Filter by page description.
		if ($md5descr = $this->getState('filter.md5descr')) {
			$query->where('MD5('.$db->qn('description').') = ' . $db->quote($md5descr));
		}
		
		// Filter by published state
		$published = $this->getState('filter.published');
		if (is_numeric($published)) {
			$query->where($db->qn('published').' = ' . (int) $published);
		}
		elseif ($published === '') {
			$query->where('('.$db->qn('published').' = 0 OR '.$db->qn('published').' = 1)');
		}
		
		// Filter by search in title
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			$search = $db->q('%'.$db->escape($search, true).'%');
			$query->where('('.$db->qn('url').' LIKE '.$search.' OR '.$db->qn('title').' LIKE '.$search.')');
		}
		
		// Add the list ordering clause
		$listOrdering = $this->getState('list.ordering', 'level');
		$listDirn = $db->escape($this->getState('list.direction', 'asc'));
		$query->order($db->qn($listOrdering).' '.$listDirn);
		return $query;
	}
	
	/**
	 * Method to get the items list.
	 *
	 * @return	mixed	An array of data items on success, false on failure.
	 * @since	1.6.1
	 */
	public function getItems() {
		$items	= parent::getItems();
		
		foreach ($items as $i => $item) {
			switch($item->grade) {
				case ($item->grade >= 0 && $item->grade < 33): 
					$items[$i]->color = 'red'; 
				break;
				
				case ($item->grade >= 33 && $item->grade < 66):
					$items[$i]->color = 'orange'; 
				break;
				
				case -1:
					$items[$i]->color = '';
				break;
				
				default: 
					$items[$i]->color = 'green'; 
				break;
			}
		}
		
		return $items;
	}
	
	/**
	 *	Method to set the side bar
	 */
	public function getSidebar() {
		if (rsseoHelper::isJ3()) {
			JHtmlSidebar::addFilter(
				JText::_('JOPTION_SELECT_MAX_LEVELS'),
				'filter_level',
				JHtml::_('select.options', $this->getLevels(), 'value', 'text', $this->getState('filter.level'), true)
			);
			JHtmlSidebar::addFilter(
				JText::_('JOPTION_SELECT_PUBLISHED'),
				'filter_published',
				JHtml::_('select.options', $this->getStates(), 'value', 'text', $this->getState('filter.published'), true)
			);
			JHtmlSidebar::addFilter(
				JText::_('COM_RSSEO_SITEMAP_FILTER'),
				'filter_insitemap',
				JHtml::_('select.options', $this->getYesNo(), 'value', 'text', $this->getState('filter.insitemap'), true)
			);
			JHtmlSidebar::addFilter(
				JText::_('COM_RSSEO_PAGE_MODIFIED_FILTER'),
				'filter_modified',
				JHtml::_('select.options', $this->getYesNo(), 'value', 'text', $this->getState('filter.modified'), true)
			);
			
			return JHtmlSidebar::render();
		}
	}
	
	/**
	 *	Method to set the filter bar
	 */
	public function getFilterBar() {
		$config	 = rsseoHelper::getConfig();
		$options = array();
		$options['search'] = array(
			'label' => JText::_('JSEARCH_FILTER'),
			'value' => $this->getState('filter.search')
		);
		$options['listDirn']  = $this->getState('list.direction', 'level');
		$options['listOrder'] = $this->getState('list.ordering', 'asc');
		$options['limitBox']  = $this->getPagination()->getLimitBox();
		$options['sortFields'] = array(
			JHtml::_('select.option', 'id', JText::_('COM_RSSEO_GLOBAL_SORT_ID')),
			JHtml::_('select.option', 'url', JText::_('COM_RSSEO_PAGES_URL')),
			JHtml::_('select.option', 'title', JText::_('COM_RSSEO_PAGES_TITLE')),
			JHtml::_('select.option', 'level', JText::_('COM_RSSEO_PAGES_LEVEL')),
			JHtml::_('select.option', 'grade', JText::_('COM_RSSEO_PAGES_GRADE')),
			JHtml::_('select.option', 'crawled', JText::_('COM_RSSEO_PAGES_CRAWLED')),
			JHtml::_('select.option', 'hits', JText::_('COM_RSSEO_HITS')),
			JHtml::_('select.option', 'date', JText::_('COM_RSSEO_PAGES_DATE'))
		);
		
		if (!rsseoHelper::isJ3()) {
			$options['rightItems'] = array(
				array('input' => JHtml::_('icon.filter', 'filter_level', $this->getLevels(), JText::_('JOPTION_SELECT_MAX_LEVELS'), $this->getState('filter.level'))),
				array('input' => JHtml::_('icon.filter', 'filter_published', $this->getStates(), JText::_('JOPTION_SELECT_PUBLISHED'), $this->getState('filter.published'))),
				array('input' => JHtml::_('icon.filter', 'filter_insitemap', $this->getYesNo(), JText::_('COM_RSSEO_SITEMAP_FILTER'), $this->getState('filter.insitemap'))),
				array('input' => JHtml::_('icon.filter', 'filter_modified', $this->getYesNo(), JText::_('COM_RSSEO_PAGE_MODIFIED_FILTER'), $this->getState('filter.modified')))
			);
		}
		
		$bar = new RSFilterBar($options);
		return $bar;
	}
	
	/**
	 *	Method to get filter levels
	 */
	protected function getLevels() {
		$options	= array();
		$options[]	= JHtml::_('select.option', '127', JText::_('COM_RSSEO_GLOBAL_UNDEFINED'));
		$options[]	= JHtml::_('select.option', '1', JText::_('J1'));
		$options[]	= JHtml::_('select.option', '2', JText::_('J2'));
		$options[]	= JHtml::_('select.option', '3', JText::_('J3'));
		$options[]	= JHtml::_('select.option', '4', JText::_('J4'));
		$options[]	= JHtml::_('select.option', '5', JText::_('J5'));
		$options[]	= JHtml::_('select.option', '6', JText::_('J6'));
		$options[]	= JHtml::_('select.option', '7', JText::_('J7'));
		$options[]	= JHtml::_('select.option', '8', JText::_('J8'));
		$options[]	= JHtml::_('select.option', '9', JText::_('J9'));
		$options[]	= JHtml::_('select.option', '10', JText::_('J10'));
		
		return $options;
	}
	
	/**
	 *	Method to get a Yes/No filter option
	 */
	protected function getYesNo() {
		$options   = array();
		$options[] = JHtml::_('select.option', '1', JText::_('JYES'));
		$options[] = JHtml::_('select.option', '0', JText::_('JNO'));
		
		return $options;
	}
	
	/**
	 *	Method to get the pages states
 	 */
	protected function getStates() {
		$states   = JHtml::_('jgrid.publishedOptions', array('published' => true, 'unpublished' => true, 'archived' => false, 'trash' => false, 'all' => false));
		$states[] = JHtml::_('select.option', '-1', JText::_('COM_RSSEO_GLOBAL_INVALID'));
		
		return $states;
	}
}