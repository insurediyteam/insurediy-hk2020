<?php
/**
* @package RSSeo!
* @copyright (C) 2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/copyleft/gpl.html
*/
defined('_JEXEC') or die('Restricted access');

class rsseoModelErrorlinks extends JModelList
{
	/**
	 * Constructor.
	 *
	 * @param	array	An optional associative array of configuration settings.
	 * @see		JController
	 * @since	1.6
	 */
	public function __construct($config = array()) {
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'id', 'url',
				'count', 'code'
			);
		}

		parent::__construct($config);
	}
	
	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @return	void
	 * @since	1.6
	 */
	protected function populateState($ordering = null, $direction = null) {
		$this->setState('filter.search', $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search'));
		
		// List state information.
		parent::populateState('id', 'asc');
	}
	
	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return	JDatabaseQuery
	 * @since	1.6
	 */
	protected function getListQuery() {
		$db 	= JFactory::getDBO();
		$query 	= $db->getQuery(true);
		
		// Select fields
		$query->select('*');
		
		// Select from table
		$query->from($db->qn('#__rsseo_error_links'));
		
		// Filter by search in title
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			$search = $db->q('%'.$db->escape($search, true).'%');
			$query->where($db->qn('url').' LIKE '.$search);
		}
		
		// Add the list ordering clause
		$listOrdering = $this->getState('list.ordering', 'id');
		$listDirn = $db->escape($this->getState('list.direction', 'asc'));
		$query->order($db->qn($listOrdering).' '.$listDirn);
		return $query;
	}
	
	public function delete($pks) {
		$db 	= JFactory::getDBO();
		$query 	= $db->getQuery(true);
		
		$query->delete($db->qn('#__rsseo_error_links'))
			->where($db->qn('id').' IN ('.implode(',',$pks).')');
		$db->setQuery($query);
		$db->execute();
	}
	
	/**
	 *	Method to set the side bar
	 */
	public function getSidebar() {
		if (rsseoHelper::isJ3()) {
			return JHtmlSidebar::render();
		}
	}
	
	/**
	 *	Method to set the filter bar
	 */
	public function getFilterBar() {
		$config	 = rsseoHelper::getConfig();
		$options = array();
		$options['search'] = array(
			'label' => JText::_('JSEARCH_FILTER'),
			'value' => $this->getState('filter.search')
		);
		$options['listDirn']  = $this->getState('list.direction', 'id');
		$options['listOrder'] = $this->getState('list.ordering', 'asc');
		$options['limitBox']  = $this->getPagination()->getLimitBox();
		$options['sortFields'] = array(
			JHtml::_('select.option', 'id', JText::_('COM_RSSEO_GLOBAL_SORT_ID')),
			JHtml::_('select.option', 'url', JText::_('COM_RSSEO_ERROR_LINK_URL')),
			JHtml::_('select.option', 'code', JText::_('COM_RSSEO_ERROR_LINK_CODE')),
			JHtml::_('select.option', 'count', JText::_('COM_RSSEO_ERROR_LINK_URL_COUNT'))
		);
		
		$bar = new RSFilterBar($options);
		return $bar;
	}
}