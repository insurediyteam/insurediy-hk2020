CREATE TABLE IF NOT EXISTS `#__rsseo_broken_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `url` varchar(500) CHARACTER SET utf8 NOT NULL,
  `code` varchar(10) CHARACTER SET utf8 NOT NULL,
  `published` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `pid` (`pid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `#__rsseo_competitors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `age` int(11) NOT NULL,
  `pagerank` int(11) NOT NULL DEFAULT '-1',
  `alexa` int(11) NOT NULL DEFAULT '-1',
  `technorati` int(11) NOT NULL DEFAULT '-1',
  `googlep` int(11) NOT NULL DEFAULT '-1',
  `bingp` int(11) NOT NULL DEFAULT '-1',
  `googleb` int(11) NOT NULL DEFAULT '-1',
  `bingb` int(11) NOT NULL DEFAULT '-1',
  `googler` int(11) NOT NULL DEFAULT '-1',
  `dmoz` int(1) NOT NULL DEFAULT '-1',
  `date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `tags` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__rsseo_errors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `error` int(5) NOT NULL,
  `type` tinyint(1) NOT NULL,
  `url` varchar(500) NOT NULL,
  `layout` text NOT NULL,
  `published` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__rsseo_error_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(500) NOT NULL,
  `code` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `#__rsseo_keywords` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `keyword` varchar(255) NOT NULL,
  `importance` enum('low','relevant','important','critical') NOT NULL,
  `position` int(11) NOT NULL,
  `lastposition` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `bold` int(2) NOT NULL,
  `underline` int(2) NOT NULL,
  `limit` int(3) NOT NULL,
  `attributes` text NOT NULL,
  `link` text NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `Keyword` (`keyword`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `#__rsseo_pages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(333) NOT NULL,
  `title` text NOT NULL,
  `keywords` text NOT NULL,
  `keywordsdensity` text NOT NULL,
  `description` text NOT NULL,
  `sitemap` tinyint(1) NOT NULL,
  `insitemap` int(2) NOT NULL,
  `crawled` tinyint(1) NOT NULL,
  `date` datetime NOT NULL,
  `modified` int(3) NOT NULL,
  `level` tinyint(4) NOT NULL,
  `grade` float(10,2) NOT NULL DEFAULT '-1.00',
  `params` text NOT NULL,
  `densityparams` text NOT NULL,
  `canonical` varchar(500) NOT NULL,
  `robots` varchar(255) NOT NULL,
  `frequency` varchar(255) NOT NULL,
  `priority` varchar(255) NOT NULL,
  `imagesnoalt` text NOT NULL,
  `imagesnowh` text NOT NULL,
  `hits` int(11) NOT NULL,
  `custom` text NOT NULL,
  `published` tinyint(2) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `PageURL` (`url`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `#__rsseo_redirects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from` varchar(255) NOT NULL,
  `to` varchar(255) NOT NULL,
  `type` enum('301','302') NOT NULL,
  `hits` int(11) NOT NULL,
  `published` int(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__rsseo_redirects_referer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rid` int(11) NOT NULL,
  `referer` varchar(500) NOT NULL,
  `url` varchar(500) NOT NULL,
  `date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `rid` (`rid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__rsseo_statistics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` datetime NOT NULL,
  `age` varchar(255) NOT NULL,
  `pagerank` int(11) NOT NULL,
  `googlep` int(11) NOT NULL,
  `googleb` int(11) NOT NULL,
  `googler` int(11) NOT NULL,
  `bingp` int(11) NOT NULL,
  `bingb` int(11) NOT NULL,
  `alexa` int(11) NOT NULL,
  `gplus` int(11) NOT NULL,
  `twitter` int(11) NOT NULL,
  `fb_share_count` int(11) NOT NULL,
  `fb_like_count` int(11) NOT NULL,
  `linkedin` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

INSERT IGNORE INTO `#__rsseo_pages` (`id`, `url`, `title`, `keywords`, `keywordsdensity`, `description`, `sitemap`, `insitemap`, `crawled`, `date`, `modified`, `level`, `grade`, `params`, `densityparams`, `canonical`, `robots`, `frequency`, `priority`, `imagesnoalt`, `imagesnowh`, `published`) VALUES (1, '', '', '', '', '', 0, 0, 0, NOW(), 0, 0, 0, '', '', '', '', '', '', '', '', 1);

INSERT IGNORE INTO `#__rsseo_statistics` (`id`, `date`, `age`, `pagerank`, `googlep`, `googleb`, `googler`, `bingp`, `bingb`, `alexa`, `gplus`, `twitter`, `fb_share_count`, `fb_like_count`, `linkedin`) VALUES(1, '0000-00-00 00:00:00', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);