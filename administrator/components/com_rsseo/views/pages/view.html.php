<?php
/**
* @package RSSeo!
* @copyright (C) 2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/copyleft/gpl.html
*/
defined('_JEXEC') or die('Restricted access');

class rsseoViewPages extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;
	protected $config;
	
	public function display($tpl = null) {
		$this->items 		= $this->get('Items');
		$this->pagination 	= $this->get('Pagination');
		$this->state 		= $this->get('State');
		$this->sidebar		= $this->get('Sidebar');
		$this->filterbar	= $this->get('Filterbar');
		$this->config 		= rsseoHelper::getConfig();
		
		$this->addToolBar();
		parent::display($tpl);
	}
	
	protected function addToolBar() {
		JToolBarHelper::title(JText::_('COM_RSSEO_LIST_PAGES'),'rsseo');

		JToolBarHelper::addNew('page.add');
		JToolBarHelper::editList('page.edit');
		JToolBarHelper::deleteList('COM_RSSEO_PAGE_CONFIRM_DELETE','pages.delete');
		$bar = JToolBar::getInstance('toolbar');
		$bar->appendButton('Confirm',JText::_('COM_RSSEO_DELETE_ALL_PAGES_MESSAGE',true),'delete',JText::_('COM_RSSEO_DELETE_ALL_PAGES'),'pages.removeall',false);
		JToolBarHelper::publishList('pages.publish');
		JToolBarHelper::unpublishList('pages.unpublish');
		
		JToolBarHelper::custom('pages.addsitemap','new','new',JText::_('COM_RSSEO_PAGE_ADDTOSITEMAP'));
		JToolBarHelper::custom('pages.removesitemap','trash','trash',JText::_('COM_RSSEO_PAGE_REMOVEFROMSITEMAP'));
		JToolBarHelper::custom('restore','restore','restore',JText::_('COM_RSSEO_RESTORE_PAGES'));
		JToolBarHelper::custom('refresh','refresh','refresh',JText::_('COM_RSSEO_BULK_REFRESH'));
		
		if (JFactory::getUser()->authorise('core.admin', 'com_rsseo'))
			JToolBarHelper::preferences('com_rsseo');
		
		JToolBarHelper::custom('main', 'rsseo.png', 'rsseo.png', JText::_('COM_RSSEO_GLOBAL_COMPONENT'), false);
	}
}