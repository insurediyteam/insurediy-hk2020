<?php
/**
* @package RSSeo!
* @copyright (C) 2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/copyleft/gpl.html
*/
defined('_JEXEC') or die('Restricted access');

class rsseoViewRsseo extends JViewLegacy
{
	protected $jversion;
	protected $code;
	
	public function display($tpl=null) {
		$layout = $this->getLayout();
		
		$this->version = (string) new RSSeoVersion();
		
		if ($layout == 'update') {
			$this->sidebar = rsseoHelper::isJ3() ? JHtmlSidebar::render() : '';
			$jversion = new JVersion();
			$this->jversion = $jversion->getShortVersion();
		} else {
			$this->code			= rsseoHelper::getConfig('global_register_code');
			$this->statistics	= rsseoHelper::getStatistics();
			$this->pages		= rsseoHelper::getMostVisited();
		}
		
		$this->addToolBar($layout);
		parent::display($tpl);
	}
	
	protected function addToolBar($layout) {
		if ($layout == 'update') {
			JToolBarHelper::title(JText::_('COM_RSSEO_MENU_UPDATE'),'rsseo');
			JToolBarHelper::custom('main', 'rsseo.png', 'rsseo.png', JText::_('COM_RSSEO_GLOBAL_COMPONENT'), false);
		} else {
			JToolBarHelper::title(JText::_('COM_RSSEO_GLOBAL_COMPONENT'),'rsseo');
			
			JFactory::getDocument()->addStyleSheet(JURI::root().'administrator/components/com_rsseo/assets/css/dashboard.css?v='.RSSEO_REVISION);
		}
		
		if (JFactory::getUser()->authorise('core.admin', 'com_rsseo'))
			JToolBarHelper::preferences('com_rsseo');
	}
}