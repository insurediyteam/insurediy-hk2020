<?php
/**
* @package RSSeo!
* @copyright (C) 2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/copyleft/gpl.html
*/
defined('_JEXEC') or die('Restricted access');

class RSSeoGoogle
{
	/*
	* The URL
	*/
	protected $url = null;
	
	
	public function __construct($url) {
		$this->url = $url;
	}
	
	public function prank() {
		$hashes	= $this->hashes();
		$q		= "info:".urlencode($this->url);
		$rank	= false;
		
		if (!empty($hashes)) {
			foreach ($hashes as $hash) {
				if (empty($hash)) continue;
				$link = "http://toolbarqueries.google.ro/tbr?client=navclient-auto&ch=".$hash."&ie=UTF-8&oe=UTF-8&features=Rank&q=".$q;
				$contents = rsseoHelper::fopen($link,0);
				$contents = explode(':',$contents);
				
				if (isset($contents[2])) {
					$rank = $contents[2];
					break;
				}
			}
		}
		
		return trim($rank);
	}
	
	public function check() {
		$hashes	  = $this->hashes();
		$q		  = "info:".urlencode($this->url);
		$errors	  = 0;
		$contents = '';
		
		if (!empty($hashes)) {
			foreach ($hashes as $hash) {
				if (empty($hash)) continue;
				$link = "http://toolbarqueries.google.ro/tbr?client=navclient-auto&ch=".$hash."&ie=UTF-8&oe=UTF-8&features=Rank&q=".$q;
				$contents = rsseoHelper::fopen($link,0);
				if (strpos($contents,'<html') !== false) {
					$errors++;
				}
			}
		}
		
		return $errors == 5 ? $contents : true;
	}
	
	// Get hashes
	protected function hashes() {
		$hashes = array($this->jenkinsHash($this->url),
			$this->jenkinsHash2($this->url),
			$this->ieHash($this->url),
			$this->awesomeHash($this->url),
			$this->rsseoHash($this->url)
		);
		
		return $hashes;
	}
	
	// RSSeo! hash
	protected function rsseoHash($url) {
		$mGoogleCH = $this->googleCH($this->strord("info:".$url));
		return "6".$this->googleNewCh($mGoogleCH);
	}
	
	protected function googleCH($url) {
		$init = 0xE6359A60;	
		$length = count($url);
		$a = 0x9E3779B9;
		$b = 0x9E3779B9;
		$c = 0xE6359A60;
		$k = 0;
		$len = $length;
		$mixo = array(); 
		
		while( $len >= 12 ) {
			$a += ($url[$k+0] +($url[$k+1]<<8) +($url[$k+2]<<16) +($url[$k+3]<<24));
			$b += ($url[$k+4] +($url[$k+5]<<8) +($url[$k+6]<<16) +($url[$k+7]<<24));
			$c += ($url[$k+8] +($url[$k+9]<<8) +($url[$k+10]<<16)+($url[$k+11]<<24));
			$mixo = $this->mix($a,$b,$c);
			
			$a = $mixo[0]; $b = $mixo[1]; $c = $mixo[2];
			$k += 12;
			$len -= 12;
		}
		
		$c += $length;
		
		switch( $len ) {
			case 11: $c += $url[$k+10]<<24;
			case 10: $c+=$url[$k+9]<<16;
			case 9 : $c+=$url[$k+8]<<8;
			case 8 : $b+=($url[$k+7]<<24);
			case 7 : $b+=($url[$k+6]<<16);
			case 6 : $b+=($url[$k+5]<<8);
			case 5 : $b+=($url[$k+4]);
			case 4 : $a+=($url[$k+3]<<24);
			case 3 : $a+=($url[$k+2]<<16);
			case 2 : $a+=($url[$k+1]<<8);
			case 1 : $a+=($url[$k+0]);
		}
		
		$mixo = $this->mix( $a, $b, $c );
		if( $mixo[2] < 0 )
			return ( 0x100000000 + $mixo[2] );
		else
			return $mixo[2];
	}

	protected function mix( $a, $b, $c ) {
		$a -= $b; $a -= $c; $a ^= ( $this->zeroFill( $c, 13 ) );
		$b -= $c; $b -= $a; $b ^= ( $a << 8 );
		$c -= $a; $c -= $b; $c ^= ( $this->zeroFill( $b, 13 ) );
		$a -= $b; $a -= $c; $a ^= ( $this->zeroFill( $c, 12 ) );
		$b -= $c; $b -= $a; $b ^= ( $a << 16);
		$c -= $a; $c -= $b; $c ^= ( $this->zeroFill( $b, 5 ) );
		$a -= $b; $a -= $c; $a ^= ( $this->zeroFill( $c, 3 ) ); 
		$b -= $c; $b -= $a; $b ^= ( $a << 10);
		$c -= $a; $c -= $b; $c ^= ( $this->zeroFill( $b, 15 ) );
		
		return array($a,$b,$c);
	}
	
	protected function zeroFill( $a, $b ) {
		$z = hexdec(80000000);
		
		if ($z & $a) {
			$a = $a >> 1;
			$a &= ~$z;
			$a |= 0x40000000;
			$a = $a >> ( $b - 1 );
		} else
			$a = $a >> $b;
		
		return $a;
	}

	protected function strord($string) {	
		$result = array();
		for($i = 0; $i < strlen($string); $i++ ) {
			$result[$i] =  ord($string[$i]);
		}	
		return $result;
	}

	protected function googleNewCh( $ch ) {
		$ch = ( ( ( $ch / 7 ) << 2 ) | ( ( $this->myfmod( $ch,13 ) ) & 7 ) );
	  
		$prbuf = array();
		$prbuf[0] = $ch;
		for( $i = 1; $i < 20; $i++ ) {
		  $prbuf[$i] = $prbuf[$i-1] - 9;
		}
		$ch = $this->googleCH($this->c32to8bit( $prbuf ), 80 );
		
		return $ch;
	 }
	 
	protected function c32to8bit( $arr32 ) {
		$arr8 = array();	
		
		for( $i = 0; $i < count($arr32); $i++ ) {
			for( $bitOrder = $i * 4; $bitOrder <= $i * 4 + 3; $bitOrder++ )  {
				$arr8[$bitOrder] = $arr32[$i] & 255;
				$arr32[$i] = $this->zeroFill( $arr32[$i], 8 );
			}
		}
		
		return $arr8;
	}
	
	protected function myfmod( $x, $y ) {
		$i = floor( $x / $y );
		return ( $x - $i * $y );
	}
	
	protected function leftShift32($a, $b) {
		$c = $a << $b;
		if (PHP_INT_MAX != 0x80000000) {
			$c = -(~($c & 0x00000000FFFFFFFF) + 1);
		} 
		return (int)$c;
	}
	
	protected function unsignedRightShift($x, $y) {
		if (0xffffffff < $x || -0xffffffff > $x) {
			$x = $this->myfmod($x, 0xffffffff + 1);
		}
		
		if (0x7fffffff < $x) {
			$x -= 0xffffffff + 1.0;
		} elseif (-0x80000000 > $x) {
			$x += 0xffffffff + 1.0;
		}
		
		if (0 > $x) {
			$x &= 0x7fffffff;
			$x >>= $y;
			$x |= 1 << (31 - $y);
		} else {
			$x >>= $y;
		}
		return (int)$x;
	}
	
	protected function mask32($a) {
		if (PHP_INT_MAX != 0x0000000080000000) {
			$a = -(~($a  & 0x00000000FFFFFFFF) + 1);
		} 
		return (int)$a;
	}
	
	protected function strToNum($str, $c, $k) {
		$int32unit = 4294967296;
		
		for ($i=0; $i<strlen($str); $i++) {
			$c *= $k;
			if ($c >= $int32unit) {
				$c = ($c - $int32unit * (int)($c / $int32unit));
				$c = ($c < 0x0000000080000000) ? ($c + $int32unit) : $c;
			}
			
			$c += $this->charCodeAt($str, $i);
		} 
		
		return $c;
	}
	
	protected function charCodeAt($a, $b) {
		$a = mb_convert_encoding($a,"UCS-4BE","UTF-8");
		$c = unpack("N", mb_substr($a,$b,1,"UCS-4BE"));
		return $c[1];
	}
	
	protected function mixJenkins($a, $b, $c) {
		$a -= $b; $a -= $c; $a ^= $this->unsignedRightShift($c, 13);
		$b -= $c; $b -= $a; $b ^= $this->leftShift32($a, 8);
		$c -= $a; $c -= $b; $c ^= $this->unsignedRightShift(($b & 0x00000000FFFFFFFF), 13);
		$a -= $b; $a -= $c; $a ^= $this->unsignedRightShift(($c & 0x00000000FFFFFFFF), 12);
		$b -= $c; $b -= $a; $b  = ($b ^ ($this->leftShift32($a, 16))) & 0x00000000FFFFFFFF;
		$c -= $a; $c -= $b; $c  = ($c ^ ($this->unsignedRightShift($b,  5))) & 0x00000000FFFFFFFF;
		$a -= $b; $a -= $c; $a  = ($a ^ ($this->unsignedRightShift($c,  3))) & 0x00000000FFFFFFFF;
		$b -= $c; $b -= $a; $b  = ($b ^ ($this->leftShift32($a, 10))) & 0x00000000FFFFFFFF;
		$c -= $a; $c -= $b; $c  = ($c ^ ($this->unsignedRightShift($b, 15))) & 0x00000000FFFFFFFF;
		
		return array($a, $b, $c);
	}
	
	protected function jenkinsHash($url, $encode = true) {
		$url = $this->strord('info:'.$url);
		
		$length = sizeof($url);
		$a = $b = 0x000000009E3779B9;
		$c = 0x00000000E6359A60;
		$k = 0; $len = $length;
		
		while ($len >= 12) {
			$a += $url[$k+0];
			$a += $this->leftShift32($url[$k+1],  8);
			$a += $this->leftShift32($url[$k+2], 16);
			$a += $this->leftShift32($url[$k+3], 24);
			$b += $url[$k+4];
			$b += $this->leftShift32($url[$k+5],  8);
			$b += $this->leftShift32($url[$k+6], 16);
			$b += $this->leftShift32($url[$k+7], 24);
			$c += $url[$k+8];
			$c += $this->leftShift32($url[$k+9],  8);
			$c += $this->leftShift32($url[$k+10],16);
			$c += $this->leftShift32($url[$k+11],24);
			$mix = $this->mixJenkins($a, $b, $c);
			$a = $mix[0]; $b = $mix[1]; $c = $mix[2];
			$len -= 12; $k += 12;
		}
		
		$c += $length;
		
		switch($len) {
			case 11: $c += $this->leftShift32($url[$k+10],24);
			case 10: $c += $this->leftShift32($url[$k+9], 16);
			case 9 : $c += $this->leftShift32($url[$k+8],  8);
			case 8 : $b += $this->leftShift32($url[$k+7], 24);
			case 7 : $b += $this->leftShift32($url[$k+6], 16);
			case 6 : $b += $this->leftShift32($url[$k+5],  8);
			case 5 : $b += $url[$k+4];
			case 4 : $a += $this->leftShift32($url[$k+3], 24);
			case 3 : $a += $this->leftShift32($url[$k+2], 16);
			case 2 : $a += $this->leftShift32($url[$k+1],  8);
			case 1 : $a += $url[$k+0];
		}
		
		$mix = $this->mixJenkins($a, $b, $c);
		$ch  = $this->mask32($mix[2]);
		return $encode !== true ? $ch : sprintf("6%u", $ch);
	}
	
	protected function jenkinsHash2($url) {
		$ch = sprintf("%u", $this->jenkinsHash($url, FALSE));
		$ch = (($this->leftShift32(($ch/7), 2)) | (($this->myfmod($ch, 13)) & 7));
		$buf = array($ch);
		
		for($i=1; $i<20; $i++) { 
			$buf[$i] = $buf[$i-1]-9; 
		}
		
		return sprintf("6%u", $this->jenkinsHash($this->c32to8bit($buf), FALSE));
	}
	
	protected function ieHash($url) {
		$hashString = sprintf('%u', $this->mixIE($url));
		$hashLength = strlen($hashString);
		$checkByte = 0;
		
		for ($i=($hashLength-1); $i>=0; $i--) {
			$num = $hashString{$i};
			$checkByte += (1===($i % 2)) ? (int)((($num*2)/10)+(($num*2)%10)) : $num;
		}   
		
		$checkByte %= 10;
		
		if ($checkByte !== 0) {
			$checkByte = 10-$checkByte;
			if (($hashLength % 2) === 1) {
				if (($checkByte % 2) === 1) {
					$checkByte += 9; 
				}
				$checkByte >>= 1; 
			}
		}
		return '7'.$checkByte.$hashString;
	}
	
	protected function mixIE($url) {
		$c1 =  $this->strToNum($url, 0x1505, 0x21);
		$c2 =  $this->strToNum($url, 0,   0x1003f);
		$c1 =  $this->unsignedRightShift($c1, 2);
		$c1 = ($this->unsignedRightShift($c1, 4) & 0x3ffffc0) | ($c1 &   0x3f);
		$c1 = ($this->unsignedRightShift($c1, 4) &  0x3ffc00) | ($c1 &  0x3ff);
		$c1 = ($this->unsignedRightShift($c1, 4) &   0x3c000) | ($c1 & 0x3fff);
		$t1 = ($this->leftShift32( ( $this->leftShift32( ($c1 &      0x3c0), 4) | ($c1 &   0x3c)),   2)) | ($c2 &     0xf0f);
		$t2 = ($this->leftShift32( ( $this->leftShift32( ($c1 & 0xffffc000), 4) | ($c1 & 0x3c00)), 0xa)) | ($c2 & 0xf0f0000);
		return $this->mask32(($t1 | $t2));
	}
	
	protected function awesomeHash($url) {
		$string = "Mining PageRank is AGAINST GOOGLE'S TERMS OF SERVICE. Yes, I'm talking to you, scammer.";
		$b		= 16909125;
		
		for ($c=0; $c<strlen($url); $c++) {
			$b ^= ($this->charCodeAt($string, ($c%87))) ^ ($this->charCodeAt($url, $c));
			$b = $this->unsignedRightShift($b, 23) | $b << 9;
		}
		return '8'.$this->hexEncodeU32($b);
	}
	
	protected function hexEncodeU32($a) {
		$b  = $this->toHex8($this->unsignedRightShift($a, 24));
		$b .= $this->toHex8($this->unsignedRightShift($a, 16) & 255);
		$b .= $this->toHex8($this->unsignedRightShift($a,  8) & 255);
		
		return $b.$this->toHex8($a & 255);
	}

	protected function toHex8($a) {
		return ($a < 16 ? "0" : "") . dechex($a);
	}
}