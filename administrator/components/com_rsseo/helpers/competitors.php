<?php
/**
* @package RSSeo!
* @copyright (C) 2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/copyleft/gpl.html
*/
defined('_JEXEC') or die('Restricted access');

class competitorsHelper {
	
	protected $id;
	protected $url;
	protected $statistics = false;
	
	protected $values = array(
		'age' => 0,
		'pagerank' => 0,
		'googlep' => 0,
		'googleb' => 0,
		'googler' => 0,
		'bingp' => 0,
		'bingb' => 0,
		'alexa' => 0,
		'technorati' => 0,
		'dmoz' => 0,
	);
	
	public function __construct($id, $url, $statistics = false) {
		// Set Competitor ID
		$this->id = $id;
		
		// Set Competitor URL
		$this->url = $url;
		
		// Statistics
		$this->statistics = $statistics;
	}
	
	public static function getInstance($id, $url, $statistics = false) {
		$modelClass = 'competitorsHelper';
		return new $modelClass($id, $url, $statistics);
	}
	
	public function check() {
		// Get configuration
		$config = rsseoHelper::getConfig();
		
		if ($config->enable_age || $this->statistics) {
			$this->domainage();
		}
		
		if ($config->enable_pr || $this->statistics) {
			$this->pagerank();
		}
		
		if ($config->enable_googlep || $this->statistics) {
			$this->googlepages();
		}
		
		if ($config->enable_googleb || $this->statistics) {
			$this->googlebacklinks();
		}
		
		if ($config->enable_googler || $this->statistics) {
			$this->googleRelated();
		}
		
		if ($config->enable_bingp || $this->statistics) {
			$this->bingpages();
		}
		
		if ($config->enable_bingb || $this->statistics) {
			$this->bingbacklinks();
		}
		
		if ($config->enable_alexa || $this->statistics) {
			$this->alexa();
		}
		
		if ($config->enable_tehnorati) {
			$this->technorati();
		}
		
		if ($config->enable_dmoz) {
			$this->dmoz();
		}
		
		if ($this->statistics) {
			unset($this->values['technorati'],$this->values['dmoz']);
			$this->googleplus();
			$this->twitter();
			$this->facebook();
			$this->linkedin();
		}
		
		$this->update();
		
		return $this->statistics ? $this->values : json_encode($this->values);
	}
	
	/**
	 *	Calculate Google page rank
	 */
	protected function pagerank() {
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$check	= false;
		
		if ($this->statistics) {
			$check = true;
		} else {
			$query->clear()
				->select($db->qn('pagerank'))->select($db->qn('date'))
				->from($db->qn('#__rsseo_competitors'))
				->where($db->qn('parent_id').' = '.(int) $this->id)
				->order($db->qn('date').' DESC');
			$db->setQuery($query,0,1);
			$cache = $db->loadObject();
			
			if (empty($cache)) {
				$check = true;
			} else {
				if (JFactory::getDate($cache->date)->toUnix() + 86400 < JFactory::getDate()->toUnix())
					$check = true;
				else 
					$this->values['pagerank'] = (int) $cache->pagerank;
			}
		}
		
		if ($check) {
			require_once JPATH_ADMINISTRATOR. '/components/com_rsseo/helpers/google.php';
			$google = new RSSeoGoogle($this->url);
			$this->values['pagerank'] = (int) $google->prank();
		}
	}
	
	/**
	 *	Calculate Google pages
	 */
	protected function googlepages() {
		$url		= str_replace(array('http://','https://','www.'),'',$this->url);
		$g_pages	= false;
		$search 	= urlencode('site:'.$url);
		$url		= 'https://www.'.rsseoHelper::getConfig('google_domain').'/search?q='.$search;
		
		$response = rsseoHelper::fopen($url);
		if ($response && $response != 'RSSEOINVALID') {
			$pattern = '#<div id=["|\']resultStats["|\']>(.*?)<nobr>#is';
			if (preg_match($pattern, $response, $match)) {
				if (isset($match[1])) {
					$result  = trim($match[1]);
					$result  = preg_replace('#[^0-9]#', '', $result);
					$g_pages = $result;
				}
			}
		}
		
		$this->values['googlep'] = $g_pages === false ? -1 : (int) $g_pages;
	}
	
	/**
	 *	Calculate Google backlinks
	 */
	protected function googlebacklinks() {
		$url	= str_replace(array('http://','https://','www.'), '', $this->url);
		$search = urlencode('"'.$url.'" -site:'.$url);
		$url	= 'https://www.'.rsseoHelper::getConfig('google_domain').'/search?q='.$search.'&as_lq=&num=100&start=0&filter=0';
		$g_back	= false;
		
		$response = rsseoHelper::fopen($url);
		if ($response && $response != 'RSSEOINVALID') {
			$pattern = '#<div id=["|\']resultStats["|\']>(.*?)<nobr>#is';
			if (preg_match($pattern, $response, $match)) {
				if (isset($match[1])) {
					$result  = trim($match[1]);
					$result  = preg_replace('#[^0-9]#', '', $result);
					$g_back	 = $result;
				}
			}
		}
		
		$this->values['googleb'] = $g_back === false ? -1 : (int) $g_back;
	}
	
	/**
	 *	Calculate Google similar pages
	 */
	protected function googleRelated() {
		$url		= str_replace(array('http://','https://','www.'),'',$this->url);
		$r_pages	= false;
		$search 	= urlencode('related:'.$url);
		$url		= 'https://www.'.rsseoHelper::getConfig('google_domain').'/search?q='.$search;
		
		$response = rsseoHelper::fopen($url);
		if ($response && $response != 'RSSEOINVALID') {
			$pattern = '#<div id=["|\']resultStats["|\']>(.*?)<nobr>#is';
			if (preg_match($pattern, $response, $match)) {
				if (isset($match[1])) {
					$result  = trim($match[1]);
					$result  = preg_replace('#[^0-9]#', '', $result);
					$r_pages = $result;
				}
			}
		}
		
		$this->values['googler'] = $r_pages === false ? -1 : (int) $r_pages;
	}
	
	/**
	 *	Calculate Bing pages
	 */
	protected function bingpages() {
		$url = str_replace(array('http://','https://','www.'), '', $this->url);
		$url = 'http://www.bing.com/search?q='.urlencode($url);
		$found = false;
		
		$response = rsseoHelper::fopen($url);
		if ($response && $response != 'RSSEOINVALID') {
			$pattern1 = '#<span class="sb_count" id="count">(.*?)<\/span>#i';
			$pattern2 = '#<span class="sb_count" id="count">(.*?) of (.*?) results<\/span>#i';
			$pattern3 = '#<span class="sb_count">(.*?)<\/span>#i';
			
			if (preg_match($pattern1, $response, $matches1)) {
				if (!empty($matches1[1])) {
					$number = explode(' ',$matches1[1]);
					
					$this->values['bingp'] = (int) str_replace(array(',','.','&#160;'),'',@$number[0]);
					$found = true;
				}
			}
			
			if (!$found) {
				if (preg_match($pattern2, $response, $matches2)) {
					if (!empty($matches2[2])) {
						$this->values['bingp'] = (int) str_replace(array(',','.','&#160;'), '', $matches2[2]);
						$found = true;
					}
				}
			}
			
			if (!$found) {
				if (preg_match($pattern3, $response, $matches3)) {
					if (!empty($matches3[1])) {
						$number = explode(' ',$matches3[1]);
						
						$this->values['bingp'] = str_replace(array(',','.',' '), '', @$number[0]);
						$found = true;
					}
				}
			}
		}
		
		if (!$found)
			$this->values['bingp'] = -1;
	}
	
	/**
	 *	Calculate Bing backlinks
	 */
	protected function bingbacklinks() {
		$url = str_replace(array('http://','https://','www.'),'',$this->url);
		$url = 'http://www.bing.com/search?filt=all&q='.urlencode('link: '.$url);
		$found = false;
		
		$response = rsseoHelper::fopen($url);
		if ($response && $response != 'RSSEOINVALID') {
			$pattern1 = '#<span class="sb_count" id="count">(.*?)<\/span>#i';
			$pattern2 = '#<span class="sb_count" id="count">(.*?) of (.*?) results<\/span>#is';
			$pattern3 = '#<span class="sb_count">(.*?)<\/span>#i';
			
			if (preg_match($pattern1, $response, $matches1)) {
				if (!empty($matches1[1])) {
					$number = explode(' ',$matches1[1]);
					$this->values['bingb'] = (int) str_replace(array(',','.','&#160;'), '', @$number[0]);
					$found = true;
				}
			}
			
			if (!$found) {
				if (preg_match($pattern2, $response, $matches2)) {
					if (!empty($matches2[2])) {
						$this->values['bingb'] = (int) str_replace(array(',','.','&#160;'), '', $matches2[2]);
						$found = true;
					}
				}
			}
			
			if (!$found) {
				if (preg_match($pattern3, $response, $matches3)) {
					if (!empty($matches3[1])) {
						$number = explode(' ',$matches3[1]);
						
						$this->values['bingb'] = str_replace(array(',','.',' '), '', @$number[0]);
						$found = true;
					}
				}
			}
		}
		
		if (!$found)
			$this->values['bingb'] = -1;
	}
	
	/**
	 *	Calculate Alexa rank
	 */
	protected function alexa() {
		$url = trim($this->url);
		$url = str_replace(array('http://','https://','www.'), '', $url);
		$url = 'http://data.alexa.com/data?cli=10&dat=snbamz&url='.urlencode($url);
		
		$response = rsseoHelper::fopen($url);
		if ($response && $response != 'RSSEOINVALID') {
			$pattern = '#<popularity url="(.*?)" text="([0-9]+)"#is';
			if (preg_match($pattern, $response, $match)) {
				if (!empty($match[2]))
					$this->values['alexa'] = (int) $match[2];
			} else $this->values['alexa'] = 0;
		} else $this->values['alexa'] = -1;
	}
	
	/**
	 *	Calculate Technorati rank
	 */
	protected function technorati() {
		if ($this->statistics) return;
		
		$url = trim($this->url);
		$url = str_replace(array('http://','https://'),'',$url);
		$url = 'http://technorati.com/blogs/'. urlencode($url);
		
		$response = rsseoHelper::fopen($url);
		if ($response && $response != 'RSSEOINVALID') {
			if (preg_match('/Authority: (.*)<\/strong>/isU',$response,$match)) {
				if (!empty($match[1])) {
					$this->values['technorati'] = $match[1];
				} else $this->values['technorati'] = 0;
			} $this->values['technorati'] = 0;
		} else $this->values['technorati'] = -1;
	}
	
	/**
	 *	Calculate Dmoz
	 */
	protected function dmoz() {
		if ($this->statistics) return;
		
		$url = str_replace(array('http://','https://','www.'), '', $this->url);
		$url = 'http://www.dmoz.org/search?q='.urlencode($url).'&cat=all&all=no';
		
		$response = rsseoHelper::fopen($url);
		if ($response && $response != 'RSSEOINVALID') {
			$pattern = '#<h3 class=\"open-dir-sites\">(.*?)<\/h3>#is';
			if (preg_match($pattern,$response,$matches)) {
				if (!empty($matches[1])) {
					$pattern = '#<small>\((.*?) of (.*?)\)</small>#is';
					if (preg_match($pattern,$matches[1],$match)) {
						if (!empty($match[2])) {
							$this->values['dmoz'] = 1;
						} else $this->values['dmoz'] = 0;
					} else $this->values['dmoz'] = 0;
				} else $this->values['dmoz'] = 0;
			} else $this->values['dmoz'] = 0;
		} else $this->values['dmoz'] = 0;
	}
	
	/**
	 *	Get Google+ shares
	 */
	protected function googleplus() {
		$url = $this->url;
		$url = substr($url,0,4) == 'http' ? $url : 'http://'.$url;
		$url = 'https://plusone.google.com/u/0/_/+1/fastbutton?count=true&url='.urlencode($url);
		
		$response = rsseoHelper::fopen($url);
		if ($response && $response != 'RSSEOINVALID') {
			preg_match_all('/window\.__SSR\s\=\s\{c:\s(\d+?)\./', $response, $match, PREG_SET_ORDER);
			if (count($match) == 1 && count($match[0]) == 2) {
				$this->values['gplus'] = (int) $match[0][1];
			} else {
				$this->values['gplus'] = -1;
			}
		} else {
			$this->values['gplus'] = -1;
		}
	}
	
	/**
	 *	Get Twitter shares
	 */
	protected function twitter() {
		$url = $this->url;
		$url = substr($url,0,4) == 'http' ? $url : 'http://'.$url;
		$url = 'http://urls.api.twitter.com/1/urls/count.json?url='.urlencode($url);
		
		$response = rsseoHelper::fopen($url);
		if ($response && $response != 'RSSEOINVALID') {
			if ($response = json_decode($response)) {
				if (isset($response) && isset($response->count)) {
					$this->values['twitter'] = $response->count;
				} else $this->values['twitter'] = -1;
			} else $this->values['twitter'] = -1;
		} else $this->values['twitter'] = -1;
	}
	
	/**
	 *	Get Facebook shares & likes
	 */
	protected function facebook() {
		$url = $this->url;
		$url = substr($url,0,4) == 'http' ? $url : 'http://'.$url;
		$fql = sprintf('SELECT share_count, like_count FROM link_stat WHERE url="%s"', $url);
		$url = 'https://api.facebook.com/method/fql.query?query='.rawurlencode($fql).'&format=json';
		
		$response = rsseoHelper::fopen($url);
		if ($response && $response != 'RSSEOINVALID') {
			if ($response = json_decode($response)) {
				if (is_object($response)) {
					$this->values['fb_share_count'] = -1;
					$this->values['fb_like_count'] = -1;
				} elseif (is_array($response)) {
					$response = isset($response[0]) ? $response[0] : false;
					if (isset($response->share_count)) {
						$this->values['fb_share_count'] = $response->share_count;
					} else $this->values['fb_share_count'] = -1;
					
					if (isset($response->like_count)) {
						$this->values['fb_like_count'] = $response->like_count;
					} else $this->values['fb_like_count'] = -1;
				} else {
					$this->values['fb_share_count'] = -1;
					$this->values['fb_like_count'] = -1;
				}
			} else {
				$this->values['fb_share_count'] = -1;
				$this->values['fb_like_count'] = -1;
			}
		} else {
			$this->values['fb_share_count'] = -1;
			$this->values['fb_like_count'] = -1;
		}
	}
	
	/**
	 *	Get LinkedIn shares
	 */
	protected function linkedin() {
		$url = $this->url;
		$url = substr($url,0,4) == 'http' ? $url : 'http://'.$url;
		$url = 'http://www.linkedin.com/countserv/count/share?url='.urlencode($url).'&format=json';
		
		$response = rsseoHelper::fopen($url);
		if ($response && $response != 'RSSEOINVALID') {
			if ($response = json_decode($response)) {
				if (isset($response->count)) {
					$this->values['linkedin'] = $response->count;
				} else $this->values['linkedin'] = -1;
			} else $this->values['linkedin'] = -1;
		} else $this->values['linkedin'] = -1;
	}
	
	/**
	 *	Get domain age
	 */
	protected function domainage() {
		require_once JPATH_ADMINISTRATOR.'/components/com_rsseo/helpers/whois.php';
		$whois	= new rsseoWhois($this->url);
		$age	= $whois->age();
		
		$this->values['age'] = $age ? $age : -1;
	}
	
	/**
	 *	Update values
	 */
	protected function update() {
		$db		= JFactory::getDbo();
		$query	= $db->getQuery(true);
		$now	= JFactory::getDate()->toSql();
		
		if ($this->statistics) {
			$query->clear()
				->update($db->qn('#__rsseo_statistics'))
				->set($db->qn('date').' = '.$db->q($now));
			
			foreach($this->values as $name => $value) {
				$query->set($db->qn($name).' = '.$db->q($value));
			}
			
			$db->setQuery($query);
			$db->execute();
		} else {
			// Add new record for history
			$query->clear()
				->insert($db->qn('#__rsseo_competitors'))
				->set($db->qn('parent_id').' = '. (int) $this->id)
				->set($db->qn('date').' = '.$db->q($now));
			
			foreach($this->values as $name => $value) {
				$query->set($db->qn($name).' = '.$db->q($value));
			}
			
			$db->setQuery($query);
			$db->execute();
			
			// Update parent
			$query->clear()
				->update($db->qn('#__rsseo_competitors'))
				->set($db->qn('date').' = '.$db->q($now))
				->where($db->qn('id').' = '.(int) $this->id);
			
			foreach($this->values as $name => $value) {
				$query->set($db->qn($name).' = '.$db->q($value));
			}
			
			$db->setQuery($query);
			$db->execute();
		}
	}
}