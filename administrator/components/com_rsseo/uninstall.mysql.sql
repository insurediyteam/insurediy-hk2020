DROP TABLE IF EXISTS 
`#__rsseo_broken_links`,
`#__rsseo_competitors`,
`#__rsseo_keywords`,
`#__rsseo_pages`,
`#__rsseo_errors`,
`#__rsseo_error_links`,
`#__rsseo_redirects`,
`#__rsseo_redirects_referer`,
`#__rsseo_statistics`;