<?php
/**
* @package RSSeo!
* @copyright (C) 2014 www.rsjoomla.com
* @license GPL, http://www.gnu.org/licenses/gpl-2.0.html
*/
defined( '_JEXEC' ) or die( 'Restricted access' );

// ACL Check
if (!JFactory::getUser()->authorise('core.manage', 'com_rsseo'))
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));

require_once(JPATH_COMPONENT.'/helpers/rsseo.php');
require_once(JPATH_COMPONENT.'/helpers/adapter/adapter.php');
require_once(JPATH_COMPONENT.'/controller.php');
JHtml::addIncludePath(JPATH_ADMINISTRATOR . '/components/com_rsseo/helpers');

JHTML::_('behavior.framework');

// Load scripts
rsseoHelper::setScripts('administrator');
// Check for keywords config
rsseoHelper::keywords();

$controller	= JControllerLegacy::getInstance('RSSeo');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();