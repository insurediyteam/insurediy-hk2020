<?php

defined('_JEXEC') or die;

class InsureDIYTravelControllerCSVImport extends JControllerForm {

	protected function allowAdd($data = array()) {
		$allow = null;
		if ($allow === null) {
			return parent::allowAdd($data);
		} else {
			return $allow;
		}
	}

	protected function allowEdit($data = array(), $key = 'id') {
		$recordId = (int) isset($data[$key]) ? $data[$key] : 0;
		$categoryId = 0;
		if ($recordId) {
			$categoryId = (int) $this->getModel()->getItem($recordId)->catid;
		}

		if ($categoryId) {
			return JFactory::getUser()->authorise('core.edit', $this->option . '.category.' . $categoryId);
		} else {
			return parent::allowEdit($data, $key);
		}
	}

	public function batch($model = null) {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$model = $this->getModel('CSVImport', '', array());
		$this->setRedirect(JRoute::_('index.php?option=com_insurediytravel&view=csvimport' . $this->getRedirectToListAppend(), false));
		return parent::batch($model);
	}

	protected function postSaveHook(JModelLegacy $model, $validData = array()) {
		if ($task == 'save') {
			$this->setRedirect(JRoute::_('index.php?option=com_insurediytravel&view=csvimport', false));
		}
	}

	public function plans() {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		// Set the model
		$model = $this->getModel('CSVImport');

		if ($model->import_plans()) {
			$msg = ' Importing Plans Success. ';
		} else {
			$msg = ' Importing Plans Failed! Please try again...';
		}

		$this->setRedirect(JRoute::_('index.php?option=com_insurediytravel&view=csvimport', false), $msg);
	}

	public function generateData() {
		ini_set('memory_limit', '-1'); //**************IMPORTANT

		$csvHeaders = array("plan_name", "insurer_code", "year_code", "is_smoking", "gender", "age", "sum_insured", "price", "renewable", "terminal_illness_benefit", "benefits", "flag_banner", "download_file", "nationality", "residency", "published");
		$insurers = array("AIA", "AXA", "SUN", "ZUR");
		$years = array("01YRT", "05YRT", "10YRT", "15YRT", "20YRT", "60T75");
		$genders = array("M", "F");
		$nationalities = array("CN", "HK", "MO", "XX");
		$residencies = array("CN", "HK", "MO", "XX");
		$ages = range(18, 55, 1);
		$sum_insured_s = array("1000000", "2000000", "3000000", "4000000", "5000000", "6000000", "8000000", "10000000");
		$count = 1;
		$csvMaster = array();
		$csv_lines = array();
		foreach ($insurers as $i_key => $insurer) {
			foreach ($genders as $gender) {
				foreach ($years as $year) {
					foreach ($nationalities as $nationality) {
						foreach ($residencies as $residency) {
							foreach ($ages as $agekey => $age) {
								set_time_limit(0);
								foreach ($sum_insured_s as $si_key => $sum_insured) {
									$csv = array();
									foreach ($csvHeaders as $k => $v) {
										switch ($v) {
											case "plan_name":
												$csv[] = $insurer . " Plan " . $count;
												break;
											case "insurer_code":
												$csv[] = $insurer;
												break;
											case "year_code":
												$csv[] = $year;
												break;
											case "is_smoking":
												$csv[] = "NS";
												break;
											case "gender":
												$csv[] = $gender;
												break;
											case "age":
												$csv[] = $age;
												break;
											case "sum_insured":
												$csv[] = $sum_insured;
												break;
											case "price":
												$csv[] = $agekey + 1 + $si_key + $i_key;
												break;
											case "renewable":
												$csv[] = 1;
												break;
											case "terminal_illness_benefit":
												$csv[] = 1;
												break;
											case "nationality":
												$csv[] = $nationality;
												break;
											case "residency":
												$csv[] = $residency;
												break;
											case "published":
												$csv[] = 1;
												break;
											default:
												$csv[] = "";
												break;
										}
									}
									$count ++;
									$csv_lines[] = implode(',', MyHelper::escapeCSV($csv, TRUE));
								}
							}
						}
					}
				}
			}
			$count = 1;
		}
		$csvMaster = implode("\n", $csv_lines);
		$csvHeader = implode(',', MyHelper::escapeCSV($csvHeaders, TRUE));

		$actualCSVarr = array(
			$csvHeader,
			$csvMaster
		);

		$actualCSVData = implode("\n", $actualCSVarr);
		MyHelper::fileDownload("sample.csv", $actualCSVData);
	}

	public function generateData2() {
		ini_set('memory_limit', '-1'); //**************IMPORTANT

		$csvHeaders = array("plan_name", "insurer_code", "trip_type", "group_type", "no_of_days", "no_of_travellers",
			"no_of_adults", "no_of_children", "no_of_s_children", "flag_banner", "premium",
			"emergency_me", "medical_expenses", "personal_accident", "rental_vehicle", "unattended_child", "personal_property",
			"max_limit_ei", "cancellation", "interruption", "amateur_sport", "tnc", "published");

		$insurers = array("AIA", "AXA", "SUN", "ZUR", "ACE");
		$triptypes = array("ST", "AT");
		$grouptypes = array("JM", "WF", "WG");
		$adults = range(0, 5, 1);
		$kids = range(0, 5, 1);
		$skids = range(0, 5, 1);
		$travellers = range(0, 5, 1);
		$days = range(1, 10, 1);
		$count = 1;
		$csv_lines = array();
		foreach ($insurers as $insurer) {
			foreach ($triptypes as $triptype) {
				foreach ($grouptypes as $grouptype) {
					foreach ($days as $day) {
						if ($grouptype == "WF") {
							foreach ($adults as $adult) {
								foreach ($kids as $kid) {
									foreach ($skids as $skid) {
										set_time_limit(0);
										$csv = array();
										$traveller = $adult + $kid + $skid;
										foreach ($csvHeaders as $k => $v) {
											switch ($v) {
												case "plan_name":
													$csv[] = $insurer . " Plan " . $count;
													break;
												case "insurer_code":
													$csv[] = $insurer;
													break;
												case "trip_type":
													$csv[] = $triptype;
													break;
												case "group_type":
													$csv[] = $grouptype;
													break;
												case "no_of_days":
													$csv[] = $day;
													break;
												case "no_of_travellers":
													$csv[] = $traveller;
													break;
												case "premium":
													if ($triptype == "ST") {
														$csv[] = 100;
													} else {
														$csv[] = 110;
													}
													break;
												case "no_of_adults":
													$csv[] = $adult;
													break;
												case "no_of_children":
													$csv[] = $kid;
													break;
												case "no_of_s_children":
													$csv[] = $skid;
													break;
												case "emergency_me":
												case "medical_expenses":
												case "personal_accident":
												case "rental_vehicle":
												case "unattended_child":
												case "personal_property":
												case "max_limit_ei":
												case "cancellation":
												case "interruption":
													$csv[] = rand(0, 100) * 100;
													break;
												case "amateur_sport":
													$csv[] = rand(0, 1);
													break;
												case "published":
													$csv[] = 1;
													break;
												default:
													$csv[] = "";
													break;
											}
										}
										$count ++;
										$csv_lines[] = implode(',', MyHelper::escapeCSV($csv, TRUE));
									}
								}
							}
						} else {
							foreach ($travellers as $traveller) {
								set_time_limit(0);
								$csv = array();
								foreach ($csvHeaders as $k => $v) {
									switch ($v) {
										case "plan_name":
											$csv[] = $insurer . " Plan " . $count;
											break;
										case "insurer_code":
											$csv[] = $insurer;
											break;
										case "trip_type":
											$csv[] = $triptype;
											break;
										case "no_of_days":
											$csv[] = $day;
											break;
										case "group_type":
											$csv[] = $grouptype;
											break;
										case "no_of_travellers":
											$csv[] = $traveller;
											break;
										case "premium":
											if ($triptype == "ST") {
												$csv[] = 100;
											} else {
												$csv[] = 110;
											}
											break;
										case "no_of_adults":
										case "no_of_children":
										case "no_of_s_children":
											$csv[] = 0;
											break;
										case "emergency_me":
										case "medical_expenses":
										case "personal_accident":
										case "rental_vehicle":
										case "unattended_child":
										case "personal_property":
										case "max_limit_ei":
										case "cancellation":
										case "interruption":
											$csv[] = rand(0, 100) * 100;
											break;
										case "amateur_sport":
											$csv[] = rand(0, 1);
											break;
										case "published":
											$csv[] = 1;
											break;
										default:
											$csv[] = "";
											break;
									}
								}
								$count ++;
								$csv_lines[] = implode(',', MyHelper::escapeCSV($csv, TRUE));
							}
						}
					}
				}
			}
		}
		$csvMaster = implode("\n", $csv_lines);
		$csvHeader = implode(',', MyHelper::escapeCSV($csvHeaders, TRUE));

		$actualCSVarr = array(
			$csvHeader,
			$csvMaster
		);

		$actualCSVData = implode("\n", $actualCSVarr);
		MyHelper::fileDownload("sample.csv", $actualCSVData);
	}

}
