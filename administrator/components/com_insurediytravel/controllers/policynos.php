<?php
defined('_JEXEC') or die;
class InsureDIYTravelControllerPolicyNos extends JControllerAdmin {

	protected $text_prefix = "COM_INSUREDIYLIFE";

	/**
	 * Proxy for getModel.
	 * @since   1.6
	 */
	public function getModel($name = 'Policynos', $prefix = 'InsureDIYTravelModel', $config = array('ignore_request' => true)) {
		$model = parent::getModel($name, $prefix, $config);
		return $model;
	}

	/**
	 * Method to save the submitted ordering values for records via AJAX.
	 *
	 * @return  void
	 *
	 * @since   3.0
	 */
	public function saveOrderAjax() {
		// Get the input
		$input = JFactory::getApplication()->input;
		$pks = $input->post->get('cid', array(), 'array');
		$order = $input->post->get('order', array(), 'array');

		// Sanitize the input
		JArrayHelper::toInteger($pks);
		JArrayHelper::toInteger($order);

		// Get the model
		$model = $this->getModel();

		// Save the ordering
		$return = $model->saveorder($pks, $order);

		if ($return) {
			echo "1";
		}

		// Close the application
		JFactory::getApplication()->close();
	}

	public function deletePolicyNos() {
		
	}

	/**
	 * Method to provide child classes the opportunity to process after the delete task.
	 *
	 * @param   JModelLegacy   $model   The model for the component
	 * @param   mixed          $ids     array of ids deleted.
	 *
	 * @return  void
	 *
	 * @since   3.1
	 */
	protected function postDeleteHook(JModelLegacy $model, $ids = null) {

	}

	public function cancel() {
		MyUri::redirect("index.php?option=com_insurediytravel&view=policynos");
	}

}
