<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_CedeleDepots
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * View to edit a weblink.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_CedeleDepots
 * @since       1.5
 */
class InsureDIYTravelViewPNImport extends JViewLegacy {

	protected $state;
	protected $item;
	protected $form;

	/**
	 * Display the view
	 */
	public function display($tpl = null) {
		$this->state = $this->get('State');
		$this->form = $this->get('Form');

		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar() {
		JToolBar::getInstance('toolbar');

		JToolbarHelper::title(JText::_('COM_INSUREDIYTRAVEL_HEADER_IMPORT_POLICY_NUMBERS'), 'insurediytravel.png');

		// If not checked out, can save the item.
		if (empty($this->item->id)) {
			JToolbarHelper::cancel('policynos.cancel');
		} else {
			JToolbarHelper::cancel('policynos.cancel', 'JTOOLBAR_CLOSE');
		}
	}

}
