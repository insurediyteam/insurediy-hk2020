<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_cedeledepots
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('MyBehavior.jsInsurediy');

$form = $this->form;
$item = $this->item;
$activetab = JFactory::getApplication()->input->get("activetab", "info_details");
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'quotation.cancel' || document.formvalidator.isValid(document.id('quotation-form')))
		{
			Joomla.submitform(task, document.getElementById('quotation-form'));
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_insurediytravel'); ?>" method="post" name="adminForm" id="quotation-form" class="form-validate">
	<div class="row-fluid">
		<div class="span11 form-horizontal">
			<fieldset>
				<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => $activetab)); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'info_details', JText::_('COM_INSUREDIYTRAVEL_QUOTATION_BASIC_INFORMATION')); ?>
				<?php foreach ($form->getFieldset("details") as $field): ?>
					<div class="control-group">
						<div class="control-label"><?php echo $field->label; ?></div>
						<div class="controls"><?php echo $field->input; ?></div>
					</div>
				<?php endforeach; ?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'contact_details', JText::_('COM_INSUREDIYTRAVEL_QUOTATION_CONTACT')); ?>
				<?php foreach ($form->getFieldset("contact") as $field): ?>
					<div class="control-group">
						<div class="control-label"><?php echo $field->label; ?></div>
						<div class="controls"><?php echo $field->input; ?></div>
					</div>
				<?php endforeach; ?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'plan_details', JText::_('COM_INSUREDIYTRAVEL_QUOTATION_PLAN_SELECTED')); ?>
				<?php foreach ($form->getFieldset("plan_details") as $field): ?>
					<div class="control-group">
						<div class="control-label"><?php echo $field->label; ?></div>
						<div class="controls" style="padding:5px;"><?php echo $field->input; ?></div>
					</div>
				<?php endforeach; ?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'payment-details', JText::_('COM_INSUREDIYTRAVEL_QUOTATION_PAYMENT_DETAILS')); ?>
				<?php foreach ($form->getFieldset("payment-details") as $field): ?>
					<div class="control-group">
						<div class="control-label"><?php echo $field->label; ?></div>
						<div class="controls" style="padding:5px;"><?php echo $field->input; ?></div>
					</div>
				<?php endforeach; ?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'traveller-info', JText::_('COM_INSUREDIYTRAVEL_QUOTATION_TRAVELLERS')); ?>
				<div class="span7">

					<?php
					$no_of_travellers = count($this->item->travellers);
					if ($no_of_travellers > 0):
						?>
						<?php
						foreach ($this->item->travellers as $k => $traveller):
							$kplus = $k + 1;
							$formname = "jform" . $kplus;
							$form->setFormControl($formname);
							$fid = "traveller-fields-" . $kplus;
							?>
							<fieldset id="<?php echo $fid; ?>">
								<legend>
									<?php echo JText::_("COM_INSUREDIYTRAVEL_TEXT_TRAVELLER") . " " . $kplus; ?>
								</legend>
								<?php
								foreach ($form->getFieldset("traveller-info") as $field):
									$field->setValue($traveller[$field->fieldname])
									?>
									<div class="control-group">
										<div class="control-label"><?php echo $field->label; ?></div>
										<div class="controls" style="padding:5px;"><?php echo $field->input; ?></div>
									</div>
								<?php endforeach; ?>
								<input type="hidden" name="formname" value="<?php echo $formname; ?>" />
								<input type="hidden" name="quotation_id" value="<?php echo $item->id; ?>" />
								<input type="hidden" name="traveller_id" value="<?php echo $traveller['traveller_id']; ?>" />
								<?php echo JHtml::_('form.token'); ?>
								<a href="#deleteModel" role="button" style="width:65px;" class="btn btn-small btn-danger" data-toggle="modal" onclick="javascript:jsInsurediy.callDeleteForm('deleteForm', '<?php echo $traveller['traveller_id']; ?>', 'quotation.deleteTraveller');"><?php echo JText::_("COM_INSUREDIYTRAVEL_BUTTON_DELETE"); ?></a>
								<a href="#" role="button" style="width: 65px;" class="btn btn-small btn-success" onclick="javascript:jsInsurediy.saveWithAjax('<?php echo $fid; ?>', 'quotation.saveTraveller', 'com_insurediytravel');
												return false;"><?php echo JText::_("COM_INSUREDIYTRAVEL_BUTTON_SAVE"); ?></a>
							</fieldset>
							<div style="height:20px;"></div>
						<?php endforeach; ?>
						<fieldset>
							<legend>
								<?php echo JText::_("COM_INSUREDIYTRAVEL_TEXT_ADD_TRAVELLER"); ?>
							</legend>
							<div id="travellerAddForm" name="travellerAddForm">
								<?php
								$form->setFormControl("jform");
								foreach ($form->getFieldset("traveller-info") as $field):
									?>
									<div class="control-group">
										<div class="control-label"><?php echo $field->label; ?></div>
										<div class="controls"><?php echo $field->input; ?></div>
									</div>
								<?php endforeach; ?>
								<input type="hidden" name="quotation_id" value="<?php echo $item->id; ?>" />
								<?php echo JHtml::_('form.token'); ?>
								<a href="#" role="button" style="width:65px" class="btn btn-small btn-success" onclick="javascript:jsInsurediy.addWithAjax('travellerAddForm', 'quotation.addTraveller', 'com_insurediytravel');
											return false;"><?php echo JText::_("COM_INSUREDIYTRAVEL_BUTTON_ADD"); ?></a>
							</div>
						</fieldset>

					<?php else: ?>
						<?php echo "No traveller entered"; ?>
					<?php endif; ?>
				</div>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<input type="hidden" name="task" value="" />
				<?php echo JHtml::_('form.token'); ?>
				<?php echo JHtml::_('bootstrap.endTabSet'); ?>
			</fieldset>
		</div>
	</div>
</form>

<div id="deleteModel" class="modal hide fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3><?php echo JText::_("COM_INSUREDIYTRAVEL_WARNING_HEADER"); ?></h3>
	</div>
	<div class="modal-body">
		<?php echo JText::_("COM_INSUREDIYTRAVEL_EXISTING_INSURANCES_DELETE_WARNING"); ?>
	</div>
	<div class="modal-footer center">
		<form style="margin: 0;" action="<?php echo JRoute::_('index.php?option=com_insurediytravel'); ?>" method="post" name="deleteForm" id="deleteForm">
			<input type="hidden" id="task" name="task" value="" />
			<input type="hidden" id="element_id" name="element_id" value="" />
			<input type="hidden" name="quotation_id" value="<?php echo $item->id; ?>" />
			<?php echo JHtml::_('form.token'); ?>
			<a href="#" data-dismiss="modal" class="btn btn-small"><?php echo JText::_("COM_INSUREDIYTRAVEL_BUTTON_CANCEL"); ?></a>
			<button type="submit" class="btn btn-small btn-danger"><?php echo JText::_("COM_INSUREDIYTRAVEL_BUTTON_DELETE"); ?></button>
		</form>
	</div>
</div>
