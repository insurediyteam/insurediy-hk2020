<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_cedeledepots
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'plan.cancel' || document.formvalidator.isValid(document.id('insurediytravel-plans-form')))
		{
<?php //echo $this->form->getField('description')->save();               ?>
			Joomla.submitform(task, document.getElementById('insurediytravel-plans-form'));
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_insurediytravel'); ?>" method="post" name="adminForm" id="insurediytravel-plans-form" class="form-validate">
	<div class="row-fluid">
		<div class="span10 form-horizontal">
			<fieldset>
				<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'info_details')); ?>
				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'info_details', JText::_('COM_INSUREDIYTRAVEL_PLAN_INFORMATION')); ?>
				<?php foreach ($this->form->getFieldset("basic") as $field): ?>
					<div class="control-group">
						<div class="control-label"><?php echo $field->label; ?></div>
						<div class="controls"><?php echo $field->input; ?></div>
					</div>
				<?php endforeach; ?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>
				<input type="hidden" name="task" value="" />
				<?php echo JHtml::_('form.token'); ?>

				<?php echo JHtml::_('bootstrap.endTabSet'); ?>
			</fieldset>
		</div>
		<!-- End Weblinks -->
		<!-- Begin Sidebar -->
		<?php //echo JLayoutHelper::render('joomla.edit.details', $this);  ?>
		<!-- End Sidebar -->
	</div>
</form>
