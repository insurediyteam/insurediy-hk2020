<?php

/**
 * @package     Joomla.Platform
 * @subpackage  Form
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */
defined('JPATH_PLATFORM') or die;

/**
 * Form Field class for the Joomla Platform.
 * Supports a one line text field.
 *
 * @package     Joomla.Platform
 * @subpackage  Form
 * @link        http://www.w3.org/TR/html-markup/input.text.html#input.text
 * @since       11.1
 */
class JFormFieldFileLink extends JFormField {

	protected $type = 'FileLink';

	protected function getInput() {
		$isList = (isset($this->element['islist'])) ? (int) $this->element['islist'] : 0;
		$basepath = (isset($this->element['basepath'])) ? (string) $this->element['basepath'] : 'media/com_insurediytravel/documents/';
		if ($this->value) :
			if ($isList) {
				foreach ($this->value as $key => $val) {
					$html[] = '<a target="_blank" href="' . MyUri::getUrls("site") . $val . '">' . $key . '</a><br>';
				}
			} else {
				$val = explode('|', $this->value);
				$path = JURI::root() . $basepath;
				$html[] = '<a target="_blank" href="' . $path . $val[0] . '">' . $val[1] . '</a>';
			}
		else :
			$html[] = 'N/A';
		endif;
		return implode($html);
	}

}
