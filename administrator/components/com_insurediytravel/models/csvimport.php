<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Transactions model.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 * @since       1.5
 */
class InsureDIYTravelModelCSVImport extends JModelAdmin {

	/**
	 * @var		string	The prefix to use with controller messages.
	 * @since   1.6
	 */
	protected $text_prefix = 'COM_INSUREDIYTRAVEL';

	/**
	 * Method to test whether a record can be deleted.
	 *
	 * @param   object	A record object.
	 * @return  boolean  True if allowed to delete the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canDelete($record) {
		if (!empty($record->id)) {
			if ($record->state != -2) {
				return;
			}
			$user = JFactory::getUser();

			if ($record->catid) {
				return $user->authorise('core.delete', 'com_insurediytravel.category.' . (int) $record->catid);
			} else {
				return parent::canDelete($record);
			}
		}
	}

	/**
	 * Method to test whether a record can have its state changed.
	 *
	 * @param   object	A record object.
	 * @return  boolean  True if allowed to change the state of the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canEditState($record) {
		$user = JFactory::getUser();

		if (!empty($record->catid)) {
			return $user->authorise('core.edit.state', 'com_insurediytravel.category.' . (int) $record->catid);
		} else {
			return parent::canEditState($record);
		}
	}

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   type	The table type to instantiate
	 * @param   string	A prefix for the table class name. Optional.
	 * @param   array  Configuration array for model. Optional.
	 * @return  JTable	A database object
	 * @since   1.6
	 */
	public function getTable($type = 'Plan', $prefix = 'InsureDIYTravelTable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array  $data		An optional array of data for the form to interogate.
	 * @param   boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return  JForm	A JForm object on success, false on failure
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true) {
		$app = JFactory::getApplication();

// Get the form.
		$form = $this->loadForm('com_insurediytravel.csvimport', 'csvimport', array('control' => 'jform', 'load_data' => $loadData));

		if (empty($form)) {
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 * @since   1.6
	 */
	protected function loadFormData() {
// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_insurediytravel.edit.plan.data', array());

		if (empty($data)) {
			$data = $this->getItem();

// Prime some default values.
			if ($this->getState('plan.id') == 0) {
				$app = JFactory::getApplication();
//$data->set('catid', $app->input->get('catid', $app->getUserState('com_contact.contacts.filter.category_id'), 'int'));
			}
		}

		$this->preprocessData('com_insurediytravel.plan', $data);

		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param   integer	The id of the primary key.
	 *
	 * @return  mixed  Object on success, false on failure.
	 * @since   1.6
	 */
	public function getItem($pk = null) {
		if ($item = parent::getItem($pk)) {

//$item->articletext = trim($item->fulltext) != '' ? $item->introtext . "<hr id=\"system-readmore\" />" . $item->fulltext : $item->introtext;

			if (!empty($item->id)) {
				/* $item->tags = new JHelperTags;
				  $item->tags->getTagIds($item->id, 'com_insurediytravel.cedeledepot');
				  $item->metadata['tags'] = $item->tags;
				 */
			}
		}

		return $item;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @since   1.6
	 */
	protected function prepareTable($table) {
		$date = JFactory::getDate();
		$user = JFactory::getUser();

		/* $table->title		= htmlspecialchars_decode($table->title, ENT_QUOTES);
		  //$table->alias		= JApplication::stringURLSafe($table->alias);

		  if (empty($table->alias))
		  {
		  $table->alias = JApplication::stringURLSafe($table->title);
		  }

		  if (empty($table->id))
		  {
		  // Set the values
		  // Set the values
		  //$table->modified	= $date->toSql();
		  //$table->modified_by	= $user->get('id');

		  // Increment the content version number.
		  //$table->version++;
		  }
		 */
	}

	/**
	 * A protected method to get a set of ordering conditions.
	 *
	 * @param   object	A record object.
	 * @return  array  An array of conditions to add to add to ordering queries.
	 * @since   1.6
	 */
	protected function getReorderConditions($table) {
		$condition = array();

		return $condition;
	}

	/**
	 * Method to import plan data
	 *
	 * @param   array  $data  The form data.
	 *
	 * @return  boolean  True on success.
	 *
	 * @since	3.1
	 */
	public function import_plans() {
		ini_set('memory_limit', '-1'); //**************IMPORTANT
		$app = JFactory::getApplication();
		$db = JFactory::getDBO();

		$files = $app->input->files->get('jform');

		$file = $files['csvimport'];

		$filename = $file['name'];
		$filename = JFile::makeSafe($filename);

		$ext = JFile::getExt($filename);

		if ($ext != 'csv') {
			return false;
		}

		$dest = JPATH_ROOT . '/idy_tmp/' . $filename;
		move_uploaded_file($file['tmp_name'], $dest);
//		JFile::upload($file['tmp_name'], $dest);

		if (($handle = fopen($dest, "r")) !== FALSE) {

			$header = fgetcsv($handle, 1000, ",");
			if (count($header) < 25) {
				return false;
			}
			$query = " TRUNCATE #__insure_travel_plans ;";
			$db->setQuery($query);
			$db->query();

			$queryData = array();
//			$fields = array("plan_name", "insurer_code", "trip_type", "group_type", "no_of_days", "no_of_travellers",
//				"no_of_adults", "no_of_children", "no_of_s_children", "flag_banner", "premium",
//				"emergency_me", "medical_expenses", "personal_accident", "rental_vehicle", "unattended_child", "personal_property",
//				"max_limit_ei", "cancellation", "interruption", "amateur_sport", "tnc", "state", "plan_index_code");

			$fields = array(
				"plan_name",
				"insurer_code",
				"trip_type",
				"group_type",
				"no_of_days",
				"no_of_travellers",
				"no_of_adults",
				"no_of_children",
				"no_of_s_children",
				"flag_banner",
				"premium",
				"medical_expenses",
				"emergency_me",
				"personal_accident",
				"personal_property",
				"baggage_delay",
				"interruption",
				"cancellation",
				"trip_rerouting",
				"rental_vehicle",
				"liability",
				"amateur_sport",
				"other_benefits",
				"tnc",
				"state",
				"ref_points",
				"pur_points",
				"plan_index_code"
			);

			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				set_time_limit(0);

				$insurer_code = isset($data[1]) ? $data[1] : "";
				$trip_type = isset($data[2]) ? $data[2] : "";
				$group_type = isset($data[3]) ? $data[3] : $data[3];
				$format = '%1$02d';
				$no_of_days = isset($data[4]) ? sprintf($format, $data[4]) : "00";
				$no_of_travellers = isset($data[5]) ? sprintf($format, $data[5]) : "00";
				$no_of_adults = isset($data[6]) ? sprintf($format, $data[6]) : "00";
				$no_of_children = isset($data[7]) ? sprintf($format, $data[7]) : "00";
				$no_of_s_children = isset($data[8]) ? sprintf($format, $data[8]) : "00";

				$plan_index_code = $insurer_code . $trip_type . $group_type . $no_of_days . $no_of_travellers . $no_of_adults . $no_of_children . $no_of_s_children;
				$data[] = $plan_index_code;
				$queryData[] = "('" . implode("', '", $data) . "')";
			}
			$count = count($queryData);
			$perrun = 1000;
			if ($count > $perrun) {
				$times = floor($count / $perrun) + 1;
				for ($i = 0; $i < $times; $i++) {
					$left = $count - ($perrun * $i);
					$length = ($left > $perrun) ? $perrun : $left;
					$tempData = array_slice($queryData, $i * $perrun, $length);
					$query = "INSERT INTO #__insure_travel_plans (" . implode(",", $fields) . ") VALUES " . implode(",", $tempData) . ";";
					$db->setQuery($query);
					$db->query();
				}
			} else {
				$query = "INSERT INTO #__insure_travel_plans (" . implode(",", $fields) . ") VALUES " . implode(",", $queryData) . ";";
				$db->setQuery($query);
				$db->query();
			}
			fclose($handle);
		}
		unlink($dest);

// iFoundries : to do
// save the Rebate Points Here

		return true;
	}

	/**
	 * Method to change the title & alias.
	 *
	 * @param   integer  $category_id  The id of the parent.
	 * @param   string   $alias        The alias.
	 * @param   string   $name         The title.
	 *
	 * @return  array  Contains the modified title and alias.
	 *
	 * @since   3.1
	 */
	protected function generateNewTitle($category_id, $alias, $name) {
// Alter the title & alias
		$table = $this->getTable();

		while ($table->load(array('alias' => $alias))) {
			if ($name == $table->title) {
				$name = JString::increment($name);
			}
			$alias = JString::increment($alias, 'dash');
		}

		return array($name, $alias);
	}

}
