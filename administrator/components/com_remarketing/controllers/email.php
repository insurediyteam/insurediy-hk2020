<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Remarketing
 * @author     Yossava A.S <yossava.adhi@insurediy.com>
 * @copyright  2019
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access
defined('_JEXEC') or die;

jimport('joomla.application.component.controllerform');

/**
 * Email controller class.
 *
 * @since  1.6
 */
class RemarketingControllerEmail extends \Joomla\CMS\MVC\Controller\FormController
{
	/**
	 * Constructor
	 *
	 * @throws Exception
	 */
	public function __construct()
	{
		$this->view_list = 'emails';
		parent::__construct();
	}
}
