<?php
/**
 * @version    CVS: 1.0.0
 * @package    Com_Remarketing
 * @author     Yossava A.S <yossava.adhi@insurediy.com>
 * @copyright  2019
 * @license    GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access
defined('_JEXEC') or die;

?>
<?php
echo "<h3>subject: ".$this->item->subject."</h3>";
echo '<h3 style="color:#3071a9;">recipient: '.$this->item->email.'</h3>';
echo '<hr>';
echo $this->item->body;
?>