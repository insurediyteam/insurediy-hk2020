<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_insurediylife
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Component Controller
 *
 * @package     Joomla.Site
 * @subpackage  com_insurediy
 * @since       1.5
 */
class InsureDIYController extends JControllerLegacy {

	protected $default_view = 'feeder';

	public function display() {
		$vName = $this->input->get('view', $this->default_view);
		$this->input->set('view', $vName);

		return parent::display();
	}

}
