<?php

defined('_JEXEC') or die;

class InsureDIYModelFeeder extends JModelForm {

	protected function populateState() {
		$app = JFactory::getApplication();
		$params = $app->getParams();
		$this->setState('params', $params);
	}

	public function getForm($data = array(), $loadData = FALSE) {
		// Get the form.
		$form = $this->loadForm('com_insurediy.feeder', 'feeder');
		if (empty($form)) {
			return false;
		}

		return $form;
	}

}
