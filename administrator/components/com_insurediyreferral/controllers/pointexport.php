<?php

defined('_JEXEC') or die;

class InsurediyReferralControllerPointExport extends JControllerForm {

	protected function allowAdd($data = array()) {
		$allow = null;
		if ($allow === null) {
			return parent::allowAdd($data);
		} else {
			return $allow;
		}
	}

	protected function allowEdit($data = array(), $key = 'id') {
		$recordId = (int) isset($data[$key]) ? $data[$key] : 0;
		$categoryId = 0;
		if ($recordId) {
			$categoryId = (int) $this->getModel()->getItem($recordId)->catid;
		}

		if ($categoryId) {
			return JFactory::getUser()->authorise('core.edit', $this->option . '.category.' . $categoryId);
		} else {
			return parent::allowEdit($data, $key);
		}
	}
	
	protected function postSaveHook(JModelLegacy $model, $validData = array()) {
		if ($task == 'save') {
			$this->setRedirect(JRoute::_('index.php?option=com_insurediyreferral&view=points', false));
		}
	}

	public function general() {

		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Set the model
		$model = $this->getModel('PointExport');

		if ($model->export_csv()) {
			$msg = ' Exporting Points Success. ';
		} else {
			$msg = ' Exporting Points Failed! Please try again...';
		}
		
		exit;
	}

}
