<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Transactions model.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 * @since       1.5
 */
class InsurediyReferralModelPointImport extends JModelAdmin {
	
	/**
	 * @var		string	The prefix to use with controller messages.
	 * @since   1.6
	 */
	protected $text_prefix = 'COM_INSUREDIYREFERRAL';
	protected $_tb_promocodes = "#__insure_promocodes";
	
	/**
	 * Method to test whether a record can be deleted.
	 *
	 * @param   object	A record object.
	 * @return  boolean  True if allowed to delete the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canDelete($record) {
		if (!empty($record->id)) {
			if ($record->state != -2) {
				return;
			}
			$user = JFactory::getUser();
			
			if ($record->catid) {
				return $user->authorise('core.delete', 'com_insurediyreferral.category.' . (int) $record->catid);
			} else {
				return parent::canDelete($record);
			}
		}
	}
	
	/**
	 * Method to test whether a record can have its state changed.
	 *
	 * @param   object	A record object.
	 * @return  boolean  True if allowed to change the state of the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canEditState($record) {
		$user = JFactory::getUser();
		
		if (!empty($record->catid)) {
			return $user->authorise('core.edit.state', 'com_insurediyreferral.category.' . (int) $record->catid);
		} else {
			return parent::canEditState($record);
		}
	}
	
	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   type	The table type to instantiate
	 * @param   string	A prefix for the table class name. Optional.
	 * @param   array  Configuration array for model. Optional.
	 * @return  JTable	A database object
	 * @since   1.6
	 */
	public function getTable($type = 'Promocode', $prefix = 'InsurediyReferralTable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}
	
	/**
	 * Method to get the record form.
	 *
	 * @param   array  $data		An optional array of data for the form to interogate.
	 * @param   boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return  JForm	A JForm object on success, false on failure
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true) {
		$app = JFactory::getApplication();
		
		// Get the form.
		$form = $this->loadForm('com_insurediyreferral.csvimport', 'csvimport', array('control' => 'jform', 'load_data' => $loadData));
		
		if (empty($form)) {
			return false;
		}
		
		return $form;
	}
	
	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 * @since   1.6
	 */
	protected function loadFormData() {
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_insurediyreferral.edit.promocode.data', array());
		
		if (empty($data)) {
			$data = $this->getItem();
			
			// Prime some default values.
			if ($this->getState('point.id') == 0) {
				$app = JFactory::getApplication();
				//$data->set('catid', $app->input->get('catid', $app->getUserState('com_contact.contacts.filter.category_id'), 'int'));
			}
		}
		
		$this->preprocessData('com_insurediyreferral.promocode', $data);
		
		return $data;
	}
	
	/**
	 * Method to get a single record.
	 *
	 * @param   integer	The id of the primary key.
	 *
	 * @return  mixed  Object on success, false on failure.
	 * @since   1.6
	 */
	public function getItem($pk = null) {
		if ($item = parent::getItem($pk)) {
			
			//$item->articletext = trim($item->fulltext) != '' ? $item->introtext . "<hr id=\"system-readmore\" />" . $item->fulltext : $item->introtext;
			
			if (!empty($item->id)) {
				/* $item->tags = new JHelperTags;
				 $item->tags->getTagIds($item->id, 'com_insurediytravel.cedeledepot');
				 $item->metadata['tags'] = $item->tags;
				 */
			}
		}
		
		return $item;
	}
	
	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @since   1.6
	 */
	protected function prepareTable($table) {
		$date = JFactory::getDate();
		$user = JFactory::getUser();
		
		/* $table->title		= htmlspecialchars_decode($table->title, ENT_QUOTES);
		 //$table->alias		= JApplication::stringURLSafe($table->alias);
		 
		 if (empty($table->alias))
		 {
		 $table->alias = JApplication::stringURLSafe($table->title);
		 }
		 
		 if (empty($table->id))
		 {
		 // Set the values
		 // Set the values
		 //$table->modified	= $date->toSql();
		 //$table->modified_by	= $user->get('id');
		 
		 // Increment the content version number.
		 //$table->version++;
		 }
		 */
	}
	
	/**
	 * A protected method to get a set of ordering conditions.
	 *
	 * @param   object	A record object.
	 * @return  array  An array of conditions to add to add to ordering queries.
	 * @since   1.6
	 */
	protected function getReorderConditions($table) {
		$condition = array();
		
		return $condition;
	}
	
	/**
	 * Method to import plan data
	 *
	 * @param   array  $data  The form data.
	 *
	 * @return  boolean  True on success.
	 *
	 * @since	3.1
	 */
	public function import_csv() {
		ini_set('memory_limit', '-1'); //**************IMPORTANT
		$app = JFactory::getApplication();
		$db = JFactory::getDBO();
		
		$files = $app->input->files->get('jform');
		
		$file = $files['csvimport'];
		
		$filename = $file['name'];
		$filename = JFile::makeSafe($filename);
		
		$ext = JFile::getExt($filename);
		
		if ($ext != 'csv') {
			return false;
		}
		
		$dest = JPATH_ROOT . '/idy_tmp/' . $filename;
		move_uploaded_file($file['tmp_name'], $dest);
		//		JFile::upload($file['tmp_name'], $dest);
		
		if (($handle = fopen($dest, "r")) !== FALSE) {
			$header = fgetcsv($handle, 1000, ",");
			
			$user = JFactory::getUser();
			$now = new JDate;
			
			$queryData = array();
			
			$field_names = array(
					"point_id",
					"user_id",
					"type",
					"points",
					"quotation",
					"unique_order_no",
					"product_id",
					"description",
					"comment",
					"created",
					"created_by",
					"modified",
					"modified_by",
					"status"
			);
			
			$adminUser = JFactory::getUser();
			
			$now = new JDate;
			
			
			$modifiedDateKey = array_search("modified", $field_names);
			$modifiedByKey = array_search("modified_by", $field_names);
			$createdDateKey = array_search("created", $field_names);
			$createdByKey= array_search("created_by", $field_names);
			
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				$dataSetString = array();
				
				set_time_limit(0);
				
				$name = $data[0];
				$email = $data[1];
				$password = $data[2];
				
				unset($data[0]);
				unset($data[1]);
				unset($data[2]);
				
				$data = array_values($data);
				
				if(count($field_names) != count($data)) {
					break;
				}
				
				if($data[$createdDateKey] == "" && $data[$createdByKey] == "") { //this section only allow insert to db
					jimport('joomla.user.helper');
					$userId = InsureDIYHelper::getUserIdByEmail($email);
					
					if($userId <= 0) {
						$user = new JUser;
						$userData = array(
								"name" => $name,
								"username" =>$email,
								"email" => $email,
								"password" => $password,
								"block"=> 0,
								"groups"=> array("1","2"),
								"resetCount" => 1
						);
						
						if(!$user->bind($userData)) {
							throw new Exception("Could not save user. Error: " . $user->getError());
							break;
						}
						
						if (!$user->save()) {
							throw new Exception("Could not save user. Error: " . $user->getError());
							break;
						}
						
						$data[1] = $user->id;
					}
					else {
						$data[1] = $userId;
					}
					
					$data[$createdDateKey] = $now->toSql();
					$data[$createdByKey] = $adminUser->id;
					$data[$modifiedDateKey] = $now->toSql();
					$data[$modifiedByKey] = $adminUser->id;
					
					$field_names2 = $field_names;
					unset($field_names2[0]);
					unset($data[0]);
					$data = array_values($data);
					
					foreach($data as $item) {
						$dataSetString[] = $db->quote($item);
					}
					
					$query = $db->getQuery(true);
					$query
						->insert($db->quoteName('#__insure_points'))
						->columns($db->quoteName($field_names2))
						->values( implode(",", $dataSetString));
					
					$db->setQuery($query);
					$db->execute();
				}
				else { //this section only allow update to db
					$data[$modifiedDateKey] = $now->toSql();
					$data[$modifiedByKey] = $adminUser->id;
					
					$queryData[] = "('" . implode("', '", $data) . "')";
					
					
					
					$updateData = (object)array_combine($field_names, $data);
					
					$check = $db->updateObject("#__insure_points", $updateData, "point_id");
				}
			}
			
// 			foreach($field_names as $item) {
// 				if($item != "unique_order_no") {
// 					$dataSetString[] = $item . " = VALUES(" . $item . ")";
// 				}				
// 			}
			
			//this section is allow insert + update to the db
// 			$count = count($queryData);
// 			$perrun = 500;
// 			if ($count > $perrun) {
// 				$times = floor($count / $perrun) + 1;
// 				for ($i = 0; $i < $times; $i++) {
// 					$left = $count - ($perrun * $i);
// 					$length = ($left > $perrun) ? $perrun : $left;
// 					$tempData = array_slice($queryData, $i * $perrun, $length);
// 					$tempData2 = array_slice($dataSetString, $i * $perrun, $length);
// 					$query = "INSERT INTO #__insure_points (" . implode(",", $field_names) . ") VALUES " . implode(",", $tempData) . " ON DUPLICATE KEY UPDATE " . implode(",", $tempData2) . ";";
// 					$db->setQuery($query);
// 					$db->query();
					
// 				}
// 			} else {
// 				$query = "INSERT INTO #__insure_points (" . implode(",", $field_names) . ") VALUES " . implode(",", $queryData) . " ON DUPLICATE KEY UPDATE " . implode(",", $dataSetString) . ";";
// 				$db->setQuery($query);
// 				$db->query();
// 			}
			
			fclose($handle);
		}
		unlink($dest);
		
		return true;
	}
}
