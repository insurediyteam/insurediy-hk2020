<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_insurediyreferral
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Transactions model.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_insurediyreferral
 * @since       1.5
 */
class InsurediyReferralModelPoint extends JModelAdmin {

	/**
	 * @var		string	The prefix to use with controller messages.
	 * @since   1.6
	 */
	protected $text_prefix = 'COM_INSUREDIYREFERRAL';
	protected $data = array();
	protected $iserror_key = "com_insurediyreferral.pointform.iserror";
	protected $form_key = "com_insurediyreferral.pointform.data";
	protected $point_id_key = "com_insurediyreferral.pointform.point_id";
	protected $_users = array();

	/**
	 * Method to test whether a record can be deleted.
	 *
	 * @param   object	A record object.
	 * @return  boolean  True if allowed to delete the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canDelete($record) {
		if (!empty($record->id)) {
			if ($record->state != -2) {
				return;
			}
			$user = JFactory::getUser();

			if ($record->catid) {
				return $user->authorise('core.delete', 'com_insurediyreferral.category.' . (int) $record->catid);
			} else {
				return parent::canDelete($record);
			}
		}
	}

	/**
	 * Method to test whether a record can have its state changed.
	 *
	 * @param   object	A record object.
	 * @return  boolean  True if allowed to change the state of the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canEditState($record) {
		$user = JFactory::getUser();

		if (!empty($record->catid)) {
			return $user->authorise('core.edit.state', 'com_insurediyreferral.category.' . (int) $record->catid);
		} else {
			return parent::canEditState($record);
		}
	}

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   type	The table type to instantiate
	 * @param   string	A prefix for the table class name. Optional.
	 * @param   array  Configuration array for model. Optional.
	 * @return  JTable	A database object
	 * @since   1.6
	 */
	public function getTable($type = 'Point', $prefix = 'InsureTable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array  $data		An optional array of data for the form to interogate.
	 * @param   boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return  JForm	A JForm object on success, false on failure
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true) {
		$app = JFactory::getApplication();
		
		// Get the form.
		$form = $this->loadForm('com_insurediyreferral.point', 'point', array('control' => 'jform', 'load_data' => $loadData));
		
		if (empty($form)) {
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 * @since   1.6
	 */
	protected function loadFormData() {
		// Check the session for previously entered form data.
// 		if (empty($this->_users)) {
// 			$this->_users = InsureDIYHelper::getAllUsers("id");
// 		}
		if (empty($this->data)) {
			$app = JFactory::getApplication();
			$this->data = array();
			$point_id = JRequest::getVar("point_id", FALSE);
			if ($point_id) {
				$tmp = $this->getItem($point_id);
				$data = & $this->data;
				foreach ($tmp as $k => $v) {
					$data[$k] = $this->prepareFormData($k, $v);
				}
			}

			$temp = (array) $app->getUserState($this->form_key, array());
			$isError = $app->getUserState($this->iserror_key, FALSE);
			if ($isError) {
				foreach ($temp as $k => $v) {
					$data[$k] = $v;
				}
				$isError = $app->setUserState($this->iserror_key, NULL);
			}
			if ($point_id) {
				$data['point_id_display'] = $point_id;
				$app->setUserState($this->point_id_key, $point_id);
			}
		}
		$this->preprocessData('com_insurediyreferral.point', $this->data);
		return $this->data;
	}

	protected function prepareFormData($k, $v) {
		switch ($k) {
			case "created_by":
				if ($v == "0") {
					return JText::_("COM_INSUREDIYREFERRAL_SYSTEM");
				}
				return isset($this->_users[$v]) ? $this->_users[$v]['username'] : JText::_("COM_INSUREDIYREFERRAL_UNDEFINED");
			case "modified_by":
				if ($v == "0") {
					return JText::_("JNEVER");
				}
				return isset($this->_users[$v]) ? $this->_users[$v]['username'] : JText::_("COM_INSUREDIYREFERRAL_UNDEFINED");
			case "modified":
				if ($v == "0000-00-00 00:00:00") {
					return JText::_("JNEVER");
				}
				return JHTML::date($v, JText::_("DATE_FORMAT_CUS"));
			case "created":
				if ($v == "0000-00-00 00:00:00") {
					return JText::_("JNEVER");
				}
				return JHTML::date($v, JText::_("DATE_FORMAT_CUS"));
			default:
				return $v;
		}
	}

	/**
	 * Method to get a single record.
	 *
	 * @param   integer	The id of the primary key.
	 *
	 * @return  mixed  Object on success, false on failure.
	 * @since   1.6
	 */
	public function getItem($pk = null) {
		$item = parent::getItem($pk);
		return $item;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @since   1.6
	 */
	protected function prepareTable($table) {
		$date = JFactory::getDate();
		$user = JFactory::getUser();
	}

	public function save($data) {
		$app = JFactory::getApplication();
		$post = $app->input->get('jform', '', 'array');
		unset($post['created']);
//
//		$table = $this->getTable();
//		$tmp = array();
//		foreach ($table->getFields() as $k => $f) {
//			if (isset($post[$k])) {
//				$tmp[$k] = $post[$k];
//			}
//		}
//		$table->bind(MyHelper::array2jObject($tmp));
//		$result = $table->store();
//		if ($result) {
//			$app->setUserState($this->point_id_key, $table->point_id);
//			return $table->point_id;
//		}
		return parent::save($post);
	}

}
