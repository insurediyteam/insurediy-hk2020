<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_insurediytravel
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Insurediy Travel model.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_insurediytravel
 * @since       1.5
 */
class InsurediyReferralModelCSVExport extends JModelAdmin {

	/**
	 * @var		string	The prefix to use with controller messages.
	 * @since   1.6
	 */
	protected $text_prefix = 'COM_INSUREDIYREFERRAL';
	protected $_tb_promocodes = "#__insure_promocodes";

	/**
	 * Method to test whether a record can be deleted.
	 *
	 * @param   object	A record object.
	 * @return  boolean  True if allowed to delete the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canDelete($record) {
		if (!empty($record->id)) {
			if ($record->state != -2) {
				return;
			}
			$user = JFactory::getUser();

			if ($record->catid) {
				return $user->authorise('core.delete', 'com_insurediyreferral.category.' . (int) $record->catid);
			} else {
				return parent::canDelete($record);
			}
		}
	}

	/**
	 * Method to test whether a record can have its state changed.
	 *
	 * @param   object	A record object.
	 * @return  boolean  True if allowed to change the state of the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canEditState($record) {
		$user = JFactory::getUser();

		if (!empty($record->catid)) {
			return $user->authorise('core.edit.state', 'com_insurediyreferral.category.' . (int) $record->catid);
		} else {
			return parent::canEditState($record);
		}
	}

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   type	The table type to instantiate
	 * @param   string	A prefix for the table class name. Optional.
	 * @param   array  Configuration array for model. Optional.
	 * @return  JTable	A database object
	 * @since   1.6
	 */
	public function getTable($type = 'Promocode', $prefix = 'InsurediyReferralTable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array  $data		An optional array of data for the form to interogate.
	 * @param   boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return  JForm	A JForm object on success, false on failure
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true) {
		// Get the form.
		$form = $this->loadForm('com_insurediyreferral.csvexport', 'csvexport', array('control' => 'jform', 'load_data' => $loadData));

		if (empty($form)) {
			return false;
		}
		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 * @since   1.6
	 */
	protected function loadFormData() {
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_insurediyreferral.edit.promocode.data', array());

		if (empty($data)) {
			$data = $this->getItem();
			if ($this->getState('promocode.id') == 0) {

			}
		}

		$this->preprocessData('com_insurediyreferral.promocode', $data);
		return $data;
	}

	public function getItem($pk = null) {
		if ($item = parent::getItem($pk)) {

			if (!empty($item->id)) {

			}
		}

		return $item;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @since   1.6
	 */
	protected function prepareTable($table) {

	}

	/**
	 * A protected method to get a set of ordering conditions.
	 *
	 * @param   object	A record object.
	 * @return  array  An array of conditions to add to add to ordering queries.
	 * @since   1.6
	 */
	protected function getReorderConditions($table) {
		$condition = array();

		return $condition;
	}

	/**
	 * Method to save the form data.
	 *
	 * @param   array  $data  The form data.
	 *
	 * @return  boolean  True on success.
	 *
	 * @since	3.1
	 */
	public function save($data) {
		$jinput = JFactory::getApplication()->input->getArray($_POST);
		$data['id'] = $jinput['jform']['id'];

		$return = parent::save($data);

		return $return;
	}

	public function export_csv() {
		ini_set('memory_limit', '-1'); //**************IMPORTANT

		$app = JFactory::getApplication();
		$db = JFactory::getDBO();

		$jform = $app->input->post->get('jform', '', 'filter');

		set_time_limit(0);
		$field_names = array(
			"code",
			"uses",
			"promo_type",
			"promo_method",
			"promo_value",
			"max_value",
			"minimum_spend",
			"usability",
			"check_new_user",
			"expiry_of",
			"applicable_products",
			"check_bought",
			"prerequisite_sale",
			//"used_by",
			"specified_users",
			"start_date",
			"end_date",
			"publish"			
		);

		$query = $db->getQuery(TRUE)
				->select($field_names)
				->from("#__insure_promocodes");
		$db->setQuery($query);
		$items = $db->loadAssocList();
		$headers = array_keys(reset($items));

		if (count($items) == 0) {
			echo 'No Items';
		} else {

			ob_start();

			$df = fopen("php://output", 'w');

			fputcsv($df, $headers);

			foreach ($items as $row) {
				$row['start_date'] = date('d-m-Y', strtotime($row['start_date']));
				$row['end_date'] = date('d-m-Y', strtotime($row['start_date']));
				//$row['created'] = date('d-m-Y H:i', strtotime($row['created']));
				
				fputcsv($df, $row);
			}
			fclose($df);
		}


		// disable caching
		$filename = 'promocodes_' . JHtml::date(NULL, 'EXPORT_DATE_FORMAT') . '.csv';

		$now = gmdate("D, d M Y H:i:s");
		header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
		header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
		header("Last-Modified: {$now} GMT");

		// force download
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");

		// disposition / encoding on response body
		header("Content-Disposition: attachment;filename={$filename}");
		header("Content-Transfer-Encoding: binary");

		return true;
	}
}
