<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_weblinks
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

if (!JFactory::getUser()->authorise('core.manage', 'com_insurediyreferral')) {
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

JLoader::register('InsurediyReferralHelper', __DIR__ . '/helpers/insurediyreferral.php');

$controller = JControllerLegacy::getInstance('InsurediyReferral');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();
