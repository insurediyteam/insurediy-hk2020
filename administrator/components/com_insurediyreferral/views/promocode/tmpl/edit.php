<?php
/**
 * @package     Joomla.Administrator
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
$form = $this->form;
$item = $this->item;
$fldGroupBasic = "basic";
$id = JRequest::getVar("id", 0);
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task) {
		if (task == 'promocode.cancel' || document.formvalidator.isValid(document.id('adminForm'))) {
			Joomla.submitform(task, document.getElementById('adminForm'));
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_insurediyreferral'); ?>" method="post" name="adminForm" id="adminForm" class="form-validate" enctype="multipart/form-data">
	<div class="row-fluid">
		<div class="span10 form-horizontal">
			<fieldset>
				<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'basic')); ?>
				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'basic', JText::_('COM_INSUREDIYREFERRAL_ITEM_TAB_BASIC')); ?>
				<?php foreach ($form->getFieldset($fldGroupBasic) as $fields): ?>
					<div class="control-group">
						<div class="control-label"><?php echo $fields->label; ?></div>
						<div class="controls"><?php echo $fields->input; ?></div>
					</div>
				<?php endforeach; ?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>
				
				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'addination', JText::_('COM_INSUREDIYREFERRAL_ITEM_TAB_ADDINATION')); ?>
				<?php foreach ($form->getFieldset('addination') as $fields): ?>
					<div class="control-group">
						<div class="control-label"><?php echo $fields->label; ?></div>
						<div class="controls"><?php echo $fields->input; ?></div>
					</div>
				<?php endforeach; ?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>
				<?php if($item->id != 0): ?>
				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'used', JText::_('COM_INSUREDIYREFERRAL_ITEM_TAB_USED_BY')); ?>
				<?php 
					$used_by = json_decode($item->used_by, true); 
					if(!empty($used_by)):
					foreach($used_by as $used):
						$user = JFactory::getUser($used['user_id']); ?>
						<div class="control-group">
							<div class="control-label"><?php echo $user->email; ?></div>
							<div class="controls" style="padding:5px;"><?php echo 'was used this promo code for: ' . $used['promo_type'] . '. Quotation id: <a href="'. JRoute::_('index.php?option=com_insurediytravel&view=quotation&layout=edit&id=' . $used['quotation_id']) .'" target="_blank">#'. $used['quotation_id'] .'</a>'; ?></div>
						</div>
				<?php endforeach;
					else:
						echo JText::_('COM_INSUREDIYREFERRAL_ITEM_NO_USED');
					endif;
				?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>
				<?php endif; ?>
				<input type="hidden" name="task" value="" />
				<?php echo JHtml::_('form.token'); ?>
				<?php echo JHtml::_('bootstrap.endTabSet'); ?>
			</fieldset>
		</div>
		<!-- Begin Sidebar -->
		<?php //echo JLayoutHelper::render('joomla.edit.details', $this);     ?>
		<!-- End Sidebar -->
	</div>
</form>
