<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_CedeleDepots
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * View to edit a weblink.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_CedeleDepots
 * @since       1.5
 */
class InsurediyReferralViewPointExport extends JViewLegacy {

	protected $state;
	protected $item;
	protected $form;

	/**
	 * Display the view
	 */
	public function display($tpl = null) {
		$this->state = $this->get('State');
		$this->item = $this->get('Item');
		$this->form = $this->get('Form');

		// Check for errors.
		if (empty($this->item)) {
			JError::raiseError(500, "No Cedele Depot Item");
			return false;
		}

		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar() {
		JFactory::getApplication()->input->set('hidemainmenu', true);
		JToolbarHelper::title("Export Points", 'product.png');
		JToolbarHelper::cancel('promocode.cancel');
	}

}
