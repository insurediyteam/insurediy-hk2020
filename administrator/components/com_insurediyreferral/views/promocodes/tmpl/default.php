<?php
/**
 * @package   Joomla.Administrator
 *
 * @copyright  Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license   GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

//JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$user = JFactory::getUser();
$userId = $user->get('id');
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
$canOrder = $user->authorise('core.edit.state', 'com_insurediyreferral.category');
$saveOrder = $listOrder == 'a.ordering';
if ($saveOrder) {
	$saveOrderingUrl = 'index.php?option=com_insurediyreferral&task=promocodes.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'formsList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}
$sortFields = $this->getSortFields();
?>
<script type="text/javascript">
	Joomla.orderTable = function()
	{
		table = document.getElementById("sortTable");
		direction = document.getElementById("directionTable");
		order = table.options[table.selectedIndex].value;
		if (order != '<?php echo $listOrder; ?>')
		{
			dirn = 'asc';
		}
		else
		{
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, '');
	}
</script>
<form action="<?php echo JRoute::_('index.php?option=com_insurediyreferral&view=promocodes'); ?>" method="post" name="adminForm" id="adminForm">
	<?php if (!empty($this->sidebar)) : ?>
		<div id="j-sidebar-container" class="span2">
			<?php echo $this->sidebar; ?>
		</div>
		<div id="j-main-container" class="span10">
		<?php else : ?>
			<div id="j-main-container">
			<?php endif; ?>
			<div id="filter-bar" class="btn-toolbar">
				<div class="filter-search btn-group pull-left">
					<label for="filter_search" class="element-invisible"><?php echo JText::_('JSEARCH_FILTER'); ?></label>
					<input type="text" name="filter_search" id="filter_search" placeholder="<?php echo JText::_('JSEARCH_FILTER'); ?>" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" title="<?php echo JText::_('COM_INSUREDIYREFERRAL_SEARCH_IN_TITLE'); ?>" />
				</div>
				<div class="btn-group pull-left">
					<button class="btn hasTooltip" type="submit" title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
					<button class="btn hasTooltip" type="button" title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>" onclick="document.id('filter_search').value = '';
							this.form.submit();"><i class="icon-remove"></i></button>
				</div>
				<div class="btn-group pull-right hidden-phone">
					<label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?></label>
					<?php echo $this->pagination->getLimitBox(); ?>
				</div>
				<div class="btn-group pull-right hidden-phone">
					<label for="directionTable" class="element-invisible"><?php echo JText::_('JFIELD_ORDERING_DESC'); ?></label>
					<select name="directionTable" id="directionTable" class="input-medium" onchange="Joomla.orderTable()">
						<option value=""><?php echo JText::_('JFIELD_ORDERING_DESC'); ?></option>
						<option value="asc" <?php if ($listDirn == 'asc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_ASCENDING'); ?></option>
						<option value="desc" <?php if ($listDirn == 'desc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_DESCENDING'); ?></option>
					</select>
				</div>
				<div class="btn-group pull-right">
					<label for="sortTable" class="element-invisible"><?php echo JText::_('JGLOBAL_SORT_BY'); ?></label>
					<select name="sortTable" id="sortTable" class="input-medium" onchange="Joomla.orderTable()">
						<option value=""><?php echo JText::_('JGLOBAL_SORT_BY'); ?></option>
						<?php echo JHtml::_('select.options', $sortFields, 'value', 'text', $listOrder); ?>
					</select>
				</div>
			</div>
			<div class="clearfix"> </div>
			<table class="table table-striped" id="formsList">
				<thead>
					<tr>
						<th class="hidden-phone">
							<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
						</th>
						<th class="nowrap">
							<?php echo JHtml::_('grid.sort', 'COM_INSUREDIYREFERRAL_TB_HEADER_ID', 'p.id', $listDirn, $listOrder); ?>
						</th>
						<th class="nowrap">
							<?php echo JHtml::_('grid.sort', 'COM_INSUREDIYREFERRAL_TB_HEADER_CODE', 'p.code', $listDirn, $listOrder); ?>
						</th>
						<th class="" class="nowrap center hidden-phone">
							<?php echo JHtml::_('grid.sort', 'COM_INSUREDIYREFERRAL_TB_HEADER_USES', 'p.uses', $listDirn, $listOrder); ?>
						</th>
						<th class="" class="nowrap center hidden-phone">
							<?php echo JText::_('COM_INSUREDIYREFERRAL_FIELD_PROMO_METHOD_LABEL'); ?>
						</th>
						<th class="" class="nowrap center hidden-phone">
							<?php echo JHtml::_('grid.sort', 'COM_INSUREDIYREFERRAL_TB_HEADER_PROMO_VALUE', 'p.promo_value', $listDirn, $listOrder); ?>
						</th>
						<th class="" class="nowrap center hidden-phone">
							<?php echo JText::_('COM_INSUREDIYREFERRAL_TB_HEADER_MINIMUM_SPEND'); ?>
						</th>
						<th class="" class="nowrap center hidden-phone">
							<?php echo JHtml::_('grid.sort', 'COM_INSUREDIYREFERRAL_TB_HEADER_START_DATE', 'p.start_date', $listDirn, $listOrder); ?>
						</th>
						<th class="" class="nowrap center hidden-phone">
							<?php echo JHtml::_('grid.sort', 'COM_INSUREDIYREFERRAL_TB_HEADER_END_DATE', 'p.end_date', $listDirn, $listOrder); ?>
						</th>
						<th class="nowrap" class="nowrap hidden-phone">
							<?php echo JHtml::_('grid.sort', 'COM_INSUREDIYREFERRAL_TB_HEADER_PUBLISH', 'p.publish', $listDirn, $listOrder); ?>
						</th>
						<th class="nowrap" class="nowrap hidden-phone">
							<?php echo JHtml::_('grid.sort', 'COM_INSUREDIYREFERRAL_TB_HEADER_CREATED', 'p.created', $listDirn, $listOrder); ?>
						</th>
						<th class="" class="nowrap hidden-phone">
							<?php echo JHtml::_('grid.sort', 'COM_INSUREDIYREFERRAL_TB_HEADER_CREATED_BY', 'p.created_by', $listDirn, $listOrder); ?>
						</th>
						<th class="nowrap" class="nowrap hidden-phone">
							<?php echo JHtml::_('grid.sort', 'COM_INSUREDIYREFERRAL_TB_HEADER_MODIFIED', 'p.modified', $listDirn, $listOrder); ?>
						</th>
						<th class="" class="nowrap hidden-phone">
							<?php echo JHtml::_('grid.sort', 'COM_INSUREDIYREFERRAL_TB_HEADER_MODIFIED_BY', 'p.modified_by', $listDirn, $listOrder); ?>
						</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="14">
							<?php echo $this->pagination->getListFooter(); ?>
						</td>
					</tr>
				</tfoot>
				<tbody>
					<?php
					foreach ($this->items as $i => $item) :
						//echo print_r($item);
						$ordering = ($listOrder == 'a.ordering');
						$item->transaction_single_link = JRoute::_('index.php?option=com_insurediyreferral&view=promocode&layout=edit&id=' . $item->id);
						$canCreate = $user->authorise('core.create', 'com_insurediyreferral.category.' . $item->id);
						$canEdit = $user->authorise('core.edit', 'com_insurediyreferral.category.' . $item->id);
//						$canCheckin = $user->authorise('core.manage', 'com_checkin') || $item->checked_out == $user->get('id') || $item->checked_out == 0;
						$canCheckin = TRUE;
						$canChange = $user->authorise('core.edit.state', 'com_insurediyreferral.category.' . $item->id) && $canCheckin;
						?>
						<tr class="row<?php echo $i % 2; ?>" >
							<td class="center hidden-phone">
								<?php echo JHtml::_('grid.id', $i, $item->id); ?>
							</td>
							<td class="center">
								<?php echo $item->id; ?>
							</td>
							<td class="nowrap has-context">
								<a href="<?php echo $item->transaction_single_link ?>">
									<?php echo (strlen($item->code) < 27) ? $item->code : substr($item->code, 0, 27) . "..."; ?>
								</a>
							</td>
							<td class="nowrap has-context">
								<?php echo ($item->uses) ? $item->uses : 'N/A'; ?>
							</td>
							<td class="nowrap has-context">
								<?php echo ($item->promo_method) ? $item->promo_method : 'Percentage'; ?>
							</td>
							<td class="nowrap has-context">
								<?php echo ($item->promo_value) ? $item->promo_value : 'N/A'; ?>
							</td>
							<td class="nowrap has-context">
								<?php echo ($item->minimum_spend) ? $item->minimum_spend : 'N/A'; ?>
							</td>
							<td class="nowrap has-context">
								<?php echo ($item->start_date != "0000-00-00 00:00:00") ? JHtml::date($item->start_date, "d/m/y h:t") : "N/A"; ?>
							</td>
							<td class="nowrap has-context">
								<?php echo ($item->end_date != "0000-00-00 00:00:00") ? JHtml::date($item->end_date, "d/m/y h:t") : "N/A"; ?>
							</td>
							<td class="nowrap has-context">
								<?php echo JHtml::_('jgrid.published', $item->publish, $i, 'promocodes.', TRUE, 'cb'); ?>
							</td>
							<td class="nowrap has-context hidden-phone">
								<?php echo $item->created; ?>
							</td>
							<td class="nowrap has-context hidden-phone">
								<?php
								echo $item->created_by;
								?>
							</td>
							<td class="nowrap has-context hidden-phone">
								<?php
								echo ($item->modified != "0000-00-00 00:00:00") ? JHtml::date($item->modified, "d/m/y h:t") : "Never";
								?>
							</td>
							<td class="nowrap has-context hidden-phone">
								<?php echo ($item->modified != "0000-00-00 00:00:00") ? $item->modified_by : "-"; ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>

			<?php //Load the batch processing form. ?>
			<?php //echo $this->loadTemplate('batch');   ?>

			<input type="hidden" name="task" value="" />
			<input type="hidden" name="boxchecked" value="0" />
			<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
			<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
			<?php echo JHtml::_('form.token'); ?>
		</div>
	</div>
</form>
