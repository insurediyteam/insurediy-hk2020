<?php
/**
 * @package     Joomla.Administrator
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
//JHtml::_('formbehavior.chosen', 'select');
$form = $this->form;
$item = $this->item;

$fldGroup = "detail";
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task) {
		console.log(Joomla);
		if (task == 'point.cancel' || document.formvalidator.isValid(document.getElementById('adminForm'))) {
			Joomla.submitform(task, document.getElementById('adminForm'));
		}
	}
</script>
<form action="<?php echo JRoute::_('index.php?option=com_insurediyreferral'); ?>" method="post" name="adminForm" id="adminForm" class="form-validate" enctype="multipart/form-data">
	<div class="row-fluid">
		<div class="span10 form-horizontal">
			<fieldset>
				<?php foreach ($form->getFieldset($fldGroup) as $fields): ?>
					<div class="control-group">
						<div class="control-label"><?php echo $fields->label; ?></div>
						<div class="controls"><?php echo $fields->input; ?></div>
					</div>
				<?php endforeach; ?>
				<input type="hidden" name="task" value="" />
				<?php echo JHtml::_('form.token'); ?>
			</fieldset>
		</div>
	</div>
</form>
