<?php

defined('_JEXEC') or die;

class InsureDIYMotorTableQuotation extends JTable {

	protected $tagsHelper = null;

	public function __construct(&$db) {
		parent::__construct('#__insure_motor_quote_master', 'id', $db);

		$this->tagsHelper = new JHelperTags();
		$this->tagsHelper->typeAlias = 'com_insurediymotor.quotation';
	}

	public function bind($array, $ignore = '') {
		if (isset($array['params']) && is_array($array['params'])) {
			$registry = new JRegistry;
			$registry->loadArray($array['params']);
			$array['params'] = (string) $registry;
		}

		return parent::bind($array, $ignore);
	}

	public function store($updateNulls = false) {

		$this->tagsHelper->preStoreProcess($this);
		$result = parent::store($updateNulls);

		return $result && $this->tagsHelper->postStoreProcess($this);
	}

	public function check() {
		return true;
	}

	public function delete($pk = null) {
		$result = parent::delete($pk);
		return $result && $this->tagsHelper->deleteTagData($this, $pk);
	}

	public function publish($pks = null, $state = 1, $userId = 0) {
		$k = $this->_tbl_key;
		JArrayHelper::toInteger($pks);
		$userId = (int) $userId;
		$state = (int) $state;
		if (empty($pks)) {
			if ($this->$k) {
				$pks = array($this->$k);
			}
			// Nothing to set publishing state on, return false.
			else {
				$this->setError(JText::_('JLIB_DATABASE_ERROR_NO_ROWS_SELECTED'));
				return false;
			}
		}

		// Build the WHERE clause for the primary keys.
		$where = $k . '=' . implode(' OR ' . $k . '=', $pks);

		if (!property_exists($this, 'checked_out') || !property_exists($this, 'checked_out_time')) {
			$checkin = '';
		} else {
			$checkin = ' AND (checked_out = 0 OR checked_out = ' . (int) $userId . ')';
		}

		$this->_db->setQuery(
				'UPDATE ' . $this->_db->quoteName($this->_tbl) .
				' SET ' . $this->_db->quoteName('state') . ' = ' . (int) $state .
				' WHERE (' . $where . ')' .
				$checkin
		);

		try {
			$this->_db->execute();
		} catch (RuntimeException $e) {
			$this->setError($e->getMessage());
			return false;
		}

		if ($checkin && (count($pks) == $this->_db->getAffectedRows())) {
			foreach ($pks as $pk) {
				$this->checkin($pk);
			}
		}

		if (in_array($this->$k, $pks)) {
			$this->state = $state;
		}

		$this->setError('');

		return true;
	}

}
