<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_InsureDIYMotor
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 * View class for a list of InsureDIYMotor.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_InsureDIYMotor
 * @since       1.5
 */
class InsureDIYMotorViewZones extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;

	/**
	 * Display the view
	 *
	 * @return  void
	 */
	public function display($tpl = null)
	{
		$this->state		= $this->get('State');
		$this->items		= $this->get('Items');
		$this->pagination	= $this->get('Pagination');

		InsureDIYMotorHelper::addSubmenu('zones');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar()
	{

		$state	= $this->get('State');
		$canDo	= InsureDIYMotorHelper::getActions($state->get('filter.category_id'));
		$user	= JFactory::getUser();
		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');

		JToolbarHelper::title(JText::_('Motor Insurances Zones'), 'insurediymotor.png');
		
		if ($canDo->get('core.create'))
		{
			//JToolbarHelper::addNew('gender.add');
		}

		if ($canDo->get('core.edit'))
		{
			//JToolbarHelper::editList('gender.edit');
		}
		
		
		if ($canDo->get('core.edit.state')) {

			//JToolbarHelper::publish('genders.publish', 'JTOOLBAR_PUBLISH', true);
			//JToolbarHelper::unpublish('genders.unpublish', 'JTOOLBAR_UNPUBLISH', true);

			//JToolbarHelper::archiveList('gender.archive');
			//JToolbarHelper::checkin('gender.checkin');
		}
		
		
		if ($state->get('filter.state') == -2 && $canDo->get('core.delete'))
		{
			//JToolbarHelper::deleteList('', 'genders.delete', 'JTOOLBAR_EMPTY_TRASH');
			
		} elseif ($canDo->get('core.edit.state')) {
		
			//JToolbarHelper::trash('genders.trash');
			
		}
		
		
	
		// Add a batch button
		/*if ($canDo->get('core.edit'))
		{
			JHtml::_('bootstrap.modal', 'collapseModal');
			$title = JText::_('JTOOLBAR_BATCH');
			$dhtml = "<button data-toggle=\"modal\" data-target=\"#collapseModal\" class=\"btn btn-small\">
						<i class=\"icon-checkbox-partial\" title=\"$title\"></i>
						$title</button>";
			$bar->appendButton('Custom', $dhtml, 'batch');
		}
		*/
		if ($canDo->get('core.admin'))
		{
			JToolbarHelper::preferences('com_insurediymotor');
		}

		//JToolbarHelper::help('JHELP_COMPONENTS_CedeleDepotS_LINKS');

		JHtmlSidebar::setAction('index.php?option=com_insurediymotor&view=quotations');
		JHtmlSidebar::setAction('index.php?option=com_insurediymotor&view=plans');
		JHtmlSidebar::setAction('index.php?option=com_insurediymotor&view=zones');
		
		/*JHtmlSidebar::addFilter(
			JText::_('JOPTION_SELECT_INSUREDIYMOTOR_GENDER'),
			'filter_gender',
			JHtml::_('select.options', InsureDIYMotorHelper::getGenderOptions() , 'value', 'text', $this->state->get('filter.gender'), true)
		);*/
		
		
		/*JHtmlSidebar::addFilter(
			JText::_('JOPTION_SELECT_PUBLISHED'),
			'filter_state',
			JHtml::_('select.options', JHtml::_('jgrid.publishedOptions'), 'value', 'text', $this->state->get('filter.state'), true)
		);
		*/
		
		/*
		JHtmlSidebar::addFilter(
			JText::_('JOPTION_SELECT_CATEGORY'),
			'filter_category_id',
			JHtml::_('select.options', JHtml::_('category.options', 'com_InsureDIYMotor'), 'value', 'text', $this->state->get('filter.category_id'))
		);

		JHtmlSidebar::addFilter(
			JText::_('JOPTION_SELECT_ACCESS'),
			'filter_access',
			JHtml::_('select.options', JHtml::_('access.assetgroups'), 'value', 'text', $this->state->get('filter.access'))
		);

		JHtmlSidebar::addFilter(
			JText::_('JOPTION_SELECT_LANGUAGE'),
			'filter_language',
			JHtml::_('select.options', JHtml::_('contentlanguage.existing', true, true), 'value', 'text', $this->state->get('filter.language'))
		);

		JHtmlSidebar::addFilter(
		'-' . JText::_('JSELECT') . ' ' . JText::_('JTAG') . '-',
		'filter_tag',
		JHtml::_('select.options', JHtml::_('tag.options', true, true), 'value', 'text', $this->state->get('filter.tag'))
		);
		*/ 
	}

	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 *
	 * @since   3.0
	 */
	protected function getSortFields()
	{
		return array(
			'g.country_iso' => JText::_('Country'),
			'g.insurer_code' => JText::_('Insurer')
		);
	}
}
