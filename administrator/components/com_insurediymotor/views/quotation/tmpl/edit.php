<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_cedeledepots
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('formbehavior.chosen', 'select');
JHtml::_('MyBehavior.jsInsurediy');
JHtml::_('script', 'https://code.jquery.com/ui/1.12.1/jquery-ui.js', false, true);
JHtml::_('stylesheet', 'https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css', false, true);
$form = $this->form;
$item = $this->item;
// print_r($item);
$activetab = JFactory::getApplication()->input->get("activetab", "info_details");

?>
<style>
.payment-column-label {
	text-align: right;
	font-weight: bold;
	width: 35%;
    padding-bottom: 10px;
}
.payment-column-value {
	padding-left: 10px;
    padding-bottom: 10px;
}

</style>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'quotation.cancel' || document.formvalidator.isValid(document.id('quotation-form')))
		{
			Joomla.submitform(task, document.getElementById('quotation-form'));
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_insurediymotor&view=quotation&layout=edit&id='.$item->id); ?>" method="post" name="adminForm" id="quotation-form" class="form-validate">
	<div class="row-fluid">
		<div class="span11 form-horizontal">
			<fieldset>
				<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => $activetab)); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'info_details', JText::_('COM_INSUREDIYMOTOR_QUOTATION_BASIC_INFORMATION')); ?>
				<?php foreach ($form->getFieldset("details") as $field): ?>
					<div class="control-group">
						<div class="control-label"><?php echo $field->label; ?></div>
						<div class="controls"><?php echo $field->input; ?></div>
					</div>
				<?php endforeach; ?>
				<div class="control-group">
						<div class="control-label">
							<label id="jform_quote_cs_required-lbl" for="jform_quote_cs_required" class="">
								User Referral Code
							</label>
						</div>
						<div class="controls" style="margin-top: 4px; color: red; font-weight: bold; font-size: 16px;">
							<?php if(isset($item->referral_id)) { echo $item->referral_id; } ?>
						</div>
					</div>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'driver-info', JText::_('COM_INSUREDIYMOTOR_QUOTATION_DRIVERS')); ?>
				<div class="span7">
						<fieldset>
						<legend>Document</legend>
						<div class="control-group">
							<div class="control-label">
								<label>Vehicle Registration Document</label>
							</div>
							<div class="controls" style="padding:5px;">
							<?php if($item->quote_file_vehicle_reg != ""): ?>
								<a download="" href="<?php echo JURI::root().$item->quote_file_vehicle_reg; ?>" class="btn button-apply btn-success"><span class="icon-download icon-white" aria-hidden="true"></span> Download</a>
							<?php else: ?>
								<button type="button" class="btn button-apply btn-warning">N/A</button>
							<?php endif; ?>
						</div>
						</div>
						<div class="control-group">
							<div class="control-label">
								<label>Driver’s Driving License</label>
							</div>
							<div class="controls" style="padding:5px;">
							<?php if($item->quote_file_driving_license != ""): ?>
								<a download="" href="<?php echo JURI::root().$item->quote_file_driving_license; ?>" class="btn button-apply btn-success"><span class="icon-download icon-white" aria-hidden="true"></span> Download</a>
							<?php else: ?>
								<button type="button" class="btn button-apply btn-warning">N/A</button>
							<?php endif; ?>
							</div>
						</div>
						<div class="control-group">
							<div class="control-label">
								<label>Driver’s HKID</label>
							</div>
							<div class="controls" style="padding:5px;">
							<?php if($item->quote_file_hkid != ""): ?>
								<a download="" href="<?php echo JURI::root().$item->quote_file_hkid; ?>" class="btn button-apply btn-success"><span class="icon-download icon-white" aria-hidden="true"></span> Download</a>
							<?php else: ?>
								<button type="button" class="btn button-apply btn-warning">N/A</button>
							<?php endif; ?>
							</div>
						</div>
						<div class="control-group">
							<div class="control-label">
								<label>Renewal Notice</label>
							</div>
							<div class="controls" style="padding:5px;">
							<?php if($item->quote_file_renewal_notice != ""): ?>
								<a download="" href="<?php echo JURI::root().$item->quote_file_renewal_notice; ?>" class="btn button-apply btn-success"><span class="icon-download icon-white" aria-hidden="true"></span> Download</a>
							<?php else: ?>
								<button type="button" class="btn button-apply btn-warning">N/A</button>
							<?php endif; ?>
							</div>
						</div>
						<div class="control-group">
							<div class="control-label">
								<label>NCD Confirmation Proof</label>
							</div>
							<div class="controls" style="padding:5px;">
							<?php if($item->quote_file_ncd != ""): ?>
								<a download="" href="<?php echo JURI::root().$item->quote_file_ncd; ?>" class="btn button-apply btn-success"><span class="icon-download icon-white" aria-hidden="true"></span> Download</a>
							<?php else: ?>
								<button type="button" class="btn button-apply btn-warning">N/A</button>
							<?php endif; ?>
							</div>
						</div>
						</fieldset>
					<?php
					$no_of_drivers = count($item->drivers);
					if ($no_of_drivers> 0):
						?>
						<?php
						$hideField = ['jform[quote_driver_loan_company][1]','jform[quote_driver_loan][1]','jform[quote_offence_point][1]', 'jform[quote_offence_point][2]', 'jform[quote_offence_point][3]', 'jform[quote_offence_point][4]', 'jform[quote_offence_point][5]', 'jform[quote_highest_offence_point][1]', 'jform[quote_highest_offence_point][2]', 'jform[quote_highest_offence_point][3]', 'jform[quote_highest_offence_point][4]', 'jform[quote_highest_offence_point][5]', 'jform[quote_driver_contact][1]', 'jform[quote_driver_contact][2]', 'jform[quote_driver_contact][3]', 'jform[quote_driver_contact][4]', 'jform[quote_driver_contact][5]', 'jform[quote_driver_email][1]', 'jform[quote_driver_email][2]', 'jform[quote_driver_email][3]', 'jform[quote_driver_email][4]', 'jform[quote_driver_email][5]', 'jform[quote_driver_exp][1]', 'jform[quote_driver_exp][2]', 'jform[quote_driver_exp][3]', 'jform[quote_driver_exp][4]', 'jform[quote_driver_exp][5]'];
						foreach ($item->drivers as $k => $driver):
							$kplus = $k + 1;
							$formname = "";
							$form->setFormControl($formname);
							$fid = "driver-fields-" . $kplus;
							?>
							<fieldset id="<?php echo $fid; ?>">
								<legend>
									<?php echo JText::_("COM_INSUREDIYMOTOR_TEXT_DRIVER") . " " . $kplus; ?>
								</legend>
								<?php
								foreach ($form->getFieldset("driver-details") as $field):
								$oriFieldName = $field->fieldname;
								$field->__set("name", "jform[".$field->fieldname."][".$k."]");
								$field->__set("id", $field->fieldname.$k);
								$field->setValue($driver[$oriFieldName]);
									?>
									<div class="control-group" style="<?php echo (in_array($field->fieldname, $hideField)) ? "display:none;" : ""; ?>">
										<div class="control-label"><?php echo $field->label; ?></div>
										<div class="controls" style="padding:5px;"><?php echo $field->input; ?></div>
									</div>
								<?php endforeach; ?>
								<input type="hidden" name="formname" value="<?php echo $formname; ?>" />
								<input type="hidden" name="quotation_id" value="<?php echo $item->id; ?>" />
								<input type="hidden" name="traveller_id" value="<?php //echo $driver['traveller_id']; ?>" />
								<?php echo JHtml::_('form.token'); ?>
								<!--  <a href="#deleteModel" role="button" style="width:65px;" class="btn btn-small btn-danger" data-toggle="modal" onclick="javascript:jsInsurediy.callDeleteForm('deleteForm', '<?php //echo $driver['traveller_id']; ?>', 'quotation.deleteDriver');"><?php echo JText::_("COM_INSUREDIYMOTOR_BUTTON_DELETE"); ?></a>
								<a href="#" role="button" style="width: 65px;" class="btn btn-small btn-success" onclick="javascript:jsInsurediy.saveWithAjax('<?php echo $fid; ?>', 'quotation.saveDriver', 'com_insurediymotor');
												return false;"><?php echo JText::_("COM_INSUREDIYMOTOR_BUTTON_SAVE"); ?></a> -->
							</fieldset>
							<div style="height:20px;"></div>
						<?php endforeach; ?>
						<!-- <fieldset>
							<legend>
								<?php //echo JText::_("COM_INSUREDIYMOTOR_TEXT_ADD_DRIVER"); ?>
							</legend>
							<div id="travellerAddForm" name="travellerAddForm">
								<?php
								$form->setFormControl("jform");
								foreach ($form->getFieldset("driver-details") as $field):
									?>
									<div class="control-group">
										<div class="control-label"><?php //echo $field->label; ?></div>
										<div class="controls"><?php //echo $field->input; ?></div>
									</div>
								<?php endforeach; ?>
								<input type="hidden" name="quotation_id" value="<?php echo $item->id; ?>" />
								<?php //echo JHtml::_('form.token'); ?>
								<a href="#" role="button" style="width:65px" class="btn btn-small btn-success" onclick="javascript:jsInsurediy.addWithAjax('travellerAddForm', 'quotation.addDriver', 'com_insurediymotor');
											return false;"><?php //echo JText::_("COM_INSUREDIYMOTOR_BUTTON_ADD"); ?></a>
							</div>
						</fieldset> -->
						
	
					<?php else: ?>
						<?php echo "No Driver Details!"; ?>
					<?php endif; ?>
				</div>
				<?php echo JHtml::_('bootstrap.endTab'); ?>
				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'vehicle_details', JText::_('COM_INSUREDIYMOTOR_QUOTATION_VEHICLE')); ?>
				<?php foreach ($form->getFieldset("vehicle_details") as $field): 
					$field->setValue($item->drivers_info[$field->fieldname])
				?>
					<div class="control-group">
						<div class="control-label"><?php echo $field->label; ?></div>
						<div class="controls"><?php echo $field->input; ?></div>
					</div>
				<?php endforeach; ?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'plan_details', JText::_('COM_INSUREDIYMOTOR_QUOTATION_PLAN_SELECTED')); ?>
					<?php foreach ($form->getFieldset("plan_header") as $field): ?>
						<div class="control-group">
							<div class="control-label"><b><?php echo $field->label; ?> </b></div>
							<div class="controls" style="padding:5px;"><b><?php echo $field->input; ?></b></div>
						</div>
					<?php endforeach; ?>
					<hr/>
				<?php foreach ($item->plans as $key => $plan): ?>
					<?php foreach ($form->getFieldset("plan_details") as $field):
						$field->setValue($plan[$field->fieldname]);
					?>
						<div class="control-group">
							<div class="control-label"><?php echo $field->label; ?></div>
							<div class="controls" style="padding:5px;"><?php echo $field->input; ?></div>
						</div>
					<?php endforeach; ?>
					<hr/>
				<?php endforeach; ?>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'payment-details', JText::_('COM_INSUREDIYMOTOR_QUOTATION_PAYMENT_DETAILS')); ?>
				<?php foreach ($form->getFieldset("payment-details") as $field): ?>
					<div class="control-group">
						<div class="control-label"><?php echo $field->label; ?></div>
						<div class="controls" style="padding:5px;"><?php echo $field->input; ?></div>
					</div>
				<?php endforeach; ?>
				<button type="button" id="payment-check-button" data-quotation-id="<?php echo $item->id; ?>">Payment Status</button>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<input type="hidden" name="task" value="quotation.saveQuote" />
				<?php echo JHtml::_('form.token'); ?>
				<?php echo JHtml::_('bootstrap.endTabSet'); ?>
			</fieldset>
		</div>
	</div>
</form>

<div id="deleteModel" class="modal hide fade">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h3><?php echo JText::_("COM_INSUREDIYMOTOR_WARNING_HEADER"); ?></h3>
	</div>
	<div class="modal-body">
		<?php echo JText::_("COM_INSUREDIYMOTOR_EXISTING_INSURANCES_DELETE_WARNING"); ?>
	</div>
	<div class="modal-footer center">
		<form style="margin: 0;" action="<?php echo JRoute::_('index.php?option=com_insurediymotor'); ?>" method="post" name="deleteForm" id="deleteForm">
			<input type="hidden" id="task" name="task" value="" />
			<input type="hidden" id="element_id" name="element_id" value="" />
			<input type="hidden" name="quotation_id" value="<?php echo $item->id; ?>" />
			<?php echo JHtml::_('form.token'); ?>
			<a href="#" data-dismiss="modal" class="btn btn-small"><?php echo JText::_("COM_INSUREDIYMOTOR_BUTTON_CANCEL"); ?></a>
			<button type="submit" class="btn btn-small btn-danger"><?php echo JText::_("COM_INSUREDIYMOTOR_BUTTON_DELETE"); ?></button>
		</form>
	</div>
</div>
<div id="dialog" title="Payment Details" style="display:none">
	<table>
		<tr>
			<td class="payment-column-label">Payment Status : </td>
			<td class="payment-column-status payment-column-value"></td>
		</tr>
		<tr>
			<td class="payment-column-label">Payment Time : </td>
			<td class="payment-column-time payment-column-value"></td>
		</tr>
		<tr>
			<td class="payment-column-label">Payment Amount : </td>
			<td class="payment-column-amount payment-column-value"></td>
		</tr>
		<tr>
			<td class="payment-column-label">Credit Card No. : </td>
			<td class="payment-column-card-no payment-column-value"></td>
		</tr>
		<tr>
			<td class="payment-column-label">Card Type : </td>
			<td class="payment-column-card-type payment-column-value"></td>
		</tr>
		<tr>
			<td class="payment-column-label">Card Holder : </td>
			<td class="payment-column-card-name payment-column-value"></td>
		</tr>
		<tr>
			<td class="payment-column-label">IP Address : </td>
			<td class="payment-column-ip-address payment-column-value"></td>
		</tr>
	</table>
</div>

<script>
	jQuery( "#dialog" ).dialog({ autoOpen: false, width: 500 });
	
	jQuery("#payment-check-button").on("click", function(obj){
		params = [
			{'name': 'id', 'value': jQuery(this).attr("data-quotation-id")},
			{'name': 'option', 'value': 'com_insurediymotor'},
			{'name': 'task', 'value': 'quotation.getPaymentStatus'},
			{'name': 'tmpl', 'value': 'ajax'},
			{'name': 'type', 'value': 'raw'}
		];
		jQuery.ajax({
			type: "POST",
			url: "index.php",
			data: params,
			dataType: "json",
			beforeSend: function() {
				jQuery(".payment-column-status").html("");
				jQuery(".payment-column-time").html("");
				jQuery(".payment-column-amount").html("");
				jQuery(".payment-column-card-no").html("");
				jQuery(".payment-column-card-type").html("");
				jQuery(".payment-column-card-name").html("");
				jQuery(".payment-column-ip-address").html("");
			},
			success: function(result){
				console.log(result);
				jQuery(".payment-column-status").html(result.status);
				jQuery(".payment-column-time").html(result.time);
				jQuery(".payment-column-amount").html(result.amt);
				jQuery(".payment-column-card-no").html(result.cardNo);
				jQuery(".payment-column-card-type").html(result.cardType);
				jQuery(".payment-column-card-name").html(result.cardName);
				jQuery(".payment-column-ip-address").html(result.ipAddress);
				
				jQuery( "#dialog" ).dialog( "open" );
			},
			error: function(xhr, textStatus, errorThrown){
				jQuery(".payment-column-status").html("");
				jQuery(".payment-column-time").html("");
				jQuery(".payment-column-amount").html("");
				jQuery(".payment-column-card-no").html("");
				jQuery(".payment-column-card-type").html("");
				jQuery(".payment-column-card-name").html("");
				jQuery(".payment-column-ip-address").html("");
				
				alert('request failed : '+errorThrown);
			}
		});
	});
</script>