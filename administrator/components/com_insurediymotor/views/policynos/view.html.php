<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_InsureDIYMotor
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * View class for a list of InsureDIYMotor.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_InsureDIYMotor
 * @since       1.5
 */
class InsureDIYMotorViewPolicyNos extends JViewLegacy {

	protected $items;
	protected $pagination;
	protected $state;

	/**
	 * Display the view
	 *
	 * @return  void
	 */
	public function display($tpl = null) {
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');

		InsureDIYMotorHelper::addSubmenu('policynos');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar() {
		require_once JPATH_COMPONENT . '/helpers/insurediymotor.php';

		$state = $this->get('State');
		$canDo = InsureDIYMotorHelper::getActions($state->get('filter.category_id'));
		$user = JFactory::getUser();
		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');

		JToolbarHelper::title(JText::_('COM_INSUREDIYMOTOR_MANAGER_PLANS'), 'insurediymotor.png');
		JToolbarHelper::deleteList('', 'policynos.delete', 'COM_INSUREDIYMOTOR_BUTTON_DELETE');
		/* if ($canDo->get('core.create'))
		  {
		  JToolbarHelper::addNew('plan.add');
		  }
		 */
//
//		if ($canDo->get('core.edit')) {
//			JToolbarHelper::editList('plan.edit');
//		}
//		if ($canDo->get('core.edit.state')) {
//
//			JToolbarHelper::publish('plans.publish', 'JTOOLBAR_PUBLISH', true);
//			JToolbarHelper::unpublish('plans.unpublish', 'JTOOLBAR_UNPUBLISH', true);
//		}
//
//		if ($canDo->get('core.admin')) {
//			JToolbarHelper::preferences('com_insurediymotor');
//		}

		JHtmlSidebar::setAction('index.php?option=com_insurediymotor&view=quotations');
		JHtmlSidebar::setAction('index.php?option=com_insurediymotor&view=genders');
		JHtmlSidebar::setAction('index.php?option=com_insurediymotor&view=plans');
		JHtmlSidebar::setAction('index.php?option=com_insurediymotor&view=csvexport');
		JHtmlSidebar::setAction('index.php?option=com_insurediymotor&view=csvimport');
		JHtmlSidebar::setAction('index.php?option=com_insurediymotor&view=policynos');

		JHtmlSidebar::addFilter(
				JText::_('JOPTION_SELECT_PUBLISHED'), 'filter_state', JHtml::_('select.options', InsureDIYMotorHelper::getPolicyNumberStateOptions(), 'value', 'text', $this->state->get('filter.state'), true)
		);
	}

	protected function getSortFields() {
		return array(
			'p.plan_name' => JText::_('COM_INSUREDIYMOTOR_HEADER_PLAN_NAME'),
			'p.state' => JText::_('JSTATUS'),
			'p.policy_number' => JText::_('COM_INSUREDIYMOTOR_HEADER_POLICY_NUMBERS'),
		);
	}

}
