<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_insurediymotor
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$user = JFactory::getUser();
$userId = $user->get('id');
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
$canOrder = $user->authorise('core.edit.state', 'com_insurediymotor.category');
$saveOrder = $listOrder == 'a.ordering';
if ($saveOrder) {
	$saveOrderingUrl = 'index.php?option=com_insurediymotor&task=plans.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'plansList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}
$sortFields = $this->getSortFields();
?>
<script type="text/javascript">
	Joomla.orderTable = function()
	{
		table = document.getElementById("sortTable");
		direction = document.getElementById("directionTable");
		order = table.options[table.selectedIndex].value;
		if (order != '<?php echo $listOrder; ?>')
		{
			dirn = 'asc';
		}
		else
		{
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, '');
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_insurediymotor&view=plans'); ?>" method="post" name="adminForm" id="adminForm">
	<?php if (!empty($this->sidebar)) : ?>
		<div id="j-sidebar-container" class="span2">
			<?php echo $this->sidebar; ?>
		</div>
		<div id="j-main-container" class="span10">
		<?php else : ?>
			<div id="j-main-container">
			<?php endif; ?>
			<div id="filter-bar" class="btn-toolbar">
				<div class="filter-search btn-group pull-left">
					<label for="filter_search" class="element-invisible"><?php echo JText::_('COM_INSUREDIYMOTOR_SEARCH_IN_TITLE'); ?></label>
					<input type="text" name="filter_search" id="filter_search" placeholder="<?php echo JText::_('COM_INSUREDIYMOTOR_SEARCH_IN_TITLE'); ?>" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" title="<?php echo JText::_('COM_INSUREDIYMOTOR_SEARCH_IN_TITLE'); ?>" />
				</div>
				<div class="btn-group pull-left">
					<button class="btn hasTooltip" type="submit" title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
					<button class="btn hasTooltip" type="button" title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>" onclick="document.id('filter_search').value = '';
							this.form.submit();"><i class="icon-remove"></i></button>
				</div>
				<div class="btn-group pull-right hidden-phone">
					<label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?></label>
					<?php echo $this->pagination->getLimitBox(); ?>
				</div>
				<div class="btn-group pull-right hidden-phone">
					<label for="directionTable" class="element-invisible"><?php echo JText::_('JFIELD_ORDERING_DESC'); ?></label>
					<select name="directionTable" id="directionTable" class="input-medium" onchange="Joomla.orderTable()">
						<option value=""><?php echo JText::_('JFIELD_ORDERING_DESC'); ?></option>
						<option value="asc" <?php if ($listDirn == 'asc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_ASCENDING'); ?></option>
						<option value="desc" <?php if ($listDirn == 'desc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_DESCENDING'); ?></option>
					</select>
				</div>
				<div class="btn-group pull-right">
					<label for="sortTable" class="element-invisible"><?php echo JText::_('JGLOBAL_SORT_BY'); ?></label>
					<select name="sortTable" id="sortTable" class="input-medium" onchange="Joomla.orderTable()">
						<option value=""><?php echo JText::_('JGLOBAL_SORT_BY'); ?></option>
						<?php echo JHtml::_('select.options', $sortFields, 'value', 'text', $listOrder); ?>
					</select>
				</div>
			</div>
			<div class="clearfix"> </div>
			<table class="table table-striped" id="plansList">
				<thead>
					<tr>
						<th width="1%" class="hidden-phone">
							<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
						</th>
						<th style="min-width:55px" class="nowrap center">
							<?php echo JHtml::_('grid.sort', 'JSTATUS', 'p.state', $listDirn, $listOrder); ?>
						</th>
						<th style="min-width:55px" class="nowrap">
							<?php echo JHtml::_('grid.sort', 'COM_INSUREDIYMOTOR_HEADER_PLAN_NAME', 'p.plan_name', $listDirn, $listOrder); ?>
						</th>
						<th class="title nowrap hidden-phone">
							<?php echo JHtml::_('grid.sort', 'COM_INSUREDIYMOTOR_HEADER_PLAN_INDEX_CODE', 'p.plan_index_code', $listDirn, $listOrder); ?>
						</th>
						<th width="5%" class="title nowrap center hidden-phone">
							<?php echo JHtml::_('grid.sort', 'COM_INSUREDIYMOTOR_HEADER_TRIP_TYPE', 'p.trip_type', $listDirn, $listOrder); ?>
						</th>
						<th style="min-width:120px;" class="title nowrap center hidden-phone">
							<?php echo JHtml::_('grid.sort', 'COM_INSUREDIYMOTOR_HEADER_GROUP_TYPE', 'p.group_type', $listDirn, $listOrder); ?>
						</th>
						<th style="min-width:120px;" class="title nowrap center hidden-phone">
							<?php echo JHtml::_('grid.sort', 'COM_INSUREDIYMOTOR_HEADER_PREMIUM', 'p.premium', $listDirn, $listOrder); ?>
						</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="10">
							<?php echo $this->pagination->getListFooter(); ?>
						</td>
					</tr>
				</tfoot>
				<tbody>
					<?php
					foreach ($this->items as $i => $item) :
						//echo print_r($item);
						$ordering = ($listOrder == 'a.ordering');
						$item->transaction_single_link = JRoute::_('index.php?option=com_insurediymotor&view=plan&layout=edit&plan_id=' . $item->plan_id);
						$canCreate = $user->authorise('core.create', 'com_insurediymotor.category.' . $item->plan_id);
						$canEdit = $user->authorise('core.edit', 'com_insurediymotor.category.' . $item->plan_id);
						$canCheckin = $user->authorise('core.manage', 'com_checkin') || $item->checked_out == $user->get('id') || $item->checked_out == 0;
						$canChange = $user->authorise('core.edit.state', 'com_insurediymotor.category.' . $item->plan_id) && $canCheckin;
						?>
						<tr class="row<?php echo $i % 2; ?>" >
							<td class="center hidden-phone">
								<?php echo JHtml::_('grid.id', $i, $item->plan_id); ?>
							</td>
							<td class="center">
								<div class="btn-group">
									<?php echo JHtml::_('jgrid.published', $item->state, $i, 'plans.', $canChange); ?>
								</div>
							</td>
							<td class="nowrap has-context">
								<a href="<?php echo $item->transaction_single_link ?>"><?php echo ($item->plan_name) ? $item->plan_name : 'N/A'; ?></a>
							</td>
							<td class="nowrap has-context">
								<?php echo (isset($item->plan_index_code)) ? $item->plan_index_code : 'N/A'; ?>
							</td>
							<td class="nowrap center has-context">
								<?php echo (isset($item->trip_type)) ? JText::_("COM_INSUREDIYMOTOR_TRIP_TYPE_" . $item->trip_type) : 'N/A'; ?>
							</td>
							<td class="nowrap center has-context">
								<?php echo (isset($item->group_type)) ? JText::_("COM_INSUREDIYMOTOR_GROUP_TYPE_" . $item->group_type) : 'N/A'; ?>
							</td>
							<td class="nowrap center has-context">
								<?php echo (isset($item->premium)) ? number_format($item->premium) : 'N/A'; ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>

			<?php //Load the batch processing form.  ?>
			<?php //echo $this->loadTemplate('batch');  ?>

			<input type="hidden" name="task" value="" />
			<input type="hidden" name="boxchecked" value="0" />
			<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
			<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
			<?php echo JHtml::_('form.token'); ?>
		</div>
</form>
