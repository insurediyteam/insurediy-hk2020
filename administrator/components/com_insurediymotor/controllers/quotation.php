<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_CedeleDepots
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Weblink controller class.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_CedeleDepots
 * @since       1.6
 */
class InsureDIYMotorControllerQuotation extends JControllerForm {

	/**
	 * Method override to check if you can add a new record.
	 *
	 * @param   array  $data  An array of input data.
	 *
	 * @return  boolean
	 *
	 * @since   1.6
	 */
	protected function allowAdd($data = array()) {
		$user = JFactory::getUser();
		//$categoryId = JArrayHelper::getValue($data, 'catid', $this->input->getInt('filter_category_id'), 'int');
		$allow = null;

		/* if ($categoryId)
		  {
		  // If the category has been passed in the URL check it.
		  $allow = $user->authorise('core.create', $this->option . '.category.' . $categoryId);
		  }
		 */

		if ($allow === null) {
			// In the absense of better information, revert to the component permissions.
			return parent::allowAdd($data);
		} else {
			return $allow;
		}
	}

	/**
	 * Method to check if you can add a new record.
	 *
	 * @param   array   $data  An array of input data.
	 * @param   string  $key   The name of the key for the primary key.
	 *
	 * @return  boolean
	 * @since   1.6
	 */
	protected function allowEdit($data = array(), $key = 'id') {
		$recordId = (int) isset($data[$key]) ? $data[$key] : 0;
		$categoryId = 0;
		if ($recordId) {
			$categoryId = (int) $this->getModel()->getItem($recordId)->catid;
		}

		if ($categoryId) {
			// The category has been set. Check the category permissions.
			return JFactory::getUser()->authorise('core.edit', $this->option . '.category.' . $categoryId);
		} else {
			// Since there is no asset tracking, revert to the component permissions.
			return parent::allowEdit($data, $key);
		}
	}

	/**
	 * Method to run batch operations.
	 *
	 * @param   object  $model  The model.
	 *
	 * @return  boolean   True if successful, false otherwise and internal error is set.
	 *
	 * @since   1.7
	 */
	public function batch($model = null) {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Set the model
		$model = $this->getModel('InsuredDIYLife', '', array());

		// Preset the redirect
		$this->setRedirect(JRoute::_('index.php?option=com_insurediymotors&view=quotations' . $this->getRedirectToListAppend(), false));

		return parent::batch($model);
	}

	/**
	 * Function that allows child controller access to model data after the data has been saved.
	 *
	 * @param   JModelLegacy  $model      The data model object.
	 * @param   array         $validData  The validated data.
	 *
	 * @return	void
	 * @since	1.6
	 */
	protected function postSaveHook(JModelLegacy $model, $validData = array()) {
		if ($task == 'save') {
			$this->setRedirect(JRoute::_('index.php?option=com_insurediymotors&view=quotations', false));
		}
	}

	public function saveEI() {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$data = JFactory::getApplication()->input->getArray($_POST);
		$data['jform']['id'] = $data['ei_id'];

		$data['jform']['issuance_date'] = date("Y-m-d", strtotime($data['jform']['issuance_date']));
		$model = $this->getModel("existinginsurance");
		$result = $model->save($data['jform']);

		echo json_encode($data);
		exit;
	}

	public function addEI() {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$data = JFactory::getApplication()->input->getArray($_POST);
		$model = $this->getModel("existinginsurance");
		$data['jform']['quotation_id'] = $data['quotation_id'];
		$data['jform']['issuance_date'] = date("Y-m-d", strtotime($data['jform']['issuance_date']));
		$result = $model->save($data['jform']);
		echo json_encode($result);
		exit;
	}

	public function deleteEI() {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$jInput = JFactory::getApplication()->input->getArray($_POST);
		$model = $this->getModel("existinginsurance");
		$data['quotation_id'] = $jInput['quotation_id'];
		$data['ei_id'] = $jInput['element_id'];
		$result = $model->delete($data);
		if ($result) {
			MyUri::redirect("index.php?option=com_insurediymotor&view=quotation&layout=edit&id=" . $data['quotation_id'] . "&activetab=existing_insurances", JText::_("COM_INSUREDIYMOTOR_SUCCESSFULLY_DELETE_EI_MSG"));
		} else {
			MyUri::redirect("index.php?option=com_insurediymotor&view=quotation&layout=edit&id=" . $data['quotation_id'] . "&activetab=existing_insurances", JText::_("COM_INSUREDIYMOTOR_FAIL_TO_DELETE_EI_MSG"));
		}
	}

	public function saveDriver() {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$db = JFactory::getDbo();
		$jinput = JFactory::getApplication()->input;
		$formname = $jinput->post->get("formname");
		$data = $jinput->get($formname, array(), "array");
		$data['dob'] = date("Y-m-d", strtotime($data['dob']));
		$data['traveller_id'] = $jinput->post->get("traveller_id");
		$data['quotation_id'] = $jinput->post->get("quotation_id");

		$obj = MyHelper::array2object($data);

		$db->updateObject("#__insure_travel_quotations_to_travellers", $obj, "traveller_id");

		$result['success'] = "1";
		echo json_encode($result);
		exit;
	}

	public function addDriver() {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$db = JFactory::getDbo();
		$jinput = JFactory::getApplication()->input;
		$data = $jinput->get("jform", array(), "array");
		$quotation_id = $jinput->post->get("quotation_id");
		$data['quotation_id'] = $quotation_id;
		$data['dob'] = date("Y-m-d", strtotime($data['dob']));

		$obj = MyHelper::array2jObject($data);
		$db->insertObject("#__insure_travel_quotations_to_travellers", $obj);
		$result['success'] = "1";
		echo json_encode($result);
		exit;
	}

	public function deleteDriver() {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));
		$db = JFactory::getDbo();
		$jInput = JFactory::getApplication()->input->post;
		$quotation_id = $jInput->get('quotation_id', FALSE);
		$traveller_id = $jInput->get('element_id', FALSE);
		$result = ($quotation_id && $traveller_id);
		if ($result) {
			$query = $db->getQuery(TRUE)
					->delete("#__insure_travel_quotations_to_travellers")
					->where("traveller_id = " . $db->quote($traveller_id))
					->where("quotation_id = " . $db->quote($quotation_id));
			$db->setQuery($query);
			$result = $db->execute();
		}

		if ($result) {
			MyUri::redirect("index.php?option=com_insurediymotor&view=quotation&layout=edit&id=" . $quotation_id . "&activetab=driver-info", JText::_("COM_INSUREDIYMOTOR_SUCCESSFULLY_DELETE_EI_MSG"));
		} else {
			MyUri::redirect("index.php?option=com_insurediymotor&view=quotation&layout=edit&id=" . $quotation_id . "&activetab=driver-info", JText::_("COM_INSUREDIYMOTOR_FAIL_TO_DELETE_EI_MSG"));
		}
	}

	public function generatepdf() {
		$app = JFactory::getApplication();
		$id = $app->input->get("id", FALSE);
		$model = $this->getModel();
		if ($id && MyHelper::is_admin()) {
			$data = $model->getPdfData($id);
			$plans = $model->getPlans($id);
			$pdfarrs = array();
			$pdfarrs2 = array();
			$xmlpath = JPATH_COMPONENT . "/models/forms/";
			foreach ($plans as $plan) {
				// add plan data to the data object
				$data->plan_name = $plan->plan_name;
				$data->plan_par = "nonpar";
				$data->price = "HK$" . $plan->price;
				$data->namesurfix = "_" . JHtml::_('date', time(), 'dMYHis') . ".pdf";
				$data->policy_number = $plan->policy_number;

				//broker info
				$data->broker_code = "89192";
				$data->broker_name = "InsureDIY Limited";
				$data->broker_rep = "Choo Oi San";
				$data->broker_ph = "3975 2896";

				switch (strtolower(substr($plan->plan_index_code, 0, 3))) {
					case 'aia':
						$result = InsureDIYHelper::generateAIA($xmlpath, $data);
						$pdfarrs[$plan->plan_index_code] = $result;
						$data->namesurfix = "_" . JHtml::_('date', time(), 'dMYHis') . "2.pdf";

						$result = InsureDIYHelper::generateAIA($xmlpath, $data, FALSE);
						$pdfarrs2[$plan->plan_index_code] = $result;
						break;
					case 'sun':
						$result = InsureDIYHelper::generateSUN($xmlpath, $data);
						$pdfarrs[$plan->plan_index_code] = $result;
						$data->namesurfix = "_" . JHtml::_('date', time(), 'dMYHis') . "2.pdf";
						$result = InsureDIYHelper::generateSUN($xmlpath, $data, FALSE);
						$pdfarrs2[$plan->plan_index_code] = $result;
						break;
					default:
						break;
				}
			}


			$model->savePDFPaths($id, $pdfarrs);
			$model->savePDFPaths($id, $pdfarrs2, FALSE);
		}
		MyUri::redirect("index.php?option=com_insurediymotor&view=quotation&layout=edit&id=" . $id . "&activetab=files");
	}

	public function saveQuote() {
		$jinput = JFactory::getApplication()->input;
		
		$model = $this->getModel();
		
		$result = $model->saveQuote($jinput->get('jform', array(), 'ARRAY'));
		file_put_contents('./msigReq.log', json_encode($jinput->get('jform', array(), 'ARRAY')), FILE_APPEND);
		
		$this->setRedirect(JRoute::_('index.php?option=' . $this->option . '&view=' . $this->view_item. $this->getRedirectToItemAppend($jinput->get("id"), "id"), false));
	}
	
	public function saveQuoteClose() {
		$jinput = JFactory::getApplication()->input;
		
		$model = $this->getModel();
		
		$result = $model->saveQuote($jinput->get('jform', array(), 'ARRAY'));
		file_put_contents('./msigReq.log', json_encode($jinput->get('jform', array(), 'ARRAY')), FILE_APPEND);
		
		$this->setRedirect(JRoute::_('index.php?option=' . $this->option . '&view=quotations', false));
	}

	public function getPaymentStatus() {
		$jinput = JFactory::getApplication()->input;
		
		$model = $this->getModel();
		
		$result = $model->getPaymentStatus($jinput->get("id"));
		
		echo json_encode($result);
		JFactory::getApplication()->close(); // or jexit();
	}
}

