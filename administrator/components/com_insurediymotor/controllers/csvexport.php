<?php

defined('_JEXEC') or die;

class InsureDIYMotorControllerCSVExport extends JControllerForm {

	protected function allowAdd($data = array()) {
		$allow = null;
		if ($allow === null) {
			return parent::allowAdd($data);
		} else {
			return $allow;
		}
	}

	protected function allowEdit($data = array(), $key = 'id') {
		$recordId = (int) isset($data[$key]) ? $data[$key] : 0;
		$categoryId = 0;
		if ($recordId) {
			$categoryId = (int) $this->getModel()->getItem($recordId)->catid;
		}

		if ($categoryId) {
			return JFactory::getUser()->authorise('core.edit', $this->option . '.category.' . $categoryId);
		} else {
			return parent::allowEdit($data, $key);
		}
	}

	public function batch($model = null) {
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Set the model
		$model = $this->getModel('InsuredDIYLife', '', array());

		// Preset the redirect
		$this->setRedirect(JRoute::_('index.php?option=com_insurediymotor&view=plans' . $this->getRedirectToListAppend(), false));

		return parent::batch($model);
	}

	protected function postSaveHook(JModelLegacy $model, $validData = array()) {
		if ($task == 'save') {
			$this->setRedirect(JRoute::_('index.php?option=com_insurediymotor&view=plans', false));
		}
	}

	public function general() {

		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Set the model
		$model = $this->getModel('CSVExport');

		if ($model->export_csv()) {

		} else {
			
		}

		exit;
	}

}
