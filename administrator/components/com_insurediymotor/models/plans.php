<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Methods supporting a list of weblink records.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 * @since       1.6
 */
class InsureDIYMotorModelPlans extends JModelList {

	/**
	 * Constructor.
	 *
	 * @param   array  An optional associative array of configuration settings.
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array()) {
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'plan_index_code', 'p.plan_index_code',
				'plan_name', 'p.plan_name',
				'trip_type', 'p.trip_type',
				'group_type', 'p.group_type',
				'premium', 'p.premium'
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since   1.6
	 */
	protected function populateState($ordering = null, $direction = null) {
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$accessId = $this->getUserStateFromRequest($this->context . '.filter.access', 'filter_access');
		$this->setState('filter.access', $accessId);

		$stateId = $this->getUserStateFromRequest($this->context . '.filter.state', 'filter_state');
		$this->setState('filter.state', $stateId);

		$insurer_codeId = $this->getUserStateFromRequest($this->context . '.filter.insurer_code', 'filter_insurer_code');
		$this->setState('filter.insurer_code', $insurer_codeId);

		$year_codeId = $this->getUserStateFromRequest($this->context . '.filter.trip_type', 'filter_trip_type');
		$this->setState('filter.trip_type', $year_codeId);

		$is_smokingId = $this->getUserStateFromRequest($this->context . '.filter.group_type', 'filter_group_type');
		$this->setState('filter.group_type', $is_smokingId);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_insurediymotor');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('p.plan_name', 'asc');
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id    A prefix for the store id.
	 * @return  string  A store id.
	 * @since   1.6
	 */
	protected function getStoreId($id = '') {
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.access');
		$id .= ':' . $this->getState('filter.state');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return  JDatabaseQuery
	 * @since   1.6
	 */
	protected function getListQuery() {
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select('*');
		$query->from($db->quoteName('#__insure_motor_plans') . ' AS p');

		// Filter by published state
		$published = $this->getState('filter.state');
		if (is_numeric($published)) {
			$query->where('p.state = ' . (int) $published);
		} elseif ($published === '') {
			$query->where('(p.state IN (0, 1))');
		}

		// Filter by Insurer Code state
		$insurer_code = $this->getState('filter.insurer_code');
		if ($insurer_code) {
			$query->where('p.insurer_code = ' . $db->Quote($insurer_code, false));
		}

		// Filter by Gender state
		$trip_type = $this->getState('filter.trip_type', FALSE);
		if ($trip_type) {
			$query->where('p.trip_type = ' . $db->Quote($trip_type));
		}

		// Filter by Age state
		$group_type = $this->getState('filter.group_type', FALSE);
		if ($group_type) {
			$query->where('p.group_type = ' . $db->Quote($group_type));
		}

		// Filter by search in title
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			$search = $db->quote('%' . $db->escape($search, true) . '%');
			$query->where(' (p.plan_name LIKE ' . $search . ') OR (p.plan_index_code LIKE ' . $search . ')');
		}

		// Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		$query->order($db->escape($orderCol . ' ' . $orderDirn));


		//echo nl2br(str_replace('#__','jos_',$query));

		return $query;
	}

}
