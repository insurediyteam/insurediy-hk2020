<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_insurediytravel
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Insurediy Motor model.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_insurediymotor
 * @since       1.5
 */
class InsureDIYMotorModelCSVExport extends JModelAdmin {

	/**
	 * @var		string	The prefix to use with controller messages.
	 * @since   1.6
	 */
	protected $text_prefix = 'COM_INSUREDIYMOTOR';
	protected $_tb_quotation = "#__insure_motor_quotations";
	protected $_tb_quotation_plan = "#__insure_motor_quotations_to_plans";
	protected $_tb_traveller = "#__insure_motor_quotations_to_drivers";

	/**
	 * Method to test whether a record can be deleted.
	 *
	 * @param   object	A record object.
	 * @return  boolean  True if allowed to delete the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canDelete($record) {
		if (!empty($record->id)) {
			if ($record->state != -2) {
				return;
			}
			$user = JFactory::getUser();

			if ($record->catid) {
				return $user->authorise('core.delete', 'com_insurediymotor.category.' . (int) $record->catid);
			} else {
				return parent::canDelete($record);
			}
		}
	}

	/**
	 * Method to test whether a record can have its state changed.
	 *
	 * @param   object	A record object.
	 * @return  boolean  True if allowed to change the state of the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canEditState($record) {
		$user = JFactory::getUser();

		if (!empty($record->catid)) {
			return $user->authorise('core.edit.state', 'com_insurediymotor.category.' . (int) $record->catid);
		} else {
			return parent::canEditState($record);
		}
	}

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   type	The table type to instantiate
	 * @param   string	A prefix for the table class name. Optional.
	 * @param   array  Configuration array for model. Optional.
	 * @return  JTable	A database object
	 * @since   1.6
	 */
	public function getTable($type = 'Plan', $prefix = 'InsureDIYMotorTable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array  $data		An optional array of data for the form to interogate.
	 * @param   boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return  JForm	A JForm object on success, false on failure
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true) {
		// Get the form.
		$form = $this->loadForm('com_insurediymotor.csvexport', 'csvexport', array('control' => 'jform', 'load_data' => $loadData));

		if (empty($form)) {
			return false;
		}
		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 * @since   1.6
	 */
	protected function loadFormData() {
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_insurediymotor.edit.plan.data', array());

		if (empty($data)) {
			$data = $this->getItem();
			if ($this->getState('plan.id') == 0) {

			}
		}

		$this->preprocessData('com_insurediymotor.plan', $data);
		return $data;
	}

	public function getItem($pk = null) {
		if ($item = parent::getItem($pk)) {

			if (!empty($item->id)) {

			}
		}

		return $item;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @since   1.6
	 */
	protected function prepareTable($table) {

	}

	/**
	 * A protected method to get a set of ordering conditions.
	 *
	 * @param   object	A record object.
	 * @return  array  An array of conditions to add to add to ordering queries.
	 * @since   1.6
	 */
	protected function getReorderConditions($table) {
		$condition = array();

		return $condition;
	}

	/**
	 * Method to save the form data.
	 *
	 * @param   array  $data  The form data.
	 *
	 * @return  boolean  True on success.
	 *
	 * @since	3.1
	 */
	public function save($data) {
		$jinput = JFactory::getApplication()->input->getArray($_POST);
		$data['id'] = $jinput['jform']['id'];

		$return = parent::save($data);

		return $return;
	}

	public function export_csv() {
		ini_set('memory_limit', '-1'); //**************IMPORTANT

		$app = JFactory::getApplication();
		$db = JFactory::getDBO();

		$jform = $app->input->post->get('jform', '', 'filter');

		$datatype = $jform['export_type'];
		set_time_limit(0);

		$query = $db->getQuery(TRUE)
		->select("*")
		->from("#__insure_motor_quote_master q")
		->join("inner", "#__insure_motor_driver_info ON #__insure_motor_driver_info.quote_master_id = q.id")
		->order("q.quote_updated_time DESC");
		switch ($datatype) {
			case 'pending_quotations':
				$query->where("quote_process_stage != 'completed'");
			break;
			case 'completed_quotations':
				$query->where("quote_process_stage = 'completed'");
			break;
			case 'HLA_quotations':
				$query->where("quote_policy_selected_partner_code = 'hla'");
			break;
		}
		
		if (strlen($jform['date_start']) > 0 && strlen($jform['date_end']) > 0) {
			$sdate = new JDate($jform['date_start']);
			$edate = new JDate($jform['date_end']);
			$query->where("DATE(" . $db->quote($sdate->toSql()) . ') <= DATE(q.quote_updated_time)');
			$query->where('DATE(q.quote_updated_time) <= DATE(' . $db->quote($edate->toSql()) . ")");
		} elseif (strlen($jform['date_start']) > 0) {
			$sdate = new JDate($jform['date_start']);
			$query->where('DATE(q.quote_updated_time) >= DATE(' . $db->quote($sdate->toSql()) . ")");
		} elseif (strlen($jform['date_end']) > 0) {
			$edate = new JDate($jform['date_end']);
			$query->where('DATE(q.quote_updated_time) <= DATE(' . $db->quote($edate->toSql()) . ")");
		}
		
		$db->setQuery($query);
		$items = $db->loadAssocList();
		
		$resultArr = array();
		
		if (count($items) == 0) {
			echo 'No Items';
		} else {
			foreach ($items as $key => $row) {
				$driverOcc = explode(",", $row["quote_driver_occ"]);
				$driverIndustry = explode(",", $row["quote_driver_industry"]);
				$driverOccs = [];

				for ($i=0; $i < count($driverOcc); $i++) { 
					$query = $db->getQuery(TRUE)
					->select("*")
					->from("#__insure_motor_occupation")
					->where("occ_code = '".$driverOcc[$i]."'");
					$db->setQuery($query);
					$occ = $db->loadObject();

					$query = $db->getQuery(TRUE)
					->select("*")
					->from("#__insure_motor_occupation_parent")
					->where("id = '".$driverIndustry[$i]."'");
					$db->setQuery($query);
					$industry = $db->loadObject();

					$driverOccs[] = $industry->occ_name . "(" . $occ->occ_name . ")";
				}
				

			
				$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_motor_loan_company")
				->where("mortgagee_code = '".$row["quote_driver_loan_company"]."'");
				$db->setQuery($query);
				$loan = $db->loadObject();

				if (!isset($loan)) {
					$loan = new stdClass();
					$loan->mortgagee_name = "";
				}

				
				$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_motor_current_insurer")
				->where("insurer_code = '".$row["quote_driver_current_insurer"]."'");
				$db->setQuery($query);
				$currentInsurer = $db->loadObject();
				
				$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_motor_model_master")
				->where("model_code = '".$row["quote_car_model"]."'");
				$db->setQuery($query);
				$model = $db->loadObject();
				
				$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_motor_make_master")
				->where("make_code = '".$row["quote_car_make"]."'");
				$db->setQuery($query);
				$make = $db->loadObject();
				
				$relationshipWithDriver = "";

				$relations = explode(",", $row["quote_driver_main_driver_relation"]);
				$relationsArr = [];

				for ($i=0; $i < count($relations); $i++) { 

					switch($relations[$i]) {
						case "F":
							$relationshipWithDriver = "Friend";
							break;
						case "R":
							$relationshipWithDriver = "Relative";
							break;
						case "L":
							$relationshipWithDriver = "Siblings";
							break;
						case "P":
							$relationshipWithDriver = "Parent";
							break;
						case "S":
							$relationshipWithDriver = "Spouse";
							break;
						case "C":
							$relationshipWithDriver = "Child";
							break;
						case "I":
							$relationshipWithDriver = "Self";
							break;
						case "O":
							$relationshipWithDriver = "Others";
							break;
					}

					$relationsArr[] = $relationshipWithDriver;
				}
				
				
				
				$driverRace = "";
				
				switch($row['quote_driver_race']) {
					case "CHI":
						$driverRace= "Chinese";
						break;
					case "IND":
						$driverRace= "Indian";
						break;
					case "MAL":
						$driverRace= "Malay";
						break;
					case "OTH":
						$driverRace= "Others";
						break;
				}
				
				$nricType = "";
				
				switch($row['quote_driver_nric_type']) {
					case "N":
						$nricType= "NRIC";
						break;
					case "B":
						$nricType= "NRIC (PR)";
						break;
					case "E":
						$nricType= "FIN (Employment Pass)";
						break;
					case "W":
						$nricType= "FIN (Work Permit / S Pass)";
						break;
				}
				
				$ncdReason = "";
				
				switch($row['quote_car_zero_ncd_reason']) {
					case "null":
						$ncdReason= "N/A";
						break;
					case "NILR1":
						$ncdReason= "First time buying a vehicle";
						break;
					case "NILR2":
						$ncdReason= "2nd or 3rd vehicle";
						break;
					case "NILR3":
						$ncdReason= "Have been driving others vehicle";
						break;
					case "NILR4":
						$ncdReason= "Other reasons";
						break;
				}
				
				$user = JFactory::getUser($row["userId"]);
				$email = $user->get('email');
				$username = $user->get('username');

				if ($row['quote_cover_type'] == "01") {
					$coverType = "COMPREHENSIVE";
				} else {
					$coverType = "THIRD PARTY";
				}

				switch ($row['quote_car_body_type']) {
					case '1':
						$bodyType = "CONVERTIBLE";
						break;
					case '2':
						$bodyType = "COUPE";
						break;
					case '3':
						$bodyType = "HATCHBACK";
						break;
					case '5':
						$bodyType = "SALOON";
						break;
					case '6':
						$bodyType = "SPORTS";
						break;
					case '7':
						$bodyType = "STATION WAGGON";
						break;
					case '9':
						$bodyType = "STATION WAGGON (WA)";
						break;
					case '8':
						$bodyType = "STATION WAGGON (HVY)";
						break;
					
					default:
						$bodyType = "SALOON";
						break;
				}
				
				$quoteDataArr = array(
						"Quotation Id" => $row['quote_master_id'],
						"Username" => $username, 
						"Account Email" => $email,
						"Terms Agreed" => $row['quote_user_terms_agreed'], 
						"Privacy Agreed" => $row['quote_user_privacy_agreed'], 
						"Remarketing Agreed" => $row['quote_user_marketing_agreed'], 
						"Declaration Agreed" => $row['quote_user_declaration_agreed'], 
						"Last Updated Time" => $row['quote_updated_time'],
						"Quote Created Time" => $row['quote_created_time'],
						"Selected Car Variant" => $row['quote_selected_model_variant'], 
						"Promo Code" => $row['quote_promo_code'], 
						"Process Stage" => $row['quote_process_stage'], 
						"Changed Before Quote" => $row['quote_pre_quote_change_flag'], 
						"Policy Start Date" => $row['quote_policy_start_date'], 
						"Selected Plan Code" => $row['quote_policy_selected_plan_code'], 
						"Selected Partner Code" => $row['quote_policy_selected_partner_code'], 
						"Previous Policy Number" => $row['quote_policy_previous_code'], 
						"Policy Number" => $row['quote_policy_code'], 
						// "Any workshop?" => $row['quote_plan_workshop'], 
						// "NCD Protector" => $row['quote_plan_ncdpro'], 
						// "Loss Of Use" => $row['quote_plan_los'], 
						"Payment Transaction No." => $row['quote_payment_txn_id'],
						"Payment Steps" => $row['quote_payment_stage'], 
						"Payment Request Id" => $row['quote_payment_request_id'], 
						"Payment Amount" => $row['quote_payment_amount'], 
						"Quote Deleted Time" => $row['quote_deleted_time'], 
						// "Customer Service Requested?" => $row['quote_cs_required'],
						// "Off Peak Car" => $row['quote_off_peak'],
						"Car Chassis No." => $row['quote_car_chassis_no'],
						"Car Engine No." => $row['quote_car_engine_no'],
						"Car Make" => $make->make_name,
						"Car Manufacture Year" => $row['quote_car_make_year'],
						"Body Type" => $bodyType,
						// "Car Register Year" => $row['quote_car_reg_year'],
						"Car Model" => $model->model_name,
						"Engine CC" => $row['quote_car_engine_cc'],
						"Estimated Car Value" => $row['quote_car_value'],
						"NCD" => $row['quote_car_ncd'],
						"Other NCD in Force" => $row['quote_car_other_ncd'],
						"Night Parking" => $row['quote_night_parking'],
						"Number of Claim" => $row['quote_driver_claim_no'],
						"Claim Amount" => $row['quote_driver_claim_amount'],
						"Total Offence Point" => $row['quote_offence_point'],
						"Highest Offence Point" => $row['quote_highest_offence_point'],
						// "China Ext" => $row['quote_china_extension'],
						"Cover Type" => $coverType,
						// "0 NCD reason" => $ncdReason,
						"Previous Car Registration No." => $row['quote_car_prev_reg_no'],
						"Car Registration No." => $row['quote_car_reg_no'], 
						"Address Line 1" => $row['quote_driver_address_line_1'], 
						"Address Line 2" => $row['quote_driver_address_line_2'],
						// "COE Expiry Date" => $row['quote_car_coe_expire_date'], 
						// "Company Registered?" => $row['quote_car_com_registered'], 
						//"Block No." => $row['quote_driver_address_block_no'], 
						//"Building Name" => $row['quote_driver_address_building_name'],
						//"Street Name" => $row['quote_driver_address_street_name'], 
						//"Unit No." => $row['quote_driver_address_unit_no'], 
						// "Postcode" => $row['quote_driver_postcode'], 
						"Driver Country" => $row['quote_driver_country'], 
						// "Driver Claim Amount" => $row['quote_driver_claim_amount'], 
						// "Driver No. of Claim" => $row['quote_driver_claim_no'], 
						"Driver Contact" => $row['quote_driver_contact'], 
						// "Claimed Amount" => $row['quote_car_claim_amount'],
						// "Numbers of claims" => $row['quote_car_claim_no'], 
						"Current Insurer" => $currentInsurer->insurer_name, 
						// "Driver Demerit Points" => $row['quote_driver_demerit_point'], 
						"Driver Date Of Birth" => $row['quote_driver_dob'],
						"Driver Email" => $row['quote_driver_email'], 
						"Driver Driving Experience" => $row['quote_driver_exp'], 
						"Driver First Name" => $row['quote_driver_first_name'], 
						"Driver Last Name" => $row['quote_driver_last_name'], 
						"Driver Gender" => $row['quote_driver_gender'], 
						"Driver Nationality" => $row['quote_driver_nationality'],
						"Driver HKID" => $row['quote_driver_nric'],
						// "Driver Nric Type" => $nricType,
						"Driver Occupation" => implode(",", $driverOccs),
						// "Driver Occupation Type" => $row['quote_driver_occ_type'] == "INDO" ? "Indoor" : "Outdoor",
						// "Driver Race" => $driverRace, 
						"Driver Marital Status" => $row['quote_driver_marital_status'], 
						// "Driver is appliant" => $row['quote_driver_is_appliant'], 
						// "Driver Has Valid License?" => $row['quote_driver_valid_license'],
						"Relationship with Main Driver" => implode(",", $relationsArr), 
						"Is Car under Loan" => $row['quote_driver_loan'], 
						"Loan Company" => $loan->mortgagee_name
						// "Cert of Merit" => $row['quote_driver_merit_cert'], 
						// "Drive To West MY?" => $row['quote_driver_to_west_my']
				);
				
				
				$plansArr = array(
						"aig_plan_name" => "",
						"aig_plan_premium_total" => "",
						"aig_car_variant" => "",
						"aw_plan_name" => "",
						"aw_plan_premium_total" => "",
						"aw_car_variant" => "",
						"pingan_plan_name" => "",
						"pingan_plan_premium_total" => "",
						"pingan_car_variant" => ""
				);

				$docsArr = array(
						"Car Registration File" => $row['quote_file_vehicle_reg'],
						"Driving Lincense File" => $row['quote_file_driving_license'],
						"HKID File" => $row['quote_file_hkid'],
						"Renewal Notice File" => $row['quote_file_renewal_notice'],
						"NCD Proof File" => $row['quote_file_ncd'],
						"Copy of Named Driver 1 Driving License" => $row['quote_file_vehicle_reg_2'],
						"Copy of Named Driver 1 HKID Card" => $row['quote_file_hkid_2'],
						"Copy of Named Driver 2 Driving License" => $row['quote_file_vehicle_reg_3'],
						"Copy of Named Driver 2 HKID Card" => $row['quote_file_hkid_3']
				);
				
				
				$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_motor_quote_aig")
				->where("quote_master_id = '".$row["quote_master_id"]."'");
				$db->setQuery($query);
				$aigQuote = $db->loadAssoc();

				
				if($aigQuote) {
					
					$query = $db->getQuery(TRUE)
					->select("model_name")
					->from("#__insure_motor_make_model_aig")
					->where("id = '".$aigQuote["quote_car_variant"]."'");
					$db->setQuery($query);
					$aigModelName = $db->loadResult();

					$plansArr["aig_plan_name"] = $aigQuote["aig_plan_name"];
					$plansArr["aig_plan_premium_total"] = $aigQuote["aig_plan_premium_total"];
					$plansArr["aig_car_variant"] = $aigModelName;
				}

				$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_motor_quote_aw")
				->where("quote_master_id = '".$row["quote_master_id"]."'");
				$db->setQuery($query);
				$awQuote = $db->loadAssoc();

				
				if($awQuote) {
					
					$query = $db->getQuery(TRUE)
					->select("model_name")
					->from("#__insure_motor_make_model_aw")
					->where("id = '".$awQuote["quote_car_variant"]."'");
					$db->setQuery($query);
					$awModelName = $db->loadResult();

					$plansArr["aw_plan_name"] = $awQuote["aw_plan_name"];
					$plansArr["aw_plan_premium_total"] = $awQuote["aw_plan_premium_total"];
					$plansArr["aw_car_variant"] = $awModelName;
				}

				$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_motor_quote_pingan")
				->where("quote_master_id = '".$row["quote_master_id"]."'");
				$db->setQuery($query);
				$pinganQuote = $db->loadAssoc();

				
				if($pinganQuote) {
					
					$query = $db->getQuery(TRUE)
					->select("model_name")
					->from("#__insure_motor_make_model_pingan")
					->where("id = '".$pinganQuote["quote_car_variant"]."'");
					$db->setQuery($query);
					$pinganModelName = $db->loadResult();

					$plansArr["pingan_plan_name"] = $pinganQuote["pingan_plan_name"];
					$plansArr["pingan_plan_premium_total"] = $pinganQuote["pingan_plan_premium_total"];
					$plansArr["pingan_car_variant"] = $pinganModelName;
				}
				
				
				$resultArr[] = array_merge($quoteDataArr, $plansArr, $docsArr);

			}

			
			
			$keys = array_keys($resultArr[0]);
			
			if (($check = array_search("id", $keys)) !== false) {
				unset($keys[$check]);
			}
			
			$headers = $keys;
			
			ob_start();

			$df = fopen("php://output", 'w');

			fputcsv($df, $headers);

			foreach ($resultArr as $row) {
				fputcsv($df, $row);
			}
			fclose($df);
		}


		// disable caching
		$filename = 'motor_' . $datatype . '_' . JHtml::date(NULL, 'EXPORT_DATE_FORMAT') . '.csv';

		$now = gmdate("D, d M Y H:i:s");
		header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
		header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
		header("Last-Modified: {$now} GMT");

		// force download
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");

		// disposition / encoding on response body
		header("Content-Disposition: attachment;filename={$filename}");
		header("Content-Transfer-Encoding: binary");

		return true;
	}

	function exportQuotations() {
		$csvData = $this->getCSVData();
		$csvTitle = JHtml::date('now', "d_M_Y_H_i_s") . "_space_bookings.csv";
		MyHelper::fileDownload($csvTitle, $csvData);
		return true;
	}

	public function getCSVData($completedOnly, $pendingOnly) { // this is nasty. need to fix later.
		$items = $this->getItems($completedOnly, $pendingOnly);
		$qids = array_keys($items);
		$temps = $this->getDrivers($qids);
		$travellers = array();
		foreach ($temps as $temp) {
			$travellers[$temp['quotation_id']][] = $temp;
		}

		foreach ($travellers as $key => $q_travellers) {
			if (isset($items[$key])) {
				foreach ($q_travellers as $qkey => $q_traveller) {
					foreach (array_keys($q_traveller) as $tkey) {
						if ($tkey != "traveller_id" && $tkey != "quotation_id") {
							$field_key = $tkey . "_" . ($qkey + 1);
							$items[$key][$field_key] = $q_traveller[$tkey];
						}
					}
				}
			}
		}
		$headers = array();
		foreach ($items as $item) {
			if (count(array_keys($item)) > count(array_keys($headers))) {
				$headers = array_keys($item);
			}
		}

		$csv = array();
		foreach ($items as $item) {
			$tmp = array();
			foreach ($item as $k => $v) {
				$tmp[] = $v;
			}
			$csv[] = implode(',', MyHelper::escapeCSV($tmp, TRUE));
		}

		$csvMasterData = array(
			implode(',', MyHelper::escapeCSV($headers, TRUE)), //csv header
			implode("\n", $csv) //csv items
		);
		return (!empty($csvMasterData)) ? implode("\n", $csvMasterData) : '';
	}

	public function getItems($completedOnly, $pendingOnly) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("q.*, qp.insurer_code, qp.plan_name, qp.premium")
				->from($this->_tb_quotation . " AS q")
				->leftjoin($this->_tb_quotation_plan . " AS qp ON q.id = qp.quotation_id")
				->order("q.id", "ASC");
		if ($completedOnly) {
			$query->where("q.quote_status = 1");
		}
		if ($pendingOnly) {
			$query->where("q.quote_status = 0");
		}
		return $db->setQuery($query)->loadAssocList("id");
	}

	public function getDrivers($quotations) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from($this->_tb_traveller)
				->where("quotation_id IN (" . implode(",", $quotations) . ")");
		return $db->setQuery($query)->loadAssocList();
	}

}
