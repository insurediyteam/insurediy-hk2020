<?php

defined('_JEXEC') or die;

class InsureDIYMotorModelQuotation extends JModelAdmin {

	protected $text_prefix = 'COM_INSUREDIYMOTOR';

	protected function canDelete($record) {
		if (!empty($record->id)) {
			if ($record->state != -2) {
				return;
			}
			$user = JFactory::getUser();

			if ($record->catid) {
				return $user->authorise('core.delete', 'com_insurediymotor.category.' . (int) $record->catid);
			} else {
				return parent::canDelete($record);
			}
		}
	}

	protected function canEditState($record) {
		$user = JFactory::getUser();

		if (!empty($record->catid)) {
			return $user->authorise('core.edit.state', 'com_insurediymotor.category.' . (int) $record->catid);
		} else {
			return parent::canEditState($record);
		}
	}

	public function getTable($type = 'Quotation', $prefix = 'InsureDIYMotorTable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}

	public function getForm($data = array(), $loadData = true) {
		// Get the form.
		$form = $this->loadForm('com_insurediymotor.quotation', 'quotation', array('control' => 'jform', 'load_data' => $loadData));

		if (empty($form)) {
			return false;
		}

		return $form;
	}

	protected function loadFormData() {
		$data = JFactory::getApplication()->getUserState('com_insurediymotor.edit.quotation.data', array());

		if (empty($data)) {
			$data = $this->getItem();
		}
		$this->preprocessData('com_insurediymotor.quotation', $data);

		return $data;
	}

	public function getItem($pk = null) {
		if ($item = parent::getItem($pk)) {
			if (!empty($item->id)) {

			}
// 			$plan = $this->getPlan($item->id);
// 			if (!empty($plan)) {
// 				unset($plan['id']);
// 				unset($plan['quotation_id']);
// 				foreach ($plan as $k => $v) {
// 					$key = "plan_" . $k;
// 					$item->$key = $v;
// 				}
// 			}
			$driverData = $this->getDrivers($item->id);
			
			$item->drivers = $driverData["driverList"];
			$item->drivers_info =  $driverData["driverInfo"];
			
			$plans = $this->getPlan($item->id);
			
			if(!empty($plans)) {
				$item->plans = $plans;
			}
			
			$user = JFactory::getUser($item->userId);
			
			if($user) {
				$item->referral_id = $user->referral_id;
			}
		}

		return $item;
	}
	
	public function getDrivers($quotation_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)->select("*")
				->from("#__insure_motor_driver_info")
				->where("quote_master_id = '" . $quotation_id."'");
		$db->setQuery($query);
		$result = $db->loadAssoc();
		
		$size = count(explode(",", $result["quote_driver_nric"]));
		
		$makeCode = $result["quote_car_make"];
		$modelCode= $result["quote_car_model"];
		
		$query = $db->getQuery(TRUE)->select("make_name")
		->from("#__insure_motor_make_master")
		->where("make_code = '" . $makeCode."'");
		$db->setQuery($query);
		$make = $db->loadResult();
		
		$query = $db->getQuery(TRUE)->select("model_name")
		->from("#__insure_motor_model_master")
		->where("model_code = '" . $modelCode."'");
		$db->setQuery($query);
		$model = $db->loadResult();
		
		$query = $db->getQuery(TRUE)->select("quote_policy_previous_code")
		->from("#__insure_motor_quote_master")
		->where("id = '" . $quotation_id."'");
		$db->setQuery($query);
		$prevPolicy= $db->loadResult();
		
		$drivers = array();
		
		for($i=0; $i<$size; $i++) {
			$driver = array();
			
			$driver["quote_driver_address_line_2"] = $result["quote_driver_address_line_2"];
			$driver["quote_driver_address_line_1"] = $result["quote_driver_address_line_1"];
			$driver["quote_driver_loan_company"] = $result["quote_driver_loan_company"];
			$driver["quote_driver_loan"] = $result["quote_driver_loan"];
			$driver["quote_driver_valid_license"] = explode(",", $result["quote_driver_valid_license"])[$i];
			$driver["quote_driver_gender"] = explode(",", $result["quote_driver_gender"])[$i];
			$driver["quote_driver_first_name"] = explode(",", $result["quote_driver_first_name"])[$i];
			$driver["quote_driver_last_name"] = explode(",", $result["quote_driver_last_name"])[$i];
			$driver["quote_driver_email"] = explode(",", $result["quote_driver_email"])[$i];
			$driver["quote_driver_nric"] = explode(",", $result["quote_driver_nric"])[$i];
			$driver["quote_driver_nric_type"] = explode(",", $result["quote_driver_nric_type"])[$i];
			$driver["quote_driver_contact"] = $i==0 ? explode(",", $result["quote_driver_contact"])[$i] : "";
			$driver["quote_driver_dob"] = explode(",", $result["quote_driver_dob"])[$i];
			$driver["quote_offence_point"] = explode(",", $result["quote_offence_point"])[$i];
			$driver["quote_highest_offence_point"] = explode(",", $result["quote_highest_offence_point"])[$i];
			$driver["quote_driver_exp"] = explode(",", $result["quote_driver_exp"])[$i];
			$driver["quote_driver_occ_type"] = explode(",", $result["quote_driver_occ_type"])[$i];
			$driver["quote_driver_occ"] = explode(",", $result["quote_driver_occ"])[$i];
			$driver["quote_driver_industry"] = explode(",", $result["quote_driver_industry"])[$i];
			$driver["quote_driver_nationality"] = explode(",", $result["quote_driver_nationality"])[$i];
			$driver["quote_driver_race"] = explode(",", $result["quote_driver_race"])[$i];
			$driver["quote_driver_marital_status"] = explode(",", $result["quote_driver_marital_status"])[$i];
			$driver["quote_driver_main_driver_relation"] = explode(",", $result["quote_driver_main_driver_relation"])[$i];
			$driver["quote_driver_demerit_point"] = explode(",", $result["quote_driver_demerit_point"])[$i];
			
			$drivers[] = $driver;
		}
		
		unset($result["id"]);
		unset($result["quote_driver_gender"]);
		unset($result["quote_driver_first_name"]);
		unset($result["quote_driver_last_name"]);
		unset($result["quote_driver_email"]);
		unset($result["quote_driver_nric"]);
		unset($result["quote_driver_nric_type"]);
		unset($result["quote_driver_contact"]);
		unset($result["quote_driver_dob"]);
		unset($result["quote_driver_exp"]);
		unset($result["quote_driver_occ_type"]);
		unset($result["quote_driver_occ"]);
		unset($result["quote_driver_nationality"]);
		unset($result["quote_driver_marital_status"]);
		unset($result["quote_driver_main_driver_relation"]);
		unset($result["quote_driver_demerit_point"]);
		unset($result["quote_driver_race"]);
		
		$result["quote_car_make"] = $make;
		$result["quote_car_model"] = $model;
		$result["quote_policy_previous_code"] = $prevPolicy;
		
// 		if($result["quote_driver_current_insurer"] != "") {
// 			$query = $db->getQuery(TRUE)->select("insurer_name")
// 			->from("#__insure_motor_current_insurer")
// 			->where("insurer_code = '" . $result["quote_driver_current_insurer"]."'");
// 			$db->setQuery($query);
// 			$result["quote_driver_current_insurer"] = $db->loadResult();
// 		}
		
// 		if($result["quote_driver_loan_company"] != "") {
// 			$query = $db->getQuery(TRUE)->select("mortgagee_name")
// 			->from("#__insure_motor_loan_company")
// 			->where("mortgagee_code = '" . $result["quote_driver_loan_company"]."'");
// 			$db->setQuery($query);
// 			$result["quote_driver_loan_company"] = $db->loadResult();
// 		}
		
		return array("driverList" => $drivers, "driverInfo" => $result);
	}

	public function getPlan($quotation_id) {
		$returnData = array();
		$db = JFactory::getDbo();
		
		$query = $db->getQuery(TRUE)
		->select("*")
		->from("#__insure_motor_quote_master")
		->where("id = " . $quotation_id);
		$db->setQuery($query);
		$quoteData = $db->loadObject();
		
		// aig quote
		$query = $db->getQuery(TRUE)->select("*")
		->from("#__insure_motor_quote_aig")
		->where("quote_master_id = '" . $quotation_id."'");
		
		$db->setQuery($query);
		$result = $db->loadObject();
		
		if($result) {
			
			$excess = "n/a";
			$query = $db->getQuery(TRUE)->select("*")
			->from("#__insure_motor_make_model_aig")
			->where("id = '" . $result->quote_car_variant."'");
			$db->setQuery($query);
			$model = $db->loadObject();
			
			$itemData = array(
					"plan_id" => $result->aig_plan_id,
					"plan_name" => $result->aig_plan_name,
					"plan_premium_total" => $result->aig_plan_premium_total,
					//"plan_risk" => "<pre>".$excess."</pre>",
					"car_variant" => $model->model_name,
					"car_variant_cc" => $model->model_cc
			);
			
			$returnData["aig"] = $itemData;

		}

		// aw quote
		$query = $db->getQuery(TRUE)->select("*")
		->from("#__insure_motor_quote_aw")
		->where("quote_master_id = '" . $quotation_id."'");
		
		$db->setQuery($query);
		$result = $db->loadObject();
		
		if($result) {
			
			$excess = "n/a";
			$query = $db->getQuery(TRUE)->select("*")
			->from("#__insure_motor_make_model_aw")
			->where("id = '" . $result->quote_car_variant."'");
			$db->setQuery($query);
			$model = $db->loadObject();
			
			$itemData = array(
					"plan_id" => $result->aw_plan_id,
					"plan_name" => $result->aw_plan_name,
					"plan_premium_total" => $result->aw_plan_premium_total,
					//"plan_risk" => "<pre>".$excess."</pre>",
					"car_variant" => $model->model_name,
					"car_variant_cc" => $model->model_cc
			);
			
			$returnData["aw"] = $itemData;

		}

		// pingan quote
		$query = $db->getQuery(TRUE)->select("*")
		->from("#__insure_motor_quote_pingan")
		->where("quote_master_id = '" . $quotation_id."'");
		
		$db->setQuery($query);
		$result = $db->loadObject();
		
		if($result) {
			
			$excess = "n/a";
			$query = $db->getQuery(TRUE)->select("*")
			->from("#__insure_motor_make_model_pingan")
			->where("id = '" . $result->quote_car_variant."'");
			$db->setQuery($query);
			$model = $db->loadObject();
			
			$itemData = array(
					"plan_id" => $result->pingan_plan_id,
					"plan_name" => $result->pingan_plan_name,
					"plan_premium_total" => $result->pingan_plan_premium_total,
					//"plan_risk" => "<pre>".$excess."</pre>",
					"car_variant" => $model->model_name,
					"car_variant_cc" => $model->model_cc
			);
			
			$returnData["pingan"] = $itemData;

		}
		
		
		return $returnData;
	}

	public function getTransaction($unique_order_no) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)->select("*")
				->from("#__insure_motor_quote_master")
				->where("quote_payment_txn_id = '" . $db->quote($unique_order_no)."'");
		$db->setQuery($query);

		return $db->loadObject();
	}

	public function getFileApplicationPaths($json, $id) {
		$tmp = array();
		$decoded = json_decode($json);
		$array = (is_array($decoded)) ? $decoded : MyHelper::object2array($decoded);
		foreach ($array as $path) {
			$vals = explode('|', $path);
			foreach ($vals as $val) {
				$tmp[$val] = MyHelper::getDeepPath(QUOTATION_PDF_SAVE_PATH, $id, TRUE) . trim($val);
			}
		}

		return $tmp;
	}

	protected function prepareTable($table) {

	}

	protected function getReorderConditions($table) {
		$condition = array();

		return $condition;
	}

	public function getPaymentStatus($quoteId) {
		date_default_timezone_set("Asia/Kuala_Lumpur");
		
		$db = JFactory::getDbo();
		$params = JComponentHelper::getParams('com_insurediymotor');
		$ch = curl_init();
		
		$query = $db->getQuery(TRUE)->select("*")
		->from("#__insure_motor_quote_master")
		->where("id = '" . $quoteId."'");
		$db->setQuery($query);
		
		$quoteData = $db->loadObject();
		
		$returnData = (object)array(
				"status" => "",
				"time" => "",
				"amt" => "",
				"cardNo" => "",
				"cardType" => "",
				"ipAddress" => "",
		);
		
		switch ($quoteData->quote_policy_selected_partner_code) {
			case "sompo":
				$url = $params->get('wcRestUrl', "").$params->get('wcMerchantId', "")."/payments/?request_id=".$quoteData->quote_payment_request_id;
				
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array(
						'Accept: application/json',
				));
				curl_setopt($ch, CURLOPT_USERPWD, $params->get('wcUsername', "").":".$params->get('wcPassword', ""));
				curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
				
				$server_output = curl_exec ($ch);
				$result = json_decode($server_output);
				$errorMsg = curl_error($ch);
				
				curl_close($ch);
				
				$db = JFactory::getDBO();
				
				if($server_output) {
					if(!empty((array)$result->payments)) {
						$data = $result->payments->payment[0];
						$reqId = $data->{'request-id'};
						$transactionState = $data->{'transaction-state'};
						$transactionId = $data->{'transaction-id'};
						
						if($transactionState == "success") {
							$returnData->status = $transactionState;
							$returnData->time = date('Y-m-d H:i:s', ($data->{'completion-time-stamp'}/ 1000));
							$returnData->amt = "S$" . $data->{'requested-amount'}->value;
							$returnData->cardNo = $data->{'card-token'}->{'masked-account-number'};
							$returnData->cardType = $data->{'card'}->{'card-type'};
							$returnData->cardName = $data->{'account-holder'}->{'first-name'}. " " . $data->{'account-holder'}->{'last-name'};
							$returnData->ipAddress = $data->{'ip-address'};
							
							return $returnData;
						}
					}
					else {
						return false;
					}
				}
				else {

					return false;
				}
				
				break;
			case "msig":
				if($quoteData->quote_payment_request_id != "") {
					$postData = array(
							"merchantId" => $params->get('merchantId', ""),
							"loginId" => $params->get('apiloginId', ""),
							"password" => $params->get('apiPassword', ""),
							"actionType" => "Query",
							"orderRef" => $quoteData->quote_payment_request_id
					);
					
					curl_setopt($ch, CURLOPT_URL, $params->get('apiUrl', ""));
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
					
					$server_output = curl_exec ($ch);
					$server_output = json_encode(simplexml_load_string($server_output, "SimpleXMLElement", LIBXML_NOCDATA));
					$server_output = json_decode($server_output,TRUE);
					$server_output = $server_output["record"];
					$errorMsg = curl_error($ch);
					
					curl_close($ch);
					
					$result= array();
					
					if($server_output) {
						$returnData->status = $server_output["orderStatus"];
						$returnData->time = $server_output["txTime"];
						$returnData->amt = "S$" . $server_output["amt"];
						$returnData->cardNo = $server_output["panFirst4"]. "********" . $server_output["panLast4"];
						$returnData->cardType = $server_output["payMethod"];
						$returnData->cardName = $server_output["holder"];
						$returnData->ipAddress = $server_output["sourceIp"];
						
						if(isset($server_output["orderStatus"]) && $server_output["orderStatus"] == "Accepted") {
							//update payment details if it is not populated
							$updateData = (object)array(
									"id" => $quoteId,
									"quote_process_stage" => "completed",
									"quote_payment_stage" => "success",
									"quote_payment_txn_id" => $server_output["payRef"]
							);
							
							$db->updateObject("#__insure_motor_quote_master", $updateData, "id");
						}
						
						return $returnData;
					}
				}
				break;
			case "pingan":
				if($quoteData->quote_payment_request_id != "") {
					$postData = array(
							"merchantId" => $params->get('merchantId', ""),
							"loginId" => $params->get('apiloginId', ""),
							"password" => $params->get('apiPassword', ""),
							"actionType" => "Query",
							"orderRef" => $quoteData->quote_payment_request_id
					);
					
					curl_setopt($ch, CURLOPT_URL, $params->get('apiUrl', ""));
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
					curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($postData));
					
					$server_output = curl_exec ($ch);
					echo $server_output;
					exit();
					$server_output = json_encode(simplexml_load_string($server_output, "SimpleXMLElement", LIBXML_NOCDATA));
					$server_output = json_decode($server_output,TRUE);
					$server_output = $server_output["record"];
					$errorMsg = curl_error($ch);
					
					curl_close($ch);
					
					$result= array();
					
					if($server_output) {
						$returnData->status = $server_output["orderStatus"];
						$returnData->time = $server_output["txTime"];
						$returnData->amt = "HK$" . $server_output["amt"];
						$returnData->cardNo = $server_output["panFirst4"]. "********" . $server_output["panLast4"];
						$returnData->cardType = $server_output["payMethod"];
						$returnData->cardName = $server_output["holder"];
						$returnData->ipAddress = $server_output["sourceIp"];
						
						if(isset($server_output["orderStatus"]) && $server_output["orderStatus"] == "Accepted") {
							//update payment details if it is not populated
							$updateData = (object)array(
									"id" => $quoteId,
									"quote_process_stage" => "completed",
									"quote_payment_stage" => "success",
									"quote_payment_txn_id" => $server_output["payRef"]
							);
							
							$db->updateObject("#__insure_motor_quote_master", $updateData, "id");
						}
						
						return $returnData;
					}
				}
				break;
		}
	}
	
	public function save($data) {
		$app = JFactory::getApplication();

		$jinput = JFactory::getApplication()->input->getArray($_POST);
		$data['id'] = $jinput['jform']['id'];
		$return = parent::save($data);

		return $return;
	}

	public function saveQuote($data) {
		if($data["quote_process_stage"] != "completed" && $data["quote_payment_txn_id"] == "0") {
			$db = JFactory::getDbo();
			
			$quoteData = (object)Array(
					"id" => $data["id"],
					"quote_promo_code" => $data["quote_promo_code"],
					"quote_policy_previous_code" => $data["quote_policy_previous_code"]
					);
			
			$driverData = (object)Array(
					"quote_master_id" => $data["id"],
					"quote_driver_first_name" => implode(",", $data["quote_driver_first_name"]),
					"quote_driver_last_name" => implode(",", $data["quote_driver_last_name"]),
					"quote_driver_gender" => implode(",", $data["quote_driver_gender"]),
					"quote_driver_main_driver_relation" => implode(",", $data["quote_driver_main_driver_relation"]),
					"quote_driver_dob" => implode(",", $data["quote_driver_dob"]),
					"quote_driver_nric_type" => implode(",", $data["quote_driver_nric_type"]),
					"quote_driver_nric" => implode(",", $data["quote_driver_nric"]),
					"quote_driver_marital_status" => implode(",", $data["quote_driver_marital_status"]),
					"quote_driver_race" => implode(",", $data["quote_driver_race"]),
					"quote_driver_occ" => implode(",", $data["quote_driver_occ"]),
					"quote_driver_occ_type" => implode(",", $data["quote_driver_occ_type"]),
					"quote_driver_contact" => implode(",", $data["quote_driver_contact"]),
					"quote_offence_point" => implode(",", $data["quote_offence_point"]),
					"quote_highest_offence_point" => implode(",", $data["quote_highest_offence_point"]),
					"quote_driver_email" => implode(",", $data["quote_driver_email"]),
					"quote_driver_exp" => implode(",", $data["quote_driver_exp"]),
					"quote_driver_demerit_point" => implode(",", $data["quote_driver_demerit_point"]),
					"quote_driver_nationality" => implode(",", $data["quote_driver_nationality"]),
					"quote_car_make" => $data["quote_car_make"],
					"quote_car_model" => $data["quote_car_model"],
					"quote_off_peak" => $data["quote_off_peak"],
					"quote_car_com_registered" => $data["quote_car_com_registered"],
					"quote_car_engine_no" => $data["quote_car_engine_no"],
					"quote_car_chassis_no" => $data["quote_car_chassis_no"],
					"quote_car_reg_no" => $data["quote_car_reg_no"],
					"quote_car_prev_reg_no" => $data["quote_car_prev_reg_no"],
					"quote_car_make_year" => $data["quote_car_make_year"],
					"quote_night_parking" => $data["quote_night_parking"],
					"quote_car_value" => $data["quote_car_value"],
					"quote_cover_type" => $data["quote_cover_type"],
					"quote_car_reg_year" => $data["quote_car_reg_year"],
					"quote_car_ncd" => $data["quote_car_ncd"],
					"quote_car_zero_ncd_reason" => $data["quote_car_zero_ncd_reason"],
					"quote_car_coe_expire_date" => $data["quote_car_coe_expire_date"],
					"quote_driver_merit_cert" => $data["quote_driver_merit_cert"],
					"quote_driver_address_block_no" => $data["quote_driver_address_block_no"],
					"quote_driver_address_street_name" => $data["quote_driver_address_street_name"],
					"quote_driver_address_unit_no" => $data["quote_driver_address_unit_no"],
					"quote_driver_address_building_name" => $data["quote_driver_address_building_name"],
					"quote_driver_postcode" => $data["quote_driver_postcode"],
					"quote_driver_loan_company" => $data["quote_driver_loan_company"],
					"quote_driver_current_insurer" => $data["quote_driver_current_insurer"],
					"quote_driver_country" => $data["quote_driver_country"]
					);
			
			$query = $db->getQuery(TRUE)
			->select("id")
			->from("#__insure_motor_driver_info")
			->where("quote_master_id = " . $quoteData->id);
			$db->setQuery($query);
			
			$driverId = $db->loadResult();
			
			$driverData->id = $driverId;
			
			$check = $db->updateObject("#__insure_motor_quote_master", $quoteData, "id");
			
			if($check) {
				$check = $db->updateObject("#__insure_motor_driver_info", $driverData, "id");
				
				return $check;
			}
			
			return false;
		}
	}
	
	protected function generateNewTitle($category_id, $alias, $name) {
		// Alter the title & alias
		$table = $this->getTable();

		while ($table->load(array('alias' => $alias))) {
			if ($name == $table->title) {
				$name = JString::increment($name);
			}
			$alias = JString::increment($alias, 'dash');
		}

		return array($name, $alias);
	}

	public function getPdfData($quotation_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)->select("*")
				->from("#__insure_motor_quotations")
				->where("id=" . $quotation_id);
		$db->setQuery($query);
		$result = $db->loadObject();

		$query = $db->getQuery(TRUE)->select("*")
				->from("#__insure_motor_quotation_existing_insurances")
				->where("quotation_id=" . $quotation_id);
		$db->setQuery($query);
		$existing_insurances = $db->loadObjectList();

		$query = $db->getQuery(TRUE)->select("*")
				->from("#__insure_motor_quotation_properties")
				->where("quotation_id=" . $quotation_id)
				->order("id");
		$db->setQuery($query);
		$properties = $db->loadObjectList();

		$query = $db->getQuery(TRUE)->select("*")
				->from("#__insure_motor_quotation_cars")
				->where("quotation_id=" . $quotation_id);
		$db->setQuery($query, 0, 3);
		$cars = $db->loadObjectList();

		// Prepare data before return
		$result->hasLargeSum = $this->checkSumInsuredQuotationTotal($quotation_id) >= 5000000;
		$result->contact_firstname = strtoupper($result->contact_firstname);
		$result->contact_lastname = strtoupper($result->contact_lastname);
		$result->country_residence = InsureDIYHelper::getCountryOfResidence($result->country_residence);
		$result->nationality = InsureDIYHelper::getNationality($result->nationality);

		$result->monthly_income_ckbox = $result->monthly_income;
		$result->monthly_income = JText::_($result->monthly_income);

		switch ($result->contact_identity_type) {
			case 'chid' :
				$result->contact_identity_no2 = $result->contact_identity_no;
				unset($result->contact_identity_no);

				$tmp = explode('-', $result->contact_expiry_date);
				$result->contact_expiry_date_y = $tmp[0];
				$result->contact_expiry_date_m = $tmp[1];
				$result->contact_expiry_date_d = $tmp[2];
				break;
			case 'passport':
				$result->contact_identity_no3 = $result->contact_identity_no;
				unset($result->contact_identity_no);

				$tmp = explode('-', $result->contact_expiry_date);
				$result->contact_expiry_date_y2 = $tmp[0];
				$result->contact_expiry_date_m2 = $tmp[1];
				$result->contact_expiry_date_d2 = $tmp[2];

				$result->bank_acct_holder_type = "p";
				break;
			case "hkid":
				$result->bank_acct_holder_type = "i";
			default:
				break;
		} $dob = explode("-", $result->dob);
		$result->dob_y = $dob[0];
		$result->dob_m = $dob[1];
		$result->dob_d = $dob[2];
		$result->age = (int) date("Y") - (int) $dob[0];
		$result->contact_telephone = (($result->contact_country_code) ? '(' . $result->contact_country_code . ') ' : '') . $result->contact_contact_no;

		$result->job_nature = JText::_($result->job_nature);
		$result->curreny = "hkd";
		$result->mode = "monthly";
		$result->source = "savings";
		$result->purpose = "protection";
		$result->cover_amt = "HK$" . $result->cover_amt;

		$result->contact_lang = "english";

		$result->height_unit = "cm";
		$result->weight_unit = "kg";
		$result->has_used_drugs = "1";

		$result->direct_promotion = "1";
		$result->full_name = $result->contact_firstname . " " . $result->contact_lastname;
		$result->relationship = "applicant";

		foreach ($existing_insurances as $k => $existing_insurance) {
			$key = $k + 1;
			$insurer_key = "ex_insurer_name_" . $key;
			$insured_key = "ex_insured_name_" . $key;
			$sum_key = "ex_sum_insured_" . $key;
			$date_key = "ex_issuance_date_" . $key;

			$result->$insurer_key = $existing_insurance->insurer_name;
			$result->$insured_key = $result->full_name;
			$result->$sum_key = $existing_insurance->sum_insured;
			$result->$date_key = $existing_insurance->issuance_date;
		}

		$year = (int) date("Y", strtotime($result->created_date));
		$result->lastyear = $year - 1;
		$result->last2year = $year - 2;
		$result->last3year = $year - 3;
		foreach ($properties as $k => $property) {
			$key = $k + 1;
			$property_purchase_key = "property_purchase_" . $key;
			$property_price_key = "property_price_" . $key;
			$property_mortgage_key = "property_mortgage_" . $key;
			$property_value_key = "property_value_" . $key;
			$result->$property_purchase_key = $property->property_purchase;
			$result->$property_price_key = $property->property_price;
			$result->$property_mortgage_key = $property->property_mortgage;
			$result->$property_value_key = $property->property_value;
		}

		$result->no_of_cars = count($cars);

		foreach ($cars as $k => $car) {
			$key = $k + 1;
			$model_key = "car_model_" . $key;
			$result->$model_key = $car->car_model;
		}

		$result->contact_country = ($result->contact_country == "HK") ? $result->contact_country : "OTH";

		$result->fna = "0"; // SUN only
		$result->owner_type = "0";

		return $result;
	}

	public function checkSumInsuredQuotationTotal($quotation_id) {
		$db = JFactory::getDbo();
		/* 1. Sum Insured from Plans - see Step 3 page */
		$query = " SELECT sum_insured FROM #__insure_motor_quotation_plan_xref WHERE quotation_id = '$quotation_id' ";
		$db->setQuery($query);
		$rows = $db->loadObjectList();

		$total_sum_insured = 0;
		foreach ($rows as $r):
			$total_sum_insured += (int) $r->sum_insured;
		endforeach;

		/* 2. Sum Insured from Existing Plans  - see Step 4 page */
		$query = " SELECT sum_insured FROM #__insure_motor_quotation_existing_insurances WHERE quotation_id = '$quotation_id' ";
		$db->setQuery($query);
		$rows = $db->loadObjectList();

		foreach ($rows as $r):
			$total_sum_insured += intval(str_replace(",", "", $r->sum_insured));
		endforeach;

		return $total_sum_insured;
	}

	public function getPlans($quotation_id) {
		$db = JFactory::getDBO();
		$query = " SELECT * FROM #__insure_motor_quotation_plan_xref WHERE quotation_id = '$quotation_id' ";
		$db->setQuery($query);
		$rows = $db->loadObjectList();

		return $rows;
	}

	public function savePDFPaths($quotation_id, $array = array(), $client = TRUE) {
		if (empty($array)) {
			return;
		}

		$toJson = new stdClass();
		foreach ($array as $k => $v) {
			if (is_array($v)) {
				$toJson->$k = implode(" | ", $v);
			} else {
				$toJson->$k = $v;
			}
		}

		$db = JFactory::getDBO();
		if ($client) {
			$query = " UPDATE #__insure_motor_quotations SET `file_json_pdfpath` = " . $db->Quote(json_encode($toJson), false) . " WHERE id = '$quotation_id' ";
		} else {
			$query = " UPDATE #__insure_motor_quotations SET `file_json_pdfpath2` = " . $db->Quote(json_encode($toJson), false) . " WHERE id = '$quotation_id' ";
		}
		$db->setQuery($query);
		$db->query();
	}

}
