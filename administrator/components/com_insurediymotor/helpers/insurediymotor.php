<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Transactions helper.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 * @since       1.6
 */
class InsureDIYMotorHelper {

	/**
	 * Configure the Linkbar.
	 *
	 * @param   string	The name of the active view.
	 * @since   1.6
	 */
	public static function addSubmenu($vName = 'quotations') {
		JHtmlSidebar::addEntry(
				JText::_('COM_INSUREDIYMOTOR_SUBMENU_QUOTATIONS'), 'index.php?option=com_insurediymotor&view=quotations', $vName == 'quotations'
		);

// 		JHtmlSidebar::addEntry(
// 				JText::_('COM_INSUREDIYMOTOR_SUBMENU_PLANS'), 'index.php?option=com_insurediymotor&view=plans', $vName == 'plans'
// 		);

// 		JHtmlSidebar::addEntry(
// 				JText::_('Zones'), 'index.php?option=com_insurediymotor&view=zones', $vName == 'zones'
// 		);
		
		JHtmlSidebar::addEntry(
				JText::_('COM_INSUREDIYMOTOR_SUBMENU_CSV_EXPORT'), 'index.php?option=com_insurediymotor&view=csvexport', $vName == 'csvexport'
		);
		

// 		JHtmlSidebar::addEntry(
// 				JText::_('COM_INSUREDIYMOTOR_SUBMENU_CSV_IMPORT'), 'index.php?option=com_insurediymotor&view=csvimport', $vName == 'csvimport'
// 		);
	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @param   integer  The category ID.
	 * @return  JObject
	 * @since   1.6
	 */
	public static function getActions($categoryId = 0) {
		$user = JFactory::getUser();
		$result = new JObject;

		if (empty($categoryId)) {
			$assetName = 'com_insurediy';
			$level = 'component';
		} else {
			$assetName = 'com_insurediy.category.' . (int) $categoryId;
			$level = 'category';
		}

		$actions = JAccess::getActions('com_insurediymotor', $level);

		foreach ($actions as $action) {
			$result->set($action->name, $user->authorise($action->name, $assetName));
		}

		return $result;
	}

	public static function getYesNoOptions() {
		$options = array();

		array_unshift($options, JHtml::_('select.option', '0', JText::_('JNO')));
		array_unshift($options, JHtml::_('select.option', '1', JText::_('JYES')));


		return $options;
	}

	public static function getInsuranceCompanyOptions() {
		$db = JFactory::getDBO();

		$options = array();

		$query = " SELECT * FROM #__insure_companies ";
		$db->setQuery($query);
		$rows = $db->loadObjectList();

		foreach ($rows as $r) :
			$options[] = JHtml::_('select.option', $r->insurer_code, $r->company_name);
		endforeach;

		return $options;
	}

	public static function getPolicyNumberStateOptions() {
		$options = array();
		$options[] = JHtml::_('select.option', 0, "COM_INSUREDIYMOTOR_POLICY_NUMBER_STATE_USED");
		$options[] = JHtml::_('select.option', 1, "COM_INSUREDIYMOTOR_POLICY_NUMBER_STATE_AVAILABLE");

		return $options;
	}

	public static function getTripTypeOptions() {
		$options = array();
		$options[] = JHtml::_('select.option', "ST", "Single Trip");
		$options[] = JHtml::_('select.option', "AT", "Annual Trip");

		return $options;
	}

	public static function getGroupTypeOptions() {
		$options = array();
		$options[] = JHtml::_('select.option', "JM", "Just Me");
		$options[] = JHtml::_('select.option', "WG", "With Group");
		$options[] = JHtml::_('select.option', "WF", "With Family");

		return $options;
	}

	public static function getSumInsuredOptions() {

		$arrays = array('1000000', '1800000', '2000000', '3000000', '4000000', '5000000', '6000000',
			'7000000', '9500000', '12000000', '14500000', '17000000', '19500000', '22000000',
			'24500000', '27000000');

		$options = array();

		foreach ($arrays as $a) :

			$text_number = (string) number_format($a);
			$options[] = JHtml::_('select.option', $a, $text_number);

		endforeach;

		return $options;
	}

}
