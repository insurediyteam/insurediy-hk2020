CREATE TABLE IF NOT EXISTS `#__insure_motor_occupation` (
  `id` INTEGER NOT NULL auto_increment,
  `occ_code` VARCHAR(255) NOT NULL DEFAULT '',
  `occ_name` VARCHAR(255) NOT NULL DEFAULT '',
  `occ_hla_code` VARCHAR(255) NOT NULL DEFAULT '',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__insure_motor_make_master` (
  `id` INTEGER NOT NULL auto_increment,
  `make_code` VARCHAR(255) NOT NULL DEFAULT '',
  `make_name` VARCHAR(255) NOT NULL DEFAULT '',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__insure_motor_model_master` (
  `id` INTEGER NOT NULL auto_increment,
  `make_code` VARCHAR(255) NOT NULL DEFAULT '',
  `model_code` VARCHAR(255) NOT NULL DEFAULT '',
  `model_name` VARCHAR(255) NOT NULL DEFAULT '',
  `model_cc` INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__insure_motor_loan_company` (
  `mortgagee_code` VARCHAR(255) NOT NULL DEFAULT '',
  `mortgagee_name` VARCHAR(255) NOT NULL DEFAULT '',
  PRIMARY KEY  (`mortgagee_code`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__insure_motor_current_insurer` (
  `insurer_code` VARCHAR(255) NOT NULL DEFAULT '',
  `insurer_name` VARCHAR(255) NOT NULL DEFAULT '',
  `insurer_sequence` INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY  (`insurer_code`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__insure_motor_renewal_partner` (
  `renewal_partner_code` VARCHAR(255) NOT NULL DEFAULT '',
  `renewal_partner_name` VARCHAR(255) NOT NULL DEFAULT '',
  `renewal_partner_name_short` VARCHAR(255) NOT NULL DEFAULT '',
  PRIMARY KEY  (`renewal_partner_code`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__insure_motor_make_model_sompo` (
  `id` INTEGER NOT NULL auto_increment,
  `model_code` VARCHAR(255) NOT NULL DEFAULT '',
  `model_name` VARCHAR(255) NOT NULL DEFAULT '',
  `model_master_code` VARCHAR(255) NOT NULL DEFAULT '',
  `model_cc` INTEGER NOT NULL DEFAULT 0,
  `model_bodytype` VARCHAR(255) NOT NULL DEFAULT '',
  `model_body_desc` VARCHAR(255) NOT NULL DEFAULT '',
  `model_no_passenger` VARCHAR(255) NOT NULL DEFAULT '',
  `model_make_code` VARCHAR(255) NOT NULL DEFAULT '',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__insure_motor_make_model_msig` (
  `id` INTEGER NOT NULL auto_increment,
  `model_code` VARCHAR(255) NOT NULL DEFAULT '',
  `model_name` VARCHAR(255) NOT NULL DEFAULT '',
  `model_master_code` VARCHAR(255) NOT NULL DEFAULT '',
  `model_cc` INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__insure_motor_make_model_hla` (
  `id` INTEGER NOT NULL auto_increment,
  `make_code` VARCHAR(255) NOT NULL DEFAULT '',
  `model_code` VARCHAR(255) NOT NULL DEFAULT '',
  `model_name` VARCHAR(255) NOT NULL DEFAULT '',
  `model_master_code` VARCHAR(255) NOT NULL DEFAULT '',
  `model_cc` INTEGER NOT NULL DEFAULT 0,
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__insure_motor_quote_master` (
  `id` INTEGER NOT NULL auto_increment,
  `userId` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_pre_quote_change_flag` VARCHAR(255) NOT NULL DEFAULT 'true',
  `quote_plan_workshop` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_plan_los` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_plan_ncdpro` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_policy_start_date` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_policy_selected_plan_code` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_policy_selected_partner_code` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_policy_code` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_policy_previous_code` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_created_time` DATETIME DEFAULT NULL,
  `quote_updated_time` DATETIME DEFAULT NULL,
  `quote_deleted_time` DATETIME DEFAULT NULL,
  `quote_process_stage` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_payment_stage` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_payment_txn_id` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_payment_request_id` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_payment_amount` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_promo_code` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_cs_required` VARCHAR(255) NOT NULL DEFAULT 'false', 
  `quote_user_privacy_agreed` VARCHAR(255) NOT NULL DEFAULT 'false', 
  `quote_user_marketing_agreed` VARCHAR(255) NOT NULL DEFAULT 'true',
  `quote_user_declaration_agreed` VARCHAR(255) NOT NULL DEFAULT 'false',
  `quote_user_terms_agreed` VARCHAR(255) NOT NULL DEFAULT 'false',
  `quote_selected_model_variant` VARCHAR(255) NOT NULL DEFAULT 'false',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__insure_motor_driver_info` (
  `id` INTEGER NOT NULL auto_increment,
  `quote_master_id` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_car_make` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_car_model` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_off_peak` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_car_com_registered` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_car_engine_no` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_car_chassis_no` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_car_reg_no` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_car_prev_reg_no` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_car_make_year` INTEGER NOT NULL DEFAULT 2007,
  `quote_car_reg_year` INTEGER NOT NULL DEFAULT 2007,
  `quote_car_ncd` INTEGER NOT NULL DEFAULT 0,
  `quote_car_zero_ncd_reason` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_car_claim_no` INTEGER NOT NULL DEFAULT 0,
  `quote_car_claim_amount` INTEGER NOT NULL DEFAULT 0,
  `quote_car_coe_expire_date` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_merit_cert` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_gender` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_first_name` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_last_name` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_email` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_nric` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_nric_type` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_contact` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_dob` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_exp` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_occ_type` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_occ` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_nationality` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_race` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_marital_status` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_main_driver_relation` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_demerit_point` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_is_appliant` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_claim_no` INTEGER NOT NULL DEFAULT 0,
  `quote_driver_claim_amount` INTEGER NOT NULL DEFAULT 0,
  `quote_driver_valid_license` VARCHAR(255) NOT NULL DEFAULT 'Y',
  `quote_driver_address_line_1` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_address_line_2` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_address_line_3` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_address_street_name` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_address_block_no` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_address_unit_no` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_address_building_name` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_to_west_my` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_country` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_postcode` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_loan` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_loan_company` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_driver_current_insurer` VARCHAR(255) NOT NULL DEFAULT '',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__insure_motor_quote_sompo` (
  `id` INTEGER NOT NULL auto_increment,
  `quote_master_id` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_car_variant` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_req_unique_id` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_app_quote_ref_id` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_pol_agent_ref_id` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_pol_ack_id` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_plan_id` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_plan_name` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_plan_premium_before_tax` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_plan_premium_tax_amt` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_plan_premium_tax_percent` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_plan_premium_total` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_plan_order` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_plan_category` VARCHAR(255) NOT NULL DEFAULT '',
  `sompo_plan_risk` VARCHAR(2000) NOT NULL DEFAULT '',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__insure_motor_policy_error` (
  `id` INTEGER NOT NULL auto_increment,
  `quote_master_id` VARCHAR(255) NOT NULL DEFAULT '',
  `policy_request_data` VARCHAR(2000) NOT NULL DEFAULT '',
  `policy_error_msg` VARCHAR(1000) NOT NULL DEFAULT '',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__insure_motor_quote_msig` (
  `id` INTEGER NOT NULL auto_increment,
  `quote_master_id` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_car_variant` VARCHAR(255) NOT NULL DEFAULT '',
  `msig_app_ref_id` VARCHAR(255) NOT NULL DEFAULT '',
  `msig_ref_id` VARCHAR(255) NOT NULL DEFAULT '',
  `msig_quote_valid_through` VARCHAR(255) NOT NULL DEFAULT '',
  `msig_plan_id` VARCHAR(255) NOT NULL DEFAULT '',
  `msig_plan_name` VARCHAR(255) NOT NULL DEFAULT '',
  `msig_plan_description` VARCHAR(255) NOT NULL DEFAULT '',
  `msig_plan_premium_tax_amt` VARCHAR(255) NOT NULL DEFAULT '',
  `msig_plan_premium_tax_percent` VARCHAR(255) NOT NULL DEFAULT '',
  `msig_plan_premium_total` VARCHAR(255) NOT NULL DEFAULT '',
  `msig_plan_premium_before_disc` VARCHAR(255) NOT NULL DEFAULT '',
  `msig_plan_premium_disc_type` VARCHAR(255) NOT NULL DEFAULT '',
  `msig_plan_premium_disc_amt` VARCHAR(255) NOT NULL DEFAULT '',
  `msig_plan_excess_standard` VARCHAR(255) NOT NULL DEFAULT '',
  `msig_plan_excess_windscreen` VARCHAR(255) NOT NULL DEFAULT '',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__insure_motor_quote_hla` (
  `id` INTEGER NOT NULL auto_increment,
  `quote_master_id` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_car_variant` VARCHAR(255) NOT NULL DEFAULT '',
  `hla_plan_id` VARCHAR(255) NOT NULL DEFAULT '',
  `hla_plan_name` VARCHAR(255) NOT NULL DEFAULT '',
  `hla_plan_premium_total` VARCHAR(255) NOT NULL DEFAULT '',
  `hla_plan_excess_standard` VARCHAR(255) NOT NULL DEFAULT '',
  `hla_plan_excess_young` VARCHAR(255) NOT NULL DEFAULT '',
  `hla_plan_excess_windscreen` VARCHAR(255) NOT NULL DEFAULT '',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__insure_motor_quote_renewal` (
  `id` INTEGER NOT NULL auto_increment,
  `quote_master_id` VARCHAR(255) NOT NULL DEFAULT '',
  `quote_car_variant` VARCHAR(255) NOT NULL DEFAULT '',
  `renewal_plan_id` VARCHAR(255) NOT NULL DEFAULT '',
  `renewal_plan_name` VARCHAR(255) NOT NULL DEFAULT '',
  `renewal_plan_premium_total` VARCHAR(255) NOT NULL DEFAULT '',
  `renewal_plan_excess_standard` VARCHAR(255) NOT NULL DEFAULT '',
  `renewal_plan_excess_young` VARCHAR(255) NOT NULL DEFAULT '',
  `renewal_plan_status` VARCHAR(255) NOT NULL DEFAULT '',
  `renewal_plan_partner` VARCHAR(255) NOT NULL DEFAULT '',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__insure_motor_partner_setting` (
  `id` INTEGER NOT NULL auto_increment,
  `partner_id` VARCHAR(255) NOT NULL DEFAULT '',
  `partner_trip_type` VARCHAR(255) NOT NULL DEFAULT '',
  `partner_plan_discount` INTEGER NOT NULL DEFAULT 0,
  `partner_plan_rewards_point` INTEGER NOT NULL DEFAULT 0,
  `partner_plan_damage_coverage` VARCHAR(1000) NOT NULL DEFAULT '',
  `partner_plan_windscreen_cover` VARCHAR(1000) NOT NULL DEFAULT '',
  `partner_plan_windscreen_excess` VARCHAR(1000) NOT NULL DEFAULT '',
  `partner_plan_roadside_assist` VARCHAR(1000) NOT NULL DEFAULT '',
  `partner_plan_evacuation_oversea` VARCHAR(1000) NOT NULL DEFAULT '',
  `partner_plan_personal_accident_policyholder` VARCHAR(1000) NOT NULL DEFAULT '',
  `partner_plan_personal_accident_authorized` VARCHAR(1000) NOT NULL DEFAULT '',
  `partner_plan_medic_expense_policyholder` VARCHAR(1000) NOT NULL DEFAULT '',
  `partner_plan_medic_expense_authorized` VARCHAR(1000) NOT NULL DEFAULT '',
  `partner_plan_third_party_liability_death` VARCHAR(1000) NOT NULL DEFAULT '',
  `partner_plan_third_party_liability_property` VARCHAR(1000) NOT NULL DEFAULT '',
  `partner_plan_third_party_liability_legal` VARCHAR(1000) NOT NULL DEFAULT '',
  `partner_plan_other_benefit` VARCHAR(1000) NOT NULL DEFAULT '',
  `partner_plan_ncd_protector` VARCHAR(1000) NOT NULL DEFAULT '',
  `partner_plan_loss_of_use_benefit` VARCHAR(1000) NOT NULL DEFAULT '',
  `partner_plan_workshop` VARCHAR(500) NOT NULL DEFAULT '',
  `partner_plan_geographical_area` VARCHAR(1000) NOT NULL DEFAULT '',
  PRIMARY KEY  (`id`)
) DEFAULT CHARSET=utf8;

ALTER TABLE `#__insure_motor_make_master` AUTO_INCREMENT=1000000;
ALTER TABLE `#__insure_motor_model_master` AUTO_INCREMENT=1000000;
ALTER TABLE `#__insure_motor_make_model_sompo` AUTO_INCREMENT=1000000;
ALTER TABLE `#__insure_motor_make_model_msig` AUTO_INCREMENT=1000000;
ALTER TABLE `#__insure_motor_quote_master` AUTO_INCREMENT=5000000;
ALTER TABLE `#__insure_motor_driver_info` AUTO_INCREMENT=1000000;
ALTER TABLE `#__insure_motor_quote_sompo` AUTO_INCREMENT=1000000;
ALTER TABLE `#__insure_motor_quote_msig` AUTO_INCREMENT=1000000;

INSERT INTO `#__insure_motor_renewal_partner` (`renewal_partner_code`, `renewal_partner_name`, `renewal_partner_name_short`) VALUES ('NCD000018', 'NTUC INCOME', 'ntuc');
INSERT INTO `#__insure_motor_renewal_partner` (`renewal_partner_code`, `renewal_partner_name`, `renewal_partner_name_short`) VALUES ('NCD000004', 'AXA INSURANCE', 'axa');

INSERT INTO `#__insure_motor_partner_setting` (`id`, `partner_id`, `partner_trip_type`, `partner_plan_discount`, `partner_plan_rewards_point`, `partner_plan_damage_coverage`, `partner_plan_windscreen_cover`, `partner_plan_windscreen_excess`, `partner_plan_roadside_assist`, `partner_plan_evacuation_oversea`, `partner_plan_personal_accident_policyholder`, `partner_plan_personal_accident_authorized`, `partner_plan_medic_expense_policyholder`, `partner_plan_medic_expense_authorized`, `partner_plan_third_party_liability_death`, `partner_plan_third_party_liability_property`, `partner_plan_third_party_liability_legal`, `partner_plan_other_benefit`, `partner_plan_ncd_protector`, `partner_plan_loss_of_use_benefit`, `partner_plan_workshop`, `partner_plan_geographical_area`) VALUES ('1', 'sompo', 'comprehensive', '0', '0', 'Market Value', 'Windscreen Replacement: unlimited replacement with applicable excess.<br>Windscreen Repair: applicable excess is waived, provided repair is done at appointed workshop.', '$107 (including GST) each claim.', '24/7 Emergency Mobile Accident Response Service for advice, photo taking, accident statement taking, e-filling of claims, workshop referral and towing arrangement.', 'Not covered', 'Up to $20,000', 'Up to $10,000', 'Up to $300', 'Up to $300', 'Unlimited', 'Up to $5m', 'Up to $3,000', 'Waiver of Standard Excess up to $1,000 if accident repair is done at ExcelDrive Workshops for the first claim per policy year - applicable to ExcelDrive Prestige and Gold Plans only;<br>For ExcelDrive Focus Plan, Standard Excess is halved up to max. of $6', 'Included at no charge for NCD 30% and above', '$100 per day of approved repair period, up to 10 days (applicable to ExcelDrive Prestige and Gold Plans only)', 'Any workshop for ExcelDrive Prestige Plan;<br>Sompo ExcelDrive Workshops for ExcelDrive Gold and Focus Plans.', 'Includes: (1) Singapore, (2) West Malaysia and (3) that part of Thailand within 80.5km of the border between Thailand and West Malaysia.');
INSERT INTO `#__insure_motor_partner_setting` (`id`, `partner_id`, `partner_trip_type`, `partner_plan_discount`, `partner_plan_rewards_point`, `partner_plan_damage_coverage`, `partner_plan_windscreen_cover`, `partner_plan_windscreen_excess`, `partner_plan_roadside_assist`, `partner_plan_evacuation_oversea`, `partner_plan_personal_accident_policyholder`, `partner_plan_personal_accident_authorized`, `partner_plan_medic_expense_policyholder`, `partner_plan_medic_expense_authorized`, `partner_plan_third_party_liability_death`, `partner_plan_third_party_liability_property`, `partner_plan_third_party_liability_legal`, `partner_plan_other_benefit`, `partner_plan_ncd_protector`, `partner_plan_loss_of_use_benefit`, `partner_plan_workshop`, `partner_plan_geographical_area`) VALUES ('2', 'msig', 'comprehensive', '0', '0', 'Market Value', 'Auto reinstatement of Windscreen Cover at no cost', '$100 each claim', '24/7 Automobile and Medical Assistance Services for towing, roadside assistance, vehicle repatriation, accident & police report notification, locksmith referral, etc.', 'Up to $50,000 per accident and in aggregate for each Period of Insurance, Emergency Medical Evacuation and Repatriation of the Policyholder for accidents within the Geographical area (excl. Singapore) in connection with the Insured Vehicle.', 'Up to $100,000 (MotorMax Plus); Up to $20,000 (MotorMax)', 'Up to $50,000 Each (MotorMax Plus); Up tp $10,000 Each (MotorMax)', 'Up to $1,000', 'Up to $1,000 Each', 'Unlimited', 'Up to $5m', 'Up to $3,000', 'Car insured under the MotorMax Plus Plan enjoys New for Old Replacement of your car (less than 12 months old) in the event of an accident,<br>and up to $100,000 of the outstanding loan of your car in the event of accidental death of the car owner. (T&Cs a', 'Optional at additional premium<br>This is included in the above quote if you have selected to add NCD Protector Benefit ', '$50 per day, up to 10 days (applicable to MotorMax Plus Plan only)', 'Any workshop for MotorMax Plus Plan;<br>MSIG�s Authorised Workshops for MotorMax Plan.', 'Includes: (1) Singapore, (2) West Malaysia and (3) that part of Thailand within 80 kilometres of the border between Thailand and West Malaysia.');
INSERT INTO `#__insure_motor_partner_setting` (`id`, `partner_id`, `partner_trip_type`, `partner_plan_discount`, `partner_plan_rewards_point`, `partner_plan_damage_coverage`, `partner_plan_windscreen_cover`, `partner_plan_windscreen_excess`, `partner_plan_roadside_assist`, `partner_plan_evacuation_oversea`, `partner_plan_personal_accident_policyholder`, `partner_plan_personal_accident_authorized`, `partner_plan_medic_expense_policyholder`, `partner_plan_medic_expense_authorized`, `partner_plan_third_party_liability_death`, `partner_plan_third_party_liability_property`, `partner_plan_third_party_liability_legal`, `partner_plan_other_benefit`, `partner_plan_ncd_protector`, `partner_plan_loss_of_use_benefit`, `partner_plan_workshop`, `partner_plan_geographical_area`) VALUES ('3', 'hla', 'comprehensive', '0', '0', 'Market Value', 'Windscreen Replacement: unlimited replacement', 'Windscreen Excess of $100 unless windscreen is repaired at HL Assurance Appointed Windscreen Repairer.', '24/7 Automobile and Medical Assistance Services for towing, roadside repair, claims procedure / reporting, workshop referral, service centre referral in Malaysia, car replacement / rental arrangement, hotel accomodation arrangement and emergency message t', 'Up to $50,000  per accident and in the aggregate for each Period of Insurance, Emergency Medical Evacuation / Repatriation of Policyholder / Authorised Driver and/or passenger(s) for accidents within the Geographical area (excl. Singapore) in connection w', 'Up to $20,000', 'Up to $10,000', 'Up to $1,000', 'Up to $1,000 Each', 'Unlimited', 'Up to $5m', 'Up to $3,000', 'For accident repairs at any HL Assurance Approved workshops, Own Damage Excess is halved and be assured that genuine manufacturers� parts will be used that comes with a full 9-month repair warranty.', 'Optional at additional premium<br>This is included in the above quote if you have selected to add NCD Protector Benefit ', 'Daily transport allowance of $50 (up to ten days if repairs exceed three days and subject to max of $1,000 per policy year)', 'Freedom of choice of any workshop or HL Assurance Approved Workshops', 'Includes: (1) Singapore, (2) West Malaysia, (3) Transit by direct sea route across the straits between Penang and the mainland of West Malaysia, Direct sea route across the straits between Changi Point, Singapore and Tanjong Belungkor, Johor and (4) Part ');

INSERT INTO `#__insure_motor_loan_company` (`mortgagee_code`, `mortgagee_name`) VALUES ('01050414', 'Sing Investments & Finance Ltd');
INSERT INTO `#__insure_motor_loan_company` (`mortgagee_code`, `mortgagee_name`) VALUES ('03008986', 'Singapura Finance Ltd');
INSERT INTO `#__insure_motor_loan_company` (`mortgagee_code`, `mortgagee_name`) VALUES ('03010586', 'DBS Bank Ltd');
INSERT INTO `#__insure_motor_loan_company` (`mortgagee_code`, `mortgagee_name`) VALUES ('03174351', 'Standard Chartered Bank');
INSERT INTO `#__insure_motor_loan_company` (`mortgagee_code`, `mortgagee_name`) VALUES ('03246177', 'Oversea-Chinese Banking Corporation Ltd');
INSERT INTO `#__insure_motor_loan_company` (`mortgagee_code`, `mortgagee_name`) VALUES ('10000777', 'Hong Leong Finance Limited');
INSERT INTO `#__insure_motor_loan_company` (`mortgagee_code`, `mortgagee_name`) VALUES ('10000787', 'The Hongkong and Shanghai Banking Corporation Ltd');
INSERT INTO `#__insure_motor_loan_company` (`mortgagee_code`, `mortgagee_name`) VALUES ('10000867', 'Mayban Finance (S) Ltd');
INSERT INTO `#__insure_motor_loan_company` (`mortgagee_code`, `mortgagee_name`) VALUES ('10000869', 'Orix Leasing Singapore Limited');
INSERT INTO `#__insure_motor_loan_company` (`mortgagee_code`, `mortgagee_name`) VALUES ('10000922', 'United Overseas Bank Limited');
INSERT INTO `#__insure_motor_loan_company` (`mortgagee_code`, `mortgagee_name`) VALUES ('10001152', 'Tan Chong Credit Pte Ltd');
INSERT INTO `#__insure_motor_loan_company` (`mortgagee_code`, `mortgagee_name`) VALUES ('10002143', 'Malayan Banking Berhad');
INSERT INTO `#__insure_motor_loan_company` (`mortgagee_code`, `mortgagee_name`) VALUES ('10030000', 'United Overseas Finance Ltd');
INSERT INTO `#__insure_motor_loan_company` (`mortgagee_code`, `mortgagee_name`) VALUES ('10036250', 'RHB Bank Berhad');
INSERT INTO `#__insure_motor_loan_company` (`mortgagee_code`, `mortgagee_name`) VALUES ('10038028', 'ABN AMRO Bank N.V.');
INSERT INTO `#__insure_motor_loan_company` (`mortgagee_code`, `mortgagee_name`) VALUES ('10071450', 'Volkswagen Financial Services S\'pore Ltd');
INSERT INTO `#__insure_motor_loan_company` (`mortgagee_code`, `mortgagee_name`) VALUES ('10245097', 'Citibank Singapore Ltd');
INSERT INTO `#__insure_motor_loan_company` (`mortgagee_code`, `mortgagee_name`) VALUES ('O', 'Others');

INSERT INTO `#__insure_motor_current_insurer` (`insurer_code`, `insurer_name`, `insurer_sequence`) VALUES ('NCD000001', 'ACE INSURANCE LTD', '1');
INSERT INTO `#__insure_motor_current_insurer` (`insurer_code`, `insurer_name`, `insurer_sequence`) VALUES ('NCD000005', 'AIG ASIA PACIFIC INSURANCE PTE. LTD.', '2');
INSERT INTO `#__insure_motor_current_insurer` (`insurer_code`, `insurer_name`, `insurer_sequence`) VALUES ('NCD000002', 'ALLIANZ INSURANCE (SINGAPORE) PTE LTD', '3');
INSERT INTO `#__insure_motor_current_insurer` (`insurer_code`, `insurer_name`, `insurer_sequence`) VALUES ('NCD000003', 'AVIVA LTD', '4');
INSERT INTO `#__insure_motor_current_insurer` (`insurer_code`, `insurer_name`, `insurer_sequence`) VALUES ('NCD000004', 'AXA INSURANCE SINGAPORE PTE LTD', '5');
INSERT INTO `#__insure_motor_current_insurer` (`insurer_code`, `insurer_name`, `insurer_sequence`) VALUES ('NCD000025', 'CHARTIS SINGAPORE INSURANCE PTE LTD', '6');
INSERT INTO `#__insure_motor_current_insurer` (`insurer_code`, `insurer_name`, `insurer_sequence`) VALUES ('NCD000006', 'CHINA TAIPING INSURANCE (S) PTE LTD', '7');
INSERT INTO `#__insure_motor_current_insurer` (`insurer_code`, `insurer_name`, `insurer_sequence`) VALUES ('NCD000007', 'DIRECT ASIA INSURANCE (S) PTE LTD', '8');
INSERT INTO `#__insure_motor_current_insurer` (`insurer_code`, `insurer_name`, `insurer_sequence`) VALUES ('NCD000008', 'EQ INSURANCE COMPAN LIMITED', '9');
INSERT INTO `#__insure_motor_current_insurer` (`insurer_code`, `insurer_name`, `insurer_sequence`) VALUES ('NCD000031', 'ERGO INSURANCE PTE. LTD.', '10');
INSERT INTO `#__insure_motor_current_insurer` (`insurer_code`, `insurer_name`, `insurer_sequence`) VALUES ('NCD000009', 'ETIQA INSURANCE BERHAD (Maban)', '11');
INSERT INTO `#__insure_motor_current_insurer` (`insurer_code`, `insurer_name`, `insurer_sequence`) VALUES ('NCD000010', 'FIRST CAPITAL INSURANCE LTD', '12');
INSERT INTO `#__insure_motor_current_insurer` (`insurer_code`, `insurer_name`, `insurer_sequence`) VALUES ('NCD000030', 'HL ASSURANCE PTE LTD', '13');
INSERT INTO `#__insure_motor_current_insurer` (`insurer_code`, `insurer_name`, `insurer_sequence`) VALUES ('NCD000011', 'HSBC INSURANCE LTD', '14');
INSERT INTO `#__insure_motor_current_insurer` (`insurer_code`, `insurer_name`, `insurer_sequence`) VALUES ('NCD000012', 'INDIA INTERNATIONAL INSURANCE PTE LTD', '15');
INSERT INTO `#__insure_motor_current_insurer` (`insurer_code`, `insurer_name`, `insurer_sequence`) VALUES ('NCD000013', 'LIBERT INSURANCE PTE LTD', '16');
INSERT INTO `#__insure_motor_current_insurer` (`insurer_code`, `insurer_name`, `insurer_sequence`) VALUES ('NCD000014', 'LONPAC INSURANCE BHD', '17');
INSERT INTO `#__insure_motor_current_insurer` (`insurer_code`, `insurer_name`, `insurer_sequence`) VALUES ('NCD000015', 'MITSUI SUMITOMO INSURANCE (S) PTE LTD', '18');
INSERT INTO `#__insure_motor_current_insurer` (`insurer_code`, `insurer_name`, `insurer_sequence`) VALUES ('NCD000016', 'MSIG INSURANCE (SINGAPORE) PTE', '19');
INSERT INTO `#__insure_motor_current_insurer` (`insurer_code`, `insurer_name`, `insurer_sequence`) VALUES ('NCD000024', 'NIPPONKOA INSURANCE COMPAN LIMITED', '20');
INSERT INTO `#__insure_motor_current_insurer` (`insurer_code`, `insurer_name`, `insurer_sequence`) VALUES ('NCD000018', 'NTUC INCOME INSURANCE CO-OPERA', '21');
INSERT INTO `#__insure_motor_current_insurer` (`insurer_code`, `insurer_name`, `insurer_sequence`) VALUES ('NCD000019', 'OVERSEAS ASSURANCE CORPN LTD', '22');
INSERT INTO `#__insure_motor_current_insurer` (`insurer_code`, `insurer_name`, `insurer_sequence`) VALUES ('NCD000020', 'QBE INSURANCE (INTERNATIONAL)', '23');
INSERT INTO `#__insure_motor_current_insurer` (`insurer_code`, `insurer_name`, `insurer_sequence`) VALUES ('NCD000021', 'ROAL & SUN ALLIANCE INSURANCE', '24');
INSERT INTO `#__insure_motor_current_insurer` (`insurer_code`, `insurer_name`, `insurer_sequence`) VALUES ('NCD000023', 'SHC CAPITAL LIMITED', '25');
INSERT INTO `#__insure_motor_current_insurer` (`insurer_code`, `insurer_name`, `insurer_sequence`) VALUES ('NCD000022', 'SINGAPORE AVIATION & GEN.INS.CO (PTE)LTD', '26');
INSERT INTO `#__insure_motor_current_insurer` (`insurer_code`, `insurer_name`, `insurer_sequence`) VALUES ('NCD000017', 'SOMPO INSURANCE SINGAPORE PTE. LTD.', '27');
INSERT INTO `#__insure_motor_current_insurer` (`insurer_code`, `insurer_name`, `insurer_sequence`) VALUES ('NCD000026', 'TM ASIA INSURANCE SINGAPORE LTD', '28');
INSERT INTO `#__insure_motor_current_insurer` (`insurer_code`, `insurer_name`, `insurer_sequence`) VALUES ('NCD000027', 'TOKIO MARINE & FIRE INSCE CO (S) PTE LTD', '29');
INSERT INTO `#__insure_motor_current_insurer` (`insurer_code`, `insurer_name`, `insurer_sequence`) VALUES ('NCD000029', 'TOKIO MARINE INSURANCE SINGAPORE LTD', '30');
INSERT INTO `#__insure_motor_current_insurer` (`insurer_code`, `insurer_name`, `insurer_sequence`) VALUES ('NCD000028', 'UNITED OVERSEAS INSURANCE LTD', '31');



