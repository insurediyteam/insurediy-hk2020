DROP TABLE IF EXISTS `#__insure_motor_occupation`;

DROP TABLE IF EXISTS `#__insure_motor_make_master`;

DROP TABLE IF EXISTS `#__insure_motor_model_master`;

DROP TABLE IF EXISTS `#__insure_motor_loan_company`;

DROP TABLE IF EXISTS `#__insure_motor_current_insurer`;

DROP TABLE IF EXISTS `#__insure_motor_renewal_partner`;

DROP TABLE IF EXISTS `#__insure_motor_make_model_sompo`;

DROP TABLE IF EXISTS `#__insure_motor_make_model_msig`;

DROP TABLE IF EXISTS `#__insure_motor_make_model_hla`;

DROP TABLE IF EXISTS `#__insure_motor_quote_master`;

DROP TABLE IF EXISTS `#__insure_motor_driver_info`;

DROP TABLE IF EXISTS `#__insure_motor_quote_sompo`;

DROP TABLE IF EXISTS `#__insure_motor_policy_error`;

DROP TABLE IF EXISTS `#__insure_motor_quote_msig`;

DROP TABLE IF EXISTS `#__insure_motor_quote_hla`;

DROP TABLE IF EXISTS `#__insure_motor_quote_renewal`;

DROP TABLE IF EXISTS `#__insure_motor_partner_setting`;


