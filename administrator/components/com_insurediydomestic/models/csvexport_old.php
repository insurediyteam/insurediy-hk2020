<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Transactions model.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 * @since       1.5
 */
class InsureDIYDomesticModelCSVExport extends JModelAdmin {

	/**
	 * @var		string	The prefix to use with controller messages.
	 * @since   1.6
	 */
	protected $text_prefix = 'COM_INSUREDIYDOMESTIC';

	/**
	 * Method to test whether a record can be deleted.
	 *
	 * @param   object	A record object.
	 * @return  boolean  True if allowed to delete the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canDelete($record) {
		if (!empty($record->id)) {
			if ($record->state != -2) {
				return;
			}
			$user = JFactory::getUser();

			if ($record->catid) {
				return $user->authorise('core.delete', 'com_insurediydomestic.category.' . (int) $record->catid);
			} else {
				return parent::canDelete($record);
			}
		}
	}

	/**
	 * Method to test whether a record can have its state changed.
	 *
	 * @param   object	A record object.
	 * @return  boolean  True if allowed to change the state of the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canEditState($record) {
		$user = JFactory::getUser();

		if (!empty($record->catid)) {
			return $user->authorise('core.edit.state', 'com_insurediydomestic.category.' . (int) $record->catid);
		} else {
			return parent::canEditState($record);
		}
	}

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   type	The table type to instantiate
	 * @param   string	A prefix for the table class name. Optional.
	 * @param   array  Configuration array for model. Optional.
	 * @return  JTable	A database object
	 * @since   1.6
	 */
	public function getTable($type = 'Plan', $prefix = 'InsureDIYDomesticTable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array  $data		An optional array of data for the form to interogate.
	 * @param   boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return  JForm	A JForm object on success, false on failure
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true) {
		$app = JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm('com_insurediydomestic.csvexport', 'csvexport', array('control' => 'jform', 'load_data' => $loadData));

		if (empty($form)) {
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 * @since   1.6
	 */
	protected function loadFormData() {
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_insurediydomestic.edit.plan.data', array());

		if (empty($data)) {
			$data = $this->getItem();

			// Prime some default values.
			if ($this->getState('plan.id') == 0) {
				$app = JFactory::getApplication();
				//$data->set('catid', $app->input->get('catid', $app->getUserState('com_contact.contacts.filter.category_id'), 'int'));
			}
		}

		$this->preprocessData('com_insurediydomestic.plan', $data);

		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param   integer	The id of the primary key.
	 *
	 * @return  mixed  Object on success, false on failure.
	 * @since   1.6
	 */
	public function getItem($pk = null) {
		if ($item = parent::getItem($pk)) {

			//$item->articletext = trim($item->fulltext) != '' ? $item->introtext . "<hr id=\"system-readmore\" />" . $item->fulltext : $item->introtext;

			if (!empty($item->id)) {

			}
		}

		return $item;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @since   1.6
	 */
	protected function prepareTable($table) {
		$date = JFactory::getDate();
		$user = JFactory::getUser();
	}

	/**
	 * A protected method to get a set of ordering conditions.
	 *
	 * @param   object	A record object.
	 * @return  array  An array of conditions to add to add to ordering queries.
	 * @since   1.6
	 */
	protected function getReorderConditions($table) {
		$condition = array();

		return $condition;
	}

	/**
	 * Method to save the form data.
	 *
	 * @param   array  $data  The form data.
	 *
	 * @return  boolean  True on success.
	 *
	 * @since	3.1
	 */
	public function save($data) {
		$app = JFactory::getApplication();

		$jinput = JFactory::getApplication()->input->getArray($_POST);
		$data['id'] = $jinput['jform']['id'];

		$return = parent::save($data);

		return $return;
	}

	public function export_csv() {

		//ini_set('display_errors',1);
		//ini_set('display_startup_errors',1);
		//error_reporting(-1);
		//DB limit override
		ini_set('memory_limit', '-1'); //**************IMPORTANT

		$app = JFactory::getApplication();
		$db = JFactory::getDBO();

		$jform = $app->input->post->get('jform', '', 'filter');

		$datatype = $jform['export_type'];

		$items = array();
		$headers = FALSE;

		set_time_limit(0);
		switch ($datatype) {
			case 'plans':
				$fields = array(
					"plan_name",
					"insurer_code",
					"premium",
					"plan_duration",
					"plan_no_of_helper",
					"plan_is_oversea",
					"compensation",
					"hs_expense",
					"outpatient",
					"dental",
					"personal_accident",
					"personal_liability",
					"repatriation",
					"re_hiring",
					"hospital_cash",
					"loan_protection",
					"fidelity_protection",
					"critital_illness",
					"brochure",
					"flag_banner",
					"state",
					"ref_points",
					"pur_points",
					"other_benefit"
				);
				//insurer_code AS Insurer, year_code AS plans, is_smoking , gender AS Gender, age AS Age, sum_insured AS SA, plan_index_code AS PlanIndexCode, price AS Premium
				$query = " SELECT " . implode(',', $fields) . " FROM #__insure_domestic_plans";
				$db->setQuery($query);
				$items = $db->loadAssocList();

				break;

			case 'all_quotations':
			case 'completed_quotations':
			case 'pending_quotations':

				$query = $db->getQuery(TRUE)
						->select("q.*, qp.insurer_code, qp.plan_name, qp.premium")
						->from("#__insure_domestic_quotations as q")
						->leftJoin("#__insure_domestic_quotations_to_plans as qp ON q.id = qp.quotation_id");


				if ($datatype == "completed_quotations") {
					$query->where("q.quote_status = 1");
				}
				if ($datatype == "pending_quotations") {
					$query->where("q.quote_status != 1");
				}

				if (strlen($jform['date_start']) > 0 && strlen($jform['date_end']) > 0) {
					$sdate = new JDate($jform['date_start']);
					$edate = new JDate($jform['date_end']);
					$query->where("DATE(" . $db->quote($sdate->toSql()) . ') <= DATE(q.created_date)');
					$query->where('DATE(q.created_date) <= DATE(' . $db->quote($edate->toSql()) . ")");
				} elseif (strlen($jform['date_start']) > 0) {
					$sdate = new JDate($jform['date_start']);
					$query->where('DATE(q.created_date) >= DATE(' . $db->quote($sdate->toSql()) . ")");
				} elseif (strlen($jform['date_end']) > 0) {
					$edate = new JDate($jform['date_end']);
					$query->where('DATE(q.created_date) <= DATE(' . $db->quote($edate->toSql()) . ")");
				}

				$db->setQuery($query);
				$quotations = $db->loadAssocList();
				if (empty($quotations)) {
					break;
				}
				$headers = array_keys(reset($quotations));

				$fieldNames = array("firstname", "lastname", "dob", "nationality", "id_no", "contract_start", "contract_end");
				$query->clear();
				$query->select("quotation_id," . implode(",", $fieldNames))
						->from("#__insure_domestic_quotations_to_helpers");
				$helpers = $db->loadObjectList();
				$groupedHelpers = array();
				foreach ($helpers as $helper) {
					$key = $helper->quotation_id;
					unset($helper->quotation_id);
					$groupedHelpers[$key][] = $helper;
				}
				$maxNoOfHelpers = 0;
				foreach ($groupedHelpers as $groupedHelper) {
					if (count($groupedHelper) > $maxNoOfHelpers) {
						$maxNoOfHelpers = count($groupedHelper);
					}
				}
				for ($i = 1; $i <= $maxNoOfHelpers; $i++) {
					foreach ($fieldNames as $fieldName) {
						$headers[] = $fieldName . "_" . $i;
					}
				}

				foreach ($quotations as $quotation) {
					if (isset($groupedHelpers[$quotation["id"]])) {
						foreach ($groupedHelpers[$quotation["id"]] as $pk => $helpers) {
							foreach ($helpers as $k => $v) {
								$key = $k . "_" . ($pk + 1);
								$quotation[$key] = $v;
							}
						}
					}
					$items[] = $quotation;
				}
			break;
			case 'policies':
				$fields = array(
					'p.insurer',
					'p.policy_number',
					'c.code AS currency',
					'p.cover_amt',
					'p.start_date',
					'p.end_date',
					'p.status'
				);
				
				$query = $db->getQuery(TRUE)
					->select($fields)
					->from("#__insure_policies AS p ")
					->join("left", "#__currencies AS c ON c.id = p.currency ")
					->where("p.type = 6 ");
				
				$db->setQuery($query);
				$items = $db->loadAssocList();
				
				foreach($items as $i => &$item) {
					$item['status'] = InsureDIYHelper::getPoliciesStatus($item['status']);
				}
			
			break;
		}

		if (count($items) == 0) {
			echo 'No Items';
		} else {

			ob_start();

			$df = fopen("php://output", 'w');

			$headers = ($headers) ? $headers : array_keys(reset($items));

			fputcsv($df, $headers);

			foreach ($items as $row) {

				switch ($datatype) {
					case 'plans':
						break;
					case 'all_quotations':
					case 'completed_quotations':
					case 'pending_quotations':
						$row['created_date'] = JHtml::_('date', $row['created_date'], 'd-m-Y H:i');
						break;
					default:
						break;
				}

				fputcsv($df, $row);
			}
			fclose($df);
		}


		// disable caching
		$filename = 'domestic_' . $datatype . '_' . JHtml::date(NULL, 'EXPORT_DATE_FORMAT') . '.csv';
		$now = gmdate("D, d M Y H:i:s");
		header("Expires: Tue, 03 Jul 2001 06:00:00 GMT");
		header("Cache-Control: max-age=0, no-cache, must-revalidate, proxy-revalidate");
		header("Last-Modified: {$now} GMT");

		// force download
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");

		// disposition / encoding on response body
		header("Content-Disposition: attachment;filename={$filename}");
		header("Content-Transfer-Encoding: binary");

		return true;
	}

	/**
	 * Method to change the title & alias.
	 *
	 * @param   integer  $category_id  The id of the parent.
	 * @param   string   $alias        The alias.
	 * @param   string   $name         The title.
	 *
	 * @return  array  Contains the modified title and alias.
	 *
	 * @since   3.1
	 */
	protected function generateNewTitle($category_id, $alias, $name) {
		// Alter the title & alias
		$table = $this->getTable();

		while ($table->load(array('alias' => $alias))) {
			if ($name == $table->title) {
				$name = JString::increment($name);
			}
			$alias = JString::increment($alias, 'dash');
		}

		return array($name, $alias);
	}

}
