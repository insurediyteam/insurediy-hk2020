<?php

defined('_JEXEC') or die;

class InsureDIYDomesticModelPNImport extends JModelForm {

	protected $text_prefix = 'COM_INSUREDIYDOMESTIC';

	public function getForm($data = array(), $loadData = true) {
// Get the form.
		$form = $this->loadForm('com_insurediydomestic.pnimport', 'pnimport', array('control' => 'jform', 'load_data' => $loadData));

		if (empty($form)) {
			return false;
		}

		return $form;
	}

	public function import_policynos() {
		ini_set('memory_limit', '-1'); //**************IMPORTANT
		$app = JFactory::getApplication();
		$db = JFactory::getDBO();
		$existing_pns = $this->getPolicyNos();

		$files = $app->input->files->get('jform');
		$file = $files['pnimport'];
		$filename = JFile::makeSafe($file['name']);
		$ext = JFile::getExt($filename);

		if ($ext != 'csv') {
			return false;
		}

		$dest = JPATH_ROOT . '/tmp/' . $filename;

		JFile::upload($file['tmp_name'], $dest);

		$insertQuery = $db->getQuery(TRUE);
		if (($handle = fopen($dest, "r")) !== FALSE) {

			$header = fgetcsv($handle, 1000, ",");
			if (count($header) != 2) {
				return FALSE;
			}
			$queryData = array();
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				set_time_limit(0);

				$plan_name = isset($data[0]) ? $data[0] : "";
				$policy_number = isset($data[1]) ? $data[1] : "";

				if (!in_array($policy_number, $existing_pns)) {
					$queryData[] = "('$plan_name', '$policy_number')";
				}
			}
			$count = count($queryData);
			$perrun = 1000;
			if ($count > $perrun) {
				$times = floor($count / $perrun) + 1;
				for ($i = 0; $i < $times; $i++) {
					$left = $count - ($perrun * $i);
					$length = ($left > $perrun) ? $perrun : $left;
					$tempData = array_slice($queryData, $i * $perrun, $length);
					$query = "INSERT INTO #__insure_domestic_policy_numbers (plan_name, policy_number) VALUES " . implode(",", $tempData) . ";";
					$db->setQuery($query);
					$db->query();
				}
			} else {
				$query = "INSERT INTO #__insure_domestic_policy_numbers (plan_name, policy_number) VALUES " . implode(",", $queryData) . ";";
				$db->setQuery($query);
				$db->query();
			}
			fclose($handle);
		}

		unlink($dest);
		return true;
	}

	public function getPolicyNos() {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("policy_number")
				->from("#__insure_domestic_policy_numbers");
		$db->setQuery($query);
		return $db->loadColumn();
	}

}
