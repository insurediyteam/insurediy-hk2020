<?php

defined('_JEXEC') or die;

class InsureDIYDomesticModelQuotation extends JModelAdmin {

	protected $text_prefix = 'COM_INSUREDIYDOMESTIC';
	protected $_tb_plan = "#__insure_domestic_plans";
	protected $_tb_company = "#__insure_companies";
	protected $_tb_quotation = "#__insure_domestic_quotations";
	protected $_tb_quotation_plan = "#__insure_domestic_quotations_to_plans";
	protected $_tb_quotation_traveller = "#__insure_domestic_quotations_to_travellers";
	protected $_tb_questionnaire = "#__insure_domestic_questionnaires";
	protected $_tb_quotation_questionnaire = "#__insure_domestic_quotation_questionnaires";
	protected $_tb_quotation_ex_insurance = "#__insure_domestic_quotation_existing_insurances";
	protected $_tb_quotation_maid = "#__insure_domestic_quotations_to_helpers";

	protected function canDelete($record) {
		if (!empty($record->id)) {
			if ($record->state != -2) {
				return;
			}
			$user = JFactory::getUser();

			if ($record->catid) {
				return $user->authorise('core.delete', 'com_insurediydomestic.category.' . (int) $record->catid);
			} else {
				return parent::canDelete($record);
			}
		}
	}

	protected function canEditState($record) {
		$user = JFactory::getUser();

		if (!empty($record->catid)) {
			return $user->authorise('core.edit.state', 'com_insurediydomestic.category.' . (int) $record->catid);
		} else {
			return parent::canEditState($record);
		}
	}

	public function getTable($type = 'Quotation', $prefix = 'InsureDIYDomesticTable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}

	public function getForm($data = array(), $loadData = true) {
		// Get the form.
		$form = $this->loadForm('com_insurediydomestic.quotation', 'quotation', array('control' => 'jform', 'load_data' => $loadData));

		if (empty($form)) {
			return false;
		}

		return $form;
	}

	protected function loadFormData() {
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_insurediydomestic.edit.quotation.data', array());

		if (empty($data)) {
			$data = $this->getItem();

			// Prime some default values.
			if ($this->getState('quotation.id') == 0) {

			}
		}
		$this->preprocessData('com_insurediydomestic.quotation', $data);

		return $data;
	}

	public function getItem($pk = null) {
		if ($item = parent::getItem($pk)) {

			$item->file_application = $this->getFileApplicationPaths($item->file_json_pdfpath, $item->id);
			$item->file_application2 = $this->getFileApplicationPaths($item->file_json_pdfpath2, $item->id);
//			$existingInsurances = $this->getExistingInsurances($item->id);
//			$values = array();
//			foreach ($existingInsurances as $existingInsurance) {
//				foreach ($existingInsurance as $k => $v) {
//					if ($k == "issuance_date") {
//						$values[$k][] = date("d-m-Y", strtotime($v));
//					} else {
//						$values[$k][] = $v;
//					}
//				}
//			}

			$transaction = $this->getTransaction($item->unique_order_no);
			if (is_object($transaction)) {
				$item->PayRef = $transaction->PayRef;
				$item->TxTime = $transaction->TxTime;
				$item->Amt = $transaction->Amt;
			}

//			$item->existing_insurances = $values;
			$item->selected_plan = $this->getSelectedPlan($item->id);
			if (!empty($item->selected_plan)) {
				unset($item->selected_plan['id']);
				unset($item->selected_plan['quotation_id']);
				foreach ($item->selected_plan as $k => $v) {
					$key = "" . $k;
					$item->$key = $v;
				}
			}
		}

		return $item;
	}

	public function getTransaction($unique_order_no) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_transactions")
				->where("unique_order_no = " . $db->quote($unique_order_no));
		$db->setQuery($query);

		return $db->loadObject();
	}

	public function getFileApplicationPaths($json, $id) {
		$tmp = array();
		$decoded = json_decode($json);
		$array = (is_array($decoded)) ? $decoded : MyHelper::object2array($decoded);
		foreach ($array as $path) {
			$vals = explode('|', $path);
			foreach ($vals as $val) {
				$tmp[$val] = MyHelper::getDeepPath(QUOTATION_PDF_SAVE_PATH, $id, TRUE) . trim($val);
			}
		}
		return $tmp;
	}

	public function getSelectedPlan($pk) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$query->select("*");
		$query->from($this->_tb_quotation_plan);
		$query->where("quotation_id=" . $db->quote($pk));
		return $db->setQuery($query)->loadAssoc();
	}

	public function getExistingInsurances($pk) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$query->select("id, insurer_name, policy_type, sum_insured, issuance_date");
		$query->from($this->_tb_quotation_ex_insurance);
		$query->where("quotation_id=" . $db->quote($pk));
		$db->setQuery($query);
		$result = $db->loadAssocList();
		if (!$result) {

		}
		return $result;
	}

	public function getMaids($pk) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$query->select("*");
		$query->from($this->_tb_quotation_maid);
		$query->where("quotation_id=" . $db->quote($pk));
		$db->setQuery($query);
		$result = $db->loadAssocList();
		if (!$result) {

		}
		return $result;
	}

	protected function prepareTable($table) {
		$date = JFactory::getDate();
		$user = JFactory::getUser();
	}

	protected function getReorderConditions($table) {
		$condition = array();

		return $condition;
	}

	public function getQuestionnaires() {
		$db = JFactory::getDBO();

		$query = " SELECT * FROM #__insure_domestic_quotation_questionnaires AS qx "
				. " LEFT JOIN #__insure_domestic_questionnaires AS q ON q.id = qx.question_id "
				. " WHERE qx.quotation_id = " . $db->Quote($this->getState('quotation.id'), false);
		$db->setQuery($query);
		$rows = $db->loadObjectList();

		return $rows;
	}

	public function save($data) {
		$app = JFactory::getApplication();
		$post = $app->input->get('jform', '', 'array');
		$data['id'] = $post['id'];
		$return = parent::save($data);
		return $return;
	}

	public function getPdfData($quotation_id) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_domestic_quotations")
				->where("id=" . $quotation_id);
		$db->setQuery($query);
		$result = $db->loadObject();

		$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_domestic_quotation_existing_insurances")
				->where("quotation_id=" . $quotation_id);
		$db->setQuery($query);
		$existing_insurances = $db->loadObjectList();

		$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_domestic_quotation_properties")
				->where("quotation_id=" . $quotation_id)
				->order("id");
		$db->setQuery($query);
		$properties = $db->loadObjectList();

		$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_domestic_quotation_cars")
				->where("quotation_id=" . $quotation_id);
		$db->setQuery($query, 0, 3);
		$cars = $db->loadObjectList();

		// Prepare data before return
		$result->hasLargeSum = $this->checkSumInsuredQuotationTotal($quotation_id) >= 5000000;
		$result->contact_firstname = strtoupper($result->contact_firstname);
		$result->contact_lastname = strtoupper($result->contact_lastname);
		$result->country_residence = InsureDIYHelper::getCountryOfResidence($result->country_residence);
		$result->nationality = InsureDIYHelper::getNationality($result->nationality);

		$result->monthly_income_ckbox = $result->monthly_income;
		$result->monthly_income = JText::_($result->monthly_income);

		switch ($result->contact_identity_type) {
			case 'chid' :
				$result->contact_identity_no2 = $result->contact_identity_no;
				unset($result->contact_identity_no);

				$tmp = explode('-', $result->contact_expiry_date);
				$result->contact_expiry_date_y = $tmp[0];
				$result->contact_expiry_date_m = $tmp[1];
				$result->contact_expiry_date_d = $tmp[2];
				break;
			case 'passport':
				$result->contact_identity_no3 = $result->contact_identity_no;
				unset($result->contact_identity_no);

				$tmp = explode('-', $result->contact_expiry_date);
				$result->contact_expiry_date_y2 = $tmp[0];
				$result->contact_expiry_date_m2 = $tmp[1];
				$result->contact_expiry_date_d2 = $tmp[2];

				$result->bank_acct_holder_type = "p";
				break;
			case "hkid":
				$result->bank_acct_holder_type = "i";
			default:
				break;
		}

		$dob = explode("-", $result->dob);
		$result->dob_y = $dob[0];
		$result->dob_m = $dob[1];
		$result->dob_d = $dob[2];
		$result->age = (int) date("Y") - (int) $dob[0];
		$result->contact_telephone = (($result->contact_country_code) ? '(' . $result->contact_country_code . ') ' : '') . $result->contact_contact_no;

		$result->job_nature = JText::_($result->job_nature);
		$result->curreny = "hkd";
		$result->mode = "monthly";
		$result->source = "savings";
		$result->purpose = "protection";
		$result->cover_amt = "HK$" . $result->cover_amt;

		$result->contact_lang = "english";

		$result->height_unit = "cm";
		$result->weight_unit = "kg";
		$result->has_used_drugs = "1";

		$result->direct_promotion = "1";
		$result->full_name = $result->contact_firstname . " " . $result->contact_lastname;
		$result->relationship = "applicant";

		foreach ($existing_insurances as $k => $existing_insurance) {
			$key = $k + 1;
			$insurer_key = "ex_insurer_name_" . $key;
			$insured_key = "ex_insured_name_" . $key;
			$sum_key = "ex_sum_insured_" . $key;
			$date_key = "ex_issuance_date_" . $key;

			$result->$insurer_key = $existing_insurance->insurer_name;
			$result->$insured_key = $result->full_name;
			$result->$sum_key = $existing_insurance->sum_insured;
			$result->$date_key = $existing_insurance->issuance_date;
		}

		$year = (int) date("Y", strtotime($result->created_date));
		$result->lastyear = $year - 1;
		$result->last2year = $year - 2;
		$result->last3year = $year - 3;
		foreach ($properties as $k => $property) {
			$key = $k + 1;
			$property_purchase_key = "property_purchase_" . $key;
			$property_price_key = "property_price_" . $key;
			$property_mortgage_key = "property_mortgage_" . $key;
			$property_value_key = "property_value_" . $key;
			$result->$property_purchase_key = $property->property_purchase;
			$result->$property_price_key = $property->property_price;
			$result->$property_mortgage_key = $property->property_mortgage;
			$result->$property_value_key = $property->property_value;
		}

		$result->no_of_cars = count($cars);

		foreach ($cars as $k => $car) {
			$key = $k + 1;
			$model_key = "car_model_" . $key;
			$result->$model_key = $car->car_model;
		}

		$result->contact_country = ($result->contact_country == "HK") ? $result->contact_country : "OTH";

		$result->fna = "0"; // SUN only
		$result->owner_type = "0";
		return $result;
	}

	public function checkSumInsuredQuotationTotal($quotation_id) {
		$db = JFactory::getDbo();
		/* 1. Sum Insured from Plans - see Step 3 page */
		$query = " SELECT sum_insured FROM #__insure_domestic_quotation_plan_xref WHERE quotation_id = '$quotation_id' ";
		$db->setQuery($query);
		$rows = $db->loadObjectList();

		$total_sum_insured = 0;
		foreach ($rows as $r):
			$total_sum_insured += (int) $r->sum_insured;
		endforeach;

		/* 2. Sum Insured from Existing Plans  - see Step 4 page */
		$query = " SELECT sum_insured FROM #__insure_domestic_quotation_existing_insurances WHERE quotation_id = '$quotation_id' ";
		$db->setQuery($query);
		$rows = $db->loadObjectList();

		foreach ($rows as $r):
			$total_sum_insured += intval(str_replace(",", "", $r->sum_insured));
		endforeach;

		return $total_sum_insured;
	}

	public function getPlans($quotation_id) {
		$db = JFactory::getDBO();
		$query = " SELECT * FROM #__insure_domestic_quotation_plan_xref WHERE quotation_id = '$quotation_id' ";
		$db->setQuery($query);
		$rows = $db->loadObjectList();
		return $rows;
	}

	public function savePDFPaths($quotation_id, $array = array(), $client = TRUE) {
		if (empty($array)) {
			return;
		}

		$toJson = new stdClass();

		foreach ($array as $k => $v) {
			if (is_array($v)) {
				$toJson->$k = implode(" | ", $v);
			} else {
				$toJson->$k = $v;
			}
		}

		$db = JFactory::getDBO();
		if ($client) {
			$query = " UPDATE #__insure_domestic_quotations SET `file_json_pdfpath` = " . $db->Quote(json_encode($toJson), false) . " WHERE id = '$quotation_id' ";
		} else {
			$query = " UPDATE #__insure_domestic_quotations SET `file_json_pdfpath2` = " . $db->Quote(json_encode($toJson), false) . " WHERE id = '$quotation_id' ";
		}
		$db->setQuery($query);
		$db->query();
	}

}
