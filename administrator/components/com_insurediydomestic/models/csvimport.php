<?php

defined('_JEXEC') or die;

class InsureDIYDomesticModelCSVImport extends JModelAdmin {

	protected $text_prefix = 'COM_INSUREDIYDOMESTIC';

	public function getForm($data = array(), $loadData = true) {
		$form = $this->loadForm('com_insurediydomestic.csvimport', 'csvimport', array('control' => 'jform', 'load_data' => $loadData));

		if (empty($form)) {
			return false;
		}

		return $form;
	}

	public function getTable($type = 'Plan', $prefix = 'InsureDIYDomesticTable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}

	public function import_plans() {
		$app = JFactory::getApplication();
		$db = JFactory::getDBO();

		$cols = array(
			"plan_name",
			"insurer_code",
			"premium",
			"plan_duration",
			"plan_no_of_helper",
			"plan_is_oversea",
			"compensation",
			"hs_expense",
			"outpatient",
			"dental",
			"personal_accident",
			"personal_liability",
			"repatriation",
			"re_hiring",
			"hospital_cash",
			"loan_protection",
			"fidelity_protection",
			"critital_illness",
			"brochure",
			"flag_banner",
			"state",
			"ref_points",
			"pur_points",
			"other_benefit"
		);

		$files = $app->input->files->get('jform');
		$file = $files['csvimport'];
		$filename = JFile::makeSafe($file['name']);

		$ext = JFile::getExt($filename);

		if ($ext != 'csv') {
			return false;
		}

		$dest = JPATH_ROOT . '/idy_tmp/' . $filename;
		move_uploaded_file($file['tmp_name'], $dest);
//		JFile::upload($file['tmp_name'], $dest);



		if (($handle = fopen($dest, "r")) !== FALSE) {
			$header = fgetcsv($handle, 1000, ",");
			if (count($header) < 21) {
				return false;
			}
			$query = " TRUNCATE #__insure_domestic_plans ;";
			$db->setQuery($query);
			$db->query();

			$queryData = array();
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
				set_time_limit(0);
				$plan_index_code = $data[1] . $data[3] . $data[4] . $data[5];
				$queryData[] = "('$plan_index_code', '" . implode("','", $data) . "')";
			}
			$count = count($queryData);
			$perrun = 1000;
			if ($count > $perrun) {
				$times = floor($count / $perrun) + 1;
				for ($i = 0; $i < $times; $i++) {
					$left = $count - ($perrun * $i);
					$length = ($left > $perrun) ? $perrun : $left;
					$tempData = array_slice($queryData, $i * $perrun, $length);
					$query = "INSERT INTO #__insure_domestic_plans (plan_index_code, " . implode(",", $cols) . ") VALUES " . implode(",", $tempData) . ";";
					$db->setQuery($query);
					$db->query();
				}
			} else {
				$query = "INSERT INTO #__insure_domestic_plans (plan_index_code," . implode(",", $cols) . ") VALUES " . implode(",", $queryData) . ";";
				$db->setQuery($query);
				$db->query();
			}
			fclose($handle);
		}
		unlink($dest);
		return true;
	}

}
