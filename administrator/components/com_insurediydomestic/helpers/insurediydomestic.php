<?php

defined('_JEXEC') or die;

class InsureDIYDomesticHelper {

	public static function addSubmenu($vName = 'quotations') {
		JHtmlSidebar::addEntry(
				JText::_('COM_INSUREDIYDOMESTIC_SUBMENU_QUOTATIONS'), 'index.php?option=com_insurediydomestic&view=quotations', $vName == 'quotations'
		);

		JHtmlSidebar::addEntry(
				JText::_('COM_INSUREDIYDOMESTIC_SUBMENU_PLANS'), 'index.php?option=com_insurediydomestic&view=plans', $vName == 'plans'
		);

		JHtmlSidebar::addEntry(
				JText::_('COM_INSUREDIYDOMESTIC_SUBMENU_CSV_EXPORT'), 'index.php?option=com_insurediydomestic&view=csvexport', $vName == 'csvexport'
		);

		JHtmlSidebar::addEntry(
				JText::_('COM_INSUREDIYDOMESTIC_SUBMENU_CSV_IMPORT'), 'index.php?option=com_insurediydomestic&view=csvimport', $vName == 'csvimport'
		);
//
//		JHtmlSidebar::addEntry(
//				JText::_('COM_INSUREDIYDOMESTIC_SUBMENU_POLICYNOS'), 'index.php?option=com_insurediydomestic&view=policynos', $vName == 'policynos'
//		);
//		JHtmlSidebar::addEntry(
//				JText::_('COM_INSUREDIYDOMESTIC_SUBMENU_PNIMPORT'), 'index.php?option=com_insurediydomestic&view=pnimport', $vName == 'pnimport'
//		);
	}

	public static function getActions($categoryId = 0) {
		$user = JFactory::getUser();
		$result = new JObject;

		if (empty($categoryId)) {
			$assetName = 'com_insurediy';
			$level = 'component';
		} else {
			$assetName = 'com_insurediy.category.' . (int) $categoryId;
			$level = 'category';
		}

		$actions = JAccess::getActions('com_insurediydomestic', $level);

		foreach ($actions as $action) {
			$result->set($action->name, $user->authorise($action->name, $assetName));
		}

		return $result;
	}

	public static function getYesNoOptions() {
		$options = array();

		array_unshift($options, JHtml::_('select.option', '0', JText::_('JNO')));
		array_unshift($options, JHtml::_('select.option', '1', JText::_('JYES')));


		return $options;
	}

	public static function getGenderOptions() {
		$options = array();

		array_unshift($options, JHtml::_('select.option', 'F', JText::_('COM_INSUREDIYDOMESTIC_GENDER_FEMALE')));
		array_unshift($options, JHtml::_('select.option', 'M', JText::_('COM_INSUREDIYDOMESTIC_GENDER_MALE')));

		return $options;
	}

	public static function getInsuranceCompanyOptions() {
		$db = JFactory::getDBO();

		$options = array();

		$query = " SELECT * FROM #__insure_companies ";
		$db->setQuery($query);
		$rows = $db->loadObjectList();

		foreach ($rows as $r) :
			$options[] = JHtml::_('select.option', $r->insurer_code, $r->company_name);
		endforeach;

		return $options;
	}

	public static function getPolicyNumberStateOptions() {
		$options = array();
		$options[] = JHtml::_('select.option', 0, "COM_INSUREDIYDOMESTIC_POLICY_NUMBER_STATE_USED");
		$options[] = JHtml::_('select.option', 1, "COM_INSUREDIYDOMESTIC_POLICY_NUMBER_STATE_AVAILABLE");

		return $options;
	}

}
