<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_insurediy
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Weblink controller class.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_insurediy
 * @since       1.6
 */
class InsureDIYDomesticControllerPNImport extends JControllerForm {

	public function policynos() {

		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		$model = $this->getModel('PNImport');

		if ($model->import_policynos()) {
			$msg = ' Importing Policy Numbers Success. ';
		} else {
			$msg = ' Importing Policy Numbers Failed! Please try again...';
		}
		$this->setRedirect(JRoute::_('index.php?option=com_insurediydomestic&view=pnimport', false), $msg);
	}

}
