<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_CedeleDepots
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * View to edit a weblink.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_CedeleDepots
 * @since       1.5
 */
class InsureDIYDomesticViewQuotation extends JViewLegacy {

	protected $state;
	protected $item;
	protected $form;

	/**
	 * Display the view
	 */
	public function display($tpl = null) {
		$this->state = $this->get('State');
		$this->item = $this->get('Item');
		$this->form = $this->get('Form');
//		$this->questionnaires = $this->get('Questionnaires');
		$this->applications_form = $this->get('ListApplicationFormHTML');
		$model = $this->getModel("Existinginsurance");
		$model2 = $this->getModel();

		$this->maids = $model2->getMaids($this->item->id);
		$this->ei_form = $model->getForm();


		// Check for errors.
		if (empty($this->item)) {
			JError::raiseError(500, "No Cedele Depot Item");
			return false;
		}

		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar() {
		JFactory::getApplication()->input->set('hidemainmenu', true);

		JToolbarHelper::title(JText::_('COM_INSUREDIYDOMESTIC_MANAGER_QUOTATION'), 'insurediydomestic.png');
		JToolbarHelper::apply('quotation.apply');
		JToolbarHelper::save('quotation.save');

		if (empty($this->item->id)) {
			JToolbarHelper::cancel('quotation.cancel');
		} else {
			JToolbarHelper::cancel('quotation.cancel', 'JTOOLBAR_CLOSE');
		}
	}

}
