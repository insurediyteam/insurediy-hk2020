<?php

defined('_JEXEC') or die;

class InsureDIYDomesticViewCSVExport extends JViewLegacy {

	protected $state;
	protected $item;
	protected $form;

	public function display($tpl = null) {
		$this->state = $this->get('State');
		$this->item = $this->get('Item');
		$this->form = $this->get('Form');

		$db = JFactory::getDBO();

		// Check for errors.
		if (empty($this->item)) {
			JError::raiseError(500, "No Cedele Depot Item");
			return false;
		}

		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar() {
		JFactory::getApplication()->input->set('hidemainmenu', true);

		$user = JFactory::getUser();
	

		JToolbarHelper::title(JText::_('COM_INSUREDIYDOMESTIC_MANAGER_PLAN'), 'insurediydomestic.png');

		// If not checked out, can save the item.
		if (empty($this->item->id)) {
			JToolbarHelper::cancel('plan.cancel');
		} else {
			JToolbarHelper::cancel('plan.cancel', 'JTOOLBAR_CLOSE');
		}

		/*
		  JToolbarHelper::divider();
		  JToolbarHelper::help('JHELP_COMPONENTS_CedeleDepotS_LINKS_EDIT');
		 */
	}

}
