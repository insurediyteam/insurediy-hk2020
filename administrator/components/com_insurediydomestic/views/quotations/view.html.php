<?php

defined('_JEXEC') or die;

class InsureDIYDomesticViewQuotations extends JViewLegacy {

	protected $items;
	protected $pagination;
	protected $state;

	/**
	 * Display the view
	 *
	 * @return  void
	 */
	public function display($tpl = null) {
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');

		InsureDIYDomesticHelper::addSubmenu('quotations');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar() {
		require_once JPATH_COMPONENT . '/helpers/insurediydomestic.php';

		$state = $this->get('State');
		$canDo = InsureDIYDomesticHelper::getActions($state->get('filter.category_id'));
		$user = JFactory::getUser();
		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');

		JToolbarHelper::title(JText::_('COM_INSUREDIYDOMESTIC_MANAGER_QUOTATIONS'), 'insurediydomestic.png');

		if ($canDo->get('core.create')) {
			JToolbarHelper::addNew('quotation.add');
		}

		if ($canDo->get('core.edit')) {
			JToolbarHelper::editList('quotation.edit');
		}


		if ($canDo->get('core.edit.state')) {

			JToolbarHelper::publish('quotation.publish', 'JTOOLBAR_PUBLISH', true);
			JToolbarHelper::unpublish('quotation.unpublish', 'JTOOLBAR_UNPUBLISH', true);

			//JToolbarHelper::archiveList('quotation.archive');
			//JToolbarHelper::checkin('quotation.checkin');
		}


		if ($state->get('filter.state') == -2 && $canDo->get('core.delete')) {
			JToolbarHelper::deleteList('', 'quotation.delete', 'JTOOLBAR_EMPTY_TRASH');
		} elseif ($canDo->get('core.edit.state')) {
			JToolbarHelper::trash('quotation.trash');
		}

		if ($canDo->get('core.admin')) {
			JToolbarHelper::preferences('com_insurediydomestic');
		}

		JHtmlSidebar::setAction('index.php?option=com_insurediydomestic&view=quotations');
		JHtmlSidebar::setAction('index.php?option=com_insurediydomestic&view=genders');
		JHtmlSidebar::setAction('index.php?option=com_insurediydomestic&view=plans');
	}

	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 *
	 * @since   3.0
	 */
	protected function getSortFields() {
		return array(
//			't.insurediydomestic_date' => JText::_('COM_INSUREDIYDOMESTIC_CEDELEDEPOT_DATE'),
//			'a.ordering' => JText::_('JGRID_HEADING_ORDERING'),
		);
	}

}
