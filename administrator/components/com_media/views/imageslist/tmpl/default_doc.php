<?php
defined('_JEXEC') or die;

$params = new JRegistry;
$dispatcher = JEventDispatcher::getInstance();
$dispatcher->trigger('onContentBeforeDisplay', array('com_media.file', &$this->_tmp_doc, &$params));
?>
<li class="imgOutline thumbnail height-80 width-80 center">
	<a class="img-preview" href="javascript:ImageManager.populateFields('<?php echo $this->_tmp_doc->path_relative; ?>')" title="<?php echo $this->_tmp_doc->name; ?>" >
		<div class="height-50">
			<?php
				echo JHtml::image($this->_tmp_doc->icon_32, $this->_tmp_doc->name, null, true, true)?
					JHtml::image($this->_tmp_doc->icon_32, $this->_tmp_doc->title, null, true) : JHtml::image('media/con_info.png', $this->_tmp_doc->name, null, true);
			?>
		</div>
		<div class="small">
			<?php echo JHtml::_('string.truncate', $this->_tmp_doc->name, 10, false); ?>
		</div>
	</a>
</li>
<?php
$dispatcher->trigger('onContentAfterDisplay', array('com_media.file', &$this->_tmp_doc, &$params));
?>
