<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Methods supporting a list of weblink records.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 * @since       1.6
 */
class InsurePoliciesModelPolicies extends JModelList {

	/**
	 * Constructor.
	 *
	 * @param   array  An optional associative array of configuration settings.
	 * @see     JController
	 * @since   1.6
	 */
	public function __construct($config = array()) {
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'id', 'c.id',
				'policy_number', 'c.policy_number',
				'user_id', 'c.user_id',
				'type', 'c.type',
				'insurer', 'c.insurer',
				'start_date', 'c.start_date',
				'end_date', 'c.end_date',
				'status', 'c.status'
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since   1.6
	 */
	protected function populateState($ordering = null, $direction = null) {
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$accessId = $this->getUserStateFromRequest($this->context . '.filter.access', 'filter_access', null, 'int');
		$this->setState('filter.access', $accessId);

		// Load the parameters.
		$params = JComponentHelper::getParams('com_insurepolicies');
		$this->setState('params', $params);

		// List state information.
		parent::populateState('c.id', 'asc');
	}

	protected function getStoreId($id = '') {
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.access');

		return parent::getStoreId($id);
	}

	protected function getListQuery() {
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$user = JFactory::getUser();

		// Select the required fields from the table.
		$query->select('c.*, u.username');
		$query->from($db->quoteName('#__insure_policies') . ' AS c');

		// Filter by search in title
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			$search = $db->quote('%' . $db->escape($search, true) . '%');
			$query->where(' (c.policy_number LIKE ' . $search . ') ');
		}

		$query->leftJoin("#__users u ON c.user_id = u.id");

		// Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		$query->order($db->escape($orderCol . ' ' . $orderDirn));
		return $query;
	}

	public function delete($cid) {
		$cids = (array) $cid;
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		if (count($cids) > 0) {
			$query->delete("#__insure_policies");
			$query->where("id IN (" . implode(",", $cids) . ")");
			$db->setQuery($query);
			$result = $db->execute();
			if ($result) {
				return TRUE;
			}
			$this->setError(JText::_('COM_INSUREDIYLIFE_DELETING_ITEMS_FAIL'));
			return FALSE;
		}
		$this->setError(JText::_('COM_INSUREDIYLIFE_NO_ITEM_SELECTED'));
		return FALSE;
	}

}
