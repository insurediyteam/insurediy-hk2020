<?php

defined('_JEXEC') or die;

class InsurePoliciesViewPolicies extends JViewLegacy {

	protected $items;
	protected $pagination;
	protected $state;

	public function display($tpl = null) {
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');

		InsurePoliciesHelper::addSubmenu('policies');

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	protected function addToolbar() {
		require_once JPATH_COMPONENT . '/helpers/insurepolicies.php';

		$state = $this->get('State');
		$canDo = InsurePoliciesHelper::getActions($state->get('filter.category_id'));
		$user = JFactory::getUser();
		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');

		JToolbarHelper::title(JText::_('COM_INSUREPOLICIES_MANAGER_POLICY'), 'insurepolicies.png');

		if ($canDo->get('core.create')) {
			JToolbarHelper::addNew('policy.add');
		}

		if ($canDo->get('core.edit')) {
			JToolbarHelper::editList('policy.edit');
		}


		if ($canDo->get('core.edit.state')) {

		}

		JToolbarHelper::deleteList('', 'policies.delete', 'COM_INSUREPOLICIES_BUTTON_DELETE');

		if ($canDo->get('core.admin')) {
			JToolbarHelper::preferences('com_insurepolicies');
		}

		JHtmlSidebar::setAction('index.php?option=com_insurepolicies&view=policies');
	}

	protected function getSortFields() {
		return array(
			'c.policy_number' => JText::_('COM_INSUREPOLICIES_HEADER_TEXT_POLICY_NO'),
			'c.user_id' => JText::_('COM_INSUREPOLICIES_HEADER_TEXT_USER'),
			'c.insurer' => JText::_('COM_INSUREPOLICIES_HEADER_TEXT_INSURER'),
			'c.type' => JText::_('COM_INSUREPOLICIES_HEADER_TEXT_TYPE'),
			'c.start_date' => JText::_('COM_INSUREPOLICIES_HEADER_TEXT_START_DATE'),
			'c.end_date' => JText::_('COM_INSUREPOLICIES_HEADER_TEXT_END_DATE'),
			'c.status' => JText::_('COM_INSUREPOLICIES_HEADER_TEXT_STATUS'),
		);
	}

}
