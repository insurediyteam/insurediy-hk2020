<?php

defined('_JEXEC') or die;

class InsurePoliciesController extends JControllerLegacy {

	protected $default_view = 'policies';

	public function display($cachable = false, $urlparams = false) {

		$view = $this->input->get('view', 'policies');
		$layout = $this->input->get('layout', 'default');
		$id = $this->input->getInt('id');

		parent::display();

		return $this;
	}

}
