<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_CedeleDepots
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Weblink controller class.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_CedeleDepots
 * @since       1.6
 */
class InsurePoliciesControllerPolicy extends JControllerForm {

	protected function postSaveHook(JModelLegacy $model, $validData = array()) {
		if ($task == 'save') {
			$this->setRedirect(JRoute::_('index.php?option=com_insurepolicies&view=policies', false));
		}
	}

}
