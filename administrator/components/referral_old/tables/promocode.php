<?php

defined('JPATH_PLATFORM') or die;

jimport('joomla.database.table');
jimport('joomla.database.tableasset');

class InsurediyReferralTablePromocode extends JTable {

	protected $_tbl = "#__insure_promocodes";
	protected $_tbl_key = "id";
	protected $_db;

	public function __construct(&$db) {
		$this->_db = $db;
		parent::__construct($this->_tbl, $this->_tbl_key, $this->_db);
	}

	public function store($updateNulls = false) {
		$date = JFactory::getDate();
		$user = JFactory::getUser();

		if ($this->id) {
			$this->modified = $date->toSql();
			$this->modified_by = $user->id;
		} else {
			$this->created = $date->toSql();
			$this->created_by = $user->id;
			$this->trash = 0;
			$this->publish = 1;
		}
		return parent::store($updateNulls);
	}

	public function publish($pks = null, $state = 1, $userId = 0) {
		$k = $this->_tbl_key;
		// Sanitize input.
		JArrayHelper::toInteger($pks);
		$userId = (int) $userId;
		$state = (int) $state;

		// If there are no primary keys set check to see if the instance key is set.
		if (empty($pks)) {
			if ($this->$k) {
				$pks = array($this->$k);
			}
			// Nothing to set publishing state on, return false.
			else {
				$this->setError(JText::_('JLIB_DATABASE_ERROR_NO_ROWS_SELECTED'));
				return false;
			}
		}

		// Build the WHERE clause for the primary keys.
		$where = $k . '=' . implode(' OR ' . $k . '=', $pks);


		$this->_db->setQuery(
				'UPDATE ' . $this->_db->quoteName($this->_tbl) .
				' SET ' . $this->_db->quoteName('publish') . ' = ' . (int) $state .
				' WHERE (' . $where . ')'
		);

		try {
			$this->_db->execute();
		} catch (RuntimeException $e) {
			$this->setError($e->getMessage());
			return false;
		}

		// If the JTable instance value is in the list of primary keys that were set, set the instance.
		if (in_array($this->$k, $pks)) {
			$this->publish = $state;
		}

		$this->setError('');

		return true;
	}

}
