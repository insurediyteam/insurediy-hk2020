<?php

/**
 * @package     Joomla.Administrator
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 *
 * @package     Joomla.Administrator
 * @since       1.5
 */
class InsurediyReferralViewReferrals extends JViewLegacy {

	protected $items;
	protected $pagination;
	protected $state;

	/**
	 * Display the view
	 *
	 * @return  void
	 */
	public function display($tpl = null) {
		$this->state = $this->get('State');
		$items = $this->get('Items');
		$this->pagination = $this->get('Pagination');

		InsurediyReferralHelper::addSubmenu('referrals');

		$users = $this->getAllUsers();

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
		$users = InsureDIYHelper::getAllUsers("referral_id");
		$users[0]['username'] = JText::_("COM_INSUREDIYREFERRAL_SYSTEM");
		foreach ($items as $item) {
			$item->referred_to = (isset($users[$item->referred_to])) ? $users[$item->referred_to]['username'] : "Undefined User";
			$item->referred_by = (isset($users[$item->referred_by])) ? $users[$item->referred_by]['username'] : "Undefined User";
//			$item->modified_by = (isset($users[$item->modified_by])) ? $users[$item->modified_by]['username'] : "Undefined User";
			$item->created_by = (isset($users[$item->created_by])) ? $users[$item->created_by]['username'] : "Undefined User";
			$quotation = explode(".", $item->quotation);
			$item->quotation = InsureDIYHelper::getInsuranceName($quotation[0]) . " " . "ID : " . $quotation[1];
		}
		$this->items = $items;

		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar() {
		require_once JPATH_COMPONENT . '/helpers/insurediyreferral.php';

		$state = $this->get('State');
		$canDo = InsurediyReferralHelper::getActions($state->get('filter.category_id'));
		$user = JFactory::getUser();
		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');

		JToolbarHelper::title(JText::_('COM_INSUREDIYREFERRAL_REFERRALS_MANAGEMENT'), 'forms.png');

		if ($state->get('filter.status') != 2 && $canDo->get('core.edit')) {
//			JToolbarHelper::addNew('points.add');
			if ($canDo->get('core.edit.state')) {
//				JToolbarHelper::publish('points.publish', 'JTOOLBAR_PUBLISH', true);
//				JToolbarHelper::unpublish('points.unpublish', 'JTOOLBAR_UNPUBLISH', true);
			}
//			JToolbarHelper::editList('points.edit');
			JToolbarHelper::deleteList('', 'referrals.delete', 'COM_INSUREDIYREFERRAL_JTOOLBAR_DELETE_FOREVER');
		}
		if ($state->get('filter.status') == 2 && $canDo->get('core.edit')) {
			JToolbarHelper::deleteList('', 'points.delete', 'COM_INSUREDIYREFERRAL_JTOOLBAR_DELETE_FOREVER');
			JToolBarHelper::custom('points.restore', 'publish.png', 'publish_f2.png', 'COM_INSUREDIYREFERRAL_JTOOLBAR_RESTORE', true);
		}

		if ($state->get('filter.status') == 1 && $canDo->get('core.delete')) {
			
		} elseif ($canDo->get('core.edit.state')) {

		}

		if ($canDo->get('core.admin')) {
			JToolbarHelper::preferences('com_insurediyreferral');
		}

		$filterOptions = array();
		$filterOptions[] = JHtml::_('select.option', '1', 'JPUBLISHED');
		$filterOptions[] = JHtml::_('select.option', '0', 'JUNPUBLISHED');
		$filterOptions[] = JHtml::_('select.option', '2', 'JTRASHED');


		JHtmlSidebar::addFilter(
				JText::_('JOPTION_SELECT_PUBLISHED'), 'filter_status', JHtml::_('select.options', $filterOptions, 'value', 'text', $this->state->get('filter.status'), true)
		);
	}

	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 *
	 * @since   3.0
	 */
	protected function getSortFields() {
		return array(
			'r_id' => JText::_('COM_INSUREDIYREFERRAL_REFERRALS_HEADER_ID'),
			'referred_to' => JText::_('COM_INSUREDIYREFERRAL_REFERRALS_HEADER_REFERRED_TO'),
			'referred_by' => JText::_('COM_INSUREDIYREFERRAL_REFERRALS_HEADER_REFERRED_BY'),
			'quotation' => JText::_('COM_INSUREDIYREFERRAL_REFERRALS_HEADER_QUOTATION'),
			'unique_order_no' => JText::_('COM_INSUREDIYREFERRAL_REFERRALS_HEADER_UON'),
			'created' => JText::_('COM_INSUREDIYREFERRAL_REFERRALS_HEADER_CREATED'),
			'created_by' => JText::_('COM_INSUREDIYREFERRAL_REFERRALS_HEADER_CREATED_BY'),
			'modified' => JText::_('COM_INSUREDIYREFERRAL_REFERRALS_HEADER_MODIFIED'),
			'modified_by' => JText::_('COM_INSUREDIYREFERRAL_REFERRALS_HEADER_MODIFIED_BY')
		);
	}

	protected function getAllUsers() {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$query->select("id,username");
		$query->from("#__users");
		$db->setQuery($query);
		$result = $db->loadAssocList("id");
		$result[0]['username'] = "System";
		return $result;
	}

}
