<?php

/**
 * @package     Joomla.Administrator
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 *
 * @package     Joomla.Administrator
 * @since       1.5
 */
class InsurediyReferralViewPromocodes extends JViewLegacy {

	protected $items;
	protected $pagination;
	protected $state;

	/**
	 * Display the view
	 *
	 * @return  void
	 */
	public function display($tpl = null) {
		$this->state = $this->get('State');
		$items = $this->get('Items');
		$this->pagination = $this->get('Pagination');

		InsurediyReferralHelper::addSubmenu('promocodes');

		$users = $this->getAllUsers();

		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
		foreach ($items as $key => $item) {
			$item->status = JHtml::_('jgrid.published', $item->publish, $key, 'promocodes.', TRUE, 'cb');
			$item->modified_by = (isset($users[$item->modified_by])) ? $users[$item->modified_by]['username'] : "Undefined User";
			$item->created_by = (isset($users[$item->created_by])) ? $users[$item->created_by]['username'] : "Undefined User";
		}
		$this->items = $items;

		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar() {
		require_once JPATH_COMPONENT . '/helpers/insurediyreferral.php';

		$state = $this->get('State');
		$canDo = InsurediyReferralHelper::getActions($state->get('filter.category_id'));
		$user = JFactory::getUser();
		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');

		JToolbarHelper::title(JText::_('COM_INSUREDIYREFERRAL_PROMOCODE_LIST_TITLE'), 'product.png');

		if ($state->get('filter.status') != 2 && $canDo->get('core.edit')) {
			JToolbarHelper::addNew('promocodes.add');
			if ($canDo->get('core.edit.state')) {
				JToolbarHelper::publish('promocodes.publish', 'JTOOLBAR_PUBLISH', true);
				JToolbarHelper::unpublish('promocodes.unpublish', 'JTOOLBAR_UNPUBLISH', true);
			}
			JToolbarHelper::editList('promocodes.edit');
			JToolbarHelper::trash('promocodes.trash');
		}
		if ($state->get('filter.status') == 2 && $canDo->get('core.edit')) {
			JToolbarHelper::deleteList('', 'promocodes.delete', 'JTOOLBAR_EMPTY_TRASH');
			JToolBarHelper::custom('promocodes.restore', 'publish.png', 'publish_f2.png', 'BTN_RESTORE', true);
		}

		if ($state->get('filter.status') == 1 && $canDo->get('core.delete')) {
			
		} elseif ($canDo->get('core.edit.state')) {

		}

		if ($canDo->get('core.admin')) {
			JToolbarHelper::preferences('com_insurediyreferral');
		}

		$filterOptions = array();
		$filterOptions[] = JHtml::_('select.option', '1', 'JPUBLISHED');
		$filterOptions[] = JHtml::_('select.option', '0', 'JUNPUBLISHED');
		$filterOptions[] = JHtml::_('select.option', '2', 'JTRASHED');


		JHtmlSidebar::addFilter(
				JText::_('JOPTION_SELECT_PUBLISHED'), 'filter_status', JHtml::_('select.options', $filterOptions, 'value', 'text', $this->state->get('filter.status'), true)
		);
	}

	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 *
	 * @since   3.0
	 */
	protected function getSortFields() {
		return array(
			'p.id' => JText::_('COM_INSUREDIYREFERRAL_TB_HEADER_ID'),
			'p.name' => JText::_('COM_INSUREDIYREFERRAL_TB_HEADER_NAME'),
			'p.points' => JText::_('COM_INSUREDIYREFERRAL_TB_HEADER_POINTS'),
			'p.publish' => JText::_('COM_INSUREDIYREFERRAL_TB_HEADER_PUBLISH'),
			'p.created' => JText::_('COM_INSUREDIYREFERRAL_TB_HEADER_CREATED'),
			'p.created_by' => JText::_('COM_INSUREDIYREFERRAL_TB_HEADER_CREATED_BY'),
			'p.modified' => JText::_('COM_INSUREDIYREFERRAL_TB_HEADER_MODIFIED'),
			'p.modified_by' => JText::_('COM_INSUREDIYREFERRAL_TB_HEADER_MODIFIED_BY')
		);
	}

	protected function getParent($id = FALSE) {
		if ($id) {
			$db = JFactory::getDbo();
			$query = $db->getQuery(TRUE);
			$query->select("c.company");
			$query->from("#__form_edirectory ed, #__form_booking b, #__form_company c");
			$query->where("ed.owner=b.created_by")->where("b.company_id=c.company_id");
			$query->where("ed.id=" . $db->quote($id));
			$db->setQuery($query);
			$result = $db->loadResult();
			return $result;
		}
		return FALSE;
	}

	protected function getAllUsers() {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$query->select("id,username");
		$query->from("#__users");
		$db->setQuery($query);
		$result = $db->loadAssocList("id");
		return $result;
	}

}
