<?php

/**
 * @package     Joomla.Administrator
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 *
 * @package     Joomla.Administrator
 * @since       1.5
 */
class InsurediyReferralViewPoints extends JViewLegacy {

	protected $items;
	protected $pagination;
	protected $state;

	/**
	 * Display the view
	 *
	 * @return  void
	 */
	public function display($tpl = null) {
		$this->state = $this->get('State');
		$items = $this->get('Items');
		$this->pagination = $this->get('Pagination');

		InsurediyReferralHelper::addSubmenu('points');

		$users = $this->getAllUsers();
		$users[0]['username'] = JText::_("COM_INSUREDIYREFERRAL_SYSTEM");
		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		foreach ($items as $item) {
			$item->user_id = (isset($users[$item->user_id])) ? $users[$item->user_id]['username'] : "Undefined User";
			$item->modified_by = (isset($users[$item->modified_by])) ? $users[$item->modified_by]['username'] : "Undefined User";
			$item->created_by = (isset($users[$item->created_by])) ? $users[$item->created_by]['username'] : "Undefined User";
		}
		$this->items = $items;

		$this->addToolbar();
		$this->sidebar = JHtmlSidebar::render();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar() {
		require_once JPATH_COMPONENT . '/helpers/insurediyreferral.php';

		$state = $this->get('State');
		$canDo = InsurediyReferralHelper::getActions($state->get('filter.category_id'));
		$user = JFactory::getUser();
		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');

		JToolbarHelper::title(JText::_('COM_INSUREDIYREFERRAL_POINTS_MANAGEMENT'), 'forms.png');

		if ($state->get('filter.status') != 2 && $canDo->get('core.edit')) {
			JToolbarHelper::addNew('points.add');
//			if ($canDo->get('core.edit.state')) {
//				JToolbarHelper::publish('points.publish', 'JTOOLBAR_PUBLISH', true);
//				JToolbarHelper::unpublish('points.unpublish', 'JTOOLBAR_UNPUBLISH', true);
//			}
			JToolbarHelper::editList('points.edit');
			JToolbarHelper::trash('points.trash');
		}
		if ($state->get('filter.status') == 2 && $canDo->get('core.edit')) {
			JToolbarHelper::deleteList('', 'points.delete', 'COM_INSUREDIYREFERRAL_JTOOLBAR_DELETE_FOREVER');
			JToolBarHelper::custom('points.restore', 'publish.png', 'publish_f2.png', 'COM_INSUREDIYREFERRAL_JTOOLBAR_RESTORE', true);
		}

		if ($state->get('filter.status') == 1 && $canDo->get('core.delete')) {
			
		} elseif ($canDo->get('core.edit.state')) {

		}

		if ($canDo->get('core.admin')) {
			JToolbarHelper::preferences('com_insurediyreferral');
		}

		$filterOptions = array();
		$filterOptions[] = JHtml::_('select.option', 'P', 'Pending');
		$filterOptions[] = JHtml::_('select.option', 'W', 'Waiting');
		$filterOptions[] = JHtml::_('select.option', 'A', 'Approved');
		$filterOptions[] = JHtml::_('select.option', 'R', 'Rejected');
		//$filterOptions[] = JHtml::_('select.option', '2', 'JTRASHED');


		JHtmlSidebar::addFilter(
				JText::_('JOPTION_SELECT_PUBLISHED'), 'filter_status', JHtml::_('select.options', $filterOptions, 'value', 'text', $this->state->get('filter.status'), true)
		);
	}

	/**
	 * Returns an array of fields the table can be sorted by
	 *
	 * @return  array  Array containing the field name to sort by as the key and display text as value
	 *
	 * @since   3.0
	 */
	protected function getSortFields() {
		return array(
			'point_id' => JText::_('COM_INSUREDIYREFERRAL_POINTS_HEADER_ID'),
			'user_id' => JText::_('COM_INSUREDIYREFERRAL_POINTS_HEADER_USER'),
			'points' => JText::_('COM_INSUREDIYREFERRAL_POINTS_HEADER_POINTS'),
			'created' => JText::_('COM_INSUREDIYREFERRAL_POINTS_HEADER_CREATED'),
			'created_by' => JText::_('COM_INSUREDIYREFERRAL_POINTS_HEADER_CREATED_BY'),
			'modified' => JText::_('COM_INSUREDIYREFERRAL_POINTS_HEADER_MODIFIED'),
			'modified_by' => JText::_('COM_INSUREDIYREFERRAL_POINTS_HEADER_MODIFIED_BY'),
			'status' => JText::_('COM_INSUREDIYREFERRAL_POINTS_HEADER_STATUS')
		);
	}

	protected function getAllUsers() {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$query->select("id,username");
		$query->from("#__users");
		$db->setQuery($query);
		$result = $db->loadAssocList("id");
		$result[0]['username'] = "System";
		return $result;
	}

}
