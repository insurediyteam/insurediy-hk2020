<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_insurediyreferral
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Referral helper.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_insurediyreferral
 * @since       1.6
 */
class InsurediyReferralHelper {

	/**
	 * Configure the Linkbar.
	 *
	 * @param   string	The name of the active view.
	 * @since   1.6
	 */
	public static function addSubmenu($vName = 'forms') {
		JHtmlSidebar::addEntry(
				JText::_('COM_INSUREDIYREFERRAL_SUBMENU_REFERRALS'), 'index.php?option=com_insurediyreferral&view=referrals', $vName == 'referrals'
		);

		JHtmlSidebar::addEntry(
				JText::_('COM_INSUREDIYREFERRAL_SUBMENU_POINTS'), 'index.php?option=com_insurediyreferral&view=points', $vName == 'points'
		);

		JHtmlSidebar::addEntry(
				JText::_('COM_INSUREDIYREFERRAL_SUBMENU_PROMOCODES'), 'index.php?option=com_insurediyreferral&view=promocodes', $vName == 'promocodes'
		);
	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @param   integer  The category ID.
	 * @return  JObject
	 * @since   1.6
	 */
	public static function getActions($categoryId = 0) {
		$user = JFactory::getUser();
		$result = new JObject;

		if (empty($categoryId)) {
			$assetName = 'com_insurediyreferral';
			$level = 'component';
		} else {
			$assetName = 'com_insurediyreferral.category.' . (int) $categoryId;
			$level = 'category';
		}

		$actions = JAccess::getActions('com_insurediyreferral', $level);

		foreach ($actions as $action) {
			$result->set($action->name, $user->authorise($action->name, $assetName));
		}

		return $result;
	}

}
