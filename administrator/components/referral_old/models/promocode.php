<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Transactions model.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 * @since       1.5
 */
class InsurediyReferralModelPromocode extends JModelAdmin {

	/**
	 * @var		string	The prefix to use with controller messages.
	 * @since   1.6
	 */
	protected $text_prefix = 'COM_INSUREDIYREFERRAL';
	protected $data = array();
	protected $iserror_key = "com_insurediyreferral.promocodeform.iserror";
	protected $form_key = "com_insurediyreferral.promocodeform.data";
	protected $id_key = "com_insurediyreferral.promocodeform.id";
	protected $_tb_promocode = "#__insure_promocodes";

	/**
	 * Method to test whether a record can be deleted.
	 *
	 * @param   object	A record object.
	 * @return  boolean  True if allowed to delete the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canDelete($record) {
		if (!empty($record->id)) {
			if ($record->state != -2) {
				return;
			}
			$user = JFactory::getUser();

			if ($record->catid) {
				return $user->authorise('core.delete', 'com_insurediyreferral.category.' . (int) $record->catid);
			} else {
				return parent::canDelete($record);
			}
		}
	}

	/**
	 * Method to test whether a record can have its state changed.
	 *
	 * @param   object	A record object.
	 * @return  boolean  True if allowed to change the state of the record. Defaults to the permission set in the component.
	 * @since   1.6
	 */
	protected function canEditState($record) {
		$user = JFactory::getUser();

		if (!empty($record->catid)) {
			return $user->authorise('core.edit.state', 'com_insurediyreferral.category.' . (int) $record->catid);
		} else {
			return parent::canEditState($record);
		}
	}

	/**
	 * Returns a reference to the a Table object, always creating it.
	 *
	 * @param   type	The table type to instantiate
	 * @param   string	A prefix for the table class name. Optional.
	 * @param   array  Configuration array for model. Optional.
	 * @return  JTable	A database object
	 * @since   1.6
	 */
	public function getTable($type = 'Promocode', $prefix = 'InsurediyReferralTable', $config = array()) {
		return JTable::getInstance($type, $prefix, $config);
	}

	/**
	 * Method to get the record form.
	 *
	 * @param   array  $data		An optional array of data for the form to interogate.
	 * @param   boolean	$loadData	True if the form is to load its own data (default case), false if not.
	 * @return  JForm	A JForm object on success, false on failure
	 * @since   1.6
	 */
	public function getForm($data = array(), $loadData = true) {
		$app = JFactory::getApplication();

		// Get the form.
		$form = $this->loadForm('com_insurediyreferral.promocode', 'promocode', array('control' => 'jform', 'load_data' => $loadData));

		if (empty($form)) {
			return false;
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 * @since   1.6
	 */
	protected function loadFormData() {
		// Check the session for previously entered form data.
		if (empty($this->data)) {
			$data = array();
			$item = $this->getItem();
			foreach ($item as $k => $v) {
				$data[$k] = $this->prepareData($k, $v);
			}

			$this->data = $data;
		}
		$id = JFactory::getApplication()->input->get("id", 0, "integer");
		$this->data['display_id'] = $id;
		$this->preprocessData('com_insurediyreferral.promocode', $this->data);
		return $this->data;
	}

	private function prepareData($k, $v) {
		switch ($k) {
			case "related":
				$r = json_decode($v);
				return MyHelper::object2array($r);
			case "telephone":
			case "fax":
//				return $this->getPhoneData($v);
			default:
				return $v;
		}
	}

	private function getDataFromTable($table) {
		$data = array();
		foreach ($table as $k => $v) {
			if ($k != "_errors") {
				$data[$k] = $v;
			}
		}
		return $data;
	}

	/**
	 * Method to get a single record.
	 *
	 * @param   integer	The id of the primary key.
	 *
	 * @return  mixed  Object on success, false on failure.
	 * @since   1.6
	 */
	public function getItem($pk = null) {
		$item = parent::getItem($pk);
		return $item;
	}

	/**
	 * Prepare and sanitise the table prior to saving.
	 *
	 * @since   1.6
	 */
	protected function prepareTable($table) {
		$date = JFactory::getDate();
		$user = JFactory::getUser();
	}

	/**
	 * A protected method to get a set of ordering conditions.
	 *
	 * @param   object	A record object.
	 * @return  array  An array of conditions to add to add to ordering queries.
	 * @since   1.6
	 */
	protected function getReorderConditions($table) {
		$condition = array();

		return $condition;
	}

	public function validate($form, $data, $group = NULL) {
		$app = JFactory::getApplication();
		$app->setUserState($this->form_key, $data);
		$errorRedirectUrl = "index.php?option=com_insurediyreferral&view=promocode";
		$id = (isset($data['display_id'])) ? $data['display_id'] : FALSE;
		if ($id) {
			$errorRedirectUrl = 'index.php?option=com_insurediyreferral&view=promocode&layout=edit&id=' . $id;
		}
//		if (!isset($data['bp_email'])) {
//			$this->errorRedirect($errorRedirectUrl, JText::_("COM_INSUREDIYREFERRAL_ERR_NO_BP_EMAIL"), 'error', $id);
//		}

		$data = parent::validate($form, $data, $group);
		if (FALSE === $data) {
			return $data;
		}
		if (isset($id)) {
			$data['id'] = $id;
		}
		return $data;
	}

	protected function errorRedirect($url, $msg, $type, $id) {
		$app = JFactory::getApplication();
//		if (!$id) {
		$app->setUserState($this->iserror_key, TRUE);
//		}
		$app->redirect($url, $msg, $type);
	}

	/**
	 * Method to save the form data.
	 *
	 * @param   array  $data  The form data.
	 *
	 * @return  boolean  True on success.
	 *
	 * @since	3.1
	 */
	public function save($data) {
		return $this->saveItem($data);
	}

	protected function saveItem($data = array()) {
		$app = JFactory::getApplication();
		$table = $this->getTable("Promocode", "InsurediyReferralTable");
		$tmp = array();
		foreach ($table->getFields() as $k => $f) {
			if (isset($data[$k])) {
				$tmp[$k] = $data[$k];
			}
		}
		$table->bind(MyHelper::array2jObject($tmp));

		$result = $table->store();
		$app->setUserState($this->id_key, $table->id);
		if ($result) {
			return $table->id;
		}
		return $result;
	}

	protected function saveCompany($data = array()) {
		$table = $this->getTable("Company", "InsurediyReferralTable");
		$tmp = array();
		foreach ($table->getFields() as $k => $f) {
			if (isset($data[$k])) {
				$tmp[$k] = $data[$k];
			}
		}
		$table->bind(MyHelper::array2jObject($tmp));
		$table->store();
		return $table->company_id;
	}

	protected function getInterestData($param) {
		$interests = MyHelper::object2array(json_decode($param));
		$tmp = array();
		foreach ($interests as $k => $i) {
			$tmp[] = $k;
		}
		return $tmp;
	}

}
