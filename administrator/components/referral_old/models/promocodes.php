<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Methods supporting a list of weblink records.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 * @since       1.6
 */
class InsurediyReferralModelPromocodes extends JModelList {

	/**
	 * Constructor.
	 *
	 * @param   array  An optional associative array of configuration settings.
	 * @see     JController
	 * @since   1.6
	 */
	protected $_tb_promocode = "#__insure_promocodes";

	public function __construct($config = array()) {
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'id', 'p.id',
				'code', 'p.code',
				'uses', 'p.uses',
				'percent', 'p.percent',
				'start_date', 'p.start_date',
				'end_date', 'p.end_date',
				'modified', 'p.modified',
				'modified_by', 'p.modified_by',
				'created_by', 'p.created_by',
				'trash', 'p.trash',
				'publish', 'p.publish'
			);
		}

		parent::__construct($config);
	}

	/**
	 * Method to auto-populate the model state.
	 *
	 * Note. Calling getState in this method will result in recursion.
	 *
	 * @since   1.6
	 */
	protected function populateState($ordering = null, $direction = null) {
		$app = JFactory::getApplication('administrator');

		// Load the filter state.
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$accessId = $this->getUserStateFromRequest($this->context . '.filter.access', 'filter_access', null, 'int');
		$this->setState('filter.access', $accessId);

		// Load the filter state.
		$status = $this->getUserStateFromRequest($this->context . '.filter.status', 'filter_status');
		if ($status == "1" || $status == "0" || $status == "2") {
			$this->setState('filter.status', $status);
		}

		// Load the parameters.
		$params = JComponentHelper::getParams('com_insurediyreferral');
		$this->setState('params', $params);

		// List state information.
		parent::populateState("p.id", "asc");
	}

	/**
	 * Method to get a store id based on model configuration state.
	 *
	 * This is necessary because the model is used by the component and
	 * different modules that might need different sets of data or different
	 * ordering requirements.
	 *
	 * @param   string  $id    A prefix for the store id.
	 * @return  string  A store id.
	 * @since   1.6
	 */
	protected function getStoreId($id = '') {
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.access');

		return parent::getStoreId($id);
	}

	/**
	 * Build an SQL query to load the list data.
	 *
	 * @return  JDatabaseQuery
	 * @since   1.6
	 */
	protected function getListQuery() {
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);

		// Select the required fields from the table.
		$query->select('*');
		$query->from($db->quoteName($this->_tb_promocode) . ' AS p');

		// Filter by search
		$search = $this->getState('filter.search');
		if (!empty($search)) {
			$search = $db->quote('%' . $db->escape($search, true) . '%');
			$query->where('p.code LIKE ' . $search);
		}
		// Filter by state
		$status = $this->getState('filter.status');
		if ($status == "1" || $status == "0") {
			$query->where('p.publish=' . $db->quote($status));
		}
		if ($status == "2") {
			$query->where('p.trash=' . $db->quote(1)); // 1 = trashed
		} else {
			$query->where('p.trash=' . $db->quote(0)); // 0 = active
		}
		// Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering');
		$orderDirn = $this->state->get('list.direction');

		$query->order($db->escape($orderCol . ' ' . $orderDirn));
		return $query;
	}

	function trash() {
		$app = JFactory::getApplication();
		$cid = JRequest::getVar('cid');
		JArrayHelper::toInteger($cid);
		foreach ($cid as $id) {
			$table = JTable::getInstance('Promocode', 'InsurediyReferralTable');
			$table->load($id);
			$table->trash = 1;
			$table->publish = 0;
			$table->store();
		}
		$app->redirect('index.php?option=com_insurediyreferral&view=promocodes', JText::_('COM_INSUREDIYREFERRAL_MSG_ITEMS_MOVED_TO_TRASH'));
	}

	function restore() {
		$app = JFactory::getApplication();
		$cid = JRequest::getVar('cid');
		JArrayHelper::toInteger($cid);
		foreach ($cid as $id) {
			$table = JTable::getInstance('Promocode', 'InsurediyReferralTable');
			$table->load($id);
			$table->trash = 0;
			$table->store();
		}
		$app->redirect('index.php?option=com_insurediyreferral&view=promocodes', JText::_('COM_INSUREDIYREFERRAL_MSG_ITEMS_RESTORED'));
	}

	function delete() {
		$app = JFactory::getApplication();
		$cid = JRequest::getVar('cid');
		JArrayHelper::toInteger($cid);
		foreach ($cid as $id) {
			//TO-DO: Delete image
			$table = JTable::getInstance('Promocode', 'InsurediyReferralTable');
			if ($table->load($id)) {
				$table->delete($id);
			}
		}
		$app->redirect('index.php?option=com_insurediyreferral&view=promocodes', JText::_('COM_INSUREDIYREFERRAL_MSG_ITEMS_DELETED'));
	}

	function publish() {
		$app = JFactory::getApplication();
		$cid = JRequest::getVar('cid');
		foreach ($cid as $id) {
			$row = JTable::getInstance('Promocode', 'InsurediyReferralTable');
			$row->load($id);
			$row->publish($id, 1);
		}
		$app->redirect('index.php?option=com_insurediyreferral&view=promocodes');
	}

	function unpublish() {
		$app = JFactory::getApplication();
		$cid = JRequest::getVar('cid');
		foreach ($cid as $id) {
			$row = JTable::getInstance('Promocode', 'InsurediyReferralTable');
			$row->load($id);
			$row->publish($id, 0);
		}
		$app->redirect('index.php?option=com_insurediyreferral&view=promocodes');
	}

}
