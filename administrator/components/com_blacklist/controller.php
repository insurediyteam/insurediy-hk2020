<?php

defined('_JEXEC') or die;

class BlacklistController extends JControllerLegacy {

	protected $default_view = 'items';

	public function display($cachable = false, $urlparams = false) {

		$view = $this->input->get('view', 'banks');
		$layout = $this->input->get('layout', 'default');
		$id = $this->input->getInt('id');

		parent::display();

		return $this;
	}

}
