<?php

defined('_JEXEC') or die;

class BlacklistViewCSVExport extends JViewLegacy {

	protected $state;
	protected $item;
	protected $form;

	/**
	 * Display the view
	 */
	public function display($tpl = null) {
		$this->state = $this->get('State');
		$this->item = $this->get('Item');
		$this->form = $this->get('Form');

		BlacklistHelper::addSubmenu('csvexport');
		$this->sidebar = JHtmlSidebar::render();

		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar() {
		require_once JPATH_COMPONENT . '/helpers/blacklist.php';

		$state = $this->get('State');
		$canDo = BlacklistHelper::getActions($state->get('filter.category_id'));

		JToolbarHelper::title(JText::_('COM_BLACKLIST_CSV_EXPORT'), 'insurediylife.png');


		if ($canDo->get('core.admin')) {
			JToolbarHelper::preferences('com_blacklist');
		}

		JHtmlSidebar::setAction('index.php?option=com_blacklist&view=csvexport');
	}

}
