<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_CedeleDepots
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * View to edit a weblink.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_CedeleDepots
 * @since       1.5
 */
class BlacklistViewItem extends JViewLegacy {

	protected $state;
	protected $item;
	protected $form;

	/**
	 * Display the view
	 */
	public function display($tpl = null) {
		$this->state = $this->get('State');
		$this->item = $this->get('Item');
		$this->form = $this->get('Form');

		$db = JFactory::getDBO();

		// Check for errors.
		if (empty($this->item)) {
			JError::raiseError(500, "No Bank");
			return false;
		}

		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar() {
		JFactory::getApplication()->input->set('hidemainmenu', true);

		$user = JFactory::getUser();
		$userId = $user->get('id');
		$isNew = ($this->item->id == 0);
		if ($isNew) {
			JToolbarHelper::title(JText::_('COM_BLACKLIST_MANAGER_NEW_ITEM'), 'insurediylife.png');
		} else {
			JToolbarHelper::title(JText::_("COM_BLACKLIST_MANAGER_EDIT") . " " . $this->item->name, 'insurediylife.png');

		}

		// Since it's an existing record, check the edit permission, or fall back to edit own if the owner.
		JToolbarHelper::apply('item.apply');
		JToolbarHelper::save('item.save');

		// If not checked out, can save the item.
		if (empty($this->item->id)) {
			JToolbarHelper::cancel('item.cancel');
		} else {
			JToolbarHelper::cancel('item.cancel', 'JTOOLBAR_CLOSE');
		}
	}

}
