SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";
SET FOREIGN_KEY_CHECKS=0;
DROP TABLE IF EXISTS `#__insure_promocodes`;
CREATE TABLE IF NOT EXISTS `#__insure_promocodes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(255) NOT NULL,
  `uses` int(11) NOT NULL,
  `promo_type` enum('public','private','restricted') NOT NULL DEFAULT 'public',
  `promo_method` enum('percentage','absolute','mixed') NOT NULL DEFAULT 'percentage',
  `promo_value` decimal(15,2) NOT NULL,
  `max_value` float(11,2) NOT NULL DEFAULT '10.00',
  `minimum_spend` float(11,2) NOT NULL,
  `usability` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Flexibility on promo code usage',
  `check_new_user` tinyint(1) NOT NULL DEFAULT '0',
  `expiry_of` tinyint(2) NOT NULL DEFAULT '1',
  `applicable_products` varchar(255) NOT NULL DEFAULT 'life',
  `check_bought` tinyint(1) NOT NULL DEFAULT '0',
  `prerequisite_sale` enum('none','travel','life','domestic','domesticph') NOT NULL DEFAULT 'none',
  `used_by` text NOT NULL,
  `specified_users` varchar(255) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `created` datetime NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified` datetime NOT NULL,
  `modified_by` int(11) NOT NULL,
  `trash` tinyint(4) NOT NULL,
  `publish` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

--
-- Dumping data for table `#__insure_promocodes`
--

INSERT INTO `#__insure_promocodes` (`id`, `code`, `uses`, `promo_type`, `promo_method`, `promo_value`, `max_value`, `minimum_spend`, `usability`, `check_new_user`, `expiry_of`, `applicable_products`, `check_bought`, `prerequisite_sale`, `used_by`, `specified_users`, `start_date`, `end_date`, `created`, `created_by`, `modified`, `modified_by`, `trash`, `publish`) VALUES
(1, 'TEST_PROMO_CODE', 5, 'public', 'absolute', 20.00, 0.00, 50.00, 1, 1, 2, 'travel,life,domestic,domesticph', 0, 'none', '', '', '2018-07-01', '2018-09-05', '2018-08-23 11:11:43', 215, '2018-09-07 01:20:05', 871, 0, 1),
(2, 'ENJOY10', 9999, 'public', 'absolute', 10.00, 0.00, 40.00, 0, 1, 1, 'travel,domestic', 0, 'none', '[{"user_id":"8606","promo_type":"domestic","quotation_id":2745}]', '', '2017-11-01', '2018-11-20', '2018-08-23 11:11:43', 215, '2018-09-07 01:14:20', 871, 0, 1),
(3, 'TRAVEL10', 9999, 'public', 'percentage', 10.00, 0.00, 65.00, 0, 0, 12, 'travel', 1, 'domestic', '', 'ajs6683@yahoo.com,bunbo.milo@gmail.com', '2018-01-01', '2018-01-01', '2018-08-23 11:11:43', 215, '0000-00-00 00:00:00', 215, 0, 1),
(4, 'ENJOY5', 9999, 'public', 'absolute', 5.00, 0.00, 30.00, 0, 0, 1, 'domestic', 0, 'none', '', '', '2018-05-03', '2018-05-03', '2018-08-23 11:11:43', 215, '0000-00-00 00:00:00', 215, 0, 0),
(5, 'DHI10', 1, 'private', 'mixed', 10.00, 30.00, 65.00, 1, 0, 1, 'domestic', 1, 'travel', '', '', '2018-07-05', '2018-07-05', '2018-08-23 11:11:43', 215, '0000-00-00 00:00:00', 215, 0, 1),
(6, 'ONE_TRAVEL', 10, 'public', 'percentage', 10.00, 5.00, 50.00, 0, 1, 1, 'travel', 0, 'none', '', '', '2018-07-10', '2018-07-10', '2018-08-23 11:11:43', 215, '2018-08-28 10:14:15', 215, 0, 1),
(7, 'special10', 1, 'restricted', 'percentage', 50.00, 10.00, 60.00, 0, 1, 12, 'travel,life,domestic,domesticph', 0, 'none', '', 'anthony_sukanto1@hotmail.com', '2018-07-10', '2018-07-10', '2018-08-23 11:11:43', 215, '0000-00-00 00:00:00', 215, 0, 1),
(8, 'REPEAT10', 2, 'public', 'absolute', 10.00, 0.00, 40.00, 1, 0, 1, 'travel', 0, 'none', '', '', '2018-07-04', '2018-07-04', '2018-08-23 11:11:43', 215, '0000-00-00 00:00:00', 215, 0, 1),
(9, 'ZERO123', 99, 'public', 'absolute', 20.00, 25.00, 50.00, 0, 1, 1, 'travel,life,domestic,domesticph', 0, 'travel', '', 'test@test.com', '2018-07-10', '2018-07-10', '2018-08-23 11:11:43', 215, '0000-00-00 00:00:00', 215, 0, 1),
(10, 'Promo', 0, 'public', 'percentage', 20.00, 0.00, 0.00, 0, 0, 1, 'life', 0, 'none', '', '', '1970-01-01', '1970-01-01', '2018-08-23 11:11:43', 215, '0000-00-00 00:00:00', 215, 0, 0),
(11, '', 0, 'public', 'percentage', 20.00, 0.00, 0.00, 0, 0, 1, 'life', 0, 'none', '', '', '1970-01-01', '1970-01-01', '2018-08-23 11:11:43', 215, '0000-00-00 00:00:00', 215, 0, 0),
(12, 'MOON123', 1, 'private', 'absolute', 20.00, 10.00, 50.00, 1, 1, 1, 'travel,life,domestic,domesticph', 0, 'none', '', 'test@test.com,abc@test.com', '2018-07-01', '2018-07-01', '2018-08-23 11:11:43', 215, '0000-00-00 00:00:00', 215, 0, 1),
(13, 'TRY_TWO', 2, 'public', 'percentage', 10.00, 0.00, 50.00, 0, 0, 12, 'travel', 0, 'none', '', '', '2018-07-13', '2018-07-13', '2018-08-23 11:11:43', 215, '0000-00-00 00:00:00', 215, 0, 1),
(14, 'ABSOLUTE_DOMESTIC', 4, 'public', 'absolute', 10.00, 0.00, 150.00, 1, 0, 1, 'domestic', 1, 'travel', '', '', '2018-07-14', '2018-07-14', '2018-08-23 11:11:43', 215, '0000-00-00 00:00:00', 215, 0, 1),
(15, 'TAKE5OFF', 3, 'public', 'mixed', 10.00, 10.00, 65.00, 0, 1, 1, 'travel,domestic', 0, 'none', '', ',', '2018-07-13', '2018-07-13', '2018-08-23 11:11:43', 215, '0000-00-00 00:00:00', 215, 0, 1),
(16, 'SPECIAL_FOR_YOU', 5, 'restricted', 'percentage', 10.00, 0.00, 40.00, 0, 0, 1, 'travel,life,domestic,domesticph', 0, 'none', '', 'candywko@gmail.com,ajs6683@yahoo.com,v.vanessa189@yahoo.com.sg,anthony_sukanto1@hotmail.com,test@test.com,ajunizar@gmail.com', '2018-07-13', '2018-08-31', '2018-08-23 11:11:43', 215, '2018-08-27 03:45:29', 215, 0, 1),
(17, 'OLDTEST', 99, 'public', 'absolute', 20.00, 0.00, 40.00, 0, 1, 1, 'life,domestic', 0, 'none', '', ',', '2018-07-12', '2018-07-12', '2018-08-23 11:11:43', 215, '0000-00-00 00:00:00', 215, 0, 1),
(18, 'TWO_TRY', 2, 'public', 'percentage', 10.00, 0.00, 50.00, 0, 0, 1, 'travel', 0, 'none', '', ',', '2018-08-02', '2018-08-02', '2018-08-23 11:11:43', 215, '0000-00-00 00:00:00', 215, 0, 1),
(19, 'TRY_2', 3, 'public', 'percentage', 10.00, 0.00, 50.00, 0, 0, 1, 'travel', 0, 'none', '', ',', '2018-08-01', '2018-08-01', '2018-08-23 11:11:43', 215, '0000-00-00 00:00:00', 215, 0, 1),
(20, 'ABSOLUTE_DOM', 5, 'public', 'absolute', 10.00, 0.00, 150.00, 1, 0, 1, 'domestic', 1, 'travel', '', ',', '2018-08-02', '2018-08-02', '2018-08-23 11:11:43', 215, '0000-00-00 00:00:00', 215, 0, 1),
(21, 'NEW_TRY_TWO', 2, 'public', 'percentage', 10.00, 0.00, 50.00, 0, 0, 1, 'travel', 0, 'none', '[{"user_id":"8589","promo_type":"travel","quotation_id":12720},{"user_id":"8602","promo_type":"travel","quotation_id":12723}]', ',', '2018-08-23', '2018-08-25', '2018-08-23 11:11:43', 215, '0000-00-00 00:00:00', 215, 0, 1),
(22, 'NEW_ABSOLUTE_DOMESTIC', 6, 'public', 'absolute', 10.00, 0.00, 150.00, 1, 0, 1, 'domestic', 1, 'travel', '[{"user_id":"8603","promo_type":"domestic","quotation_id":2734},{"user_id":"8589","promo_type":"domestic","quotation_id":2735},{"user_id":"8589","promo_type":"domestic","quotation_id":2736},{"user_id":"5204","promo_type":"domestic","quotation_id":2737},{"user_id":"5204","promo_type":"domestic","quotation_id":2744}]', '', '2018-08-23', '2018-09-05', '2018-08-23 11:11:43', 215, '2018-09-02 15:38:32', 215, 0, 1),
(23, 'NEW_TAKE5OFF', 3, 'public', 'mixed', 10.00, 10.00, 65.00, 0, 1, 1, 'travel,domestic', 0, 'none', '[{"user_id":"8603","promo_type":"travel","quotation_id":12726}]', ',', '2018-08-23', '2018-08-25', '2018-08-23 11:11:43', 215, '0000-00-00 00:00:00', 215, 0, 1),
(24, 'NEW_SPECIAL_FOR_YOU', 5, 'restricted', 'absolute', 10.00, 0.00, 40.00, 0, 0, 1, 'travel,life,domestic,domesticph', 0, 'none', '[{"user_id":"5204","promo_type":"domestic","quotation_id":2740},{"user_id":"1799","promo_type":"travel","quotation_id":12757}]', 'limshujun@gmail.com,candywko@gmail.com,ajs6683@yahoo.com,v.vanessa189@yahoo.com.sg,test@test.com,ajunizar@gmail.com,shujuntest@gmail.com', '2018-08-15', '2019-07-12', '2018-08-23 11:11:43', 215, '2018-09-06 16:56:39', 871, 0, 1),
(25, 'RESTRICTED', 5, 'restricted', 'mixed', 10.00, 15.00, 50.00, 0, 0, 1, 'travel,domestic,domesticph', 0, 'none', '[{"user_id":"5204","promo_type":"travel","quotation_id":12742}]', 'shujun.lim@insurediy.com,v.vanessa189@yahoo.com.sg,test2@hotmail.com', '2018-09-01', '2018-09-30', '2018-09-02 12:30:34', 215, '2018-09-02 12:31:12', 215, 0, 1);

SET FOREIGN_KEY_CHECKS=1;