<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_insurebanks
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

/**
 *
 * @package     Joomla.Administrator
 * @subpackage  com_insurebanks
 * @since       1.5
 */
class InsureBanksController extends JControllerLegacy
{
	protected $default_view = 'banks';

	public function display($cachable = false, $urlparams = false)
	{

		$view   = $this->input->get('view', 'banks');
		$layout = $this->input->get('layout', 'default');
		$id     = $this->input->getInt('id');
			
		parent::display();

		return $this;
		
	}
}
