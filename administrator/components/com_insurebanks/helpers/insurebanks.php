<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Transactions helper.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_Transactions
 * @since       1.6
 */
class InsureBanksHelper {

	/**
	 * Configure the Linkbar.
	 *
	 * @param   string	The name of the active view.
	 * @since   1.6
	 */
	public static function addSubmenu($vName = 'banks') {
		JHtmlSidebar::addEntry(
				JText::_('COM_INSUREBANKS_SUBMENU_BANKS'), 'index.php?option=com_insurebanks&view=banks', $vName == 'banks'
		);
		JHtmlSidebar::addEntry(
				JText::_('COM_INSUREBANKS_SUBMENU_BRANCHES'), 'index.php?option=com_insurebanks&view=branches', $vName == 'branches'
		);
		JHtmlSidebar::addEntry(
				JText::_('COM_INSUREBANKS_SUBMENU_IMPORT'), 'index.php?option=com_insurebanks&view=csvimport', $vName == 'csvimport'
		);
		JHtmlSidebar::addEntry(
				JText::_('COM_INSUREBANKS_SUBMENU_EXPORT'), 'index.php?option=com_insurebanks&view=csvexport', $vName == 'csvexport'
		);
	}

	/**
	 * Gets a list of the actions that can be performed.
	 *
	 * @param   integer  The category ID.
	 * @return  JObject
	 * @since   1.6
	 */
	public static function getActions($categoryId = 0) {
		$user = JFactory::getUser();
		$result = new JObject;

		if (empty($categoryId)) {
			$assetName = 'com_insurebanks';
			$level = 'component';
		} else {
			$assetName = 'com_insurebanks.category.' . (int) $categoryId;
			$level = 'category';
		}

		$actions = JAccess::getActions('com_insurebanks', $level);

		foreach ($actions as $action) {
			$result->set($action->name, $user->authorise($action->name, $assetName));
		}

		return $result;
	}

	public static function getInsuranceCompanyOptions() {
		$db = JFactory::getDBO();

		$options = array();

		$query = " SELECT * FROM #__insure_banks ";
		$db->setQuery($query);
		$rows = $db->loadObjectList();

		foreach ($rows as $r) :
			$options[] = JHtml::_('select.option', $r->insurer_code, $r->company_name);
		endforeach;

		return $options;
	}

}
