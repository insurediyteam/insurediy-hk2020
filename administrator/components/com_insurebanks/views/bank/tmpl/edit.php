<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_cedeledepots
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
//JHtml::_('formbehavior.chosen', 'select');
?>
<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'bank.cancel' || document.formvalidator.isValid(document.id('bank-form')))
		{
<?php //echo $this->form->getField('description')->save();   ?>
			Joomla.submitform(task, document.getElementById('bank-form'));
		}
	}
</script>

<form action="<?php echo JRoute::_('index.php?option=com_insurebanks'); ?>" method="post" name="adminForm" id="bank-form" class="form-validate">
	<div class="row-fluid">
		<div class="span10 form-horizontal">
			<fieldset>
				<?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'info_details')); ?>
				<?php echo JHtml::_('bootstrap.addTab', 'myTab', 'info_details', JText::_('COM_INSUREBANKS_BANK_DETAIL')); ?>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('id'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('id'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('name'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('name'); ?></div>
				</div>
				<div class="control-group">
					<div class="control-label"><?php echo $this->form->getLabel('code'); ?></div>
					<div class="controls"><?php echo $this->form->getInput('code'); ?></div>
				</div>
				<?php echo JHtml::_('bootstrap.endTab'); ?>

				<input type="hidden" name="task" value="" />
				<?php echo JHtml::_('form.token'); ?>

				<?php echo JHtml::_('bootstrap.endTabSet'); ?>
			</fieldset>
		</div>
		<!-- End Weblinks -->
		<!-- Begin Sidebar -->
		<?php //echo JLayoutHelper::render('joomla.edit.details', $this);  ?>
		<!-- End Sidebar -->
	</div>
</form>
