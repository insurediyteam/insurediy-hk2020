<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_cedeledepots
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

//JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');
JHtml::_('bootstrap.tooltip');
JHTML::_('behavior.tooltip');
JHTML::_('behavior.modal');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$user = JFactory::getUser();
$userId = $user->get('id');
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
$canOrder = $user->authorise('core.edit.state', 'com_insurebanks.category');
$saveOrder = $listOrder == 'a.ordering';
if ($saveOrder) {
	$saveOrderingUrl = 'index.php?option=com_insurebanks&task=branches.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'quotationsList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}
$sortFields = $this->getSortFields();
?>
<script type="text/javascript">
	Joomla.orderTable = function()
	{
		table = document.getElementById("sortTable");
		direction = document.getElementById("directionTable");
		order = table.options[table.selectedIndex].value;
		if (order != '<?php echo $listOrder; ?>')
		{
			dirn = 'asc';
		}
		else
		{
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, '');
	}
</script>
<form action="<?php echo JRoute::_('index.php?option=com_insurebanks&view=branches'); ?>" method="post" name="adminForm" id="adminForm">
	<?php if (!empty($this->sidebar)) : ?>
		<div id="j-sidebar-container" class="span2">
			<?php echo $this->sidebar; ?>
		</div>
		<div id="j-main-container" class="span10">
		<?php else : ?>
			<div id="j-main-container">
			<?php endif; ?>
			<div id="filter-bar" class="btn-toolbar">
				<div class="filter-search btn-group pull-left">
					<label for="filter_search" class="element-invisible"><?php echo JText::_('COM_INSUREBANKS_SEARCH_IN_TITLE'); ?></label>
					<input type="text" name="filter_search" id="filter_search" placeholder="<?php echo JText::_('COM_INSUREBANKS_SEARCH_IN_TITLE'); ?>" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" title="<?php echo JText::_('COM_INSUREBANKS_SEARCH_IN_TITLE'); ?>" />
				</div>
				<div class="btn-group pull-left">
					<button class="btn hasTooltip" type="submit" title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
					<button class="btn hasTooltip" type="button" title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>" onclick="document.id('filter_search').value = '';
							this.form.submit();"><i class="icon-remove"></i></button>
				</div>
				<div class="btn-group pull-right hidden-phone">
					<label for="limit" class="element-invisible"><?php echo JText::_('JFIELD_PLG_SEARCH_SEARCHLIMIT_DESC'); ?></label>
					<?php echo $this->pagination->getLimitBox(); ?>
				</div>
				<div class="btn-group pull-right hidden-phone">
					<label for="directionTable" class="element-invisible"><?php echo JText::_('JFIELD_ORDERING_DESC'); ?></label>
					<select name="directionTable" id="directionTable" class="input-medium" onchange="Joomla.orderTable()">
						<option value=""><?php echo JText::_('JFIELD_ORDERING_DESC'); ?></option>
						<option value="asc" <?php if ($listDirn == 'asc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_ASCENDING'); ?></option>
						<option value="desc" <?php if ($listDirn == 'desc') echo 'selected="selected"'; ?>><?php echo JText::_('JGLOBAL_ORDER_DESCENDING'); ?></option>
					</select>
				</div>
				<div class="btn-group pull-right">
					<label for="sortTable" class="element-invisible"><?php echo JText::_('JGLOBAL_SORT_BY'); ?></label>
					<select name="sortTable" id="sortTable" class="input-medium" onchange="Joomla.orderTable()">
						<option value=""><?php echo JText::_('JGLOBAL_SORT_BY'); ?></option>
						<?php echo JHtml::_('select.options', $sortFields, 'value', 'text', $listOrder); ?>
					</select>
				</div>
			</div>
			<div class="clearfix"> </div>
			<table class="table table-striped" id="quotationsList">
				<thead>
					<tr>
						<th width="1%" class="title hidden-phone">
							<input type="checkbox" name="checkall-toggle" value="" title="<?php echo JText::_('JGLOBAL_CHECK_ALL'); ?>" onclick="Joomla.checkAll(this)" />
						</th>
						<th width="1%" class="title hidden-phone">
							<?php echo JHtml::_('grid.sort', 'COM_INSUREBANKS_HEADER_BRANCH_ID', 'c.id', $listDirn, $listOrder); ?>
						</th>
						<th width="20%" class="title nowrap">
							<?php echo JHtml::_('grid.sort', 'COM_INSUREBANKS_HEADER_BRANCH_NAME', 'c.branch_name', $listDirn, $listOrder); ?>
						</th>
						<th class="title nowrap">
							<?php echo JHtml::_('grid.sort', 'COM_INSUREBANKS_HEADER_BRANCH_CODE', 'c.branch_code', $listDirn, $listOrder); ?>
						</th>
						<th width="55%" class="title center nowrap hidden-phone">
							<?php echo JHtml::_('grid.sort', 'COM_INSUREBANKS_HEADER_BRANCH_ADDRESS', 'c.address', $listDirn, $listOrder); ?>
						</th>
						<th width="15%" class="title nowrap hidden-phone">
							<?php echo JHtml::_('grid.sort', 'COM_INSUREBANKS_HEADER_BRANCH_TEL', 'c.tel', $listDirn, $listOrder); ?>
						</th>
						<th class="title nowrap center">
							<?php echo JHtml::_('grid.sort', 'COM_INSUREBANKS_HEADER_BANK_CODE', 'c.code', $listDirn, $listOrder); ?>
						</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<td colspan="10">
							<?php echo $this->pagination->getListFooter(); ?>
						</td>
					</tr>
				</tfoot>
				<tbody>
					<?php
					foreach ($this->items as $i => $item) :
						//echo print_r($item);
						$ordering = ($listOrder == 'a.ordering');
						$item->branch_link = JRoute::_('index.php?option=com_insurebanks&view=branch&layout=edit&id=' . $item->id);
						$canCreate = $user->authorise('core.create', 'com_insurebanks.category.' . $item->id);
						$canEdit = $user->authorise('core.edit', 'com_insurebanks.category.' . $item->id);
						$canCheckin = $user->authorise('core.manage', 'com_checkin') || $item->checked_out == $user->get('id') || $item->checked_out == 0;
						$canChange = $user->authorise('core.edit.state', 'com_insurebanks.category.' . $item->id) && $canCheckin;
						?>
						<tr class="row<?php echo $i % 2; ?>" >
							<td class="center hidden-phone">
								<?php echo JHtml::_('grid.id', $i, $item->id); ?>
							</td>
							<td class="center hidden-phone">
								<?php echo ($item->id) ? $item->id : 'N/A'; ?>
							</td>
							<td class="has-context">
								<a href="<?php echo $item->branch_link ?>"><?php echo ($item->branch_name) ? $item->branch_name : 'N/A'; ?></a>
							</td>
							<td class="has-context">
								<?php echo ($item->branch_code) ? $item->branch_code : 'N/A'; ?>
							</td>
								<td class="has-context hidden-phone">
								<?php echo ($item->address) ? $item->address : 'N/A'; ?>
							</td>
							<td class="nowrap has-context hidden-phone">
								<?php echo ($item->tel) ? $item->tel : 'N/A'; ?>
							</td>
								<td class="nowrap has-context">
								<?php echo ($item->code) ? $item->code : 'N/A'; ?>
							</td>
						</tr>
					<?php endforeach; ?>
				</tbody>
			</table>

			<?php //Load the batch processing form.  ?>
			<?php //echo $this->loadTemplate('batch'); ?>

			<input type="hidden" name="task" value="" />
			<input type="hidden" name="boxchecked" value="0" />
			<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
			<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
			<?php echo JHtml::_('form.token'); ?>
		</div>
</form>
