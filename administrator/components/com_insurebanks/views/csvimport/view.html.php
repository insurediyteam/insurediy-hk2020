<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_CedeleDepots
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * View to edit a weblink.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_CedeleDepots
 * @since       1.5
 */
class InsureBanksViewCSVImport extends JViewLegacy {

	protected $state;
	protected $item;
	protected $form;

	/**
	 * Display the view
	 */
	public function display($tpl = null) {
		$this->state = $this->get('State');
		//$this->item		= $this->get('Item');
		$this->form = $this->get('Form');

		InsureBanksHelper::addSubmenu('csvimport');
		$this->sidebar = JHtmlSidebar::render();

		$this->addToolbar();
		parent::display($tpl);
	}

	/**
	 * Add the page title and toolbar.
	 *
	 * @since   1.6
	 */
	protected function addToolbar() {
//		JFactory::getApplication()->input->set('hidemainmenu', true);
		require_once JPATH_COMPONENT . '/helpers/insurebanks.php';
		$state = $this->get('State');
		$canDo = InsureBanksHelper::getActions($state->get('filter.category_id'));
		$user = JFactory::getUser();
		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');
		JToolbarHelper::title(JText::_('COM_INSUREBANKS_CSV_IMPORT'), 'insurediylife.png');

		// If not checked out, can save the item.
//		if (empty($this->item->id)) {
//			JToolbarHelper::cancel('csvimport.cancel');
//		} else {
//			JToolbarHelper::cancel('csvimport.cancel', 'JTOOLBAR_CLOSE');
//		}

		/*
		  JToolbarHelper::divider();
		  JToolbarHelper::help('JHELP_COMPONENTS_CedeleDepotS_LINKS_EDIT');
		 */

		if ($canDo->get('core.admin')) {
			JToolbarHelper::preferences('com_insurebanks');
		}

		JHtmlSidebar::setAction('index.php?option=com_insurebanks&view=csvimport');
	}

}
