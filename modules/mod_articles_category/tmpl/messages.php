<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_category
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
?>
<?php if ($grouped) : ?>
	<?php foreach ($list as $group_name => $group) : ?>
		<?php foreach ($group as $item) : ?>
			<div class="message-list">
				<div class="message-date">
					<?php if ($item->displayDate) : ?>
						<span class="mod-articles-category-date"><?php echo $item->displayDate; ?></span>
					<?php endif; ?>

				</div>
				<div class="message-body">
					<?php if ($params->get('show_introtext')) : ?>
						<p class="mod-articles-category-introtext">
							<?php echo $item->displayIntrotext; ?>
						</p>
					<?php endif; ?>
				</div>
				<div class="clear"></div>
			</div>
		<?php endforeach; ?>
	<?php endforeach; ?>
<?php else : ?>
	<?php foreach ($list as $item) :
		?>
		<div class="message-list">
			<div class="message-date">
				<?php if ($item->displayDate) : ?>
					<span class="mod-articles-category-date"><?php echo $item->displayDate; ?></span>
				<?php endif; ?>
			</div>
			<div class="message-body">
				<?php if ($params->get('show_introtext')) : ?>
					<p class="mod-articles-category-introtext">
						<?php echo $item->displayIntrotext; ?>
					</p>
				<?php endif; ?>
			</div>
			<div class="clear"></div>
		</div>
		<?php if ($item != end($list)): ?>
			<div class="message-seperator"></div>
		<?php endif; ?>

	<?php endforeach; ?>
<?php endif; ?>
