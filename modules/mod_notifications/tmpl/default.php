<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_login
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('bootstrap.tooltip');
?>
<div class="scrolled-box">
	<?php foreach ($list as $item) :
		?>
		<div class="message-list">
			<div class="message-date">
				<?php if ($item->date_time) : ?>
					<span class="mod-articles-category-date"><?php echo JHTML::_('date', $item->date_time, "d M Y"); ?></span>
				<?php endif; ?>
			</div>
			<div class="message-body border-box">
				<?php if ($item->message): ?>
					<p class="mod-articles-category-introtext">
						<?php echo $item->message; ?>
					</p>
				<?php endif; ?>
			</div>
			<div class="clear"></div>
		</div>
		<?php if (FALSE): ?>
			<?php if ($item != end($list)): ?>
				<div class="message-seperator"></div>
			<?php endif; ?>
		<?php endif; ?>
	<?php endforeach; ?>
</div>
