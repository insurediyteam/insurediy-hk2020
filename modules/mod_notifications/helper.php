<?php

/**
 * @package     Joomla.Site
 * @subpackage  mod_notifications
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * Helper for mod_notifications
 *
 * @package     Joomla.Site
 * @subpackage  mod_notifications
 * @since       1.5
 */
class ModNotificationsHelper {

	public static function getList($params) {
		$user = JFactory::getUser();
		if ($user->id < 0) {
			MyUri::toLogin("Please log in to see the content");
		}
		$db = JFactory::getDbo();

		$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_notifications")
				->where("state = 1")
				->where("user_id_to in (0," . $user->id . ")")
				->order("date_time DESC");
		$db->setQuery($query, 0, 3);
		$results = $db->loadObjectList();
		if ($results) {
			return $results;
		} else {
			//shit hpns
			return array();
		}
	}

}
