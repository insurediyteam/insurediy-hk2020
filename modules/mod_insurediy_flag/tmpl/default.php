<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_articles_latest
 *
 * @copyright   Copyright (C) 2005 - 2015 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;
?>

<ul class="insurediy-flag-country<?php echo $moduleclass_sfx; ?>">
	<li><a href="<?php echo $params->get('link1') ?>"><img <?php echo ($params->get('glow1')?'class="active"':'') ?> src="<?php echo $params->get('flag1')?>" alt="flag1" /></a></li>
	<li><a href="<?php echo $params->get('link2') ?>"><img <?php echo ($params->get('glow2')?'class="active"':'') ?> src="<?php echo $params->get('flag2')?>" alt="flag1" /></a></li>
	<li><a href="<?php echo $params->get('link3') ?>"><img <?php echo ($params->get('glow3')?'class="active"':'') ?> src="<?php echo $params->get('flag3')?>" alt="flag1" /></a></li>
</ul>
