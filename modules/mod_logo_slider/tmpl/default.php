<?php
/*------------------------------------------------------------------------
# mod_logo_slider - Client Logo Slider
# ------------------------------------------------------------------------
# author    Infyways Solutions
# copyright Copyright (C) 2012 Infyways Solutions. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.infyways.com
# Technical Support:  Forum - http://support.infyways/com
-------------------------------------------------------------------------*/
// no direct access
defined('_JEXEC') or die('Restricted access');
$document =JFactory::getDocument();
$base_url=JURI::root();
$height=$height.'px';
$width=$width.'px';
$thickness=$thickness.'px';
if($nav_button)
{
  $display='block';
  $margin = "25px";
 }
else
{
  $display='none';
  $margin = "0px";
 }
($auto==1) ? $auto='true' : $auto='false';
($start==1) ? $start='true' : $start='false';
$base_url_nxt=$base_url.'modules/mod_logo_slider/tmpl/images/'.$navigation.'_next.png';
$base_url_pre=$base_url.'modules/mod_logo_slider/tmpl/images/'.$navigation.'_prev.png';
$document->addCustomTag("<style type='text/css'>
#slider$module->id {padding: 0;margin: 0;list-style: none;overflow: hidden;height: $height;}
#client$module->id .multiple li {width: $width; margin:0!important;padding:0!important;background: none repeat scroll 0 0 transparent !important;border:none!important;}
#client$module->id .bx-next {position:absolute;top:40%;right:-50px;width: 30px;height: 30px;text-indent: -999999px;background-image: url($base_url_nxt);background-repeat: no-repeat;background-position: 0 -30px;display: $display;}
#client$module->id .bx-prev {position:absolute;top:40%;left:-50px;width: 30px;height: 30px;text-indent: -999999px;background-image: url($base_url_pre);background-repeat: no-repeat;background-position: 0 -30px;display: $display;}
#slider$module->id img {	border: solid $img_border_color $thickness;opacity: 1!important;}
#client$module->id{margin-left:$margin;}
.canv{border: solid $img_border_color $thickness;}
/* Uncomment it! If you want to have the slider centered .bx-wrapper{margin:0 auto;}*/
</style>");
if($jsfiles==1)
{
	if($load1==1)
	{
		$document->addScript(JURI::root().'modules/mod_logo_slider/tmpl/assets/jquery.min.js' );
	}
	$document->addScript(JURI::root().'modules/mod_logo_slider/tmpl/assets/jquery.bxSlider.js' );
	
	if($greyscale==1)
	{
		$document->addScript(JURI::root().'modules/mod_logo_slider/tmpl/assets/greyScale.min.js' );
		$document->addCustomTag("<script>
	jQuery.noConflict();
      jQuery(function() {
        jQuery('#slider$module->id img').hide().fadeIn(1000); 
      });
      jQuery(window).load(function () { 
        jQuery('#slider$module->id img').hoverizr();
      });
    </script>");
	}
	$document->addCustomTag("<script type='text/javascript'>
		jQuery.noConflict();
			jQuery(document).ready(function(){
				jQuery('#slider$module->id').bxSlider({infiniteLoop: true, displaySlideQty: $number,speed: $speed,auto : $auto,pause: $pausetime,autoStart : $start});
    });
</script>");

}
else
{
	if($load1==1)
	{
		echo '<script src="'.JURI::root().'modules/mod_logo_slider/tmpl/assets/jquery.min.js" type="text/javascript"></script>';
	}
	echo '<script src="'.JURI::root().'modules/mod_logo_slider/tmpl/assets/jquery.bxSlider.js" type="text/javascript"></script>';
	
	if($greyscale==1){
echo '<script src="'.JURI::root().'modules/mod_logo_slider/tmpl/assets/greyScale.min.js" type="text/javascript"></script>';
echo <<<MYSCRIPT
	<script>
	 jQuery.noConflict();
      jQuery(function() {
        jQuery('#slider$module->id img').hide().fadeIn(1000); 
      });
      jQuery(window).load(function () { 
        jQuery('#slider$module->id img').hoverizr();
      });
    </script>
MYSCRIPT;
	}
	
	echo <<<MYSCRIPT
	<script type="text/javascript">
		jQuery.noConflict();
			jQuery(document).ready(function(){
				jQuery('#slider$module->id').bxSlider({infiniteLoop: true, displaySlideQty: $number,speed: $speed,pause: $pausetime,auto : $auto,autoStart : $start});
    });
</script>
MYSCRIPT;


}
?>
<div id="client<?php echo $module->id?>" class="<?php echo $moduleclass_sfx ?>">
	<ul id="slider<?php echo $module->id?>" class="multiple">
	<?php
		if($checker==1)
		{	
			$folder=trim($folder,'\\/').'/';
			$dir = opendir($folder);
			while ($file = readdir($dir))
			{ 
			   if (preg_match('/(.*?)\.gif/i',$file) || preg_match('/(.*?)\.png/i',$file) || preg_match('/(.*?)\.jpg/i',$file) || preg_match('/(.*?)\.jpeg/i',$file) ) 
					$img_array[] = str_replace(' ','%20',$file);
			}
			////////////Randomizing images only///////
			if($show_images)
			{
				@sort($img_array,SORT_REGULAR);
				if($sort)
					@sort($img_array,SORT_REGULAR);
				else
					$img_array=array_reverse($img_array);
			}
			else
			{
				shuffle($img_array);
			}
			////////////////////////////////////////////
			for ($i=0;$i<count($img_array);$i++) 
			{
			?>
				<li>
					<img src="<?php echo $base_url.$folder.$img_array[$i];?>" />
				</li>
			<?php
			}	
		}
		else
		{
			foreach ($items as $item)
			{
				$imgname[]=str_replace(' ','%20',$item->imgname);
				$imgthumb[]=str_replace(' ','%20',$item->imgthumb);
				$imglink[]=$item->imglink;
			}
			for($i=0;$i<count($imgname);$i++)
			{
				$com_arr[]=$imgname[$i].'-->'.$imglink[$i];
			}
			if(!$show_images)
			{
				shuffle($com_arr);
			}
			for ($i=0;$i<count($imgname);$i++) 
			{
				$image_link=explode('-->',$com_arr[$i]);
				
      		?>
				<?php if($link_enable)
						{
							if(trim($image_link[1])!='')
								$string[]='<li><a href="'.trim($image_link[1]).'" target="'.$target.'"><img src="'.$base_url.trim($image_link[0]).'"/></a></li>';
							else
								$string[]='<li><img src="'.$base_url.trim($image_link[0]).'" /></li>';
						}
						else 
						{
							$string[]='<li><img src="'.$base_url.trim($image_link[0]).'" /></li>';
						}
				?>
			
			<?php
			}					
			for ($i=0;$i<count($imgname);$i++) 
			{
				echo $string[$i];
			}
		}
	?>
	</ul>
</div>