<?php
/*------------------------------------------------------------------------
# mod_logo_slider - Client Logo Slider
# ------------------------------------------------------------------------
# author    Infyways Solutions
# copyright Copyright (C) 2012 Infyways Solutions. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Websites: http://www.infyways.com
# Technical Support:  Forum - http://support.infyways/com
-------------------------------------------------------------------------*/
// no direct access
defined( '_JEXEC' ) or die( 'Restricted access' );
$load1 = $params->get('load1');
$jsfiles = $params->get('jsfiles');
$height = $params->get('height');
$width = $params->get('width');
$start = $params->get('start');
$auto = $params->get('auto');
$nav_button = $params->get('nav_button');
$navigation = $params->get('navigation');
$speed = $params->get('speed');
$number = $params->get('number');
$checker = $params->get('checker');
$folder = $params->get('folder');
$img_path = $params->get('img_path');
$link_enable = $params->get('link_enable');
$link = $params->get('link');
$target = $params->get('target');
$img_border_color = $params->get('img_border_color');
$show_images = $params->get('show_images');
$sort = $params->get('sort');
$greyscale = $params->get('greyscale',0);
$thickness = $params->get('thickness',1);
$pausetime = $params->get('pausetime',4000);
$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));
if(!$checker)
{
	require_once dirname(__FILE__) . '/helper.php';
	$items = modClientLogoSliderHelper::getItems($params);
}
	require(JModuleHelper::getLayoutPath('mod_logo_slider','default'));

?>