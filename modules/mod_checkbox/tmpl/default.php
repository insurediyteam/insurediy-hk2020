<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_checkbox
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;
?>
<div>
	<div class="control-group">
		<div class="control-label" style="float:left;width:4.5%;">
			<div style="position:relative;">
				<input type="checkbox" name="<?php echo $fname; ?>" id="<?php echo $fid; ?>" value="1" class="styled <?php echo $class; ?>" <?php echo implode(" ", $attr) ?>>
			</div>
		</div>
		<div class="controls" style="float:left;width:95%">
			<label id="<?php echo $lname; ?>" for="<?php echo $fid; ?>" class="hasTooltip display_inline <?php echo $labelClass ?>" data-original-title="<?php echo $tooltips; ?>" title=""><strong><?php echo $label; ?></strong></label>
			<?php if ($break): ?><br><?php endif; ?>
			<span class="explanation"><?php echo $explanation; ?></span>
			<div class="error-container"></div>
		</div>
		<div class="clear"></div>
	</div>
</div>