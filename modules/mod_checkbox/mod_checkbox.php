<?php

/**
 * @package     Joomla.Site
 * @subpackage  mod_checkbox
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

// Include the login functions only once
require_once __DIR__ . '/helper.php';

$moduleclass_sfx = htmlspecialchars($params->get('moduleclass_sfx'));
$label = $params->get("label", "");
$explanation = $params->get("description", "");
$class = $params->get("class", "");
$labelClass = $params->get("label_class", "");
$tooltips = $params->get("tooltips", "");
$name = $params->get("name", "");
$form_control = $params->get("form_control", FALSE);
$break = $params->get("break", FALSE);

$fname = ($form_control) ? $form_control . "[" . $name . "]" : $name;
$fid = ($form_control) ? $form_control . "_" . $name : $name;
$lname = $fid . "-lbl";

$attr = array();
if ($params->get("required", FALSE)) {
	$attr[] = ' required aria-required="true"';
	$labelClass.= " required";
	$explanation.= '<span class="star">&nbsp;*</span>';
}
if ($params->get("checked", FALSE)) {
	$attr[] = "checked = checked";
}

require JModuleHelper::getLayoutPath('mod_checkbox', $params->get('layout', 'default'));
