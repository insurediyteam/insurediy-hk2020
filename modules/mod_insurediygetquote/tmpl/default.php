<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_login
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('bootstrap.tooltip');

$pretext = $params->get('pretext', FALSE);
if ($pretext && strlen($pretext)) {
	$pretext = str_replace("XXXX", JFactory::getUser()->name, $pretext);
}
?>
<div style="float:right;">
	<div class="insurediy-get-a-quote-retrieve-box">
		<div class="padding2520">
			<?php if (!$user->id > 0): ?>
				<div class="title-text">Retrieve Your Quote <img src="templates/protostar/images/help-quote2.png" class="hasTooltip" data-original-title="" ></div>
				<form action="index.php?option=com_users" method="post" name="retrieveQuotationForm">
					<div><input type="email" placeholder="Email Address" name="username" autocomplete="off" /></div>
					<div><input type="password" placeholder="Password" name="password" autocomplete="off" /></div>
					<div class="btn-group">
						<button type="submit" class="btn btn-primary validate" style="padding:8px 25px;font-size:13px !important;font-family:sourcesanspro-semibold;"><?php echo JText::_('JSUBMIT') ?></button>
					</div>
					<input type="hidden" name="option" value="com_users" />
					<input type="hidden" name="task" value="user.login" />
					<input type="hidden" name="return" value="<?php echo $return; ?>" />
					<?php echo JHtml::_('form.token'); ?>
				</form>
			<?php else : ?>
				<p style="font-family: 'sourcesanspro-semibold'; font-size: 26px; color: #fff; line-height: 26px;text-align:center"><?php echo trim($pretext); ?><br /></p>
				<?php echo $params->get('posttext'); ?>
			<?php endif; ?>
		</div>
	</div>
</div>	