<?php

defined('JPATH_PLATFORM') or die;

JFormHelper::loadFieldClass('text');

class JFormFieldPlainText extends JFormFieldText {

	protected $type = 'PlainText';

	protected function getInput() {
		$lang_prefix = isset($this->element['lang_prefix']) ? (string) $this->element['lang_prefix'] : FALSE;
		if ($lang_prefix) {
			$text = JText::_($lang_prefix . $this->value);
		} else {
			$text = $this->value;
		}
		return $text && (strlen($text) > 0) ? $text : "N/A";
	}

}
