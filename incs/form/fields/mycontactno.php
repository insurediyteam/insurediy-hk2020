<?php

defined('JPATH_PLATFORM') or die;

JFormHelper::loadFieldClass('text');

class JFormFieldMyContactNo extends JFormFieldText {

	protected $type = 'MyContactNo';

	protected function getInput() {

		$datatype = ($this->element['datatype']) ? (string) $this->element['datatype'] : "life";
		$data = JFactory::getSession()->get($datatype . '.data', FALSE);
		$code = isset($data['contact_country_code']) ? $data['contact_country_code'] : "";
		// Translate placeholder text
		$hint = $this->translateHint ? JText::_($this->hint) : $this->hint;

		// Initialize some field attributes.
		$size = !empty($this->size) ? ' size="' . $this->size . '"' : '';
		$maxLength = !empty($this->maxLength) ? ' maxlength="' . $this->maxLength . '"' : '';
		$class = !empty($this->class) ? ' class="' . $this->class . '"' : '';
		$readonly = $this->readonly ? ' readonly' : '';
		$disabled = $this->disabled ? ' disabled' : '';
		$required = $this->required ? ' required aria-required="true"' : '';
		$hint = $hint ? ' placeholder="' . $hint . '"' : '';
		$autocomplete = !$this->autocomplete ? ' autocomplete="off"' : ' autocomplete="' . $this->autocomplete . '"';
		$autocomplete = $autocomplete == ' autocomplete="on"' ? '' : $autocomplete;
		$autofocus = $this->autofocus ? ' autofocus' : '';
		$spellcheck = $this->spellcheck ? '' : ' spellcheck="false"';
		$pattern = !empty($this->pattern) ? ' pattern="' . $this->pattern . '"' : '';
		$inputmode = !empty($this->inputmode) ? ' inputmode="' . $this->inputmode . '"' : '';
		$dirname = !empty($this->dirname) ? ' dirname="' . $this->dirname . '"' : '';
		$list = '';

		// Initialize JavaScript field attributes.
		$onchange = !empty($this->onchange) ? ' onchange="' . $this->onchange . '"' : '';

		// Including fallback code for HTML5 non supported browsers.
		JHtml::_('jquery.framework');
		JHtml::_('script', 'system/html5fallback.js', false, true);

		// Get the field suggestions.
		$options = (array) $this->getSuggestions();
		if (!empty($options)) {
			$html[] = JHtml::_('select.suggestionlist', $options, 'value', 'text', $this->id . '_datalist"');
			$list = ' list="' . $this->id . '_datalist"';
		}
		if (isset($code)) {
			$html[] = '<input type="text" class="input-country-code" value="' . $code . '" name="jform[contact_country_code]" style="max-width: 17%;margin-right: 1%;text-align:center;" />';
		} else {
			$html[] = '<input type="text" class="input-country-code" value="" name="jform[contact_country_code]" style="max-width: 17%;margin-right: 1%;text-align:center;" />';
		}
		$html[] = '<input type="text" class="input-contact-no" style="max-width: 82%;" name="' . $this->name . '" id="' . $this->id . '"' . $dirname . ' value="'
				. htmlspecialchars($this->value, ENT_COMPAT, 'UTF-8') . '"' . $size . $disabled . $readonly . $list
				. $hint . $onchange . $maxLength . $required . $autocomplete . $autofocus . $spellcheck . $inputmode . $pattern . ' />';

		return implode($html);
	}

//
//	/**
//	 * Method to get the field suggestions.
//	 *
//	 * @return  array  The field option objects.
//	 *
//	 * @since   11.1
//	 */
//	protected function getSuggestions() {
//		$options = array();
//
//		foreach ($this->element->children() as $option) {
//			// Only add <option /> elements.
//			if ($option->getName() != 'option') {
//				continue;
//			}
//
//			// Create a new option object based on the <option /> element.
//			$options[] = JHtml::_(
//							'select.option', (string) $option['value'], JText::alt(trim((string) $option), preg_replace('/[^a-zA-Z0-9_\-]/', '_', $this->fieldname)), 'value', 'text'
//			);
//		}
//
//		reset($options);
//
//		return $options;
//	}
}
