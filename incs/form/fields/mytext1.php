<?php

defined('JPATH_PLATFORM') or die;

JFormHelper::loadFieldClass('text');

class JFormFieldMyText extends JFormFieldText {

	protected $type = 'MyText';

	protected function getLabel() {
		$label = '';
		$imghelpquote = '';


		if ($this->hidden) {
			return $label;
		}

		// Get the label text from the XML element, defaulting to the element name.
		$text = $this->element['label'] ? (string) $this->element['label'] : (string) $this->element['name'];
		$text = $this->translateLabel ? JText::_($text) : $text;

		// Build the class for the label.
		$class = !empty($this->description) ? 'hasTooltip' : '';
		$class = $this->required == true ? $class . ' required' : $class;
		$class = !empty($this->labelClass) ? $class . ' ' . $this->labelClass : $class;

		// Build explanation text
		$explanation = isset($this->element['explanation']) ? JText::_((string) $this->element['explanation']) : FALSE;

		// Add the opening label tag and main attributes attributes.
		$label .= '<label id="' . $this->id . '-lbl" for="' . $this->id . '" class="' . $class . '"';
		$imghelpquote .= ' ';

		$hasToolTip = isset($this->element['hasToolTip']) ? $this->element['hasToolTip'] == TRUE : 1;
		// If a description is specified, use it to build a tooltip.
		if (!empty($this->description) && $hasToolTip) {
			JHtml::_('bootstrap.tooltip');
			$imghelpquote .= '&nbsp;<img class="hasTooltip" alt="help-quote" src="images/help-quote.png" title="' . JHtml::tooltipText('', JText::_($this->description), 0) . '" />';
		}
		$star = '<span class="star">&#160;*</span>';
		// Add the label text and closing tag.
		if ($explanation) {
			$label .= '>' . $text . $imghelpquote . '</label>' . "<span class='explanation'>" . $explanation . (($this->required) ? $star : "") . "</span>";
		} else {
			$label .= '>' . $text . (($this->required) ? $star : "") . $imghelpquote . '</label>' . $explanation;
		}

		return $label;
	}

}
