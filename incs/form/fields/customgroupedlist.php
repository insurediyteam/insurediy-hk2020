<?php

defined('JPATH_PLATFORM') or die;

class JFormFieldCustomGroupedList extends JFormField {

	protected $type = 'CustomGroupedList';

	protected function getGroups() {
		$groups = array();
		$label = 0;

		foreach ($this->element->children() as $element) {
			switch ($element->getName()) {
				// The element is an <option />
				case 'option':
					// Initialize the group if necessary.
					if (!isset($groups[$label])) {
						$groups[$label] = array();
					}

					// Create a new option object based on the <option /> element.
					$tmp = JHtml::_(
									'select.option', ($element['value']) ? (string) $element['value'] : trim((string) $element), JText::alt(trim((string) $element), preg_replace('/[^a-zA-Z0-9_\-]/', '_', $this->fieldname)), 'value', 'text', ((string) $element['disabled'] == 'true')
					);

					// Set some option attributes.
					$tmp->class = (string) $element['class'];

					// Set some JavaScript option attributes.
					$tmp->onclick = (string) $element['onclick'];

					// Add the option.
					$groups[$label][] = $tmp;
					break;

				// The element is a <group />
				case 'group':
					// Get the group label.
					if ($groupLabel = (string) $element['label']) {
						$label = JText::_($groupLabel);
					}

					// Initialize the group if necessary.
					if (!isset($groups[$label])) {
						$groups[$label] = array();
					}

					// Iterate through the children and build an array of options.
					foreach ($element->children() as $option) {
						// Only add <option /> elements.
						if ($option->getName() != 'option') {
							continue;
						}

						// Create a new option object based on the <option /> element.
						$tmp = JHtml::_(
										'select.option', ($option['value']) ? (string) $option['value'] : JText::_(trim((string) $option)), JText::_(trim((string) $option)), 'value', 'text', ((string) $option['disabled'] == 'true')
						);

						// Set some option attributes.
						$tmp->class = (string) $option['class'];

						// Set some JavaScript option attributes.
						$tmp->onclick = (string) $option['onclick'];

						// Add the option.
						$groups[$label][] = $tmp;
					}

					if ($groupLabel) {
						$label = count($groups);
					}
					break;

				// Unknown element type.
				default:
					throw new UnexpectedValueException(sprintf('Unsupported element %s in JFormFieldGroupedList', $element->getName()), 500);
			}
		}
		if ($this->element['data']) {
			$data = array();
			$db = JFactory::getDbo();
			$grps = explode(",", $this->element['data']);
			foreach ($grps as $grp) {
				$query = $db->getQuery(TRUE);
				switch (strtolower($grp)) {
					case "country":
						$exclude = (isset($this->element['exclude'])) ? (string) $this->element['exclude'] : null;
						$in = (isset($this->element['prioritize'])) ? (string) $this->element['prioritize'] : "('CN', 'HK','MO')";
						$query->select("iso AS value, nicename AS text");
						$query->from("#__insure_countries");
						$query->where("iso IN " . $in);
						$db->setQuery($query);
						$results = $db->loadAssocList();
						foreach ($results as $k => $result) {
							$data[" "][$k]['value'] = $result['value'];
							$data[" "][$k]['text'] = $result['text'];
						}
						$query = $db->getQuery(TRUE);
						$query->select("iso AS value, nicename AS text");
						$query->from("#__insure_countries");
						$query->where("iso NOT IN " . $in);
						if($exclude) {
							$query->where("iso NOT IN " . $exclude);
						}
						$query->order("nicename");
						$db->setQuery($query);
						$results = $db->loadAssocList();
						foreach ($results as $k => $result) {
							$data["-----------"][$k]['value'] = $result['value'];
							$data["-----------"][$k]['text'] = $result['text'];
						}
						break;
					case "nationality":
						$query->select("iso AS value, nicename AS text");
						$query->from("#__insure_nationalities2");
						$query->where("iso IN ('CN', 'HK','MO')");
						$db->setQuery($query);
						$results = $db->loadAssocList();
						foreach ($results as $k => $result) {
							$data[" "][$k]['value'] = $result['value'];
							$data[" "][$k]['text'] = $result['text'];
						}
						$query = $db->getQuery(TRUE);
						$query->select("iso AS value, nicename AS text");
						$query->from("#__insure_nationalities2");
						$query->where("iso NOT IN ('CN', 'HK','MO')");
						$query->order("nicename");
						$db->setQuery($query);
						$results = $db->loadAssocList();
						foreach ($results as $k => $result) {
							$data["-----------"][$k]['value'] = $result['value'];
							$data["-----------"][$k]['text'] = $result['text'];
						}
						break;
					case "pre_location":
						$query->select("address, area");
						$query->from("#__insure_meeting_locations");
						$db->setQuery($query);
						$results = $db->loadAssocList();
						if ($results) {
							foreach ($results as $k => $result) {
								unset($result["id"]);
								$data[$result['area']][$k]["value"] = $result['address'];
								$data[$result['area']][$k]["text"] = $result['address'];
							}
						}
						break;
					default:
						break;
				}
			}
			if (count($data) > 0) {
				$groups = array_merge($groups, $data);
			}
		}
		reset($groups);
		return $groups;
	}

	protected function getInput() {
		$html = array();
		$attr = '';

		// Initialize some field attributes.
		$attr .= $this->element['class'] ? ' class="' . (string) $this->element['class'] . '"' : '';
		$attr .= ((string) $this->element['disabled'] == 'true') ? ' disabled="disabled"' : '';
		$attr .= $this->element['size'] ? ' size="' . (int) $this->element['size'] . '"' : '';
		$attr .= $this->multiple ? ' multiple="multiple"' : '';
		$attr .= $this->required ? ' required="required" aria-required="true"' : '';

		// Initialize JavaScript field attributes.
		$attr .= $this->element['onchange'] ? ' onchange="' . (string) $this->element['onchange'] . '"' : '';

		// Get the field groups.
		$groups = (array) $this->getGroups();

		// Create a read-only list (no name) with a hidden input to store the value.
		if ((string) $this->element['readonly'] == 'true') {
			$html[] = JHtml::_(
							'select.groupedlist', $groups, null, array(
						'list.attr' => $attr, 'id' => $this->id, 'list.select' => $this->value, 'group.items' => null, 'option.key.toHtml' => false,
						'option.text.toHtml' => false
							)
			);
			$html[] = '<input type="hidden" name="' . $this->name . '" value="' . $this->value . '"/>';
		}
		// Create a regular list.
		else {
			$html[] = JHtml::_(
							'select.groupedlist', $groups, $this->name, array(
						'list.attr' => $attr, 'id' => $this->id, 'list.select' => $this->value, 'group.items' => null, 'option.key.toHtml' => false,
						'option.text.toHtml' => false
							)
			);
		}

		return implode($html);
	}

}
