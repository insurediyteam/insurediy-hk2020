<?php

defined('JPATH_PLATFORM') or die;

JFormHelper::loadFieldClass('hidden');

class JFormFieldMyHidden extends JFormFieldHidden {

	protected $type = 'MyHidden';

	protected function getInput() {
		$inputClass = (isset($this->element['class'])) ? (string) $this->element['class'] : "";
		$value = empty($this->value) ? $this->default : $this->value;
		$id = str_replace('[', '_',$this->name);
		
		$inputstr = "";
		$inputstr.= '<input type="hidden" id="' . str_replace(']', '',$id) . '" value="'. $value .'" name="' . $this->name . '" />';
		return $inputstr;
	}

}
