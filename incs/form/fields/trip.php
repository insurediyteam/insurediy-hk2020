<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_banners
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('JPATH_BASE') or die;

/**
 * Impressions Field class for the Joomla Framework.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_banners
 * @since       1.6
 */
JLoader::import('incs.form.fields.customlabel', JPATH_ROOT);

class JFormFieldTrip extends JformFieldCustomLabel {

	/**
	 * The form field type.
	 *
	 * @var		string
	 * @since   1.6
	 */
	protected $type = 'Trip';

	protected function getInput() {

		$inputClass = (isset($this->element['class'])) ? (string) $this->element['class'] : "";
		$value = empty($this->value) ? $this->default : $this->value;
		$checked = ' checked="checked" ';
		$class = ' class="checked" ';

		$inputstr = "";

		$inputstr .= '<div class="idy-radio clearfix">';

		$inputstr .= '	<div class="idy-radio-wrapper idy-radio-single-trip">';
		$inputstr .= '		<input class="' . $inputClass . '" type="radio" id="insurediy-single-trip" ' . (($value == 'ST') ? $checked : '') . ' value="ST" name="' . $this->name . '" />';
		$inputstr .= '		<label for="insurediy-single-trip" ' . (($value == 'ST') ? $class : '') . '>' . JText::_("TRIP_ST") . '</label>';
		$inputstr .= '	</div>';

		$inputstr .= '	<div class="idy-radio-wrapper idy-radio-annual-trip">';
		$inputstr .= '		<input class="' . $inputClass . '" type="radio" id="insurediy-annual-trip" ' . (($value == 'AT') ? $checked : '') . ' value="AT" name="' . $this->name . '" />';
		$inputstr .= '		<label for="insurediy-annual-trip" ' . (($value == 'AT') ? $class : '') . '>' . JText::_("TRIP_AT") . '</label>';
		$inputstr .= '	</div>';
		
		$inputstr .= '</div>';

		return $inputstr;

//		$value = empty($this->value) ? $this->default : $this->value;
//		$checked = ' checked="checked" ';
//
//		return '<div class="idy-radio">'
//				. '<div class="idy-radio-single-trip"><input type="radio" id="insurediy-single-trip" ' . (($value == 'ST') ? $checked : '') . ' value="ST" name="' . $this->name . '" /><label for="insurediy-single-trip">&nbsp;</label></div>'
//				. '<div class="idy-radio-annual-trip"><input type="radio" id="insurediy-annual-trip" ' . (($value == 'AT') ? $checked : '') . ' value="AT" name="' . $this->name . '" /><label for="insurediy-annual-trip">&nbsp;</label></div>'
//				. '<div style="clear:both"></div>'
//				. '</div>';
	}

}
