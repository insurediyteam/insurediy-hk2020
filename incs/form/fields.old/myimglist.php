<?php

defined('JPATH_PLATFORM') or die;

JFormHelper::loadFieldClass('list');

class JFormFieldMyImgList extends JFormFieldList {

	protected $type = 'MyImgList';

	protected function getOptions() {
		$options = array();

		foreach ($this->element->children() as $option) {
			// Only add <option /> elements.
			if ($option->getName() != 'option') {
				continue;
			}

			// Filter requirements
			if ($requires = explode(',', (string) $option['requires'])) {
				// Requires multilanguage
				if (in_array('multilanguage', $requires) && !JLanguageMultilang::isEnabled()) {
					continue;
				}

				// Requires associations
				if (in_array('associations', $requires) && !JLanguageAssociations::isEnabled()) {
					continue;
				}
			}

			$value = (string) $option['value'];

			$disabled = (string) $option['disabled'];
			$disabled = ($disabled == 'true' || $disabled == 'disabled' || $disabled == '1');

			$disabled = $disabled || ($this->readonly && $value != $this->value);

			// Create a new option object based on the <option /> element.
			$tmp = JHtml::_(
							'select.option', $value, JText::alt(trim((string) $option), preg_replace('/[^a-zA-Z0-9_\-]/', '_', $this->fieldname)), 'value', 'text', $disabled
			);

			// Set some option attributes.
			$tmp->class = (string) $option['class'];

			// Set some JavaScript option attributes.
			$tmp->onclick = (string) $option['onclick'];

			// Add the option object to the result set.
			$options[] = $tmp;
		}
		$folder = (isset($this->element["folder"])) ? (string) $this->element["folder"] : FALSE;

		if ($folder) {
			$tmplist = array();
			if ($dir = @opendir($folder)) {
				while (false !== ($file = readdir($dir))) {
					if (preg_match('/.+\.(jpg|jpeg|gif|png)$/i', $file)) {
						if (getimagesize($folder . DS . $file) !== FALSE) {
							$tmplist[] = JHtml::_(
											'select.option', $file, $file, 'value', 'text', false
							);
						}
					}
				}
				closedir($dir);
			}
		}

		if (count($tmplist) > 0) {
			$options = array_merge($options, $tmplist);
		}
		reset($options);

		return $options;
	}

}
