<?php

defined('JPATH_PLATFORM') or die;
JFormHelper::loadFieldClass('list');

class JFormFieldCustomList extends JFormFieldList {

	protected $type = 'CustomList';

	protected function getInput() {
		$html = array();
		$attr = '';

		// Initialize some field attributes.
		$attr .=!empty($this->class) ? ' class="' . $this->class . '"' : '';
		$attr .=!empty($this->size) ? ' size="' . $this->size . '"' : '';
		$attr .= $this->multiple ? ' multiple' : '';
		$attr .= $this->required ? ' required aria-required="true"' : '';
		$attr .= $this->autofocus ? ' autofocus' : '';

		// To avoid user's confusion, readonly="true" should imply disabled="true".
		if ((string) $this->readonly == '1' || (string) $this->readonly == 'true' || (string) $this->disabled == '1' || (string) $this->disabled == 'true') {
			$attr .= ' disabled="disabled"';
		}

		// Initialize JavaScript field attributes.
		$attr .= $this->onchange ? ' onchange="' . $this->onchange . '"' : '';

		// Get the field options.
		$options = (array) $this->getOptions();
		$multipleList = (isset($this->element['multipleList'])) ? TRUE : FALSE;
		if ($multipleList) {
			$this->name = $this->name . "[]";
		}
		// Create a read-only list (no name) with a hidden input to store the value.
		if ((string) $this->readonly == '1' || (string) $this->readonly == 'true') {
			$html[] = JHtml::_('select.genericlist', $options, '', trim($attr), 'value', 'text', $this->value, $this->id);
			$html[] = '<input type="hidden" name="' . $this->name . '" value="' . $this->value . '"/>';
		} else {
			// Create a regular list.
			$html[] = JHtml::_('select.genericlist', $options, $this->name, trim($attr), 'value', 'text', $this->value, $this->id);
		}

		return implode($html);
	}

	protected function getOptions() {
		$options = array();

		foreach ($this->element->children() as $option) {
			// Only add <option /> elements.
			if ($option->getName() != 'option') {
				continue;
			}

			// Filter requirements
			if ($requires = explode(',', (string) $option['requires'])) {
				// Requires multilanguage
				if (in_array('multilanguage', $requires) && !JLanguageMultilang::isEnabled()) {
					continue;
				}

				// Requires associations
				if (in_array('associations', $requires) && !JLanguageAssociations::isEnabled()) {
					continue;
				}
			}

			$value = (string) $option['value'];

			$disabled = (string) $option['disabled'];
			$disabled = ($disabled == 'true' || $disabled == 'disabled' || $disabled == '1');

			$disabled = $disabled || ($this->readonly && $value != $this->value);

			// Create a new option object based on the <option /> element.
			$tmp = JHtml::_(
							'select.option', $value, JText::alt(trim((string) $option), preg_replace('/[^a-zA-Z0-9_\-]/', '_', $this->fieldname)), 'value', 'text', $disabled
			);

			// Set some option attributes.
			$tmp->class = (string) $option['class'];

			// Set some JavaScript option attributes.
			$tmp->onclick = (string) $option['onclick'];

			// Add the option object to the result set.
			$options[] = $tmp;
		}

		$num_range_max = ($this->element["numRangeMax"]) ? (int) $this->element["numRangeMax"] : FALSE;
		$num_range_min = ($this->element["numRangeMin"]) ? (int) $this->element["numRangeMin"] : FALSE;

		$tmplist = array();
		$unit = ($this->element['unit']) ? $this->element['unit'] : "";
		if (($this->element["numRangeMax"]) && ($this->element["numRangeMin"])) {
			for ($i = $num_range_min; $i <= $num_range_max; $i++) {
				$tmplist[] = JHtml::_(
								'select.option', $i, $i . " " . $unit, 'value', 'text', false
				);
			}
		}

		$data = (isset($this->element["data"])) ? $this->element["data"] : FALSE;
		if ($data) {
			switch ($data) {
				case "pre_location":
					$db = JFactory::getDbo();
					$query = $db->getQuery(TRUE);
					$query->select("*");
					$query->from("#__insure_meeting_locations");
					$db->setQuery($query);
					$results = $db->loadAssocList();
					if ($results) {
						foreach ($results as $k => $result) {
							unset($result["id"]);
							$address = implode(" ", $result);
							$tmplist[] = JHtml::_(
											'select.option', $address, $address . " " . $unit, 'value', 'text', false
							);
						}
					}
					break;
				case "banks":
					$db = JFactory::getDbo();
					$query = $db->getQuery(TRUE);
					$query->select("*");
					$query->from("#__insure_institution");
					$db->setQuery($query);
					$results = $db->loadAssocList();
					if ($results) {
						foreach ($results as $k => $result) {
							$text = $result['name'] . " - " . $result['code'] . "";
							$value = $result['code'];
							$tmplist[] = JHtml::_(
											'select.option', $value, $text, 'value', 'text', false
							);
						}
					}
					break;
				case "years":
					$db = JFactory::getDbo();
					$query = $db->getQuery(TRUE);
					$query->select("*");
					$query->from("#__insure_life_years");
					$db->setQuery($query);
					$results = $db->loadAssocList();
					if ($results) {
						foreach ($results as $k => $result) {
							$text = $result['year_name'];
							$value = $result['year_code'];
							$tmplist[] = JHtml::_(
											'select.option', $value, $text, 'value', 'text', false
							);
						}
					}
					break;
				case "currencies":
					$db = JFactory::getDbo();
					$query = $db->getQuery(TRUE);
					$query->select("*");
					$query->from("#__currencies");
					$db->setQuery($query);
					$results = $db->loadAssocList();
					if ($results) {
						foreach ($results as $k => $result) {
							$text = $result['name'] . " (" . $result['symbol'] . ")";
							$value = $result['id'];
							$tmplist[] = JHtml::_(
											'select.option', $value, $text, 'value', 'text', false
							);
						}
					}
					break;
				case "modules":
					$db = JFactory::getDbo();
					$query = $db->getQuery(TRUE)
							->select("*")
							->from("#__modules")
							->where("published = 1");
					$db->setQuery($query);
					$results = $db->loadAssocList();
					if ($results) {
						foreach ($results as $k => $result) {
							$text = $result['title'];
							$value = $result['id'];
							$tmplist[] = JHtml::_(
											'select.option', $value, $text, 'value', 'text', false
							);
						}
					}
					break;
				default:
					break;
			}
		}

		if (count($tmplist) > 0) {
			$options = array_merge($options, $tmplist);
		}
		reset($options);

		return $options;
	}

	protected function getRoomTypes() {
		
	}

	protected function getYears() {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$query->select("*");
		$query->from("#__insure_life_years");
		return $db->setQuery($query)->loadAssocList();
	}

}
