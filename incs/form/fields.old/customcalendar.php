<?php

defined('JPATH_PLATFORM') or die;
JFormHelper::loadFieldClass('calendar');

class JFormFieldCustomCalendar extends JFormFieldCalendar {

	protected $type = 'CustomCalendar';

	protected function getInput() {
		// Translate placeholder text
		$hint = $this->translateHint ? JText::_($this->hint) : $this->hint;
		// Initialize some field attributes.
		$format = $this->format;

		// Build the attributes array.
		$attributes = array();

		empty($this->size) ? null : $attributes['size'] = $this->size;
		empty($this->maxlength) ? null : $attributes['maxlength'] = $this->maxlength;
		empty($this->class) ? null : $attributes['class'] = $this->class;
		!$this->readonly ? null : $attributes['readonly'] = '';
		!$this->disabled ? null : $attributes['disabled'] = '';
		empty($this->onchange) ? null : $attributes['onchange'] = $this->onchange;
		empty($hint) ? null : $attributes['placeholder'] = $hint;
		$this->autocomplete ? null : $attributes['autocomplete'] = 'off';
		!$this->autofocus ? null : $attributes['autofocus'] = '';

		if ($this->required) {
			$attributes['required'] = '';
			$attributes['aria-required'] = 'true';
		}

		if (is_array($attributes)) {
			$attribs = JArrayHelper::toString($attributes);
		}

		// Handle the special case for "now".
		if (strtoupper($this->value) == 'NOW') {
			$this->value = strftime($format);
		}

		// Get some system objects.
		$config = JFactory::getConfig();
		$user = JFactory::getUser();

		// If a known filter is given use it.
		switch (strtoupper($this->filter)) {
			case 'SERVER_UTC':
				// Convert a date to UTC based on the server timezone.
				if ((int) $this->value) {
					// Get a date object based on the correct timezone.
					$date = JFactory::getDate($this->value, 'UTC');
					$date->setTimezone(new DateTimeZone($config->get('offset')));

					// Transform the date string.
					$this->value = $date->format('d-m-Y', true, false);
				}

				break;

			case 'USER_UTC':
				// Convert a date to UTC based on the user timezone.
				if ((int) $this->value) {
					// Get a date object based on the correct timezone.
					$date = JFactory::getDate($this->value, 'UTC');

					$date->setTimezone(new DateTimeZone($user->getParam('timezone', $config->get('offset'))));

					// Transform the date string.
					$this->value = $date->format('d-m-Y', true, false);
				}

				break;
		}

		// Including fallback code for HTML5 non supported browsers.
		JHtml::_('jquery.framework');
		JHtml::_('script', 'system/html5fallback.js', false, true);
		JHtml::_('script', 'system/jquery.ui.core.js', false, true);
		JHtml::_('script', 'system/jquery.ui.datepicker.js', false, true);
		JHtml::_('stylesheet', 'system/jquery-ui.css', false, true);

		$year = date("Y", time());
		$maxYr = (isset($this->element['maxYear'])) ? $year + (int) $this->element['maxYear'] : $year;
		$minYr = (isset($this->element['minYear'])) ? $year + (int) $this->element['minYear'] : $year - 1;
		$defaultDate = "01-01-1970";

		$today_d = date("d");
		$today_m = date("m");

		if ($this->element['after4pm'] == "1")  {
			$sg_time = date("G");
			if($sg_time >= 8 && $sg_time < 24) { // GMT time hours
				$today_d += 1;
			}
		}
		
		$minDate = $today_d . "-" . $today_m . "-" . $minYr;

		if (isset($this->element['minDate'])) {
			$minDate = (string) $this->element['minDate'];
		}

		if (isset($this->element['defaultDate'])) {
			$defaultDate = (string) $this->element['defaultDate'];
		}
		$document = JFactory::getDocument();
		if ($defaultDate == "+ 365 day") {
			$defaultDate = JHtml::date(strtotime(date("Y-m-d", mktime()) . " + 365 day"), "d-m-Y");
		} elseif ($defaultDate == "today") {
			$defaultDate = JHtml::date(JFactory::getDate('now'), "d-m-Y");
		} elseif ($defaultDate == "mindate") {
			$defaultDate = $minDate;
		} else {
			$defaultDate = (strlen($this->value) > 0) && (strtotime($this->value) > strtotime($minDate)) ? $this->value : $defaultDate;
		}
		
		$minDate = $minYr . "," . ($today_m - 1) . "," . $today_d;
		if (isset($this->element['minDate'])) {
			$minDate = (string) $this->element['minDate'];
		} 
		$minDate = ', minDate: new Date('.$minDate.')';
		
		$maxDate = '';
		if (isset($this->element['maxDate'])) {
			$maxDate = (string) $this->element['maxDate'];
			$maxDate = ', maxDate: new Date('.$maxDate.')';
		}
		
		$yearRange = '';
		if(!empty($minYr) && !empty($maxYr)) {
			$yearRange = ', yearRange: "' . $minYr . ':' . $maxYr . '"';
		}
		
		$btnUrl = MyUri::getUrls("site") . "media/system/images/ico-calendar.png";
		$script = 'jQuery(function() {
				jQuery( "#' . $this->id . '" ).datepicker({
					changeMonth: true,
					changeYear: true,
					showOn: "both",
					buttonImage: "' . $btnUrl . '",
					dateFormat : "dd-mm-yy"
					'. $yearRange.'
					'. $minDate . '
					' . $maxDate . '
				});
			});';

		$document->addScriptDeclaration($script);

		return '<div class="input-append"><input type="text" title="" name="' . $this->name . '" id="' . $this->id . '" value="' . htmlspecialchars($defaultDate, ENT_COMPAT, 'UTF-8') . '" ' . $attribs . ' /></div>';
	}

}
