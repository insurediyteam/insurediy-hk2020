<?php

defined('JPATH_BASE') or die;

JFormHelper::loadFieldClass('radio');

class JFormFieldAlcohol extends JFormFieldRadio {

	protected $type = 'Alcohol';

	protected function getInput() {
		$inputClass = (isset($this->element['class'])) ? (string) $this->element['class'] : "";
		$value = empty($this->value) ? $this->default : $this->value;
		$checked = ' checked="checked" ';
		$class = ' class="checked" ';

		$inputstr = "";
		$inputstr .= '<div class="idy-radio clearfix">';

		$inputstr .= '<div class="idy-radio-wrapper idy-radio-alcohol-yes">';
		$inputstr .= '<input class="' . $inputClass . '" type="radio" id="insurediy-alcohol-1" value="1" name="' . $this->name . '" ' . (($value == 1) ? $checked : '') . ' />';
		$inputstr .= '<label for="insurediy-alcohol-1" ' . (($value == 1) ? $class : '') . '>' . JText::_("JYES") . '</label>';
		$inputstr .= '</div>';

		$inputstr .= '<div class="idy-radio-wrapper idy-radio-alcohol-no">';
		$inputstr .= '<input class="' . $inputClass . '" type="radio" id="insurediy-alcohol-2" value="0" name="' . $this->name . '" ' . (($value == 0) ? $checked : '') . ' />';
		$inputstr .= '<label for="insurediy-alcohol-2" ' . (($value == 0) ? $class : '') . '>' . JText::_("JNO") . '</label>';
		$inputstr .= '</div>';

		$inputstr .= '</div>';

		return $inputstr;
	}

}
