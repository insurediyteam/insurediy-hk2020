<?php

defined('JPATH_PLATFORM') or die;

JFormHelper::loadFieldClass('checkbox');

class JFormFieldMyCheckbox extends JFormFieldCheckbox {

	protected $type = 'MyCheckbox';

}
