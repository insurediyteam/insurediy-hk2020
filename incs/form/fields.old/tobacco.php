<?php

defined('JPATH_BASE') or die;

JFormHelper::loadFieldClass('radio');

class JFormFieldTobacco extends JFormFieldRadio {

	protected $type = 'Tobacco';

	protected function getInput() {
		$inputClass = (isset($this->element['class'])) ? (string) $this->element['class'] : "";
		$value = empty($this->value) ? $this->default : $this->value;
		$checked = ' checked="checked" ';
		$class = ' class="checked" ';

		$inputstr = "";
		$inputstr.= '<div class="idy-radio clearfix">';

		$inputstr.= '<div class="idy-radio-wrapper idy-radio-smoke-yes">';
		$inputstr.= '<input class="' . $inputClass . '" type="radio" id="insurediy-smoke-1" value="1" name="' . $this->name . '" ' . (($value) ? $checked : '') . ' />';
		$inputstr.= '<label for="insurediy-smoke-1" ' . (($value) ? $class : '') . '>' . JText::_("JYES") . '</label>';
		$inputstr.= '</div>';

		$inputstr.= '<div class="idy-radio-wrapper idy-radio-smoke-no">';
		$inputstr.= '<input class="' . $inputClass . '" type="radio" id="insurediy-smoke-2" value="0" name="' . $this->name . '" ' . (($value) ? '' : $checked) . ' />';
		$inputstr.= '<label for = "insurediy-smoke-2" ' . (($value) ? '' : $class) . '>' . JText::_("JNO") . '</label>';
		$inputstr.= '</div>';

		$inputstr.= '</div>';

		return $inputstr;
	}

}
