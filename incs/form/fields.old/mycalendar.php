<?php

/**
 * @package     Joomla.Platform
 * @subpackage  Form
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE
 */
defined('JPATH_PLATFORM') or die;

JFormHelper::loadFieldClass('calendar');

class JFormFieldMyCalendar extends JFormFieldCalendar {

	protected $type = 'MyCalendar';

	protected function getInput() {
		// Translate placeholder text
		$hint = $this->translateHint ? JText::_($this->hint) : $this->hint;

		// Initialize some field attributes.
		$format = $this->format;
		$notime = isset($this->element['notime']) ? (int) $this->element['notime'] : FALSE;

		// Build the attributes array.
		$attributes = array();

		empty($this->size) ? null : $attributes['size'] = $this->size;
		empty($this->maxlength) ? null : $attributes['maxlength'] = $this->maxlength;
		empty($this->class) ? null : $attributes['class'] = $this->class;
		!$this->readonly ? null : $attributes['readonly'] = '';
		!$this->disabled ? null : $attributes['disabled'] = '';
		empty($this->onchange) ? null : $attributes['onchange'] = $this->onchange;
		empty($hint) ? null : $attributes['placeholder'] = $hint;
		$this->autocomplete ? null : $attributes['autocomplete'] = 'off';
		!$this->autofocus ? null : $attributes['autofocus'] = '';

		if ($this->required) {
			$attributes['required'] = '';
			$attributes['aria-required'] = 'true';
		}

		// Handle the special case for "now".
		if (strtoupper($this->value) == 'NOW') {
			$this->value = strftime($format);
		}

		// Get some system objects.
		$config = JFactory::getConfig();
		$user = JFactory::getUser();

		// If a known filter is given use it.
		switch (strtoupper($this->filter)) {
			case 'SERVER_UTC':
				// Convert a date to UTC based on the server timezone.
				if ((int) $this->value) {
					// Get a date object based on the correct timezone.
					$date = JFactory::getDate($this->value, 'UTC');
					$date->setTimezone(new DateTimeZone($config->get('offset')));

					// Transform the date string.
					if ($notime) {
						$this->value = $date->format('d-m-Y', true, false);
					}
				}

				break;

			case 'USER_UTC':
				// Convert a date to UTC based on the user timezone.
				if ((int) $this->value) {
					// Get a date object based on the correct timezone.
					$date = JFactory::getDate($this->value, 'UTC');

					$date->setTimezone(new DateTimeZone($user->getParam('timezone', $config->get('offset'))));

					// Transform the date string.
					if ($notime) {
						$this->value = $date->format('d-m-Y', true, false);
					}
				}

				break;
		}

		// Including fallback code for HTML5 non supported browsers.
		JHtml::_('jquery.framework');
		JHtml::_('script', 'system/html5fallback.js', false, true);

		return JHtml::_('calendar', $this->value, $this->name, $this->id, $format, $attributes);
	}

}
