<?php

defined('_JEXEC') or die('Restricted access');

class MyHelper {

	public static function debug($var = NULL, $heading = FALSE, $return = FALSE, $hidden = FALSE) {
		$msg = array();
		$msg[] = $hidden ? "<pre style='display:none'>" : "<pre>";
		$msg[] = $heading ? "<strong>{$heading}</strong>:\n" : '';
		$msg[] = print_r($var, TRUE);
		$msg[] = "\n</pre>\n";
		$msg = implode('', $msg);
		if ($return) {
			return $msg;
		} else {
			echo $msg;
		}
	}

	public static function getUrls($key = NULL) {
		return MyUri::getUrls($key);
	}

	public static function getCurrentComponentName($withPrefix = TRUE) {
		global $option;
		if (empty($option)) {
			$option = JRequest::getVar('option');
		}
		return self::getComponentName($option, $withPrefix);
	}

	public static function getComponentName($component, $withPrefix = FALSE) {
		if (empty($component)) {
			return $component;
		}
		$component = (string) $component;
		if (!$withPrefix) {
			$strpos = stripos($component, '_');
			if (FALSE !== $strpos) {
				$component = substr($component, ($strpos + 1));
			}
		}
		return $component;
	}

	public static function json_function_check() {
		static $jsonChecked = NULL;
		if (!isset($jsonChecked)) {
			$jsonChecked = TRUE;
			require_once(JPATH_SITE . DS . 'incs' . DS . 'lib' . DS . 'jsonwrapper' . DS . 'jsonwrapper.php');
		}
	}

	public static function json_encode($value) {
		self::json_function_check();
		return json_encode($value);
	}

	public static function json_decode($json) {
		self::json_function_check();
		return json_decode($json);
	}

	public static function is_email($email) {
		$data = FALSE;
		if ($email) {
			$data = preg_match("/^[a-zA-Z0-9.!#$%&‚Äô*+\/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/iu", $email);
		}
		return $data;
	}

	public static function setSessionData($ss_key, $data = array(), $limit = 10) {
		if ($ss_key) {
			$session = JFactory::getSession();
			$ss_keys = $session->get('my_datas_keys', array()); //get session
			$ss_keys[$ss_key] = $ss_key;
			if (count($ss_keys) > $limit) {
				$_ss_key = array_shift($ss_keys);
				if ($_ss_key) {
					$session->clear('my_datas', $_ss_key);
				}
			}
			$session->set('my_datas_keys', $ss_keys);
			$session->set('my_datas', $data, $ss_key);
			return TRUE;
		}
		return FALSE;
	}

	public static function getSessionData($ss_key) {
		$data = NULL;
		if ($ss_key) {
			$session = JFactory::getSession();
			$data = $session->get('my_datas', array(), $ss_key); //get session
		}
		return $data;
	}

	public static function trim_recursive($var) {
		if ($var) {
			$is_array = is_array($var);
			$is_object = is_object($var);
			if ($is_array || $is_object) {
				foreach ($var as $k => $v) {
					$v = self::trim_recursive($v);
					($is_array) ? $var[$k] = $v : $var->$k = $v;
				}
			} else {
				if (is_string($var)) {
					$var = trim($var);
				}
			}
		}
		return $var;
	}

	public static function mycurl($url, $post = NULL, $timeout = 30) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		$agent = "Mozilla/5.0 (Windows NT 6.1; rv:2.0) Gecko/20100101 Firefox/4.0";
		curl_setopt($ch, CURLOPT_USERAGENT, $agent);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		//curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		if (!is_null($post) && $post) {
			curl_setopt($ch, CURLOPT_POST, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		}
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
		curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
		$result = curl_exec($ch);
		curl_close($ch);
		return $result;
	}

	public static function getIncludeContents($filename, $params = array()) {
		if (is_file($filename)) {
			//set params
			if ($params) {
				foreach ($params as $k => $v) {
					$$k = $v;
				}
				unset($params, $k, $v);
			}

			ob_start();
			include $filename;
			$contents = ob_get_contents();
			ob_end_clean();
			return $contents;
		}
		return FALSE;
	}

	public static function formatPagination($pgHtml = '', $attr = '') {
		if ($pgHtml) {
			$pgHtml = str_replace('&amp;', '&', $pgHtml);
			if (stripos($pgHtml, 'href') !== FALSE) {
				$keyword = 'limitstart=';
				$copyAttr = $attr;
				while (TRUE) {
					$matches = array();
					$pattern = "/href\=[\"|\']([\w\?\&\=\.\/\s]+)[\"|\']/i";
					if (preg_match($pattern, $pgHtml, $matches)) {
						$matches[$keyword] = 0;
						$matches['strpos'] = strpos($matches[1], $keyword);
						if ($matches['strpos'] !== FALSE) {
							$matches[1] = str_replace('?', '&', $matches[1]);
							$matches['link_params'] = explode('&', $matches[1]);
							foreach ($matches['link_params'] as $param) {
								if (strpos($param, $keyword) !== FALSE) {
									$matches['link_param'] = explode('=', $param);
									$matches[$keyword] = (int) $matches['link_param'][1];
									break;
								}
							}
						}

						if (empty($matches[$keyword])) {
							$matches[$keyword] = 0;
						}
						$matches['page'] = ($matches[$keyword] + 1);


						$attr = $copyAttr;
						if ($attr) {
							$attr = str_replace('{limitstart}', $matches[$keyword], $attr);
						} else {
							$attr = "onclick=\"return false;\"";
						}

						$matches['replace'] = "href=\"#page{$matches['page']}\" {$attr}";
						$pgHtml = str_replace($matches[0], $matches['replace'], $pgHtml);
					} else {
						break;
					}
				}
			}
		}
		return $pgHtml;
	}

	public static function text2array($txt = '', $newLineChar = "\n") {
		$data = array();
		if (is_string($txt) && $txt) {
			$data = explode("{$newLineChar}", $txt);
		}
		return $data;
	}

	public static function addLogs($fileName, $logInf, $logType = 'error') {
		//making log message
		$str = "[" . date("D M d H:i:s Y") . "] [" . $logType . "] [client " . getenv("REMOTE_ADDR") . "] ";
		//appending || writing
		$fmode = (file_exists($fileName)) ? 'a' : 'w';
		//opening log file
		$fp = @fopen($fileName, $fmode);
		//writing message to log file
		@fputs($fp, $str . $logInf . "\r\n");
		//closing log file
		fclose($fp);
	}

	public static function arr2arrList($arr, $preserveKey = FALSE, $keyName = NULL, $valName = NULL) {
		$data = array();
		if (is_array($arr) && $arr) {
			foreach ($arr as $k => $v) {
				if (empty($keyName) || empty($valName)) {
					$_arr = array($k, $v);
				} else {
					$_arr = array(
						$keyName => $k,
						$valName => $v
					);
				}
				($preserveKey) ? $data[$k] = $_arr : $data[] = $_arr;
			}
		}
		return $data;
	}

	public static function objArr2OptionsList($obj, $key = 'id', $values = 'title', $titleChars = array(), $opts = array()) {
		$items = array();
		if (!isset($titleChars)) {
			$titleChars = array();
		}
		if (!isset($titleChars['prefix'])) {
			$titleChars['prefix'] = '&nbsp;';
		}
		if (!isset($titleChars['join'])) {
			$titleChars['join'] = ' - ';
		}

		foreach ($obj as $k => $item) {
			$is_object = $is_array = FALSE;
			if (is_object($item)) {
				$is_object = TRUE;
				$_key = $item->$key;
				$_value = array();
				if (!is_array($values)) {
					$values = (array) $values;
				}
				foreach ($values as $value) {
					$_value[] = $item->$value;
				}
				$_value = implode($titleChars['join'], $_value);
			} elseif (is_array($item)) {
				$is_array = TRUE;
				$_key = $item[$key];
				$_value = array();
				if (!is_array($values)) {
					$values = (array) $values;
				}
				foreach ($values as $value) {
					$_value[] = $item[$value];
				}
				$_value = implode($titleChars['join'], $_value);
			} else {
				continue;
			}

			if ($opts && is_array($opts['keys']) && $opts['keys']) {
				foreach ($opts['keys'] as $_key2) {
					if ($opts[$_key2]) {
						$_value2 = '';
						if ($is_object) {
							if (isset($opts[$_key2][$item->$_key2])) {
								$_value2 = $opts[$_key2][$item->$_key2];
							}
						} elseif ($is_array) {
							if (isset($opts[$_key2][$item[$_key2]])) {
								$_value2 = $opts[$_key2][$item[$_key2]];
							}
						}

						if ($_value2) {
							$_value .= $_value2;
						}
					}
				}
			}
			$items[$k] = JHTML::_('select.option', $_key, $titleChars['prefix'] . $_value);
		}
		return $items;
	}

	public static function array2jObject($arr = FALSE) {
		return self::array2object($arr, TRUE);
	}

	public static function array2object($arr = FALSE, $toJObj = FALSE) {
		$obj = $toJObj ? new JObject() : new stdClass();
		if (is_array($arr) && $arr) {
			foreach ($arr as $k => $v) {
				$obj->$k = $v;
			}
		}
		return $obj;
	}

	public static function object2array($obj = FALSE) {
		$arr = array();
		if (is_object($obj) && $obj) {
			foreach ($obj as $k => $v) {
				$arr[$k] = $v;
			}
		}
		return $arr;
	}

	public static function escapeDBString($str, $quote = TRUE, $extra = FALSE, $quoteNumber = TRUE) {
		global $db;
		if (!isset($db)) {
			$db = & JFactory::getDBO();
		}

		if ($str) {
			$str = $db->escape($str, $extra);
		}

		if ($quote) {
			$is_null = is_null($str);
			$is_numeric = is_numeric($str);
			if ($is_null) {
				$str = 'NULL';
			} elseif ($is_numeric) {
				$str = ($quoteNumber) ? $db->Quote($str, FALSE) : $str;
			} else {
				$str = $db->Quote($str, FALSE);
			}
		}
		return $str;
	}

	public static function escapeDBString_recursive($var, $quote = TRUE, $extra = FALSE) {
		$is_array = is_array($var);
		$is_object = is_object($var);
		if ($is_array || $is_object) {
			foreach ($var as $k => $v) {
				$v = self::escapeDBString_recursive($v, $quote, $extra);
				($is_array) ? $var[$k] = $v : $var->$k = $v;
			}
		} else {
			$var = self::escapeDBString($var, $quote, $extra);
		}
		return $var;
	}

	public static function escapeCSV($csv, $quote = FALSE) {
		$is_array = is_array($csv);
		$is_object = is_object($csv);
		if ($is_array || $is_object) {
			foreach ($csv as $k => $v) {
				$v = self::escapeCSV($v, $quote);
				($is_array) ? $csv[$k] = $v : $csv->$k = $v;
			}
		} else {
			$csv = str_replace('"', '""', $csv);
			if ($quote) {
				$csv = '"' . $csv . '"';
			}
		}
		return $csv;
	}

	//for display, will add tz to the date
	public static function formatDate($date = 'now', $dateFormat = 'datetime', $tz = 'CONFIG') {
		if ($dateFormat == 'date') {
			$dateFormat = '%d/%m/%Y';
		} elseif ($dateFormat == 'time') {
			$dateFormat = '%H:%M:%S';
		} elseif ($dateFormat == 'datetime') {
			$dateFormat = '%d/%m/%Y %H:%M:%S';
		} else {
			//own format
//			$dateFormat = $dateFormat;
		}

		if (is_string($tz) && strtoupper($tz) == 'CONFIG') {
			$tz = NULL; //joomla will handle it
		}
		return JHTML::date($date, $dateFormat, $tz);
	}

	//use GMT
	public static function MySqlDate($date = 'now', $dateFormat = 'datetime', $local = FALSE) {
		if ($dateFormat == 'date') {
			$dateFormat = '%Y-%m-%d';
		} elseif ($dateFormat == 'time') {
			$dateFormat = '%H:%M:%S';
		} elseif ($dateFormat == 'datetime') {
			$dateFormat = '%Y-%m-%d %H:%M:%S';
		}
		$tz = ($local) ? 'CONFIG' : 0;
		return self::formatDate($date, $dateFormat, $tz);
	}

	public static function initVar($var, $val = NULL) {
		if (!isset($var)) {
			$var = $val;
		}
		return $var;
	}

	public static function getUserIP(&$proxy = '') {
		global $_SERVER;
		if (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
				$proxy = $_SERVER['HTTP_CLIENT_IP'];
			} else {
				$proxy = $_SERVER['REMOTE_ADDR'];
			}
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
				$ip = $_SERVER['HTTP_CLIENT_IP'];
			} else {
				$ip = $_SERVER['REMOTE_ADDR'];
			}
		}
		return $ip;
	}

	public static function getRandValue($len = 1, $val = '23456789abcdefghkmnpqrstwxyz') {
		//1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ
		//23456789abcdefghkmnpqrstwxyzABCDEFGHKMNPQRSTWXYZ
		//23456789abcdefghkmnpqrstwxyz
		$str = '';
		$len = (int) $len;
		if ($len == 0)
			return $str;
		if ($len < 0)
			$len = abs($len);
		if (!$val)
			$val = '1234567890abcdefghijklmnopqrstuvwxyz';
		$valLen = strlen($val) - 1;
		for ($i = 0; $i < $len; $i++) {
			$str .= $val[rand(0, $valLen)];
		}
		return $str;
	}

	public static function getRandValueByArray($lens = array(), $val = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ') {
		$str = '';
		if (is_array($lens) && $lens) {
			$len = array();
			foreach ($lens as $v) {
				if (!is_array($v)) {
					$len[0] = $v;
					$len[1] = $val;
				} else {
					$len[0] = $v[0];
					$len[1] = $v[1];
				}
				$str .= self::getRandValue($len[0], $len[1]);
			}
		}
		return $str;
	}

	public static function getHashValueByArray($vals = array(), $arrKeys = array(), $glue = '_') {
		$res = '';
		if (is_array($vals) && $vals) {
			if (!is_array($arrKeys)) {
				$arrKeys = (array) $arrKeys;
			}

			if (empty($arrKeys)) {
				$arrKeys = array_keys($vals);
			}

			$arr = array();
			foreach ($arrKeys as $v) {
				$arr[] = $vals[$v];
			}

			$res = md5(implode($glue, $arr));
		} else {
			$vals = (array) $vals;
			$res = md5(implode($glue, $vals));
		}
		return $res;
	}

	public static function strlen($str, $isUnicode = FALSE, $encoding = 'UTF-8') {
		$count = 0;
		$str = trim($str);
		if ($str) {
			if (!$isUnicode) {
				$count = strlen($str);
			} else {
				if (is_null($encoding)) {
					$encoding = mb_internal_encoding();
				}
				$count = mb_strlen($str, $encoding);
			}
		}
		return $count;
	}

	public static function wordCount($str) {
		$count = 0;
		$str = trim($str);
		if ($str) {
			$pattern = "/[^A-Za-z0-9\'\"\-\s]+/i";
			$str = preg_replace($pattern, '', $str);

			$pattern = "/[^A-Za-z0-9\'\"]+/i";
			$str = preg_replace($pattern, ' ', $str);

			$strArr = explode(' ', $str);
			$count = count($strArr);
		}
		return $count;
	}

	public static function shuffle_assoc(&$array) {
		if (count($array) > 1) {
			$new = array();
			$keys = array_keys($array);
			shuffle($keys);
			foreach ($keys as $k) {
				$new[$k] = $array[$k];
			}
			$array = $new;
		}
		return true;
	}

	public static function removeItemByValue($arr, $val = '', $preserve_keys = TRUE) {
		if (empty($arr) || !is_array($arr)) {
			return $arr;
		}
		if (!in_array($val, $arr)) {
			return $arr;
		}
		foreach ($arr as $k => $v) {
			if ($v == $val) {
				unset($arr[$k]);
			}
		}
		return ($preserve_keys) ? $arr : array_values($arr);
	}

	public static function getArrayByKeys(&$input, $keys = array(), $deleteAfterAssign = FALSE) {
		$arr = array();
		if (is_array($input) && $input && is_array($keys) && $keys) {
			foreach ($keys as $v) {
				$arr[$v] = $input[$v];
				if ($deleteAfterAssign) {
					unset($input[$v]);
				}
			}
		}
		return $arr;
	}

	public static function groupArrayByKey($input, $key = NULL, $overwrite = FALSE, $preserveKey = FALSE) {
		$arr = array();
		if (is_array($input) && $input) {
			foreach ($input as $k => $v) {
				$_v = !is_array($v) ? (array) $v : $v;

				$keyVal = $_v[$key];
				if ($overwrite) {
					$arr[$keyVal] = $v;
				} else {
					if ($preserveKey) {
						$arr[$keyVal][$k] = $v;
					} else {
						$arr[$keyVal][] = $v;
					}
				}
			}
		}
		return $arr;
	}

	public static function swapValues(&$input1, &$input2) {
		$swap = $input1;
		$input1 = $input2;
		$input2 = $swap;
	}

	public static function round($val, $precision = 0, $mode = NULL) {
		if (is_null($mode)) {
			$mode = PHP_ROUND_HALF_UP;
		}

		if ('literal' == $mode) {
			$val = '' . (0 + @$val);
			if ($precision < 0) {
				$precision = 0;
			}

			$part = '';
			$strpos = strpos($val, '.');
			if (FALSE !== $strpos) {
				$part = substr($val, ($strpos + 1));
				$val = substr($val, 0, $strpos);
				if (0 == $precision) {
					$part = '';
				} else {
					$strlen = strlen($part);
					if ($strlen > $precision) {
						$part = '.' . substr($part, 0, $precision);
					} elseif ($strlen < $precision) {
						$part = '.' . str_pad($part, $precision, '0');
					} else {
						$part = '.' . $part;
					}
				}
			} else {
				if ($precision > 0) {
					$part = '.' . str_repeat('0', $precision);
				}
			}
			$res = $val . $part;
		} else {
			$res = round($val, $precision, $mode);
		}
		return $res;
	}

	public static function formatCurrency($number = 0, $currency = '$', $negValueSign = 'parentheses', $decimals = 2, $dec_point = '.', $thousands_sep = ',') {
		$number = 0 + $number;
		if ($number < 0) {
			$number = abs($number);
			$data = $currency . number_format($number, $decimals, $dec_point, $thousands_sep);
			switch ($negValueSign) {
				case 'parentheses':case '(':case ')':
					$data = "({$data})";
					break;
				case '-':
				default:
					$data = '-' . $data;
					break;
			}
			return $data;
		} else {
			return $currency . number_format($number, $decimals, $dec_point, $thousands_sep);
		}
	}

	public static function timeSpanDuration($start, $end = null, $format = '*', $ignore = TRUE) {
		$data = array();
		$end = $end ? $end : time();
		if ($start > $end) {
			self::swapValues($start, $end);
		}
		$duration = $end - $start;

		if (!$format) {
			$format = '*';
		}
		$format = str_replace(' ', '', $format);
		$format = explode(',', $format);

		if ($format) {
			$_formats = array(
				'day' => 86400,
				'hour' => 3600,
				'minute' => 60,
				'second' => 1,
			);
			foreach ($_formats as $k => $v) {
				$arrVal = $k;
				$in_array_all = in_array('*', $format);
				$in_array_custom = in_array($arrVal, $format);
				if ($in_array_all || $in_array_custom) {
					if ($in_array_custom) {
						$format = self::removeItemByValue($format, $arrVal);
					}
					$arrCount = count($format);
					if ($arrCount <= 0) {
						$ignore = FALSE;
					}
					self::_timeSpanDuration($data, $duration, $v, $arrVal, $ignore);
				}
			}
		}
		return implode(' ', $data);
	}

	protected static function _timeSpanDuration(&$data, &$duration, $gap, $unit, $ignore = TRUE) {
		if ($duration && $duration > $gap) {
			$freq = floor($duration / $gap);
			$duration -= $gap * $freq;
			$data[] = "{$freq} {$unit}";
		} else {
			if (!$ignore) {
				$data[] = "0 {$unit}";
			}
		}
	}

	public static function getTimeRange($tFrom, $tTo, $inc = 1, $reset = NULL, $format = '') {
		$data = array();
		$tFrom = (int) $tFrom;
		$tTo = (int) $tTo;
		$inc = (int) $inc;
		$reset = (int) $reset;

		if ($tFrom == $tTo) {
			$tFrom = 0;
			$tTo = $reset;
		} else {
			if ($tFrom > $tTo) {
				if ($tTo == 0) {
					$tTo = $reset;
					$tTo2 = 0;
				} else {
					//remain $tFrom
					$tTo2 = $tTo; //swap
					$tTo = $reset;
					$tFrom2 = 0;
				}
			}
		}

		for ($i = $tFrom; $i <= $tTo; $i+=$inc) {
			$val = $format ? sprintf("{$format}", $i) : $i;
			$data[$val] = $val;
		}

		if (isset($tTo2)) {
			if (isset($tFrom2)) {
				for ($i = $tFrom2; $i <= $tTo2; $i+=$inc) {
					$val = $format ? sprintf("{$format}", $i) : $i;
					$data[$val] = $val;
				}
			} else {
				$val = $format ? sprintf("{$format}", $tTo2) : $tTo2;
				$data[$val] = $val;
			}
		}
		return $data;
	}

	public static function calculatePercentage($percent = 0, $subject = 1) {
		if ($percent == 0 || $subject == 0) {
			return 0;
		}
		return ($percent / 100) * $subject;
	}

	public static function getPercentageRate($percentAmount = 0, $base = 1, $isPercent = TRUE) {
		$data = 0;
		if ($percentAmount == $base) {
			$data = 1;
		} else {
			if ($base == 0) {
				return FALSE;
			}

			if ($percentAmount == 0) {
				$data = 0;
				$isPercent = FALSE;
			} else {
				$data = ($percentAmount / $base);
			}
		}

		if ($isPercent) {
			$data = $data * 100;
		}
		if ($data < 0) {
			$data *= -1;
		}
		return $data;
	}

	public static function extractCBracketFromText($txt = '', $unique = TRUE) {
		$data = array();
		if ($txt) {
			$matches = array();
			$pattern = "/\{(\w+[\.?\w+]+)\}/i";
			if (preg_match_all($pattern, $txt, $matches)) {
				if ($matches) {
					array_shift($matches); //remove first matched value
				}
				if ($matches) {
					$data = $unique ? array_unique($matches[0]) : $matches[0];
				}
			}
		}
		return $data;
	}

	public static function replaceCBracketFromText($CBracketTexts = array(), $txt = '', $replace = array(), $lookFrom = NULL) {
		if ($CBracketTexts && $txt) {
			foreach ($CBracketTexts as $CBracketText) {
				$parts = explode('.', $CBracketText);
				if ($parts) {
					$value = $lookFrom;
					$counter = 1;
					foreach ($parts as $part) {
						$_part = $part;
						if ($counter == 1) {
							$counter++;
							if ($replace && $replace[$_part]) {
								$_part = $replace[$_part];
							}
						}
						$value = self::getValueFromVariable($_part, $value);
					}
					$txt = str_replace('{' . $CBracketText . '}', $value, $txt);
				}
			}
		}
		return $txt;
	}

	public static function getValueFromVariable($input, $lookFrom = NULL) {
		$data = '';
		if (isset($input)) {
			if (is_null($lookFrom)) {
				$lookFrom = $GLOBALS;
			}

			$is_array = is_array($lookFrom);
			$is_object = is_object($lookFrom);
			if ($is_array || $is_object) {
				if ($is_array && isset($lookFrom[$input])) {
					$data = $lookFrom[$input];
				} elseif ($is_object && isset($lookFrom->$input)) {
					$data = $lookFrom->$input;
				}
			}
		}
		return $data;
	}

	public static function centralizeEmailContent($content = '', $width = '800px') {
		$content = '<div style="width:' . $width . '; margin-left:auto; margin-right:auto;">'
				. '<div>'
				. $content
				. '</div>'
				. '</div>';
		return $content;
	}

	public static function nl2br($str = '', $is_xhtml = TRUE) {
		//php nl2br 2nd param is not compatible with older version
		if ($str) {
			$pattern = "/\r\n|\n|\r/";
			$replacement = $is_xhtml ? "<br />\n" : "<br>\n";
			$str = preg_replace($pattern, $replacement, $str);
		}
		return $str;
	}

	public static function nl2br2($arrStr = array(), $arrKeys = array(), $is_xhtml = TRUE) {
		if (is_array($arrStr) && $arrStr) {
			if (!is_array($arrKeys) || empty($arrKeys)) {
				$arrKeys = array_keys($arrStr);
			}
			foreach ($arrKeys as $k) {
				$v = $arrStr[$k];
				if (isset($v) && is_string($v)) {
					$arrStr[$k] = self::nl2br($v, $is_xhtml);
				}
			}
		}
		return $arrStr;
	}

	public static function truncateText($string, $length = 80, $etc = '...', $break_words = FALSE, $middle = FALSE) {
		if ($length == 0)
			return '';

		if (strlen($string) > $length) {
			$length -= min($length, strlen($etc));
			if (!$break_words && !$middle) {
				$string = preg_replace('/\s+?(\S+)?$/', '', substr($string, 0, $length + 1));
			}
			if (!$middle) {
				return substr($string, 0, $length) . $etc;
			} else {
				return substr($string, 0, $length / 2) . $etc . substr($string, -$length / 2);
			}
		} else {
			return $string;
		}
	}

	public static function arr2number($arr = array(), $removeMin = NULL, $unique = TRUE, $removeMax = NULL) {
		$data = array();
		if (is_array($arr) && $arr) {
			$data = $arr;
			unset($arr);
			foreach ($data as $k => $v) {
				$v = 0 + $v;
				if (isset($removeMin)) {
					if ($v < $removeMin) {
						unset($data[$k]);
						continue;
					}
				}
				if (isset($removeMax)) {
					if ($v > $removeMax) {
						unset($data[$k]);
						continue;
					}
				}
				$data[$k] = $v;
			}
			if ($unique && $data) {
				$data = array_unique($data);
			}
		}
		return $data;
	}

	public static function arr2int($arr = array(), $positiveOnly = TRUE, $unique = TRUE) {
		$data = array();
		if (is_array($arr) && $arr) {
			$data = $arr;
			unset($arr);
			foreach ($data as $k => $v) {
				$v = (int) $v;
				if ($v < 1) {
					if ($positiveOnly) {
						unset($data[$k]);
						continue;
					}
				}
				$data[$k] = $v;
			}
			if ($unique && $data) {
				$data = array_unique($data);
			}
		}
		return $data;
	}

	public static function htmlspecialchars_recursive($str, $flags = '', $encoding = 'UTF-8', $double_encode = TRUE) {
		if ($str) {
			$flags = (empty($flags)) ? ENT_COMPAT | ENT_HTML401 : $flags;
			$is_array = is_array($str);
			$is_object = is_object($str);
			if ($is_array || $is_object) {
				foreach ($str as $k => $v) {
					$v = self::htmlspecialchars_recursive($v, $flags, $encoding, $double_encode);
					($is_array) ? $str[$k] = $v : $str->$k = $v;
				}
			} else {
				if (is_string($str)) {
					$str = htmlspecialchars($str, $flags, $encoding, $double_encode);
				}
			}
		}
		return $str;
	}

	public static function pathinfo($path) {
		$data = array();
		if ($path) {
			$data = pathinfo($path);
			if ($data && empty($data['filename'])) {
				if (empty($data['extension'])) {
					$data['filename'] = $data['basename'];
				} else {
					$strlen = strlen($data['extension']) + 1; //with '.'
					$data['filename'] = substr($data['basename'], -$strlen);
				}
			}
		}
		return $data;
	}

	public static function mkdir($path, $mode = 0777, $recursive = FALSE) {
		$res = TRUE;
		if (!file_exists($path)) {
			$res = @mkdir($path, $mode, $recursive);
		}
		if ($res) {
			@chmod($path, $mode);
		}
		return $res;
	}

	public static function chmod($path, $mode = 0777) {
		$res = FALSE;
		if (file_exists($path)) {
			$res = @chmod($path, $mode);
		}
		return $res;
	}

	public static function chmod_recursive($path, $mode = 0777, $stopOnFailure = TRUE) {
		if (!file_exists($path)) {
			return FALSE;
		} else {
			if (is_file($path)) {
				return @chmod($path, $mode);
			}
			$dh = opendir($path);
			while (($file = readdir($dh)) !== FALSE) {
				if ($file != '.' && $file != '..') {
					$fullpath = $path . '/' . $file;
					if (is_link($fullpath)) {
						continue;
						//return FALSE;
					} elseif (is_dir($fullpath)) {
						if (!self::chmod_recursive($fullpath, $mode, $stopOnFailure)) {
							if ($stopOnFailure) {
								return FALSE;
							}
						}
					} elseif (is_file($fullpath) && !@chmod($fullpath, $mode)) {
						if ($stopOnFailure) {
							return FALSE;
						}
					}
				}
			}
			closedir($dh);
			return (@chmod($path, $mode)) ? TRUE : FALSE;
		}
	}

	public static function fileDownload($filePath, $str = NULL, $docTitle = NULL) {
//		$jConf = JFactory::getConfig();
//		$charset = $jConf->get('charset', 'UTF-8');
		$charset = "UTF-8";
		if (headers_sent()) {
			die(''); // Must be fresh start
		}

		// Required for some browsers
		if (ini_get('zlib.output_compression'))
			ini_set('zlib.output_compression', 'Off');

		//is string
		$is_string = is_null($str) ? FALSE : TRUE;

		// is string OR File Exists?
		if ($is_string || file_exists($filePath)) {
			// Parse Info / Get Extension
			$fsize = $is_string ? strlen($str) : filesize($filePath);
			$pathinfo = (!is_null($docTitle) && $docTitle) ? pathinfo($docTitle) : pathinfo($filePath);

			// Determine Content Type
			switch (strtolower($pathinfo['extension'])) {
				case 'pdf': $ctype = 'application/pdf';
					break;
				case 'exe': $ctype = 'application/octet-stream';
					break;
				case 'css': $ctype = 'text/css';
					break;
				case 'txt': $ctype = 'text/plain';
					break;
				case 'ics': $ctype = 'text/calendar';
					break;
				case 'zip': $ctype = 'application/zip';
					break;
				case 'doc': $ctype = 'application/msword';
					break;
				case 'xls': $ctype = 'application/vnd.ms-excel';
					break;
				case 'ppt': $ctype = 'application/vnd.ms-powerpoint';
					break;
				case 'gif': $ctype = 'image/gif';
					break;
				case 'png': $ctype = 'image/png';
					break;
				case 'jpeg': case 'jpg':
					$ctype = 'image/jpg';
					break;
				case 'csv': $ctype = 'text/csv';
					break;
				default: $ctype = 'application/force-download';
			}

			if (ereg('Opera(/| )([0-9].[0-9]{1,2})', $_SERVER['HTTP_USER_AGENT'])) {
				$UserBrowser = "Opera";
			} elseif (ereg('MSIE ([0-9].[0-9]{1,2})', $_SERVER['HTTP_USER_AGENT'])) {
				$UserBrowser = "IE";
			} else {
				$UserBrowser = '';
			}

			$headers = array();
			$headers[] = 'Expires: ' . gmdate('D, d M Y H:i:s') . ' GMT';
			if ($ctype == 'text/csv') {
				$headers[] = "Content-Encoding: {$charset}";
				$headers[] = "Content-Type: {$ctype}; charset={$charset}";
			} else {
				$headers[] = "Content-Type: {$ctype}";
			}
			$headers[] = 'Content-Transfer-Encoding: binary';
			$headers[] = "Content-Length: {$fsize}";

			if ($UserBrowser == 'IE') {
				$headers[] = "Content-Disposition: attachment; filename=\"{$pathinfo['basename']}\"";
				$headers[] = 'Cache-Control: must-revalidate, post-check=0, pre-check=0';
				$headers[] = 'Pragma: public';
			} else {
				$headers[] = "Content-Disposition: attachment; filename=\"{$pathinfo['basename']}\"";
				$headers[] = 'Pragma: no-cache';
			}

			foreach ($headers as $v) {
				header($v);
			}

			ob_clean();
			flush();

			if ($is_string) {
				echo $str;
				//				echo mb_convert_encoding($str, "Shift-JIS", "UTF-8"); // convert to SJIS for japanese characters
			} else {
				readfile($filePath);
			}
			exit;
		} else {
			die('File Not Found');
		}
	}

	public static function createPdf($content, $fileName = NULL, $folderName = NULL, $params = array()) {
		include_once(JPATH_SITE . DS . 'incs' . DS . 'lib' . DS . 'tcpdf' . DS . 'config' . DS . 'lang' . DS . 'eng.php');
		include_once(JPATH_SITE . DS . 'incs' . DS . 'lib' . DS . 'tcpdf' . DS . 'tcpdf.php');

		$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, TRUE, 'UTF-8', FALSE);

		$filePath = JPATH_SITE . DS . 'tmp' . DS . 'pdfs';
		self::mkdir($filePath, 0777);

		if (!is_null($folderName) && $folderName) {
			$filePath .= DS . $folderName;
			self::mkdir($filePath, 0777);
		}

		if (empty($fileName)) {
			$fileName = 'pdf';
		}

		if (!isset($params['title'])) {
			$params['title'] = $fileName;
		}

		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor($params['author']);
		$pdf->SetTitle($params['title']);
		$pdf->SetSubject($params['subject']);
		$pdf->SetKeywords($params['keywords']);
		$pdf->SetDisplayMode('fullwidth', 'continuous');

		$params['has_header'] = (isset($params['header']) && $params['header']);
		$params['has_footer'] = (isset($params['footer']) && $params['footer']);

		// remove default header/footer
		$pdf->setPrintHeader($params['has_header']);
		$pdf->setPrintFooter($params['has_footer']);

		// set header and footer fonts
		if ($params['has_header']) {
//			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
			$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', 6));
			$pdf->SetHeaderMargin(PDF_MARGIN_HEADER); //margins

			$params['header']['logo'] = (isset($params['header']['logo']) && $params['header']['logo']) ?
					$params['header']['logo'] : '';

			$params['header']['logo_width'] = (isset($params['header']['logo_width']) && $params['header']['logo_width'] > 0) ?
					$params['header']['logo'] : 0;

			$params['header']['title'] = (isset($params['header']['title']) && $params['header']['title']) ?
					$params['header']['title'] : '';

			$params['header']['content'] = (isset($params['header']['content']) && $params['header']['content']) ?
					$params['header']['content'] : '';

			$pdf->SetHeaderData($params['header']['logo'], $params['header']['logo_width'], $params['header']['title'], $params['header']['content']);
		}
		if ($params['has_footer']) {
			$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
			$pdf->SetFooterMargin(PDF_MARGIN_FOOTER); //margins
		}

		//set margins
//		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		if (isset($params['margin']) && $params['margin']) {
			if (isset($params['margin']['top']) && is_numeric($params['margin']['top'])) {
				$pdf->SetTopMargin($params['margin']['top']);
			}
			if (isset($params['margin']['left']) && is_numeric($params['margin']['left'])) {
				$pdf->SetLeftMargin($params['margin']['left']);
			}
			if (isset($params['margin']['right']) && is_numeric($params['margin']['right'])) {
				$pdf->SetRightMargin($params['margin']['right']);
			}
		}

		//set language
		// $pdf->setLanguageArray(array('w_page'=>'Page '));
		//set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set font
		$pdf->SetFont('helvetica', '', 6);

		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

		// add a page
		$pdf->AddPage('L');

		$pdf->writeHTML($content, FALSE, 0, TRUE, FALSE);

		// ---------------------------------------------------------
		//Close and output PDF document
		$pdf->Output($filePath . DS . $fileName, 'F');

		self::chmod($filePath . DS . $fileName, 0777);
		return $filePath . DS . $fileName;
	}

	public static function createZip($zipFilePath = NULL, $addFiles = array(), $addStrings = array(), $appendEntry = FALSE, $removeEmptyZip = TRUE) {
		$res = FALSE;
		if ($zipFilePath) {
			if ($addFiles || $addStrings) {
				$zipFlag = $appendEntry ? ZIPARCHIVE::CREATE : ZIPARCHIVE::OVERWRITE;
				$zip = new ZipArchive();
				$res = $zip->open($zipFilePath, $zipFlag);
				if ($res === TRUE) {
					if ($addFiles) {
						foreach ($addFiles as $v) {
							if (!is_array($v)) {
								$v = (array) $v;
							}
							if (file_exists($v[0])) {
								//delete same filename if any
								if (!$v[1]) {
									$v[1] = $v[0];
								}
								// @$zip->deleteName($v[1]);//delete same filename if any
								$zip->addFile($v[0], $v[1]); //filePath, filename(optional)
							}
						}
					}

					if ($addStrings) {
						foreach ($addStrings as $v) {
							if (!is_array($v)) {
								$v = (array) $v;
							}
							if ($v[0]) {
								// @$zip->deleteName($v[0]);//delete same filename if any
								$zip->addFromString($v[0], $v[1]); //filename, content
							}
						}
					}

					//check has entry in zip
					if ($removeEmptyZip) {
						$hasEntry = ($zip->numFiles > 0);
					}
				}
				$zip->close();

				//remove zip file if no entry in zip
				if ($removeEmptyZip && !$hasEntry) {
					$res = FALSE;
					@unlink($zipFilePath);
				}
			}
		}
		return $res;
	}

	public static function parseYoutubeUrl($url) {
		$pattern = '#^(?:https?://)?'; // Optional URL scheme. Either http or https.
		$pattern .= '(?:www\.)?';  // Optional www subdomain.
		$pattern .= '(?:'; // Group host alternatives:
		$pattern .= 'youtu\.be/';  // Either youtu.be,
		$pattern .= '|youtube\.com'; // or youtube.com
		$pattern .= '(?:'; // Group path alternatives:
		$pattern .= '/embed/';   // Either /embed/,
		$pattern .= '|/v/'; // or /v/,
		$pattern .= '|/watch\?v=';  // or /watch?v=,
		$pattern .= '|/watch\?.+&v='; // or /watch?other_param&v=
		$pattern .= ')'; // End path alternatives.
		$pattern .= ')'; // End host alternatives.
		$pattern .= '([\w-]{11})';  // 11 characters (Length of Youtube video ids).
		$pattern .= '(?:.+)?$#x';  // Optional other ending URL parameters.
		$matches = NULL;
		preg_match($pattern, $url, $matches);
		return (isset($matches[1])) ? $matches[1] : FALSE;
	}

	public static function isMailSecretValid($secret) {
		$jConf = JFactory::getConfig();
		$confSecret = $jConf->get('mailSecret', '');
		return (!empty($secret) && $secret == $confSecret);
	}

	public static function getImageSize($imgPath, $fileSize = FALSE) {
		$imInfo = @getimagesize($imgPath);
		if (FALSE === $imInfo) {
			return FALSE;
		}

		if ($fileSize) {
			$fsize = @filesize($imgPath);
			$imInfo['filesize'] = (FALSE === $fsize) ? 0 : $fsize;
		}
		return $imInfo;
	}

	public static function numberFormat($num, $remove = FALSE, $decimal = 2) {
		if ($remove) {
			$num = str_replace(",", "", $num);
			return $num;
		} else {
			if (is_numeric($num)) {
				$num = number_format($num, $decimal);
			}
			return $num;
		}
	}

	public static function getUniqueUsername($name = FALSE) {
		if (!$name) {
			$name = "user" . time();
		}
		$name = preg_replace('/[^A-Za-z0-9]/', '', $name);

		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$query->select("username")->from("#__users");
		$db->setQuery($query);
		$results = $db->loadAssocList("username");
		$users = array_keys($results);
		$counter = 0;
		$tmp = (strlen($name) < 30) ? $name : substr($name, 0, 29);
		while (in_array($tmp, $users)) {
			$counter ++;
			$tmp = $name . $counter;
		}
		return $tmp;
	}

	public static function renderDefaultMessage() {
		$msgList = self::getMsgList();
		$buffer = '';
		$buffer .= "\n<div id=\"system-message-container\">";

		if (is_array($msgList)) {
			if (count($msgList) < 1) {
				return '';
			}
			$buffer .= "\n<div id=\"system-message\">";
			foreach ($msgList as $type => $msgs) {
				$buffer .= "\n<div class=\"alert alert-" . $type . "\">";
				$buffer .= "<button type=\"button\" class=\"close\" data-dismiss=\"alert\">&times;</button>";
				if (count($msgs)) {
					$buffer .= "\n<h4 class=\"alert-heading\">" . JText::_($type) . "</h4>";
					$buffer .= "\n<div>";
					foreach ($msgs as $msg) {
						$buffer .= "\n\t\t<p>" . $msg . "</p>";
					}
					$buffer .= "\n</div>";
				}
				$buffer .= "\n</div>";
			}
			$buffer .= "\n</div>";
		}
		$buffer .= "\n</div>";

		return $buffer;
	}

	private static function getMsgList() {
		$lists = array();
		$messages = JFactory::getApplication()->getMessageQueue();
		if (is_array($messages) && !empty($messages)) {
			foreach ($messages as $msg) {
				if (isset($msg['type']) && isset($msg['message'])) {
					$lists[$msg['type']][] = $msg['message'];
				}
			}
		}
		return $lists;
	}

	public static function load_module($module_id, $module_class = "", $module_style = "") {
		$document = JFactory::getDocument();
		$renderer = $document->loadRenderer('module');
		$params = array('style' => $module_style, 'class' => $module_class);

		//get module as an object
		$database = JFactory::getDBO();
		$database->setQuery("SELECT * FROM #__modules WHERE id='$module_id' ");
		$modules = $database->loadObjectList();
		$module = $modules[0];

		//just to get rid of that stupid php warning
		$module->user = '';
		return $renderer->render($module, $params);
	}

	public static function load_module_pos($position, $style = 'none') {
		$document = JFactory::getDocument();
		$renderer = $document->loadRenderer('module');
		$modules = JModuleHelper::getModules($position);
		$params = array('style' => $style);
		ob_start();
		foreach ($modules as $module) {
			echo $renderer->render($module, $params);
		}
		return ob_get_clean();
	}

	public static function getDeepPath($path, $id, $pathonly = TRUE, $create = TRUE) {
		JLoader::import('joomla.filesystem.folder');
		$deepPath = "";
		foreach (str_split($id, 2) as $dir) {
			$deepPath .= DS . $dir;
			if ($create && !JFolder::exists(JPATH_SITE . DS . $path . $deepPath)) {
				JFolder::create(JPATH_SITE . DS . $path . $deepPath, 0777);
			}
		}
		if ($pathonly) {
			return $path . $deepPath . DS;
		}
		return JPATH_SITE . DS . $path . $deepPath . DS;
	}

	public static function is_admin() {
		$user = JFactory::getUser();
		$groups = $user->getAuthorisedGroups();
		if (in_array("8", $groups) || in_array("10", $groups)) {
			return TRUE;
		}
		return FALSE;
	}

	public static function getCurrentUser($id = FALSE) { // This will get the value directly from db.
		$db = JFactory::getDBO();

		if ($id) {
			$query = " SELECT * FROM #__users WHERE id = " . $db->Quote($id, false) . " LIMIT 1 ";
		} else {
			$user = JFactory::getUser();
			if ($user->get('guest')) {
				return FALSE;
			}
			$query = " SELECT * FROM #__users WHERE id = " . $db->Quote($user->id, false) . " LIMIT 1 ";
		}
		$db->setQuery($query);
		return $db->loadObject();
	}

	public static function generateReferralIds() {
		$db = JFactory::getDBO();
	}

	public static function generateReferralId($user) {
		$result = FALSE;
		$randId = "";
		while (!$result) {
			$randId = self::generateRandomId(7, $user);
			$db = JFactory::getDBO();
			$query = $db->getQuery(TRUE)
					->select("count(*)")
					->from("#__users")
					->where("referral_id = " . $db->quote($randId));
			$result = $db->setQuery($query)->execute();
		}
		return $randId;
	}

	public static function getRandNumber($min, $max) {
		$range = $max - $min;
		if ($range < 0) {
			return $min;
		}
		$log = log($range, 2);
		$bytes = (int) ($log / 8) + 1;
		$bits = (int) $log + 1;
		$filter = (int) (1 << $bits) - 1;
		do {
			$rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
			$rnd = $rnd & $filter;
		} while ($rnd >= $range);
		return $min + $rnd;
	}

	public static function refIdCheck($string) {
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
				->select("count(*)")
				->from("#__users")
				->where("referral_id = " . $db->quote($string));
		$result = $db->setQuery($query)->loadResult();

		return $result;
	}

	public static function generateRandomId($length, $user) {
		$userMail = substr($user['email'], 0, 3);
		$userId = substr($user['id'], -3);
		$token = $userMail.$userId;
		$result = self::refIdCheck($token);
		if ($result > 0) {
			$codeAlphabet = "abcdefghijklmnopqrstuvwxyz";
			$codeAlphabet.= "0123456789";
			for ($i = 0; $i < 3; $i++) {
				$token .= $codeAlphabet[self::getRandNumber(0, strlen($codeAlphabet))];
			}
		}
		return $token;
	}

	public static function getAge($dob, $date = FALSE) { // $dob in Y-m-d format.
		$from = new DateTime($dob);
		$to = ($date) ? new DateTime($date) : new DateTime('today');
		return $from->diff($to)->y;
	}

	function isWeekend($date) {
		return (date('N', strtotime($date)) >= 6);
	}

	function getNextWeekday($date) {
		while (self::isWeekend("+{$date} days")) {
			$date = $date + 1;
		}
		return $date;
	}

//
//	public static function setTextField($name, $value) {
//		if (!isset($this->_formFields[$name]))
//			throw new Zend_Pdf_Exception("Field '$name' does not exist or is not a textfield");
//
//		$field = $this->_formFields[$name];
//		$field->add(new Zend_Pdf_Element_Name('V'), new Zend_Pdf_Element_String($value));
//		$field->touch();
//	}
//
//	public static function _loadFormFields(Zend_Pdf_Element_Reference $root) {
//		if ($root->AcroForm === null || $root->AcroForm->Fields === null) {
//			return;
//		}
//
//		foreach ($root->AcroForm->Fields->items as $field) {
//			if ($field->FT->value == 'Tx' && $field->T !== null) /* We only support fields that are textfields and have a name */ {
//				$this->_formFields[$field->T->value] = $field;
//			}
//		}
//
//		if (!$root->AcroForm->NeedAppearances || !$root->AcroForm->NeedAppearances->value) {
//			/* Ask the .pdf viewer to generate its own appearance data, so we do not have to */
//			$root->AcroForm->add(new Zend_Pdf_Element_Name('NeedAppearances'), new Zend_Pdf_Element_Boolean(true));
//			$root->AcroForm->touch();
//		}
//	}
}
