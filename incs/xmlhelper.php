<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_insurediylife
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('_JEXEC') or die;

/**
 * 
 *
 * @static
 * @package     Joomla.Site
 * @subpackage  com_insurediylife
 * @since       1.6
 */
class MySimpleXMLHelper {

	public static function loadFile($file) {
		// Attempt to load the XML file.
		if (!is_file($file)) {
			return false;
		}
		$xml = simplexml_load_file($file);
		return $xml;
	}

	public static function bind($xml, $data) {
		if (!($xml instanceof SimpleXMLElement)) {
			return FALSE;
		}

		foreach ($xml as $fields) {
			foreach ($fields as $field) {
				$name = (string) $field['name'];
				if (isset($data->$name)) {
					$field['value'] = $data->$name;
				}
			}
		}
		return $xml;
	}

}
