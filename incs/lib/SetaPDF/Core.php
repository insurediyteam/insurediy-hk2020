<?php
/**
 * This file is part of the SetaPDF-Core Component
 *
 * @copyright  Copyright (c) 2014 Setasign - Jan Slabon (http://www.setasign.com)
 * @category   SetaPDF
 * @package    SetaPDF_Core
 * @license    http://www.setasign.com/ Commercial
 * @version    $Id: Core.php 662 2014-09-18 12:37:04Z maximilian.kresse $
 */

/**
 * The class for main properties of the SetaPDF-Core Component
 *
 * @copyright  Copyright (c) 2014 Setasign - Jan Slabon (http://www.setasign.com)
 * @category   SetaPDF
 * @package    SetaPDF_Core
 * @license    http://www.setasign.com/ Commercial
 */
class SetaPDF_Core
{
    /**
     * The version
     *
     * @var string
     */
    const VERSION = '2.12.0.671';

    /**
     * A float comparison precision
     *
     * @var float
     */
    const FLOAT_COMPARISON_PRECISION = 1e-5;
}