<?php
/**
 * This file is part of the SetaPDF-FormFiller Component
 * 
 * @copyright  Copyright (c) 2014 Setasign - Jan Slabon (http://www.setasign.com)
 * @category   SetaPDF
 * @package    SetaPDF_FormFiller
 * @license    http://www.setasign.com/ Commercial
 * @version    $Id: Exception.php 595 2014-01-30 15:20:13Z maximilian.kresse $
 */

/**
 * FormFiller Exception
 * 
 * @copyright  Copyright (c) 2014 Setasign - Jan Slabon (http://www.setasign.com)
 * @category   SetaPDF
 * @package    SetaPDF_FormFiller
 * @license    http://www.setasign.com/ Commercial
 */
class SetaPDF_FormFiller_Exception extends SetaPDF_Exception
{}