<?php
/**
 * This file is part of the SetaPDF-FormFiller Component
 *
 * @copyright  Copyright (c) 2014 Setasign - Jan Slabon (http://www.setasign.com)
 * @category   SetaPDF
 * @package    SetaPDF_FormFiller
 * @license    http://www.setasign.com/ Commercial
 * @version    $Id$
 */

/**
 * The helper class for processing XFA data and template data.
 *
 * This class matches data nodes to template nodes and creates data nodes if they are not available.
 *
 * The class actually only supports the default "mergeMode": "consumeData" mode.
 *
 * So the logic for updating a field by its AcroForm name will be:
 * - Check if a data node is already found for this specific field.
 *   - if not, start the data parsing process which mappes all find value nodes to specific
 *     fields if they don't have a bind-value.
 *   - direct matches have a higher priority than scope matches.
 * - The process have to be done twice in case a direct match had overwritten a scope matched relation.
 * - If the field still have no data node attached to it create a node (tree) for it and attach it.
 *   (only if the match attribute of the bind-value is not set to "none".)
 * - Update the node if available.
 *
 * Updating by changed XML data:
 * - remove all bounded data nodes from all available fields
 * - start the data parsing process and attach the nodes to the fields.
 * - Iterate over all fields (XML) and set their values in their corresponding
 *   AcroForm fields. (omit recursivity)
 *
 * TODO:
 * - “Extended Mapping Rules” on page 508. (XFA Specification - 3.3)
 * - XSLT Transformations (see page 545 - XFA Specification - 3.3)
 * - Resolve mergeMode-attribute from root node (introduced in XFA 3.1, so maybee obsolete atm)
 * - Handling of "picture" clauses (see page 156 / 1150 - XFA Specification - 3.3)
 *
 * @copyright  Copyright (c) 2014 Setasign - Jan Slabon (http://www.setasign.com)
 * @category   SetaPDF
 * @package    SetaPDF_FormFiller
 * @license    http://www.setasign.com/ Commercial
 */
class SetaPDF_FormFiller_Xfa_Bridge
{
    /**
     * The template node.
     *
     * @var DOMElement
     */
    protected $_template;

    /**
     * The data node.
     *
     * @var DOMElement
     */
    protected $_data;

    /**
     * A hash map for relation between a field node and its corresponding AcroForm representation.
     *
     * The keys are the field names used in the AcroForm field. The value is the field node itself.
     *
     * @var array
     */
    protected $_acroFormFieldNamesToTemplateNodes;

    /**
     * The current found binding from an accestor node.
     *
     * @var string
     */
    protected $_currentBinding;

    /**
     * The data node for a template node.
     *
     * @var SetaPDF_Core_Compat_SplObjectStorage
     */
    protected $_dataNodeByTemplateNode;

    /**
     * The default data path by a field node.
     *
     * @var SetaPDF_Core_Compat_SplObjectStorage
     */
    protected $_dataPathByField;

    /**
     * Fieldnames by field nodes.
     *
     * @var SetaPDF_Core_Compat_SplObjectStorage
     */
    protected $_fieldToFieldName;

    /**
     * Direct bindings of field nodes.
     *
     * @var SetaPDF_Core_Compat_SplObjectStorage
     */
    protected $_directBinding;

    /**
     * Global bindungs by field nodes.
     *
     * @var SetaPDF_Core_Compat_SplObjectStorage
     */
    protected $_globalBinding;

    /**
     * None bindings by field nodes.
     *
     * @var SetaPDF_Core_Compat_SplObjectStorage
     */
    protected $_noneBinding;

    /**
     * The current index of the data nodes in the data root element.
     *
     * @var integer
     */
    protected $_currentRootIndex;

    /**
     * Flag indicating if the data were processed or not.
     *
     * @var bool
     */
    protected $_dataProcessed = false;

    /**
     * The constructor
     *
     * @param DOMElement $template
     * @param DOMElement $data
     */
    public function __construct(DOMElement $template, DOMElement $data)
    {
        $this->_template = $template;
        $this->_data = $data;

        $this->_parseTemplate();
    }

    /**
     * Release memory and cycled references.
     */
    public function cleanUp()
    {
        $this->_template = null;
        $this->_data = null;

        $this->_acroFormFieldNamesToTemplateNodes = null;
        $this->_dataNodeByTemplateNode = null;
        $this->_dataPathByField = null;
        $this->_fieldToFieldName = null;
        $this->_directBinding = null;
        $this->_globalBinding = null;
        $this->_noneBinding = null;
    }

    /**
     * Parses the template and evaluates direct and global bindings.
     */
    protected function _parseTemplate()
    {
        // reset data
        $this->_acroFormFieldNamesToTemplateNodes = array();
        $this->_dataNodeByTemplateNode = new SetaPDF_Core_Compat_SplObjectStorage();
        $this->_dataPathByField = new SetaPDF_Core_Compat_SplObjectStorage();
        $this->_fieldToFieldName = new SetaPDF_Core_Compat_SplObjectStorage();
        $this->_directBinding = new SetaPDF_Core_Compat_SplObjectStorage();
        $this->_globalBinding = new SetaPDF_Core_Compat_SplObjectStorage();
        $this->_noneBinding = new SetaPDF_Core_Compat_SplObjectStorage();
        $this->_dataProcessed = false;

        $this->_processTemplate($this->_template);
        // try to get the value via a bind element
        foreach ($this->_directBinding AS $node) {
            $this->_evaluateDirectBindings($node);
        }

        foreach ($this->_globalBinding AS $node) {
            $this->_evaluateGlobalBindings($node);
        }
    }

    /**
     * Processes a template node recursively.
     *
     * This method resolves the field names representing the AcroForm fields in the PDF structure
     * and ensures direct data pathes.
     *
     * @param DOMNode $node
     * @param array $path
     * @param array $dataPathCache
     * @param string $currentDataPath
     */
    private function _processTemplate(
        DOMNode $node,
        array $path = array(),
        array &$dataPathCache = array(),
        $currentDataPath = ''
    ) {
        $bind = false;

        if ($node instanceof DOMElement &&
            ($node->tagName == 'field' || $node->tagName == 'exclGroup')
        ) {
            $name = $originalName = $node->getAttribute('name');
            $index = 0;

            $dataPath = trim($currentDataPath . '.' . $name . '[' . $index . ']', '.');
            while (isset($dataPathCache[$dataPath])) {
                $index++;
                $dataPath = trim($currentDataPath . '.' . $name . '[' . $index . ']', '.');;
            }

            $dataPathCache[$dataPath] = true;
            $name = $name . '[' . $index . ']';

            $path[] = $name;
            $fieldName = join('.', $path);
            $this->_acroFormFieldNamesToTemplateNodes[$fieldName] = $node;

            $this->_fieldToFieldName->attach($node, $fieldName);

            $this->_dataPathByField->attach($node, $dataPath);

            // direct "bind"
            $xpath = new DOMXPath($node->ownerDocument);
            $xpath->registerNamespace('tpl', $node->namespaceURI);

            $match = $xpath->query("tpl:bind[@match]", $node);
            if ($match->length > 0) {
                $match = $match->item(0);
                $matchValue = $match->getAttribute('match');

                switch ($matchValue) {
                    case 'none':
                        $this->_noneBinding->attach($node, $fieldName);
                        break;
                    case 'global':
                        $this->_globalBinding->attach($node, $originalName);
                        break;
                    case 'dataRef':
                        $dataRef = $match;
                        break;
                }
            }

            if (isset($dataRef)) {
                $dataRoot = $this->_currentBinding !== null
                    ? $this->_currentBinding['to']
                    : $currentDataPath;

                $binding = SetaPDF_FormFiller_Xfa_Som::evaluateSomShortcuts($dataRef->getAttribute('ref'), $dataRoot);

            } elseif ($this->_currentBinding !== null) {
                // a binding in a parent <subform>
                $binding = $this->_currentBinding['to'] . '.'
                    . trim(substr($dataPath, strlen($this->_currentBinding['dataPath'])), '.');
            }

            if (isset($binding)) {
                $this->_directBinding->attach($node, $binding);
            }

            return;
        }

        if ($node instanceof DOMElement && (
                $node->tagName == 'subform' || $node->tagName == 'area' ||
                $node->tagName == 'pageSet' || $node->tagName == 'pageArea'
            )) {
            $namedElement = $node->hasAttribute('name');
            $tagName = '#' . $node->tagName;
            if ($namedElement) {
                $name = $node->getAttribute('name');
            } else {
                $name = $tagName;
            }

            $index = 0;

            // increment by tag name (behaviour of Acrobat/Adobe Reader)
            $dataPath = trim($currentDataPath . '.' . $tagName . '[' . $index . ']', '.');
            while (isset($dataPathCache[$dataPath])) {
                $index++;
                $dataPath = trim($currentDataPath . '.' . $tagName . '[' . $index . ']', '.');
            }

            $dataPathCache[$dataPath] = true;

            if ($node->tagName == 'subform') {
                $xpath = new DOMXPath($node->ownerDocument);
                $xpath->registerNamespace('tpl', $node->namespaceURI);
                $bind = $xpath->query("tpl:bind[@match='dataRef']", $node);
                if ($bind->length > 0) {
                    $dataRoot = $this->_currentBinding !== null
                        ? $this->_currentBinding['to']
                        : $currentDataPath;

                    $bind = SetaPDF_FormFiller_Xfa_Som::evaluateSomShortcuts($bind->item(0)->getAttribute('ref'), $dataRoot);
                } else {
                    $bind = null;
                }
            }

            // if named we need to increment by the name of the element
            if ($namedElement) {
                $index = 0;

                // Real name
                $dataPath = trim($currentDataPath . '.' . $name . '[' . $index . ']', '.');;
                while (isset($dataPathCache[$dataPath])) {
                    $index++;
                    $dataPath = trim($currentDataPath . '.' . $name . '[' . $index . ']', '.');;
                }

                if ($tagName == '#subform') {
                    $dataPathCache[$dataPath] = true;
                    $currentDataPath = $dataPath;
                }
            }

            if ($bind) {
                $this->_currentBinding = array(
                    'dataPath' => $currentDataPath,
                    'to' => $bind
                );
            }

            $name = $name . '[' . $index . ']';
            $path[] = $name;
        }

        if ($node->hasChildNodes()) {
            foreach ($node->childNodes AS $childNode) {
                $this->_processTemplate($childNode, $path, $dataPathCache, $currentDataPath);
            }
        }

        if ($bind)
            $this->_currentBinding = null;
    }

    /**
     * Get all template nodes by their corresponding name used in the AcroForm representation.
     *
     * @return DOMElement[]
     */
    public function getAcroFormFieldNamesToTemplateNodes()
    {
        return $this->_acroFormFieldNamesToTemplateNodes;
    }

    /**
     * Get a fields node from the template packet.
     *
     * @param string $fieldName
     * @return bool|DOMElement
     */
    public function getTemplateNode($fieldName)
    {
        if (isset($this->_acroFormFieldNamesToTemplateNodes[$fieldName])) {
            return $this->_acroFormFieldNamesToTemplateNodes[$fieldName];
        }

        return false;
    }

    /**
     * Gets a data node by a field name.
     *
     * This method returns the matching data node (DOMElement or DOMAttributeNode) object
     * which is matched to the given field.
     * If no data node is found it will create a new data node. If the field is defined to
     * bind to nothing (match="none") the method will return false.
     *
     * If an unknown fieldname is passed the method will throw an InvalidArgumentException
     * exception.
     *
     * @param string $name
     * @param boolean $create Defines if a data node should be created if it cannot be found.
     * @see getAcroFormFieldNamesToTemplateNodes()
     * @return bool|DOMElement
     * @throws InvalidArgumentException
     */
    public function getDataNodeByFieldName($name, $create = true)
    {
        if (!isset($this->_acroFormFieldNamesToTemplateNodes[$name])) {
            throw new InvalidArgumentException('No field "' . $name . '" found in XFA template structure.');
        }

        $fieldNode = $this->_acroFormFieldNamesToTemplateNodes[$name];

        if ($this->_noneBinding->contains($fieldNode)) {
            return false;
        }

        if ($this->_dataNodeByTemplateNode->contains($fieldNode)) {
            return $this->_dataNodeByTemplateNode[$fieldNode];
        }

        /* Let's process all data in "consumeData" mode (which is its default)
         */
        if ($this->_dataProcessed === false) {
            // do this 2 times to ensure that overwritten nodes get a snd chance to bind correctly
            for ($i = 2; 0 < $i; $i--) {
                $this->_currentRootIndex = 0;
                foreach ($this->_data->childNodes AS $child) {
                    if ($child instanceof DOMElement) {
                        $this->_processData($child);
                        $this->_currentRootIndex++;
                    }
                }
            }

            foreach ($this->_data->childNodes AS $child) {
                $this->_processDataAttributes($child);
            }

            $this->_currentRootIndex = null;
            $this->_dataProcessed = true;
        }

        if ($this->_dataNodeByTemplateNode->contains($fieldNode)) {
            return $this->_dataNodeByTemplateNode[$fieldNode];
        }

        if ($create) {
            return $this->_createDataNode($fieldNode);
        }

        return false;
    }

    /**
     * Checks a field node for a global binding.
     *
     * @param $fieldNode
     * @return bool
     */
    public function isGlobalBinding($fieldNode)
    {
        if (is_string($fieldNode)) {
            $fieldNode = $this->getTemplateNode($fieldNode);
        }

        return $this->_globalBinding->contains($fieldNode);
    }

    /**
     * Get field names or nodes which are bound to the same data node.
     *
     * @param string|DOMElement $fieldNode
     * @param bool $asNames Defines wheter the method should return the names or field nodes.
     * @return array|DOMElement[] An array of field names or fields (DOMElements).
     */
    public function getSameBoundFields($fieldNode, $asNames = false)
    {
        if (is_string($fieldNode)) {
            $fieldNode = $this->getTemplateNode($fieldNode);
        }

        $result = array();

        if ($this->_globalBinding->contains($fieldNode)) {
            $globalPath = $this->_globalBinding[$fieldNode];
            foreach ($this->_globalBinding AS $_fieldNode) {
                if ($fieldNode === $_fieldNode) {
                    continue;
                }

                if ($globalPath === $this->_globalBinding->getInfo()) {
                    $result[] = $_fieldNode;
                }
            }
        }

        if ($this->_directBinding->contains($fieldNode)) {
            $directPath = $this->_directBinding[$fieldNode];
            foreach ($this->_directBinding AS $_fieldNode) {
                if ($fieldNode === $_fieldNode) {
                    continue;
                }

                if ($directPath === $this->_directBinding->getInfo()) {
                    $result[] = $_fieldNode;
                }
            }
        }

        if ($asNames) {
            foreach ($result AS $key => $_fieldNode) {
                $result[$key] = array_search($_fieldNode, $this->_acroFormFieldNamesToTemplateNodes, true);
            }
        }

        return $result;
    }

    /**
     * Processes the data and bind the nodes to the template/field nodes.
     *
     * @param DOMNode $node
     * @param array $path
     * @param array $pathes
     */
    protected function _processData(
        DOMNode $node,
        array $path = array(),
        array &$pathes = array()
    ) {
        if ($node instanceof DOMElement) {
            $name = $node->tagName;

            $currentPath = join('.', $path);

            $index = 0;
            while (isset($pathes[$currentPath . '.' . $name. '[' . $index . ']'])) {
                $index++;
            }

            $path[] = $name. '[' . $index . ']';

            $this->_bindDataNode(SetaPDF_FormFiller_Xfa_Som::escape($path), $node);

            $pathes[join('.', $path)] = true;
        }

        if ($node->hasChildNodes()) {
            foreach ($node->childNodes AS $child) {
                if ($child instanceof DOMElement) {
                    $this->_processData($child, $path, $pathes);
                }
            }
        }
    }

    /**
     * Process attributes to unbind tempalte/field nodes.
     *
     * @param DOMNode $node
     * @param array $path
     * @param array $pathes
     */
    protected function _processDataAttributes(
        DOMNode $node,
        array $path = array(),
        array &$pathes = array()
    ) {
        $hasChildNodes = $node->hasChildNodes();
        if ($node instanceof DOMElement) {

            $name = $node->tagName;

            $currentPath = join('.', $path);

            $index = 0;
            while (isset($pathes[$currentPath . '.' . $name. '[' . $index . ']'])) {
                $index++;
            }

            // Attributes are only resolved in data nodes.
            $isDataNode = true;
            if ($hasChildNodes) {
                foreach ($node->childNodes AS $child) {
                    if ($child instanceof DOMElement) {
                        $isDataNode = false;
                        break;
                    }
                }
            }

            if ($isDataNode) {
                foreach ($node->attributes AS $attribute) {
                    $this->_bindDataNode(array_merge($path, array($attribute->name)), $attribute);
                }
            }

            $path[] = $name. '[' . $index . ']';
            $pathes[join('.', $path)] = true;
        }

        if ($hasChildNodes) {
            foreach ($node->childNodes AS $child) {
                if ($child instanceof DOMElement) {
                    $this->_processDataAttributes($child, $path, $pathes);
                }
            }
        }
    }

    /**
     * Tries to bind a data node to a field node.
     *
     * Both "direct match" and "scope match" are evaluated.
     *
     * @param array $path
     * @param DOMNode $dataNode
     * @return boolean
     */
    protected function _bindDataNode(array $path, DOMNode $dataNode)
    {
        $fieldNode = null;

        if ($this->_isBind($dataNode)) {
            return true;
        }

        // Direct Match
        $fieldNode = $this->_doDirectMatch($path);
        if ($fieldNode) {
            $this->_dataNodeByTemplateNode->attach($fieldNode, $dataNode);
            return true;
        }

        // scope match
        $path[0] = '*[0]';
        $fieldNode = $this->_doScopeMatch($path);
        if ($fieldNode) {
            $this->_dataNodeByTemplateNode->attach($fieldNode, $dataNode);
            return true;
        }

        return false;
    }

    /**
     * Tries to find a field node by a "direct match".
     *
     * @param array $path
     * @return bool
     */
    private function _doDirectMatch(array $path)
    {
        $som = new SetaPDF_FormFiller_Xfa_Som();

        /* The highest-level subform and the data node representing the current record
         * are special; they are always bound even if their names don't match. In fact
         * it is common for the highest-level subform in a template to be unnamed, that
         * is to not have a name attribute. In the example assume that the data holds
         * just one record. This is a common arrangement.
         */
        $path[0] = '*[' . $this->_currentRootIndex . ']';

        $fieldNodes = $som->evaluate(join('.', $path), $this->_template);
        $result = $this->_filtertBindings($fieldNodes, $this->_currentRootIndex == 0);

        if (count($result)) {
            return $result[0];
        }

        return false;
    }

    /**
     * Tries to find a field node by a "scope match".
     *
     * Example from the XFA specification:
     *
     * Template:
     * <template …>
     *     <subform name="registration">
     *         <field name="first" …>… </field>
     *         <field name="last" …> … </field>
     *         <subform name="address">
     *             <field name="apt" …> … </field>
     *             <field name="street" …> … </field>
     *             <field name="city"…> … </field>
     *             <field name="country"…> … </field>
     *             <field name="postalcode"…> … </field>
     *         </subform>
     *     </subform>
     * </template>
     *
     * Data:
     * <?xml version="1.0"?>
     * <registration>
     *     <first>Jack</first>
     *     <last>Spratt</last>
     *     <apt></apt>
     *     <street>99 Candlestick Lane</street>
     *     <city>London</city>
     *     <country>UK</country>
     *     <postalcode>SW1</postalcode>
     * </registration>
     *
     * @param array $path
     * @return boolean|DOMElement
     */
    private function _doScopeMatch(array $path)
    {
        if (count($path) === 0)
            return false;

        $som = new SetaPDF_FormFiller_Xfa_Som();

        /* "[...]A scope match occurs when the data node in question is the sibling of a
         * node which has an ancestor bound to an ancestor of a node with the same name as
         * the data node.[...]"
         */

        $newPath = array();
        $ancestorIndex = null;
        $tmpResult = $this->_template;
        $part = null;

        foreach ($path AS $index => $part) {
            if (($pos = strpos($part, '[')) !== false) {
                $part = substr($part, 0, $pos);
            }

            if ($tmpResult) {
                $tmpResult = $som->evaluate($part, $tmpResult);
                if (false === $tmpResult) {
                    $newPath[] = '';
                    $ancestorIndex = $index;
                    continue;
                }
            }

            if ($ancestorIndex === null)
                $newPath[] = $part;
        }

        if ($newPath[count($newPath) - 1] === '') {
            $newPath[] = $part;
        }

        if (!in_array('', $newPath) && count($newPath) > 1) {
            $last = array_pop($newPath);
            $newPath[] = '';
            $newPath[] = $last;
        }
        $_newPath = join('.', $newPath); // result= A[0].B[0]..C[0]

        $fieldNodes = $som->evaluate($_newPath, $this->_template);
        $result = $this->_filtertBindings($fieldNodes);

        if (count($result))
            return $result[0];

        /* "[...]Only if unable to find an ancestor match does the data binding process fall back
         * upon a search for a scope match involving sibling(s) of ancestor(s) (also known
         * as a sibling match). In other words, the data binding process tries to find a
         * match within the current branch of the Data DOM, but if it can't, it looks for a
         * match in a related branch.[...]"
         */
        unset($newPath[$ancestorIndex - 1]);
        $newPath = array_values($newPath);

        if ($newPath[0] === '') {
            $newPath[0] = '*.';
        }

        $_newPath = join('.', $newPath); // result= A[0].B[0]..C[0]
        $fieldNodes = $som->evaluate($_newPath, $this->_template);
        $result = $this->_filtertBindings($fieldNodes);
        if (count($result))
            return $result[0];

        return false;
    }

    /**
     * This method filters field nodes which are already bound.
     *
     * @param DOMElement[] $fieldNodes
     * @param bool $isDirect
     * @return array
     */
    private function _filtertBindings($fieldNodes, $isDirect = false)
    {
        if (!is_array($fieldNodes)) {
            $fieldNodes = array($fieldNodes);
        }

        $result = array();
        foreach ($fieldNodes AS $fieldNode) {
            if (!$fieldNode instanceof DOMElement) {
                continue;
            }

            if ($this->_fieldToFieldName->contains($fieldNode) && !$this->_noneBinding->contains($fieldNode) &&
                !$this->_directBinding->contains($fieldNode) &&
                ($isDirect || !$this->_dataNodeByTemplateNode->contains($fieldNode))
            ) {
                $result[] = $fieldNode;
            }
        }

        return $result;
    }

    /**
     * This method evaluates a direct binding of a field node.
     *
     * @param DOMElement $fieldNode
     * @return bool
     */
    protected function _evaluateDirectBindings(DOMElement $fieldNode)
    {
        if (!$this->_directBinding->contains($fieldNode)) {
            return false;
        }

        $dataPath = $this->_directBinding[$fieldNode];
        #$dataPath = SetaPDF_FormFiller_Xfa_Som::evaluateSomShortcuts($dataPath);

        // make sure that the path points to the data packet
        if (strpos($dataPath, 'xfa.datasets.data.') === 0) {
            // remove path to data packet
            $dataPath = substr($dataPath, strlen('xfa.datasets.data.'));
        }

        // direct match
        $som = new SetaPDF_FormFiller_Xfa_Som();
        $dataNode = $som->evaluate($dataPath, $this->_data);
        if ($dataNode) {
            $this->_dataNodeByTemplateNode->attach($fieldNode, $dataNode);
            return true;
        }

        // try wildcard in the root node if the match is not the root node:
        $dataPath = preg_split("/(?<!\\\)\./", $dataPath);
        // only if it's not only a root node.
        if (count($dataPath) === 1) {
            return false;
        }

        $dataPath[0] = '*[0]';
        $dataPath = join('.', $dataPath);
        $dataNode = $som->evaluate($dataPath, $this->_data);
        if ($dataNode) {
            $this->_dataNodeByTemplateNode->attach($fieldNode, $dataNode);
            return true;
        }

        // TODO: Scope match

        // try via attribute match
        $dataPath = preg_split("/(?<!\\\)\./", $dataPath);
        $last = array_pop($dataPath);
        $dataPath[] = '#' . $last;
        $dataPath = join('.', $dataPath);

        $dataNode = $som->evaluate($dataPath, $this->_data);
        if ($dataNode) {
            $this->_dataNodeByTemplateNode->attach($fieldNode, $dataNode);
            return true;
        }

        return false;
    }

    /**
     * This method evaluates a global binding, if exists, for a field node.
     *
     * @param DOMElement $fieldNode
     * @return bool
     */
    protected function _evaluateGlobalBindings(DOMElement $fieldNode)
    {
        if (!$this->_globalBinding->contains($fieldNode)) {
            return false;
        }

        $name = $this->_globalBinding[$fieldNode];

        $som = new SetaPDF_FormFiller_Xfa_Som();
        $dataNode = $som->evaluate('*[0]..' . $name . '[0]', $this->_data);
        if ($dataNode) {
            $this->_dataNodeByTemplateNode->attach($fieldNode, $dataNode);
            return true;
        }

        return false;
    }

    /**
     * Checks if a data node is already bound to a field node.
     *
     * @param DOMNode $dataNode
     * @return bool|DOMNode
     */
    protected function _isBind(DOMNode $dataNode)
    {
        foreach ($this->_dataNodeByTemplateNode AS $fieldNode) {
            $_dataNode = $this->_dataNodeByTemplateNode->getInfo();
            if ($_dataNode === $dataNode)
                return true;
        }

        return false;
    }

    /**
     * Creates of gets a data node by a field node.
     *
     * @param DOMElement $fieldNode
     * @return DOMElement
     */
    protected function _createDataNode(DOMElement $fieldNode)
    {
        // check for a global binding
        if ($this->_globalBinding->contains($fieldNode)) {
            /* Acutally we add global bindings to the root element.
             * Acrobat adds it to a (first?) matching field into the default structure.
             */
            $dataPath = $this->_globalBinding[$fieldNode];

        // check for direct binding
        } else if ($this->_directBinding->contains($fieldNode)) {
            $dataPath = $this->_directBinding[$fieldNode];

            // make sure that the path points to the data packet
            if (strpos($dataPath, 'xfa.datasets.data.') === 0) {
                // remove path to data packet
                $dataPath = substr($dataPath, strlen('xfa.datasets.data.'));
            }

        // default data path
        } else {
            $dataPath = $this->_dataPathByField[$fieldNode];
        }

        $parts = explode('.', $dataPath);

        $domDocument = $this->_data->ownerDocument;
        $xpath = new DOMXPath($domDocument);
        $currentNode = $this->_data;
        foreach ($parts AS $part) {
            $start = strpos($part, '[');
            if ($start === false) {
                $tagName = $part;
                $index = 0;
            } else {
                $tagName = substr($part, 0, $start);
                $index = substr($part, $start + 1, -1);
            }

            while (($xpath->query($tagName . '[' . ($index + 1) .']', $currentNode)->length) === 0) {
                $_node = $domDocument->createElement($tagName);
                $currentNode->appendChild($_node);
            }

            $currentNode = $xpath->query($tagName . '[' . ($index + 1) .']', $currentNode)->item(0);
        }

        return $currentNode;
    }
}