<?php
/**
 * This file is part of the SetaPDF-Core Component
 *
 * @copyright  Copyright (c) 2014 Setasign - Jan Slabon (http://www.setasign.com)
 * @category   SetaPDF
 * @package    SetaPDF_Core
 * @subpackage Parser
 * @license    http://www.setasign.com/ Commercial
 * @version    $Id: InvalidTokenException.php 608 2014-02-03 09:35:49Z maximilian.kresse $
 */

/**
 * Invalid token exception
 *
 * @copyright  Copyright (c) 2014 Setasign - Jan Slabon (http://www.setasign.com)
 * @category   SetaPDF
 * @package    SetaPDF_Core
 * @subpackage Parser
 * @license    http://www.setasign.com/ Commercial
 */
class SetaPDF_Core_Parser_Pdf_InvalidTokenException
    extends SetaPDF_Core_Parser_Exception
{
  /** Constants prefix: 0x0a **/
}