<?php
/**
 * This file is part of the SetaPDF-Core Component
 *
 * @copyright  Copyright (c) 2014 Setasign - Jan Slabon (http://www.setasign.com)
 * @category   SetaPDF
 * @package    SetaPDF_Core
 * @subpackage Font
 * @license    http://www.setasign.com/ Commercial
 * @version    $Id: Type0.php 608 2014-02-03 09:35:49Z maximilian.kresse $
 */

/**
 * Class for Type0 fonts
 *
 * @TODO Not fully implemented yet!
 *
 * @copyright  Copyright (c) 2014 Setasign - Jan Slabon (http://www.setasign.com)
 * @category   SetaPDF
 * @package    SetaPDF_Core
 * @subpackage Font
 * @license    http://www.setasign.com/ Commercial
 */
class SetaPDF_Core_Font_Type0 extends SetaPDF_Core_Font
    implements SetaPDF_Core_Font_Glyph_Collection_CollectionInterface
{
    /**
     * The font name
     *
     * @var string
     */
    protected $_fontName;

    /**
     * @return mixed|void
     * @throws SetaPDF_Exception_NotImplemented
     * @internal
     */
    protected function _getEncodingTable()
    {
        throw new SetaPDF_Exception_NotImplemented('Type0 font support is not implemented.');
    }

    /**
     * Get the font name.
     *
     * @return string
     */
    public function getFontName()
    {
        if (null === $this->_fontName)
            $this->_fontName = $this->_dictionary->offsetGet('BaseFont')->ensure()->getValue();

        return $this->_fontName;
    }

    /**
     * Get the font family.
     *
     * @return string|void
     * @throws SetaPDF_Exception_NotImplemented
     * @internal
     */
    public function getFontFamily()
    {
        throw new SetaPDF_Exception_NotImplemented('Type0 font support is not implemented.');
    }

    /**
     * Checks if the font is bold.
     *
     * @return boolean
     * @throws SetaPDF_Exception_NotImplemented
     * @internal
     */
    public function isBold()
    {
        throw new SetaPDF_Exception_NotImplemented('Type0 font support is not implemented.');
    }

    /**
     * Checks if the font is italic.
     *
     * @return boolean
     * @throws SetaPDF_Exception_NotImplemented
     * @internal
     */
    public function isItalic()
    {
        throw new SetaPDF_Exception_NotImplemented('Type0 font support is not implemented.');
    }

    /**
     * Checks if the font is monospace.
     *
     * @return boolean
     * @throws SetaPDF_Exception_NotImplemented
     * @internal
     */
    public function isMonospace()
    {
        throw new SetaPDF_Exception_NotImplemented('Type0 font support is not implemented.');
    }

    /**
     * Returns the font bounding box.
     *
     * @return array
     * @throws SetaPDF_Exception_NotImplemented
     * @internal
     */
    public function getFontBBox()
    {
        throw new SetaPDF_Exception_NotImplemented('Type0 font support is not implemented.');
    }

    /**
     * Returns the italic angle.
     *
     * @return float
     * @throws SetaPDF_Exception_NotImplemented
     */
    public function getItalicAngle()
    {
        throw new SetaPDF_Exception_NotImplemented('Type0 font support is not implemented.');
    }

    /**
     * Returns the distance from baseline of highest ascender (Typographic ascent).
     *
     * @return float
     * @throws SetaPDF_Exception_NotImplemented
     * @internal
     */
    public function getAscent()
    {
        throw new SetaPDF_Exception_NotImplemented('Type0 font support is not implemented.');
    }

    /**
     * Returns the distance from baseline of lowest descender (Typographic descent).
     *
     * @return float
     * @throws SetaPDF_Exception_NotImplemented
     * @internal
     */
    public function getDescent()
    {
        throw new SetaPDF_Exception_NotImplemented('Type0 font support is not implemented.');
    }

    /**
     * Get the width of a glyph/character.
     *
     * @param string $char
     * @param string $encoding The input encoding
     * @return float|int
     * @throws SetaPDF_Exception_NotImplemented
     * @internal
     */
    public function getGlyphWidth($char, $encoding = 'UTF-16BE')
    {
        throw new SetaPDF_Exception_NotImplemented('Type0 font support is not implemented.');
    }
}