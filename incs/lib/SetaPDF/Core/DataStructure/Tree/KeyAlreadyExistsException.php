<?php
/**
 * This file is part of the SetaPDF-Core Component
 *
 * @copyright  Copyright (c) 2014 Setasign - Jan Slabon (http://www.setasign.com)
 * @category   SetaPDF
 * @package    SetaPDF_Core
 * @license    http://www.setasign.com/ Commercial
 * @version    $Id: KeyAlreadyExistsException.php 608 2014-02-03 09:35:49Z maximilian.kresse $
 */

/**
 * Exception class which is thrown if a key that should be set already exists in a tree
 *
 * @copyright  Copyright (c) 2014 Setasign - Jan Slabon (http://www.setasign.com)
 * @category   SetaPDF
 * @package    SetaPDF_Core
 * @license    http://www.setasign.com/ Commercial
 */
class SetaPDF_Core_DataStructure_Tree_KeyAlreadyExistsException
    extends SetaPDF_Core_Exception
{
}