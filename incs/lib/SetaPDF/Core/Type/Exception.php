<?php
/**
 * This file is part of the SetaPDF-Core Component
 * 
 * @copyright  Copyright (c) 2014 Setasign - Jan Slabon (http://www.setasign.com)
 * @category   SetaPDF
 * @package    SetaPDF_Core
 * @subpackage Type
 * @license    http://www.setasign.com/ Commercial
 * @version    $Id: Exception.php 608 2014-02-03 09:35:49Z maximilian.kresse $
 */

/**
 * Type exception
 * 
 * @copyright  Copyright (c) 2014 Setasign - Jan Slabon (http://www.setasign.com)
 * @category   SetaPDF
 * @package    SetaPDF_Core
 * @subpackage Type
 * @license    http://www.setasign.com/ Commercial
 */
class SetaPDF_Core_Type_Exception extends SetaPDF_Core_Exception
{
    /** Constants prefix: 0x07 **/
}