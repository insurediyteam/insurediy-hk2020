<?php
/**
 * This file is part of the SetaPDF library
 * 
 * @copyright  Copyright (c) 2014 Setasign - Jan Slabon (http://www.setasign.com)
 * @category   SetaPDF
 * @package    SetaPDF
 * @license    http://www.setasign.com/ Commercial
 * @version    $Id: NotImplemented.php 608 2014-02-03 09:35:49Z maximilian.kresse $
 */

/**
 * Not implemented exception
 * 
 * @copyright  Copyright (c) 2014 Setasign - Jan Slabon (http://www.setasign.com)
 * @category   SetaPDF
 * @package    SetaPDF
 * @license    http://www.setasign.com/ Commercial
 */
class SetaPDF_Exception_NotImplemented extends SetaPDF_Exception
{}