<?php

defined('JPATH_PLATFORM') or die;

abstract class ZFactory {

	public static $application = null;
	public static $cache = null;
	public static $config = null;
	public static $dates = array();
	public static $session = null;
	public static $language = null;
	public static $document = null;
	public static $acl = null;
	public static $database = null;
	public static $mailer = null;
	public static $currency = null;
	public static $currencies = array();

	public static function getCurrency($id = null) {
		if (!self::$currencies) {
			self::$currencies = new ZCurrency();
		}
		return self::$currencies;
	}

	private function loadCurrencies() {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$query->select("*");
		$query->from("#__currencies");
		$db->setQuery($query);
		return $db->loadObjectList();
	}

	public static function getApplication($id = null, array $config = array(), $prefix = 'J') {
		if (!self::$application) {
			if (!$id) {
				throw new Exception('Application Instantiation Error', 500);
			}

			self::$application = JApplicationCms::getInstance($id);
		}

		return self::$application;
	}

	public static function getConfig($file = null, $type = 'PHP', $namespace = '') {
		if (!self::$config) {
			if ($file === null) {
				$file = JPATH_PLATFORM . '/config.php';
			}

			self::$config = self::createConfig($file, $type, $namespace);
		}

		return self::$config;
	}

	public static function getSession(array $options = array()) {
		if (!self::$session) {
			self::$session = self::createSession($options);
		}

		return self::$session;
	}

	public static function getLanguage() {
		if (!self::$language) {
			self::$language = self::createLanguage();
		}

		return self::$language;
	}

	public static function getDocument() {
		if (!self::$document) {
			self::$document = self::createDocument();
		}

		return self::$document;
	}

	public static function getUser($id = null) {
		$instance = self::getSession()->get('user');

		if (is_null($id)) {
			if (!($instance instanceof JUser)) {
				$instance = JUser::getInstance();
			}
		} elseif ($instance->id != $id) {
			$instance = JUser::getInstance($id);
		}

		return $instance;
	}

	public static function getCache($group = '', $handler = 'callback', $storage = null) {
		$hash = md5($group . $handler . $storage);

		if (isset(self::$cache[$hash])) {
			return self::$cache[$hash];
		}

		$handler = ($handler == 'function') ? 'callback' : $handler;

		$options = array('defaultgroup' => $group);

		if (isset($storage)) {
			$options['storage'] = $storage;
		}

		$cache = JCache::getInstance($handler, $options);

		self::$cache[$hash] = $cache;

		return self::$cache[$hash];
	}

	public static function getACL() {
		JLog::add(__METHOD__ . ' is deprecated. Use JAccess directly.', JLog::WARNING, 'deprecated');

		if (!self::$acl) {
			self::$acl = new JAccess;
		}

		return self::$acl;
	}

	public static function getDbo() {
		if (!self::$database) {
			self::$database = self::createDbo();
		}

		return self::$database;
	}

	public static function getMailer() {
		if (!self::$mailer) {
			self::$mailer = self::createMailer();
		}

		$copy = clone self::$mailer;

		return $copy;
	}

	public static function getFeedParser($url, $cache_time = 0) {
		if (!class_exists('JSimplepieFactory')) {
			throw new BadMethodCallException('JSimplepieFactory not found');
		}

		JLog::add(__METHOD__ . ' is deprecated.   Use JFeedFactory() or supply SimplePie instead.', JLog::WARNING, 'deprecated');

		return JSimplepieFactory::getFeedParser($url, $cache_time);
	}

	public static function getXML($data, $isFile = true) {
		JLog::add(__METHOD__ . ' is deprecated. Use SimpleXML directly.', JLog::WARNING, 'deprecated');

		$class = 'SimpleXMLElement';

		if (class_exists('JXMLElement')) {
			$class = 'JXMLElement';
		}

		// Disable libxml errors and allow to fetch error information as needed
		libxml_use_internal_errors(true);

		if ($isFile) {
			// Try to load the XML file
			$xml = simplexml_load_file($data, $class);
		} else {
			// Try to load the XML string
			$xml = simplexml_load_string($data, $class);
		}

		if ($xml === false) {
			JLog::add(JText::_('JLIB_UTIL_ERROR_XML_LOAD'), JLog::WARNING, 'jerror');

			if ($isFile) {
				JLog::add($data, JLog::WARNING, 'jerror');
			}

			foreach (libxml_get_errors() as $error) {
				JLog::add($error->message, JLog::WARNING, 'jerror');
			}
		}

		return $xml;
	}

	public static function getEditor($editor = null) {
		JLog::add(__METHOD__ . ' is deprecated. Use JEditor directly.', JLog::WARNING, 'deprecated');

		if (!class_exists('JEditor')) {
			throw new BadMethodCallException('JEditor not found');
		}

		// Get the editor configuration setting
		if (is_null($editor)) {
			$conf = self::getConfig();
			$editor = $conf->get('editor');
		}

		return JEditor::getInstance($editor);
	}

	public static function getURI($uri = 'SERVER') {
		JLog::add(__METHOD__ . ' is deprecated. Use JUri directly.', JLog::WARNING, 'deprecated');

		return JUri::getInstance($uri);
	}

	public static function getDate($time = 'now', $tzOffset = null) {
		static $classname;
		static $mainLocale;

		$language = self::getLanguage();
		$locale = $language->getTag();

		if (!isset($classname) || $locale != $mainLocale) {
			// Store the locale for future reference
			$mainLocale = $locale;

			if ($mainLocale !== false) {
				$classname = str_replace('-', '_', $mainLocale) . 'Date';

				if (!class_exists($classname)) {
					// The class does not exist, default to JDate
					$classname = 'JDate';
				}
			} else {
				// No tag, so default to JDate
				$classname = 'JDate';
			}
		}

		$key = $time . '-' . ($tzOffset instanceof DateTimeZone ? $tzOffset->getName() : (string) $tzOffset);

		if (!isset(self::$dates[$classname][$key])) {
			self::$dates[$classname][$key] = new $classname($time, $tzOffset);
		}

		$date = clone self::$dates[$classname][$key];

		return $date;
	}

	protected static function createConfig($file, $type = 'PHP', $namespace = '') {
		if (is_file($file)) {
			include_once $file;
		}

		// Create the registry with a default namespace of config
		$registry = new JRegistry;

		// Sanitize the namespace.
		$namespace = ucfirst((string) preg_replace('/[^A-Z_]/i', '', $namespace));

		// Build the config name.
		$name = 'JConfig' . $namespace;

		// Handle the PHP configuration type.
		if ($type == 'PHP' && class_exists($name)) {
			// Create the JConfig object
			$config = new $name;

			// Load the configuration values into the registry
			$registry->loadObject($config);
		}

		return $registry;
	}

	protected static function createSession(array $options = array()) {
		// Get the editor configuration setting
		$conf = self::getConfig();
		$handler = $conf->get('session_handler', 'none');

		// Config time is in minutes
		$options['expire'] = ($conf->get('lifetime')) ? $conf->get('lifetime') * 60 : 900;

		$session = JSession::getInstance($handler, $options);

		if ($session->getState() == 'expired') {
			$session->restart();
		}

		return $session;
	}

	protected static function createDbo() {
		$conf = self::getConfig();

		$host = $conf->get('host');
		$user = $conf->get('user');
		$password = $conf->get('password');
		$database = $conf->get('db');
		$prefix = $conf->get('dbprefix');
		$driver = $conf->get('dbtype');
		$debug = $conf->get('debug');

		$options = array('driver' => $driver, 'host' => $host, 'user' => $user, 'password' => $password, 'database' => $database, 'prefix' => $prefix);

		try {
			$db = JDatabaseDriver::getInstance($options);
		} catch (RuntimeException $e) {
			if (!headers_sent()) {
				header('HTTP/1.1 500 Internal Server Error');
			}

			jexit('Database Error: ' . $e->getMessage());
		}

		$db->setDebug($debug);

		return $db;
	}

	protected static function createMailer() {
		$conf = self::getConfig();

		$smtpauth = ($conf->get('smtpauth') == 0) ? null : 1;
		$smtpuser = $conf->get('smtpuser');
		$smtppass = $conf->get('smtppass');
		$smtphost = $conf->get('smtphost');
		$smtpsecure = $conf->get('smtpsecure');
		$smtpport = $conf->get('smtpport');
		$mailfrom = $conf->get('mailfrom');
		$fromname = $conf->get('fromname');
		$mailer = $conf->get('mailer');

		// Create a JMail object
		$mail = JMail::getInstance();

		// Set default sender without Reply-to
		$mail->SetFrom(JMailHelper::cleanLine($mailfrom), JMailHelper::cleanLine($fromname), 0);

		// Default mailer is to use PHP's mail function
		switch ($mailer) {
			case 'smtp':
				$mail->useSMTP($smtpauth, $smtphost, $smtpuser, $smtppass, $smtpsecure, $smtpport);
				break;

			case 'sendmail':
				$mail->IsSendmail();
				break;

			default:
				$mail->IsMail();
				break;
		}

		return $mail;
	}

	protected static function createLanguage() {
		$conf = self::getConfig();
		$locale = $conf->get('language');
		$debug = $conf->get('debug_lang');
		$lang = JLanguage::getInstance($locale, $debug);

		return $lang;
	}

	protected static function createDocument() {
		$lang = self::getLanguage();

		$input = self::getApplication()->input;
		$type = $input->get('format', 'html', 'word');

		$version = new JVersion;

		$attributes = array(
			'charset' => 'utf-8',
			'lineend' => 'unix',
			'tab' => '  ',
			'language' => $lang->getTag(),
			'direction' => $lang->isRTL() ? 'rtl' : 'ltr',
			'mediaversion' => $version->getMediaVersion()
		);

		return JDocument::getInstance($type, $attributes);
	}

	public static function getStream($use_prefix = true, $use_network = true, $ua = null, $uamask = false) {
		jimport('joomla.filesystem.stream');

		// Setup the context; Joomla! UA and overwrite
		$context = array();
		$version = new JVersion;

		// Set the UA for HTTP and overwrite for FTP
		$context['http']['user_agent'] = $version->getUserAgent($ua, $uamask);
		$context['ftp']['overwrite'] = true;

		if ($use_prefix) {
			$FTPOptions = JClientHelper::getCredentials('ftp');
			$SCPOptions = JClientHelper::getCredentials('scp');

			if ($FTPOptions['enabled'] == 1 && $use_network) {
				$prefix = 'ftp://' . $FTPOptions['user'] . ':' . $FTPOptions['pass'] . '@' . $FTPOptions['host'];
				$prefix .= $FTPOptions['port'] ? ':' . $FTPOptions['port'] : '';
				$prefix .= $FTPOptions['root'];
			} elseif ($SCPOptions['enabled'] == 1 && $use_network) {
				$prefix = 'ssh2.sftp://' . $SCPOptions['user'] . ':' . $SCPOptions['pass'] . '@' . $SCPOptions['host'];
				$prefix .= $SCPOptions['port'] ? ':' . $SCPOptions['port'] : '';
				$prefix .= $SCPOptions['root'];
			} else {
				$prefix = JPATH_ROOT . '/';
			}

			$retval = new JStream($prefix, JPATH_ROOT, $context);
		} else {
			$retval = new JStream('', '', $context);
		}

		return $retval;
	}

}
