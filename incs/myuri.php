<?php

defined('_JEXEC') or die('Restricted access');

require_once(JPATH_SITE . DS . 'libraries' . DS . 'src' . DS . 'Uri' . DS . 'Uri.php');

class MyUri extends JURI {

	public function __construct($uri = null) {
		parent::__construct($uri);
	}

	public static function getItemId($link, $access = NULL) {
		$itemid = 0;
		if (is_string($link) && $link) {
			static $mItems = array();
			if (!isset($mItems[$link])) {
				$app = JFactory::getApplication();
				$menu = $app->getMenu();
				$mItems[$link] = $menu->getItems('link', $link);
			}

			$items = $mItems[$link];
			if ($items && isset($items[0])) {
				if (is_null($access)) {
					$itemid = $items[0]->id;
				} else {
					foreach ($items as $item) {
						if ($access == $item->access) {
							$itemid = $item->id;
						}
					}
				}
			}
		}
		return $itemid;
	}

	public static function currentComUrl($params = array(), $encodeUrl = FALSE, $url = 'index.php') {
		return self::comUrl(MyHelper::getCurrentComponentName(TRUE), $params, $encodeUrl, $url);
	}

	public static function comUrl($component, $params = array(), $encodeUrl = FALSE, $url = 'index.php') {
		if (is_array($params) && count($params)) {
			$params['option'] = $component;
		} elseif (is_string($params)) {
			if ($params) {
				$params .= "&option={$component}";
			} else {
				$params .= "option={$component}";
			}
		}
		return self::url($url, $params, $encodeUrl);
	}

	public static function url($url = 'index.php', $params = array(), $encodeUrl = FALSE, $encodeFn = 'rawurlencode', $strip = array()) {
		if (empty($url) || !is_string($url)) {
			$url = 'index.php';
		}

		//strip url params
		if ($strip) {
			$_url = parse_url($url);
			if (isset($_url['query']) && $_url['query']) {
				$_url['_query'] = explode('&', $_url['query']);
				foreach ($_url['_query'] as $v) {
					$v = explode('=', $v);
					$_url['_query_parts'][$v[0]] = $v[1] ? $v[1] : '';
				}

				//case $strip if not array
				if (!is_array($strip)) {
					$strip = (array) $strip;
				}

				//striping
				foreach ($strip as $v) {
					if (isset($_url['_query_parts'][$v])) {
						unset($_url['_query_parts'][$v]);
					}
				}

				$url = str_replace($_url['query'], '', $url);
				if ($_url['_query_parts']) {
					$_url['_query_join'] = array();
					foreach ($_url['_query_parts'] as $k => $v) {
						$_url['_query_join'][] = "{$k}={$v}";
					}
					$url .= implode('&', $_url['_query_join']);
				}
			}
		}

		if ($params) {
			if (is_array($params) && count($params)) {
				$p = array();
				$encodeParams = ($encodeFn && function_exists($encodeFn));
				foreach ($params as $k => $v) {
					if ($encodeParams) {
						$v = $encodeFn($v);
					}
					$p[] = "{$k}={$v}";
				}
				$p = implode('&', $p);
			} elseif (is_string($params) && $params) {
				$p = $params;
			}

			if (!empty($p)) {
				$url .= (stripos($url, '?') !== FALSE) ? '&' . $p : '?' . $p;
			}
		}

		$encodeUrl = ($encodeUrl && $encodeFn && function_exists($encodeFn));
		if ($encodeUrl) {
			$url = $encodeFn($url);
		}
		return $url;
	}

	public static function getUrls($key = NULL) {
		static $allUrls = NULL;
		if (isset($allUrls)) {
			$urls = $allUrls;
		} else {
			$app = JFactory::getApplication();
			$isAdmin = $app->isAdmin();
			$template = $app->getTemplate();
			$rootUrl = self::root();
			$baseUrl = self::base();

			if ($baseUrl) {
				$_baseUrl = str_replace(array('/', '.'), '', $baseUrl); //make sure it is empty
			}

			if (empty($_baseUrl)) {
				$jConf = & JFactory::getConfig();
				$baseUrl = $jConf->get('siteUrl');
			}
			unset($_baseUrl);

			if ($isAdmin) {
				$siteUrl = $rootUrl;
				$adminUrl = $baseUrl;
			} else {
				$siteUrl = $baseUrl;
				$adminUrl = $siteUrl . 'administrator/';
			}

			//Hacked to display active menu item
			$Itemid = JRequest::getInt('Itemid');
			$Itemid = $Itemid ? $Itemid : 103;
			//Hacked to display active menu item

			$urls = array(
				'root' => $rootUrl,
				'base' => $baseUrl,
				'site' => $siteUrl,
				'admin' => $adminUrl,
				'site_merchant' => $rootUrl . 'merchant/',
				'site_admin' => $rootUrl . 'admin/',
				'current' => self::getInstance()->toString(),
				'js' => "{$rootUrl}incs/js/",
				'css' => "{$rootUrl}incs/css/",
				'img' => "{$rootUrl}images/",
				'tpl' => array(
					'html' => "{$siteUrl}templates/{$template}/html/",
					'css' => "{$siteUrl}templates/{$template}/css/",
					'img' => "{$siteUrl}templates/{$template}/images/",
				),
				'user' => array(
					'login' => "index.php?option=com_users&view=login", //[return=]
					'profile' => "index.php?option=com_users&view=profile",
				),
				'campaign' => array(
					'new' => "index.php?option=com_campaigns&view=campaign&Itemid=" . $Itemid, //[id=]
					'list' => "index.php?option=com_campaigns&view=list&Itemid=" . $Itemid, //[id=]
					'template' => "index.php?option=com_campaigns&view=template&Itemid=" . $Itemid, //[id=]
					'templatepage' => "index.php?option=com_campaigns&view=templatepage",
					'page' => "index.php?option=com_campaigns&view=page&Itemid=" . $Itemid, //cid=[pid=]
					'elements' => "index.php?option=com_campaigns&task=elements&Itemid=" . $Itemid, //cid=
					'preview' => "index.php?option=com_campaigns&view=draft", //cid=pid=
					'preview_pc' => "index.php?option=com_campaigns&view=draft&pc=1", //cid=pid=
					'view_participants' => "index.php?option=com_members&view=participants", //cid=
				),
				'gmap' => array(
					'js' => 'http://maps.googleapis.com/maps/api/js?v=3&sensor=false',
					'static' => 'http://maps.googleapis.com/maps/api/staticmap?sensor=false', //&center=1.39199,103.90459&zoom=13&size=400x400&markers=color:blue|1.39199,103.90459
					'geocode' => 'http://maps.googleapis.com/maps/api/geocode/json?sensor=false',
					'geocode_xml' => 'http://maps.googleapis.com/maps/api/geocode/xml?sensor=false',
				),
			);
			$urls = array_merge($urls, array(
				'swfobject' => array(
					'js' => "{$urls['js']}swfobject/swfobject.js",
					'expressInstall' => "{$urls['js']}swfobject/expressInstall.swf",
				),
				'js_scripts' => array(
					'jquery_ui' => "{$urls['js']}jquery-ui/jquery-ui-1.9.2.custom.min.js",
					'jquery_noConflict' => "{$urls['js']}noConflict.js",
					'fancybox' => "{$urls['js']}jquery.fancybox/jquery.fancybox-1.3.4.min.js",
					'myscript' => "{$urls['js']}myscript.js?v1.1",
					'myready' => "{$urls['js']}myready.js?v1.1",
				),
				'css_scripts' => array(
					'jquery_ui' => "{$urls['js']}jquery-ui/css/smoothness/jquery-ui-1.9.2.custom.min.css",
					'fancybox' => "{$urls['js']}jquery.fancybox/jquery.fancybox-1.3.4.min.css",
				),
			));
			$allUrls = $urls;
		}

		if (!is_null($key)) {
			if (is_array($key)) {
				$urls = MyHelper::getArrayByKeys($urls, $key);
			} else {
				$urls = $urls[$key];
			}
		}
		return $urls;
	}

	static function getLogin($return = NULL) {
		$urls = self::getUrls();
		$urlParams = array();

		if ($return) {
			if ($return == 'current') {
				$return = $urls['current'];
			} elseif ($return == 'home') {
				$return = $urls['base'];
			}
			$urlParams['return'] = base64_encode($return);
		}
		return self::url($urls['user']['login'], $urlParams);
	}

	static function getUserLogin($mid = NULL, $return = NULL) {
		$urlParams = array();
		if (!is_null($mid)) {
			$urlParams['mid'] = (int) $mid;
		}

		$cid = JRequest::getInt('cid', FALSE);
		if (FALSE !== $cid) {
			$urlParams['cid'] = $cid;
		}
		return self::url(self::getLogin($return), $urlParams);
	}

	/**
	 *
	 * @param string $url
	 * @param string $msg
	 * @param string $msgType message, notice, error
	 */
	static function redirect($url = 'index.php', $msg = NULL, $msgType = 'message') {
		$app = & JFactory::getApplication();
		$app->redirect(JRoute::_($url, FALSE), $msg, $msgType);
	}

	static function toHome($msg = NULL, $msgType = 'message') {
		self::redirect(self::getUrls('base'), $msg, $msgType);
	}

	static function toSite($msg = NULL, $msgType = 'message') {
		self::redirect(self::getUrls('root'), $msg, $msgType);
	}

	static function toLogin($msg = NULL, $return = 'current', $msgType = 'notice') {
		self::toUserLogin(NULL, $msg, $return, $msgType);
	}

	static function toUserProfile($mid = NULL) {
		self::redirect(self::getUserProfile($mid));
	}

	static function JRoute_recursive($urls, $is_xhtml = FALSE) {
		if ($urls) {
			$is_array = is_array($urls);
			$is_object = is_object($urls);
			if ($is_array || $is_object) {
				foreach ($urls as $k => $v) {
					$v = self::JRoute_recursive($v, $is_xhtml);
					($is_array) ? $urls[$k] = $v : $urls->$k = $v;
				}
			} else {
				if (is_string($urls)) {
					$urls = JRoute::_($urls, $is_xhtml);
				}
			}
		}
		return $urls;
	}

}
