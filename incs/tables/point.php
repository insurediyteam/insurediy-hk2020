<?php

defined('JPATH_PLATFORM') or die;

jimport('joomla.database.table');
jimport('joomla.database.tableasset');

class InsureTablePoint extends JTable {

	protected $_tbl = "#__insure_points";
	protected $_tbl_key = "point_id";
	protected $_db;

	public function __construct(&$db) {
		$this->_db = $db;
		parent::__construct($this->_tbl, $this->_tbl_key, $this->_db);
	}

	public function store($updateNulls = false) {
		$date = JFactory::getDate();
		$user = JFactory::getUser();

		if ($this->point_id) {
			$this->modified = $date->toSql();
			$this->modified_by = $user->id;
		} else {
			$this->created = $date->toSql();
		}
		return parent::store($updateNulls);
	}

}
