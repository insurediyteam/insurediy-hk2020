<?php

defined('JPATH_PLATFORM') or die;

jimport('joomla.database.table');
jimport('joomla.database.tableasset');

class InsureTablePaydollarFeed extends JTable {

	protected $_tbl = "#__paydollar_feed_log";
	protected $_tbl_key = "id";
	protected $_db;

	public function __construct(&$db) {
		$this->_db = $db;
		parent::__construct($this->_tbl, $this->_tbl_key, $this->_db);
	}

	public function store($updateNulls = false) {
		$date = JFactory::getDate();
		if (!$this->id) {
			$this->created = $date->toSql();
		}
		return parent::store($updateNulls);
	}

}
