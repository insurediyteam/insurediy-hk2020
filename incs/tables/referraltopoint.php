<?php

defined('JPATH_PLATFORM') or die;

jimport('joomla.database.table');
jimport('joomla.database.tableasset');

class InsureTableReferralToPoint extends JTable {

	protected $_tbl = "#__insure_referral_to_point";
	protected $_tbl_key = "id";
	protected $_db;

	public function __construct(&$db) {
		$this->_db = $db;
		parent::__construct($this->_tbl, $this->_tbl_key, $this->_db);
	}

}
