<?php

defined('JPATH_PLATFORM') or die;

jimport('joomla.database.table');
jimport('joomla.database.tableasset');

class InsureTableProductHistory extends JTable {

	protected $_tbl = "#__products_history";
	protected $_tbl_key = "id";
	protected $_db;

	public function __construct(&$db) {
		$this->_db = $db;
		parent::__construct($this->_tbl, $this->_tbl_key, $this->_db);
	}

	public function store($updateNulls = false) {
		$date = JFactory::getDate();
//		$user = JFactory::getUser();

		if ($this->id) {
//			$this->modified = $date->toSql();
//			$this->modified_by = $user->id;
		} else {
			$this->created = $date->toSql();
		}
		return parent::store($updateNulls);
	}

}
