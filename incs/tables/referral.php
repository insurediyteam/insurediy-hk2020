<?php

defined('JPATH_PLATFORM') or die;

jimport('joomla.database.table');
jimport('joomla.database.tableasset');

class InsureTableReferral extends JTable {

	protected $_tbl = "#__insure_referrals";
	protected $_tbl_key = "r_id";
	protected $_db;

	public function __construct(&$db) {
		$this->_db = $db;
		parent::__construct($this->_tbl, $this->_tbl_key, $this->_db);
	}

	public function store($updateNulls = false) {
		$date = JFactory::getDate();
		if ($this->r_id) {
			$this->modified = $date->toSql();
		} else {
			$this->created = $date->toSql();
		}
		return parent::store($updateNulls);
	}

}
