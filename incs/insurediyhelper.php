<?php

defined('_JEXEC') or die;

class InsureDIYHelper {

	public static function generatePDF($xmlpath, $data, $pdfpath, $pdfname, $path, $bg = TRUE) {
		$raw = MySimpleXMLHelper::loadFile($xmlpath);
		$xml = MySimpleXMLHelper::bind($raw, $data);
		$namesurfix = (isset($data->namesurfix)) ? $data->namesurfix : "_" . JHtml::_('date', time(), 'dMYHis') . ".pdf";
		return MySimpleFPDIHelper::generate($path . $pdfpath, $xml, $data->id, $pdfname . $namesurfix, $bg);
	}

	public static function generateAIA($xmlpath, $data) {
		$arr = array();
		$temp = array();
		$path = JPATH_SITE . "/images/insurediyform/form/aia/";
		$temp[] = InsureDIYHelper::generatePDF($xmlpath . "aia.app.xml", $data, "AIA-ApplicationV2.pdf", "AIA-Application", $path);
		$temp[] = InsureDIYHelper::generatePDF($xmlpath . "aia.dd.xml", $data, "AIA-Direct_Debit.pdf", "AIA-Direct_Debit", $path);
		if ($data->hasLargeSum) {
			$temp[] = InsureDIYHelper::generatePDF($xmlpath . "aia.ls.xml", $data, "AIA-Large_Amount_Questionnaire.pdf", "AIA-Large_Amount_Questionnaire", $path);
		}
		foreach ($temp as $v) {
			if ($v) {
				$arr[] = $v;
			} else {
				return FALSE;
			}
		}
		return $arr;
	}

	public static function generateSUN($xmlpath, $data, $client = TRUE) {
		$arr = array();
		$temp = array();
		$path = JPATH_SITE . "/images/insurediyform/form/sun/";
		if ($client) {
			$temp[] = InsureDIYHelper::generatePDF($xmlpath . "sun.app1.xml", $data, "SunLife-Application-1.pdf", "SunLife-Application-1", $path);
			$temp[] = InsureDIYHelper::generatePDF($xmlpath . "sun.app2.xml", $data, "SunLife-Application-2.pdf", "SunLife-Application-2", $path);
			$temp[] = InsureDIYHelper::generatePDF($xmlpath . "sun.app3.xml", $data, "SunLife-Application-3.pdf", "SunLife-Application-3", $path);
			$temp[] = InsureDIYHelper::generatePDF($xmlpath . "sun.dda.xml", $data, "Sun-DDA.pdf", "Sun-Direct_Debit", $path);
			$temp[] = InsureDIYHelper::generatePDF($xmlpath . "sun.cd.xml", $data, "Sun-Customer_Declaration.pdf", "Sun-Customer_Declaration", $path);
			if ($data->hasLargeSum) {
				$temp[] = InsureDIYHelper::generatePDF($xmlpath . "sun.ls.xml", $data, "Sun-Large_Amount_Questionarie.pdf", "Sun-Large_Amount_Questionnaire", $path);
			}
		} else {
			$temp[] = InsureDIYHelper::generatePDF($xmlpath . "sun.app1.xml", $data, "SunLife-Application-1.pdf", "SunLife-Application-1", $path, FALSE);
			$temp[] = InsureDIYHelper::generatePDF($xmlpath . "sun.app2.xml", $data, "SunLife-Application-2.pdf", "SunLife-Application-2", $path, FALSE);
			$temp[] = InsureDIYHelper::generatePDF($xmlpath . "sun.app3.xml", $data, "SunLife-Application-3.pdf", "SunLife-Application-3", $path, FALSE);
			$temp[] = InsureDIYHelper::generatePDF($xmlpath . "sun.dda.xml", $data, "Sun-DDA.pdf", "Sun-Direct_Debit", $path, FALSE);
			$temp[] = InsureDIYHelper::generatePDF($xmlpath . "sun.cd.xml", $data, "Sun-Customer_Declaration.pdf", "Sun-Customer_Declaration", $path, FALSE);
			if ($data->hasLargeSum) {
				$temp[] = InsureDIYHelper::generatePDF($xmlpath . "sun.ls.xml", $data, "Sun-Large_Amount_Questionarie.pdf", "Sun-Large_Amount_Questionnaire", $path, FALSE);
			}
		}


		foreach ($temp as $v) {
			if ($v) {
				$arr[] = $v;
			} else {
				return FALSE;
			}
		}
		return $arr;
	}

	public static function getVariableList() {
		$variables = array(
			"first_name" => "{first_name}",
			"last_name" => "{last_name}",
			"attach_form_list" => "{attach_form_list}",
			"order_no" => "{order_no}",
			"address" => "{address}",
			"date_of_appointment" => "{date_of_appointment}",
			"time_of_appointment" => "{time_of_appointment}",
			"contact_number" => "{contact_number}",
			"insurer" => "{insurer}",
			"product" => "{product}",
			"points" => "{points}",
			"order_ref" => "{order_ref}",
			"user_email" => "{user_email}",
			"date" => "{date}",
			"password" => "{user_password}",
			"start_date" => "{start_date}"
		);
		return $variables;
	}

	public static function replaceVariables($body, $data) {
		$variables = self::getVariableList();
		foreach ($variables as $k => $v) {
			if (isset($data[$k])) {
				$body = str_replace($v, $data[$k], $body);
			}
		}
		$body = str_replace("images/logo_4_email.png", MyUri::getUrls("site") . "images/logo_4_email.png", $body);
		return $body;
	}

	public static function getNumOfDays($start_date, $end_date) {
		$date = new JDate($start_date);
		$date2 = new JDate($end_date);
		$days = $date->diff($date2, TRUE);
		return $days->days + 1;
	}

	public static function getUniqueOrderNo($userid = FALSE) {
		if (!$userid) {
			$user = JFactory::getUser();
			$userid = $user->id;
		}
		return JHtml::date("now", 'ymdHis') . $userid;
	}

	public static function getQuotationNoFromUON($uon, $table) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$query->select("id");
		$query->from($table);
		$query->where("unique_order_no = " . $db->quote($uon));
		$db->setQuery($query);
		return $db->loadResult();
	}

	public static function toProperTimeString($TxTime) {
		$date = substr($TxTime, 0, 10);
		$hour = substr($TxTime, 10, 2);
		$min = substr($TxTime, 12, 2);
		$sec = substr($TxTime, 14, 2);

		return "$date $hour:$min:$sec";
	}

	public static function getNationality($code) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$query->select("nicename");
		$query->from("#__insure_nationalities2");
		$query->where("iso = " . $db->quote($code));
		$db->setQuery($query);
		$result = $db->loadResult();
		return ($result) ? $result : "";
	}

	public static function getCountryOfResidence($code) {
		$db = JFactory::getDbo();
		$query = $db->getQuery(TRUE);
		$query->select("nicename");
		$query->from("#__insure_countries");
		$query->where("iso = " . $db->quote($code));
		$db->setQuery($query);
		$result = $db->loadResult();
		return ($result) ? $result : "";
	}

	public static function getCountryPhoneCode($country_id) {
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
				->select("phonecode")
				->from("#__insure_countries")
				->where("iso = " . $db->quote($country_id));
		$db->setQuery($query, 0, 1);
		return $db->loadResult();
	}

	public static function getMonthlySalary() {
		$salaries = array(
			"JGLOBAL_MONTHLY_INCOME_LESS_THAN_10000" => 10000,
			"JGLOBAL_MONTHLY_INCOME_10000_20000" => 20000,
			"JGLOBAL_MONTHLY_INCOME_20000_30000" => 30000,
			"JGLOBAL_MONTHLY_INCOME_30000_40000" => 40000,
			"JGLOBAL_MONTHLY_INCOME_40000_50000" => 50000,
			"JGLOBAL_MONTHLY_INCOME_50000_60000" => 60000,
			"JGLOBAL_MONTHLY_INCOME_60000_70000" => 70000,
			"JGLOBAL_MONTHLY_INCOME_70000_80000" => 80000,
			"JGLOBAL_MONTHLY_INCOME_80000_90000" => 90000,
			"JGLOBAL_MONTHLY_INCOME_90000_100000" => 100000,
			"JGLOBAL_MONTHLY_INCOME_100000_110000" => 110000,
			"JGLOBAL_MONTHLY_INCOME_110000_120000" => 120000,
			"JGLOBAL_MONTHLY_INCOME_MORE_THAN_120000" => 130000,
		);
		$user = JFactory::getUser();
		if (!$user->get('guest')) {
			return isset($salaries[$user->monthly_salary]) ? $salaries[$user->monthly_salary] : "";
		}
		return 0;
	}

	public static function getCurrency($default = "HK$") {
		$params = JComponentHelper::getParams("com_insurediylife");
		return $params->get("currency", $default);
	}

	public static function toMoneyFormat($num, $decimal) {
		return self::getCurrency() . " " . MyHelper::numberFormat($num, FALSE, $decimal);
	}

	public static function updateReferredBy($referred_by) {
		$user = JFactory::getUser();
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
				->update("#__users")
				->set("referred_by = " . $db->quote($referred_by))
				->where("id = " . $user->id);
		return $db->setQuery($query)->execute();
	}

	public static function updateReferredByWithQuotation($quotation) {
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
				->update("#__users")
				->set("referred_by = " . $db->quote($quotation['referred_by']))
				->where("id = " . $quotation['user_id']);
		return $db->setQuery($query)->execute();
	}

	public static function getUserFromRefid($referral_id) {
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__users")
				->where("referral_id = " . $db->quote($referral_id));
		return $db->setQuery($query)->loadObject();
	}

	public static function checkReferral($referred_by) {
		$db = JFactory::getDBO();
		$user = MyHelper::getCurrentUser();

		if ($user->referred_by) { // user already has referral
			return FALSE;
		}

		$query = $db->getQuery(TRUE)
				->select("count(*)")
				->from("#__users")
				->where("referral_id = " . $db->quote($referred_by))
				->where("id != " . $user->id); // to prevent stupid ppl from referring themselves

		return $db->setQuery($query)->loadResult(); // if count more than 1, its a valid referral
	}

	public static function checkReferralWithQuotation($quotation) {
		$db = JFactory::getDBO();
		$user = MyHelper::getCurrentUser($quotation['user_id']);
		if ($user->referred_by) { // user already has referral
			return FALSE;
		}

		$query = $db->getQuery(TRUE)
				->select("count(*)")
				->from("#__users")
				->where("referral_id = " . $db->quote($quotation['referred_by']))
				->where("id != " . $user->id); // to prevent stupid ppl from referring themselves

		return $db->setQuery($query)->loadResult(); // if count = or more than 1, its a valid referral
	}

	public static function createReferral($referred_by, $quotation, $unique_order_no, $ref_points = 0, $desc = "Others") {
		$user = MyHelper::getCurrentUser();
		$refUser = self::getUserFromRefid($referred_by);

		$referral_table = JTable::getInstance("Referral", "InsureTable");
		$rtp_table = JTable::getInstance("ReferralToPoint", "InsureTable");

		$refObj = new stdClass();
		$refObj->referred_to = $user->referral_id;
		$refObj->referred_by = $referred_by;
		$refObj->quotation = $quotation;
		$refObj->unique_order_no = $unique_order_no;
		$referral_table->bind($refObj);
		$referral_table->store();

		$point_id = self::createPoint($refUser->id, $ref_points, $quotation, $unique_order_no, "", $desc, 2);

		$rtpObj = new stdClass();
		$rtpObj->r_id = $referral_table->r_id;
		$rtpObj->point_id = $point_id;
		$rtp_table->bind($rtpObj);
		return $rtp_table->store();
	}

	public static function createReferralWithQuotation($quotation, $quotation_desc, $unique_order_no, $ref_points = 0, $desc = "Others") {
		$user = MyHelper::getCurrentUser($quotation['user_id']);
		$refUser = self::getUserFromRefid($quotation['referred_by']);

		$referral_table = JTable::getInstance("Referral", "InsureTable");
		$rtp_table = JTable::getInstance("ReferralToPoint", "InsureTable");

		$refObj = new stdClass();
		$refObj->referred_to = $user->referral_id;
		$refObj->referred_by = $quotation['referred_by'];
		$refObj->quotation = $quotation_desc;
		$refObj->unique_order_no = $unique_order_no;
		$referral_table->bind($refObj);
		$referral_table->store();

		$point_id = self::createPoint($refUser->id, $ref_points, $quotation_desc, $unique_order_no, "", $desc, 2);

		$rtpObj = new stdClass();
		$rtpObj->r_id = $referral_table->r_id;
		$rtpObj->point_id = $point_id;
		$rtp_table->bind($rtpObj);
		return $rtp_table->store();
	}

	public static function createPoint($user, $points, $quotation = "", $uon = "", $product_id = "", $desc = "Other", $type = 4) {
		$point_table = JTable::getInstance("Point", "InsureTable");
		$pointObj = new stdClass();
		$pointObj->user_id = $user;
		$pointObj->type = $type; // 1 = pur points, 2 = ref points, 3 = redeemed points, 4 = others
		$pointObj->points = $points;
		$pointObj->quotation = $quotation;
		$pointObj->unique_order_no = $uon;
		$pointObj->product_id = $product_id;
		$pointObj->description = $desc;
		$point_table->bind($pointObj);
		$point_table->store();
		return $point_table->point_id;
	}

	public static function getPromoCode($code) {
		$db = JFactory::getDBO();

		$query = $db->getQuery(TRUE)
				->select("percent")
				->from("#__insure_promocodes")
				->where("code=" . $db->quote($code) . " AND uses>0 AND trash=0 AND publish=1 AND NOW()>start_date AND NOW()<end_date");

		return $db->setQuery($query)->loadResult();
	}

	public static function usePromoCode($code) {
		$db = JFactory::getDBO();

		$query = $db->getQuery(TRUE)
				->update("#__insure_promocodes")
				->set("uses = uses-1")
				->where("code=" . $db->quote($code));

		return $db->setQuery($query)->execute();
	}

	public static function getAllUsers($key = "id") {
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__users");
		return $db->setQuery($query)->loadAssocList($key);
	}

	public static function getInsuranceName($alias = "life") {
		$array = array(
			"life" => JText::_("POLICY_TYPE_1"),
			"ci" => JText::_("POLICY_TYPE_2"),
			"hospitalself" => JText::_("POLICY_TYPE_3"),
			"travel" => JText::_("POLICY_TYPE_4"),
			"motor" => JText::_("POLICY_TYPE_5"),
			"domestic" => JText::_("POLICY_TYPE_6"),
			"hospitalchild" => JText::_("POLICY_TYPE_7"),
		);
		return isset($array[$alias]) ? $array[$alias] : JText::_("POLICY_TYPE_1");
	}

	public static function getUserPoints($user_id) {
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
				->select("SUM(points)")
				->from("#__insure_points")
				->where("status = 'A'")
				->where("user_id = " . $db->quote($user_id));
		$points = $db->setQuery($query)->loadResult();
		return (int) $points;
	}

	public static function printPlanAttr($attr, $decimal = 0) {
		if (is_numeric($attr)) {
			return MyHelper::numberFormat($attr, FALSE, $decimal);
		} else {
			return $attr;
		}
	}

	public static function getFbTitle($default = "") {
		$params = JComponentHelper::getParams("com_insurediy");
		return $params->get("fbtitle", $default);
	}

	public static function getFbDesc($default = "") {
		$params = JComponentHelper::getParams("com_insurediy");
		return $params->get("fbdesc", $default);
	}

	public static function getFbImage($default = "") {
		$params = JComponentHelper::getParams("com_insurediy");
		return MyUri::getUrls("site") . $params->get("fbimage", $default);
	}

	public static function getFbSiteName($default = FALSE) {
		$default = ($default) ? $default : JFactory::getConfig()->get("sitename", "");
		$params = JComponentHelper::getParams("com_insurediy");
		return $params->get("fbsitename", $default);
	}

	public static function getFbAppId($default = "") {
		$params = JComponentHelper::getParams("com_insurediy");
		return $params->get("fbappid", $default);
	}

	public static function getWbAppId($default = "") {
		$params = JComponentHelper::getParams("com_insurediy");
		return $params->get("wbappid", $default);
	}

	public static function getWbImage($default = "") {
		$params = JComponentHelper::getParams("com_insurediy");
		return MyUri::getUrls("site") . $params->get("wbimage", $default);
	}

	public static function getWbDesc($default = "") {
		$params = JComponentHelper::getParams("com_insurediy");
		return $params->get("wbdesc", $default);
	}

	public static function getShareLink($refid = FALSE) {
		if ($refid) {
			return sprintf(JText::_("URL_SHARE_LINK"), MyUri::getUrls("site"), $refid);
		}
		$user = JFactory::getUser();
		return sprintf(JText::_("URL_SHARE_LINK"), MyUri::getUrls("site"), $user->referral_id);
	}

	public static function getCompanyForms($insurer_code) {
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
				->select("*")
				->from("#__insure_companies")
				->where("LCASE(insurer_code) = " . $db->quote(strtolower($insurer_code)));
		$result = $db->setQuery($query)->loadObject();
		if ($result) {
			$registry = new JRegistry;
			$registry->loadString($result->forms);
			return $registry->toArray();
		}
		return FALSE;
	}

//	public static function getFbSharer($display = "popup", $appid = FALSE, $displayUrl = FALSE, $url = FALSE) {
//		$appid = ($appid) ? $appid : self::getFbAppId();
//		$displayUrl = ($displayUrl) ? $displayUrl : self::getShareLink();
//		$url = ($url) ? $url : self::getShareLink();
//
//
//		return sprintf(JText::_("URL_FB_SHARER"), self::getFbTitle(), $displayUrl, self::getFbDesc(), self::getFbImage());
//	}

	public static function getFbSharer($title = FALSE, $url = FALSE, $desc = FALSE, $image = FALSE) {
		$title = ($title) ? $title : self::getFbTitle();
		$url = ($url) ? $url : self::getShareLink();
		$desc = ($desc) ? $desc : self::getFbDesc();
		$image = ($image) ? $image : self::getFbImage();
		return sprintf(JText::_("URL_FB_SHARER"), urlencode($title), urlencode($url), urlencode($desc), ($image));
	}

	public static function getWeiboSharer($appid = FALSE, $url = FALSE, $title = FALSE, $image = FALSE, $lang = "zh_cn") {
		$appid = ($appid) ? $appid : self::getWbAppId();
		$url = ($url) ? $url : self::getShareLink();
		$title = ($title) ? $title : self::getWbDesc();
		$image = ($image) ? $image : self::getWbImage();
		return sprintf(JText::_("URL_WEIBO_SHARER"), urlencode($url), $appid, urlencode($title), urlencode($image), $lang);
	}

	public static function generateQuestionMark($text, $icon = "images/help-quote.png") {
		$output = array();
		$output[] = '<img class="hasTooltip" alt="help-quote" ';
		$output[] = 'src="' . $icon . '" title="" ';
		$output[] = 'data-original-title="' . JText::_($text) . '" />';
		return implode("", $output);
	}

	public static function generatePdfForm($origin, $dest, $data) {
		require_once(JPATH_SITE . DS . 'incs/lib/SetaPDF/Autoload.php');
		$writer = new SetaPDF_Core_Writer_File($dest);
		$document = SetaPDF_Core_Document::loadByFilename($origin, $writer);
		$formFiller = new SetaPDF_FormFiller($document);

		$fields = $formFiller->getFields();
		foreach ($fields AS $name => $field) {
			if ($field instanceof SetaPDF_FormFiller_Field_Text) :
				$value = (isset($data[$name]) && is_string($data[$name])) ? $data[$name] : "";
				$field->setValue($value);
			elseif ($field instanceof SetaPDF_FormFiller_Field_Combo):
				if (isset($data[$name]) && $data[$name]) :
					$field->checked();
				else:
					$field->unchecked();
				endif;
			elseif ($field instanceof SetaPDF_FormFiller_Field_ButtonGroup):
				$value = (isset($data[$name]) && is_string($data[$name])) ? $data[$name] : "";
				$field->setValue($value);
			endif;
		}
		$fields->flatten();
		$formFiller->save();
		$document->finish();
		return $dest;
	}

	public static function generateFileName($original, $surfix = ".pdf") {
		$timestamp = JHtml::_('date', time(), 'YmdHis');
		$name = basename($original, $surfix);
		return $name . "_" . $timestamp . $surfix;
	}

	public static function getQuotationId($uon, $remark = FALSE) {
		$allow = array("life", "travel", "hospitalself", "hospitalchild", "domestic", "ci");
		if (!$remark && !in_array($remark, $allow)) {
			return FALSE;
		}
		$db_table = "#__insure_" . $remark . "_quotations";
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
				->select("id")
				->from($db_table)
				->where("unique_order_no = " . $db->quote($uon));
		return $db->setQuery($query)->loadResult();
	}

	public static function getAllPolicies() {
		JFactory::getLanguage()->load('com_users', JPATH_ROOT);

		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
						->select("user_id, type, insurer, policy_number, cover_amt, currency, start_date, end_date, status, room_type, cover_type")
						->from("#__insure_policies")->order("type ASC");
		$policies = $db->setQuery($query)->loadAssocList();
		$temp = array();
		foreach ($policies as $policy) {
			$key = strlen($policy['user_id']) ? $policy['user_id'] : FALSE;
			$type = strlen($policy['type']) ? $policy['type'] : FALSE;
			if ($key && $type) {
				$policy_str = "";
				foreach ($policy as $k => $v) {
					switch ($k) {
						case "insurer":
							$policy_str.= JText::_("COM_USERS_POLICY_FORM_INSURER_LABEL") . " : " . $v . "; ";
							break;
						case "policy_number":
							$policy_str.= JText::_("COM_USERS_POLICY_FORM_POLICY_NO_LABEL") . " : " . $v . "; ";
							break;
						case "cover_amt":
							$policy_str.= JText::_("COM_USERS_POLICY_FORM_COVER_AMOUNT_LABEL") . " : " . $v . "; ";
							break;
						case "currency":
							$policy_str.= JText::_("COM_USERS_POLICY_FORM_CURRENCY_LABEL") . " : " . JText::_("CUR_" . $v) . "; ";
							break;
						case "start_date":
							$date = ($v == "0000-00-00") ? "N/A" : JHtml::date(strtotime($v), 'd-m-Y');
							$policy_str.= JText::_("COM_USERS_POLICY_FORM_START_DATE_LABEL") . " : " . $date . "; ";
							break;
						case "end_date":
							$date = ($v == "0000-00-00") ? "N/A" : JHtml::date(strtotime($v), 'd-m-Y');
							$policy_str.= JText::_("COM_USERS_POLICY_FORM_END_DATE_LABEL") . " : " . $date . "; ";
							break;
						case "status":
							$policy_str.= JText::_("COM_USERS_POLICY_FORM_STATUS_LABEL") . " : " . JText::_("LD_POLICY_STATUS_OPTION_" . $v) . "; ";
							break;
						case "room_type":
							if ($v) {
								$policy_str.= JText::_("COM_USERS_POLICY_FORM_ROOM_TYPE_LABEL") . " : " . JText::_("LD_ROOM_TYPE_OPTION_" . $v) . "; ";
							}
							break;
						case "cover_type":
							if ($v) {
								$policy_str.= JText::_("COM_USERS_POLICY_FORM_COVER_TYPE_LABEL") . " : " . JText::_("LD_COVER_TYPE_OPTION_" . $v) . "; ";
							}
							break;
						case "type":
						case "user_id":
						default:
							break;
					}
				}
				$temp[$key][$type][] = $policy_str;
			}
		}
		return $temp;
	}

	public static function getCalculatorCiData() {
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
				->select("user_id, annual_salary AS 'Annual Salary', years AS Years, medical_cost AS 'Medical Cost', existing_ci AS 'Existing CI', total_ci AS 'Total CI', monthly_premium AS 'Monthly Premium'")
				->from("#__calculator_ci");
		$results = $db->setQuery($query)->loadAssocList("user_id");
		$temp = array();
		foreach ($results as $key => $value) {
			$value_str = "";
			foreach ($value as $k => $v) {
				if ($k != "user_id") {
					$value_str.= $k . " : " . $v . "; ";
				}
			}
			$temp[$key] = $value_str;
		}
		return $temp;
	}

	public static function getCalculatorLifeData() {
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
				->select("user_id, expense AS 'Expense', years AS Years, child_education AS 'Child Education', outstanding_loan AS 'Outstanding Loan', addtional_sum AS 'Additional Sum', existing_life AS 'Existing Life Insurance', total_life AS 'Total Life Insurance', monthly_premium AS 'Monthly Premium'")
				->from("#__calculator_life");
		$results = $db->setQuery($query)->loadAssocList("user_id");
		$temp = array();
		foreach ($results as $key => $value) {
			$value_str = "";
			foreach ($value as $k => $v) {
				if ($k != "user_id") {
					$value_str.= $k . " : " . $v . "; ";
				}
			}
			$temp[$key] = $value_str;
		}
		return $temp;
	}

	public static function getCalculatorHosData() {
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
				->select("user_id, employer_insurance AS 'Employer Insurance', has_deductible AS 'Deductible', has_limit AS 'Limit', has_private AS 'Ward', has_international AS 'International'")
				->from("#__calculator_hospital");
		$results = $db->setQuery($query)->loadAssocList("user_id");
		$temp = array();
		foreach ($results as $key => $value) {
			$value_str = "";
			foreach ($value as $k => $v) {
				if ($k != "user_id" && $k != 'Ward') {
					$text = ($v) ? "Yes" : "No";
					$value_str.= $k . " : " . $text . "; ";
				}
				if ($k == 'Ward') {
					$text = ($v) ? "Private" : "Semi-Privates";
					$value_str.= $k . " : " . $text . "; ";
				}
			}
			$temp[$key] = $value_str;
		}
		return $temp;
	}

	public static function getMaxLifeSumInsured() {
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
				->select("MAX(sum_insured)")
				->from("#__insure_life_sum_insured");
		return $db->setQuery($query)->loadResult();
	}

	public static function getMaxCiSumInsured() {
		$db = JFactory::getDBO();
		$query = $db->getQuery(TRUE)
				->select("MAX(sum_insured)")
				->from("#__insure_ci_sum_insured");
		return $db->setQuery($query)->loadResult();
	}

	/*********************
		Policies 
	*********************/
	public static function getPoliciesStatus($status = 1) {
		$status_text = '';
		switch($status) {
			case 1: $status_text = 'Active'; break;
			case 2: $status_text = 'Inactive'; break;
			case 3: $status_text = 'Pending';break;
		}
		
		return $status_text;
	}
	
	public static function getAllUserPolicies($uid = FALSE) {
		$app = JFactory::getApplication();
		$key = "user.existing.policies." . $type;
		$data = $app->get($key, FALSE);
		$uid = ($uid) ? $uid : JFactory::getUser()->id;
		if (!$data) {
			$db = JFactory::getDbo();
			$query = $db->getQuery(TRUE)
					->select("*")
					->from("#__insure_policies")
					->where("status = 1")
					->where("user_id = " . $uid);
			$data = $db->setQuery($query)->loadObjectList();
			$app->set($key, $data);
		}
		return $data;
	}

}
