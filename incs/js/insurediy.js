
if ('undefined' === typeof (jsInsurediy)) {
	var jsInsurediy = {
		// Common stuffs
		data: {},
		_setNameSpace: function (namespace) {
			namespace = namespace || 'default';
			if (typeof (this.data[namespace]) === 'undefined') {
				this.data[namespace] = {};
			}
			return namespace;
		},
		set: function (name, value, namespace) {
			namespace = this._setNameSpace(namespace);
			var old = (null === this.data[namespace][name]) ? null : this.data[namespace][name];
			this.data[namespace][name] = value;
			return old;
		},
		get: function (name, dflt, namespace) {
			namespace = this._setNameSpace(namespace);
			dflt = (typeof (dflt) !== 'undefined') ? dflt : null;
			var val = (typeof (this.data[namespace][name]) !== 'undefined') ?
					this.data[namespace][name] : dflt;
			return val;
		},
		remove: function (name, namespace) {
			namespace = this._setNameSpace(namespace);
			var old = null;
			if (typeof (this.data[namespace][name]) !== 'undefined') {
				old = this.data[namespace][name];
				delete this.data[namespace][name];
			}
			return old;
		},
		// Project specific stuffs
		loadBranches: function (bank_id, target_id, params, preselect) {
			params = params || {};

			var _params = {
				'option': 'com_insurediylife',
				'task': 'ajax.loadBranches',
				'code': bank_id,
				//'tmpl': 'ajax',
				'type': 'json'
			};

			params = jQuery.extend({}, params, _params);
			var xhrKey = 'xhr_bank.loadBranches' + bank_id, xhr;
			xhr = this.get(xhrKey, null, 'ajax');
			if ('undefined' !== typeof (xhr) && xhr !== null) {
				if (xhr.readyState !== 4) {
					xhr.abort();
				}
				this.remove(xhrKey, 'ajax');
			}

			var jTarget = jQuery("#" + target_id);
			xhr = jQuery.ajax({
				type: 'POST',
				url: "index.php",
				data: params,
				beforeSend: function () {

				},
				success: function (r) {
					var res = jQuery.parseJSON(r);

					jTarget.html("");
					jTarget.html(res.data);
					jTarget.trigger("liszt:updated");
					if (preselect) {
						jTarget.val(preselect);
						jTarget.trigger("liszt:updated");
					}
					var jControl = jTarget.closest("div.controls");
					if(jControl.length){
						var error = jControl.find(".error-container label.error");
						if(error.length){
							error.remove();
						}
					}
					
				}
			});
			this.set(xhrKey, xhr, 'ajax');
		},
		callDeleteForm: function (fid, eid, task) {
			var jForm = document.getElementById(fid);
			var jElement = jForm.getElementById("element_id");
			var jTask = jForm.getElementById("task");
			jElement.value = eid;
			jTask.value = task;
		},
		saveWithAjax: function (fid, task, option) {
			var jForm = jQuery("#" + fid);
			var params = jForm.find(':input').serializeArray();
			jQuery.merge(params, [
				{'name': 'option', 'value': option},
				{'name': 'task', 'value': task},
				//{'name': 'tmpl', 'value': 'ajax'},
				{'name': 'type', 'value': 'json'}
			]);

			var xhrKey = 'xhr_save.stuffs.with.ajax' + fid, xhr;
			xhr = this.get(xhrKey, null, 'ajax');
			if ('undefined' !== typeof (xhr) && xhr !== null) {
				if (xhr.readyState !== 4) {
					xhr.abort();
				}
				this.remove(xhrKey, 'ajax');
			}

			xhr = jQuery.ajax({
				type: 'POST',
				url: "index.php",
				data: params,
				beforeSend: function () {

				},
				success: function (r) {
					console.log(r);
					jForm.append("<span class='mymsg' style='color:green;display:none;'>success</span>");
					jForm.find(".mymsg").fadeIn(500).fadeOut(1000);
					setTimeout(function () {
						jForm.find(".mymsg").remove()
					}, 1500);
				}
			});
		},
		addWithAjax: function (fid, task, option) {
			var jForm = jQuery("#" + fid);
			var params = jForm.find(':input').serializeArray();
			jQuery.merge(params, [
				{'name': 'option', 'value': option},
				{'name': 'task', 'value': task},
				//{'name': 'tmpl', 'value': 'ajax'},
				{'name': 'type', 'value': 'json'}
			]);

			var xhrKey = 'xhr_save.stuffs.with.ajax' + fid, xhr;
			xhr = this.get(xhrKey, null, 'ajax');
			if ('undefined' !== typeof (xhr) && xhr !== null) {
				if (xhr.readyState !== 4) {
					xhr.abort();
				}
				this.remove(xhrKey, 'ajax');
			}

			xhr = jQuery.ajax({
				type: 'POST',
				url: "index.php",
				data: params,
				beforeSend: function () {

				},
				success: function (r) {
					alert("Successfully Added!");
					location.reload();
				}
			});
		},
		saveBeforePayment: function (param1, param2) {
			var params = [
				{'name': 'option', 'value': "com_insurediylife"},
				{'name': 'task', 'value': "form.saveBeforePayment"},
				//{'name': 'tmpl', 'value': 'ajax'},
				{'name': 'type', 'value': 'json'},
				{'name': 'declare_have_you_replaced', 'value': param1},
				{'name': 'declare_do_you_intend_to_replace', 'value': param2}
			];

			var xhrKey = 'xhr_save.before.payment.ajax', xhr;
			xhr = this.get(xhrKey, null, 'ajax');
			if ('undefined' !== typeof (xhr) && xhr !== null) {
				if (xhr.readyState !== 4) {
					xhr.abort();
				}
				this.remove(xhrKey, 'ajax');
			}
			xhr = jQuery.ajax({
				type: 'POST',
				url: "index.php",
				data: params,
				beforeSend: function () {

				},
				success: function (r) {

				}
			});
		},
		onNumberOnlyAndFormat: function (input_id) {
			var jInput = jQuery(input_id);
			jInput.keydown(function (event) {
				var jThis = jQuery(this);
				var val = jThis.val();
				var hasDot = val.indexOf(".");
				// Allow only backspace and delete
				if (event.keyCode == 9 || event.keyCode == 46 || event.keyCode == 8 || (event.keyCode == 190 && hasDot == -1)) {
					// let it happen, don't do anything
				}
				else {
					// Ensure that it is a number and stop the keypress
					if ((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105)) {
						// 0-9
					} else {
						event.preventDefault();
					}
				}
			});
			jInput.on("keyup change", function () {
				var jThis = jQuery(this);
				var val = jThis.val();
				if (val.length > 0) {
					jThis.val(jsInsurediy.numberFormat(val));
				}
			});
		},
		numberFormat: function (number) {
			number = parseFloat(number.replace(/,/g, ''));
			return number.formatMoney(0, '.', ',');
		},
		testing: function () {
			console.log("called");
		}
	};

	Number.prototype.formatMoney = function (c, d, t) {
		var n = this;
		c = isNaN(c = Math.abs(c)) ? 2 : c;
		d = d === undefined ? "." : d;
		t = t === undefined ? "," : t;
		var s = n < 0 ? "-" : "";
		var i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "";
		var j = (j = i.length) > 3 ? j % 3 : 0;
		return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
	};
}

