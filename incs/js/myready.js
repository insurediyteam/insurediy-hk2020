if (typeof(_myready) === 'undefined') {
	var _myready = 1;

	if (typeof($jq) === 'undefined') {
		if (typeof(jQuery) !== 'undefined') {
			var $jq = jQuery.noConflict();
		}
	}

	$jq(document).ready(function() {
		$jq('table tr:even').addClass('even');
		$jq('table tr:odd').addClass('odd');
		$jq('table tr:first').addClass('first');
		$jq('table tr:last').addClass('last');
	});

}
